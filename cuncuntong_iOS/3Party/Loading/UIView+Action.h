//
//  ThirdViewController.m
//  Mandaring
//
//  Created by ForDeng on 14-9-25.
//  Copyright (c) 2014年 ForDeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BCAction)

- (void)makeToastActivity;

- (void)hideToastActivity;

@end
