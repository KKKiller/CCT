//
//  GifView.m
//  HotFitness
//
//  Created by 周吾昆 on 15/12/28.
//  Copyright © 2015年 HeGuangTongChen. All rights reserved.
//

#import "GifView.h"
#import <QuartzCore/QuartzCore.h>
@implementation GifView
- (id)initWithFrame:(CGRect)frame filePath:(NSString *)_filePath
{
    self = [super initWithFrame:frame];
    if (self) {
        gifProperties = [NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:0] forKey:(NSString *)kCGImagePropertyGIFLoopCount]forKey:(NSString *)kCGImagePropertyGIFDictionary];
        gif = CGImageSourceCreateWithURL((__bridge CFURLRef)[NSURL fileURLWithPath:_filePath], (__bridge CFDictionaryRef)gifProperties);
        count =CGImageSourceGetCount(gif);
        CGFloat timeIntaval = frame.size.width == 20 ? 0.07 : 0.3;
        timer = [NSTimer scheduledTimerWithTimeInterval:timeIntaval target:self selector:@selector(play) userInfo:nil repeats:YES];
        [timer fire];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame data:(NSData *)_data
{
    self = [super initWithFrame:frame];
    if (self) {
        gifProperties = [NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:0] forKey:(NSString *)kCGImagePropertyGIFLoopCount]forKey:(NSString *)kCGImagePropertyGIFDictionary];
        gif = CGImageSourceCreateWithData((__bridge CFDataRef)_data, (__bridge CFDictionaryRef)gifProperties);
        count =CGImageSourceGetCount(gif);
        CGFloat timeIntaval = frame.size.width == 20 ? 0.07 : 0.3;
        timer = [NSTimer scheduledTimerWithTimeInterval:timeIntaval target:self selector:@selector(play) userInfo:nil repeats:YES];
        [timer fire];
    }
    return self;
}

-(void)play
{
    index ++;
    index = index%count;
    CGImageRef ref = CGImageSourceCreateImageAtIndex(gif, index, (__bridge CFDictionaryRef)gifProperties);
    self.layer.contents = (__bridge id)ref;
    CFRelease(ref);
}

-(void)removeFromSuperview
{
    NSLog(@"removeFromSuperview");
    [timer invalidate];
    timer = nil;
    [super removeFromSuperview];
}

- (void)dealloc
{
    NSLog(@"dealloc");
    CFRelease(gif);
}
@end
