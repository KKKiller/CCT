//
//  ThirdViewController.m
//  Mandaring
//
//  Created by ForDeng on 14-9-25.
//  Copyright (c) 2014年 ForDeng. All rights reserved.
//

#import "UIView+Action.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
//#import "FXBlurView.h"
#import "GifView.h"
/*
 *  CONFIGURE THESE VALUES TO ADJUST LOOK & FEEL,
 *  DISPLAY DURATION, ETC.
 */

// general appearance
static const CGFloat CSToastMaxWidth            = 0.8;      // 80% of parent view width
static const CGFloat CSToastMaxHeight           = 0.8;      // 80% of parent view height
static const CGFloat CSToastHorizontalPadding   = 10.0;
static const CGFloat CSToastVerticalPadding     = 10.0;
static const CGFloat CSToastCornerRadius        = 10.0;
static const CGFloat CSToastOpacity             = 0.8;
static const CGFloat CSToastFontSize            = 16.0;
static const CGFloat CSToastMaxTitleLines       = 0;
static const CGFloat CSToastMaxMessageLines     = 0;
static const CGFloat CSToastFadeDuration        = 0.2;

// shadow appearance
static const CGFloat CSToastShadowOpacity       = 0.8;
static const CGFloat CSToastShadowRadius        = 6.0;
static const CGSize  CSToastShadowOffset        = { 4.0, 4.0 };
static const BOOL    CSToastDisplayShadow       = YES;

// display duration and position
//static const CGFloat CSToastDefaultDuration     = 2.0;
static const NSString * CSToastDefaultPosition  = @"bottom";

// image view size
static const CGFloat CSToastImageViewWidth      = 50.0;
static const CGFloat CSToastImageViewHeight     = 50.0;

// activity
static const NSString * CSToastActivityDefaultPosition = @"center";
static const NSString * CSToastActivityViewKey  = @"CSToastActivityViewKey";
static const NSString * CSToastCoverViewKey  = @"CSToastCoverViewKey";



@interface UIView (ActionPrivate)

- (CGPoint)centerPointForPosition:(id)position withToast:(UIView *)toast;
- (UIView *)viewForMessage:(NSString *)message title:(NSString *)title image:(UIImage *)image;

@end


@implementation UIView (BCAction)


#pragma mark - Toast Activity Methods

- (void)makeToastActivity {
    [self makeToastActivity:CSToastActivityDefaultPosition];
}

- (void)makeToastActivity:(id)position {
    //容器
    UIView *existingActivityView = (UIView *)objc_getAssociatedObject(self, &CSToastActivityViewKey);
    if (existingActivityView != nil) return;

    
    //灰色透明背景
//    CGFloat Y = self.tag == 1010 ? 0 : 64;
    UIView * activityBgView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
    activityBgView.backgroundColor=[UIColor clearColor];

//    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, APP_Frame_Height + 20)];
//    backView.backgroundColor = [UIColor blackColor];
//    backView.alpha = 0.2;
//    [activityBgView addSubview:backView];
    
    //gif图
//    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"loading" ofType:@"gif"]];
//    GifView *dataView = [[GifView alloc] initWithFrame:CGRectMake(0, 0, 75, 75) data:data];
//    [activityBgView addSubview:dataView];
    
//    NSMutableArray *refreshImgs = [NSMutableArray array];
//    for (int i = 0; i<= 9; i++) {
//        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"loading%d", i]];
//        [refreshImgs addObject:image];
//    }
//    UIImageView *dataView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 75, 75)];
//    dataView.animationDuration = 0.75;
//    dataView.animationImages = refreshImgs;
//    dataView.animationRepeatCount = LONG_MAX;
//    [activityBgView addSubview:dataView];
//    [dataView startAnimating];
//    
//    [dataView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(activityBgView.mas_centerX);
//        make.centerY.equalTo(activityBgView.mas_centerY).offset(-40);
//        make.width.height.mas_equalTo(75);
//    }];
    
    
    //tabbar遮盖view
//    UIView *existingCoverView = (UIView *)objc_getAssociatedObject(App_Delegate.mainTabbarVC.tabBarView, &CSToastCoverViewKey);
//    if (existingCoverView == nil){
//        UIView *coverView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Frame_Width, 49)];
//        coverView.backgroundColor = [UIColor blackColor];
//        coverView.userInteractionEnabled = NO;
//        coverView.alpha = 0.2;
//        objc_setAssociatedObject (App_Delegate.mainTabbarVC.tabBarView, &CSToastCoverViewKey, coverView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//        [App_Delegate.mainTabbarVC.tabBarView addSubview:coverView];
//    }
   
    //转菊花
//    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:activityBgView];
//    [activityBgView addSubview:HUD];
//    HUD.opacity = 0.3;
//    [HUD show:YES];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.center = CGPointMake(kScreenWidth * 0.5, kScreenHeight * 0.5 - 60);
    [indicator startAnimating];
    [activityBgView addSubview:indicator];
    
    objc_setAssociatedObject (self, &CSToastActivityViewKey, activityBgView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addSubview:activityBgView];

}

- (void)hideToastActivity {
    
//    UIView *existingCoverView = (UIView *)objc_getAssociatedObject(App_Delegate.mainTabbarVC.tabBarView, &CSToastCoverViewKey);
//    if (existingCoverView != nil){
//        [UIView animateWithDuration:CSToastFadeDuration
//                              delay:0.0
//                            options:(UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState)
//                         animations:^{
//                             existingCoverView.alpha = 0.0;
//                         } completion:^(BOOL finished) {
//                             [existingCoverView removeFromSuperview];
//                             objc_setAssociatedObject (App_Delegate.mainTabbarVC.tabBarView, &CSToastCoverViewKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//                         }];
//    }
    //菊花
//    [MBProgressHUD hideAllHUDsForView:self animated:YES];
    
    UIView *existingActivityView = (UIView *)objc_getAssociatedObject(self, &CSToastActivityViewKey);
    if (existingActivityView != nil) {
        [UIView animateWithDuration:CSToastFadeDuration
                              delay:0.0
                            options:(UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState)
                         animations:^{
                             existingActivityView.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             [existingActivityView removeFromSuperview];
                             objc_setAssociatedObject (self, &CSToastActivityViewKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                         }];
    }
}

- (void)tapStopAction:(UITapGestureRecognizer *)tap
{
    [self hideToastActivity];
}

#pragma mark - Private Methods

- (CGPoint)centerPointForPosition:(id)point withToast:(UIView *)toast {
    if([point isKindOfClass:[NSString class]]) {
        // convert string literals @"top", @"bottom", @"center", or any point wrapped in an NSValue object into a CGPoint
        if([point caseInsensitiveCompare:@"top"] == NSOrderedSame) {
            return CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
        } else if([point caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
            return CGPointMake(self.bounds.size.width/2, self.bounds.size.height / 2);
        } else if([point caseInsensitiveCompare:@"center"] == NSOrderedSame) {
            return CGPointMake(App_Width / 2, App_Height / 2 - 50);
        }
    } else if ([point isKindOfClass:[NSValue class]]) {
        return [point CGPointValue];
    }

    return [self centerPointForPosition:CSToastDefaultPosition withToast:toast];
}

- (UIView *)viewForMessage:(NSString *)message title:(NSString *)title image:(UIImage *)image {
    // sanity
    if((message == nil) && (title == nil) && (image == nil)) return nil;
    
    // dynamically build a toast view with any combination of message, title, & image.
    UILabel *messageLabel = nil;
    UILabel *titleLabel = nil;
    UIImageView *imageView = nil;
    
    // create the parent view
    UIView *wrapperView = [[UIView alloc] init];
    wrapperView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    wrapperView.layer.cornerRadius = CSToastCornerRadius;
    
    if (CSToastDisplayShadow) {
        wrapperView.layer.shadowColor = [UIColor blackColor].CGColor;
        wrapperView.layer.shadowOpacity = CSToastShadowOpacity;
        wrapperView.layer.shadowRadius = CSToastShadowRadius;
        wrapperView.layer.shadowOffset = CSToastShadowOffset;
    }
    
    wrapperView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:CSToastOpacity];
    
    if(image != nil) {
        imageView = [[UIImageView alloc] initWithImage:image];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.frame = CGRectMake(CSToastHorizontalPadding, CSToastVerticalPadding, CSToastImageViewWidth, CSToastImageViewHeight);
    }
    
    CGFloat imageWidth, imageHeight, imageLeft;
    
    // the imageView frame values will be used to size & position the other views
    if(imageView != nil) {
        imageWidth = imageView.bounds.size.width;
        imageHeight = imageView.bounds.size.height;
        imageLeft = CSToastHorizontalPadding;
    } else {
        imageWidth = imageHeight = imageLeft = 0.0;
    }
    
    if (title != nil) {
        titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = CSToastMaxTitleLines;
        titleLabel.font = [UIFont boldSystemFontOfSize:CSToastFontSize];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.alpha = 1.0;
        titleLabel.text = title;
        
        // size the title label according to the length of the text
        CGSize maxSizeTitle = CGSizeMake((self.bounds.size.width * CSToastMaxWidth) - imageWidth, self.bounds.size.height * CSToastMaxHeight);
//        CGSize expectedSizeTitle = [title sizeWithFont:titleLabel.font constrainedToSize:maxSizeTitle lineBreakMode:titleLabel.lineBreakMode];
//        
        
        NSDictionary *attrDict = @{NSFontAttributeName:titleLabel.font,};
        CGSize expectedSizeTitle = [title boundingRectWithSize:maxSizeTitle options:NSStringDrawingUsesLineFragmentOrigin attributes:attrDict context:nil].size;
        
        titleLabel.frame = CGRectMake(0.0, 0.0, expectedSizeTitle.width, expectedSizeTitle.height);
    }
    
    if (message != nil) {
        messageLabel = [[UILabel alloc] init];
        messageLabel.numberOfLines = CSToastMaxMessageLines;
        messageLabel.font = [UIFont systemFontOfSize:CSToastFontSize];
        messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        messageLabel.textColor = [UIColor whiteColor];
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.alpha = 1.0;
        messageLabel.text = message;
        
        // size the message label according to the length of the text
        CGSize maxSizeMessage = CGSizeMake((self.bounds.size.width * CSToastMaxWidth) - imageWidth, self.bounds.size.height * CSToastMaxHeight);
//        CGSize expectedSizeMessage = [message sizeWithFont:messageLabel.font constrainedToSize:maxSizeMessage lineBreakMode:messageLabel.lineBreakMode];
      
        NSDictionary *attrDict = @{NSFontAttributeName:messageLabel.font,};
        CGSize expectedSizeMessage = [title boundingRectWithSize:maxSizeMessage options:NSStringDrawingUsesLineFragmentOrigin attributes:attrDict context:nil].size;
        messageLabel.frame = CGRectMake(0.0, 0.0, expectedSizeMessage.width, expectedSizeMessage.height);
    }
    
    // titleLabel frame values
    CGFloat titleWidth, titleHeight, titleTop, titleLeft;
    
    if(titleLabel != nil) {
        titleWidth = titleLabel.bounds.size.width;
        titleHeight = titleLabel.bounds.size.height;
        titleTop = CSToastVerticalPadding;
        titleLeft = imageLeft + imageWidth + CSToastHorizontalPadding;
    } else {
        titleWidth = titleHeight = titleTop = titleLeft = 0.0;
    }
    
    // messageLabel frame values
    CGFloat messageWidth, messageHeight, messageLeft, messageTop;
    
    if(messageLabel != nil) {
        messageWidth = messageLabel.bounds.size.width;
        messageHeight = messageLabel.bounds.size.height;
        messageLeft = imageLeft + imageWidth + CSToastHorizontalPadding;
        messageTop = titleTop + titleHeight + CSToastVerticalPadding;
    } else {
        messageWidth = messageHeight = messageLeft = messageTop = 0.0;
    }
    
    
    CGFloat longerWidth = MAX(titleWidth, messageWidth);
    CGFloat longerLeft = MAX(titleLeft, messageLeft);
    
    // wrapper width uses the longerWidth or the image width, whatever is larger. same logic applies to the wrapper height
    CGFloat wrapperWidth = MAX((imageWidth + (CSToastHorizontalPadding * 2)), (longerLeft + longerWidth + CSToastHorizontalPadding));
    CGFloat wrapperHeight = MAX((messageTop + messageHeight + CSToastVerticalPadding), (imageHeight + (CSToastVerticalPadding * 2)));
    
    wrapperView.frame = CGRectMake(0.0, 0.0, wrapperWidth, wrapperHeight);
    
    if(titleLabel != nil) {
        titleLabel.frame = CGRectMake(titleLeft, titleTop, titleWidth, titleHeight);
        [wrapperView addSubview:titleLabel];
    }
    
    if(messageLabel != nil) {
        messageLabel.frame = CGRectMake(messageLeft, messageTop, messageWidth, messageHeight);
        [wrapperView addSubview:messageLabel];
    }
    
    if(imageView != nil) {
        [wrapperView addSubview:imageView];
    }
    
    return wrapperView;
}

@end
