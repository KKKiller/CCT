//
//  MBProgressHUDs+Show.m
//  Mentor
//
//  Created by 7 on 2017/5/9.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import "MBProgressHUDs+Show.h"

@implementation MBProgressHUDs (Show)

+ (MBProgressHUDs *)showLoadingHUDWithText:(NSString *)text {
    // 显示加载失败
    MBProgressHUDs *hud = [MBProgressHUDs showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    
//    hud.yOffset = -([UIScreen mainScreen].bounds.size.height * 0.15);
//    hud.offset = CGPointMake(hud.offset.x, -([UIScreen mainScreen].bounds.size.height * 0.15));
    
    // 显示一张图片(mode必须写在customView设置之前)
    hud.mode = MBProgressHUDModeIndeterminate;
    // 设置一张图片
    //    name = [NSString stringWithFormat:@"MBProgressHUD.bundle/%@", name];
    //    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:name]];
    
    hud.label.text = text;
    
    // 隐藏的时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    return hud;
}

@end
