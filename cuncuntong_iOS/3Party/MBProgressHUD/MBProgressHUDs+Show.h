//
//  MBProgressHUDs+Show.h
//  Mentor
//
//  Created by 7 on 2017/5/9.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import "MBProgressHUDs.h"

@interface MBProgressHUDs (Show)

+ (MBProgressHUDs *)showLoadingHUDWithText:(NSString *)text;

@end
