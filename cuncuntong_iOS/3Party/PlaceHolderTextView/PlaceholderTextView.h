//
//  PlaceholderTextView.h
//  Offering
//
//  Created by admin on 15/8/8.
//  Copyright (c) 2015年 rgc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceholderTextView : UITextView

@property (nonatomic,strong) NSString *placehoder;
@property (nonatomic,strong) UIColor  *placeholderTextColor;

@end
