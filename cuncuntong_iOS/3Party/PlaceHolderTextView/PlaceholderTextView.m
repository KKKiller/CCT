//
//  PlaceholderTextView.m
//  Offering
//
//  Created by admin on 15/8/8.
//  Copyright (c) 2015年 rgc. All rights reserved.
//

#import "PlaceholderTextView.h"
#define kNotificationCenter [NSNotificationCenter defaultCenter]

@interface PlaceholderTextView()
{
    BOOL _sholdDrawPlaceholder;
}

@end
@implementation PlaceholderTextView


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (void)setText:(NSString *)text
{
    [super setText:text];
    [self drawPlaceholder];
    return;
}
- (void)setPlacehoder:(NSString *)placehoder
{
    if (![placehoder isEqualToString:_placehoder]) {
        _placehoder = placehoder;
        [self drawPlaceholder];
    }
    return;
}
#pragma mark - 父类方法
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder]) {
        [self configureBase];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self configureBase];
    }
    return self;
}
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (_sholdDrawPlaceholder) {
        [_placeholderTextColor set];
        [_placehoder drawInRect:CGRectMake(8.0f, 8.0f, self.frame.size.width-16.0f, self.frame.size.width-16.0f) withFont:self.font];
    }
    return;
}
- (void)configureBase
{
    
    [kNotificationCenter addObserver:self
                            selector:@selector(textChanged:)
                                name:UITextViewTextDidChangeNotification
                              object:self];
    
    self.placeholderTextColor = [UIColor colorWithWhite:0.702f alpha:1.0f];
    _sholdDrawPlaceholder = NO;
    return;
}
- (void)drawPlaceholder {
    BOOL prev = _sholdDrawPlaceholder;
    _sholdDrawPlaceholder = self.placehoder && self.placeholderTextColor && self.text.length == 0;
    
    if (prev != _sholdDrawPlaceholder) {
        [self setNeedsDisplay];
    }
    return;
}

- (void)textChanged:(NSNotification *)notification {
    [self drawPlaceholder];
    return;
}

- (void)dealloc {
    [kNotificationCenter removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    return;
}

@end
