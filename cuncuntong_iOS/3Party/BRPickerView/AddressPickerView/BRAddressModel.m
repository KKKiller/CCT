//
//  BRAddressModel.m
//  BRPickerViewDemo
//
//  Created by 任波 on 2017/8/11.
//  Copyright © 2017年 renb. All rights reserved.
//

#import "BRAddressModel.h"

@implementation BRProvinceModel


+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"subList" : [BRProvinceModel class]};
}

@end

