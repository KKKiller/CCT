//
//  CCAnimationConfig.h
//  animation
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN


/*
 addCountOneMinute 代码中动态修改此属性，可改变金币产出速度
 totalCoinCount  用户总的金币数，用户点击采集按钮后，会改变该值
 currentCoinCount 金币采集过程中，该值也在改变
 */
@interface CCCoinConfig : NSObject
//每分钟采集金币个数
@property(nonatomic,assign)int addCountOneMinute;
//当前总的金币数量
@property(nonatomic,assign)int totalCoinCount;
//当前采集的金币数量
@property(nonatomic,assign)int currentCoinCount;
//上限
@property(nonatomic,assign)int maxCoinCount;
@property(nonatomic,assign)int leftMaxCoinCount;





//产生泡泡的时间间隔
@property(nonatomic,assign)CGFloat newPaoPaoTime;
//抛物线的金币最顶部的y值
@property(nonatomic,assign)int pull_coin_min_y;
//气泡大小
@property(nonatomic,assign)int pao_pao_size;
//气泡横向开始的x坐标
@property(nonatomic,assign)int pao_start_x;
//气泡开始的Y坐标
@property(nonatomic,assign)int pao_satrt_y;
//气泡水平垂直位置位移范围
@property(nonatomic,assign)int pao_area;
//抛物线金币开始的x坐标
@property(nonatomic,assign)int coin_start_x;
//抛物线金币开始的y坐标
@property(nonatomic,assign)int coin_start_y;
//抛物线金币的宽高尺寸
@property(nonatomic,assign)int coin_body_size;
//盘子里面金币下落时的距离
@property(nonatomic,assign)int collectCoinDownDistance;


/**
 获取抛物线终点坐标
 @param currentNum 当前盘子里面的采集数量，即为currentCoinCount
 @return 抛物线终点坐标
 */
- (CGPoint)getDesPoint:(int)currentNum;
@end

NS_ASSUME_NONNULL_END
