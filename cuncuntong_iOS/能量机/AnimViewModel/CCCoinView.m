//
//  CCCoinView.m
//  animation
//
//

#import "CCCoinView.h"
#import "CCTimeHolder.h"
#import <Masonry.h>
#import "UIImage+GIF.h"
#import "UIImageView+WebCache.h"

@interface PullCoinModel : NSObject
@property (nonatomic,assign)int currentStep;
@property (nonatomic,assign)int totalStep;
@property (nonatomic,assign)CGFloat startX;
@property (nonatomic,assign)CGFloat startY;
@property (nonatomic,assign)CGFloat desX;
@property (nonatomic,assign)CGFloat desY;
@property (nonatomic,assign)CGFloat minY;
@property (nonatomic,assign)CGFloat a;
@property (nonatomic,assign)CGFloat b;
@property (nonatomic,retain)UIImageView *pullImgView;
@end
@implementation PullCoinModel
@end




#define TOTAL_ITEM_NUM_HEIGHT 20 //底部滚动文字每个item的高度
#define TEXT_ORIGIN_TOP_GAP 80 //文字动画的顶部间距

@interface CCCoinView(){
    CCCoinConfig *_config;
    UIImage *coinAnimImg;//闪光的金币图片
}
@property (weak, nonatomic) IBOutlet UIImageView *yanImgView;
@property (weak, nonatomic) IBOutlet UIView *topBgView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
//点击采集按钮后动画的背景view
@property (weak, nonatomic) IBOutlet UIView *coinAnimBgView;
//采集金币时动画的金币控件
@property (nonatomic,strong)NSArray *collectCoinAnimViewList;
//当前采集的金币数量的label
@property (weak, nonatomic) IBOutlet UILabel *panCoinCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *maxCoinLbl;

//盘子里面金币的数组
@property (nonatomic,strong)NSArray *panCoinList;
//当前正在闪动星星的金币
@property (nonatomic,assign)int currentAnimCoinNum;

//抛物线模型数组
@property (nonatomic,strong)NSMutableArray *pullCoinModelList;

//总共金额的背景View
@property (weak, nonatomic) IBOutlet UIView *totalCountBgView;
//文字向上并淡出的动画view
@property (weak, nonatomic) IBOutlet UIView *addTextView;
//文字向上动画的背景视图的顶部间距
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightTextTopGap;


//泡泡的计时任务
@property(nonatomic,strong)PTActionModel *paopaoAction;
//进度条的计时任务
@property(nonatomic,strong)PTActionModel *progressAction;
//抛物线的计时任务
@property(nonatomic,strong)PTActionModel *coinPostAction;



//底部总金额每列背景view 的顶部间距
@property (nonatomic,strong)NSArray<NSLayoutConstraint *> *totalNumBgViewList;
//动画是否正在进行中
@property (nonatomic,assign)BOOL isPlaying;
@end
@implementation CCCoinView
+ (id)getCoinView{
   return [[[NSBundle mainBundle] loadNibNamed:@"CCCoinView" owner:nil options:nil] lastObject];
}
- (instancetype)init{
    if (self = [super init]) {
        [self initSubUI];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self initSubUI];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initSubUI];
    }
    return self;
}
- (void)layoutSubviews{
    if (self.yanImgView.image == nil) {
//        NSMutableArray<UIImage *> *imgs = [NSMutableArray new];
//        for (int i = 0; i < 5; i++) {
//            NSString *name = [NSString stringWithFormat:@"img%d",i + 1];
//            [imgs addObject:[UIImage imageNamed:name]];
//        }
//        UIImage *img = [UIImage animatedImageWithImages:imgs duration:1];
//        self.yanImgView.image = img;
        
        NSString *fielPath = [[NSBundle mainBundle] pathForResource:@"yan_animation" ofType:@"gif"];
        NSData *data = [NSData dataWithContentsOfFile:fielPath];
        UIImage *img = [UIImage sd_animatedGIFWithData:data];
        self.yanImgView.image = img;
    }
}
- (void)initSubUI{

}



//- (void)setMaxCoin:(NSInteger)maxCoin {
//    self.maxCoinLbl.text = [NSString stringWithFormat:@"上限%ld",maxCoin];
//}
//- (void)setTotalCoin:(NSInteger)totalCoin {
//    _totalCoin = totalCoin;
//    self.config.totalCoinCount = (int)totalCoin;
//}
//- (void)setCurrentAnimCoinNum:(int)currentAnimCoinNum{
//    _currentAnimCoinNum = currentAnimCoinNum;
//    self.config.currentCoinCount = currentAnimCoinNum;
//
//    self.panCoinCountLabel.text = [NSString stringWithFormat:@"%ld",self.config.currentCoinCount + self.producedCoin];
//
//}

- (void)startWithConfig:(CCCoinConfig *)config collectCoin:(nonnull collectCoin)collectCoin{
    self.collectCoin = collectCoin;
    self.progressView.progress = 0;
    _config = config;
    
    [self setStartData];
}
- (void)reStart{
    if (self.isPlaying) return;
    [self setStartData];
}
- (void)pause{
    self.isPlaying = false;
    [[CCTimeHolder share] removeActionTargetModel:self];
    self.paopaoAction = nil;
    self.progressAction = nil;
    self.coinPostAction = nil;
}
//在开始时候设置初始化数据
- (void)setStartData{
    //初始化盘子中的金币
    self.maxCoinLbl.text = [NSString stringWithFormat:@"上限%ld",self.config.maxCoinCount];
    self.panCoinCountLabel.text = [NSString stringWithFormat:@"%d",self.config.currentCoinCount];
    for (int i = 0; i < self.panCoinList.count; i++) {
        UIView *panCoin = self.panCoinList[i];
        panCoin.hidden = self.config.currentCoinCount <= i;
    }
    [self resetTotalMoneyAnimation];

    
    if (self.config.leftMaxCoinCount == 0) {
        
        return;
    }
    self.isPlaying = true;

    
    
    if (self.paopaoAction == nil) {
        self.paopaoAction = [[CCTimeHolder share] addTarget:self action:@selector(showOneNewPaoPao) unitTime:((int)(60 * self.config.newPaoPaoTime))];
    }
    if (self.progressAction == nil) {
        self.progressAction = [[CCTimeHolder share] addTarget:self action:@selector(progressAnimation) unitTime:2];
    }
    if (self.coinPostAction == nil) {
        self.coinPostAction = [[CCTimeHolder share] addTarget:self action:@selector(coinPullTimeLoop) unitTime:([self getAddNewCoinProgressTime] / 20 * 60)];
    }
    
    
    
    [self progressAnimation];
}

#pragma mark - 采集按钮点击事件
- (IBAction)collectionBtnClick:(id)sender {
    [self startPullCoinAnimation];
}
#pragma mark -进度条动画
- (void)progressAnimation{
    if (self.isPlaying == NO) return;
    if (self.progressView.progress == 1) {
        [self createNewCoin];
        self.progressView.progress = 0;
    }
    CGFloat time = 1 / ([self getAddNewCoinProgressTime] * 30);
    self.progressView.progress += time;
    
}
#pragma mark - 气泡动画
- (void)showOneNewPaoPao{
    UIImageView *img = [self getNewPaoPaoView];
    CGFloat time = (img.frame.origin.y - (-self.config.pao_area)) / self.config.pao_pao_size * self.config.newPaoPaoTime;
    //(rand() % 2 == 1 ? 1 : -1)
    CGFloat ranX = (-1) * ((rand() % 1000 / 1000.0f) * self.config.pao_area + 50);
    CGRect originFrame = CGRectMake(ranX, (-self.config.pao_area), self.config.pao_pao_size, self.config.pao_pao_size);
    img.frame = originFrame;
    
    CGRect desFrame = CGRectMake(self.config.pao_start_x, -self.config.pao_satrt_y, self.config.pao_pao_size, self.config.pao_pao_size);
    [UIView animateWithDuration:time animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        img.frame = desFrame;
    } completion:^(BOOL finished) {
        [img removeFromSuperview];
    }];
}
#pragma mark - 金币抛物线
//生产出新的金币
- (void)createNewCoin{
    //添加金币图片
    UIImageView *imgView = [self getNewCoinView];
    
    //设置轨迹模型
    CGPoint desPoint = [self.config getDesPoint:self.config.currentCoinCount];
    PullCoinModel *model = [PullCoinModel new];
    model.startX = self.config.coin_start_x;
    model.startY = self.config.coin_start_y;
    model.currentStep = 0;
    model.totalStep = 10;
    model.desX = desPoint.x;
    model.desY = desPoint.y;
    model.minY = self.config.pull_coin_min_y;
    double sqrt = sqrtf((model.startY - model.minY) / (model.desY - model.minY));
    model.a = (float) ((model.startX + model.desX * sqrt) / (1 + sqrt));
    model.b = (float) ((model.startY - model.minY) / (pow(model.a - model.startX, 2)));
    model.pullImgView = imgView;

    //添加到动画队列中
    [self.pullCoinModelList addObject:model];
}
- (void)coinPullTimeLoop {
    for (NSInteger i = self.pullCoinModelList.count - 1; i >= 0; i--) {
        PullCoinModel *model = self.pullCoinModelList[i];
        UIImageView *img = model.pullImgView;
        
        if (model.currentStep == model.totalStep) {
            [img removeFromSuperview];
            [self.pullCoinModelList removeObjectAtIndex:i];
            
            NSLog(@"+++++++");
            self.config.currentCoinCount = self.config.currentCoinCount + 1;
            if (self.config.currentCoinCount > self.config.leftMaxCoinCount || self.config.currentCoinCount > self.config.maxCoinCount) {
                [self pause];
//                if (self.config.maxCoinCount == 5000) {
//                    SHOW(@"继续生产金币就领用能量机");
//                }
                return;
            }
            
            
            self.panCoinCountLabel.text = [NSString stringWithFormat:@"%ld",self.config.currentCoinCount];
            [self.panCoinCountLabel.superview layoutIfNeeded];
            if (self.config.currentCoinCount <= 7) {
                if(self.currentAnimCoinNum != 0){
                    UIImageView *view = self.panCoinList[self.currentAnimCoinNum - 1];
                    view.image = [UIImage imageNamed:@"one_coin_icon"];
                }
                UIImageView *view = self.panCoinList[self.config.currentCoinCount - 1];
                view.hidden = NO;
                self.currentAnimCoinNum = self.config.currentCoinCount;
                view.image = [self getCoinAnimImage];
            }else if(self.currentAnimCoinNum != 0){
                UIImageView *view = self.panCoinList[self.currentAnimCoinNum - 1];
                view.image = [UIImage imageNamed:@"one_coin_icon"];
            }
            
            //盘子里面的图片动画
            [self addNewCoinPanAnimation];
            
            //加一的文字动画
            [self addOneTextViewAnimation:self.addTextView];
        } else {
            model.currentStep++;
            //抛物线  y = b*pow(a-x,2) + coinMinY;
            
            double moveToX = (model.desX - model.startX) * model.currentStep / model.totalStep + model.startX;
            double moveToY = model.minY + model.b * pow((model.a - moveToX), 2);
            CGRect frame = img.frame;
            frame.origin.x = moveToX;
            frame.origin.y = moveToY;
            img.frame = frame;
        }
    }
}
#pragma mark - 金币采集下落到总金币动画
- (void)startPullCoinAnimation {
    if (self.config.currentCoinCount <= 0){
        return;
    }
//    if (!self.isPlaying) {
//        return;
//    }
    if (self.collectCoin) {
        self.collectCoin(self.config.currentCoinCount);
    }
    NSLog(@"下落--%d",self.config.currentCoinCount);
    self.config.totalCoinCount = self.config.totalCoinCount + self.config.currentCoinCount;
    self.config.currentCoinCount = 0;
    self.panCoinCountLabel.text = [NSString stringWithFormat:@"%d",self.config.currentCoinCount];
    [self reStart];

    
    
    //隐藏盘子中的金币
    for (int i = 0; i < self.panCoinList.count; i++) {
        UIView *panCoin = self.panCoinList[i];
        panCoin.hidden = YES;
    }
    
    //加载金币采集动画
    for (int i = 0; i < self.collectCoinAnimViewList.count; i++) {
        UIImageView *imgView = self.collectCoinAnimViewList[i];
        imgView.hidden = NO;
        CGRect frame = CGRectMake((i % 5) * 20, 0, 20, 20);
        imgView.frame = frame;
        frame.origin.y = self.config.collectCoinDownDistance;
        frame.origin.x = 40;
        [UIView animateWithDuration:((rand() % 6) /10.0 + 0.1) animations:^{
            imgView.frame = frame;
        } completion:^(BOOL finished) {
            imgView.hidden = YES;
        }];
    }
    
    
    //金币增长动画
    [self resetTotalMoneyAnimation];
}
#pragma mark -新增金币时，文字view动画
- (void)addOneTextViewAnimation:(UIView *)view{
    view.hidden = NO;
    self.rightTextTopGap.constant = TEXT_ORIGIN_TOP_GAP - 30;
    [UIView animateWithDuration:([self getAddNewCoinProgressTime] *0.8) animations:^{
        view.alpha = 0;
        [view.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        view.hidden = YES;
        view.alpha = 1;
        self.rightTextTopGap.constant = TEXT_ORIGIN_TOP_GAP;
    }];
}
#pragma mark - 金币落入盘子，金币弹跳动画
- (void)addNewCoinPanAnimation {
    //隐藏盘子中的金币
    for (int i = 0; i < self.panCoinList.count; i++) {
        if (self.config.currentCoinCount <= i) return;
        UIView *view = self.panCoinList[i];
        float moveY = (float) (-(random() % 10 + 5));
        CGRect originFrame = view.frame;
        CGRect jumpFrame = view.frame;
        jumpFrame.origin.y += moveY;
        [UIView animateWithDuration:10/60.0f animations:^{
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            view.frame = jumpFrame;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:10/60.0f animations:^{
                [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
                view.frame = originFrame;
            }];
        }];
    }
}
#pragma mark - 底部总金币view
- (void)resetTotalMoneyAnimation{
    int totalNum = self.config.totalCoinCount;
    for (int i = 0; i < self.totalNumBgViewList.count; i++) {
//        UIView *view = [self.totalCountBgView viewWithTag:9900 + i];
        NSLayoutConstraint *constraint = self.totalNumBgViewList[i];
        
        int num = totalNum % 10;
    
//        if (num <= 0 && totalNum <= 0 && i > 3) {
//            //千位以上 都为0
//            view.hidden = NO;
//            continue;
//        }else{
//           view.hidden = NO;
//        }
        
        constraint.constant = -num * TOTAL_ITEM_NUM_HEIGHT;
        totalNum /= 10;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        [self.totalCountBgView layoutIfNeeded];
    }];
}
- (CGFloat)getAddNewCoinProgressTime{
    return 60.0 / self.config.addCountOneMinute;
}
#pragma mark - 懒加载
//生成一个新的气泡view
- (UIImageView *)getNewPaoPaoView{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paopao"]];
    
    [self.topBgView addSubview:imgView];
    return imgView;
}
//生成一个新的抛物线的金币view
- (UIImageView *)getNewCoinView{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"one_coin_icon"]];
    imgView.frame = CGRectMake(self.config.coin_start_x, self.config.coin_start_y, self.config.coin_body_size, self.config.coin_body_size * 9 / 20);
    [self.topBgView addSubview:imgView];
    return imgView;
}
- (NSArray *)panCoinList{
    if (_panCoinList == nil) {
        NSMutableArray *arr = [NSMutableArray new];
        for (int i = 0; i < 7; i++) {
            UIImageView *imgView = [self viewWithTag:(2220 + i)];
            [arr addObject:imgView];
        }
        _panCoinList = arr;
    }
    return _panCoinList;
}
- (NSMutableArray *)pullCoinModelList{
    if (_pullCoinModelList == nil) {
        _pullCoinModelList = [NSMutableArray new];
    }
    return _pullCoinModelList;
}
- (NSArray *)collectCoinAnimViewList{
    if (_collectCoinAnimViewList == nil) {
        NSMutableArray *arr = [NSMutableArray new];
        //得钱的动画
        for (int i = 0; i < 20; i++) {
            UIImageView *imgView = [[UIImageView alloc] init];
            imgView.hidden = YES;
            [self.coinAnimBgView addSubview:imgView];
            imgView.image = [UIImage imageNamed:@"current_coin_icon"];
            [arr addObject:imgView];
        }
        _collectCoinAnimViewList = arr;
    }
    return _collectCoinAnimViewList;
}
- (NSArray<NSLayoutConstraint *> *)totalNumBgViewList{
    if (_totalNumBgViewList == nil) {
        NSMutableArray *arr = [NSMutableArray new];
        for (int i = 0; i < 8; i++) {
            UIView *view = [self.totalCountBgView viewWithTag:9900 + i];
            for (NSLayoutConstraint *constraint in view.constraints) {
                if (constraint.firstAttribute == NSLayoutAttributeTop) {
                    [arr addObject:constraint];
                    break;
                }
            }
        }
        _totalNumBgViewList = arr;
    }
    return _totalNumBgViewList;
}

- (UIImage *)getCoinAnimImage{
    if(coinAnimImg == nil){
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"one_coin_animation" ofType:@"gif"]];
        coinAnimImg = [UIImage sd_animatedGIFWithData:data];
    }
    return coinAnimImg;
}
@end

