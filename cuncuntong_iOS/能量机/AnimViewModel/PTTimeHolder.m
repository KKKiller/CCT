//
//

#import "CCTimeHolder.h"




@interface CCTimeHolder()
@property (nonatomic,retain)NSTimer *timer;
@property (nonatomic,retain)NSMutableArray<PTActionModel *> *actionModelList;
@end
@implementation CCTimeHolder
static CCTimeHolder *this;
+ (CCTimeHolder *)share{
    if(this == nil){
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            this = [CCTimeHolder new];
            
            //            [[NSRunLoop currentRunLoop] run]; //子线程中务必调用
            
            
        });
    }
    return this;
}
- (instancetype)init{
    if(this)return this;
    if(self = [super init]){
    }
    return self;
    
    
}
- (void)timeLoop{
    if(self.actionModelList.count == 0)return;
    static long long count = 0;
    
    for(NSInteger i = self.actionModelList.count - 1;i >= 0 ; i--){
        if(i >= self.actionModelList.count)continue;
        PTActionModel *model = self.actionModelList[i];
        if ([model.target respondsToSelector:model.action] && count % model.unitTime == 0) {
            switch (model.values.count) {
                case 0:{
                    [model.target performSelector:model.action];
                }
                    break;
                case 1:{
                    [model.target performSelector:model.action withObject:model.values[0]];
                }
                    break;
                case 2:{
                    [model.target performSelector:model.action withObject:model.values[0] withObject:model.values[1]];
                }
                    break;
                default:
                    break;
            }
        }
    }
    count ++;
}


- (PTActionModel *)addTarget:(id)target action:(SEL)action unitTime:(int)time{
    if (time <= 0) {
        return nil;
    }
    PTActionModel *model = [PTActionModel new];
    model.target = target;
    model.action  = action;
    model.unitTime = time;
    [self.actionModelList addObject:model];
    [self actionListCountChange];
    return model;
    
}
- (PTActionModel *)addTarget:(id)target action:(SEL)action values1:(nonnull id)obj1 unitTime:(int)time{
    if (time <= 0) {
        return nil;
    }
    PTActionModel *model = [PTActionModel new];
    model.target = target;
    model.action  = action;
    model.unitTime = time;
    model.values = @[obj1];
    [self.actionModelList addObject:model];
    [self actionListCountChange];
    return model;
}
- (PTActionModel *)addTarget:(id)target action:(SEL)action values1:(nonnull id)obj1 values2:(id)obj2 unitTime:(int)time{
    if (time <= 0) {
        return nil;
    }
    PTActionModel *model = [PTActionModel new];
    model.target = target;
    model.action  = action;
    model.unitTime = time;
    model.values = @[obj1,obj2];
    [self.actionModelList addObject:model];
    [self actionListCountChange];
    return model;
}
- (void)removeActionModel:(PTActionModel *)model{
    [self.actionModelList removeObject:model];
    [self actionListCountChange];
}
- (void)removeActionTargetModel:(id)target{
    for(NSInteger i = self.actionModelList.count - 1;i >= 0 ; i--){
        PTActionModel *model = self.actionModelList[i];
        if (model.target == target) {
            [self.actionModelList removeObject:model];
        }
    }
    [self actionListCountChange];
}
- (void)actionListCountChange{
    if (self.actionModelList.count == 0) {
        [self.timer invalidate];
        self.timer = nil;
    }else{
        [self timer];
    }
}
#pragma mark-懒加载
- (NSMutableArray<PTActionModel *> *)actionModelList{
    if(_actionModelList == nil){
        _actionModelList = [NSMutableArray new];
    }
    return _actionModelList;
}
- (NSTimer *)timer{
    if (_timer == nil) {
        _timer = [NSTimer timerWithTimeInterval:1.0/60 target:this selector:@selector(timeLoop) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return _timer;
}
@end


@implementation PTActionModel
@end
