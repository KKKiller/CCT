//
//  CCAnimationConfig.m
//  animation

//

#import "CCCoinConfig.h"

@implementation CCCoinConfig
- (instancetype)init{
    if (self = [super init]) {
        self.addCountOneMinute = 50;
        self.newPaoPaoTime = 0.8;
        self.pull_coin_min_y = -20;
        self.pao_pao_size = 25;
        self.pao_start_x = 85;
        self.pao_satrt_y = 0;
        self.pao_area = 300;
        
        self.coin_start_x = 137;
        self.coin_start_y = 0;
        self.coin_body_size = 30;
        self.collectCoinDownDistance = 110;
    }
    return self;
}

- (CGPoint)getDesPoint:(int)currentNum{
    CGPoint point;
    switch (currentNum) {
        case 0: {
            point = CGPointMake(165, 122);
        }
            break;
        case 1: {
           point = CGPointMake(135, 122);
        }
            break;
        case 2: {
            point = CGPointMake(105, 122);
        }
            break;
        case 3: {
            point = CGPointMake(150, 116);
        }
            break;
        case 4: {
            point = CGPointMake(116, 116);
        }
            break;
        case 5: {
            point = CGPointMake(107, 112);
        }
            break;
        default: {
            point = CGPointMake(160, 112);
        }
            break;
    }
    return point;
}
@end
