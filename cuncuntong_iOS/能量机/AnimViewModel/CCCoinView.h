//
//  CCCoinView.h
//  animation
//
//

#import <UIKit/UIKit.h>
#import "CCCoinConfig.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^collectCoin)(int gold);
@interface CCCoinView : UIView
@property (nonatomic,strong,readonly)CCCoinConfig *config;
@property (nonatomic,strong)collectCoin collectCoin;

//创建一个新的view控件
+ (id)getCoinView;

//开始动画
- (void)startWithConfig:(CCCoinConfig *)config collectCoin:(collectCoin)collectCoin;
- (void)reStart;
- (void)pause;
- (void)startPullCoinAnimation;

@property (nonatomic,assign)NSInteger maxCoin;
//@property (nonatomic,assign)NSInteger totalCoin;
//@property (nonatomic,assign)NSInteger producedCoin;

@end

NS_ASSUME_NONNULL_END
