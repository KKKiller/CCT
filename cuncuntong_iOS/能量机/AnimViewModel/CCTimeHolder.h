//
//

#import <Foundation/Foundation.h>
/**
 每个时间回调事件的model
 */
@interface PTActionModel :NSObject
@property (nonatomic,retain)id target;
@property (nonatomic,assign)SEL action;
@property (nonatomic,assign)int unitTime;//单位为1/60秒
@property (nonatomic,retain)NSArray *values;
@end



@interface CCTimeHolder : NSObject
+ (CCTimeHolder *)share;

/**
 添加计时回调方法
 @param time 1/60 秒的整倍数

 */
- (PTActionModel *)addTarget:(id)target action:(SEL)action unitTime:(int)time;
- (PTActionModel *)addTarget:(id)target action:(SEL)action values1:(nonnull id)obj1 unitTime:(int)time;
- (PTActionModel *)addTarget:(id)target action:(SEL)action values1:(nonnull id)obj1 values2:(id)obj2 unitTime:(int)time;
- (void)removeActionModel:(PTActionModel *)model;
- (void)removeActionTargetModel:(id)target;
@end
