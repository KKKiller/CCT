//
//  CCSaleEnergyView.h
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCSaleEnergyView : UIView
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UIButton *saleBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
+ (instancetype)instanceView ;

@end

NS_ASSUME_NONNULL_END
