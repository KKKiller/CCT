//
//  CCUpgradeCollectionViewCell.m
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCUpgradeCollectionViewCell.h"

@implementation CCUpgradeCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
        [self layoutUI];
    }
    return self;
}
- (void)setSelected:(BOOL)selected {
    if (selected) {
        [self.backBtn setBackgroundColor:MTRGB(0x8BE2D7)];
    }else{
        [self.backBtn setBackgroundColor:[UIColor whiteColor]];
    }
}
- (void)setupUI {
    self.backBtn = [[UIButton alloc]init];
    [self addSubview:self.backBtn];
    self.backBtn.layer.cornerRadius = 8;
    self.backBtn.layer.borderColor = MTRGB(0x8BE2D7).CGColor;
    self.backBtn.layer.borderWidth = 0.5;
    self.backBtn.userInteractionEnabled = NO;
    
    self.levelLbl = [[UILabel alloc]init];
    [self addSubview:self.levelLbl];
    self.levelLbl.font = [UIFont boldSystemFontOfSize:14];
    self.levelLbl.textAlignment = NSTextAlignmentCenter;
    
    self.descLbl = [[UILabel alloc]init];
    [self addSubview:self.descLbl];
    self.descLbl.font = FontSize(9);
    self.descLbl.text = @"40币/分钟";
    self.descLbl.textAlignment = NSTextAlignmentCenter;
    
}
- (void)layoutUI {
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.levelLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.mas_top).offset(4);
    }];
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self.mas_bottom).offset(-3);
    }];
}
@end
