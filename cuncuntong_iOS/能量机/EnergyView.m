//
//  EnergyView.m
//  CunCunTong
//
//  Created by TAL on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "EnergyView.h"

@implementation EnergyView

+ (instancetype)instanceView {
    EnergyView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    CGFloat bottomSpace = 80;
    CGFloat topSpace = 150;
    CGFloat barSpace = 95;
    if (IS_iPhone_5) {
        bottomSpace = 25;
        topSpace = 15;
        barSpace = 93;
    }else if (IS_iPhone_6){
        bottomSpace = 40;
        topSpace = 30;
        barSpace = 93;
    }else if (IS_iPhone6_Plus){
        bottomSpace = 53;
        topSpace = 80;
        barSpace = 97;
    }else if (IS_iPhoneXR){
        bottomSpace = 80;
        topSpace = 150;
        barSpace = 95;
    }else if (IS_iPhoneX){
        bottomSpace = 70;
        topSpace = 150;
        barSpace = 95;
    }
    view.bottomSpace.constant = bottomSpace;
    view.topSpace.constant = topSpace;
    view.barSpace.constant = barSpace;
    
    view.starArray = @[view.star1,view.star2,view.star3,view.star4,view.star5,view.star6,view.star7];
    return view;

}

- (void)setEnergyInfoModel:(CCEnergyInfoModel *)energyInfoModel {
    _energyInfoModel = energyInfoModel;
    [self.avatarView setImageWithURL:URL(energyInfoModel.portrait) placeholder:IMAGENAMED(@"")];
    if (energyInfoModel.actualLevel < 3) {
        if (energyInfoModel.actualLevel == 0) {
            self.levelLabel.text = @"0级";
        }else{
            self.levelLabel.text = [NSString  stringWithFormat:@"T%ld级",energyInfoModel.actualLevel];
        }

    }else{
        self.levelLabel.text = [NSString  stringWithFormat:@"%ld级",energyInfoModel.actualLevel - 2];
    }
    
    self.levelDescLbl.text = [NSString  stringWithFormat:@"%ld币/分钟",energyInfoModel.coinsOneMinute];
    for (int i = 0; i<self.starArray.count; i++) {
        UIButton *star = self.starArray[i];
        if (i+1 <= energyInfoModel.actualLevel - 2) {
            star.selected = YES;
        }else{
            star.selected = NO;
        }
    }
}
@end
