//
//  CCUpgradeEnergyView.m
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCUpgradeEnergyView.h"
#import "CCUpgradeCollectionViewCell.h"
@interface CCUpgradeEnergyView()<UICollectionViewDelegate,UICollectionViewDataSource>

@end
@implementation CCUpgradeEnergyView
+ (instancetype)instanceView {
    CCUpgradeEnergyView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    [view setCollectionView];
    
    [view.payBtn addTarget:view action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    return view;
}
- (void)payBtnClick {
    if ([self.delegate respondsToSelector:@selector(onSelectLevel:)]) {
        [self.delegate onSelectLevel:(int)self.selectedIndex + 1];
    }
}
- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;

    self.priceLbl.text = [NSString stringWithFormat:@"%ld元",[self getPriceOfLevel:selectedIndex]];
    self.originalSelectedIndex = selectedIndex;
    [self.collectionView reloadData];
}
- (void)setCollectionView{
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(60, 40);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//    layout.minimumLineSpacing = 5;
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(15, 190, self.size.width-30,140) collectionViewLayout:layout];
    self.collectionView = collectionView;
    [self addSubview:collectionView];
    
    [self.collectionView registerClass:[CCUpgradeCollectionViewCell class] forCellWithReuseIdentifier:@"upgradeCollectionViewCellId"];
//    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 9;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CCUpgradeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"upgradeCollectionViewCellId" forIndexPath:indexPath];
    NSString *title ;
    if (indexPath.row < 2) {
        title = [NSString stringWithFormat:@"T%ld级",indexPath.row+1];
    }else{
        title = [NSString stringWithFormat:@"%ld级",indexPath.row-1];
    }
    
    cell.levelLbl.text = title;
    NSString *desc;
    if (indexPath.row < 2) {
//        desc = @"15000币/天";
//    }else if (indexPath.row ==1) {
        desc = @"20币/分钟";

    }else{
        desc = [NSString stringWithFormat:@"%ld币/分钟",(indexPath.row - 2)*20 + 40];
    }
    cell.descLbl.text = desc;
    cell.selected = indexPath.row == self.selectedIndex;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    _selectedIndex = indexPath.row;
    [self.collectionView reloadData];
    self.priceLbl.text = [NSString stringWithFormat:@"%ld元",[self getPriceOfLevel:indexPath.row]];
    if (self.selectedIndex > self.originalSelectedIndex) {
        NSInteger addPrice = [self getPriceOfLevel:self.selectedIndex] - [self getPriceOfLevel:self.originalSelectedIndex];
        
        self.realPriceLbl.text = [NSString stringWithFormat:@"升级需补%ld元",addPrice];
        self.realPriceLbl.hidden = NO;
    }else{
        self.realPriceLbl.hidden = YES;
    }
}

- (NSInteger)getPriceOfLevel:(NSInteger)level {
    if (level < 2) {
        return 500 * (level + 1) ;
    }else{
        return 2000 * (level - 1);
    }
    return 0;
}
@end
