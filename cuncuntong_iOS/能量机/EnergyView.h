//
//  EnergyView.h
//  CunCunTong
//
//  Created by TAL on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCEnergyInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EnergyView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelDescLbl;
@property (weak, nonatomic) IBOutlet UIButton *star2;
@property (weak, nonatomic) IBOutlet UIButton *star1;
@property (weak, nonatomic) IBOutlet UIButton *star3;
@property (weak, nonatomic) IBOutlet UIButton *star4;
@property (weak, nonatomic) IBOutlet UIButton *star5;
@property (weak, nonatomic) IBOutlet UIButton *star6;
@property (weak, nonatomic) IBOutlet UIButton *star7;
//@property (weak, nonatomic) IBOutlet UIButton *star8;
//@property (weak, nonatomic) IBOutlet UIButton *star9;
//@property (weak, nonatomic) IBOutlet UIButton *star10;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *gonglue;
@property (weak, nonatomic) IBOutlet UIButton *shengji;
@property (weak, nonatomic) IBOutlet UIButton *cashOut;
@property (weak, nonatomic) IBOutlet UIButton *saleBtn;

@property (weak, nonatomic) IBOutlet UIButton *duihuanBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *barSpace;

+ (instancetype)instanceView;

@property (strong, nonatomic) CCEnergyInfoModel  *energyInfoModel;
@property (strong, nonatomic) NSArray  *starArray;

@end

NS_ASSUME_NONNULL_END
