//
//  CCEnergyController.h
//  CunCunTong
//
//  Created by TAL on 2019/11/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "MYUserInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CCEnergyController : BaseViewController
@property (nonatomic, strong) MYUserInfoModel *userInfo;
@property (nonatomic, assign) BOOL showShare;
@property (nonatomic, assign) BOOL fromJindou;

@end

NS_ASSUME_NONNULL_END
