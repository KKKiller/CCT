//
//  CCUpgradeEnergyView.h
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CCUpgradeEnergyViewDelegate <NSObject>

- (void)onSelectLevel:(int)level;

@end
@interface CCUpgradeEnergyView : UIView
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *realPriceLbl;

@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property(nonatomic, assign) NSInteger selectedIndex;
@property(nonatomic, assign) NSInteger originalSelectedIndex;

@property (weak, nonatomic) id<CCUpgradeEnergyViewDelegate> delegate;

+ (instancetype)instanceView ;
@end

NS_ASSUME_NONNULL_END
