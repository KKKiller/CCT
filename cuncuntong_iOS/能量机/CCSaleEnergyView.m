//
//  CCSaleEnergyView.m
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCSaleEnergyView.h"

@implementation CCSaleEnergyView
+ (instancetype)instanceView {
    CCSaleEnergyView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.saleBtn.layer.cornerRadius = 10;
    view.saleBtn.layer.masksToBounds = YES;
    
    view.cancelBtn.layer.cornerRadius = 10;
    view.cancelBtn.layer.masksToBounds = YES;
    view.cancelBtn.layer.borderWidth = 1;
    view.cancelBtn.layer.borderColor = MTRGB(0x8BE2D7).CGColor;
    
    return view;
}

@end
