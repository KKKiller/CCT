//
//  CCUpgradeCollectionViewCell.h
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCUpgradeCollectionViewCell : UICollectionViewCell
@property(nonatomic, strong) UIButton *backBtn;
@property(nonatomic, strong) UILabel *levelLbl;
@property(nonatomic, strong) UILabel *descLbl;
@property(nonatomic, assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
