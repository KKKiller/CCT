//
//  CCEnergyController.m
//  CunCunTong
//
//  Created by TAL on 2019/11/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCEnergyController.h"
#import "EnergyView.h"
#import "CCCoinView.h"
#import "UIImage+GIF.h"
#import "CCTimeHolder.h"
#import "CCUpgradeEnergyView.h"
#import "CCSaleEnergyView.h"
#import "CCShareView.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "CCEnergyInfoModel.h"
#import "MYHomeController.h"
@interface CCEnergyController()<CNContactPickerDelegate,MFMessageComposeViewControllerDelegate,CCUpgradeEnergyViewDelegate>
@property (nonatomic, strong) EnergyView *energyView;
@property (nonatomic,strong)CCCoinConfig *config;
@property (nonatomic,strong)CCCoinView *coinView;
@property (strong, nonatomic) PathDynamicModal *animateModel;//
@property (nonatomic, strong) CCUpgradeEnergyView *upgradeView;
@property (nonatomic, strong) CCSaleEnergyView *saleView;
@property (nonatomic, strong) CCShareView *shareView;
@property (nonatomic, strong) CCEnergyInfoModel *energyInfoModel;

@end
@implementation CCEnergyController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.showShare) {
        [self share];
    }
}
- (void)loadData {
    [[MyNetWorking sharedInstance]GetUrlNew:@"http://www.cct369.com/village/public/emachine/machineData" params:@{@"uid":USERID} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.energyInfoModel = [CCEnergyInfoModel modelWithJSON:success[@"data"]];
            
//            self.energyInfoModel.machine_level = 11;
//            self.energyInfoModel.my_energy_coin = 4996;
//            self.energyInfoModel.total_energy_coin_ceiling = 30;
//            self.energyInfoModel.total_produce_energy_coin = 4994;
            
            if (self.energyInfoModel.my_energy_coin == self.energyInfoModel.max_coin_today && self.energyInfoModel.machine_level == 0) {
                SHOW(@"继续生产金币就领用能量机");
            }
            
            self.energyView.energyInfoModel = self.energyInfoModel;
            self.upgradeView.selectedIndex = self.energyInfoModel.actualLevel - 1;
            self.config = [CCCoinConfig new];
            self.config.totalCoinCount = self.energyInfoModel.my_energy_coin;
            self.config.currentCoinCount = self.energyInfoModel.total_produce_energy_coin;
            self.config.maxCoinCount = self.energyInfoModel.total_energy_coin_ceiling;
            if (self.energyInfoModel.actualLevel < 2) {
                self.config.leftMaxCoinCount = self.energyInfoModel.max_coin_today - self.energyInfoModel.my_energy_coin;
            }else{
                self.config.leftMaxCoinCount = self.energyInfoModel.total_energy_coin_ceiling;
            }
            if (self.energyInfoModel.machine_level == 0) {
                [self.energyView.shengji setImage:IMAGENAMED(@"lingyong") forState:UIControlStateNormal];
            }
            self.config.addCountOneMinute = self.energyInfoModel.coinsOneMinute;
            WEAKSELF
            
            if (self.energyInfoModel.now_day_total_gold_number > self.energyInfoModel.max_coin_today) {
                [self.coinView pause];
            }else{
                [self.coinView startWithConfig:self.config collectCoin:^(int gold) {
                    [weakSelf collectCoin:gold];
                }];
            }
            
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)setupUI {
    self.energyView = [EnergyView instanceView];
    [self.view addSubview:self.energyView];
    self.energyView.frame = self.view.bounds;
    CGFloat top = 467;
    if (IS_iPhone_5) {
        top = 197;
    }else if (IS_iPhone_6){
        top = 277;
    }else if (IS_iPhone6_Plus){
        top = 333;
    }else if (IS_iPhoneXR){
        top = 467;
    }else if (IS_iPhoneX){
        top = 393;
    }
    
    self.coinView = [CCCoinView getCoinView];
    self.coinView.frame = CGRectMake(0, top, [UIScreen mainScreen].bounds.size.width, 254);
    [self.view addSubview:self.coinView];
    
    
    
    self.upgradeView = [CCUpgradeEnergyView instanceView];
    self.upgradeView.delegate = self;
    self.saleView = [CCSaleEnergyView instanceView];
    [self.view addSubview:self.shareView];
    
    [self.energyView.gonglue addTarget:self action:@selector(gonglue) forControlEvents:UIControlEventTouchUpInside];
    [self.energyView.shengji addTarget:self action:@selector(shengji) forControlEvents:UIControlEventTouchUpInside];
    [self.upgradeView.closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.saleView.closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.saleView.cancelBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.saleView.saleBtn addTarget:self action:@selector(sale) forControlEvents:UIControlEventTouchUpInside];
    [self.energyView.cashOut addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [self.energyView.duihuanBtn addTarget:self action:@selector(duihuan) forControlEvents:UIControlEventTouchUpInside];
    [self.energyView.saleBtn addTarget:self action:@selector(showSaleAlertView) forControlEvents:UIControlEventTouchUpInside];
    [self.energyView.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
}

- (void)gonglue {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.titleStr = @"能量机攻略";
    vc.urlStr = @"http://www.cct369.com/village/public/article?p=B8A6E4868C84ED5206506C2DB2F2CBE3&u=24";
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)shengji {
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:self.upgradeView inView:self.view];
}
- (void)close {
    [self.animateModel closeWithStraight];
}
//购买能量机
- (void)onSelectLevel:(int)level {
    NSString *url;
    NSString *key;
    //升级
    if ((level > self.energyInfoModel.machine_level || (self.energyInfoModel.machine_level > 10 && level < 10)) && self.energyInfoModel.machine_level != 0) {
        url = @"http://www.cct369.com/village/public/emachine/upgradeMachine";
        key = @"level";
    }else{
        //购买
        url = @"http://www.cct369.com/village/public/emachine/buyingMachines";
        key = @"machine_level";
    }
    if (level >= 3) {
        level = level - 2;
    }else{
        level = level + 10;
    }
    [[MyNetWorking sharedInstance]PostUrlNew:url params:@{key:@(level),@"uid":USERID} success:^(NSDictionary *success) {
        NSString *msg = success[@"msg"];
        SHOW(msg);
        if ([success[@"error"] integerValue] == 0) {
            [self loadData];
        }
        [self close];
    } failure:^(NSError *failure) {
        
    }];
}

- (void)showSaleAlertView {
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:self.saleView inView:self.view];
}
//采集金币
- (void)collectCoin:(int)gold {
    [[MyNetWorking sharedInstance]PostUrlNew:@"http://www.cct369.com/village/public/emachine/collectGold" params:@{@"uid":USERID} success:^(NSDictionary *success) {
        NSString *msg = success[@"msg"];
        SHOW(msg);
        [self loadData];
    } failure:^(NSError *failure) {
    }];
}
//出售
- (void)sale {
    [[MyNetWorking sharedInstance]GetUrlNew:@"http://www.cct369.com/village/public/emachine/sellingMachines" params:@{@"uid":USERID} success:^(NSDictionary *success) {
        NSString *msg = success[@"msg"];
        SHOW(msg);
        [self close];
        if ([success[@"error"] integerValue] == 0) {
            [self loadData];
        }
    } failure:^(NSError *failure) {
    }];
}
//兑换
- (void)duihuan {
    if (self.config.totalCoinCount == 0) {
        SHOW(@"金币为0，不能兑换");
        return;
    }
    [[MyNetWorking sharedInstance]PostUrlNew:@"http://www.cct369.com/village/public/emachine/exchangeEnergy" params:@{@"total_gold_number":@(self.config.totalCoinCount)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"兑换成功");
            self.config.totalCoinCount = 0;
            WEAKSELF
            [self.coinView startWithConfig:self.config collectCoin:^(int gold) {
                [weakSelf collectCoin:gold];
            }];
            if (self.fromJindou) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                MYHomeController *vc = [[MYHomeController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else{
            NSString *msg = success[@"msg"];
             SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}
#pragma mark - 分享
- (CCShareView *)shareView {
    if (!_shareView) {
        _shareView = [CCShareView instanceView];
        _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.pengyouquanBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareView;
}
//分享
- (void)share {
    NSLog(@"share");

    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height;
    }];
}
- (void)weixinBtnClick:(UIButton *)sender {
    [self shareCancelBtnClick];
    NSInteger tag = sender.tag;
    UMSocialPlatformType  platform = tag == 10 ? UMSocialPlatformType_WechatSession : UMSocialPlatformType_WechatTimeLine;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@""]]];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: @"领个能量机，日夜得收益"  descr:@"领用一个能量机，日夜不停给你挣金币，爽爆了！" thumImage:IMAGENAMED(@"能量机分享")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
    }];
}
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    WEAKSELF
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [weakSelf sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    // 设置短信内容
    NSString *shareText = [@"领用一个能量机，日夜不停给你挣金币，爽爆了！攻略：" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, @"http://www.cct369.com/village/public/article?p=B8A6E4868C84ED5206506C2DB2F2CBE3&u=24"];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
    
}


@end
