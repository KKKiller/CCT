//
//  MyNetWorking.m
//  zuniu_iOS
//
//  Created by 朱帅 on 16/8/16.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MyNetWorking.h"
#import "UIImage+LTTImage.h"

@implementation MyNetWorking

+ (MyNetWorking *)sharedInstance
{
    static MyNetWorking *httpApiBl = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        httpApiBl = [[MyNetWorking alloc] init];
    });
    return httpApiBl;
}

- (void)GetUrl:(NSString *)url
         params:(NSDictionary *)paramsDict
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure

{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:url parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        kLog(@"%@",url);
        kLog(@"%@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(error){
            failure(error);
        }
    }];
}

- (void)GetUrlNew:(NSString *)url
        params:(NSDictionary *)paramsDict
       success:(void (^)(NSDictionary *success))success
       failure:(void (^)(NSError *failure))failure

{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObjectsFromSet:[NSSet setWithObjects:@"text/html",@"text/plain", nil]];
    [manager GET:url parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        kLog(@"%@",url);
        kLog(@"%@",paramsDict);
        kLog(@"%@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(error){
            kLog(@"%@",error);
            failure(error);
        }
    }];
}

- (void)PostUrl:(NSString *)url
         params:(NSDictionary *)paramsDict
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    NSSet *types = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", nil]
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        kLog(@"%@",url);
        kLog(@"%@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        kLog(@"%@",error);
        if (error.code != 3840) {
            [MBProgressHUD showMessage:@"服务器响应失败,请稍后再试"];
        }
        failure(error);
    }];

}

- (void)PostUrlNew:(NSString *)url
         params:(NSDictionary *)paramsDict
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObjectsFromSet:[NSSet setWithObjects:@"text/html",@"text/plain", nil]];

    //    NSSet *types = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", nil]
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        kLog(@"%@",url);
        kLog(@"%@",paramsDict);
        kLog(@"%@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        kLog(@"%@",error);
        if (error.code != 3840) {
            [MBProgressHUD showMessage:@"服务器响应失败,请稍后再试"];
        }
        failure(error);
    }];
    
}

    

- (void)PostUrl:(NSString *)url
         params:(NSDictionary *)paramsDict
         target:(UIViewController *)target
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure

{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 25.0;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    if (target) {
        [TOOL showLoading:target.view];
    }
    
    kLog(@"URL---%@",url);
    kLog(@"参数----%@",paramsDict);
    [manager POST:url parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (target) {
            [TOOL hideLoading:target.view];
        }
        kLog(@"返回值---%@",responseObject);
        NSString *msg = responseObject[@"msg"];
        if ([responseObject[@"status"] isEqualToString:@"0000"]) {
            SHOW(msg);
            return;
        }
        if ([responseObject[@"status"] isEqualToString:@"1000"]) {
            SHOW(msg);
            return;
        }
        if ([responseObject[@"status"] isEqualToString:@"2000"]) {
//            SHOW(msg);
        }
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (target) {
            [TOOL hideLoading:target.view];
        }
        if (error.code != 3840) {
            [MBProgressHUD showMessage:@"服务器响应失败,请稍后再试"];
        }
        kLog(@"%@",error);
        failure(error);
    }];
}

- (void)PostUrl:(NSString *)url
         params:(NSDictionary *)paramsDict
         target:(UIViewController *)target
          imgaeData:(NSData *)imageData
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure

{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
     manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:url parameters:paramsDict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSString *avatar = [paramsDict valueForKey:@"avatar"];
        NSString *szj = [paramsDict valueForKey:@"s_zj"];
        NSString *name = avatar ? avatar : szj ? szj : [paramsDict containsObjectForKey:@"sp_ct"] ? @"sp_ct" : @"binary";
        if (paramsDict[@"name"]) {
            name = paramsDict[@"name"];
        }
        [formData appendPartWithFileData:imageData name:name fileName:@"image.png" mimeType:@"image/png"];
        NSLog(@"%@",paramsDict);
        NSLog(@"%@",url);
     } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
        kLog(@"URL---%@",url);
        NSLog(@"参数---%@",paramsDict);
        kLog(@"返回值---%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        kLog(@"%@",error);
    }];
}


//Get请求带token
- (void)GetUrlToken:(NSString *)url
            success:(void (^)(NSDictionary *success))success
            failure:(void (^)(NSError *failure))failure
{
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
     manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserManager sharedInstance].token] forHTTPHeaderField:@"Authorization"];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
        kLog(@"%@",url);
        kLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         failure(error);
    }];
    
    
}
//put方法带token
- (void)PutUrlToken:(NSString *)url
             params:(NSDictionary *)paramsDict
            success:(void (^)(NSDictionary *success))success
            failure:(void (^)(NSError *failure))failure
{
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
     manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserManager sharedInstance].token] forHTTPHeaderField:@"Authorization"];
    
    [manager PUT:url parameters:paramsDict success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
        kLog(@"%@",url);
        kLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         failure(error);
        
    }];
    
    
    
}
//post带token
- (void)PostUrlToken:(NSString *)url
              params:(NSDictionary *)paramsDict
             success:(void (^)(NSDictionary *success))success
             failure:(void (^)(NSError *failure))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
     manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserManager sharedInstance].token] forHTTPHeaderField:@"Authorization"];
    
    [manager POST:url parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
        kLog(@"%@",url);
        kLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         failure(error);
    }];
    
}
//这是delete方法 带token的
- (void)DeleteUrlToken:(NSString *)url
                params:(NSDictionary *)paramsDict
               success:(void (^)(NSDictionary *success))success
               failure:(void (^)(NSError *failure))failure
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserManager sharedInstance].token] forHTTPHeaderField:@"Authorization"];
    
    [manager DELETE:url parameters:paramsDict success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
        kLog(@"%@",url);
        kLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         failure(error);
    }];
    
}

//- (void)alert:(NSString *)title target:(UIViewController *)target {
//    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:1];
//    
//    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
//                                {
//                                }];
////    //取消栏
////    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
////    }];
//    [alertController addAction:chooseOne];
////    [alertController addAction:cancelOne];
//    [target presentViewController:alertController animated:YES completion:nil];
//}



- (void)LTTPostURL:(NSString *)url params:(NSDictionary *)params target:(UIViewController *)target success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    
    if (target) { [TOOL showLoading:target.view]; }
    
    
    [self PostUrl:url params:params success:^(NSDictionary *res) {
        [TOOL hideLoading:target.view];
        if (success) {
            success(res);
        }
        
    } failure:^(NSError *err) {
        [TOOL hideLoading:target.view];
        if (failure) {
            failure(err);
        }
        
    }];
    
    
}

- (void)LTTGetUrl:(NSString *)url params:(NSDictionary *)paramsDict target:(UIViewController *)target success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    if (target) { [TOOL showLoading:target.view]; }
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:url parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [TOOL hideLoading:target.view];
        kLog(@"%@",url);
        kLog(@"%@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [TOOL hideLoading:target.view];
        if(error){
            failure(error);
        }
    }];
    
}

//- (void)alert:(NSString *)title target:(UIViewController *)target {
//
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:1];
//
//    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
//                                {
//                                }];
////    //取消栏
////    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
////    }];
//    [alertController addAction:chooseOne];
////    [alertController addAction:cancelOne];
//    [target presentViewController:alertController animated:YES completion:nil];
//}

- (void)lttImage:(UIImage *)image success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    image = [image lj_fixOrientationImage];
    NSData *data =   [TOOL imageCompress: UIImagePNGRepresentation(image)];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    // ContentType设置
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"image/jpeg",@"image/png",@"application/octet-stream",@"text/json",nil];
    
    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
    
    [manager POST:LTTURL(@"uploadCoverPicture") parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        //把image  转为data , POST上传只能传data
        
        // 1) 取当前系统时间
        NSDate *date = [NSDate date];
        
        // 2) 使用日期格式化工具
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        
        // 3) 指定日期格式
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dateStr = [formatter stringFromDate:date];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", dateStr];
        
        [formData appendPartWithFileData:data name:@"prize_cover_picture" fileName:fileName mimeType:@"image/png"];
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSError *error = nil;
        
        NSDictionary *dic =  [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            if (failure) { failure(error);}
        }else{
            if (success) { success(dic);}
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) { failure(error);}
    }];
    
}


- (void)lttMoreImages:(NSArray<UIImage *> *)images URL:(NSString *)URL name:(NSString *)name ID:(NSString *)ID success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    // ContentType设置
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"image/jpeg",@"image/png",@"application/octet-stream",@"text/json",@"text/html",nil];
    
    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
    
    NSDictionary *parameter;
    
    if (ID != nil) {parameter = @{@"id":ID};}
    
    [manager POST:URL parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        //把image  转为data , POST上传只能传data
        
        // 1) 取当前系统时间
        NSDate *date = [NSDate date];
        
        // 2) 使用日期格式化工具
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        
        // 3) 指定日期格式
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dateStr = [formatter stringFromDate:date];
        
        
        for (int i = 0; i< images.count; i++) {
            NSString *fileName = [NSString stringWithFormat:@"%@%d.png", dateStr,i];
            UIImage *image = [images[i] lj_fixOrientationImage];
            NSData *data =   [TOOL imageCompress: UIImagePNGRepresentation(image)];
            
            
            [formData appendPartWithFileData:data name:name fileName:fileName mimeType:@"image/png"];
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        
        NSError *error = nil;
        
        NSDictionary *dic =  [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            if (failure) { failure(error);}
        }else{
            if (success) { success(dic);}
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) { failure(error);}
    }];
    
}

@end
