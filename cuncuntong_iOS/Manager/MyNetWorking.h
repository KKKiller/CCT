//
//  MyNetWorking.h
//  zuniu_iOS
//
//  Created by 朱帅 on 16/8/16.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "UserManager.h"

@interface MyNetWorking : NSObject

+(MyNetWorking *)sharedInstance;

- (void)GetUrl:(NSString *)url
        params:(NSDictionary *)paramsDict
       success:(void (^)(NSDictionary *success))success
       failure:(void (^)(NSError *failure))failure;

/**
 适配新的请求接口

 @param url
 @param paramsDict
 @param success
 @param failure
 */
- (void)GetUrlNew:(NSString *)url
        params:(NSDictionary *)paramsDict
       success:(void (^)(NSDictionary *success))success
       failure:(void (^)(NSError *failure))failure;

// 自动登录
- (void)PostUrl:(NSString *)url
         params:(NSDictionary *)paramsDict
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure;

- (void)PostUrl:(NSString *)url
         params:(NSDictionary *)paramsDict
         target:(UIViewController *)target
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure;

- (void)PostUrlToken:(NSString *)url
              params:(NSDictionary *)paramsDict
             success:(void (^)(NSDictionary *success))success
             failure:(void (^)(NSError *failure))failure;


- (void)GetUrlToken:(NSString *)url
            success:(void (^)(NSDictionary *success))success
            failure:(void (^)(NSError *failure))failure;


- (void)PutUrlToken:(NSString *)url
             params:(NSDictionary *)paramsDict
            success:(void (^)(NSDictionary *success))success
            failure:(void (^)(NSError *failure))failure;

- (void)DeleteUrlToken:(NSString *)url
                params:(NSDictionary *)paramsDict
               success:(void (^)(NSDictionary *success))success
               failure:(void (^)(NSError *failure))failure;

- (void)PostUrl:(NSString *)url
         params:(NSDictionary *)paramsDict
         target:(UIViewController *)target
          imgaeData:(NSData *)imageData
        success:(void (^)(NSDictionary *success))success
        failure:(void (^)(NSError *failure))failure;

/**
 新的post请求方式，创建群聊这些地方使用

 @param url
 @param paramsDict
 @param success
 @param failure
 */
- (void)PostUrlNew:(NSString *)url
         params:(NSDictionary *)paramsDict
        success:(void (^)(NSDictionary *success))success
           failure:(void (^)(NSError *failure))failure;

- (void)LTTPostURL:(NSString *)url params:(NSDictionary *)params target:(UIViewController *)target success:(void (^)(NSDictionary *success))success
           failure:(void (^)(NSError *failure))failure;

- (void)LTTGetUrl:(NSString *)url
           params:(NSDictionary *)paramsDict
           target:(UIViewController *)target
          success:(void (^)(NSDictionary *success))success
          failure:(void (^)(NSError *failure))failure;


- (void)lttImage:(UIImage *)image
         success:(void (^)(NSDictionary *success))success
         failure:(void (^)(NSError *failure))failure;

- (void)lttMoreImages:(NSArray<UIImage *>*)images
                  URL:(NSString *)URL
                 name:(NSString *)name
                   ID:(NSString *)ID
              success:(void (^)(NSDictionary *success))success
              failure:(void (^)(NSError *failure))failure;

@end
