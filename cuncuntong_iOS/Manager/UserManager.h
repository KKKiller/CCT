//
//  UserManager.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/22.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, TSRole) {
    TSRoleNormal = 0,
    TSRoleDriver,
    TSRoleStation
};
typedef NS_ENUM(NSUInteger, TSRoleReviewState) {
    TSRoleReviewStateing = 0,
    TSRoleReviewStatePass,
    TSRoleReviewStateRefuse
};
@interface LocationModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@end
@interface UserManager : NSObject

+(UserManager *)sharedInstance;

@property (nonatomic, copy) NSString *actpoint;
@property (nonatomic, copy) NSString *col;
@property (nonatomic, copy) NSString *comment;
@property (nonatomic, copy) NSString *userid;
@property (nonatomic, copy) NSString *isperfect;
@property (nonatomic, copy) NSString *isthird;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *locationText;
@property (nonatomic, strong) NSMutableArray<LocationModel*> *locs;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, copy) NSString *point;
@property (nonatomic, copy) NSString *portrait;
@property (nonatomic, copy) NSString *read;
@property (nonatomic, copy) NSString *realname;
@property (nonatomic, copy) NSString *redpoint;
@property (nonatomic, copy) NSString *secret;
@property (nonatomic, copy) NSString *syspoint;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, strong) NSArray *village;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, strong) NSString *credit;
@property (nonatomic, assign) CGFloat yajin;
@property (nonatomic, assign) BOOL is_smrz;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *inviter_id;
@property (nonatomic, assign) TSRole tsRole;
@property (nonatomic, assign) TSRoleReviewState tsReviewState;
@property (nonatomic, strong) NSString *tel;

@end

