//
//  UserManager.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/22.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

+ (UserManager *)sharedInstance {
    
    static UserManager *user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        user = [[UserManager alloc] init];
    });
    
    return user;
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"userid" : @"id"};
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"locs":[LocationModel class]};
}
@end
@implementation LocationModel
@end
