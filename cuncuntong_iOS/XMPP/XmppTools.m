//
//  XmppTools.m
//  ChatMPP
//
//  Created by zhouMR on 16/10/14.
//  Copyright © 2016年 luowei. All rights reserved.
//

#import "XmppTools.h"
#import "LBFriendRequestModel.h"
NSString *const  kReceiveMessageNotification = @"kReceiveMessageNotification";
NSString *const  kUpdateBadgeNumNotification = @"kUpdateBadgeNumNotification";
NSString *const  kFriendRequestNotification = @"kFriendRequestNotification";
NSString *const  kFriendChangeNotification = @"kFriendChangeNotification";


NSString *const  kTotalUnreadCount = @"totalUnreadCount";
NSString *const  kFirendRequest = @"firendRequest";

@implementation XmppTools

+ (instancetype)sharedManager{
    static XmppTools *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[XmppTools alloc]init];
    });
    return manager;
}

- (id)init{
    if (self = [super init]) {
        [self setupStream];
        _totalUnreadCount = ((NSNumber *)[self.unreadCache objectForKey:kTotalUnreadCount]).integerValue;
    }
    return self;
}

- (void)setupStream{
    _xmppStream = [[XMPPStream alloc] init];
    _xmppStream.enableBackgroundingOnSocket = YES;
    // 在多线程中运行，为了不阻塞UI线程，
    [_xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [_xmppStream setHostName:kXMPPHost];
    [_xmppStream setHostPort:kXMPPPort];
    
    _xmppAutoPing = [[XMPPAutoPing alloc] init];
    
    [_xmppAutoPing setPingInterval:1];
    [_xmppAutoPing setRespondsToQueries:YES];
    [_xmppAutoPing activate:_xmppStream];
    [_xmppAutoPing addDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
    
    
    // 2.autoReconnect 自动重连
    _xmppReconnect = [[XMPPReconnect alloc] init];
    [_xmppReconnect activate:_xmppStream];
    [_xmppReconnect setAutoReconnect:YES];
    [_xmppReconnect addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    // 3.好友模块 支持我们管理、同步、申请、删除好友
    _xmppRosterMemoryStorage = [[XMPPRosterMemoryStorage alloc] init];
    _xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:_xmppRosterMemoryStorage];
    [_xmppRoster activate:_xmppStream];
    
    //同时给_xmppRosterMemoryStorage 和 _xmppRoster都添加了代理
    [_xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    //设置好友同步策略,XMPP一旦连接成功，同步好友到本地
    [_xmppRoster setAutoFetchRoster:YES]; //自动同步，从服务器取出好友
    //关掉自动接收好友请求，默认开启自动同意
    [_xmppRoster setAutoAcceptKnownPresenceSubscriptionRequests:NO];
    
    
    //使用电子名片
    XMPPvCardCoreDataStorage *vCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
    _xmppvCardModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:vCardStorage];
    [_xmppvCardModule activate:_xmppStream];
    [_xmppvCardModule addDelegate:self delegateQueue:dispatch_get_main_queue()];
    //头像
    _xmppAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:_xmppvCardModule];
    [_xmppAvatarModule activate:_xmppStream];
    
    self.messageArchivingCoreDataStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    self.messageArchiving = [[XMPPMessageArchiving alloc]initWithMessageArchivingStorage:self.messageArchivingCoreDataStorage dispatchQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 9)];
    self.messageArchiving.clientSideMessageArchivingOnly = YES;
    [self.messageArchiving activate:self.xmppStream];
}

/**
 *  登录方法
 *  @prarm userName 用户名
 *  @prarm userPwd  密码
 */
- (void)loginWithUser:(XMPPJID*)jid withPwd:(NSString*)userPwd withSuccess:(SuccessBlock)sblock withFail:(FailureBlock)fblock{
    self.connectToServerPurpose = ConnectToServerPurposeLogin;
    self.successBlack = sblock;
    self.failureBlack = fblock;
    self.userPassword = userPwd;
    self.userJid = jid;
    [self connection];
}

/**
 * 注册方法
 *  @prarm userName 用户名
 *  @prarm userPwd  密码
 */
- (void)registerWithUser:(XMPPJID *)jid password:(NSString *)password withSuccess:(SuccessBlock)sblock withFail:(FailureBlock)fblock
{
    self.connectToServerPurpose = ConnectToServerPurposeRegister;
    self.successBlack = sblock;
    self.failureBlack = fblock;
    self.userPassword = password;
    self.userJid = jid;
    [self connection];
}

//连接服务器
- (void)connection{
    [self.xmppStream setMyJID:self.userJid];
    // 发送请求
    if ([self.xmppStream isConnected] || [self.xmppStream isConnecting]) {
        // 先发送下线状态
        XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
        [self.xmppStream sendElement:presence];
        
        // 断开连接
        [self.xmppStream disconnect];
    }
    NSError *error;
    [self.xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error];
}

#pragma mark - XMPPStreamDelegate

- (void)xmppStreamWillConnect:(XMPPStream *)sender {
    NSLog(@"%s--%d|正在连接",__func__,__LINE__);
}

- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
    // 连接成功之后，由客户端xmpp发送一个stream包给服务器，服务器监听来自客户端的stream包，并返回stream feature包
    NSLog(@"%s--%d|连接成功",__func__,__LINE__);
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    if(error && error.code == 7){
//        [self goOffLine];
    }
    NSLog(@"%s--%d|连接失败|%@",__func__,__LINE__,error);
    
}

/**
 *  xmpp连接成功之后走这里
 */
- (void)xmppStreamDidConnect:(XMPPStream *)sender {
    NSError *error;
    switch (self.connectToServerPurpose) {
        case ConnectToServerPurposeLogin:
            [self.xmppStream authenticateWithPassword:self.userPassword error:&error];
            break;
        case ConnectToServerPurposeRegister:
            [self.xmppStream registerWithPassword:self.userPassword error:&error];
            
        default:
            break;
    }
}

/**
 *  密码验证成功（即登录成功）
 */
- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    NSLog(@"xmpp授权成功。");
//    设置当前用户上线状态
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"available"];
    [self.xmppStream sendElement:presence];
    self.successBlack();
    
    XMPPvCardTemp * myvCardTemp = (XMPPvCardTemp*)[XMPPTOOL.vCardModule myvCardTemp];
    if (!myvCardTemp) {
        myvCardTemp = [XMPPvCardTemp vCardTemp];
        myvCardTemp.nickname = @"3113";
        myvCardTemp.photo = UIImagePNGRepresentation([UIImage imageNamed:@"share_alipay"]);
        [XMPPTOOL.xmppvCardModule updateMyvCardTemp:myvCardTemp];
    }
    
}

/**
 *  密码验证失败
 */
- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error {
    NSLog(@"️xmpp授权失败:%@", error.description);
    self.failureBlack(error.description);
}

/**
 *  注册成功
 */
- (void)xmppStreamDidRegister:(XMPPStream *)sender{
    self.successBlack();
}

/**
 *  注册失败
 */
- (void)xmppStream:(XMPPStream *)sender didNotRegister:(NSXMLElement *)error{
    self.failureBlack(error.description);
}

#pragma mark - Message
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kReceiveMessageNotification object:message];
    [self receiveMessage:message];
    [App_Delegate.tabbarVc updateBadgeValueForTabBarItem];
    if (App_Delegate.needLocalNoti) {
        NSString *body = message.body;
        if ([body containsString:@"YY_"]) {
            body = @"";
        }
        [App_Delegate sendLocalNotificationWithTitle:@"新消息" content:body];
    }
}

-(void)goOffLine{
    //生成网络状态
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    //改变通道状态
    [self.xmppStream sendElement:presence];
    //断开链接
    [self.xmppStream disconnect];
}

#pragma mark - XMPPReconnectDelegate
//重新失败时走该方法
- (void)xmppReconnect:(XMPPReconnect *)sender didDetectAccidentalDisconnect:(SCNetworkConnectionFlags)connectionFlags{
    NSLog(@"%s--%d|",__func__,__LINE__);
    SHOW(@"聊天服务器重连中...");
}

//接受自动重连
- (BOOL)xmppReconnect:(XMPPReconnect *)sender shouldAttemptAutoReconnect:(SCNetworkConnectionFlags)connectionFlags{
    NSLog(@"%s--%d|",__func__,__LINE__);
    SHOW(@"允许自动重连");
    return TRUE;
}




#pragma mark ===== 好友模块 委托=======
/** 收到出席订阅请求（代表对方想添加自己为好友) */
- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence
{
    [App_Delegate sendLocalNotificationWithTitle:@"好友请求" content:[NSString stringWithFormat:@"【%@】请求添加您为好友",[self getUidWithJid:presence.from.bareJID]]];
    [self saveFriendRequestWithUid:[self getUidWithJid:presence.from.bareJID]];
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    if ([presence.type isEqualToString:@"unsubscribe"]) {
        //收到对方取消定阅我得消息，从我的本地通讯录中将他移除
        [self.xmppRoster removeUser:presence.from];
    }else if ([presence.type isEqualToString:@"subscribe"]){
        
    }else if ([presence.type isEqualToString:@"subscribed"]){
        //从我的本地通讯录中将他添加
        [self.xmppRoster addUser:sender.remoteJID withNickname:nil];
        
    }
}


- (NSManagedObjectContext *)vCardContext   //电子名片模块
{
    XMPPvCardCoreDataStorage *storage = [XMPPvCardCoreDataStorage sharedInstance];
    return [storage mainThreadManagedObjectContext];
}


#pragma mark - 获取模块管理对象
- (XMPPvCardAvatarModule *)avatarModule    //头像模块
{
    return _xmppAvatarModule;
}

- (XMPPvCardTempModule *)vCardModule
{
    return _xmppvCardModule;
}

#pragma  mark --------------------自定义方法
- (XMPPJID*)getJIDWithUserId:(NSString *)userId{
    NSString *jidStr = [NSString stringWithFormat:@"%@%@",userId,kJidParam];
    return [XMPPJID jidWithString:jidStr];

}
- (NSString *)getUidWithJid:(XMPPJID *)jid {
    NSString *uid = @"";
    if ([jid.bare containsString:@"@"]) {
        NSRange range = [jid.bare rangeOfString:@"@"];
        uid = [jid.bare substringToIndex:range.location];
    }
    return uid;
}



#pragma mark - 未读数
- (YYCache *)unreadCache {
    if (!_unreadCache) {
        _unreadCache = [YYCache cacheWithName:@"ChatUnread"];
    }
    return _unreadCache;
}
- (NSInteger)getUnreadCountWithUid:(NSString *)uid {
    
    NSInteger count = [(NSNumber*)[self.unreadCache objectForKey:uid] integerValue];
    return count;
}
- (void)receiveMessage:(XMPPMessage *)message {
    if (![self.currentJid isEqualToJID:message.from]) {
        NSString *uid = [XMPPTOOL getUidWithJid:message.from];
        NSNumber *count = (NSNumber*)[self.unreadCache objectForKey:uid] ;
        [self.unreadCache setObject:[NSNumber numberWithInteger:count.integerValue + 1] forKey:[self getUidWithJid:message.from]];
        self.totalUnreadCount += 1;
        [self.unreadCache setObject:@(self.totalUnreadCount) forKey:kTotalUnreadCount];
        [NOTICENTER postNotificationName:kUpdateBadgeNumNotification object:nil];
    }
}
- (void)setCurrentJid:(XMPPJID *)currentJid {
    _currentJid = currentJid;
    NSNumber *count = (NSNumber*)[self.unreadCache objectForKey:[self getUidWithJid:self.currentJid]];
    [self.unreadCache setObject:@(0) forKey:[self getUidWithJid:currentJid]];
    self.totalUnreadCount -= count.integerValue;
    [self.unreadCache setObject:@(self.totalUnreadCount) forKey:kTotalUnreadCount];
}

#pragma mark - 添加好友
- (YYCache *)firendRequestCache {
    if (!_firendRequestCache) {
        _firendRequestCache = [YYCache cacheWithName:kFirendRequest];
    }
    return _firendRequestCache;
}
- (NSArray *)getFriendRequestArray {
    NSArray *array = [TOOL getCachaArrayWithName:kFirendRequest];
    return array;
}
- (void)saveFriendRequestWithUid:(NSString *)uid {
    NSArray *oldArray = [TOOL getCachaArrayWithName:kFirendRequest];
    for (NSDictionary *dict in oldArray) {
        LBFriendRequestModel *model = [LBFriendRequestModel modelWithJSON:dict];
        if ([model.uid isEqualToString:uid]) {
            return;
        }
    }
    LBFriendRequestModel *model = [[LBFriendRequestModel alloc]init];
    model.uid = uid;
    [[CCUserInfoManager shareInstance] loadUserInfoWithUid:uid loadFinishBlk:^(CCUserInfo * _Nonnull info) {
        model.nickname = info.realname;
        model.avatar = info.portrait;
        model.create_date = [NSDate date];
        model.is_accept = @"0";
        NSDictionary *modelDic = [model changeModelToDic];
        NSMutableArray *arrayM = [NSMutableArray arrayWithArray:oldArray];
        [arrayM insertObject:modelDic atIndex:0];
        [TOOL saveArrayWithData:arrayM Name:kFirendRequest];
        [NOTICENTER postNotificationName:kFriendRequestNotification object:nil];

    }];
}
- (NSArray *)removeFirendRequestWithUid:(NSString *)uid {
    NSArray *oldArray = [TOOL getCachaArrayWithName:kFirendRequest];
    NSMutableArray *arrayM = [NSMutableArray arrayWithArray:oldArray];
    for (NSDictionary *oldDic in oldArray) {
        LBFriendRequestModel *oldModel = [LBFriendRequestModel modelWithJSON:oldDic];
        if ([oldModel.uid isEqualToString:uid]) {
            [arrayM removeObject:oldDic];
            break;
        }
    }
    [TOOL saveArrayWithData:arrayM Name:kFirendRequest];
    return arrayM;
}
- (NSInteger)getFirendRequestNum {
    return [self getFriendRequestArray].count;
}
@end

