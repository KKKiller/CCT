//
//  CCPostChooseCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/5.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "CCPostChooseCell.h"

@implementation CCPostChooseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btn.layer.cornerRadius = 4;
    self.btn.layer.borderWidth = 0.5;
    [self.btn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
    [self.btn setTitleColor:MAINGREEN forState:UIControlStateSelected];
}
- (void)setModel:(TSTypeModel *)model {
    _model = model;
    [self.btn setTitle:model.type_name forState:UIControlStateNormal];
    if (model.type_name.length <= 4) {
        self.btn.titleLabel.font = FFont(16);
    }else{
        self.btn.titleLabel.font = FFont(12);
    }
}
@end
