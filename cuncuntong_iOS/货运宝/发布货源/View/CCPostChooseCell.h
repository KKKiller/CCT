//
//  CCPostChooseCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/5.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSTypeModel.h"
@interface CCPostChooseCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (nonatomic, strong) TSTypeModel *model;

@end
