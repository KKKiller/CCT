//
//  TSPostView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSPostView : UIView
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *endBtn;
@property (weak, nonatomic) IBOutlet UIButton *carTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *carLengthBtn;


@property (weak, nonatomic) IBOutlet UITextField *carHeavyField;

@property (weak, nonatomic) IBOutlet UITextField *carSizeField;

@property (weak, nonatomic) IBOutlet UIButton *type1Btn;
@property (weak, nonatomic) IBOutlet UIButton *type2Btn;
@property (weak, nonatomic) IBOutlet UIButton *type3Btn;
@property (weak, nonatomic) IBOutlet UIButton *type4Btn;
@property (weak, nonatomic) IBOutlet UIButton *type5Btn;
@property (weak, nonatomic) IBOutlet UIButton *type6Btn;

//@property (weak, nonatomic) IBOutlet UIButton *fullCarBtn;
//@property (weak, nonatomic) IBOutlet UIImageView *fullCarImgView;
//@property (weak, nonatomic) IBOutlet UIButton *lessCarBtn;
//@property (weak, nonatomic) IBOutlet UIImageView *lessCarImgView;
@property (weak, nonatomic) IBOutlet UITextField *beizhuField;
@property (weak, nonatomic) IBOutlet UIButton *postBtn;
@property (nonatomic, assign) BOOL isFullCar;
@property (nonatomic, strong) NSString *downloadTypeStr;

+ (instancetype)instanceView;
- (void)clearUIAfterPost;
@end
