//
//  CCPostChooseView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/4.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSTypeModel.h"

@interface CCPostChooseView : UIView
@property (nonatomic, strong) UILabel *topLbl;
@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *sureBtn;
@property (nonatomic, strong) NSArray<TSTypeModel *> *dataArray;
@property (nonatomic, strong) NSMutableArray<TSTypeModel*> *selectedArray;


@end
