//
//  CCPostChooseView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/4.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "CCPostChooseView.h"
#import "CCPostChooseCell.h"
@interface CCPostChooseView()<UICollectionViewDelegate,UICollectionViewDataSource>
@end
@implementation CCPostChooseView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
        [self layoutUI];
        self.backgroundColor = WHITECOLOR;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.selectedArray = [NSMutableArray array];
    }
    return self;
}
- (void)setupUI{
    self.topLbl = [[UILabel alloc]initWithText:@"选择类型（可多选）" font:16 textColor:TEXTBLACK6];
    self.topLbl.textAlignment = NSTextAlignmentCenter;
    
    self.topLine = [[UIView alloc]init];
    [self addSubview:self.topLine];
    self.topLine.backgroundColor = BACKGRAY;
    
    [self addSubview:self.topLbl];
    [self addSubview:self.collectionView];
    
    self.cancelBtn = [[UIButton alloc]initWithTitle:@"取消" textColor:TEXTBLACK6 backImg:nil font:14];
    self.cancelBtn.layer.cornerRadius = 4;
    self.cancelBtn.layer.masksToBounds = YES;
    self.cancelBtn.layer.borderColor = TEXTBLACK9.CGColor;
    self.cancelBtn.layer.borderWidth = 0.5;
    [self addSubview:self.cancelBtn];
    
    self.sureBtn = [[UIButton alloc]initWithTitle:@"确定" textColor:WHITECOLOR backImg:nil font:14];
    [self.sureBtn setBackgroundColor:MAINGREEN];
    self.sureBtn.layer.cornerRadius = 4;
    self.sureBtn.layer.masksToBounds = YES;
    [self addSubview:self.sureBtn];
}
- (void)layoutUI {
    [self.topLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.topLbl.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.top.equalTo(self.topLine.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-10);
        make.bottom.equalTo(self.mas_bottom).offset(-70);
    }];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.height.mas_equalTo(34);
        make.right.equalTo(self.mas_centerX).offset(-10);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
    }];
    [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(self.cancelBtn);
        make.left.equalTo(self.cancelBtn.mas_right).offset(20);
        make.top.equalTo(self.cancelBtn);
    }];
}
- (void)setDataArray:(NSArray<TSTypeModel *> *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CCPostChooseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CCPostChooseCell" forIndexPath:indexPath];
    TSTypeModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CCPostChooseCell *cell =(CCPostChooseCell *) [collectionView cellForItemAtIndexPath:indexPath];
    cell.btn.selected = !cell.btn.isSelected;
    TSTypeModel *model = self.dataArray[indexPath.row];
    if(cell.btn.isSelected){
        model.isSelected = YES;
        cell.btn.layer.borderColor = MAINGREEN.CGColor;
        [self.selectedArray addObject:model];
    }else{
        model.isSelected = NO;
        cell.btn.layer.borderColor = TEXTBLACK6.CGColor;
        [self.selectedArray removeObject:model];
    }
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(70, 45);
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame: CGRectMake(10, 10, self.width - 20, self.height - 70) collectionViewLayout:layout];
        collectionView.backgroundColor = WHITECOLOR;
        self.collectionView = collectionView;
//        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        [self.collectionView registerNib:[UINib nibWithNibName:@"CCPostChooseCell" bundle:nil] forCellWithReuseIdentifier:@"CCPostChooseCell"];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
    }
    return _collectionView;
}
@end
