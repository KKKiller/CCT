//
//  TSPayView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSPayView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UIButton *chargeBtn;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@end
