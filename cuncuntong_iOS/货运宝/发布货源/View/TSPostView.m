//
//  TSPostView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSPostView.h"

@implementation TSPostView

+ (instancetype)instanceView {
    TSPostView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    [view setTypeBtn];
    return view;
}
- (void)setTypeBtn {
    self.isFullCar = YES;
    self.downloadTypeStr = @"一装一卸";
    NSArray *btns = @[self.type1Btn,self.type2Btn,self.type3Btn,self.type4Btn,self.type5Btn,self.type6Btn];
    for (UIButton *btn  in btns) {
        btn.layer.borderColor = MAINGREEN.CGColor;
        btn.layer.borderWidth = 0.0;
        btn.layer.cornerRadius = 4;
        btn.layer.masksToBounds = YES;
        [btn setImage:IMAGENAMED(@"btn_unchecked") forState:UIControlStateNormal];
        [btn setImage:IMAGENAMED(@"btn_checked") forState:UIControlStateSelected];
        
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0);
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
       
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    self.type1Btn.selected = YES;
    self.type1Btn.layer.borderWidth = 0.5;
    self.type3Btn.selected = YES;
    self.type3Btn.layer.borderWidth = 0.5;

}

- (void)btnClick:(UIButton *)sender {
    if (sender == self.type1Btn) {
        self.type1Btn.selected = YES;
        self.type2Btn.selected = NO;
        self.type1Btn.layer.borderWidth = 0.5;
        self.type2Btn.layer.borderWidth = 0.0;
        self.isFullCar = YES;
    }else if (sender == self.type2Btn){
        self.type2Btn.selected = YES;
        self.type1Btn.selected = NO;
        self.type2Btn.layer.borderWidth = 0.5;
        self.type1Btn.layer.borderWidth = 0.0;
        self.isFullCar = NO;
    }else{
        NSArray *btns = @[self.type3Btn,self.type4Btn,self.type5Btn,self.type6Btn];
        for (UIButton *btn in btns) {
            if (![btn isEqual:sender]) {
                btn.selected = NO;
                btn.layer.borderWidth = 0.0;
            }else{
                btn.selected = YES;
                btn.layer.borderWidth = 0.5;
                self.downloadTypeStr = btn.titleLabel.text;
            }
        }
    }
}

- (void)clearUIAfterPost {
    [self.startBtn setTitle:@"出发地" forState:UIControlStateNormal];
    [self.endBtn setTitle:@"目的地" forState:UIControlStateNormal];
    [self.carTypeBtn setTitle:@"需要的 车型" forState:UIControlStateNormal];
    [self.carLengthBtn setTitle:@"请选择需要的车长" forState:UIControlStateNormal];
    
    [self.startBtn setTitleColor:MTRGB(0xD0D0D0) forState:UIControlStateNormal];
    [self.endBtn setTitleColor:MTRGB(0xD0D0D0) forState:UIControlStateNormal];
    [self.carTypeBtn setTitleColor:MTRGB(0xD0D0D0) forState:UIControlStateNormal];
    [self.carLengthBtn setTitleColor:MTRGB(0xD0D0D0) forState:UIControlStateNormal];

    
    self.carHeavyField.text = nil;
    self.carSizeField.text = nil;
    self.beizhuField.text = nil;
    [self btnClick:self.type3Btn];
    [self btnClick:self.type1Btn];
}
@end
