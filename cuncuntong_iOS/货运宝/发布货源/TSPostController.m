//
//  TSPostController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSPostController.h"
#import "TSPostView.h"
#import "BRStringPickerView.h"
#import "TSTypeModel.h"
#import "CCPositionModel.h"
#import "SetHomeViewController.h"
#import "SystemNewsController.h"
#import "BRAddressPickerView.h"
#import "CCPostChooseView.h"
#import "BRAddressModel.h"
@interface TSPostController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) TSPostView *postView;
@property (nonatomic, assign) BOOL currentChooseEnd;
@property (nonatomic, assign) CLLocationCoordinate2D locationStart;
@property (nonatomic, assign) CLLocationCoordinate2D locationEnd;
@property (nonatomic, assign) BOOL isFullCar;
@property (nonatomic, strong) CCPositionModel *startPositionModel;
@property (nonatomic, strong) CCPositionModel *endPositionModel;
@property (nonatomic, strong) NSString *startName;
@property (nonatomic, strong) NSString *endName;
@property (nonatomic, strong) CCPostChooseView *chooseView;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) CCPostChooseView *chooseTypeView;
@property (nonatomic, strong) NSArray *carTypeArray;
@property (nonatomic, strong) NSArray *carLengthArray;


@end

@implementation TSPostController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isFullCar = YES;
    self.title = @"发布货源";

    [NOTICENTER addObserver:self selector:@selector(chooseAddressFinished:) name:@"ChooseLocationNotification" object:nil];
    [self setupUI];
    [self setRightBtn];
    self.carTypeArray = [NSMutableArray array];
    self.carLengthArray = [NSMutableArray array];
}
- (void)setRightBtn {
    if (![App_Delegate.defaultHome isEqualToString:@"default"]) {
        UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [rightBtn setTitle:@"消息" forState:UIControlStateNormal];
        rightBtn.titleLabel.font = FFont(14);
        [rightBtn addTarget:self action:@selector(message) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
        self.navigationItem.rightBarButtonItem = right;
    }
}
- (void)message {
    SystemNewsController *vc = [[SystemNewsController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)post  {
    if(![self emptyCheck])return;
    NSDictionary *dict = @{@"r_id":USERID,
                           @"location_province":STR(self.startPositionModel.provinceId),
                           @"location_city":STR(self.startPositionModel.cityId),
                           @"location_district":STR(self.startPositionModel.districtId),
                           @"location_address":STR(self.startName),
                           @"location_lat":[NSString stringWithFormat:@"%f",self.locationStart.latitude],
                           @"location_lon":[NSString stringWithFormat:@"%f",self.locationStart.longitude],
                           @"destination_lat":[NSString stringWithFormat:@"%f",self.locationEnd.latitude],
                           @"destination_lon":[NSString stringWithFormat:@"%f",self.locationEnd.longitude],
                           @"destination_province":STR(self.endPositionModel.provinceId),
                           @"destination_city":STR(self.endPositionModel.cityId),
                           @"destination_district":STR(self.endPositionModel.districtId),
                           @"address_to":STR(self.endName),
                           @"car_type1":[self getIdWithArray:self.carTypeArray],
                           @"car_type2":self.postView.carHeavyField.text,
                           @"car_type3":[self getIdWithArray:self.carLengthArray],
                           @"car_type4":self.postView.downloadTypeStr ?: @"",
                           @"size":[NSString stringWithFormat:@"%@*%@",self.postView.carHeavyField.text,self.postView.carSizeField.text],
                           @"else":self.postView.beizhuField.text,
                           @"car_space":self.postView.isFullCar ? @"2":@"1",//1：零担 2：满车
                               };
    NSMutableDictionary *param  = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"owner/add_info") params:param target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            SHOW(@"发布成功");
            [App_Delegate.transportTabbarVc setSelectIndex:2];
            [App_Delegate.transportTabbarVc.orderVc refreshData];
            [App_Delegate.transportTabbarVc.orderVc gotoIndex:0];
            [self.postView clearUIAfterPost];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}

- (void)setupUI {
    CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
    CGFloat yy = system >= 11.0  || !App_Delegate.isDefaultRoot ? 64 : 0;
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, yy, App_Width, App_Height  - 49 - yy)];
    self.postView = [TSPostView instanceView];
    self.postView.frame = CGRectMake(0, 5, App_Width, 570);
    [self.scrollView addSubview:self.postView];
    self.scrollView.contentSize = CGSizeMake(App_Width, self.postView.height + 6);
    [self.view addSubview:self.scrollView];
    //    self.scrollView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    
    [self.postView.startBtn addTarget:self action:@selector(startBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.postView.endBtn addTarget:self
                             action:@selector(endBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.postView.postBtn addTarget:self action:@selector(post) forControlEvents:UIControlEventTouchUpInside];
    [self.postView.carTypeBtn addTarget:self action:@selector(carTypeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.postView.carLengthBtn addTarget:self action:@selector(carLengthBtnClick) forControlEvents:UIControlEventTouchUpInside];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [App_Delegate.transportTabbarVc updateRightTitle];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);

}

- (void)showChooseViewWithType:(NSInteger)type {
    self.chooseTypeView = [[CCPostChooseView alloc]initWithFrame:CGRectMake(0, 0, App_Width - 50, App_Width - 40)];
    if (type == 0) {
        self.chooseTypeView.dataArray = App_Delegate.transportTabbarVc.carTypeArray;
    }else{
        self.chooseTypeView.dataArray = App_Delegate.transportTabbarVc.carLengthArray;
    }
    [self.chooseTypeView.cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.chooseTypeView.sureBtn.tag = type;
    [self.chooseTypeView.sureBtn addTarget:self action:@selector(sureBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:self.chooseTypeView inView:App_Delegate.window];
}
- (void)cancelBtnClick {
    [self.animateModel closeWithStraight];
}
- (void)sureBtnClick:(UIButton *)sender{
    if (sender.tag == 0) {//车型
        self.carTypeArray = self.chooseTypeView.selectedArray;
        [self.postView.carTypeBtn setTitle:[self getTextWithArray:self.carTypeArray] forState:UIControlStateNormal];
        [self.postView.carTypeBtn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
    }else{//车长
        self.carLengthArray = self.chooseTypeView.selectedArray;
        [self.postView.carLengthBtn setTitle:[self getTextWithArray:self.carLengthArray] forState:UIControlStateNormal];
        [self.postView.carLengthBtn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
    }
    [self.animateModel closeWithStraight];
}
- (NSString *)getDownloadIdWithDownloadText {
    NSString *idStr = @"";
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.downloadTypeArray) {
        if ([model.type_name isEqualToString:self.postView.downloadTypeStr]) {
            idStr = model.id;
            break;
        }
    }
    return idStr;
}
- (NSString *)getTextWithArray:(NSArray *)array {
    NSString *text=  @"";
    for (TSTypeModel *model in array) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@,",model.type_name]];
    }
    if (text.length > 1) {
        text = [text substringToIndex:text.length -1];
    }
    return text;
}
- (NSString *)getIdWithArray:(NSArray *)array {
    NSString *text = @"";
    for (TSTypeModel *model in array) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@,",model.id]];
    }
    if (text.length > 1) {
        text = [text substringToIndex:text.length -1];
    }
    return text;
}
- (void)carTypeBtnClick {
    [self showChooseViewWithType:0];
}
- (void)carLengthBtnClick {
    [self showChooseViewWithType:1];
}

//起点
- (void)startBtnClick {
    [BRAddressPickerView showAddressPickerWithDefaultSelected:nil isAutoSelect:NO resultBlock:^(NSArray *selectAddressArr) {
        BRProvinceModel *province = selectAddressArr[0];
        BRProvinceModel *city = selectAddressArr[1];
        BRProvinceModel *town = selectAddressArr[2];

        self.startPositionModel = [[CCPositionModel alloc]init];
        self.startPositionModel.provinceName = province.name;
        self.startPositionModel.provinceId = province.id;
        self.startPositionModel.cityName = city.name;
        self.startPositionModel.cityId = city.id;
        self.startPositionModel.districtName = town.name;
        self.startPositionModel.districtId = town.id;
        
        [self pushToMapView:NO cityName:self.startPositionModel.cityName];
    }];
}
//终点
- (void)endBtnClick {
    [BRAddressPickerView showAddressPickerWithDefaultSelected:nil isAutoSelect:NO resultBlock:^(NSArray *selectAddressArr) {
        BRProvinceModel *province = selectAddressArr[0];
        BRProvinceModel *city = selectAddressArr[1];
        BRProvinceModel *town = selectAddressArr[2];
        
        self.endPositionModel = [[CCPositionModel alloc]init];
        self.endPositionModel.provinceName = province.name;
        self.endPositionModel.provinceId = province.id;
        self.endPositionModel.cityName = city.name;
        self.endPositionModel.cityId = city.id;
        self.endPositionModel.districtName = town.name;
        self.endPositionModel.districtId = town.id;
        
        [self pushToMapView:YES cityName:self.endPositionModel.cityName];
    }];
}
- (void)pushToMapView:(BOOL)isEnd cityName:(NSString *)cityName{
    NSString *url = [NSString stringWithFormat:@"?=%@&=json&=",cityName];
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]GetUrl:@"http://api.map.baidu.com/geocoder" params:@{@"key":@"Og6DKLflNeib9DqK72Qr2sqTMvyvtctI",@"output":@"json",@"address":STR(cityName)} success:^(NSDictionary *success) {
        if ([success[@"status"]isEqualToString:@"OK"] && [success[@"result"] isKindOfClass:[NSDictionary class]]) {
            NSString *lat = [NSString stringWithFormat:@"%@", success[@"result"][@"location"][@"lat"]];
            NSString *lng = [NSString stringWithFormat:@"%@", success[@"result"][@"location"][@"lng"]];
            if (!STRINGEMPTY(lat) && !STRINGEMPTY(lng)) {
                [Def setObject:lat forKey:@"center_lat"];
                [Def setObject:lng forKey:@"center_lng"];
            }
        }else{
            [Def removeObjectForKey:@"center_lat"];
            [Def removeObjectForKey:@"center_lng"];
        }
        [TOOL hideLoading:self.view];
        TSShowMapViewController *vc = [[UIStoryboard storyboardWithName:@"TSShowMapViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"TSShowMapViewController"];
        self.currentChooseEnd = isEnd;
        [self presentViewController:vc animated:YES completion:nil];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
    });
}

- (void)chooseAddressFinished:(NSNotification *)noti {
    NSDictionary *dict = noti.userInfo;
    NSString *city = dict[@"name"];
    NSString *lat = dict[@"lat"];
    NSString *lng = dict[@"lng"];
   
    if(self.currentChooseEnd){
        self.locationEnd = CLLocationCoordinate2DMake([lat doubleValue], [lng doubleValue]);
        [self.postView.endBtn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
        NSString *title;
        if ([city containsString:self.endPositionModel.cityName] && ![self.endPositionModel.districtName isEqualToString:@"不限"]) {
            title = [NSString stringWithFormat:@"%@ %@",self.endPositionModel.cityName,self.endPositionModel.districtName];
        }else{
            title = city;
        }
        self.endName = city;
        [self.postView.endBtn setTitle:title forState:UIControlStateNormal];
    }else{
        self.locationStart = CLLocationCoordinate2DMake([lat doubleValue], [lng doubleValue]);
        [self.postView.startBtn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
        NSString *title;
        if ([city containsString:self.startPositionModel.cityName] && ![self.startPositionModel.districtName isEqualToString:@"不限"]) {
            title = [NSString stringWithFormat:@"%@ %@",self.startPositionModel.cityName,self.startPositionModel.districtName];
        }else{
            title = city;
        }
        self.startName = city;
        [self.postView.startBtn setTitle:title forState:UIControlStateNormal];
    }
}

- (BOOL)emptyCheck {
    if ([self.postView.startBtn.titleLabel.text isEqualToString:@"出发地"]) {
        SHOW(@"请选择出发地");
        return NO;
    }
    if ([self.postView.endBtn.titleLabel.text isEqualToString:@"目的地"]) {
        SHOW(@"请选择目的地");
        return NO;
    }
    if (self.locationStart.latitude == 0 ) {
        SHOW(@"出发地址定位出错，请重新选择");
        return NO;
    }
    if (self.locationEnd.latitude == 0) {
        SHOW(@"目的地址定位出错，请重新选择");
        return NO;
    }
    if ([self.postView.carTypeBtn.titleLabel.text isEqualToString:@"需要的 车型"]) {
        SHOW(@"请选择车型");
        return NO;
    }
    if ([self.postView.carLengthBtn.titleLabel.text isEqualToString:@"请输入需要的车长"]) {
        SHOW(@"请选择车长");
        return NO;
    }
    if (!([self.postView.carHeavyField.text integerValue] > 0 && [self.postView.carHeavyField.text integerValue] <= 1000)) {
        SHOW(@"车重输入不符合要求");
        return NO;
    }
    if (self.postView.carHeavyField.text.length == 0) {
        SHOW(@"请输入重量");
        return NO;
    }
    return YES;
}
- (NSString *)carType {
    NSString *carType = @"" ;
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carTypeArray) {
        if ([self.postView.carTypeBtn.titleLabel.text isEqualToString:model.type_name]) {
            carType = model.id;
            break;
        }
    }
    return carType;
}
- (NSString *)carLength {
    NSString *carLength = @"" ;
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carLengthArray) {
        if ([self.postView.carLengthBtn.titleLabel.text isEqualToString:model.type_name]) {
            carLength = model.id;
            break;
        }
    }
    return carLength;
}

@end
