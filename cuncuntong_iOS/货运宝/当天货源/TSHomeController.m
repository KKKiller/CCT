//
//  TSHomeController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSHomeController.h"
#import "TSHomeHeaderView.h"
#import "TSHomeCell.h"
#import "JFLocation.h"
#import "JFAreaDataManager.h"
#import "JFCityViewController.h"
#import "SetHomeViewController.h"
#import "TSOrderInfo.h"
#import "TSVerifyHomeController.h"
#import "RechargeViewController.h"
#import "TSNavigationer.h"
#import "CCShareView.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "SystemNewsController.h"
static NSString *cellID = @"TSHomeCell";

@interface TSHomeController ()<UITableViewDataSource,UITableViewDelegate,JFLocationDelegate, JFCityViewControllerDelegate,UISearchBarDelegate,UIScrollViewDelegate,CNContactPickerDelegate,MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) TSHomeHeaderView *header;
@property (nonatomic, strong) CCShareView *shareView;
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
//@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) CCPositionModel *positionModel;
@property (nonatomic, strong) TSOrderInfo *selectedModel;
@property (nonatomic, assign) BOOL isSelectedSameCityOrder;
@property (nonatomic, assign) BOOL showNoOrderAlert;
@property (nonatomic, assign) TSOrderType orderType;
@property (nonatomic, assign) TSRole inviteRole;
@property (nonatomic, strong) dispatch_source_t  alertCutdownTimer;

@end

@implementation TSHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initRefreshView];
    [self.view addSubview:self.shareView];
    self.positionModel = [[CCPositionModel alloc]init];
    if ([Def objectForKey:@"cityName"]) {
        [self.header.addressBtn setTitle:[Def objectForKey:@"cityName"] forState:UIControlStateNormal];
        self.positionModel.cityName = [Def objectForKey:@"cityName"];
        self.positionModel.cityId = [Def valueForKey:@"cityId"];
    }
    if (!self.hideHeader) {
        self.title = @"附近货源";
        [self setRightBtn];
    }else{
        self.title = @"搜索货源";
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!self.hideHeader && ![Def objectForKey:@"cityName"]) {
        [self locateBtnClick];
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
    [self refreshData];
    if (!self.hideHeader) {
        App_Delegate.transportTabbarVc.rightTitle = @"切换首页";
        [self.view endEditing:YES];
    }
}
- (void)setRightBtn {
    if (![App_Delegate.defaultHome isEqualToString:@"default"]) {
        UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [rightBtn setTitle:@"消息" forState:UIControlStateNormal];
        rightBtn.titleLabel.font = FFont(14);
        [rightBtn addTarget:self action:@selector(message) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
        self.navigationItem.rightBarButtonItem = right;
    }
}
- (void)message {
    SystemNewsController *vc = [[SystemNewsController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{@"r_id":USERID}];
    if(self.keyword && ![self.keyword isEqualToString:@""]){
        [self.dataArray removeAllObjects];
        [param setValue:self.keyword forKey:@"keyword"];
        [param setValue:App_Delegate.lat forKey:@"lat"];
        [param setValue:App_Delegate.lng forKey:@"lon"];
    }else{
        [param setValue:App_Delegate.lat forKey:@"lat"];
        [param setValue:App_Delegate.lng forKey:@"lon"];
        if (self.orderType == TSOrderTypeNearby) {
            [param setValue:@"fu_ji" forKey:@"type"];
        }else{
            if (self.positionModel.cityId) {
                [param setObject:self.positionModel.cityId forKey:@"location_city"];
                if (self.orderType == TSOrderTypeSameCity) {//同城
                    [param setObject:@"1" forKey:@"city_wide"];
                }
            }
        }
        [param setObject:@(self.dataArray.count) forKey:@"offset"];
        [param setValue:@"30" forKey:@"limit"];
    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/resource_search") params:param target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            for (NSDictionary *dict in success[@"data"]) {
                TSOrderInfo *model = [TSOrderInfo modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
            if (self.dataArray.count == 0) {
                [self checkOrderCount];
            }
            [self endLoding:success[@"data"]];
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TSHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.showTimer = YES;
    if (self.dataArray.count > indexPath.row) {
        TSOrderInfo *model = self.dataArray[indexPath.row];
        cell.model = model;
        cell.phoneBtn.tag = indexPath.row;
        cell.locateBtn.tag = indexPath.row;
        [cell.phoneBtn addTarget:self action:@selector(makePhoneCall:) forControlEvents:UIControlEventTouchUpInside];
        [cell.locateBtn addTarget:self action:@selector(navBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    TSHomeCell *tsCell = (TSHomeCell *)cell;
    [tsCell removeTimer];
}

//打电话按钮
- (void)makePhoneCall:(UIButton *)sender {
    NSInteger index = sender.tag;
    self.selectedModel = self.dataArray[index];
    self.isSelectedSameCityOrder = [self.selectedModel.location_city_name isEqualToString:self.selectedModel.destination_city_name];
    NSInteger money = self.isSelectedSameCityOrder ? 25 : 45;
    if(App_Delegate.transportTabbarVc.isDriver){
        if([[UserManager sharedInstance].money floatValue] > money){
            [self moneyEnoughAlert];
        }else{
            [self noMoneyAlert];
        }
    }else{
        [self contact];
    }
}
//导航
- (void)navBtnClick:(UIButton *)sender {
    NSInteger index = sender.tag;
    if (self.dataArray.count > index) {
        TSOrderInfo *model = self.dataArray[index];
        CLLocationCoordinate2D start = CLLocationCoordinate2DMake([model.location_lat doubleValue], [model.location_lon doubleValue]);
        CLLocationCoordinate2D end =  CLLocationCoordinate2DMake([model.destination_lat doubleValue], [model.destination_lon doubleValue]);
        [App_Delegate.transportTabbarVc.navigator  navigationWithStart:start startName:@"订单起点" end:end endName:@"订单终点"];
        
    }
}
//余额不足
- (void)noMoneyAlert {
    NSInteger money = self.isSelectedSameCityOrder ? 25 : 45;
    NSString *message = [NSString stringWithFormat:@"余额不足%zd元，无法抢单，请充值",money];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(4, 2)];
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 16)];
    [alert setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"现在充值" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        RechargeViewController *vc = [[RechargeViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [action2 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action1 setValue:MTRGB(0x888888) forKey:@"titleTextColor"];
    [self presentViewController:alert animated:YES completion:nil];
}
//余额足够
- (void)moneyEnoughAlert {
    NSInteger money = self.isSelectedSameCityOrder ? 25 : 45;
    NSString *message = [NSString stringWithFormat:@"联系成功需支付%zd元信息服务费不成功不收费，是否马上联系货主？",money];

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(7, 2)];
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 31)];
    [alert setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self contact];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [action2 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action1 setValue:MTRGB(0x888888) forKey:@"titleTextColor"];
    [self presentViewController:alert animated:YES completion:nil];
}
//联系货主
- (void)contact {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/ring_up") params:@{@"r_id":USERID,@"order_id":STR(self.selectedModel.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            
            NSString *phone = success[@"data"][@"order_data"][@"reader_mobile"];
            if(STRINGEMPTY(phone)){
                phone = success[@"data"][@"order_data"][@"hz_phone_2"];
            }
            NSString *state = success[@"data"][@"state"][@"msg"];
            if ([state isEqualToString:@"可以抢"]) {
                [self phoneConsult:phone];
            }else{
                SHOW(@"订单暂时被锁定，请稍后再试");
            }
        }else{
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}

//电话咨询
- (void)phoneConsult:(NSString *)phone {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"联系货主" message:phone preferredStyle:1];
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                    
                                    [self performSelector:@selector(confirmOrder) withObject:nil afterDelay:0.5];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
        [self cancelOrderLock];
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [self presentViewController:alertController animated:YES completion:nil];
}
//确认订单
- (void)confirmOrder {
    NSInteger money = self.isSelectedSameCityOrder ? 25 : 45;
    NSString *message = [NSString stringWithFormat:@"如谈妥，请确认订单，确认订单将扣费%zd元，不确认订单会被别人抢单而且无法确认装货",money];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(17, 2)];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(26, 6)];
    
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 40)];
    [alert setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self cancelOrderLock];
        [self refreshData];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确认订单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self comfirmOrderData];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [action2 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action1 setValue:MTRGB(0x888888) forKey:@"titleTextColor"];
    [self presentViewController:alert animated:YES completion:nil];
    WEAKSELF
    if (!self.alertCutdownTimer) {
        // 倒计时时间
        __block int timeout = 15*60;
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _alertCutdownTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
            dispatch_source_set_timer(weakSelf.alertCutdownTimer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            dispatch_source_set_event_handler(_alertCutdownTimer, ^{
                if (timeout<=0) {
                    dispatch_source_cancel(weakSelf.alertCutdownTimer);
                    self.alertCutdownTimer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [alert  dismissViewControllerAnimated:YES completion:^{}];
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSInteger min = timeout / 60;
                        NSInteger second = timeout % 60;
                        if (min > 0) {
                            [action2 setValue:[NSString stringWithFormat:@"%@(%zd:%zd)", @"确认订单", min,second] forKey:@"title"];
                        }else{
                            [action2 setValue:[NSString stringWithFormat:@"%@(%zds)", @"确认订单", second] forKey:@"title"];
                        }
                    });
                    timeout--;
                }
            });
            dispatch_resume(_alertCutdownTimer);
        }
    }
    
}
- (void)comfirmOrderData {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/affirm_order") params:@{@"r_id":USERID,@"order_id":STR(self.selectedModel.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            SHOW(@"订单确认成功");
            if (self.hideHeader) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            NSInteger index = App_Delegate.transportTabbarVc.isDriver ? 1 : 2;
            [App_Delegate.transportTabbarVc setSelectIndex:index];
            [App_Delegate.transportTabbarVc.orderVc gotoIndex:1];
            [App_Delegate.transportTabbarVc.orderVc refreshData];
            [self refreshData];
        }else{
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
//订单解除锁定
- (void)cancelOrderLock {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/setinitialize") params:@{@"r_id":USERID,@"order_id":STR(self.selectedModel.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            
        }
    } failure:^(NSError *failure) {
    }];
    
}
#pragma mark - 代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.header.searchBar endEditing:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark - 定位
- (void)locateBtnClick {
    self.orderType = TSOrderTypeCity;
    self.showNoOrderAlert = NO;
    SetHomeViewController *vc = [[SetHomeViewController alloc]init];
    vc.selectPosition = YES;
    vc.selectPositionBlk = ^(CCPositionModel *positionModel) {
        [self.header.addressBtn setTitle:positionModel.cityName forState:UIControlStateNormal];
        [Def setValue:positionModel.cityName forKey:@"cityName"];
        [Def setValue:positionModel.cityId forKey:@"cityId"];
        self.positionModel = positionModel;
        App_Delegate.currentCityId = self.positionModel.cityId;
        [self refreshData];
        App_Delegate.registedJpushTag = NO;
        [App_Delegate registJPushTags];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)nearbySegmentChange {
    BOOL nearby = self.header.segmentControl.selectedSegmentIndex == 1;
    if (nearby) {
        self.orderType = TSOrderTypeNearby;
    }else{
        self.orderType = TSOrderTypeSameCity;
    }
    [self refreshData];
}

#pragma mark - 搜索
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
//    if (searchBar.text) {
//        self.keyword = searchBar.text;
//        [self refreshData];
//        [self.view endEditing:YES];
//    }
    TSHomeController *vc = [[TSHomeController alloc]init];
    vc.hideHeader = YES;
    vc.keyword = searchBar.text;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
//        self.keyword = nil;
//        [self refreshData];
        [self.view endEditing:YES];
    }
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self refreshData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
        CGFloat yy = system >= 11.0 || !App_Delegate.isDefaultRoot ? 64 : 0;
        if (self.hideHeader) {
            _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        }else{
            _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, yy, App_Width, App_Height - 49 - yy)];
        }
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 120;
        if (!self.hideHeader) {
            _tableView.tableHeaderView = self.header;            
        }
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"TSHomeCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
        
#ifdef __IPHONE_11_0
        if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
#endif
        
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (TSHomeHeaderView *)header {
    if (!_header) {
        _header = [TSHomeHeaderView instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 44);
        [_header.addressBtn addTarget:self action:@selector(locateBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_header.arrowBtn addTarget:self action:@selector(locateBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _header.searchBar.delegate = self;
        [_header.segmentControl addTarget:self action:@selector(nearbySegmentChange) forControlEvents:UIControlEventValueChanged];
        _header.segmentControl.selectedSegmentIndex = -1;

        if ([Def boolForKey:@"sameCity"]) {
            _header.segmentControl.selectedSegmentIndex = 0;
            self.orderType = TSOrderTypeSameCity;
        }
    }
    return _header;
}
- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 44, App_Width, App_Height - 64 - 49 - 44)];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

- (void)checkOrderCount {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/freightlength") params:@{@"r_id":USERID,@"type":@"all",@"city":STR(self.positionModel.cityId)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            NSInteger count = [success[@"data"] integerValue];
            if (count == 0) {
                [self noOrderAlert];
            }
        }
    } failure:^(NSError *failure) {
    }];
}


//货源不足
- (void)noOrderAlert{
    if (self.showNoOrderAlert || !App_Delegate.transportTabbarVc.isDriver) {
        return;
    }
    self.showNoOrderAlert = YES;
    NSString *str = [NSString stringWithFormat:@"本辖区注册司机达到2000人将启动市场，请邀请更多人注册，以早日启动市场！成功邀请注册一个，奖励1元！"];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:str preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"邀请货主" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.inviteRole = TSRoleStation;
        [self share];
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"邀请司机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.inviteRole = TSRoleDriver;
        [self share];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    [action2 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action3 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action1 setValue:MTRGB(0x888888) forKey:@"titleTextColor"];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - 分享

- (CCShareView *)shareView {
    if (!_shareView) {
        _shareView = [CCShareView instanceView];
        _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.pengyouquanBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareView;
}

- (void)share {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height - 49;
    }];
}
- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
    }];
}
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [self sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    // 设置短信内容
    NSString *url = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    NSString *shareText = [[NSString stringWithFormat:@"%@%@", [self getInviteTextTitle],[self getInviteTextDesc]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, url];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
    self.inviteRole = TSRoleNormal;
    
}

- (NSString *)getInviteTextTitle {
    NSString *text;
    if (self.inviteRole == TSRoleStation) {
        text = @"怪不得配货站都在这里发货，原来如此！";
    }else if (self.inviteRole == TSRoleDriver){
        text = @"怪不得兄弟们都在这里找货，原来如此。";
    }else{
        text = App_Delegate.transportTabbarVc.isDriver ? @"怪不得兄弟们都在这里找货，原来如此。" : @"怪不得配货站都在这里发货，原来如此！";
    }
    return text;
}
- (NSString *)getInviteTextDesc {
    NSString *text;
    if (self.inviteRole == TSRoleStation) {
        text = @"不收费，奖励多，海量司机一秒响应，随时随地发货！";
    }else if (self.inviteRole == TSRoleDriver){
        text = @"货源多，语音短信智能报单，不会错过每一单生意！";
    }else{
        text = App_Delegate.transportTabbarVc.isDriver ? @"货源多，语音短信智能报单，不会错过每一单生意！" : @"不收费，奖励多，海量司机一秒响应，随时随地发货！";
    }
    return text;
}
- (void)weixinBtnClick:(UIButton *)sender {
    [self shareCancelBtnClick];
    NSInteger tag = sender.tag;
    UMSocialPlatformType  platform = tag == 10 ? UMSocialPlatformType_WechatSession : UMSocialPlatformType_WechatTimeLine;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@""]]];
    NSString *title = [self getInviteTextTitle];
    NSString *desc = [self getInviteTextDesc];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: title  descr:desc thumImage:IMAGENAMED(@"cct")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        self.inviteRole = TSRoleNormal;
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

@end

