//
//  TSHomeCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSHomeCell.h"
#import "NSMutableAttributedString+Style.h"

@implementation TSHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle =  UITableViewCellSelectionStyleNone;
    self.imgView.layer.cornerRadius = 25;
    self.imgView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(TSOrderInfo *)model {
    _model = model;
    NSString *name = model.hz_name;
    NSString *url = [NSString stringWithFormat:@"images/%@",model.reader_portrait];
    url = BASEURL_WITHOBJC(url);
    [self.imgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"head")];
    self.nameLbl.text = STR(name);
    self.tagLbl.text = [model.car_space isEqualToString:@"2"] ? @"整车" :@"零担";
    self.tagLbl.backgroundColor = [model.car_space isEqualToString:@"2"] ? MTRGB(0x73a5e8) : MTRGB(0xb789ce);
    
    
    self.descLbl.text = [NSString stringWithFormat:@"%@ %@ %@",[model getCarLength],STR(model.car_type4), STR(model.beizhu)];
    
    NSString *size = @"";
    if ([model.size containsString:@"*"] ) {
        NSInteger start = [model.size rangeOfString:@"*"].location;
        if (model.size.length > start) {
            size = [model.size substringFromIndex:start+1];
        }
    }else{
        size = model.size;
    }
    if(!STRINGEMPTY(size)){
        size = [NSString stringWithFormat:@"%@方",size];
    }
    
    self.carDescLbl.text = [NSString stringWithFormat:@"%@ %@吨 %@",[model getCarType],STR(model.car_type2_name) ,size];
    
    
    self.timeLbl.text = [TOOL convertTextTime:model.create_time];
    if(model.distance > 1000){
        self.distanceLbl.text = [NSString stringWithFormat:@"%ld公里",model.distance / 1000 ];
    }else{
        self.distanceLbl.text = [NSString stringWithFormat:@"%ld米",model.distance ];
    }
    CGFloat comment;
    if (model.hz_deal_sum == 0 || model.hz_comment_h > model.hz_deal_sum) {
        comment = 100;
    }else{
        comment = (model.hz_deal_sum - model.hz_comment_c - model.hz_comment_z) / (float)model.hz_deal_sum * 100;
    }
    if (comment <= 0) {
        comment = 100;
    }
    self.commentLbl.text = [NSString stringWithFormat:@"交易%ld  好评率%.0f%%",model.hz_deal_sum,comment ];
    
    if ([model.location_city_name isEqualToString:@"市辖区"] || [model.location_city_name isEqualToString:@"县"]) {
        model.location_city_name = model.location_province_name;
    }
    if ([model.destination_city_name isEqualToString:@"市辖区"] || [model.destination_city_name isEqualToString:@"县"]) {
        model.destination_city_name = model.destination_province_name;
    }
    
    
    NSString *start = [model.location_district_name isEqualToString:@"不限"] || !model.location_district_name ? model.location_city_name : [NSString stringWithFormat:@"%@%@",model.location_city_name, model.location_district_name];
    NSString *end = [model.destination_district_name isEqualToString:@"不限"] || !model.destination_district_name ? model.destination_city_name : [NSString stringWithFormat:@"%@%@", model.destination_city_name,model.destination_district_name];

    NSString *text = [NSString stringWithFormat:@"%@—>%@",STR(start),STR(end)];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:text];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15.0] range:NSMakeRange(0, model.location_city_name.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15.0] range:[text rangeOfString:model.destination_city_name]];
    
    self.titleLbl.attributedText = attributedString;
    
    self.maskView.hidden = YES;
    self.countDownLbl.hidden = YES;
    if (self.showTimer) {
        NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
        NSTimeInterval time = [date timeIntervalSince1970];
        NSInteger left = time - [model.contact_time floatValue];
        if (left < 15 * 60) { // 倒计时
            self.leftTime = 15*60 - left;
            [self startTimer];
        }        
    }
}
- (void)startTimer {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    WEAKSELF
    self.timer = [NSTimer timerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        weakSelf.leftTime--;
        if (weakSelf.leftTime > 0) {
            NSInteger minute = ((weakSelf.leftTime % (24 * 60 * 60)) % (60 * 60)) / 60;
            NSInteger second = ((weakSelf.leftTime % (24 * 60 * 60)) % (60 * 60)) % 60;
            NSString *time = [NSString stringWithFormat:@"%zd分%zd秒", minute, second];
            NSString *text = [NSString stringWithFormat:@"洽谈中，%@后还可以抢单", time];
            weakSelf.countDownLbl.text = text;
        }
        weakSelf.countDownLbl.hidden = weakSelf.leftTime <= 0;
        weakSelf.maskView.hidden = weakSelf.leftTime <= 0;
    }];
    [[NSRunLoop currentRunLoop]addTimer:self.timer forMode:NSRunLoopCommonModes];
}
- (void)removeTimer {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}
@end
