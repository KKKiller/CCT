//
//  TSHomeHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSHomeHeaderView.h"

@implementation TSHomeHeaderView

+ (instancetype)instanceView {
    TSHomeHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    
    
    view.searchBar.layer.borderColor = BACKGRAY.CGColor;
    view.searchBar.layer.borderWidth  = 1;
    view.searchBar.placeholder = @"搜索";
    //光标颜色
    view.searchBar.tintColor = MAINGREEN;
    //拿到searchBar的输入框
    UITextField *searchTextField = [view.searchBar valueForKey:@"_searchField"];
    //字体大小
    searchTextField.font = [UIFont systemFontOfSize:13];
    
    UIView *firstSubView = view.searchBar.subviews.firstObject;
    UIView *backgroundImageView = [firstSubView.subviews firstObject];
    [backgroundImageView removeFromSuperview];
    
    return view;
}

@end
