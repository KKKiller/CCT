//
//  TSHomeController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
typedef NS_ENUM(NSUInteger, TSOrderType) {
    TSOrderTypeCity = 0,
    TSOrderTypeSameCity,
    TSOrderTypeNearby,
};
@interface TSHomeController : BaseViewController
@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, assign) BOOL hideHeader;
@end
