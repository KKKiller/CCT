//
//  TSVoiceReader.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSVoiceReader.h"
#import "BDSSpeechSynthesizer.h"


@interface TSVoiceReader ()


@end
@implementation TSVoiceReader
- (instancetype)init {
    if (self = [super init]) {
    }
    return self;
}


- (void)readText:(NSString *)text {
    [[BDSSpeechSynthesizer sharedInstance] speakSentence:text withError:nil];
}
@end
