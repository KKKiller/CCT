//
//  TSVerifyController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "TSCarModel.h"
#import "TSCarOwnerModel.h"
#import "TSStationModel.h"
typedef void (^verifedBlock)();

@interface TSVerifyController : BaseViewController
@property (nonatomic, assign) BOOL isStation;
@property (nonatomic, strong) TSCarModel *carModel;
@property (nonatomic, strong) TSCarOwnerModel *carOwnerModel;
@property (nonatomic, strong) verifedBlock verifedBlock;
@property (nonatomic, strong) TSStationModel *stationModel;

@property (nonatomic, assign) TSRole showRole;
@property (nonatomic, strong) NSString *showUserId;


@end
