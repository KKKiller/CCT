//
//  TSVerifyController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSVerifyController.h"
#import "TSVerifyView.h"
#import "TSVerifyPhotoView.h"
#import "JFLocation.h"
#import "JFAreaDataManager.h"
#import "JFCityViewController.h"
#import "BRStringPickerView.h"
#import "TSTypeModel.h"
#import "SetHomeViewController.h"
#import "CCPositionModel.h"
#import "CCShareView.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
@interface TSVerifyController ()<JFLocationDelegate, JFCityViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationBarDelegate,CNContactPickerDelegate,MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) TSVerifyView *verifyView;
@property (nonatomic, strong) TSVerifyPhotoView *photoView;
@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CCShareView *shareView;

/** 城市定位管理器*/
@property (nonatomic, strong) JFLocation *locationManager;
/** 城市数据管理器*/
@property (nonatomic, strong) JFAreaDataManager *manager;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) CCPositionModel *positionModel;

@property (nonatomic, strong) NSData *imgData1;
@property (nonatomic, strong) NSData *imgData2;
@property (nonatomic, strong) NSData *imgData3;
@property (nonatomic, strong) NSData *imgData4;
@property (nonatomic, strong) NSMutableArray *imgUrlArray;
@property (nonatomic, strong) NSMutableArray *imgDataArray;

@property (nonatomic, assign) CLLocationCoordinate2D stationLocation;

@end

@implementation TSVerifyController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI ];
    self.imgUrlArray = [NSMutableArray array];
    [NOTICENTER addObserver:self selector:@selector(chooseAddressFinished:) name:@"ChooseLocationNotification" object:nil];
    [self loadData];
    [self loadDataWithUserId];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}

- (void)loadDataWithUserId{
    if (self.showUserId) {
        NSString *url = self.showRole == TSRoleDriver ? @"freight/get_owner_info" : @"freight/get_apply_owner";
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(url) params:@{@"r_id":self.showUserId} target:self success:^(NSDictionary *success) {
            if ([success[@"error"]  isEqual: @(0)]) {
                if (self.showRole == TSRoleDriver) {
                    self.carModel = [TSCarModel modelWithJSON: success[@"data"][@"car"]];
                    self.carOwnerModel = [TSCarOwnerModel modelWithJSON:success[@"data"][@"car_owner"]];
                }else{
                    self.stationModel = [TSStationModel modelWithJSON:success[@"data"]];
                }
                [self loadData];
            }else{
                SHOW(success[@"msg"]);
            }
        } failure:^(NSError *failure) {
        }];
    }
}


- (void)loadData {
    
    if (self.carOwnerModel) {
        self.verifyView.cityLbl.text = self.carOwnerModel.city_name;
        self.verifyView.cityLbl.textColor = MTRGB(0x717171);
        self.verifyView.nameField.text = self.carOwnerModel.info_name;
        self.verifyView.idField.text = self.carOwnerModel.certificate;
        self.verifyView.contactNameField.text = self.carOwnerModel.phone_1;
        self.verifyView.contactPhoneField.text = self.carOwnerModel.phone_2;
        //位置
        self.positionModel  = [[CCPositionModel alloc]init];
        self.positionModel.provinceId = self.carOwnerModel.province;
        self.positionModel.cityId = self.carOwnerModel.city;
        self.positionModel.districtId = self.carOwnerModel.area;
        
//        NSString *url1 = BASEURL_WITHOBJC(self.carOwnerModel.province_certificate_1);
        [self.photoView.photo1 setImageWithURL:URL(self.carOwnerModel.province_certificate_1) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData1 = UIImagePNGRepresentation(image);
        }];
        [self.photoView.photo2 setImageWithURL:URL(self.carOwnerModel.province_certificate_2) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData2 = UIImagePNGRepresentation(image);

        }];
        [self.photoView.photo3 setImageWithURL:URL(self.carOwnerModel.driving_licence_1) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData3 = UIImagePNGRepresentation(image);

        }];
        [self.photoView.photo4 setImageWithURL:URL(self.carOwnerModel.driving_licence_2) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData4 = UIImagePNGRepresentation(image);
        }];
        
        if (self.carOwnerModel.province_certificate_1) {
            [self.imgUrlArray addObject:self.carOwnerModel.province_certificate_1];
        }
        
        if (self.carOwnerModel.province_certificate_2) {
            [self.imgUrlArray addObject:self.carOwnerModel.province_certificate_2];
        }
        if (self.carOwnerModel.driving_licence_1) {
            [self.imgUrlArray addObject:self.carOwnerModel.driving_licence_1];
        }
        if (self.carOwnerModel.driving_licence_2) {
            [self.imgUrlArray addObject:self.carOwnerModel.driving_licence_2];
        }

        if (self.carModel) {//车信息
            self.verifyView.carNumField.text = self.carModel.driver_id;
            self.verifyView.carTypeLbl.text = self.carModel.type1_name;
            self.verifyView.carHeavyLbl.text = self.carModel.type2_name;
            self.verifyView.carLengthLbl.text = self.carModel.type3_name;
            
            self.verifyView.carTypeLbl.textColor = MTRGB(0x717171);
            self.verifyView.carHeavyLbl.textColor = MTRGB(0x717171);
            self.verifyView.carLengthLbl.textColor = MTRGB(0x717171);
        }

    }else if (self.stationModel){//配货站资料
        self.verifyView.cityLbl.text = self.stationModel.city_name;
        self.verifyView.cityLbl.textColor = MTRGB(0x717171);
        self.verifyView.nameField.text = self.stationModel.name;
        self.verifyView.idField.text = self.stationModel.certificate;
        self.verifyView.contactNameField.text = self.stationModel.phone_1;
        self.verifyView.contactPhoneField.text = self.stationModel.phone_2;
        self.verifyView.addressField.text = self.stationModel.detailed_address;
        self.stationLocation = CLLocationCoordinate2DMake([self.stationModel.lat floatValue], [self.stationModel.log floatValue]);
        //位置
        self.positionModel  = [[CCPositionModel alloc]init];
        self.positionModel.provinceId = self.stationModel.province;
        self.positionModel.cityId = self.stationModel.city;
        self.positionModel.districtId = self.stationModel.area;
        
        [self.photoView.photo1 setImageWithURL:URL(self.stationModel.province_certificate_1) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData1 = UIImagePNGRepresentation(image);
        }];
        [self.photoView.photo2 setImageWithURL:URL(self.stationModel.province_certificate_2) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData2 = UIImagePNGRepresentation(image);
            
        }];
        [self.photoView.photo3 setImageWithURL:URL(self.stationModel.province_certificate_3) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData3 = UIImagePNGRepresentation(image);
            
        }];
        [self.photoView.photo4 setImageWithURL:URL(self.stationModel.license) placeholder:IMAGENAMED(@"addaphoto") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            self.imgData4 = UIImagePNGRepresentation(image);
        }];
    }
}
//提交
- (void)submitBtnClick {
    if (self.showUserId) {
        SHOW(@"信息仅供查看，无法修改");
        return;
    }
    if (![self checkEmpty]) {
        return;
    }
//    if (self.isStation) {
//        [self uploadVerifyData];
//    }else{
        self.imgUrlArray = [NSMutableArray array];
        self.imgDataArray = [NSMutableArray arrayWithArray: @[self.imgData1,self.imgData2,self.imgData3,self.imgData4]];
        [self uploadImg];
//    }
    [TOOL showLoading:self.view];
}
- (BOOL)checkEmpty {
    if ( (!self.imgData1 || !self.imgData2 || !self.imgData3 || !self.imgData4 )) {
        SHOW(@"请上传认证图片上传");
        return NO;
    }
    if (!self.positionModel) {
        SHOW(@"请选择地址");
        return NO;
    }
    if (self.verifyView.idField.text.length == 0 || self.verifyView.nameField.text.length == 0 || self.verifyView.contactPhoneField.text.length == 0 || self.verifyView.contactNameField.text.length == 0 ) {
        SHOW(@"请填写必要数据");
        return NO;
    }
    if (self.isStation) {
        if (self.verifyView.addressField.text.length == 0 || self.stationLocation.latitude == 0) {
            SHOW(@"请选择详细地址");
            return NO;
        }
    }else{
        if (self.verifyView.carNumField.text.length == 0 || [self.verifyView.carTypeLbl.text containsString:@"请选择"] || [self.verifyView.carHeavyLbl.text containsString:@"请选择"]|| [self.verifyView.carLengthLbl.text containsString:@"请选择"]) {
            SHOW(@"请填写车辆必要数据");
            return NO;
        }
    }
    return YES;
}
- (void)uploadImg {
    if (self.imgDataArray.count == 0) {
        //上传完成
        [self uploadVerifyData];
        return;
    }
    NSData *data = self.imgDataArray[0];
    data = [TOOL imageCompress:data];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"freight/addimg") params:@{@"r_id":USERID,@"name":@"file"} target:self imgaeData:data success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [self.imgDataArray removeObjectAtIndex:0];
            NSString *url = success[@"msg"];//[NSString stringWithFormat:@"http://www.cct369.com/village/public%@", success[@"msg"]];
            [self.imgUrlArray addObject:url];
            [self uploadImg];
        }else{
            [self.imgUrlArray removeAllObjects];
            SHOW(success[@"msg"]);
            [TOOL hideLoading:self.view];
        }
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
- (void)uploadVerifyData {
    NSDictionary *dict = @{@"r_id":USERID,
                           @"province":STR(self.positionModel.provinceId),
                           @"city":STR(self.positionModel.cityId),
                           @"area":STR(self.positionModel.districtId),
                           @"certificate":self.verifyView.idField.text,
                           @"phone_1":self.verifyView.contactNameField.text,
                           @"phone_2":self.verifyView.contactPhoneField.text,
                           
                           };
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:dict];
    if (self.isStation) {
        [param setValue:self.verifyView.nameField.text forKey:@"name"];
        [param setValue:[NSString stringWithFormat:@"%f",self.stationLocation.latitude] forKey:@"lat"];
        [param setValue:[NSString stringWithFormat:@"%f",self.stationLocation.longitude] forKey:@"log"];
        [param setValue:self.verifyView.addressField.text forKey:@"detailed_address"];
        
        [param setValue:self.imgUrlArray.count > 0 ? self.imgUrlArray[0] : @"" forKey:@"province_certificate_1"];
        [param setValue:self.imgUrlArray.count > 1 ? self.imgUrlArray[1] : @"" forKey:@"province_certificate_2"];
        [param setValue:self.imgUrlArray.count > 2 ? self.imgUrlArray[2] : @"" forKey:@"province_certificate_3"];
        [param setValue:self.imgUrlArray.count > 3 ? self.imgUrlArray[3] : @"" forKey:@"license"];
    }else{
        [param setValue:self.verifyView.nameField.text forKey:@"info_name"];
        [param setValue:self.imgUrlArray.count > 0 ? self.imgUrlArray[0] : @"" forKey:@"province_certificate_1"];
        [param setValue:self.imgUrlArray.count > 1 ? self.imgUrlArray[1] : @"" forKey:@"province_certificate_2"];
        [param setValue:self.imgUrlArray.count > 2 ? self.imgUrlArray[2] : @"" forKey:@"driving_licence_1"];
        [param setValue:self.imgUrlArray.count > 3 ? self.imgUrlArray[3] : @"" forKey:@"driving_licence_2"];
        [param setValue:self.verifyView.carNumField.text forKey:@"plate_number"];
        [param setValue:[self carType] forKey:@"car_tyle_1"];
        [param setValue:[self carHeavy] forKey:@"car_tyle_2"];
        [param setValue:[self carLength] forKey:@"car_tyle_3"];

    }
    NSString *url = self.isStation ? @"freight/apply_owner" : @"freight/apply_car";
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(url) params:param target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            if (self.carOwnerModel || self.stationModel) {
                [self.navigationController popViewControllerAnimated:YES];
                SHOW(@"修改成功");
            }else{
                [self verifySuccessAlert];
            }
            if (self.verifedBlock) {
                self.verifedBlock();
            }
        }else{
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}

- (void)verifySuccessAlert {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"资料提交成功\n审核周期1-3个工作日" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"分享好友加快审核" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self share];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];
}



- (void)setupUI {
    self.view.backgroundColor = TSGRAY;
    [self setTitle:self.isStation ? @"物流配货站认证" : @"司机认证"];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.verifyView];
//    if (!self.isStation) {
    [self.scrollView addSubview:self.photoView];
//    }
    if (!self.showUserId) {
        [self.scrollView addSubview:self.submitBtn];        
    }
    CGFloat height = self.isStation ? 440 -  180 + 235 : 440 - 45 + 235;
    self.scrollView.contentSize = CGSizeMake(App_Width, height + 75);
    [self.view addSubview:self.shareView];

}
- (void)carTypeTap {
    NSMutableArray *typeArray = [NSMutableArray array];
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carTypeArray) {
        NSString *str = model.type_name;
        [typeArray addObject:str];
    }
    [BRStringPickerView showStringPickerWithTitle:@"车辆类别" dataSource:typeArray defaultSelValue:nil isAutoSelect:NO resultBlock:^(NSString * selectValue) {
        self.verifyView.carTypeLbl.text =selectValue;
        self.verifyView.carTypeLbl.textColor = MTRGB(0x717171);
    }];
}
- (void)carLengthTap {
    NSMutableArray *lengthArray = [NSMutableArray array];
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carLengthArray) {
        NSString *str = model.type_name;
        [lengthArray addObject:str];
    }
    [BRStringPickerView showStringPickerWithTitle:@"车长" dataSource:lengthArray defaultSelValue:nil isAutoSelect:NO resultBlock:^(NSString * selectValue) {
        self.verifyView.carLengthLbl.text =selectValue;
        self.verifyView.carLengthLbl.textColor = MTRGB(0x717171);
        
    }];
}
- (void)carHeavyTap {
    NSMutableArray *heavyArray = [NSMutableArray array];
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carHeaveArray) {
        NSString *str = model.type_name;
        [heavyArray addObject:str];
    }
    [BRStringPickerView showStringPickerWithTitle:@"车重" dataSource:heavyArray defaultSelValue:nil isAutoSelect:NO resultBlock:^(NSString * selectValue) {
        self.verifyView.carHeavyLbl.text =selectValue;
        self.verifyView.carHeavyLbl.textColor = MTRGB(0x717171);
    }];
}
- (NSString *)carType {
    NSString *carType = @"" ;
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carTypeArray) {
        if ([self.verifyView.carTypeLbl.text isEqualToString:model.type_name]) {
            carType = model.id;
            break;
        }
    }
    return carType;
}

- (NSString *)carLength {
    NSString *carLength = @"" ;
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carLengthArray) {
        if ([self.verifyView.carLengthLbl.text isEqualToString:model.type_name]) {
            carLength = model.id;
            break;
        }
    }
    return carLength;
}
- (NSString *)carHeavy {
    NSString *carHeavy = @"" ;
    for (TSTypeModel *model in App_Delegate.transportTabbarVc.carHeaveArray) {
        if ([self.verifyView.carHeavyLbl.text isEqualToString:model.type_name]) {
            carHeavy = model.id;
            break;
        }
    }
    return carHeavy;
}
//自动定位
- (void)locateBtnClick {
    TSShowMapViewController *vc = [[UIStoryboard storyboardWithName:@"TSShowMapViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"TSShowMapViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)chooseAddressFinished:(NSNotification *)noti {
    NSDictionary *dict = noti.userInfo;
    NSString *city = dict[@"name"];
    NSString *address = dict[@"address"];
    NSString *lat = dict[@"lat"];
    NSString *lng = dict[@"lng"];
   
    self.stationLocation = CLLocationCoordinate2DMake([lat doubleValue], [lng doubleValue]);
    self.verifyView.addressField.text = address;
}
#pragma mark - 定位
- (void)chooseCity {
//    JFCityViewController *cityViewController = [[JFCityViewController alloc] init];
//    cityViewController.delegate = self;
//    cityViewController.title = @"选择城市";
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:cityViewController];
//    [self presentViewController:navigationController animated:YES completion:nil];
    
        SetHomeViewController *vc = [[SetHomeViewController alloc]init];
        vc.selectPosition = YES;
        vc.selectPositionBlk = ^(CCPositionModel *positionModel) {
            self.positionModel = positionModel;
            self.verifyView.cityLbl.text = self.positionModel.cityName;
            self.verifyView.cityLbl.textColor =  MTRGB(0x717171);
        };
        [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark - 照片
- (void)choosePicWithIndex:(NSInteger)index {
    self.currentIndex = index;
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
//    imagePicker.editing = YES;
    imagePicker.delegate = self;
//    imagePicker.allowsEditing = YES;
    //创建sheet提示框，提示选择相机还是相册
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"请选择打开方式" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    //相机选项
    
    UIAlertAction * camera = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //选择相机时，设置UIImagePickerController对象相关属性
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
        imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        //跳转到UIImagePickerController控制器弹出相机
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    
    //相册选项
    UIAlertAction * photo = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //选择相册时，设置UIImagePickerController对象相关属性
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //跳转到UIImagePickerController控制器弹出相册
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    
    //取消按钮
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    //添加各个按钮事件
    [alert addAction:camera];
    [alert addAction:photo];
    [alert addAction:cancel];
    //弹出sheet提示框
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    switch (self.currentIndex) {
        case 0:
            self.photoView.photo1.image = img;
            self.imgData1 = UIImagePNGRepresentation(img);
            break;
        case 1:
            self.photoView.photo2.image = img;
            self.imgData2 = UIImagePNGRepresentation(img);
            break;
        case 2:
            self.photoView.photo3.image = img;
            self.imgData3 = UIImagePNGRepresentation(img);
            break;
        case 3:
            self.photoView.photo4.image = img;
            self.imgData4 = UIImagePNGRepresentation(img);
            break;
            
        default:
            break;
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
    }
    return _scrollView;
}
- (TSVerifyView *)verifyView {
    if (!_verifyView) {
        _verifyView = [TSVerifyView instanceView];
        CGFloat height = self.isStation ? 440 -  180 : 440 - 45;
        _verifyView.frame = CGRectMake(0, 0, App_Width, height);
        _verifyView.addressView.hidden = !self.isStation;
        _verifyView.carView.hidden = self.isStation;
        _verifyView.carTopMargin.constant = 0;
        [_verifyView.chooseCityTap addTarget:self action:@selector(chooseCity)];
        [_verifyView.carTypeTap addTarget:self action:@selector(carTypeTap)];
        [_verifyView.carHeavyTap addTarget:self action:@selector(carHeavyTap)];
        [_verifyView.carLengthTap addTarget:self action:@selector(carLengthTap)];

        [_verifyView.locateBtn addTarget:self action:@selector(locateBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _verifyView;
}

- (TSVerifyPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [TSVerifyPhotoView instanceView];
        _photoView.isStation = self.isStation;
        _photoView.frame = CGRectMake(0, CGRectGetMaxY(self.verifyView.frame), App_Width, 235);
        WEAKSELF
        [_photoView.photoTap1 addActionBlock:^(id  _Nonnull sender) {
            [weakSelf choosePicWithIndex:0];
        }];
        [_photoView.photoTap2 addActionBlock:^(id  _Nonnull sender) {
            [weakSelf choosePicWithIndex:1];
        }];
        [_photoView.photoTap3 addActionBlock:^(id  _Nonnull sender) {
            [weakSelf choosePicWithIndex:2];
        }];
        [_photoView.photoTap4 addActionBlock:^(id  _Nonnull sender) {
            [weakSelf choosePicWithIndex:3];
        }];
    }
    return _photoView;
}
- (JFAreaDataManager *)manager {
    if (!_manager) {
        _manager = [JFAreaDataManager shareInstance];
        [_manager areaSqliteDBData];
    }
    return _manager;
}
- (UIButton *)submitBtn {
    if (!_submitBtn) {
        CGFloat y =  CGRectGetMaxY(self.photoView.frame) + 20; //self.isStation ? CGRectGetMaxY(self.verifyView.frame) + 20 :
        _submitBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, y, App_Width- 100, 35)];
        [_submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_submitBtn setTitle:@"提交" forState:UIControlStateNormal];
        _submitBtn.titleLabel.font = FFont(14);
        _submitBtn.titleLabel.textColor = WHITECOLOR;
        _submitBtn.backgroundColor = MAINGREEN;
        _submitBtn.layer.cornerRadius = 4;
        _submitBtn.layer.masksToBounds = YES;
    }
    return _submitBtn;
}




#pragma mark - 分享

- (CCShareView *)shareView {
    if (!_shareView) {
        _shareView = [CCShareView instanceView];
        _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.pengyouquanBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareView;
}

- (void)share {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height;
    }];
}
- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    
}
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [self sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    NSString *text = !self.isStation ? @"怪不得兄弟们都在这里找货，原来如此。货源多，语音短信智能报单，不会错过每一单生意！" : @"怪不得配货站都在这里发货，原来如此！不收费，奖励多，海量司机一秒响应，随时随地发货！";
    
    // 设置短信内容
    NSString *url = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    NSString *shareText = [text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, url];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
}


- (void)weixinBtnClick:(UIButton *)sender {
    [self shareCancelBtnClick];
    NSInteger tag = sender.tag;
    UMSocialPlatformType  platform = tag == 10 ? UMSocialPlatformType_WechatSession : UMSocialPlatformType_WechatTimeLine;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@""]]];
    NSString *title = !self.isStation  ? @"怪不得兄弟们都在这里找货，原来如此。" : @"怪不得配货站都在这里发货，原来如此！";
    NSString *desc = !self.isStation  ? @"货源多，语音短信智能报单，不会错过每一单生意！" : @"不收费，奖励多，海量司机一秒响应，随时随地发货！";
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: title  descr:desc thumImage:IMAGENAMED(@"cct")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}
@end
