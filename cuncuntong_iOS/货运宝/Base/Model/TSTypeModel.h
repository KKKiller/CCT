//
//  TSTypeModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/10.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSTypeModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, assign) NSInteger level;
@property (nonatomic, strong) NSString *type_name;
@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, assign) BOOL isSelected;
@end
