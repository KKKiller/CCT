//
//  TSVerifyHomeController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSVerifyHomeController.h"
#import "TSVerifyController.h"
@interface TSVerifyHomeController ()
@property (nonatomic, strong) UIButton *driverBtn;
@property (nonatomic, strong) UIButton *stationBtn;
@property (nonatomic, strong) UILabel *driverLbl;
@property (nonatomic, strong) UILabel *stationLbl;
@property (nonatomic, strong) UIButton *nextBtn;
@end

@implementation TSVerifyHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"认证"];
    self.view.backgroundColor = TSGRAY;
    CGFloat wh = 100;
    CGFloat margin = (App_Width - wh * 2) / 3;
    
    self.driverBtn = [[UIButton alloc]initWithFrame:CGRectMake(margin, 200, wh, wh)];
    [self.view addSubview:self.driverBtn];
    [self.driverBtn addTarget:self action:@selector(driverBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.driverBtn setImage:IMAGENAMED(@"btn_driver_selected") forState:UIControlStateSelected];
    [self.driverBtn setImage:IMAGENAMED(@"btn_driver_normal") forState:UIControlStateNormal];
    self.driverBtn.selected = YES;
    
    self.driverLbl = [[UILabel alloc]initWithFrame:CGRectMake(margin, CGRectGetMaxY(self.driverBtn.frame )+5, wh, 20)];
    [self.view addSubview:self.driverLbl];
    self.driverLbl.text = @"司机";
    self.driverLbl.textColor =TEXTBLACK3;
    self.driverLbl.font = FFont(14);
    self.driverLbl.textAlignment = NSTextAlignmentCenter;
    
    self.stationBtn = [[UIButton alloc]initWithFrame:CGRectMake(margin * 2 + wh, 200, wh, wh)];
    [self.view addSubview:self.stationBtn];
    [self.stationBtn addTarget:self action:@selector(stationBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.stationBtn setImage:IMAGENAMED(@"btn_distributionstation_selected") forState:UIControlStateSelected];
    [self.stationBtn setImage:IMAGENAMED(@"btn_distributionstation_normal") forState:UIControlStateNormal];
    
    
    self.stationLbl = [[UILabel alloc]initWithFrame:CGRectMake(margin * 2 + wh, CGRectGetMaxY(self.driverBtn.frame )+5, wh, 20)];
    [self.view addSubview:self.stationLbl];
    self.stationLbl.text = @"物流配货站";
    self.stationLbl.textColor =TEXTBLACK3;
    self.stationLbl.font = FFont(14);
    self.stationLbl.textAlignment = NSTextAlignmentCenter;
    
    self.nextBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 400, App_Width - 100, 35)];
    [self.view addSubview:self.nextBtn];
    self.nextBtn.layer.cornerRadius = 4;
    self.nextBtn.layer.masksToBounds = YES;
    self.nextBtn.backgroundColor = MAINGREEN;
    [self.nextBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    [self.nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [self.nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}
- (void)driverBtnClick {
    self.driverBtn.selected = YES;
    self.stationBtn.selected = NO;
    self.driverLbl.textColor = MAINGREEN;
    self.stationLbl.textColor = TEXTBLACK3;
}
- (void)stationBtnClick{
    self.stationBtn.selected = YES;
    self.driverBtn.selected = NO;
    self.driverLbl.textColor = TEXTBLACK3;
    self.stationLbl.textColor = MAINGREEN;
}
- (void)nextBtnClick {
    if (!self.driverBtn.selected && !self.stationBtn.selected) {
        SHOW(@"请选择类型");
        return;
    }
    TSVerifyController *vc = [[TSVerifyController alloc]init];
    vc.isStation = self.stationBtn.selected;
    if (self.verifyBlock) {
        vc.verifedBlock = ^{
            self.verifyBlock();
        };
    }
    [self.navigationController pushViewController:vc animated:YES];
}

@end
