//
//  TSMessageController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSMessageController.h"
#import "TSMessageCell.h"

static NSString *cellID = @"TSMessageCell";

@interface TSMessageController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, assign) NSInteger pageIndex;
@end

@implementation TSMessageController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"消息"];
    [self.view addSubview:self.tableView];
    [self initRefreshView];
    self.pageIndex = 1;
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}

- (void)refreshData { //点击tabbar刷新
    self.pageIndex = 1;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 获取数据
- (void)loadData {
    return;
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/wine_list") params:@{@"page":@(self.pageIndex),@"rid":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"code"]  isEqual: @(1)]) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"msg"]) {
                
            }
            [self endLoding:success[@"msg"]];
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.dataArray.count;
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TSMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.dataArray.count > indexPath.row) {
        
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        id model = self.dataArray[indexPath.row];
        [self.navigationController pushViewController:nil animated:YES];
    }
}


#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
//        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
//        CGFloat yy = system >= 11.0 ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 80;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"TSMessageCell" bundle:nil] forCellReuseIdentifier:cellID];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 64)];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}


@end
