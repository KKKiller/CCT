//
//  TSVoiceReader.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSVoiceReader : NSObject
- (void)readText:(NSString *)text;
@end
