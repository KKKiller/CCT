//
//  TSVerifyHomeController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
typedef void (^verifyBlock)();
@interface TSVerifyHomeController : BaseViewController
@property (nonatomic, strong) verifyBlock verifyBlock;
@end
