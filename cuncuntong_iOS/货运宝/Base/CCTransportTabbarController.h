//
//  CCTransportTabbarController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSVoiceReader.h"
#import "TSNavigationer.h"
#import "TSCarOwnerModel.h"
#import "TSStationModel.h"
#import "TSMyOrderController.h"
@interface CCTransportTabbarController : UITabBarController
@property (nonatomic, strong) NSString *rightTitle;
@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, strong) NSMutableArray *carTypeArray;
@property (nonatomic, strong) NSMutableArray *carHeaveArray;
@property (nonatomic, strong) NSMutableArray *carLengthArray;
@property (nonatomic, strong) NSMutableArray *downloadTypeArray;

@property (nonatomic, strong) TSVoiceReader *voiceReader;
@property (nonatomic, strong) TSNavigationer *navigator;
@property (nonatomic, assign) BOOL isDriver;
@property (nonatomic, assign) NSInteger messageCount;
@property (nonatomic, strong) TSMyOrderController *orderVc;

@property (nonatomic, strong) TSCarOwnerModel *carOwnerModel;
@property (nonatomic, strong) TSStationModel *stationModel;

- (void)setSelectIndex:(NSInteger)index;
- (void)updateRightTitle;
@end
