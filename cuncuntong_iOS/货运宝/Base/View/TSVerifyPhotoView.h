//
//  TSVerifyPhotoView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSVerifyPhotoView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *photo1;
@property (weak, nonatomic) IBOutlet UIImageView *photo2;
@property (weak, nonatomic) IBOutlet UIImageView *photo3;
@property (weak, nonatomic) IBOutlet UIImageView *photo4;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *photoTap1;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *photoTap2;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *photoTap3;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *photoTap4;

@property (weak, nonatomic) IBOutlet UILabel *textLbl1;
@property (weak, nonatomic) IBOutlet UILabel *textLbl2;
@property (weak, nonatomic) IBOutlet UILabel *textLbl3;
@property (weak, nonatomic) IBOutlet UILabel *textLbl4;


@property (nonatomic, assign) BOOL isStation;
+ (instancetype)instanceView;
@end
