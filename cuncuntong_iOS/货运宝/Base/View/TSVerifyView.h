//
//  TSVerifyView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSVerifyView : UIView
@property (weak, nonatomic) IBOutlet UILabel *cityLbl;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *idField;
@property (weak, nonatomic) IBOutlet UITextField *contactNameField;
@property (weak, nonatomic) IBOutlet UITextField *contactPhoneField;
@property (weak, nonatomic) IBOutlet UITextField *addressField;
@property (weak, nonatomic) IBOutlet UIButton *locateBtn;
@property (weak, nonatomic) IBOutlet UITextField *carNumField;
@property (weak, nonatomic) IBOutlet UILabel *carTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *carHeavyLbl;
@property (weak, nonatomic) IBOutlet UILabel *carLengthLbl;

@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *carView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carTopMargin;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *carTypeTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *chooseCityTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *carHeavyTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *carLengthTap;
+ (instancetype)instanceView ;
@end
