//
//  TSVerifyPhotoView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSVerifyPhotoView.h"

@implementation TSVerifyPhotoView

+ (instancetype)instanceView {
    TSVerifyPhotoView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    return view;
}
- (void)setIsStation:(BOOL)isStation {
    if(isStation){
        self.textLbl1.text = @"身份证正面";
        self.textLbl2.text = @"身份证反面";
        self.textLbl3.text = @"手持身份证";
        self.textLbl4.text = @"营业执照";        
    }
}
@end
