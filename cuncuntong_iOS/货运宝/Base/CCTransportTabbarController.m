//
//  CCTransportTabbarController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCTransportTabbarController.h"
#import "NavigationViewController.h"
#import "TSMessageController.h"
#import "TSHelpController.h"
#import "TSTypeModel.h"
#import "CCChangeHomeView.h"
#import "SystemNewsController.h"

@interface CCTransportTabbarController ()
@property (nonatomic, strong) UIButton *leftButton;
@property (strong, nonatomic) PathDynamicModal *animateModel;

@end

@implementation CCTransportTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isDriver = [UserManager sharedInstance].tsRole == TSRoleDriver;
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.navigationItem.title = @"货运通";
    [self initSelf];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    App_Delegate.transportTabbarVc = self;
    [self.rightBtn setTitle:@"切换首页" forState:UIControlStateNormal];
    
    self.carTypeArray = [NSMutableArray array];
     self.carHeaveArray = [NSMutableArray array];
     self.carLengthArray = [NSMutableArray array];
    self.downloadTypeArray = [NSMutableArray array];
    
    [self loadTypeData];
    self.voiceReader = [[TSVoiceReader alloc]init];
}

- (void)initSelf {
    NSArray *vcArr;
    NSArray *imageArr;
    NSArray *selImageArr;
    NSArray *titleArr;
    if (self.isDriver) {
        vcArr = @[@"TSHomeController",
                  @"TSMyOrderController",
                  @"TSProfileController"];
        
        imageArr = @[@"btn_source_normal",
                     @"btn_order_normal",
                     @"btn_mine_normal"];
        
        selImageArr = @[@"btn_source_selected",
                        @"btn_order_selected",
                        @"btn_mine_selected"];
        
        titleArr = @[@"当天货源",
                     @"我的订单",
                     @"我的主页"];
    }else{
        vcArr = @[@"TSHomeController",
                           @"TSPostController",
                           @"TSMyOrderController",
                           @"TSProfileController"];
        
        imageArr = @[@"btn_source_normal",
                              @"btn_release_normal",
                              @"btn_order_normal",
                              @"btn_mine_normal"];
        
        selImageArr = @[@"btn_source_selected",
                                 @"btn_release_selected",
                                 @"btn_order_selected",
                                 @"btn_mine_selected"];
        
        titleArr = @[@"当天货源",
                     @"发布货源",
                     @"我的订单",
                     @"我的主页"];
    }
    
    NSMutableArray *tabArrs = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < vcArr.count; i++) {
        UIViewController *vc ;
        if ( !App_Delegate.defaultHome ||  [App_Delegate.defaultHome isEqualToString:@"default"]) {
            vc = [[NSClassFromString(vcArr[i]) alloc] init];
            if ([vcArr[i] isEqualToString:@"TSMyOrderController"]) {
                self.orderVc = (TSMyOrderController *)vc;
            }
        }else{
            UIViewController *vvcc = [[NSClassFromString(vcArr[i]) alloc] init];
            vc = [[NavigationViewController alloc] initWithRootViewController:vvcc];
            if ([vcArr[i] isEqualToString:@"TSMyOrderController"]) {
                self.orderVc = (TSMyOrderController *)vvcc;
            }
        }
        
//        UIViewController *vc = [[NSClassFromString(vcArr[i]) alloc] init];
        vc.tabBarItem.image = [[UIImage imageNamed:imageArr[i]]
                               imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selImageArr[i]]
                                       imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.title = titleArr[i];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x999999)} forState:UIControlStateNormal];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x55ba97)} forState:UIControlStateSelected];

       
        
        [tabArrs addObject:vc];
    }
    
    // 解决push/pop黑影
    self.view.backgroundColor = MTRGB(0xf7f7f7);
    self.tabBar.tintColor = [UIColor greenColor];
    self.viewControllers = tabArrs;
    
}
- (void)setSelectIndex:(NSInteger)index {
    self.selectedIndex = index;
}
- (void)setRightTitle:(NSString *)rightTitle {
    _rightTitle = rightTitle;
    [self.rightBtn setTitle:rightTitle forState:UIControlStateNormal];
}

-(void)backMove {
    if ([App_Delegate.window.rootViewController isKindOfClass:[self class]]) {
        App_Delegate.defaultHome = @"default";
        [App_Delegate setRootViewController];
    }else{
        UINavigationController *nav = self.selectedViewController;
        if ([self.selectedViewController isKindOfClass:[UINavigationController class]] &&  nav.viewControllers.count > 1) {
            [nav popViewControllerAnimated:YES];
        }else{
            [App_Delegate.tabbarVc gotoHome];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}
- (void)rightBtnClick {
    if ([self.rightTitle containsString:@"消息"]) {
        SystemNewsController *vc = [[SystemNewsController alloc]init];
        vc.type = @"1";
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([self.rightTitle isEqualToString:@"帮助"]){
        TSHelpController *vc = [[TSHelpController alloc]init];
        if (self.carOwnerModel) {
            vc.userDistrictId = self.carOwnerModel.area;
        }else if(self.stationModel){
            vc.userDistrictId = self.stationModel.area;
        }
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([self.rightTitle isEqualToString:@"切换首页"]){
        [self changeRootView];
    }
}
- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_leftButton addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        self.navigationItem.leftBarButtonItem = left;
    }
    return _leftButton;
}
- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,10, 60, 20)];
        [_rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _rightBtn.titleLabel.font = FFont(13);
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:_rightBtn];
        self.navigationItem.rightBarButtonItem = right;
        _rightBtn.titleLabel.contentMode = UIViewContentModeRight;
    }
    return _rightBtn;
}

- (TSNavigationer *)navigator {
    if (!_navigator) {
        _navigator = [[TSNavigationer alloc]init];
    }
    return _navigator;
}
- (void)loadTypeData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_car_type") params:@{@"r_id":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            
            for (NSDictionary *dict in success[@"data"]) {
                TSTypeModel *model = [TSTypeModel modelWithJSON:dict];
                if (model.level == 1) {
                    [self.carTypeArray addObject:model];
                }else if (model.level == 2){
                    [self.carHeaveArray addObject:model];
                }else if (model.level == 3){
                    [self.carLengthArray addObject:model];
                }else if (model.level == 4){
                    [self.downloadTypeArray addObject:model];
                }
            }
        }
    } failure:^(NSError *failure) {
    }];
}

- (void)updateRightTitle {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"main/unreadmsg") params:@{@"id":USERID,@"msg_type":@"1"} target:self success:^(NSDictionary *success) {
        NSInteger count = [success[@"msg"] integerValue];
        self.messageCount = count;
        if (self.messageCount > 0) {
            NSString *text = [NSString stringWithFormat:@"消息(%zd)",self.messageCount];
            [self setRightTitle:text];
        }else{
            [self setRightTitle:@"消息"];
        }
        
    } failure:^(NSError *failure) {
    }];
    
}

//切换首页
- (void)changeRootView {
    CCChangeHomeView *view = [CCChangeHomeView instanceView];
    view.frame = CGRectMake(0, 0, 260, 155);
    view.title = @"货运宝";
    
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}
@end
