//
//  TSNavigationController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSNavigationer.h"
#import <AMapNaviKit/AMapNaviKit.h>

@interface TSNavigationer ()<AMapNaviCompositeManagerDelegate>
@property (nonatomic, strong) AMapNaviCompositeManager *compositeManager;
@property (nonatomic, strong) AMapNaviLocation *lastLocation;
@end

@implementation TSNavigationer


- (AMapNaviCompositeManager *)compositeManager {
    if (!_compositeManager) {
        _compositeManager = [[AMapNaviCompositeManager alloc] init];  // 初始化
        _compositeManager.delegate = self;  // 如果需要使用AMapNaviCompositeManagerDelegate的相关回调（如自定义语音、获取实时位置等），需要设置delegate
    }
    return _compositeManager;
}
- (void)navigationWithStart:(CLLocationCoordinate2D)start startName:(NSString *)startName end:(CLLocationCoordinate2D)end endName:(NSString *)endName{
    AMapNaviCompositeUserConfig *config = [[AMapNaviCompositeUserConfig alloc] init];
    
    AMapNaviVehicleInfo *info = [[AMapNaviVehicleInfo alloc] init];
//    info.vehicleId = @"京N66Y66";
    info.type = 1;  // 设置车辆类型为货车
//    info.size = 4;
//    info.height = 3.9;
//    info.length = 15;
//    info.weight = 50;
//    info.load = 45;
//    info.width = 3;
//    info.axisNums = 6;
    [config setVehicleInfo:info];
    
    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeStart location:[AMapNaviPoint locationWithLatitude:start.latitude longitude:start.longitude] name:startName POIId:nil];
    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeEnd location:[AMapNaviPoint locationWithLatitude:end.latitude longitude:end.longitude] name:endName POIId:nil];
    
    [self.compositeManager presentRoutePlanViewControllerWithOptions:config];
}




#pragma mark - AMapNaviCompositeManagerDelegate

// 发生错误时,会调用代理的此方法
- (void)compositeManager:(AMapNaviCompositeManager *)compositeManager error:(NSError *)error {
    NSLog(@"error:{%ld - %@}", (long)error.code, error.localizedDescription);
}

// 算路成功后的回调函数,路径规划页面的算路、导航页面的重算等成功后均会调用此方法
- (void)compositeManagerOnCalculateRouteSuccess:(AMapNaviCompositeManager *)compositeManager {
    NSLog(@"onCalculateRouteSuccess,%ld",(long)compositeManager.naviRouteID);
}

// 算路失败后的回调函数,路径规划页面的算路、导航页面的重算等失败后均会调用此方法
- (void)compositeManager:(AMapNaviCompositeManager *)compositeManager onCalculateRouteFailure:(NSError *)error {
    NSLog(@"onCalculateRouteFailure error:{%ld - %@}", (long)error.code, error.localizedDescription);
}

// 开始导航的回调函数
- (void)compositeManager:(AMapNaviCompositeManager *)compositeManager didStartNavi:(AMapNaviMode)naviMode {
    NSLog(@"didStartNavi,%ld",(long)naviMode);
}

// 当前位置更新回调
- (void)compositeManager:(AMapNaviCompositeManager *)compositeManager updateNaviLocation:(AMapNaviLocation *)naviLocation {
    NSLog(@"updateNaviLocation,%@",naviLocation);
    if (ABS(naviLocation.coordinate.latitude - self.lastLocation.coordinate.latitude) > 0.00001 || ABS(naviLocation.coordinate.longitude - self.lastLocation.coordinate.longitude) > 0.00001 ) {
        [self updateLocationWithLocation:naviLocation];
    }
    
}

// 导航到达目的地后的回调函数
- (void)compositeManager:(AMapNaviCompositeManager *)compositeManager didArrivedDestination:(AMapNaviMode)naviMode {
    NSLog(@"didArrivedDestination,%ld",(long)naviMode);
}

- (void)updateLocationWithLocation:(AMapNaviLocation *)location {
    self.lastLocation = location;
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/update_location") params:@{@"r_id":USERID,@"lat":@(location.coordinate.latitude),@"lon":@(location.coordinate.longitude)} target:nil success:^(NSDictionary *success) {
    } failure:^(NSError *failure) {
    }];
}

@end
