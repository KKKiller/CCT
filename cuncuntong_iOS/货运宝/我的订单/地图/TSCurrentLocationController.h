//
//  TSCurrentLocationController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/24.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSCurrentLocationController : BaseViewController
@property (nonatomic, assign) CLLocationCoordinate2D location;
@property (nonatomic, strong) NSString *driverId;

@end
