//
//  TSCurrentLocationController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/24.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "TSCurrentLocationController.h"
#import <MAMapKit/MAMapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "AnnotationModel.h"
#import "TSCarModel.h"

@interface TSCurrentLocationController()<CLLocationManagerDelegate,MKMapViewDelegate>
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) MKAnnotationView *annotation;
@property (nonatomic, strong) TSCarModel *carModel;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) AnnotationModel *anno;


@end
@implementation TSCurrentLocationController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupMapView];
    [self addTitle:@"司机实时位置"];
    WEAKSELF
    self.timer = [NSTimer timerWithTimeInterval:5 block:^(NSTimer * _Nonnull timer) {
        [weakSelf getCarCurrentLocation];
    } repeats:YES];
    [[NSRunLoop currentRunLoop]addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)getCarCurrentLocation {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_owner_info") params:@{@"r_id":STR(self.driverId)} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            self.carModel = [TSCarModel modelWithJSON: success[@"data"][@"car"]];
            self.location = CLLocationCoordinate2DMake([self.carModel.lat floatValue], [self.carModel.lon floatValue]);
            [self addAnno];
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)setupMapView {
    _manager = [[CLLocationManager alloc]init];
    _manager.delegate = self;
    [_manager requestAlwaysAuthorization];
    [_manager startUpdatingLocation];
    
    
    _mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height  - 64)];
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
    //    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    
    [self.view addSubview:self.mapView];
    _mapView.delegate = self;
    
    
    _manager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self addAnno];
}

- (void)addAnno {
    if (self.anno) {
        [self.mapView removeAnnotation:self.anno];
    }
    self.anno = [[AnnotationModel alloc]init];
    self.anno.coordinate = self.location;
    self.anno.iconName = @"arrow_location";
    self.anno.title = @"司机当前位置";
    [_mapView addAnnotation:self.anno];
    
    MKCoordinateSpan theSpan;
    theSpan.latitudeDelta= 0.1;
    theSpan.longitudeDelta= 0.1;
    MKCoordinateRegion theRegion;
    theRegion.center= self.location;
    theRegion.span= theSpan;
    [self.mapView setRegion:theRegion animated:YES];
    [self.mapView regionThatFits:theRegion];
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:   (id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    _annotation = [self.mapView dequeueReusableAnnotationViewWithIdentifier:@"ID"];
    
    if (_annotation == nil) {
        _annotation = [[MKAnnotationView alloc]initWithAnnotation:annotation   reuseIdentifier:@"ID"];
    }
    _annotation.canShowCallout = YES;
    // 根据模型对象，决定本大头针显示的图片样式。
    _annotation.image = [UIImage imageNamed:@"arrow_location"];
//    _annotation.
    return _annotation;
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"--%f",manager.location.coordinate.latitude);
    NSLog(@"--%f",manager.location.coordinate.longitude);
    
}
@end
