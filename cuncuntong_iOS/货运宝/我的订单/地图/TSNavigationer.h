//
//  TSNavigationController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//


#import <CoreLocation/CoreLocation.h>
@interface TSNavigationer : NSObject
- (void)navigationWithStart:(CLLocationCoordinate2D)start startName:(NSString *)startName end:(CLLocationCoordinate2D)end endName:(NSString *)endName;
@end
