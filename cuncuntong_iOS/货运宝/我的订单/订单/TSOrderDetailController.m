//
//  TSOrderDetailController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSOrderDetailController.h"
#import "TSHomeCell.h"
#import "TSNamePhoneView.h"
#import "TSCommentView.h"
#import "TSTransportLineontroller.h"
#import "TSOrderInfo.h"
#import "TSCurrentLocationController.h"
#import "TSCarModel.h"
#import "RechargeViewController.h"
@interface TSOrderDetailController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *topLbl;
@property (nonatomic, strong) TSHomeCell *topCell;
@property (nonatomic, strong) UIButton *showDetailBtn;
@property (nonatomic, strong) TSNamePhoneView *namePhoneView;
@property (nonatomic, strong) TSCommentView *commentView;
@property (nonatomic, strong) TSOrderInfo *orderInfo;
@property (nonatomic, strong) TSCarModel *carModel;
@property (nonatomic, assign) BOOL isSameCityOrder;
@property (nonatomic, assign) BOOL showBtn;
@end

@implementation TSOrderDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单信息";
    self.view.backgroundColor = TSGRAY;
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
    
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.topCell];
    //派单中：司机显示确认订单   交易中：货运站查看司机位置 司机确认装货  已完成：隐藏
    self.showBtn = (self.orderState == TSOrderStateSending && App_Delegate.transportTabbarVc.isDriver) || self.orderState == TSOrderStateTrading;
    if (self.showBtn) {
        [self.scrollView addSubview:self.showDetailBtn];
    }
    
    if (self.orderState != TSOrderStateSending) {
        [self.scrollView addSubview:self.namePhoneView];
        [self.scrollView addSubview:self.commentView];
    }
    CGFloat offset = self.orderState != TSOrderStateSending ? CGRectGetMaxY(self.commentView.frame) : CGRectGetMaxY(self.topCell.frame);
    self.scrollView.contentSize = CGSizeMake(App_Width, offset + 20);
    if (!App_Delegate.transportTabbarVc.isDriver  && self.orderState == TSOrderStateTrading) {
        UIButton *rightBtn = [[UIButton alloc]initWithTitle:@"完成订单" textColor:WHITECOLOR backImg:nil font:14];
        rightBtn.frame = CGRectMake(0, 10, 60, 40);
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
        [rightBtn addTarget:self action:@selector(finishOrder) forControlEvents:UIControlEventTouchUpInside];
    }

    [self loadData];
}
- (void)updateFrame {
    if ([self.orderInfo.order_state isEqualToString:@"3"]) {
        self.topLbl.hidden = YES;
        self.namePhoneView.y = CGRectGetMaxY(self.showDetailBtn.frame) + 10;
        self.commentView.y = CGRectGetMaxY(self.namePhoneView.frame)+ 10;
    }else if ([self.orderInfo.order_state isEqualToString:@"2"]){
        self.topLbl.hidden = NO;
        self.namePhoneView.y = CGRectGetMaxY(self.topLbl.frame) + 10;
        self.commentView.y = CGRectGetMaxY(self.namePhoneView.frame)+ 10;
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
    
}

- (void)setDetailBtnTitle {
    NSString *title = @"";
    if (App_Delegate.transportTabbarVc.isDriver) {
        title = self.orderState == TSOrderStateSending ? @"确认订单" : [self.orderInfo.order_state isEqualToString:@"2"] ? @"确认装货" : @"查看实时位置";
    }else{
        title =  [self.orderInfo.order_state isEqualToString:@"2"] ? @"联系司机装货" : @"查看货物实时位置";
    }
    [self.showDetailBtn setTitle:title forState:UIControlStateNormal];
}

- (void)loadData {
    NSString *url = App_Delegate.transportTabbarVc.isDriver ? @"car/get_order_info" : @"owner/get_news";
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{@"order_id":STR(self.orderId),@"r_id":self.userId ?: USERID}];
    [param setValue:STR(App_Delegate.lat) forKey:@"lat"];
    [param setValue:STR(App_Delegate.lng) forKey:@"lon"];
    if (App_Delegate.transportTabbarVc.isEditing) {
    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(url) params:param target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            NSDictionary *data = success[@"data"];
            self.orderInfo = [TSOrderInfo modelWithJSON:data[@"data"]];
            self.isSameCityOrder = [self.orderInfo.destination_city_name isEqualToString:self.orderInfo.location_city_name];
            [self getCarCurrentLocation];
            if (self.showBtn) {
                [self setDetailBtnTitle];
            }
            if ( [self.orderInfo.order_state isEqualToString:@"2"]) { //确认装货显示文字提示
                [self.scrollView addSubview:self.topLbl];
            }
            self.topCell.model = self.orderInfo;
            self.namePhoneView.model = self.orderInfo;
            [self updateFrame];
        }else{
            SHOW(@"获取订单信息失败");
        }
    } failure:^(NSError *failure) {
        
    }];
}


- (void)comment {
    NSString *type = self.commentView.commentTag == 1 ? @"1" : self.commentView.commentTag == 2 ? @"3": @"5";
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/order_comment") params:@{@"order_id":STR(self.orderId),@"r_id":USERID,@"comment":@"",@"type":type} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            SHOW(@"评论成功");
            [self.navigationController popViewControllerAnimated:YES];
            [App_Delegate.transportTabbarVc.orderVc gotoIndex:2];
            [App_Delegate.transportTabbarVc.orderVc refreshData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (void)getCarCurrentLocation {
    if (self.orderInfo.r_car_id) {
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_owner_info") params:@{@"r_id":STR(self.orderInfo.r_car_id)} target:self success:^(NSDictionary *success) {
            if ([success[@"error"]  isEqual: @(0)]) {
                self.carModel = [TSCarModel modelWithJSON: success[@"data"][@"car"]];
            }
        } failure:^(NSError *failure) {
        }];
    }
}

#pragma mark - click
- (void)showDetailBtnClick {
    if (App_Delegate.transportTabbarVc.isDriver) {
        if( self.orderState == TSOrderStateSending){//确认订单
            [self confirmOrder];
        } else if([self.orderInfo.order_state isEqualToString:@"2"]){//确认装货
            [self confirnOnload];
            [self updateFrame];
        } else if ([self.orderInfo.order_state isEqualToString:@"3"]){//查看实时位置
            [self currentLocation];
        }else{
            SHOW(@"订单已结束，请在我的订单中查看");
            [self.navigationController popViewControllerAnimated:YES];
            [App_Delegate.transportTabbarVc.orderVc gotoIndex:2];
            [App_Delegate.transportTabbarVc.orderVc refreshData];
        }
    }else{
        if ([self.orderInfo.order_state isEqualToString:@"2"]) {//联系司机装货
            [self contactDirver];
            [self updateFrame];
        }else if ([self.orderInfo.order_state isEqualToString:@"3"]){//查看实时位置
            [self currentLocation];
        }else {
            SHOW(@"订单已结束，请在我的订单中查看");
            [self.navigationController popViewControllerAnimated:YES];
            [App_Delegate.transportTabbarVc.orderVc gotoIndex:2];
            [App_Delegate.transportTabbarVc.orderVc refreshData];
        }
    }
    
}
//司机确认订单
- (void)confirmOrder {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/affirm_order") params:@{@"r_id":USERID,@"order_id":STR(self.orderInfo.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            SHOW(@"确认成功");
            [self.navigationController popViewControllerAnimated:YES];
            [App_Delegate.transportTabbarVc.orderVc gotoIndex:1];
            [App_Delegate.transportTabbarVc.orderVc refreshData];
        }else{
//            SHOW(@"订单已过15分钟期限，无法确认订单");
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
        [self loadData];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
//联系司机确认装货
- (void)contactDirver {
    NSString *phone = self.orderInfo.che_phone_2;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"联系司机装货" message:phone preferredStyle:1];
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [self presentViewController:alertController animated:YES completion:nil];
}
//确认装货
- (void)confirnOnload {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/affirm_loading") params:@{@"r_id":USERID,@"order_id":STR(self.orderInfo.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            SHOW(@"确认装货成功");
        }else{
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
        [self loadData];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
//查看实时位置
- (void)currentLocation {
    if (self.carModel.lat) {
        TSCurrentLocationController *vc = [[TSCurrentLocationController alloc]init];
        vc.location = CLLocationCoordinate2DMake([self.carModel.lat floatValue], [self.carModel.lon floatValue]);
        vc.driverId = self.carModel.driver_id;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        SHOW(@"位置更新中，请稍后查看");
    }
    
//    CLLocationCoordinate2D start = CLLocationCoordinate2DMake(39.846414, 116.382766);
//    CLLocationCoordinate2D end = CLLocationCoordinate2DMake(39.82414, 116.312766);
//    [App_Delegate.transportTabbarVc.navigator navigationWithStart:start startName:@"起点" end:end endName:@"终点"];
}
//完成订单
- (void)finishOrder {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"owner/order_accomplish") params:@{@"r_id":USERID,@"order_id":STR(self.orderInfo.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            SHOW(@"确认成功,请对司机进行评价");
            [self.navigationController popViewControllerAnimated:YES];
            [App_Delegate.transportTabbarVc.orderVc gotoIndex:2];
            [App_Delegate.transportTabbarVc.orderVc refreshData];
        }else{
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
        [self loadData];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
- (void)contact1 {
    NSString *phone = App_Delegate.transportTabbarVc.isDriver ? self.orderInfo.reader_mobile : self.orderInfo.che_user_mobile;
    [self contact:phone];
}
- (void)contact2 {
    NSString *phone = App_Delegate.transportTabbarVc.isDriver ? self.orderInfo.hz_phone_2 : self.orderInfo.che_phone_2;
    [self contact:phone];
}
- (void)contact:(NSString *)phone {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"联系对方" message:phone preferredStyle:1];
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [self presentViewController:alertController animated:YES completion:nil];
}









///////////////////
#pragma 推送跳转

//导航
- (void)navBtnClick:(UIButton *)sender {
    CLLocationCoordinate2D start = CLLocationCoordinate2DMake([self.orderInfo.location_lat doubleValue], [self.orderInfo.location_lon doubleValue]);
    CLLocationCoordinate2D end =  CLLocationCoordinate2DMake([self.orderInfo.destination_lat doubleValue], [self.orderInfo.destination_lon doubleValue]);
    [App_Delegate.transportTabbarVc.navigator  navigationWithStart:start startName:@"订单起点" end:end endName:@"订单终点"];
}
//打电话按钮
- (void)makePhoneCall:(UIButton *)sender {
    if (self.orderState == TSOrderStateTrading || self.orderState == TSOrderStateFinish) {
        return;
    }
    if([[UserManager sharedInstance].money floatValue] > 0){
        [self moneyEnoughAlert];
    }else{
        [self noMoneyAlert];
        
    }
}

//余额不足
- (void)noMoneyAlert {
    NSInteger money = self.isSameCityOrder ? 25 : 45;
    NSString *message = [NSString stringWithFormat:@"余额不足%zd元，无法抢单，请充值",money];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(4, 2)];
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 16)];
    [alert setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"现在充值" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        RechargeViewController *vc = [[RechargeViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [action2 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action1 setValue:MTRGB(0x666666) forKey:@"titleTextColor"];
    [self presentViewController:alert animated:YES completion:nil];
}
//余额足够
- (void)moneyEnoughAlert {
    NSInteger money = self.isSameCityOrder ? 25 : 45;
    NSString *message = [NSString stringWithFormat:@"联系成功需支付%zd元信息服务费不成功不收费，是否马上联系货主？",money];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(7, 2)];
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 31)];
    [alert setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self contactWithDriver];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [action2 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action1 setValue:MTRGB(0x777777) forKey:@"titleTextColor"];
    [self presentViewController:alert animated:YES completion:nil];
}
//联系货主
- (void)contactWithDriver {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/ring_up") params:@{@"r_id":USERID,@"order_id":STR(self.orderInfo.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            NSString *phone = success[@"data"][@"data"][@"phone_2"];
            if (phone.length != 11) {
                phone = success[@"data"][@"data"][@"phone_1"];
            }
            NSString *state = success[@"data"][@"state"][@"msg"];
            if ([state isEqualToString:@"可以抢"]) {
                [self phoneConsult:phone];
            }else{
                SHOW(@"订单暂时被锁定，请稍后再试");
            }
        }else{
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}

//电话咨询
- (void)phoneConsult:(NSString *)phone {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"联系货主" message:phone preferredStyle:1];
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                    
                                    [self performSelector:@selector(lockOrder) withObject:nil afterDelay:0.5];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
        [self cancelOrderLock];
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [self presentViewController:alertController animated:YES completion:nil];
}
//确认订单
- (void)lockOrder {
    NSInteger money = self.isSameCityOrder ? 25 : 45;
    NSString *message = [NSString stringWithFormat:@"如谈妥，请确认订单，确认订单将扣费%zd元，不确认订单会被别人抢单而且无法确认装货",money];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(17, 2)];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:MTRGB(0xFF9012) range:NSMakeRange(26, 6)];

    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 40)];
    [alert setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self cancelOrderLock];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确认订单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self comfirmOrderData];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [action2 setValue:MTRGB(0x55ba97) forKey:@"titleTextColor"];
    [action1 setValue:MTRGB(0x666666) forKey:@"titleTextColor"];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)comfirmOrderData {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/affirm_order") params:@{@"r_id":USERID,@"order_id":STR(self.orderInfo.id)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            SHOW(@"订单确认成功");
        }else{
            SHOW(success[@"msg"]);
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
- (void)cancelOrderLock {
    //解除订单锁定
}





#pragma 懒加载


- (TSHomeCell *)topCell {
    if (!_topCell) {
//        CGFloat y = self.orderState == TSOrderStateTrading ? 44 : 0;
        _topCell = [[NSBundle mainBundle] loadNibNamed:@"TSHomeCell" owner:nil options:nil].firstObject;
        _topCell.frame = CGRectMake(0, 0, App_Width, 120);
        [_topCell.locateBtn addTarget:self action:@selector(navBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_topCell.phoneBtn addTarget:self action:@selector(makePhoneCall:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _topCell;
}
- (UIButton *)showDetailBtn {
    if (!_showDetailBtn) {
        _showDetailBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.topCell.frame) + 10, App_Width - 20, 34)];
        _showDetailBtn.layer.borderColor = MAINGREEN.CGColor;
        _showDetailBtn.layer.cornerRadius = 4;
        _showDetailBtn.backgroundColor = MTRGB(0x6db898);
        _showDetailBtn.layer.masksToBounds = YES;
        _showDetailBtn.layer.borderWidth = 0.5;
        _showDetailBtn.titleLabel.font = FFont(12);
        [_showDetailBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        [_showDetailBtn addTarget:self action:@selector(showDetailBtnClick) forControlEvents:UIControlEventTouchUpInside];

    }
    return _showDetailBtn;
}
- (UILabel *)topLbl {
    if (!_topLbl) {
        _topLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.showDetailBtn.frame)+10, App_Width, 20)];
        NSString *text = @"确认装货后奖励货主5元";
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
        [attributedText addAttribute:NSForegroundColorAttributeName value:MTRGB(0x666666) range:NSMakeRange(0, text.length)];
        [attributedText addAttribute:NSForegroundColorAttributeName value:MTRGB(0xEE9393) range:NSMakeRange(9, 1)];
        [attributedText addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, 11)];
        _topLbl.attributedText = attributedText;
//        if ([UserManager sharedInstance].tsRole  == TSRoleDriver) {
//            _topLbl.text = @"确认装货后奖励货主5元";
//        }else{
//            _topLbl.text = @"装货后请让司机点击确认装货\n确认后您将获得5元奖励!";
//        }
        _topLbl.textAlignment = NSTextAlignmentCenter;
//        _topLbl.textColor = MTRGB(0x666666);
//        _topLbl.numberOfLines = 0;
//        _topLbl.font = FFont(12);

    }
    return _topLbl;
}
- (TSNamePhoneView *)namePhoneView {
    if (!_namePhoneView) {
        CGFloat y = self.showBtn ? CGRectGetMaxY(self.showDetailBtn.frame)  : CGRectGetMaxY(self.topCell.frame);
        if ([self.orderInfo.order_state isEqualToString:@"2"]) {
            y = CGRectGetMaxY(self.topLbl.frame) + 10;
        }
        _namePhoneView  = [TSNamePhoneView instanceView];
        _namePhoneView.frame = CGRectMake(10,y + 10, App_Width -20, 90);
        _namePhoneView.layer.cornerRadius = 4;
        _namePhoneView.layer.masksToBounds = YES;
        [_namePhoneView.phone1Tap addTarget:self action:@selector(contact1)];
        [_namePhoneView.phone2Tap addTarget:self action:@selector(contact2)];
    }
    return _namePhoneView;
}
- (TSCommentView *)commentView {
    if (!_commentView) {
        _commentView = [TSCommentView instanceView];
        _commentView.frame = CGRectMake(10, CGRectGetMaxY(self.namePhoneView.frame) + 10, App_Width - 20, 160);
        _commentView.layer.cornerRadius = 4;
        _commentView.layer.masksToBounds = YES;
        [_commentView.submitBtn addTarget:self action:@selector(comment) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commentView;
}
@end
