//
//  TSOrderCompleteController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSOrderCompleteController.h"
#import "TSOrderDetailController.h"
#import "TSOrderInfo.h"
#import "TSHomeCell.h"
static NSString *cellID = @"TSHomeCell3";
@interface TSOrderCompleteController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, assign) BOOL loading;

@end

@implementation TSOrderCompleteController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    [self initRefreshView];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = self.view.bounds;
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);

}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    if (self.loading) {
        return;
    }
    self.loading = YES;
    //0派单中  2,3交易中  4,5已完成
    NSString *str = App_Delegate.transportTabbarVc.isDriver ? @"car/order_list" : @"owner/get_news_list";
    NSDictionary *dict = @{@"r_id":USERID,@"offset":@(self.dataArray.count)};
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:dict];
    [param setValue:@"4,5" forKey:@"state"];
    [param setValue:STR(App_Delegate.lat) forKey:@"lat"];
    [param setValue:STR(App_Delegate.lng) forKey:@"lon"];
    if (!App_Delegate.transportTabbarVc.isDriver ) {

    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(str) params:param target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            for (NSDictionary *dict in success[@"data"]) {
                TSOrderInfo *orderInfo = [TSOrderInfo modelWithJSON:dict];
                [self.dataArray addObject:orderInfo];

            }
            [self endLoding:success[@"data"]];
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    self.loading = NO;
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TSHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.dataArray.count > indexPath.row) {
        TSOrderInfo *model = self.dataArray[indexPath.row];
        cell.model = model;
        cell.locateBtn.tag = indexPath.row;
        [cell.locateBtn addTarget:self action:@selector(navBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.phoneBtn.tag = indexPath.row + 100;
        [cell.phoneBtn addTarget:self action:@selector(makePhoneCall:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
- (void)navBtnClick:(UIButton *)sender {
    NSInteger index = sender.tag;
    if (self.dataArray.count > index) {
        TSOrderInfo *model = self.dataArray[index];
        CLLocationCoordinate2D start = CLLocationCoordinate2DMake([model.location_lat doubleValue], [model.location_lon doubleValue]);
        CLLocationCoordinate2D end =  CLLocationCoordinate2DMake([model.destination_lat doubleValue], [model.destination_lon doubleValue]);
        [App_Delegate.transportTabbarVc.navigator  navigationWithStart:start startName:@"起点" end:end endName:@"终点"];
    }
}
//打电话按钮
- (void)makePhoneCall:(UIButton *)sender {
    NSInteger index = sender.tag - 100;
    if (self.dataArray.count > index) {
        TSOrderInfo *model = self.dataArray[index];
        NSString *phone;
        if (App_Delegate.transportTabbarVc.isDriver) {
            phone = model.reader_mobile ?: model.hz_phone_2;
        }else{
            phone = model.che_user_mobile ?: model.che_phone_2;
        }
        if (STRINGEMPTY(phone)) {
            return;
        }
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"联系货主" message:phone preferredStyle:1];
        UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                    {
                                        NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                    }];
        //取消栏
        UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
        }];
        [alertController addAction:chooseOne];
        [alertController addAction:cancelOne];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        TSOrderInfo *model = self.dataArray[indexPath.row];
        TSOrderDetailController *vc = [[TSOrderDetailController alloc]init];
        vc.orderId = model.id;
        vc.orderState = TSOrderStateFinish;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 120;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"TSHomeCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 64 - 49 - 45)];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
@end
