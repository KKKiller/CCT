//
//  TSMyOrderController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSMyOrderController.h"
#import "TSOrderPageView.h"
#import "TSOrderSendingController.h"
#import "TSOrderTradingController.h"
#import "TSOrderCompleteController.h"
#import "SystemNewsController.h"
static const CGFloat kLearningPageHeight = 45.0;
//static const CGFloat kLearningTopHeight = 64.0;

@interface TSMyOrderController ()<UIScrollViewDelegate,TSOrderPageViewDelegate>
@property (strong, nonatomic) TSOrderSendingController *sendingVc;
@property (strong, nonatomic) TSOrderTradingController *tradingVc;
@property (strong, nonatomic) TSOrderCompleteController *completeVc;

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) TSOrderPageView *pageView; 
@property (nonatomic, assign) CGFloat kLearningTopHeight;
@property (nonatomic, assign) NSInteger gotoIndex;

@end

@implementation TSMyOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的订单";
    CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
    self.kLearningTopHeight = system >= 11.0 || !App_Delegate.isDefaultRoot ? 64 : 0;
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.pageView];
    self.gotoIndex = -1;
    self.scrollView.backgroundColor = [UIColor purpleColor];
    [self setRightBtn];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [App_Delegate.transportTabbarVc updateRightTitle];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.gotoIndex >= 0) {
        [self pageViewClickAtPageIndex:self.gotoIndex];
        self.gotoIndex = -1;
    }
}
- (void)setRightBtn {
    if (![App_Delegate.defaultHome isEqualToString:@"default"]) {
        UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [rightBtn setTitle:@"消息" forState:UIControlStateNormal];
        rightBtn.titleLabel.font = FFont(14);
        [rightBtn addTarget:self action:@selector(message) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
        self.navigationItem.rightBarButtonItem = right;
    }
}
- (void)message {
    SystemNewsController *vc = [[SystemNewsController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)refreshData {
    [self.sendingVc refreshData];
    [self.tradingVc refreshData];
    [self.completeVc refreshData];
}
- (void)gotoIndex:(NSInteger)index {
//    [self pageViewClickAtPageIndex:index];
//    [self.scrollView setContentOffset:CGPointMake(App_Width*index, 0) animated:YES];
    self.gotoIndex = index;
}

#pragma mark - 代理
- (void)pageViewClickAtPageIndex:(NSInteger)index {
    CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
    CGFloat offset = system >= 11.0 || !App_Delegate.isDefaultRoot ? 0 : 64;
    [self.scrollView setContentOffset:CGPointMake(App_Width*index, -offset) animated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat startX = (App_Width - kLearningPageViewPageWidth*3 - kLearningPageViewMargin*2) * 0.5;
    CGFloat xOffset = (kLearningPageViewMargin + kLearningPageViewPageWidth) * (scrollView.contentOffset.x / App_Width);
    self.pageView.barView.x = startX + xOffset;
    NSArray *colorArray = [TOOL colorWithR1:102 g1:214 b1:166 r2:34 g2:34 b2:34 currentOffset:(float)scrollView.contentOffset.x totlalLength:(float)App_Width];
    if(scrollView.contentOffset.x <= App_Width){
        self.pageView.firstLbl.textColor = [colorArray firstObject];
        self.pageView.secondLbl.textColor = [colorArray lastObject];
        self.pageView.thirdLbl.textColor = TEXTBLACK3;
    }else{
        self.pageView.firstLbl.textColor = TEXTBLACK3;
        self.pageView.secondLbl.textColor = [colorArray firstObject];
        self.pageView.thirdLbl.textColor = [colorArray lastObject];
    }
}

#pragma mark - 懒加载
- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        CGFloat y =  kLearningPageHeight + self.kLearningTopHeight;
        CGFloat height = App_Height - y - 49;
        CGFloat hh = App_Height;
        kLog(@"hh%f %f %f",hh,height,y);
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, y,App_Width, height)];
        _scrollView.delegate = self;
        _scrollView.backgroundColor = WHITECOLOR;
        _scrollView.contentSize = CGSizeMake(App_Width * 3, 0);
        _scrollView.pagingEnabled= YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        
        [self addChildViewController:self.sendingVc];
        [self.scrollView addSubview:self.sendingVc.view];
        [self.scrollView addSubview:self.tradingVc.view];
        [self.scrollView addSubview:self.completeVc.view];
        
    }
    return _scrollView;
}
- (TSOrderSendingController *)sendingVc {
    if (_sendingVc == nil) {
        _sendingVc = [[TSOrderSendingController alloc]init];
//        [self addChildViewController:_sendingVc];
        CGFloat y =  kLearningPageHeight + 64;
        CGFloat height = App_Height - y - 49;
        CGRect frame = CGRectMake(0, 0, App_Width, height);
        _sendingVc.view.frame = frame;
//        _sendingVc.tableView.frame = frame;
        [self addChildViewController:_sendingVc];
    }
    return _sendingVc;
}
- (TSOrderTradingController *)tradingVc {
    if (_tradingVc == nil) {
        _tradingVc = [[TSOrderTradingController alloc]init];
        CGFloat y =  kLearningPageHeight + 64;
        CGFloat height = App_Height - y - 49;
        CGRect frame = CGRectMake(App_Width, 0, App_Width, height);
        _tradingVc.view.frame = frame;
//        _tradingVc.tableView.frame = CGRectMake(0, 0, App_Width, height);
        [self addChildViewController:_tradingVc];
    }
    return _tradingVc;
}
- (TSOrderCompleteController *)completeVc {
    if (_completeVc == nil) {
        _completeVc = [[TSOrderCompleteController alloc]init];
        CGFloat y =  kLearningPageHeight + 64;
        CGFloat height = App_Height - y - 49;
        CGRect frame = CGRectMake(App_Width*2, 0, App_Width, height);
        _completeVc.view.frame = frame;
        _completeVc.tableView.frame = CGRectMake(0, 0, App_Width, height);
        [self addChildViewController:_completeVc];
    }
    return _completeVc;
}

- (TSOrderPageView *)pageView {
    if (_pageView == nil) {
        _pageView = [[TSOrderPageView alloc]init];
        _pageView.delegate = self;
        _pageView.frame = CGRectMake(0, 64, App_Width, kLearningPageHeight);
        [self.view addSubview:_pageView];
    }
    return _pageView;
}


@end
