//
//  TSOrderDetailController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "TSMyOrderController.h"

@interface TSOrderDetailController : BaseViewController
//@property (nonatomic, assign) BOOL showTopLbl;
//@property (nonatomic, assign) BOOL showDetail;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, assign) TSOrderState orderState;
@property (nonatomic, strong) NSString *userId;


@end
