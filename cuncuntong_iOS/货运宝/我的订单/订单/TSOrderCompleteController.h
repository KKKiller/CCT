//
//  TSOrderCompleteController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "TSMyOrderController.h"


@interface TSOrderCompleteController : BaseViewController
@property (strong, nonatomic) UITableView *tableView;

- (void)refreshData;

@end
