//
//  MTLearningPageView.h
//  Mentor
//
//  Created by 我是MT on 2017/4/10.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import <UIKit/UIKit.h>
static const CGFloat kLearningPageViewMargin = 40.0;
static const CGFloat kLearningPageViewPageWidth = 62.0;


@protocol TSOrderPageViewDelegate<NSObject>
- (void)pageViewClickAtPageIndex:(NSInteger)index;
@end
@interface TSOrderPageView : UIView
@property (strong, nonatomic) UILabel *topLineLbl;
@property (strong, nonatomic) UIView *barView;
@property (strong, nonatomic) UILabel *bottomLineLbl;
@property (strong, nonatomic) UILabel *firstLbl;
@property (strong, nonatomic) UILabel *secondLbl;
@property (strong, nonatomic) UILabel *thirdLbl;
@property (weak, nonatomic) id<TSOrderPageViewDelegate> delegate;

@end
