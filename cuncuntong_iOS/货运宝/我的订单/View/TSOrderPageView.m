//
//  MTLearningPageView.m
//  Mentor
//
//  Created by 我是MT on 2017/4/10.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import "TSOrderPageView.h"
@implementation TSOrderPageView

- (instancetype)init {
    if (self = [super init]) {
        [self creatViews];
        [self layoutSubView];
        self.backgroundColor = WHITECOLOR;
    }
    return self;
}


- (void)creatViews {
    self.topLineLbl = [[UILabel alloc]initWithShallowLine];
    [self addSubview:self.topLineLbl];
    self.topLineLbl.hidden = YES;
    
    self.barView = [[UIView alloc]init];
    [self addSubview:self.barView];
    self.barView.backgroundColor = MAINGREEN;
    self.barView.frame = CGRectMake((App_Width-kLearningPageViewPageWidth*3-kLearningPageViewMargin*2)*0.5, 43, kLearningPageViewPageWidth, 2);

    self.firstLbl = [[UILabel alloc]initWithText:@"派单中" font:14 textColor:MAINGREEN];
    [self addSubview:self.firstLbl];
     self.firstLbl.userInteractionEnabled =YES;
    self.firstLbl.tag = 10;
    self.firstLbl.textAlignment = NSTextAlignmentCenter;
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAtPageText:)];
    [self.firstLbl addGestureRecognizer:tap1];
    
    self.secondLbl = [[UILabel alloc]initWithText:@"交易中" font:14 textColor:TEXTBLACK3];
    [self addSubview:self.secondLbl];
    self.secondLbl.tag = 11;
     self.secondLbl.userInteractionEnabled =YES;
    self.secondLbl.textAlignment = NSTextAlignmentCenter;
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAtPageText:)];
    [self.secondLbl addGestureRecognizer:tap2];
    
    self.thirdLbl = [[UILabel alloc]initWithText:@"已完成" font:14 textColor:TEXTBLACK3];
    [self addSubview:self.thirdLbl];
    self.thirdLbl.tag = 12;
    self.thirdLbl.userInteractionEnabled =YES;
    self.thirdLbl.textAlignment = NSTextAlignmentCenter;
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAtPageText:)];
    [self.thirdLbl addGestureRecognizer:tap3];
    
    self.bottomLineLbl = [[UILabel alloc]initWithShallowLine];
    [self addSubview:self.bottomLineLbl];
    
}

-(void)tapAtPageText:(UIGestureRecognizer *)recog{
    NSInteger index = recog.view.tag - 10;
    if ([self.delegate respondsToSelector:@selector(pageViewClickAtPageIndex:)]) {
        [self.delegate pageViewClickAtPageIndex:index];
    }
}

- (void)layoutSubView {
    [self.secondLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.width.mas_equalTo(kLearningPageViewPageWidth);
    }];
    [self.firstLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.secondLbl.mas_left).offset(-kLearningPageViewMargin);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.width.mas_equalTo(kLearningPageViewPageWidth);
    }];
    [self.thirdLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.secondLbl.mas_right).offset(kLearningPageViewMargin);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.width.mas_equalTo(kLearningPageViewPageWidth);
    }];
    [self.topLineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(0.5);
    }];
    [self.bottomLineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.width.mas_equalTo(App_Width);
        make.height.mas_equalTo(0.5);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
@end
