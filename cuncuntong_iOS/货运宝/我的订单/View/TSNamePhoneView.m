//
//  TSNamePhoneView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSNamePhoneView.h"

@implementation TSNamePhoneView

+ (instancetype)instanceView {
    TSNamePhoneView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.imgView.layer.cornerRadius = 25;
    view.imgView.layer.masksToBounds = YES;
    return view;
}
- (void)setModel:(TSOrderInfo *)model {
    _model = model;
    if (App_Delegate.transportTabbarVc.isDriver) {
        self.nameLbl1.text = model.hz_name;
        self.phoneLbl1.text = model.reader_mobile;
        self.phoneLbl2.text = model.hz_phone_2;
        self.nameLbl2.text = model.hz_phone_1;
        NSString *url = [NSString stringWithFormat:@"images/%@",model.reader_portrait];
        url = BASEURL_WITHOBJC (url);
        [self.imgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"head")];
        self.headWidth.constant = 0;
        self.VlineView.hidden = YES;
    }else{
        self.nameLbl1.text = model.che_info_name;
        self.phoneLbl1.text = model.che_user_mobile;
        
        self.phoneLbl2.text = model.che_phone_2;
        self.nameLbl2.text = model.che_phone_1;
        
        NSString *url = [NSString stringWithFormat:@"images/%@",model.che_user_portrait];
        url = BASEURL_WITHOBJC (url);
        [self.imgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"head")];
    }
    
}
@end
