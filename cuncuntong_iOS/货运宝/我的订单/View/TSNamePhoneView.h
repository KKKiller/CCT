//
//  TSNamePhoneView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSOrderInfo.h"
@interface TSNamePhoneView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl1;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl1;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl2;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl2;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *phone1Tap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *phone2Tap;
@property (weak, nonatomic) IBOutlet UIView *VlineView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headWidth;
@property (nonatomic, strong) TSOrderInfo *model;
+ (instancetype)instanceView;
@end
