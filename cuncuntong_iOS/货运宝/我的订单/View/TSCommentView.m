//
//  TSCommentView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSCommentView.h"

@implementation TSCommentView

+ (instancetype)instanceView {
    TSCommentView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    [view.goodBtn addTarget:view action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.normalBtn addTarget:view action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.badBtn addTarget:view action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    view.textView.delegate = view;
    return view;
}

- (void)btnClick:(UIButton *)sender {
    BOOL selected = sender.selected;
    self.goodBtn.selected  = NO;
    self.normalBtn.selected  = NO;
    self.badBtn.selected  = NO;

    sender.selected = !selected;
    self.commentTag = sender.tag;
}

- (void)textViewDidChange:(UITextView *)textView {
    self.placeHolderLbl.hidden = textView.text.length != 0;
}

@end
