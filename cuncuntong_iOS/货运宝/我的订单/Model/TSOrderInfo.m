//
//  TSOrderInfo.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSOrderInfo.h"
@implementation TSOrderStateModel

@end

@implementation TSOrderInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"beizhu" : @"else"};
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"car_type3_name" : [TSTypeModel class],@"car_type1_name" : [TSTypeModel class]};
}
- (NSString *)getCarType {
    NSString *text = @"";
    for (TSTypeModel *model in self.car_type1_name) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@,",model.type_name]];
    }
    if (text.length > 0) {
        text = [text substringToIndex:text.length - 1];
    }
    return text;
}
- (NSString *)getCarLength {
    NSString *text = @"";
    for (TSTypeModel *model in self.car_type3_name) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@,",model.type_name]];
    }
    if (text.length > 0) {
        text = [text substringToIndex:text.length - 1];
    }
    return text;
}
@end
