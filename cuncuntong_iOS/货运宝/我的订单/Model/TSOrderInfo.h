//
//  TSOrderInfo.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSTypeModel.h"
@interface TSOrderStateModel : NSObject
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSString *r_id;
@property (nonatomic, strong) NSString *cost;
@end
@interface TSOrderInfo : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *r_id;
@property (nonatomic, strong) NSString *car_type1;
@property (nonatomic, strong) NSString *car_type2;
@property (nonatomic, strong) NSString *car_type3;
@property (nonatomic, strong) NSArray<TSTypeModel *> *car_type1_name;
@property (nonatomic, strong) NSString *car_type2_name;
@property (nonatomic, strong) NSArray<TSTypeModel *> *car_type3_name;
@property (nonatomic, strong) NSString *car_type4;

@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) NSString *location_province;
@property (nonatomic, strong) NSString *location_city;
@property (nonatomic, strong) NSString *location_district;
@property (nonatomic, strong) NSString *location_address;
@property (nonatomic, strong) NSString *address_to;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *location_lat;
@property (nonatomic, strong) NSString *location_lon;
@property (nonatomic, strong) NSString *destination_lat;
@property (nonatomic, strong) NSString *destination_lon;
@property (nonatomic, strong) NSString *beizhu;
@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, strong) NSString *contact_time;
@property (nonatomic, strong) NSString *car_id;
@property (nonatomic, strong) NSString *cargo_time;
@property (nonatomic, strong) NSString *r_car_id;
@property (nonatomic, strong) NSString *order_state; //资源状态 0：初始状态   1：这个字段保留 没用到  2订单已谈好 3：确认装货 4：手动交易成功 5：自动交易成功
@property (nonatomic, strong) NSString *loading_time;
@property (nonatomic, strong) NSString *order_affirm_time;
@property (nonatomic, strong) NSString *order_over_time;
@property (nonatomic, strong) NSString *destination_province;
@property (nonatomic, strong) NSString *destination_city;

@property (nonatomic, strong) NSString *destination_district;
@property (nonatomic, strong) NSString *car_space; //1零担 2满车
@property (nonatomic, strong) NSString *delete_time;
@property (nonatomic, strong) NSString *location_encode;
@property (nonatomic, strong) NSString *comment_car;
@property (nonatomic, strong) NSString *comment_owner;
@property (nonatomic, strong) NSString *location_province_name;
@property (nonatomic, strong) NSString *location_city_name;
@property (nonatomic, strong) NSString *location_district_name;

@property (nonatomic, strong) NSString *destination_province_name;
@property (nonatomic, strong) NSString *destination_city_name;
@property (nonatomic, strong) NSString *destination_district_name;

@property (nonatomic, assign) NSInteger hz_deal_sum;
@property (nonatomic, assign) NSInteger hz_comment_h;
@property (nonatomic, assign) NSInteger hz_comment_z;
@property (nonatomic, assign) NSInteger hz_comment_c;
@property (nonatomic, assign) NSInteger distance;


@property (nonatomic, strong) NSString *hz_name;
@property (nonatomic, strong) NSString *reader_mobile;
@property (nonatomic, strong) NSString *hz_phone_1;
@property (nonatomic, strong) NSString *hz_phone_2;
@property (nonatomic, strong) NSString *reader_portrait;

@property (nonatomic, strong) NSString *che_info_name;
@property (nonatomic, strong) NSString *che_user_mobile;
@property (nonatomic, strong) NSString *che_phone_1;
@property (nonatomic, strong) NSString *che_phone_2;
@property (nonatomic, strong) NSString *che_user_portrait;

@property (nonatomic, strong) TSOrderStateModel *orderState;

- (NSString *)getCarLength;
- (NSString *)getCarType;
@end
