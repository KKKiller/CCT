//
//  TSTextController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSTextController.h"

@interface TSTextController ()
@property (nonatomic, strong) UITextView *textView;
@end

@implementation TSTextController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
    [self.view addSubview:self.textView];
    self.textView.contentInset = UIEdgeInsetsMake(10, 15, 10, 15);
    self.textView.text = self.text;
    self.textView.editable = NO;
    self.title = self.navTitle;
    self.textView.font = FFont(14);
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}


@end
