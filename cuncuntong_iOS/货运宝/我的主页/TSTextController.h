//
//  TSTextController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface TSTextController : BaseViewController
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *navTitle;
@end
