//
//  TSHelpController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSHelpController.h"
#import "TSTextController.h"
#import "CCHomeVillageModel.h"
static NSString *cellID = @"TSHelpCell";

@interface TSHelpController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArray; //网络获取数据
@property (nonatomic, strong) NSArray *textArray;
@property (nonatomic, strong) NSString *phoneNum;

@end

@implementation TSHelpController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"帮助"];
    [self.view addSubview:self.tableView];
    [self loadData];
}

- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_agency") params:@{@"r_id":USERID,@"w_group":@"6",@"vid":STR(self.userDistrictId)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.phoneNum = success[@"data"][@"mobile"];
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = FFont(15);
    cell.textLabel.textColor = TEXTBLACK6;
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 1) {
        TSTextController *vc  = [[TSTextController alloc]init];
        vc.navTitle = self.dataArray[indexPath.row];
        vc.text = self.textArray[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self phoneConsult];
    }
}
//电话咨询
- (void)phoneConsult {
    NSString *phone = self.phoneNum ?:  @"010-82886232";
    if ([TOOL stringEmpty:App_Delegate.villageModel.cadminTel] && App_Delegate.villageModel.cadminTel.length == 11) {
        phone = App_Delegate.villageModel.cadminTel;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"联系客服" message:phone preferredStyle:1];
    //18039290408
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
//        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
//        CGFloat yy = system >= 11.0 ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64 )];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 50;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.tableFooterView = [[UIView alloc] init];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
    }
    return _tableView;
}
- (NSArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray =  @[@"关于货运通",@"客服",@"收费标准"];
    }
    return _dataArray;
}
- (NSArray *)textArray {
    if (!_textArray) {
        _textArray = @[@"关于货运通\n\n   1、货运通是全球村村通旗下的，为全国十余万家配货站和数千万名货运司机提供智慧对接服务的网络平台。\n   2、货运通对配货站永不收费。\n   3、货运通将为司机朋友提供海量货源，努力降低司机朋友的空载率。\n   4、平台确保司机不放空。如货主不守信，须支付司机200元放空费。货主不支付的配货站支付。配货站不支付，平台先行垫付放空费，并对配货站做追责下架处理！\n\n操作指南\n\n    1、司机接单需预先充值，余额不足无法查看订单信息。\n    2、司机与配货站对接时，该订单处于封闭状态，其它司机无法查看。\n    3、司机与配货站对接成功须确认订单，确认订单后该订单即处于交易状态，其它司机无法再查看。如不确认订单，订单信息将释放，其它司机可继续接单，有可能出现低价抢单的情况。\n    4、每单对接时间为15分钟，超过15分钟不能确认订单的，订单自动释放。\n    5、确认订单后扣取司机平台信息费。\n    6、司机装货后点击确认装货，配货站的奖励会自动返还。\n    7、发货后，货主点击导航地图可以随时跟踪查看货运信息。",@"空",@"同城货运（发货地与收货地为同一城市），每单收司机信息服务费25元\n异城货运（发货地与收货地非同一城市），每单收司机信息服务费45元"];
    }
    return _textArray;
}

@end
