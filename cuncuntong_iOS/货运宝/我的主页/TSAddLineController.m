//
//  TSAddLineController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSAddLineController.h"
#import "TSAddAddressView.h"
#import "CCPositionModel.h"
#import "SetHomeViewController.h"
@interface TSAddLineController ()
@property (nonatomic, strong) TSAddAddressView *addAddressView;
@property (nonatomic, strong) CCPositionModel *startPositionModel;
@property (nonatomic, strong) CCPositionModel *endPositionModel;
@end

@implementation TSAddLineController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"添加线路"];
    self.view.backgroundColor = TSGRAY;
    self.addAddressView = [TSAddAddressView instanceView];
    [self.view addSubview:self.addAddressView];
    self.addAddressView.frame = CGRectMake(0, 64, App_Width, 250);
    [self.addAddressView.startBtn addTarget:self action:@selector(startBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.addAddressView.endBtn addTarget:self action:@selector(endBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.addAddressView.submitBtn addTarget:self action:@selector(addLine) forControlEvents:UIControlEventTouchUpInside];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}
- (void)addLine {
    if (!self.startPositionModel || !self.endPositionModel) {
        SHOW(@"请选择起点与终点");
        return;
    }
    NSDictionary *param = @{@"r_id":USERID,@"location_province":STR(self.startPositionModel.provinceId),@"location_city":STR(self.startPositionModel.cityId),@"location_district":STR(self.startPositionModel.districtId),@"destination_province":STR(self.endPositionModel.provinceId),@"destination_city":STR(self.endPositionModel.cityId),@"destination_district":STR(self.endPositionModel.districtId)};
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/add_path") params:param target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            if (self.addLineBlock) {
                self.addLineBlock();
            }
            SHOW(@"添加成功");
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}

//起点
- (void)startBtnClick {
    SetHomeViewController *vc = [[SetHomeViewController alloc]init];
    vc.selectPosition = YES;
    vc.selectPositionBlk = ^(CCPositionModel *positionModel) {
        self.startPositionModel = positionModel;
        [self.addAddressView.startBtn setImage:nil forState:UIControlStateNormal];
        [self.addAddressView.startBtn setTitle:[NSString stringWithFormat:@"%@ %@",positionModel.cityName,positionModel.districtName] forState:UIControlStateNormal];
    };
    [self.navigationController pushViewController:vc animated:YES];
    
}
//终点
- (void)endBtnClick {
    SetHomeViewController *vc = [[SetHomeViewController alloc]init];
    vc.selectPosition = YES;
    vc.selectPositionBlk = ^(CCPositionModel *positionModel) {
        self.endPositionModel = positionModel;
        [self.addAddressView.endBtn setImage:nil forState:UIControlStateNormal];
        [self.addAddressView.endBtn setTitle:[NSString stringWithFormat:@"%@ %@",positionModel.cityName,positionModel.districtName] forState:UIControlStateNormal];

    };
    [self.navigationController pushViewController:vc animated:YES];
}
@end
