//
//  TSProfileController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSProfileController.h"
#import "TSMyDataView.h"
#import "TSMyLineCell.h"
#import "TSAddLineController.h"
#import "TSVerifyController.h"
#import "TSLineModel.h"
#import "TSCarOwnerModel.h"
#import "TSCarModel.h"
#import "RechargeViewController.h"
#import "AccountViewController.h"
#import "RecordViewController.h"
#import "WithdrawViewController.h"
#import "CCShareView.h"
#import <ContactsUI/ContactsUI.h>
#import "TSStationModel.h"
#import "TSHelpController.h"
#import <MessageUI/MessageUI.h>
static NSString *cellID = @"TSProfileCell";

@interface TSProfileController ()<UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,CNContactPickerDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *myLineArray; //网络获取数据
@property (nonatomic, strong) TSMyDataView *header;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, assign) BOOL isDriver;
@property (nonatomic, strong) CCShareView *shareView;

@property (nonatomic, strong) TSCarOwnerModel *carOwnerModel;
@property (nonatomic, strong) TSCarModel *carModel;
@property (nonatomic, strong) TSStationModel *stationModel;
@property (nonatomic, assign) TSRole inviteRole;
@end

@implementation TSProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的主页";

    self.isDriver = App_Delegate.transportTabbarVc.isDriver;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.shareView];
    [self setRightBtn];
}
- (void)setRightBtn {
    if (![App_Delegate.defaultHome isEqualToString:@"default"]) {
        UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [rightBtn setTitle:@"帮助" forState:UIControlStateNormal];
        rightBtn.titleLabel.font = FFont(14);
//        [rightBtn addTarget:self action:@selector(help) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
        self.navigationItem.rightBarButtonItem = right;
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    App_Delegate.transportTabbarVc.rightTitle = @"帮助";
    [self refreshData];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
}
- (void)refreshData {
    [self loadData];
    [self loadLineArray];
    [self loadBalance];
}

#pragma mark - 获取数据
- (void)loadData {
        NSString *url = self.isDriver ? @"freight/get_owner_info" : @"freight/get_apply_owner";
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(url) params:@{@"r_id":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            if (App_Delegate.transportTabbarVc.isDriver) {
                self.carModel = [TSCarModel modelWithJSON: success[@"data"][@"car"]];
                self.carOwnerModel = [TSCarOwnerModel modelWithJSON:success[@"data"][@"car_owner"]];
                self.header.carOwnerModel = self.carOwnerModel;
                App_Delegate.transportTabbarVc.carOwnerModel = self.carOwnerModel;
                self.header.carModel = self.carModel;
                [self.header.headImgView setImageWithURL:URL([UserManager sharedInstance].portrait) placeholder:IMAGENAMED(@"head")];

            }else{
                
                [self.header.headImgView setImageWithURL:URL([UserManager sharedInstance].portrait) placeholder:IMAGENAMED(@"head")];
                self.stationModel = [TSStationModel modelWithJSON:success[@"data"]];
                self.header.stationModel = self.stationModel;
                App_Delegate.transportTabbarVc.stationModel = self.stationModel;
            }
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}

- (void)loadLineArray {
    if (self.isDriver) {
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/get_path") params:@{@"r_id":USERID} target:self success:^(NSDictionary *success) {
            if ([success[@"error"]  isEqual: @(0)]) {
                    [self.myLineArray removeAllObjects];
                for (NSDictionary *dict in success[@"data"]) {
                    TSLineModel *model = [TSLineModel modelWithJSON:dict];
                    [self.myLineArray addObject:model];
                }
                self.header.lineCount = self.myLineArray.count;
                [self.tableView reloadData];
            }else{
                SHOW(success[@"msg"]);
            }
        } failure:^(NSError *failure) {
        }];
    }
}
- (void)loadBalance {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": [UserManager sharedInstance].secret ?:@""} target:self success:^(NSDictionary *success) {
        self.header.balanceLbl.text = [NSString stringWithFormat:@"%@元",success[@"data"][@"money"]];
    } failure:^(NSError *failure) {
    }];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.myLineArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TSMyLineCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.myLineArray.count > indexPath.row) {
        TSLineModel *model = self.myLineArray[indexPath.row];
        cell.model = model;
        cell.deleteBtn.tag = indexPath.row;
        [cell.deleteBtn addTarget:self action:@selector(deleteMyLine:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
//删除我的线路
- (void)deleteMyLine:(UIButton *)sender {
    NSInteger index = sender.tag;
    TSLineModel *model = self.myLineArray[index];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/delete_path") params:@{@"r_id":USERID,@"id":model.id} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            [self.myLineArray removeObject:model];
            [self.tableView reloadData];
            [self refreshData];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}
//添加我的线路
- (void)addLine {
    TSAddLineController *vc = [[TSAddLineController alloc]init];
    vc.addLineBlock = ^{
        [self refreshData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
//点击姓名 车型
- (void)verify {
    TSVerifyController *vc = [[TSVerifyController alloc]init];
    vc.carModel = self.carModel;
    vc.isStation = !self.isDriver;
    vc.carOwnerModel = self.carOwnerModel;
    vc.stationModel = self.stationModel;
    [self.navigationController pushViewController:vc animated:YES];
}
//点击电话
- (void)tapAtPhone {
    AccountViewController *vc = [[AccountViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

//修改头像
- (void)modifyAvatar {
    AccountViewController *vc = [[AccountViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)voiceChange:(UISwitch *)sender {
    BOOL on = sender.isOn;
    [Def setBool:!on forKey:@"muteVoice"];
    [self settingWithType:@"voice" value:on];
}

///同城优先
- (void)nearbyBtnClick {
    self.header.showTypeNearBtn.selected = YES;
    self.header.showTypeOtherBtn.selected = NO;
    [self settingWithType:@"source_type" value:NO];
    [Def setBool:YES forKey:@"sameCity"];
}
///其他优先
- (void)otherTypeBtnClick {
    self.header.showTypeNearBtn.selected = NO;
    self.header.showTypeOtherBtn.selected = YES;
    [self settingWithType:@"source_type" value:YES];
    [Def setBool:NO forKey:@"sameCity"];
}
//0 货源 1语音
- (void)settingWithType:(NSString *)type value:(BOOL)value{
    //设置的参数 参数source_type(0:同城优先 1：其他 ) voice(语音设置 1：开通 0关闭)
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/basicsset") params:@{@"r_id":USERID,@"field":type,@"val":@(value)} target:self success:^(NSDictionary *success) {
        
    } failure:^(NSError *failure) {
    }];
}
///充值
- (void)chargeBtnClick {
    RechargeViewController *vc = [[RechargeViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
///提现
- (void)takeoutBtnClick {
    WithdrawViewController *vc = [[WithdrawViewController alloc] init];
    vc.maxMoney = [[UserManager sharedInstance].money floatValue];
    vc.aliName = [Def valueForKey:@"aliName"];
    vc.aliAccount = [Def valueForKey: @"aliAccount"];
    [self.navigationController pushViewController:vc animated:YES];

}
///账单
- (void)billBtnClick {
    RecordViewController *vc = [[RecordViewController alloc]init];
    vc.recordType = RecordShare;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 分享
- (void)share:(UIButton *)sender {
    if (self.header.inviteStationBtn == sender) {
        self.inviteRole = TSRoleStation;
    }else if (self.header.inviteDriverBtn == sender){
        self.inviteRole = TSRoleDriver;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height - 49;
    }];
}
- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
    }];
}
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [self sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    // 设置短信内容
    NSString *url = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    NSString *shareText = [[NSString stringWithFormat:@"%@%@", [self getInviteTextTitle],[self getInviteTextDesc]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, url];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
    self.inviteRole = TSRoleNormal;
}


- (void)weixinBtnClick:(UIButton *)sender {
    [self shareCancelBtnClick];
    NSInteger tag = sender.tag;
    UMSocialPlatformType  platform = tag == 10 ? UMSocialPlatformType_WechatSession : UMSocialPlatformType_WechatTimeLine;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@""]]];
    NSString *title = [self getInviteTextTitle];
    NSString *desc = [self getInviteTextDesc];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: title  descr:desc thumImage:IMAGENAMED(@"cct")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        self.inviteRole = TSRoleNormal;
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

- (NSString *)getInviteTextTitle {
    NSString *text;
    if (self.inviteRole == TSRoleStation) {
        text = @"怪不得配货站都在这里发货，原来如此！";
    }else if (self.inviteRole == TSRoleDriver){
        text = @"怪不得兄弟们都在这里找货，原来如此。";
    }else{
        text = App_Delegate.transportTabbarVc.isDriver ? @"怪不得兄弟们都在这里找货，原来如此。" : @"怪不得配货站都在这里发货，原来如此！";
    }
    return text;
}
- (NSString *)getInviteTextDesc {
    NSString *text;
    if (self.inviteRole == TSRoleStation) {
        text = @"不收费，奖励多，海量司机一秒响应，随时随地发货！";
    }else if (self.inviteRole == TSRoleDriver){
        text = @"货源多，语音短信智能报单，不会错过每一单生意！";
    }else{
        text = App_Delegate.transportTabbarVc.isDriver ? @"货源多，语音短信智能报单，不会错过每一单生意！" : @"不收费，奖励多，海量司机一秒响应，随时随地发货！";
    }
    return text;
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
        CGFloat yy = system >= 11.0 || !App_Delegate.isDefaultRoot ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, yy, App_Width, App_Height - 49 - yy)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.header;
        if (self.isDriver) {
            _tableView.tableFooterView = self.footerView;
        }
        _tableView.rowHeight = 40;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"TSMyLineCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
#ifdef __IPHONE_11_0
        if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
#endif
    }
    return _tableView;
}
- (NSMutableArray *)myLineArray {
    if (_myLineArray == nil) {
        _myLineArray = [[NSMutableArray alloc]init];
    }
    return _myLineArray;
}
- (TSMyDataView *)header {
    if (!_header) {
        _header = [TSMyDataView instanceView];
        CGFloat height = self.isDriver ? 506 : 506 - 205;
        _header.frame = CGRectMake(0, 0, App_Width, height);
        _header.driverContainerView.hidden = !self.isDriver;
        _header.takeoutBtn.hidden = self.isDriver;
        _header.billBtnRightMargin.constant = self.isDriver ? 15 : 77;
        _header.chargeBtn.hidden = !self.isDriver;
        _header.chargeBtnWidth.constant = self.isDriver ? 52 : 0;
        _header.phoneTopMargin.constant = self.isDriver ? 64 : 15;
        _header.carTypeLbl.hidden = !self.isDriver;
        _header.carLbl.hidden = !self.isDriver;
        _header.carArrow.hidden = !self.isDriver;
        WEAKSELF
        [_header.nameTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf verify];
        }];
        [_header.carTypeTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf verify];
        }];
        [_header.phoneTap addActionBlock:^(id  _Nonnull sender) {
            [self tapAtPhone];
        }];
        [_header.inviteDriverBtn addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
        [_header.inviteStationBtn addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];

        [_header.headImgTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf modifyAvatar];
        }];
        [_header.carTypeArrowTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf verify];
        }];
        [_header.headArrowTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf modifyAvatar];
        }];
        [_header.carTypeArrowTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf verify];
        }];
        [_header.vioceSwitch addTarget:self action:@selector(voiceChange:) forControlEvents:UIControlEventValueChanged];
        [_header.showTypeNearBtn addTarget:self action:@selector(nearbyBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_header.showTypeOtherBtn addTarget:self action:@selector(otherTypeBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_header.chargeBtn addTarget:self action:@selector(chargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_header.takeoutBtn addTarget:self action:@selector(takeoutBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_header.billBtn addTarget:self action:@selector(billBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _header.vioceSwitch.on = [Def boolForKey:@"voice"];
    }
    return _header;
}

- (UIView *)footerView {
    if (!_footerView) {
        _footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 50)];
        _footerView.backgroundColor = WHITECOLOR;
        CGFloat width = 70;
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake((App_Width - width)*0.5, 7, width, 35)];
        [btn setTitle:@"添加线路" forState:UIControlStateNormal];
        btn.layer.cornerRadius = 4;
        btn.layer.masksToBounds = YES;
        btn.layer.borderColor = MAINGREEN.CGColor;
        btn.layer.borderWidth = 0.5;
        [btn setTitleColor:MAINGREEN forState:UIControlStateNormal];
        btn.titleLabel.font = FFont(14);
        [_footerView addSubview:btn];
        [btn addTarget:self action:@selector(addLine) forControlEvents:UIControlEventTouchUpInside];
    }
    return _footerView;
}
- (CCShareView *)shareView {
    if (!_shareView) {
        _shareView = [CCShareView instanceView];
        _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.pengyouquanBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareView;
}
@end




/*
 
 
 #pragma mark - 照片
 - (void)chooseAvatar{
 UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
 imagePicker.editing = YES;
 imagePicker.delegate = self;
 imagePicker.allowsEditing = YES;
 //创建sheet提示框，提示选择相机还是相册
 
 UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"请选择打开方式" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
 
 //相机选项
 
 UIAlertAction * camera = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 //选择相机时，设置UIImagePickerController对象相关属性
 imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
 imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
 imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
 //跳转到UIImagePickerController控制器弹出相机
 [self presentViewController:imagePicker animated:YES completion:nil];
 }];
 
 //相册选项
 UIAlertAction * photo = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 //选择相册时，设置UIImagePickerController对象相关属性
 imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
 //跳转到UIImagePickerController控制器弹出相册
 [self presentViewController:imagePicker animated:YES completion:nil];
 }];
 
 //取消按钮
 UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
 [self dismissViewControllerAnimated:YES completion:nil];
 }];
 
 //添加各个按钮事件
 [alert addAction:camera];
 [alert addAction:photo];
 [alert addAction:cancel];
 //弹出sheet提示框
 
 [self presentViewController:alert animated:YES completion:nil];
 
 }
 
 -(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
 
 UIImage *img = info[UIImagePickerControllerEditedImage];
 self.header.headImgView.image = img;
 [picker dismissViewControllerAnimated:YES completion:nil];
 }

 */
