//
//  TSAddAddressView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSAddAddressView.h"

@implementation TSAddAddressView

+ (instancetype)instanceView {
    TSAddAddressView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    return view;
}

@end
