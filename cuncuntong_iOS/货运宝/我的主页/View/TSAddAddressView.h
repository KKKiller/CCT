//
//  TSAddAddressView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSAddAddressView : UIView
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *endBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;


+ (instancetype)instanceView;
@end
