//
//  TSMyLineCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSMyLineCell.h"

@implementation TSMyLineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(TSLineModel *)model {
    _model = model;
    if ([model.location_city_name isEqualToString:@"市辖区"] || [model.location_city_name isEqualToString:@"县"]) {
        model.location_city_name = model.location_province_name;
    }
    if ([model.destination_city_name isEqualToString:@"市辖区"] || [model.destination_city_name isEqualToString:@"县"]) {
        model.destination_city_name = model.destination_province_name;
    }
    self.textLbl.text = [NSString stringWithFormat:@"%@——>%@", model.location_city_name,model.destination_city_name];
}

@end
