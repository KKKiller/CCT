//
//  TSMyDataView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "TSMyDataView.h"

@implementation TSMyDataView

+ (instancetype)instanceView {
    TSMyDataView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.headImgView.layer.cornerRadius = 25;
    view.headImgView.layer.masksToBounds = YES;
    
    view.billBtn.layer.cornerRadius = 5;
    view.billBtn.layer.masksToBounds = YES;
    
    view.chargeBtn.layer.cornerRadius = 5;
    view.chargeBtn.layer.masksToBounds = YES;
    
    view.takeoutBtn.layer.cornerRadius = 5;
    view.takeoutBtn.layer.masksToBounds = YES;
    return view;
}

- (void)setCarModel:(TSCarModel *)carModel {
    _carModel = carModel;
    self.carTypeLbl.text = [NSString stringWithFormat:@"%@/%@/%@",carModel.type1_name,carModel.type2_name,carModel.type3_name];

}
- (void)setCarOwnerModel:(TSCarOwnerModel *)carOwnerModel {
    _carOwnerModel = carOwnerModel;
    self.nameLbl.text = STR(carOwnerModel.info_name);
    NSString *avatar = [carOwnerModel.avatar containsString:@"http"] ? carOwnerModel.avatar : BASEURL_WITHOBJC(carOwnerModel.avatar);
    [self.headImgView setImageWithURL:URL(avatar) placeholder:IMAGENAMED(@"head")];
    self.phoneLbl.text = STRINGEMPTY(carOwnerModel.info_phone) ? carOwnerModel.phone_2 : carOwnerModel.info_phone;
//    self.balanceLbl.text = carOwnerModel.money;
    self.vioceSwitch.on = carOwnerModel.voice;
    self.showTypeNearBtn.selected = !carOwnerModel.source_type;
    self.showTypeOtherBtn.selected = carOwnerModel.source_type;
}
- (void)setStationModel:(TSStationModel *)stationModel {
    _stationModel = stationModel;
    self.nameLbl.text = STR(stationModel.name);
    self.phoneLbl.text = STRINGEMPTY(stationModel.phone_2) ? stationModel.phone_2 : stationModel.phone_1;
}
- (void)setLineCount:(NSInteger)lineCount {
    _lineCount =lineCount;
    self.myLiveLbl.text = [NSString stringWithFormat:@"我的线路（%@/10）",@(lineCount)];
}
@end
