//
//  TSMyDataView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSCarModel.h"
#import "TSCarOwnerModel.h"
#import "TSStationModel.h"
@interface TSMyDataView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *carTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *carLbl;
@property (weak, nonatomic) IBOutlet UIImageView *carArrow;

@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UIButton *billBtn;
@property (weak, nonatomic) IBOutlet UIButton *chargeBtn;
@property (weak, nonatomic) IBOutlet UIButton *takeoutBtn;
@property (weak, nonatomic) IBOutlet UIButton *showTypeOtherBtn;
@property (weak, nonatomic) IBOutlet UIButton *showTypeNearBtn;
@property (weak, nonatomic) IBOutlet UISwitch *vioceSwitch;
@property (weak, nonatomic) IBOutlet UILabel *myLiveLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneTopMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *billBtnRightMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chargeBtnWidth;

@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *headImgTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *nameTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *carTypeTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *phoneTap;
@property (weak, nonatomic) IBOutlet UIButton *inviteDriverBtn;
@property (weak, nonatomic) IBOutlet UIButton *inviteStationBtn;

@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *headArrowTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *nameArrowTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *phoneArrowTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *carTypeArrowTap;

@property (weak, nonatomic) IBOutlet UIView *driverContainerView;

+ (instancetype)instanceView;

@property (nonatomic, strong) TSCarModel *carModel;
@property (nonatomic, strong) TSCarOwnerModel *carOwnerModel;
@property (nonatomic, assign) NSInteger lineCount;
@property (nonatomic, strong) TSStationModel *stationModel;

@end
