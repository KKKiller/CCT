//
//  TSCarModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/10.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSCarModel : NSObject
@property (nonatomic, strong) NSString *car_id;
@property (nonatomic, strong) NSString *driver_id;
@property (nonatomic, strong) NSString *load;
@property (nonatomic, strong) NSString *car_tyle_1;
@property (nonatomic, strong) NSString *car_tyle_2;
@property (nonatomic, strong) NSString *car_tyle_3;
@property (nonatomic, strong) NSString *car_img;
@property (nonatomic, strong) NSString *plate_number;
@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, strong) NSString *delete_time;
@property (nonatomic, strong) NSString *lon;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *type1_name;
@property (nonatomic, strong) NSString *type2_name;
@property (nonatomic, strong) NSString *type3_name;

@end
