//
//  TSCarOwnerModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/10.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSCarOwnerModel : NSObject
@property (nonatomic, strong) NSString *r_id;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *info_name;
@property (nonatomic, strong) NSString *province_certificate_1;
@property (nonatomic, strong) NSString *province_certificate_2;
@property (nonatomic, strong) NSString *car_type;
@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, strong) NSString *info_state;
@property (nonatomic, strong) NSString *delete_time;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) NSString *info_phone;
@property (nonatomic, strong) NSString *certificate;
@property (nonatomic, strong) NSString *phone_1;
@property (nonatomic, strong) NSString *phone_2;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *driving_licence_1;
@property (nonatomic, strong) NSString *driving_licence_2;
@property (nonatomic, strong) NSString *comment_h;
@property (nonatomic, strong) NSString *comment_z;
@property (nonatomic, strong) NSString *comment_c;
@property (nonatomic, strong) NSString *province_name;
@property (nonatomic, strong) NSString *city_name;
@property (nonatomic, strong) NSString *area_name;
@property (nonatomic, assign) BOOL source_type;
@property (nonatomic, assign) BOOL voice;

@end
