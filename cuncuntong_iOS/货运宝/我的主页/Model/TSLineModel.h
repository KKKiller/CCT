//
//  TSLineModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSLineModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *car_id;
@property (nonatomic, strong) NSString *r_id;
@property (nonatomic, strong) NSString *destination_province;
@property (nonatomic, strong) NSString *destination_city;
@property (nonatomic, strong) NSString *destination_district;

@property (nonatomic, strong) NSString *location_province;
@property (nonatomic, strong) NSString *location_city;
@property (nonatomic, strong) NSString *location_district;
@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, strong) NSString *location_province_name;
@property (nonatomic, strong) NSString *location_city_name;

@property (nonatomic, strong) NSString *location_district_name;
@property (nonatomic, strong) NSString *destination_province_name;
@property (nonatomic, strong) NSString *destination_city_name;
@property (nonatomic, strong) NSString *destination_district_name;

@end
