//
//  TSStationModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/18.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSStationModel : NSObject
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *area_name;
@property (nonatomic, strong) NSString *certificate;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *city_name;
@property (nonatomic, strong) NSString *detailed_address;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *log;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone_1;
@property (nonatomic, strong) NSString *phone_2;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *province_certificate_1;
@property (nonatomic, strong) NSString *province_certificate_2;
@property (nonatomic, strong) NSString *province_certificate_3;
@property (nonatomic, strong) NSString *license;
@property (nonatomic, strong) NSString *province_name;
@property (nonatomic, strong) NSString *r_id;
@property (nonatomic, strong) NSString *state;




@end
