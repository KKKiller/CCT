//
//  ChatGroupMemberSettingController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/11.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "ChatGroupMemberSettingController.h"
#import "ChatGroupMemberSettingView.h"
#import "ChatGroupMemberSettingCell.h"
#import "MultiSelectViewController.h"
#import "zhPopupController.h"
#define angelToRandian(x)  ((x)/180.0*M_PI)

@interface ChatGroupMemberSettingController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutletCollection(UICollectionView) NSArray *collectVS;
@property (nonatomic, strong) NSMutableArray *dataArr0;
@property (nonatomic, strong) NSMutableArray *dataArr1;
@property (nonatomic, strong) NSMutableArray *dataArr2;
@property (nonatomic, strong) NSMutableArray *arr;
@property (strong, nonatomic) IBOutlet UIView *chuFaView;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgArr;


@end

@implementation ChatGroupMemberSettingController
- (IBAction)confirmAct:(UIButton *)sender {
    [self.zh_popupController dismiss];
    for (UIImageView *img in self.imgArr) {
        if ([img.image isEqual:IMAGENAMED(@"choic_selected")]){
            NSLog(@"第%ld个被选中了",[self.imgArr indexOfObject:img]);
        }
    }
}
- (IBAction)chuFaTimeClick:(UIButton *)sender {
    
    UIView *supV = sender.superview;
    for (UIView *v in supV.subviews) {
        if ([v isKindOfClass:[UIImageView class]]){
            UIImageView *imgV = (UIImageView *)v;
            if ([imgV.image isEqual:[UIImage imageNamed:@"choic_normal"]]) {
                for (UIImageView *imag in self.imgArr) {
                    imag.image = IMAGENAMED(@"choic_normal");
                }
                imgV.image = IMAGENAMED(@"choic_selected");
            }else{
                for (UIImageView *imag in self.imgArr) {
                    imag.image = IMAGENAMED(@"choic_normal");
                }
                imgV.image = IMAGENAMED(@"choic_normal");
            }
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.chuFaView.frame = CGRectMake(0, 0, App_Width - 30, 212);
    [self setCollectionView];
    [self.dataArr0 addObjectsFromArray:@[@"0"]];
    [self.dataArr1 addObjectsFromArray:@[@"1", @"2"]];
    [self.dataArr2 addObjectsFromArray:@[@"3", @"4", @"5"]];
    self.arr = [NSMutableArray arrayWithCapacity:1];
    [self.arr addObjectsFromArray:@[self.dataArr0, self.dataArr1, self.dataArr2]];
    
  
}

- (void)setCollectionView {
   
    for (UICollectionView *collect in _collectVS) {
        [collect registerNib:[UINib nibWithNibName:NSStringFromClass([ChatGroupMemberSettingCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([ChatGroupMemberSettingCell class])];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(66, 93);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 16;
        collect.collectionViewLayout = layout;
    }
    

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger index = [_collectVS indexOfObject:collectionView];
    return ((NSMutableArray *)_arr[index]).count + 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatGroupMemberSettingCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ChatGroupMemberSettingCell class]) forIndexPath:indexPath];
    for (UIGestureRecognizer *ges in cell.imgView.gestureRecognizers) {
        [cell.imgView removeGestureRecognizer:ges];
    }
    
    NSInteger index = [_collectVS indexOfObject:collectionView];
    NSInteger rows = ((NSMutableArray *)_arr[index]).count;
    if (indexPath.row != rows){
        cell.nameLbl.text = @"王二小";
        [cell.imgView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAct:)]];
        [cell.imgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesAct:)]];
        [cell.removeBtn addTarget:self action:@selector(removeAct:) forControlEvents:UIControlEventTouchUpInside];
        
    }else {
        [cell.imgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addAct:)]];
        cell.imgView.image = IMAGENAMED(@"addaphoto");
        cell.nameLbl.text = @"";
        
    }
    
    
    
    
    return cell;
}

-(void)longPressAct:(UILongPressGestureRecognizer*)longPress
{
    if (longPress.state==UIGestureRecognizerStateBegan) {
        CAKeyframeAnimation* anim=[CAKeyframeAnimation animation];
        anim.keyPath=@"transform.rotation";
        anim.values=@[@(angelToRandian(-7)),@(angelToRandian(7)),@(angelToRandian(-7))];
        anim.repeatCount=MAXFLOAT;
        anim.duration=0.2;
        ChatGroupMemberSettingCell * cell = (ChatGroupMemberSettingCell *)[longPress.view superview].superview;
        [cell.imgView.layer addAnimation:anim forKey:@"image"];
        [cell.removeBtn.layer addAnimation:anim forKey:@"btn"];
        cell.removeBtn.hidden=NO;
    }
}

- (void)tapGesAct:(UITapGestureRecognizer *)ges{
    ChatGroupMemberSettingCell * cell = (ChatGroupMemberSettingCell *)[ges.view superview].superview;
    [cell.imgView.layer removeAnimationForKey:@"image"];
    [cell.removeBtn.layer removeAnimationForKey:@"btn"];
    cell.removeBtn.hidden=YES;
}

- (void)addAct:(UITapGestureRecognizer *)ges{
    MultiSelectViewController *Vc = [MultiSelectViewController new];
    MultiSelectItem *item0 = [[MultiSelectItem alloc]init];
    item0.name = @"中国";
    MultiSelectItem *item1 = [[MultiSelectItem alloc]init];
    item1.name = @"人迷";
    
    Vc.items = @[item0, item1];
    [self.navigationController pushViewController:Vc animated:YES];
}

-(void)removeAct:(UIButton *)sender{
    ChatGroupMemberSettingCell * cell = (ChatGroupMemberSettingCell *)sender.superview.superview;
    [cell.imgView.layer removeAnimationForKey:@"image"];
    [cell.removeBtn.layer removeAnimationForKey:@"btn"];
    cell.removeBtn.hidden=YES;
    
    for (UIImageView *img in self.imgArr) {
        img.image = IMAGENAMED(@"choic_normal");
        
    }
    
    self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
    self.zh_popupController.layoutType = zhPopupLayoutTypeCenter;
    self.zh_popupController.allowPan = YES;
    [self.zh_popupController presentContentView:self.chuFaView];
}

#pragma mark - 懒加载
-(NSMutableArray *)dataArr0{
    if (!_dataArr0){
        _dataArr0 = [NSMutableArray arrayWithCapacity:1];
    }
    return _dataArr0;
}
-(NSMutableArray *)dataArr1{
    if (!_dataArr1){
        _dataArr1 = [NSMutableArray arrayWithCapacity:1];
    }
    return _dataArr1;
}
-(NSMutableArray *)dataArr2{
    if (!_dataArr2){
        _dataArr2 = [NSMutableArray arrayWithCapacity:1];
    }
    return _dataArr2;
}

@end
