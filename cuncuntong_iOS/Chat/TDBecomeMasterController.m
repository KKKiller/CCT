//
//  TDBecomeMasterController.m
//  CunCunTong
//
//  Created by TAL on 2019/6/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "TDBecomeMasterController.h"
#import "TDBecomeMasterCell.h"
#import "CCTGroupModel.h"
@interface TDBecomeMasterController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation TDBecomeMasterController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = NO;
    [self loadGroupInfo];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TDBecomeMasterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"becomeMasterCell" forIndexPath:indexPath];
    return cell;
}

- (void)loadGroupInfo {
    [[MyNetWorking sharedInstance]GetUrlNew:BASELIVEURL_WITHOBJC(@"group/id/find") params:@{@"groupId":STR(self.groupId)} success:^(NSDictionary *success) {
        if ([success[@"code"] integerValue ] == 201) {
            CCTGroupModel *model = [CCTGroupModel modelWithDictionary:success ];
            NSLog(@"%@",model);
        }
    } failure:^(NSError *failure) {
        
    }];
}

@end
