//
//  CCChatTabbarController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCChatTabbarController.h"
#import "NavigationViewController.h"
#import "CCBuyMineController.h"
#import "RCDUIBarButtonItem.h"
#import "KxMenu.h"
#import "CCChatAddFriendController.h"
#import "CCChatAddMsgController.h"
#import "CunCunTong-Swift.h"
#import "RCDCreateGroupViewController.h"
#import "CCAddFriendView.h"
#import "RCDSearchFriendViewController.h"
#import "RCDContactSelectedTableViewController.h"
#import "CCChatContactsController.h"
#import "RCDContactSelectedTableViewController.h"
#import "THChatListController.h"
#import "CCTDController.h"
#import "UITabBar+badge.h"
@interface CCChatTabbarController ()<UITabBarDelegate,UITabBarControllerDelegate>
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) CCAddFriendView *addGroupView;
@property (nonatomic, strong) RCDUIBarButtonItem *rightBtn;

@end

@implementation CCChatTabbarController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.barTintColor = MTRGB(0xf0f0f0);

    [self initSelf];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    [NOTICENTER addObserver:self selector:@selector(updateBadge) name:kUpdateBadgeNumNotification object:nil];
    App_Delegate.chatTabbarVc = self;
    App_Delegate.needLocalNoti = NO;
    self.delegate  = self;
//    self.rightBtn =
//    [[RCDUIBarButtonItem alloc] initContainImage:[UIImage imageNamed:@"add_friend"]
//                                  imageViewFrame:CGRectMake(0, 0, 18, 20)
//                                     buttonTitle:nil
//                                      titleColor:nil
//                                      titleFrame:CGRectZero
//                                     buttonFrame:CGRectMake(0, 0, 18, 20)
//                                          target:self
//                                          action:@selector(showMenu:)];
//    self.navigationItem.rightBarButtonItems = [self.rightBtn setTranslation:self.rightBtn translation:-6];
    
    UIButton *settingBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [settingBtn setImage:IMAGENAMED(@"add_friend") forState:UIControlStateNormal];
    [settingBtn addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    UIButton *getMasterBtn =[UIButton buttonWithType:(UIButtonTypeCustom)];
//    [getMasterBtn setTitle:@"抢群主" forState:(UIControlStateNormal)];
//    [getMasterBtn setTitleColor:MTRGB(0xFF4242) forState:(UIControlStateNormal)];
//    getMasterBtn.layer.masksToBounds=YES;
//    getMasterBtn.layer.cornerRadius= 12;
//    getMasterBtn.titleLabel.font=[UIFont systemFontOfSize:12];
//    getMasterBtn.backgroundColor = MTRGB(0xFFF18B);
//    [getMasterBtn addTarget:self action:@selector(getMaster) forControlEvents:UIControlEventTouchUpInside];
    
    settingBtn.frame = CGRectMake(0, 0, 25, 12);
//    getMasterBtn.frame=CGRectMake(0, 0, 45, 12);
    
    UIBarButtonItem *setting = [[UIBarButtonItem alloc] initWithCustomView:settingBtn];
//    UIBarButtonItem *getMaster = [[UIBarButtonItem alloc] initWithCustomView:getMasterBtn];
//    [getMasterBtn setTintColor:[UIColor redColor]];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: setting,nil]];
}

- (void)initSelf {
    
    NSArray *vcArr = @[@"THChatListController",
                       @"CCTDController",
                       @"CCChatNearbyController",
                       @"CCChatVillageController",
                       @"CCChatContactsController"];
    
    NSArray *imageArr = @[@"chat_list",
                          @"chat_nearby",
                          @"chat_nearby",
                          @"chat_village",
                          @"chat_contact"];
    
    NSArray *selImageArr = @[@"chat_list_selected",
                             @"chat_nearby_selected",
                             @"chat_nearby_selected",
                             @"chat_village_selected",
                             @"chat_contact_selected"];
    NSArray *titleArr = @[@"家园",
                          @"同道",
                          @"同乡",
                          @"同村",
                          @"好友"];
    
    NSMutableArray *tabArrs = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < vcArr.count; i++) {
        
        UIViewController *vc = [[NSClassFromString(vcArr[i]) alloc] init];
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        vc.tabBarItem.image = [[UIImage imageNamed:imageArr[i]]
                               imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selImageArr[i]]
                                       imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.title = titleArr[i];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x6c6c6c)} forState:UIControlStateNormal];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MAINBLUE} forState:UIControlStateSelected];
        [tabArrs addObject:vc];
    }
    
    // 解决push/pop黑影
    self.view.backgroundColor = [UIColor whiteColor];
    self.viewControllers = tabArrs;
    
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{

}
- (void)showMenu:(UIButton *)sender {
    if (self.selectedIndex == 3) {
            NSString *vill = [[UserManager sharedInstance].village[0] valueForKey:@"id"];
            NSString *villId = [NSString stringWithFormat:@"vill_%@",[UserManager sharedInstance].location];
            CCChatController *conversationVC = [[CCChatController alloc]init];
            conversationVC.conversationType = ConversationType_GROUP;
            conversationVC.targetId = villId;
            conversationVC.title = @"同村";
            [self.navigationController pushViewController:conversationVC animated:YES];
//        }
        return;
    }
    NSArray *menuItems = @[
                           
                           [KxMenuItem menuItem:@"发起聊天"
                                          image:[UIImage imageNamed:@"startchat_icon"]
                                         target:self
                                         action:@selector(pushChat)],
                           
                           [KxMenuItem menuItem:@"创建群组"
                                          image:[UIImage imageNamed:@"creategroup_icon"]
                                         target:self
                                         action:@selector(pushCreatGroup)],
                           
                           [KxMenuItem menuItem:@"添加好友"
                                          image:[UIImage imageNamed:@"addfriend_icon"]
                                         target:self
                                         action:@selector(pushAddFriend)],];
    UIBarButtonItem *rightBarButton = self.navigationItem.rightBarButtonItems[0];
    CGRect targetFrame = rightBarButton.customView.frame;
    targetFrame.origin.y = targetFrame.origin.y + 15;
    targetFrame.origin.x = App_Width - 80;
    [KxMenu setTintColor:HEXCOLOR(0x000000)];
    [KxMenu setTitleFont:[UIFont systemFontOfSize:15]];
    [KxMenu showMenuInView:self.navigationController
     .navigationBar.superview
                  fromRect:targetFrame
                 menuItems:menuItems];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateBadge];
}
- (void)updateBadge {
//    NSInteger friendRequestCount= [XMPPTOOL getFriendRequestArray].count;
//    if (friendRequestCount == 0) {
//        [self.tabBar hideBadgeOnItemIndex:4];
//    }else{
//        [self.tabBar showBadgeOnItemIndex:4 badgeValue:(int)friendRequestCount tabbarCount:5];
//    }
    if (XMPPTOOL.totalUnreadCount == 0) {
        [self.tabBar hideBadgeOnItemIndex:0];
    }else{
        [self.tabBar showBadgeOnItemIndex:0 badgeValue:(int)XMPPTOOL.totalUnreadCount tabbarCount:5];
    }
}
- (void)pushChat {
    CCChatContactsController *vc = [[CCChatContactsController alloc]init];
    vc.notOnTab = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)pushCreatGroup {
    RCDCreateGroupViewController *vc = [[RCDCreateGroupViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)pushAddFriend {
    RCDSearchFriendViewController *vc = [[RCDSearchFriendViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)creatGourp {
    SHOW(@"创建群组");
   
    if (self.addGroupView.beizhuField.text.length == 0) {
        SHOW(@"请输入群组名称");
        return;
    }
}
-(void)backMove {
    [App_Delegate.tabbarVc gotoHome];
    [self.navigationController popViewControllerAnimated:YES];
    App_Delegate.chatTabbarVc = nil;
    App_Delegate.needLocalNoti = YES;
}
- (void)gotoOrder {
    self.selectedIndex = 1;
    CCBuyMineController *vc =  self.viewControllers[1];
    [vc refreshData];
}
- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_leftButton addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        self.navigationItem.leftBarButtonItem = left;
    }
    return _leftButton;
}

@end
