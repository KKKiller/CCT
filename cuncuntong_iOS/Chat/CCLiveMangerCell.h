//
//  CCLiveMangerCell.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/25.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCLiveMangerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabe;
@property (weak, nonatomic) IBOutlet UILabel *degreeLab;
@property (weak, nonatomic) IBOutlet UILabel *startDateLab;
@property (weak, nonatomic) IBOutlet UILabel *startTimelab;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@end

NS_ASSUME_NONNULL_END
