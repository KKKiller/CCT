//
//  LBAddFirendRequestCell.h
//  L8
//
//  Created by 周吾昆 on 2018/6/16.
//  Copyright © 2018年 周吾昆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LBFriendRequestModel.h"
#import "LBGroupModel.h"
@protocol LBAddFriendRequestCellDelegate<NSObject>
- (void)refuseButtonClick:(LBFriendRequestModel *)model;
- (void)agreeButtonClick:(LBFriendRequestModel *)model;
@end

@interface LBAddFriendRequestCell : UITableViewCell
@property (nonatomic, strong) UIImageView *leftImgView;
@property (strong, nonatomic) UILabel *nameLbl;
@property (nonatomic, strong) UIButton *refuseBtn;
@property (nonatomic, strong) UIButton *agreeBtn;
@property (strong, nonatomic) UILabel *lineLbl;
@property (nonatomic, strong) UIImageView *adminiLogo;
@property (nonatomic, assign) BOOL hiddenButton;


@property (nonatomic, weak) id<LBAddFriendRequestCellDelegate> delegate;
@property (nonatomic, strong) LBFriendRequestModel *friendRequestModel;
@property (nonatomic, strong) LBGroupModel *groupModel;
@end
