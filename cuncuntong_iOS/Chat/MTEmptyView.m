//
//  MTEmptyView.m
//  Mentor
//
//  Created by 我是MT on 16/4/2.
//  Copyright © 2016年 馒头科技. All rights reserved.
//

#import "MTEmptyView.h"
#import "UIButton+HFButton.h"
@interface MTEmptyView()
@property (copy, nonatomic) reloadBlock reloadBlock;
@property (copy, nonatomic) getMoreBtnBlock getMoreBtnBlock;
@end
@implementation MTEmptyView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatViews];
        [self layoutSubView];
        self.userInteractionEnabled = YES;
        self.backgroundColor= BACKGRAY;
    }
    return self;
}
- (instancetype)setImage:(UIImage *)image tipsText:(NSString *)tipsText getMoreBtnText:(NSString *)getMoreBtnText reloadBlock:(reloadBlock)reloadBlock getMoreBtnBlock:(getMoreBtnBlock)getMoreBtnBlock {
    self.imgView.image = image;
    self.tipsLbl.text = tipsText;
    [self.getMoreBtn setTitle:getMoreBtnText ? getMoreBtnText :@"" forState:UIControlStateNormal];
    self.getMoreBtn.hidden = !getMoreBtnText;
    self.reloadBlock = reloadBlock;
    self.getMoreBtnBlock = getMoreBtnBlock;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        self.reloadBlock();
    }];
    [self addGestureRecognizer:tap];
    [self.getMoreBtn addTarget:self action:@selector(getMoreBtnClick) forControlEvents:UIControlEventTouchUpInside];
    return self;
}
- (void)getMoreBtnClick {
    self.getMoreBtnBlock();
}
- (void)setIsNoNet:(BOOL)isNoNet {
    _isNoNet = isNoNet;
    if (isNoNet) {
        self.imgView.image = IMAGENAMED(@"noNet");
        self.tipsLbl.text = @"你好像站到了信号的背面";
        self.getMoreLbl.text = @"点击空白处刷新";
        self.getMoreBtn.hidden = YES;            

    }
}
- (void)creatViews {
    self.imgView = [[UIImageView alloc]init];
    [self addSubview:self.imgView];
    self.imgView.image = IMAGENAMED(@"noData");
    
    self.tipsLbl = [[UILabel alloc]initWithText:@"" font:15 textColor:MTRGB(0xc5c5c5)];
    [self addSubview:self.tipsLbl];
    self.tipsLbl.textAlignment = NSTextAlignmentCenter;
    
    self.getMoreBtn = [[UIButton alloc]initWithTitle:@"" textColor:WHITECOLOR backImg:nil font:15];
    [self.getMoreBtn setBackgroundColor:MTRGB(0x8fe1be)];
    self.getMoreBtn.layer.cornerRadius = 17.5;
    self.getMoreBtn.layer.masksToBounds = YES;
    [self addSubview:self.getMoreBtn];
    self.getMoreBtn.hidden = YES;
    
    self.getMoreLbl = [[UILabel alloc]initWithText:@"" font:15 textColor:MTRGB(0xc5c5c5)];
    [self addSubview:self.getMoreLbl];
}

- (void)layoutSubView {
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-50);
        make.width.height.mas_equalTo(100);
    }];
    [self.tipsLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.imgView.mas_bottom).offset(20);
        make.width.mas_equalTo(App_Width-30);
    }];
    [self.getMoreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.tipsLbl.mas_bottom).offset(15);
        make.width.mas_equalTo(155);
        make.height.mas_equalTo(35);
    }];
    [self.getMoreLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.tipsLbl.mas_bottom).offset(6);
    }];
}
@end
