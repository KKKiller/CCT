//
//  CCLiveManagePopView.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/25.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCLiveManagePopView : UIView
@property (weak, nonatomic) IBOutlet UIButton *liveBtn;
@property (weak, nonatomic) IBOutlet UIButton *videoBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
+ (instancetype)instanceView;
@end

NS_ASSUME_NONNULL_END
