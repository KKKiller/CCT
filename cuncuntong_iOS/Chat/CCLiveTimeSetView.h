//
//  CCLiveTimeSetView.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/25.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCLiveTimeSetView : UIView
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (strong, nonatomic) IBOutletCollection(UITapGestureRecognizer) NSArray *gesColl;
+ (instancetype)instanceView;
@end

NS_ASSUME_NONNULL_END
