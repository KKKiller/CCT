//
//  THChatListController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "THChatListController.h"
#import "THMessageCell.h"
#import "THChatController.h"
#import "CCChatAddMsgController.h"
#import "PopoverView.h"
#import "RCDCreateGroupViewController.h"
@interface THChatListController ()<UITableViewDelegate,UITableViewDataSource,XMPPStreamDelegate>
@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) CCEmptyView *emptyView;

@end

@implementation THChatListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [NSMutableArray array];
//    [self.view addSubview:self.searchFriendsBar];
    [self.view addSubview:self.table];
    [[XmppTools sharedManager].xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [NOTICENTER addObserver:self selector:@selector(reloadContacts) name:kFriendRequestNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = @"家园";
    [self setRightItemWithImagename:@"icon_add_white"];
    [self setBackItemWithImagename:@"btn_back"];
    self.navigationController.navigationBar.tintColor = WHITECOLOR;
    
    self.navigationController.navigationBarHidden = NO;
    [self reloadContacts];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.table reloadData];
    });
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    THMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"THMessageCell"];
    id data = [self.dataSource objectAtIndex:indexPath.row];
    if ([data isKindOfClass:[XMPPMessageArchiving_Contact_CoreDataObject class]]) {
        cell.data = data;
    }else{
        cell.friendRequestModel = [LBFriendRequestModel modelWithJSON:data];
    }
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    id data = [self.dataSource objectAtIndex:indexPath.row];
    if ([data isKindOfClass:[XMPPMessageArchiving_Contact_CoreDataObject class]]) {
        THChatController *vc = [[THChatController alloc]init];
        vc.toJid = ((XMPPMessageArchiving_Contact_CoreDataObject *)data).bareJid;
        [self.navigationController pushViewController:vc animated:TRUE];
    }else{
        LBFriendRequestModel *model = [LBFriendRequestModel modelWithJSON:data];
        CCChatAddMsgController *vc = [[CCChatAddMsgController alloc]init];
        vc.uid = model.uid;
        vc.name = model.nickname;
        vc.addMsg = @"请求添加您为好友";
        vc.finishBlk = ^(BOOL isAccept) {
            [self reloadContacts];
            if (isAccept) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    THChatController *vc = [[THChatController alloc]init];
                    vc.toUserId = model.uid;
                    vc.toJid = [XMPPTOOL getJIDWithUserId:model.uid];
                    [self.navigationController pushViewController:vc animated:YES];
                });
            }
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UITableView*)table{
    if (!_table) {
        _table = [[UITableView alloc]initWithFrame:CGRectMake(0, WCFNavigationHeight, App_Width, App_Height - WCFNavigationHeight - WCFTabBarHeight) style:UITableViewStylePlain];
        [_table registerClass:[THMessageCell class] forCellReuseIdentifier:@"THMessageCell"];
        _table.delegate = self;
        _table.dataSource = self;
        [_table setBackgroundColor:RGB(244, 244, 244)];
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}

- (void)rightBarButtomItemClick:(UIBarButtonItem *)item{
    RCDCreateGroupViewController *vc = [[RCDCreateGroupViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    return;
    // 附带左边图标的
    PopoverAction *action1 = [PopoverAction actionWithImage:IMAGENAMED(@"btn_golden_beans") title:@"金豆" handler:^(PopoverAction *action) {
        // 该Block不会导致内存泄露, Block内代码无需刻意去设置弱引用.
    }];
    PopoverAction *action2 = [PopoverAction actionWithImage:IMAGENAMED(@"icon_red_envelopes") title:@"红包" handler:^(PopoverAction *action) {
        // 该Block不会导致内存泄露, Block内代码无需刻意去设置弱引用.
    }];
    PopoverView *popoverView = [PopoverView popoverView];
    //popoverView.showShade = YES; // 显示阴影背景
    popoverView.style = PopoverViewStyleDark; // 设置为黑色风格
    //popoverView.hideAfterTouchOutside = NO; // 点击外部时不允许隐藏
    // 有两种显示方法
    // 1. 显示在指定的控件
    [popoverView showToPoint:CGPointMake(App_Width - 10 - 12.5, WCFNavigationHeight - 10) withActions:@[action1, action2]];
//    [popoverView showToView:item withActions:@[action1, action2]];
}

- (void)backClick:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)deleteBtnClick:(XMPPMessageArchiving_Contact_CoreDataObject *)data{
    [XMPPTOOL.messageArchivingCoreDataStorage.mainThreadManagedObjectContext deleteObject:data];
    [self reloadContacts];
}



#pragma mark - 收到消息
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    if (message.body) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self reloadContacts];
        });
    }
}
//获取聊天列表数据
- (void)reloadContacts{
    NSManagedObjectContext *context = [XmppTools sharedManager].messageArchivingCoreDataStorage.mainThreadManagedObjectContext;
    
    // 2.FetchRequest【查哪张表】
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"XMPPMessageArchiving_Contact_CoreDataObject"];
    //创建查询条件
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr = %@", [XmppTools sharedManager].userJid.bare];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mostRecentMessageTimestamp" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (self.dataSource != nil) {
        if ([self.dataSource count] > 0) {
            [self.dataSource removeAllObjects];
        }
    }
    if(!error && fetchedObjects.count > 0){
        
        [self.dataSource addObjectsFromArray:fetchedObjects];
    }
    if([XMPPTOOL getFriendRequestArray].count > 0){
        [self.dataSource insertObjects:[XMPPTOOL getFriendRequestArray] atIndex:0];
    }
    [self.table reloadData];
    self.emptyView.hidden = self.dataSource.count != 0;
}
#pragma - 懒加载
- (UISearchBar *)searchFriendsBar {
    if (!_searchFriendsBar) {
        _searchFriendsBar=[[UISearchBar alloc]initWithFrame:CGRectMake(2, WCFNavigationHeight, App_Width-4, 28)];
        [_searchFriendsBar sizeToFit];
        [_searchFriendsBar setPlaceholder:NSLocalizedStringFromTable(@"ToSearch", @"RongCloudKit", nil)];
        [_searchFriendsBar.layer setBorderWidth:0.5];
        [_searchFriendsBar.layer setBorderColor:[UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1].CGColor];
        [_searchFriendsBar setDelegate:self];
        [_searchFriendsBar setKeyboardType:UIKeyboardTypeDefault];
    }
    return _searchFriendsBar;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - WCFNavigationHeight - WCFTabBarHeight)];
        [_table addSubview:_emptyView];
    }
    return _emptyView;
}
@end
