//
//  LBAddFirendRequestCell.m
//  L8
//
//  Created by 周吾昆 on 2018/6/16.
//  Copyright © 2018年 周吾昆. All rights reserved.
//

#import "LBAddFriendRequestCell.h"

@implementation LBAddFriendRequestCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self creatViews];
        [self layoutSubView];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}
- (void)setHiddenButton:(BOOL)hiddenButton {
    _hiddenButton = hiddenButton;
    self.refuseBtn.hidden = hiddenButton;
    self.agreeBtn.hidden = hiddenButton;
}
- (void)setFriendRequestModel:(LBFriendRequestModel *)friendRequestModel {
    _friendRequestModel = friendRequestModel;
    [self.leftImgView setImageURL:URL(friendRequestModel.avatar)];
    self.nameLbl.text = friendRequestModel.nickname;
    self.refuseBtn.hidden = friendRequestModel.is_accept;
    self.agreeBtn.userInteractionEnabled = !friendRequestModel.is_accept;
    if (friendRequestModel.is_accept == 1) {
        [self.agreeBtn setTitle:@"已添加" forState:UIControlStateNormal];
        [self.agreeBtn setBackgroundColor:[UIColor lightGrayColor]];
    }else if(friendRequestModel.is_accept == 2){
        [self.agreeBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
        [self.agreeBtn setBackgroundColor:[UIColor lightGrayColor]];
    }else{
        [self.agreeBtn setTitle:@"同意" forState:UIControlStateNormal];
        [self.agreeBtn setBackgroundColor:RGBCOLOR(56, 126, 255)];
    }
}
- (void)setGroupModel:(LBGroupModel *)groupModel {
    _groupModel = groupModel;
    [self.leftImgView setImageURL:URL(groupModel.avatar)];
    self.nameLbl.text = groupModel.group;
    self.adminiLogo.hidden = !groupModel.owner;

}
- (void)creatViews {
    self.leftImgView = [[UIImageView alloc]init];
    self.leftImgView.image = IMAGENAMED(@"contact");
    [self.contentView addSubview:self.leftImgView];
    self.leftImgView.layer.cornerRadius = 4;
    self.leftImgView.layer.masksToBounds = YES;
    
    self.nameLbl = [[UILabel alloc]initWithText:@"好友请求" font:14 textColor:TEXTBLACK6];
    [self addSubview:self.nameLbl];


    self.agreeBtn = [[UIButton alloc]initWithTitle:@"同意" textColor:WHITECOLOR backImg:nil font:14];
    [self.agreeBtn setBackgroundColor:RGBCOLOR(56, 126, 255)];
    [self.contentView addSubview:self.agreeBtn];
    self.agreeBtn.layer.cornerRadius = 4;
    self.agreeBtn.layer.masksToBounds = YES;
    [self.agreeBtn addTarget:self action:@selector(agreeBtnClick) forControlEvents:UIControlEventTouchUpInside ];
    
    self.refuseBtn = [[UIButton alloc]initWithTitle:@"拒绝" textColor:WHITECOLOR backImg:nil font:14];
    [self.refuseBtn setBackgroundColor:MAINCOLOR];
    [self.contentView addSubview:self.refuseBtn];
    self.refuseBtn.layer.cornerRadius = 4;
    self.refuseBtn.layer.masksToBounds = YES;
    [self.refuseBtn addTarget:self action:@selector(refuseBthClick) forControlEvents:UIControlEventTouchUpInside ];

    
    self.lineLbl = [[UILabel alloc]initWithShallowLine];
    [self addSubview:self.lineLbl];
    self.adminiLogo = [[UIImageView alloc]init];
    [self addSubview:self.adminiLogo];
    self.adminiLogo.image = IMAGENAMED(@"profile_rank_first");
    self.adminiLogo.hidden = YES;

}
- (void)agreeBtnClick {
    if ([self.delegate respondsToSelector:@selector(agreeButtonClick:)]) {
        [self.delegate agreeButtonClick:self.friendRequestModel];
    }
}
- (void)refuseBthClick{
    if ([self.delegate respondsToSelector:@selector(refuseButtonClick:)]) {
        [self.delegate refuseButtonClick:self.friendRequestModel];
    }
}
- (void)layoutSubView {
    [self.leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.width.height.mas_equalTo(40);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImgView.mas_right).offset(15);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [self.agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [self.refuseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.agreeBtn.mas_left).offset(-20);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    [self.adminiLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.width.height.mas_equalTo(20);
        make.centerY.equalTo(self.mas_centerY);
    }];
}
@end
