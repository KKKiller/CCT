//
//  CCLiveMangerCell.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/25.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import "CCLiveMangerCell.h"

@implementation CCLiveMangerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _btn.layer.cornerRadius = 4;
    _btn.layer.borderWidth = 1;
    _btn.layer.borderColor = MTRGB(0x387DFF).CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
