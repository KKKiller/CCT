//
//  CCLiveDealWaintingController.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/16.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import "CCLiveDealWaintingController.h"
#import "CCLiveDealWaitingCell.h"
#import "CCLiveApplyChildController.h"

@interface CCLiveDealWaintingController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CCLiveDealWaintingController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.tableFooterView = [UIView new];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 100.0f;
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCLiveDealWaitingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CCLiveDealWaitingCell" forIndexPath:indexPath];
    cell.nameLabe.text = @"海阔天空";
    cell.btn.tag = 10000 + indexPath.row;
    cell.degreeLab.text = @"B级";
    cell.startDateLab.text = @"2018-02-19";
    cell.startTimelab.text = @"12:30";
    cell.contentLab.text = @"我岌岌可危金额为空金额为饥饿我看完我就鄂温克金额可畏惧科技文科接口";
    [cell.btn addTarget:self action:@selector(checkAction:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)checkAction:(UIButton *)sender{
    CCLiveApplyChildController *Vc = [[UIStoryboard storyboardWithName:@"Live" bundle:nil] instantiateViewControllerWithIdentifier:@"CCLiveApplyChildController"];
    [self.navigationController pushViewController:Vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
