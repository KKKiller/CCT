//
//  CCLiveManagerPopTView.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/25.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCLiveManagerPopTView : UIView
@property (weak, nonatomic) IBOutlet UIButton *modifyBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
+ (instancetype)instanceView;
@end

NS_ASSUME_NONNULL_END
