//
//  RCDContactSelectedTableViewController.m
//  RCloudMessage
//
//  Created by Jue on 16/3/17.
//  Copyright © 2016年 RongCloud. All rights reserved.
//

#import "RCDContactSelectedTableViewController.h"
#import "DefaultPortraitView.h"
#import "MBProgressHUD.h"
//#import "RCDChatViewController.h"
#import "RCDContactSelectedTableViewCell.h"
#import "RCDCreateGroupViewController.h"
//#import "RCDHttpTool.h"
//#import "RCDRCIMDataSource.h"
#import "RCDUserInfo.h"
//#import "RCDataBaseManager.h"
#import "UIImageView+WebCache.h"
#import "pinyin.h"
#import "UIColor+RCColor.h"
//#import "RCDUserInfoManager.h"
#import "RCDUtilities.h"
#import "RCDUIBarButtonItem.h"
#import "RCDNoFriendView.h"
#import "CCChatController.h"
#import "RCDContactSelectedCollectionViewCell.h"


@interface RCDContactSelectedTableViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate>
@property(nonatomic, strong) NSMutableArray *friends;

@property(strong, nonatomic) NSMutableArray *friendsArr;

@property(nonatomic, strong) NSMutableArray *tempOtherArr;

@property (nonatomic, strong) RCDUIBarButtonItem *rightBtn;

@property(nonatomic, strong) NSIndexPath *selectIndexPath;

@property (nonatomic, strong) NSMutableArray *discussionGroupMemberIdList;

@property (nonatomic, strong) RCDNoFriendView *noFriendView;

@property(nonatomic, strong) UICollectionView *selectedUsersCollectionView;

@property(nonatomic, strong) NSMutableArray *collectionViewResource;

@property(nonatomic, strong) UITableView *tableView;

//进入页面以后选中的userId的集合
@property(nonatomic, strong) NSMutableArray *selecteUserIdList;

//collectionView展示的最大数量
@property(nonatomic, assign) NSInteger maxCount;

//判断当前操作是否是删除操作
@property(nonatomic, assign) BOOL isDeleteUser;

//搜索出的结果数据集合
@property(nonatomic, strong) NSMutableArray *matchSearchList;

//是否是显示搜索的结果
@property(nonatomic, assign) BOOL isSearchResult;

//tableView中indexPath和userId的对应关系字典
@property(nonatomic, strong) NSMutableDictionary *indexPathDic;

@property(nonatomic, strong) UITextField *searchField;

@property(nonatomic, strong) UIView *searchBarLeftView;

@property(nonatomic, strong) NSString *searchContent;

@end

@implementation RCDContactSelectedTableViewController
MBProgressHUD *hud;

- (void)viewDidLoad {
  [super viewDidLoad];

  self.navigationItem.title = _titleStr;
  self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
  self.isDeleteUser = NO;
  self.view.backgroundColor = [UIColor whiteColor];
  
  [self createTableView];
  [self createCollectionView];
  
  [self createSearchBar];
  self.matchSearchList = [NSMutableArray new];
  //自定义rightBarButtonItem
  self.rightBtn =
  [[RCDUIBarButtonItem alloc] initWithbuttonTitle:@"确定"
                                       titleColor:[UIColor colorWithHexString:@"000000" alpha:1.0]
                                      buttonFrame:CGRectMake(0, 0, 90, 30)
                                           target:self
                                           action:@selector(clickedDone:)];
  self.rightBtn.button.titleLabel.font = [UIFont systemFontOfSize:16];
  [self.rightBtn.button setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
  [self.rightBtn buttonIsCanClick:NO
                      buttonColor:[UIColor colorWithHexString:@"9fcdfd" alpha:1.0]
                    barButtonItem:self.rightBtn];
  self.navigationItem.rightBarButtonItems = [self.rightBtn
                                             setTranslation:self.rightBtn
                                             translation:-11];
  
  self.selecteUserIdList = [NSMutableArray new];
  self.indexPathDic = [NSMutableDictionary new];
  
  [self setMaxCountForDevice];
  
  self.searchContent = @"";
}
#pragma mark - 添加
// 确定
- (void)clickedDone:(id)sender {
    [self.rightBtn buttonIsCanClick:NO
                        buttonColor:[UIColor colorWithHexString:@"9fcdfd" alpha:1.0]
                      barButtonItem:self.rightBtn];
//    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.color = [UIColor colorWithHexString:@"343637" alpha:0.5];
//    [hud show:YES];
    
    
    NSMutableArray *seletedUsers = [NSMutableArray new];
    NSMutableArray *seletedUsersId = [NSMutableArray new];
    for (RCUserInfo *user in self.collectionViewResource) {
        [seletedUsersId addObject:user.userId];
    }
    seletedUsers = [self.collectionViewResource mutableCopy];
    if (_selectUserList) {
        NSArray<RCUserInfo *> *userList = [NSArray arrayWithArray:seletedUsers];
        _selectUserList(userList);
        return;
    }
    
    if (seletedUsersId.count > 0) {
        NSLog(@"%@",seletedUsersId);
        SHOW(@"入群邀请发送成功");
        //批量加人
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//        [dict setValue:STR(self.groupId) forKey:@"gid"];
        for (int i = 0; i<seletedUsersId.count; i++) {
//            NSString *key = [NSString stringWithFormat:@"uids[%@]",@(i)];
//            [dict setValue:STR(seletedUsersId[i]) forKey:key];
            
            
            NSString *targetId = seletedUsersId[i];
            NSString *groupId =  [NSString stringWithFormat:@"group_%@",self.groupId];;
            NSString *msg = [NSString stringWithFormat:@"%@邀请你加入群聊%@",[UserManager sharedInstance].realname,self.groupName];
            RCContactNotificationMessage *message = [[RCContactNotificationMessage alloc]init];
            message.operation =  ContactNotificationMessage_ContactOperationRequest;
            message.sourceUserId = USERID;
            message.targetUserId = targetId;
            message.message = msg;
            message.extra = groupId;
            message.mentionedInfo.mentionedContent = msg;
            [[RCIM sharedRCIM] sendMessage:ConversationType_SYSTEM targetId:targetId content:message pushContent:msg pushData:nil success:^(long messageId) {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self.navigationController popViewControllerAnimated:YES];
                    for (UIViewController *vc in self.navigationController.viewControllers) {
                        if ([vc isKindOfClass:[CCChatTabbarController class]]) {
                            [self.navigationController popToViewController:vc animated:YES];
                        }
                    }
                });
            } error:^(RCErrorCode nErrorCode, long messageId) {
                NSLog(@"%ld",nErrorCode);
            }];
        }
        
        
        
//        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_members_add") params:dict  target:self success:^(NSDictionary *success) {
//            if ([success[@"stat"] integerValue] == 1) {
//                SHOW(@"添加成功");
//                NSString *groupId = [NSString stringWithFormat:@"group_%@",self.groupId];
//                RCTextMessage *message = [RCTextMessage messageWithContent:@"欢迎新朋友加入群聊"];
//                [[RCIM sharedRCIM] sendMessage:ConversationType_GROUP targetId:groupId content:message pushContent:nil pushData:nil success:^(long messageId) {
//                    dispatch_async_on_main_queue(^{
//                        for (UIViewController *vc in self.navigationController.viewControllers) {
//                            if ([vc isKindOfClass:[CCChatTabbarController class]]) {
//                                [self.navigationController popToViewController:vc animated:YES];
//                            }
//                        }
//                    });
//                } error:^(RCErrorCode nErrorCode, long messageId) {
//                    
//                }];
//            }else{
//                SHOW(success[@"info"]);
//            }
//        } failure:^(NSError *failure) {
//            
//        }];
    }else{
        SHOW(@"请选择好友");
    }
    
}
//用户列表, 好友 获取
//获取好友
- (void )getAllData {
    
    [self setData:[TOOL getCachaDataWithName:@"contacts"]];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_list") params:@{@"uid":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
            [self setData:success];
            [TOOL saveDataWithData:success Name:@"contacts"];
        }else{
            SHOW(success[@"info"]);
        }
        
    } failure:^(NSError *failure) {
    }];
    
}
- (void)setData:(NSDictionary *)success {
    _allFriends = [NSMutableDictionary new];
    _allKeys = [NSMutableArray new];
    _friendsArr = [NSMutableArray new];
    self.friends = [NSMutableArray array];
    NSMutableArray *members = [NSMutableArray array];
    if ([success[@"info"] isKindOfClass:[NSDictionary class]] && [success[@"info"] allKeys].count > 0) {
        for (NSArray *array in [success[@"info"] allValues]) {
            [members addObjectsFromArray:array];
        }
        for (NSDictionary *dict in members) {
            RCDUserInfo *user = [[RCDUserInfo alloc]init];
            user.portraitUri = dict[@"portrait"];
            user.name = dict[@"realname"];
            user.userId = [NSString stringWithFormat:@"%@", dict[@"rid"]];
            [self.friends addObject:user];
        }
        
    }

    [self dealWithFriendList];
    
}
//- (void)getAllData {
//    
//    NSMutableArray *friendList = [[NSMutableArray alloc] init];
//    for (int i = 0; i<20; i++) {
//        RCDUserInfo *user = [[RCDUserInfo alloc]init];
//        if (i % 2 == 0) {
//            user.name = [NSString stringWithFormat:@"2B-%@",@(i)];
//        }else{
//            user.name = [NSString stringWithFormat:@"安若宸%@",@(i)];
//        }
//        user.userId = [NSString stringWithFormat:@"%@",@(234+i)];
//        user.portraitUri = [UserManager sharedInstance].portrait;
//        [friendList addObject:user];
//    }
//    self.friends = friendList;
//    [self dealWithFriendList];
//}


- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  if ([_allFriends count] <= 0) {
    [self getAllData];
  }
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [self.rightBtn buttonIsCanClick:YES
                      buttonColor:[UIColor whiteColor]
                    barButtonItem:self.rightBtn];
  [hud hide:YES];
}

- (void)createTableView {
  self.tableView = [UITableView new];
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
    
//  self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
//     self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
  self.tableView.frame = CGRectMake(0, 54 + 64 , [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height  - 54 - 64);
  [self.view addSubview:self.tableView];
  
  //控制多选
  self.tableView.allowsMultipleSelection = YES;
//  if (_isAllowsMultipleSelection == NO) {
//    self.tableView.allowsMultipleSelection = NO;
//  }
  
  UIView *separatorLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
  separatorLine.backgroundColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1];
  self.tableView.tableHeaderView = separatorLine;
  
  self.tableView.tableFooterView = [UIView new];
}

- (void)createCollectionView {
  self.collectionViewResource = [NSMutableArray new];
  CGRect tempRect = CGRectMake(
                               0, 64, 0, 54);
  UICollectionViewFlowLayout *flowLayout =
  [[UICollectionViewFlowLayout alloc] init];
  flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  self.selectedUsersCollectionView = [[UICollectionView alloc] initWithFrame:tempRect
                                         collectionViewLayout:flowLayout];
  self.selectedUsersCollectionView.delegate = self;
  self.selectedUsersCollectionView.dataSource = self;
  self.selectedUsersCollectionView.scrollEnabled = YES;
  self.selectedUsersCollectionView.backgroundColor = [UIColor whiteColor];
  [self.selectedUsersCollectionView registerClass:[RCDContactSelectedCollectionViewCell class]
        forCellWithReuseIdentifier:@"RCDContactSelectedCollectionViewCell"];
  [self.view addSubview:self.selectedUsersCollectionView];
  self.selectedUsersCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)createSearchBar {
  self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width,54)];
  [self.searchBar setText:NSLocalizedStringFromTable(@"ToSearch", @"RongCloudKit", nil)];
  self.searchField = [self.searchBar valueForKey:@"_searchField"];
  self.searchField.clearButtonMode = UITextFieldViewModeNever;
  self.searchField.textColor = [UIColor colorWithHexString:@"999999" alpha:1.0];
  self.searchBarLeftView = self.searchField.leftView;
  
  [self.searchBar setDelegate:self];
  [self.searchBar setKeyboardType:UIKeyboardTypeDefault];
  
  for (UIView *subview in [[self.searchBar.subviews firstObject] subviews]) {
    if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
      [subview removeFromSuperview];
    }
  }
  [self.view addSubview:self.searchBar];
}

- (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height
{
  CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
  UIGraphicsBeginImageContext(r.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  CGContextSetFillColorWithColor(context, [color CGColor]);
  CGContextFillRect(context, r);
  
  UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return img;
}

- (CGRect)getSearchBarFrame :(CGRect)frame{
  CGRect searchBarFrame = CGRectZero;
  frame.origin.x = frame.size.width;
  CGFloat searchBarWidth = [UIScreen mainScreen].bounds.size.width - frame.size.width;
  frame.size.width = searchBarWidth;
  searchBarFrame = frame;
  return searchBarFrame;
}

//设置collectionView和searchBar实时显示的frame效果
- (void)setCollectonViewAndSearchBarFrame :(NSInteger)count{
//  CGRect frame = CGRectZero;
//  if (count == 0) {
//    //只显示searchBar
//    frame = CGRectMake(0, 0, 0, 54);
//    self.selectedUsersCollectionView.frame = frame;
//    self.searchBar.frame = [self getSearchBarFrame:frame];
//    self.searchField.leftView = self.searchBarLeftView;
//    self.searchBar.text = @"搜索";
//  } else if (count == 1) {
//    frame = CGRectMake(0, 0, 46, 54);
//    self.selectedUsersCollectionView.frame = frame;
//    self.searchBar.frame = [self getSearchBarFrame:frame];
//    self.searchField.leftView = nil;
//  } else if (count > 1 && count <= self.maxCount) {
//    if (self.isDeleteUser == NO) {
//      //如果是删除选中的联系人时候的处理
//      frame = CGRectMake(0, 0, 46 + (count - 1) * 46, 54);
//      self.selectedUsersCollectionView.frame = frame;
//      self.searchBar.frame = [self getSearchBarFrame:frame];
//    } else if (self.isDeleteUser == YES){
//      if (count < self.maxCount) {
//        //判断如果当前collectionView的显示数量小于最大展示数量的时候，collectionView和searchBar的frame都会改变
//        frame = CGRectMake(0, 0, 61 + (count - 1) * 46, 54);
//        self.selectedUsersCollectionView.frame = frame;
//        self.searchBar.frame = [self getSearchBarFrame:frame];
//      }
//    }
//  }
}



- (void)setDefaultDisplay {
  self.isSearchResult = NO;
  [self.tableView reloadData];
  if (self.collectionViewResource.count < 1) {
    self.searchField.leftView = self.searchBarLeftView;
  }
  self.searchBar.text = @"搜索";
  self.searchContent = @"";
  [self.searchBar resignFirstResponder];
}

-(void)setMaxCountForDevice {
  if (kScreenWidth < 375) {
    self.maxCount = 5;
  } else if (kScreenWidth >=375 && kScreenWidth < 414) {
    self.maxCount = 6;
  } else {
    self.maxCount = 7;
  }
}

- (void)setRightButton {
  NSString *titleStr;
  if (self.selecteUserIdList.count > 0) {
    titleStr = [NSString stringWithFormat:@"确定(%zd)",[self.selecteUserIdList count]];
  } else {
    titleStr =@"确定";
    [self.rightBtn buttonIsCanClick:NO
                        buttonColor:[UIColor colorWithHexString:@"9fcdfd" alpha:1.0]
                      barButtonItem:self.rightBtn];
  }
  [self.rightBtn.button setTitle:titleStr forState:UIControlStateNormal];
}

- (void)closeKeyboard {
  if ([self.searchBar isFirstResponder]) {
    [self.searchBar resignFirstResponder];
  }
  if (self.collectionViewResource.count < 1) {
    self.searchField.leftView = self.searchBarLeftView;
  }
  if (self.searchContent.length < 1) {
    self.searchBar.text = @"搜索";
  }
  if (self.isSearchResult == YES) {
    dispatch_async(dispatch_get_main_queue(), ^{
      [self setDefaultDisplay];
    });
  }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  if (self.isSearchResult == NO) {
    return [_allKeys count];
  }
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  if (self.isSearchResult == NO) {
    NSString *key = [_allKeys objectAtIndex:section];
    NSArray *arr = [_allFriends objectForKey:key];
    return [arr count];
  }
  return self.matchSearchList.count;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [RCDContactSelectedTableViewCell cellHeight];
}

// pinyin index
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
  if (self.isSearchResult == NO) {
    return _allKeys;
  }
  return nil;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
  if (self.isSearchResult == NO) {
    NSString *key = [_allKeys objectAtIndex:section];
    return key;
  }
  return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *cellReuseIdentifier = @"RCDContactSelectedTableViewCell";
    
  RCDContactSelectedTableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
    if(!cell){
        cell = [[RCDContactSelectedTableViewCell alloc]init];
    }

  [cell setUserInteractionEnabled:YES];
  [cell.nicknameLabel setFont:[UIFont fontWithName:@"Heiti SC" size:14.0]];
  RCDUserInfo *user;
  if (self.isSearchResult == NO) {
    NSString *key = [self.allKeys objectAtIndex:indexPath.section];
    NSArray *arrayForKey = [self.allFriends objectForKey:key];
    
    user = arrayForKey[indexPath.row];
  } else {
    user = [self.matchSearchList objectAtIndex:indexPath.row];
  }
  
  //给控件填充数据
  [cell setModel:user];
  //设置选中状态
  BOOL isSelected = NO;
  for (NSString *userId in self.selecteUserIdList) {
    if ([user.userId isEqualToString:userId]) {
      [tableView selectRowAtIndexPath:indexPath
                             animated:NO
                       scrollPosition:UITableViewScrollPositionNone];
      isSelected = YES;
    }
  }
  if (isSelected == NO) {
    [tableView deselectRowAtIndexPath:indexPath
                             animated:NO];
  }
    if(_isHideSelectedIcon){
        cell.selectedImageView .hidden = YES;
    }
    if ([self isContain:user.userId] == YES) {
      [cell setUserInteractionEnabled:NO];
      dispatch_async(dispatch_get_main_queue(), ^{
        cell.selectedImageView.image = [UIImage imageNamed:@"disable_select"];
      });
      cell.userInteractionEnabled = NO;
    }
  return cell;
}

// override delegate
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [self.rightBtn buttonIsCanClick:YES
                      buttonColor:[UIColor whiteColor]
                    barButtonItem:self.rightBtn];
  RCDContactSelectedTableViewCell *cell =
  (RCDContactSelectedTableViewCell *)[tableView
                                      cellForRowAtIndexPath:indexPath];
    [cell setSelected:YES];
    //  if (self.selectIndexPath && self.selectIndexPath.row == indexPath.row) {
    if(self.selectIndexPath && [self.selectIndexPath compare:indexPath]==NSOrderedSame){
      [cell setSelected:NO];
      self.selectIndexPath = nil;
    } else {
      RCUserInfo *user;
      if (self.isSearchResult == YES) {
        user = [self.matchSearchList objectAtIndex:indexPath.row];
      } else {
        self.selectIndexPath = indexPath;
        NSString *key = [self.allKeys objectAtIndex:indexPath.section];
        NSArray *arrayForKey = [self.allFriends objectForKey:key];
        user = arrayForKey[indexPath.row];
      }
      [self.collectionViewResource addObject:user];
      NSInteger count = self.collectionViewResource.count;
      self.isDeleteUser = NO;
      [self setCollectonViewAndSearchBarFrame:count];
      [self.selectedUsersCollectionView reloadData];
      [self scrollToBottomAnimated:YES];
      [self.selecteUserIdList addObject:user.userId];
      [self setRightButton];
    }
    if (_selectUserList && self.isHideSelectedIcon) {
      NSMutableArray *seletedUsers = [NSMutableArray new];
      NSString *key = [self.allKeys objectAtIndex:indexPath.section];
      NSArray *arrayForKey = [self.allFriends objectForKey:key];
      RCDUserInfo *user = arrayForKey[indexPath.row];
      //转成RCDUserInfo
      RCUserInfo *userInfo = [RCUserInfo new];
      userInfo.userId = user.userId;
      userInfo.name = user.name;
      userInfo.portraitUri = user.portraitUri;
      [[RCIM sharedRCIM] refreshUserInfoCache:userInfo
                                   withUserId:userInfo.userId];
      [seletedUsers addObject:userInfo];
      [self setRightButton];
      NSArray<RCUserInfo *> *userList = [NSArray arrayWithArray:seletedUsers];
      _selectUserList(userList);
      return;
    }
    if (self.isSearchResult == YES) {
      [self setDefaultDisplay];
    }
//  }
}

- (void)tableView:(UITableView *)tableView
    didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
  RCDContactSelectedTableViewCell *cell =
      (RCDContactSelectedTableViewCell *)[tableView
          cellForRowAtIndexPath:indexPath];
//  if (self.isAllowsMultipleSelection == NO) {
//    
//  } else
//  {
    if (self.isSearchResult == YES) {
      [self setDefaultDisplay];
      return;
    }
    [cell setSelected:NO];
    self.selectIndexPath = nil;
    NSString *key = [self.allKeys objectAtIndex:indexPath.section];
    NSArray *arrayForKey = [self.allFriends objectForKey:key];
    RCDUserInfo *user = arrayForKey[indexPath.row];
    [self.collectionViewResource removeObject:user];
    [self.selecteUserIdList removeObject:user.userId];
    [self.selectedUsersCollectionView reloadData];
    NSInteger count = self.collectionViewResource.count;
    self.isDeleteUser = YES;
    [self setCollectonViewAndSearchBarFrame:count];
//    if(self.isAllowsMultipleSelection){
      [self setRightButton];
//    }
//  }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//  if (self.isAllowsMultipleSelection == NO) {
//    if ([self.searchBar isFirstResponder]) {
//      [self.searchBar resignFirstResponder];
//    }
//  } else {
    if (self.searchField.text.length == 0 && self.searchContent.length < 1) {
      [self setDefaultDisplay];
//    }
  }
}

#pragma mark - 获取好友并且排序
////排序
//- (void)sortAndRefreshWithList:(NSArray *)friendList {
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        self.resultDic = [RCDUtilities sortedArrayWithPinYinDic:friendList];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.allFriendSectionDic = self.resultDic[@"infoDic"];
//            [self.friendsTabelView reloadData];
//        });
//    });
//}


-(void)dealWithFriendList{
  for (int i = 0; i < _friends.count; i++) {
    RCDUserInfo *user = _friends[i];
//    if ([user.status isEqualToString:@"20"]) {
//        RCDUserInfo *friend = [[RCDUserInfo alloc]init];
//      RCUserInfo *friend = [[RCDUserInfoManager shareInstance] getFriendInfoFromDB:user.userId];
//      if (friend == nil) {
//        friend = [[RCDUserInfoManager shareInstance] generateDefaultUserInfo:user.userId];
//      }
      [_friendsArr addObject:user];
//    }
  }
  if (_friendsArr.count < 1) {
    CGRect frame = CGRectMake(0, 0, RCDscreenWidth, RCDscreenHeight - 64);
    self.noFriendView = [[RCDNoFriendView alloc] initWithFrame:frame];
    self.noFriendView.displayLabel.text = @"暂无好友";
    [self.view addSubview:self.noFriendView];
    [self.view bringSubviewToFront:self.noFriendView];
  } else {
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//      [self deleteGroupMembers];
      NSMutableDictionary *resultDic = [RCDUtilities sortedArrayWithPinYinDic:_friendsArr];
      dispatch_async(dispatch_get_main_queue(), ^{
        _allFriends = resultDic[@"infoDic"];
        _allKeys = resultDic[@"allKeys"];
        [self.tableView reloadData];
      });
    });
  }
}


- (BOOL)isContain:(NSString*)userId {
  BOOL contain = NO;
  NSArray *userList;
  if (_addGroupMembers.count > 0) {
    userList = _addGroupMembers;
  }
//  if (_addDiscussionGroupMembers.count > 0) {
//    userList = self.discussionGroupMemberIdList;
//  }
  for (NSString *memberId in userList) {
    if ([userId isEqualToString:memberId]) {
      contain = YES;
      break;
    }
  }
  return contain;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake(36, 36);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
  return 10;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
  UICollectionViewFlowLayout *flowLayout =
  (UICollectionViewFlowLayout *)collectionViewLayout;
  flowLayout.minimumInteritemSpacing = 10;
  flowLayout.minimumLineSpacing = 10;
  return UIEdgeInsetsMake(10, 10, 10, 0);
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [self.collectionViewResource count];
  
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  RCDContactSelectedCollectionViewCell *cell =
  [collectionView dequeueReusableCellWithReuseIdentifier:
   @"RCDContactSelectedCollectionViewCell"
                                            forIndexPath:indexPath];
  
  if (self.collectionViewResource.count > 0) {
    RCUserInfo *user = self.collectionViewResource[indexPath.row];
    [cell setUserModel:user];
  }
  cell.ivAva.contentMode = UIViewContentModeScaleAspectFill;
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  [self closeKeyboard];
  RCUserInfo *user = [self.collectionViewResource objectAtIndex:indexPath.row];
  [self.collectionViewResource removeObjectAtIndex:indexPath.row];
  [self.selecteUserIdList removeObject:user.userId];
  NSInteger count = self.collectionViewResource.count;
  self.isDeleteUser = YES;
  [self setCollectonViewAndSearchBarFrame:count];
  [self.selectedUsersCollectionView reloadData];
  for (NSIndexPath *indexPath in self.tableView.indexPathsForSelectedRows) {
    RCDContactSelectedTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if ([cell.nicknameLabel.text isEqualToString:user.name]) {
       [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
      dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
      });
    }
  }
//  if(self.isAllowsMultipleSelection){
    [self setRightButton];
//  }
}

- (void)scrollToBottomAnimated:(BOOL)animated {
  NSUInteger finalRow = MAX(0, [self.selectedUsersCollectionView numberOfItemsInSection:0] - 1);
  
  if (0 == finalRow) {
    return;
  }
  
  NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
  [self.selectedUsersCollectionView scrollToItemAtIndexPath:finalIndexPath atScrollPosition:UICollectionViewScrollPositionRight
                                                         animated:animated];
}

#pragma mark - UISearchBarDelegate
/**
 *  执行delegate联系人
 *
 *  @param searchBar  searchBar description
 *  @param searchText searchText description
 */
- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
  [self.matchSearchList removeAllObjects];
  if ([searchText isEqualToString:@""]) {
    self.isSearchResult = NO;
    [self.tableView reloadData];
    return;
  } else {
    for (RCUserInfo *userInfo in [_friendsArr copy]) {
      //忽略大小写去判断是否包含
      if ([userInfo.name rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound
          || [[RCDUtilities hanZiToPinYinWithString:userInfo.name] rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
        [self.matchSearchList addObject:userInfo];
      }
    }
      dispatch_async(dispatch_get_main_queue(), ^{
        self.isSearchResult = YES;
        [self.tableView reloadData];
      });
  }
  
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
  if ([self.searchField.text isEqualToString:@"搜索"] || [self.searchField.text isEqualToString:@"Search"]) {
    self.searchField.leftView = nil;
    self.searchField.text = @"";
  }
  return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
  if (self.collectionViewResource.count > 0) {
    self.searchField.leftView = nil;
  }
  return YES;
}
- (BOOL)searchBar:(UISearchBar *)searchBar   shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

  if ([text isEqualToString:@""] && self.searchContent.length > 1) {
    self.searchContent = [self.searchContent substringWithRange:NSMakeRange(0, self.searchContent.length - 1)];
  }
  else if ([text isEqualToString:@""] && self.searchContent.length == 1) {
    self.searchContent = @"";
    self.isSearchResult = NO;
    [self.tableView reloadData];
    return YES;
  }
  else if ([text isEqualToString:@"\n"]) {
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.searchBar resignFirstResponder];
    });
    return YES;
  }
  else {
    self.searchContent = [NSString stringWithFormat:@"%@%@",self.searchContent,text];
  }
  [self.matchSearchList removeAllObjects];
  NSString *temp = [self.searchContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
  ;
  if (temp.length <= 0) {
    self.matchSearchList = [_friendsArr mutableCopy];
  } else {
    for (RCUserInfo *userInfo in [_friendsArr copy]) {
      //忽略大小写去判断是否包含
      if ([userInfo.name rangeOfString:temp options:NSCaseInsensitiveSearch].location != NSNotFound
          || [[RCDUtilities hanZiToPinYinWithString:userInfo.name] rangeOfString:temp options:NSCaseInsensitiveSearch].location != NSNotFound) {
        [self.matchSearchList addObject:userInfo];
      }
    }
  }
  dispatch_async(dispatch_get_main_queue(), ^{
    self.isSearchResult = YES;
    [self.tableView reloadData];
  });
  return YES;
}

@end
