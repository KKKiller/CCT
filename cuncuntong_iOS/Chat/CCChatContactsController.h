//
//  CCChatContactsController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCChatContactsController : BaseViewController<UITableViewDataSource, UITableViewDelegate,
UISearchBarDelegate, UISearchControllerDelegate>

@property(nonatomic, strong) UISearchBar *searchFriendsBar;
@property(nonatomic, strong) UITableView *friendsTabelView;

@property(nonatomic, strong) NSDictionary *allFriendSectionDic;


@property(nonatomic, strong) NSArray *seletedUsers;

@property(nonatomic, strong) NSString *titleStr;

@property(nonatomic, strong) void (^selectUserList)(NSArray<RCUserInfo *> *selectedUserList);

@property (nonatomic, assign) BOOL notOnTab;

@end


/*
 @property (nonatomic, strong) UITableView *tableView;
 @property (nonatomic, strong) NSArray *userArray;
 @property (nonatomic, strong) NSArray *indexArray;
 
 @property(nonatomic, strong) UISearchBar *searchFriendsBar;
 @property(nonatomic, strong) UITableView *friendsTabelView;
 
 
 - (void)viewDidLoad
 {
 self.title = @"我的好友";
 [super viewDidLoad];
 [self.view addSubview:self.tableView];
 [self.view addSubview:self.searchFriendsBar];
 
 NSArray *stringsToSort=[NSArray arrayWithObjects:
 @"￥hhh, .$",@" ￥Chin ese ",@"开源中国 ",@"www.oschina.net",
 @"开源技术",@"社区",@"开发者",@"传播",
 @"2014",@"2013",@"100",@"中国",@"暑假作业",
 @"键盘", @"鼠标",@"hello",@"world",
 nil];
 
 self.indexArray = [ChineseString IndexArray:stringsToSort];
 self.userArray = [ChineseString LetterSortArray:stringsToSort];
 }
 
 
 #pragma mark - 边栏
 -(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
 return self.indexArray;
 }
 - (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
 return index;
 }
 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 NSString *key = [self.indexArray objectAtIndex:section];
 return key;
 }
 #pragma mark - tableView
 -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
 return [self.indexArray count];
 }
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
 return [[self.userArray objectAtIndex:section] count];
 }
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 static NSString *CellIdentifier = @"Cell";
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (cell == nil){
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 cell.imageView.image = IMAGENAMED(@"head");
 cell.textLabel.text = [[self.userArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
 return cell;
 }
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
 
 }
 
 -(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
 UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, App_Width, 20)];
 lab.font = FFont(12);
 lab.backgroundColor = MTRGB(0xf1f5f8);
 lab.text = [NSString stringWithFormat:@"   %@",[self.indexArray objectAtIndex:section]];
 lab.textColor = TEXTBLACK3;
 return lab;
 }
 - (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
 return 20;
 }
 - (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
 return CGFLOAT_MIN;
 }
 
 #pragma mark - 懒加载
 - (UITableView *)tableView {
 if (!_tableView) {
 _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 49) style:UITableViewStyleGrouped];
 _tableView.delegate = self;
 _tableView.dataSource =self;
 _tableView.rowHeight = 60;
 [_tableView setSectionIndexBackgroundColor:[UIColor clearColor]];
 }
 return _tableView;
 }
*/
