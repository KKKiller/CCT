//
//  THChatListController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface THChatListController : BaseViewController
@property(nonatomic, strong) UISearchBar *searchFriendsBar;

@end

NS_ASSUME_NONNULL_END
