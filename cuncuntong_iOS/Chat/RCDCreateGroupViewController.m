//
//  RCDCreateGroupViewController.m
//  RCloudMessage
//
//  Created by Jue on 16/3/21.
//  Copyright © 2016年 RongCloud. All rights reserved.
//

#import "RCDCreateGroupViewController.h"
#import "DefaultPortraitView.h"
#import "MBProgressHUD.h"
//#import "RCDGroupMemberCollectionViewCell.h"
//#import "RCDHttpTool.h"
#import "RCDUserInfo.h"
//#import "RCDataBaseManager.h"
#import "UIImageView+WebCache.h"
#import <RongIMKit/RongIMKit.h>
#import "RCDContactSelectedTableViewController.h"
#import "UIColor+RCColor.h"
//#import "RCDChatViewController.h"
//#import "RCDCommonDefine.h"

// 是否iPhone5
#define isiPhone5                                                              \
  ([UIScreen instancesRespondToSelector:@selector(currentMode)]                \
       ? CGSizeEqualToSize(CGSizeMake(640, 1136),                              \
                           [[UIScreen mainScreen] currentMode].size)           \
       : NO)
// 是否iPhone4
#define isiPhone4                                                              \
  ([UIScreen instancesRespondToSelector:@selector(currentMode)]                \
       ? CGSizeEqualToSize(CGSizeMake(640, 960),                               \
                           [[UIScreen mainScreen] currentMode].size)           \
       : NO)

@interface RCDCreateGroupViewController () {
  NSData *data;
  UIImage *image;

  MBProgressHUD *hud;
  CGFloat deafultY;
}
@property (nonatomic,strong) UIView *blueLine;
@end

@implementation RCDCreateGroupViewController

+ (instancetype)createGroupViewController {
    return [[[self class] alloc] init];
}

- (instancetype)init {
    self= [super init];
    if(self){
        self.view.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSubViews];
}
- (void)initSubViews {
    self.DoneBtn.hidden = YES;
    self.title = self.isEdit ? @"编辑群组" : @"创建群组";
   

    //群组头像的UIImageView
    CGFloat groupPortraitWidth = 100;
    CGFloat groupPortraitHeight = groupPortraitWidth;
    CGFloat groupPortraitX = RCDscreenWidth/2.0-groupPortraitWidth/2.0;
    CGFloat groupPortraitY = 80;
    self.GroupPortrait = [[UIImageView alloc] initWithFrame:CGRectMake(groupPortraitX, groupPortraitY, groupPortraitWidth, groupPortraitHeight)];
    self.GroupPortrait.image = [UIImage imageNamed:@"AddPhotoDefault"];
    self.GroupPortrait.layer.masksToBounds = YES;
    self.GroupPortrait.layer.cornerRadius = 5.f;
    //为头像设置点击事件
    self.GroupPortrait.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleClick =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(chosePortrait)];
    [self.GroupPortrait addGestureRecognizer:singleClick];
    
    //群组名称的UITextField
    CGFloat groupNameWidth = 200;
    CGFloat groupNameHeight = 17;
    CGFloat groupNameX = RCDscreenWidth/2.0-groupNameWidth/2.0;
    CGFloat groupNameY = CGRectGetMaxY(self.GroupPortrait.frame)+50;
    self.GroupName = [[UITextField alloc]initWithFrame:CGRectMake(groupNameX, groupNameY, groupNameWidth, groupNameHeight)];
    self.GroupName.font = [UIFont systemFontOfSize:14];
    self.GroupName.placeholder = @"填写群名称（2-10个字符）";
    self.GroupName.textAlignment = NSTextAlignmentCenter;
    self.GroupName.delegate = self;
    self.GroupName.returnKeyType = UIReturnKeyDone;
    
    
    //底部蓝线
    CGFloat blueLineWidth = 240;
    CGFloat blueLineHeight = 1;
    CGFloat blueLineX = RCDscreenWidth/2.0-blueLineWidth/2.0;
    CGFloat blueLineY = CGRectGetMaxY(self.GroupName.frame)+1;
    self.blueLine = [[UIView alloc]initWithFrame:CGRectMake(blueLineX, blueLineY, blueLineWidth, blueLineHeight)];
    self.blueLine.backgroundColor = [UIColor colorWithRed:0 green:135/255.0 blue:251/255.0 alpha:1];
    
    [self.view addSubview:self.GroupPortrait];
    [self.view addSubview:self.GroupName];
    [self.view addSubview:self.blueLine];
    
    //给整个view添加手势，隐藏键盘
    UITapGestureRecognizer *resetBottomTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboard:)];
    [self.view addGestureRecognizer:resetBottomTapGesture];
    
    //创建rightBarButtonItem
    NSString *titled = self.isEdit ? @"确定" : @"创建";
    UIBarButtonItem *item =
    [[UIBarButtonItem alloc] initWithTitle:titled
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(ClickDoneBtn:)];
    item.tintColor = [RCIM sharedRCIM].globalNavigationBarTintColor;
    self.navigationItem.rightBarButtonItem = item;
    
    CGFloat navHeight = 44.0f;
    CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    deafultY = navHeight + statusBarHeight;

}



- (void)ClickDoneBtn:(id)sender {
//    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self moveView:deafultY];
    [_GroupName resignFirstResponder];
    
    NSString *nameStr = [self.GroupName.text copy];
    nameStr = [nameStr
               stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(!image){
        SHOW(@"请选择图片");
        return;
    }
    image = [TOOL composeImg:image];
    //群组名称需要大于2位
    if ([nameStr length] == 0) {
        [self Alert:@"群组名称不能为空"];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    //群组名称需要大于2个字
    else if ([nameStr length] < 2) {
        [self Alert:@"群组名称过短"];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    //群组名称需要小于10个字
    else if ([nameStr length] > 10) {
        [self Alert:@"群组名称不能超过10个字"];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        NSData *imgdata = UIImagePNGRepresentation(image);
        if (self.isEdit) { //修改
            if ([self.group_id containsString:@"vill_"]) {
                SHOW(@"同村群不能修改");
            }else if ([self.group_id containsString:@"group_"]){
                self.group_id = [self.group_id substringFromIndex:6];
                [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"chat/group_update") params:@{@"gid":STR(self.group_id),@"group_name":nameStr,@"avatar":@"avatar"} target:self imgaeData:imgdata success:^(NSDictionary *success) {
                    if([success[@"stat"] integerValue] == 1){
                        
                        SHOW(@"修改成功");
                        //更新信息
                        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_info") params:@{@"gid":self.group_id} target:nil success:^(NSDictionary *success) {
                            if ([success[@"stat"] integerValue] == 1) {
                                RCGroup *group = [[RCGroup alloc]init];
                                group.groupId = [NSString stringWithFormat:@"group_%@",self.group_id];
                                group.groupName = success[@"info"][@"group_name"];
                                group.portraitUri = success[@"info"][@"avatar"];
                                [[RCIM sharedRCIM] refreshGroupInfoCache:group withGroupId:[NSString stringWithFormat:@"group_%@",self.group_id]];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [self.navigationController popViewControllerAnimated:YES];
                                });
                            }else{
                            }
                        } failure:^(NSError *failure) {
                        }];
                        
                        

                        [NOTICENTER postNotificationName:@"UpdeteGroupInfo" object:nameStr];
                    }else{
                        SHOW(success[@"info"]);
                    }
                    [TOOL hideLoading:self.view];
                } failure:^(NSError *failure) {
                    [TOOL hideLoading:self.view];
                }];
            }
            
        }else{ //创建
            [TOOL showLoading:self.view];
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"chat/group_add") params:@{@"uid":USERID,@"group_name":nameStr,@"avatar":@"avatar"} target:self imgaeData:imgdata success:^(NSDictionary *success) {
                if([success[@"stat"] integerValue] == 1){
                    
                    SHOW(@"创建成功");
                    NSString *gourpId = [NSString stringWithFormat:@"group_%@",success[@"info"][@"gid"]];
                    RCTextMessage *message = [RCTextMessage messageWithContent:@"欢迎加入群组"];
                    
                    [[RCIM sharedRCIM] sendMessage:ConversationType_GROUP targetId:gourpId content:message pushContent:nil pushData:nil success:^(long messageId) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            RCDContactSelectedTableViewController *vc = [[RCDContactSelectedTableViewController alloc]init];
                            vc.groupId = [NSString stringWithFormat:@"%@", success[@"info"][@"gid"]];
                            vc.groupName = nameStr;
                            [self.navigationController pushViewController:vc animated:YES];
                            
                        });
                    } error:^(RCErrorCode nErrorCode, long messageId) {

                    }];
                }else{
                    SHOW(success[@"info"]);
                }
                [TOOL hideLoading:self.view];
            } failure:^(NSError *failure) {
                [TOOL hideLoading:self.view];
            }];
        }
    }
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
  if (isiPhone5) {
    [self moveView:-40];
  }
  if (isiPhone4) {
    [self moveView:-80];
  }
  return YES;
}



- (void)Alert:(NSString *)alertContent {
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                  message:alertContent
                                                 delegate:self
                                        cancelButtonTitle:@"确定"
                                        otherButtonTitles:nil];
  [alert show];
}

- (void)chosePortrait {
  [self moveView:deafultY];
  [_GroupName resignFirstResponder];
  UIActionSheet *actionSheet =
      [[UIActionSheet alloc] initWithTitle:nil
                                  delegate:self
                         cancelButtonTitle:@"取消"
                    destructiveButtonTitle:@"拍照"
                         otherButtonTitles:@"我的相册", nil];
  [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet
    didDismissWithButtonIndex:(NSInteger)buttonIndex {
  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
  picker.allowsEditing = YES;
  picker.delegate = self;

  switch (buttonIndex) {
  case 0:
    if ([UIImagePickerController
            isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
      picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
      NSLog(@"模拟器无法连接相机");
    }
    [self presentViewController:picker animated:YES completion:nil];
    break;

  case 1:
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
    break;

  default:
    break;
  }
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
  [UIApplication sharedApplication].statusBarHidden = NO;

  NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];

  if ([mediaType isEqual:@"public.image"]) {
    UIImage *originImage =
    [info objectForKey:UIImagePickerControllerOriginalImage];
    CGRect captureRect = [[info objectForKey:UIImagePickerControllerCropRect] CGRectValue];
    UIImage *captureImage = [self getSubImage:originImage Rect:captureRect imageOrientation:originImage.imageOrientation];
    
    UIImage *scaleImage = [self scaleImage:captureImage toScale:0.8];
    data = UIImageJPEGRepresentation(scaleImage, 0.8);
  }

  image = [UIImage imageWithData:data];
  [self dismissViewControllerAnimated:YES completion:nil];
  dispatch_async(dispatch_get_main_queue(), ^{
    self.GroupPortrait.image = image;
  });
}

-(UIImage*)getSubImage:(UIImage *)originImage Rect:(CGRect)rect imageOrientation:(UIImageOrientation)imageOrientation
{
  CGImageRef subImageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
  CGRect smallBounds = CGRectMake(0, 0, CGImageGetWidth(subImageRef), CGImageGetHeight(subImageRef));
  
  UIGraphicsBeginImageContext(smallBounds.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextDrawImage(context, smallBounds, subImageRef);
  UIImage* smallImage = [UIImage imageWithCGImage:subImageRef scale:1.f orientation:imageOrientation];
  CGImageRelease(subImageRef);
  UIGraphicsEndImageContext();
  return smallImage;
}

- (UIImage *)scaleImage:(UIImage *)Image toScale:(float)scaleSize {
  UIGraphicsBeginImageContext(
      CGSizeMake(Image.size.width * scaleSize, Image.size.height * scaleSize));
  [Image drawInRect:CGRectMake(0, 0, Image.size.width * scaleSize,
                               Image.size.height * scaleSize)];
  UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return scaledImage;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [_GroupName resignFirstResponder];
  [self moveView:deafultY];
  return YES;
}

- (void)hideKeyboard:(id)sender {
  [self moveView:deafultY];
  [_GroupName resignFirstResponder];
}

//移动屏幕
- (void)moveView:(CGFloat)Y {

//  [UIView beginAnimations:nil context:nil];
//  self.view.frame =
//      CGRectMake(0, Y, self.view.frame.size.width, self.view.frame.size.height);
//  [UIView commitAnimations];
}



@end
