//
//  LiveApplyIDView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/11.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveApplyIDView : UIView
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *idField;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *id1Tap;
@property (weak, nonatomic) IBOutlet UIImageView *id2Tap;
@property (weak, nonatomic) IBOutlet UIImageView *id3Tap;
@property (weak, nonatomic) IBOutlet UIImageView *id1ImgView;
@property (weak, nonatomic) IBOutlet UIImageView *id2ImgView;
@property (weak, nonatomic) IBOutlet UIImageView *id3ImgView;
@property (weak, nonatomic) IBOutlet UITextView *conentTextView;
@property (weak, nonatomic) IBOutlet UILabel *contentPlaceHolder;
@property (weak, nonatomic) IBOutlet UILabel *contentTitleLbl;
@property (weak, nonatomic) IBOutlet UIView *contentContainerV;

@end

NS_ASSUME_NONNULL_END
