//
//  CCAddFriendView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/30.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCAddFriendView.h"

@implementation CCAddFriendView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}

@end
