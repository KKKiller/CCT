//
//  CCNearbyModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/30.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCNearbyModel.h"

@implementation CCNearbyModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"portrait" : @[@"avatar",@"portrait"]
             };
}
@end
