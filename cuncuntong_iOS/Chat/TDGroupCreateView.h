//
//  TDGroupCreateView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/9.
//  Copyright © 2019 zhushuai. All rights reserved. 
//

#import <UIKit/UIKit.h>
#import "PlaceholderTextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDGroupCreateView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UIButton *uploadImgBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet PlaceholderTextView *descTextView;
@property (weak, nonatomic) IBOutlet PlaceholderTextView *detailTextView;
@property (weak, nonatomic) IBOutlet UIButton *createBtn;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *statementTap;

+ (instancetype)instanceView;
@end

NS_ASSUME_NONNULL_END
