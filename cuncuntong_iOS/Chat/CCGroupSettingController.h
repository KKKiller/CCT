//
//  CCGroupSettingController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCDGroupInfo.h"
@interface CCGroupSettingController : UITableViewController
//@property(nonatomic, strong) NSString *userId;
@property(nonatomic, strong) RCGroup *groupInfo;

@end
