//
//  MultiSelectViewController.h
//  MultiSelectTableViewController
//
//  Created by molon on 6/7/14.
//  Copyright (c) 2014 molon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultiSelectItem.h"
typedef void(^ContactSelectedBlock)(NSMutableArray *selectedItems);
@interface MultiSelectViewController : UIViewController

@property (nonatomic, strong) NSArray<MultiSelectItem*> *items;
@property (nonatomic, strong) NSMutableArray *selectedItems;
@property (nonatomic, strong) NSMutableArray *selectedIndexes; //记录选择项对应的路径
@property (nonatomic, strong) ContactSelectedBlock selectedBlock;

@end
