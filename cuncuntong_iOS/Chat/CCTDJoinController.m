//
//  TSOrderCompleteController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCTDJoinController.h"
#import "TSOrderDetailController.h"
#import "TSOrderInfo.h"
#import "TDJoinCell.h"
#import "THChatController.h"
#import "JoinView.h"
#import "zhPopupController.h"
#import "UIImageView+WebCache.h"
//#import "CCLiveAuthorityApplyController.h"
//#import "CCLiveApplyController.h"
#import "CCLiveManagerController.h"
#import "CCTGroupModel.h"
#import "RCDUtilities.h"
static NSString *cellID = @"TDJoinCell";
@interface CCTDJoinController ()<UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, strong) JoinView *joinview;
@property (nonatomic, strong) UIView *tableHeadView;
@property (nonatomic, assign) BOOL isBeginSearch;
@property (nonatomic, strong) NSMutableArray *matchDataArr;

@end

@implementation CCTDJoinController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    [self initRefreshView];
    [self.view addSubview:self.searchFriendsBar];
    [self loadData];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = CGRectMake(0, CGRectGetMaxY(self.searchFriendsBar.frame), App_Width, CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.searchFriendsBar.frame));
    self.navigationController.navigationBar.barTintColor = MTRGB(0x387DFF);

}


- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    WEAKSELF
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance] GetUrlNew :BASELIVEURL_WITHOBJC(@"group/hotlist") params:@{@"userId":USERID} success:^(NSDictionary *success) {
        [weakSelf.dataArray removeAllObjects];
        NSNumber *code = success[@"code"];
        if ([code.stringValue isEqualToString:@"201"]){
            for (NSDictionary *dic in success[@"groups"]){
                CCTGroupModel *model = [CCTGroupModel modelWithDictionary:dic];
                [weakSelf.dataArray appendObject:model];
            }
            [weakSelf.tableView reloadData];
        }else {
            [MBProgressHUD showMessage:success[@"reason"]];
        }
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        NSLog(@"%@", success);
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
         NSLog(@"%@", failure.description);
    }];
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.dataArray.count;
    return _isBeginSearch ? self.matchDataArr.count : self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TDJoinCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    CCTGroupModel *model;
    if (_isBeginSearch){
       model = self.matchDataArr[indexPath.row];
    }else {
        model = self.dataArray[indexPath.row];
    }
    [cell.headImgView  sd_setImageWithURL:[NSURL URLWithString:model.groupAvatar]
                         placeholderImage:[UIImage imageNamed:@"icon_person"]];
    cell.titleLbl.text = model.groupName;
    cell.userCountLbl.text = [NSString stringWithFormat:@"%@   ", model.membersCount];
    cell.descLbl.text = model.groupIntroduce;
    [cell.joinBtn addTarget:self action:@selector(joinAct:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        THChatController *vc = [[THChatController alloc]init];
        CCTGroupModel *model = self.dataArray[indexPath.row];
        vc.groupId = model.groupId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - 点击事件
- (void)joinAct:(UIButton *)sender{
    self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
    self.zh_popupController.layoutType = zhPopupLayoutTypeCenter;
    self.zh_popupController.allowPan = YES;
    [self.zh_popupController presentContentView:self.joinview];
    
    CGRect rc = [sender.superview convertRect:sender.frame toView:self.tableView];
    NSIndexPath * indexPath = [self.tableView indexPathsForRowsInRect:rc].firstObject;
    CCTGroupModel *model = self.dataArray[indexPath.row];
    self.joinview.lab1.text = model.groupName;
    self.joinview.lab2.text = [NSString stringWithFormat:@"与全国%@位同行交流学习。", model.membersCount];
    self.joinview.priceLab.text = [NSString stringWithFormat:@"加入本群需支付年费%@元", model.entryPrice];
    [self.joinview.imgV  sd_setImageWithURL:[NSURL URLWithString:model.groupAvatar]
                         placeholderImage:[UIImage imageNamed:@"icon_person"]];
    self.joinview.btn.tag = 10000 + indexPath.row;
    
}
- (void)confirmJoinAct:(UIButton *)sender{
    [self.zh_popupController dismiss];
    CCTGroupModel *model = self.dataArray[sender.tag - 10000];
    [[MyNetWorking sharedInstance] PostUrlNew:BASELIVEURL_WITHOBJC(@"group/join") params:@{@"groupId":model.groupLiveId, @"members":USERID} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {
            SHOW(@"加入成功");
            THChatController *vc = [[THChatController alloc]init];
            vc.groupId = model.groupId;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            SHOW(@"加入失败");
        }
    } failure:^(NSError *failure) {
        
    }];
    //    return;
    //    CCLiveManagerController *Vc = [[UIStoryboard storyboardWithName:@"Live" bundle:NULL] instantiateViewControllerWithIdentifier:@"CCLiveManagerController"];
    ////    CCLiveApplyController *Vc = [[CCLiveApplyController alloc] initWithNibName:NSStringFromClass([CCLiveApplyController class]) bundle:NULL];
    //    [self.navigationController pushViewController:Vc animated:YES];
    
}
- (UISearchBar *)searchFriendsBar {
    if (!_searchFriendsBar) {
        _searchFriendsBar=[[UISearchBar alloc]initWithFrame:CGRectMake(2, 0, App_Width-4, 28)];
        [_searchFriendsBar sizeToFit];
        [_searchFriendsBar setPlaceholder:NSLocalizedStringFromTable(@"ToSearch", @"RongCloudKit", nil)];
        [_searchFriendsBar.layer setBorderWidth:0.5];
        _searchFriendsBar.placeholder = @"请输入社群名称或社群号";
        [_searchFriendsBar.layer setBorderColor:[UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1].CGColor];
        [_searchFriendsBar setDelegate:self];
        [_searchFriendsBar setKeyboardType:UIKeyboardTypeDefault];
    }
    return _searchFriendsBar;
}
- (JoinView *)joinview {
    if (!_joinview) {
        _joinview = [JoinView instanceView];
        _joinview.layer.cornerRadius = 10;
        _joinview.frame = CGRectMake(14, 0, App_Width - 28, 260);
        [_joinview.btn addTarget:self action:@selector(confirmJoinAct:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _joinview;
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshData];
    }];
    
//    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf loadData];
//    }];
}
#pragma mark - 懒加载
- (UIView *)tableHeadView{
    if (_tableHeadView == nil){
        _tableHeadView = [UIView new];
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableHeadView.frame = CGRectMake(0, 0, App_Width, 40);
        UIView *bgV = [[UIView alloc] initWithFrame:CGRectMake(15, 0, App_Width - 30, 40)];
        bgV.backgroundColor = WHITECOLOR;
        bgV.clipsToBounds = YES;
        [_tableHeadView addSubview:bgV];
        
        UIView *a = [[UIView alloc] initWithFrame:CGRectMake(-15, 10, 120, 30)];
        a.backgroundColor = RGB(255, 212, 83);
        a.layer.cornerRadius = 15;
        a.layer.masksToBounds = YES;
        [bgV addSubview:a];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(25, 7, 16, 16)];
        imgV.image = IMAGENAMED(@"popular");
        [a addSubview:imgV];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, 120 - 45, 30)];
        lab.text = @"热门社群";
        lab.textColor = RGB(247, 60, 0);
        lab.textAlignment = NSTextAlignmentLeft;
        lab.font = FontSize(13);
        [a addSubview:lab];
    }
    return _tableHeadView;
}
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 80;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"TDJoinCell" bundle:nil] forCellReuseIdentifier:cellID];
        _tableView.tableHeaderView = self.tableHeadView;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

-(NSMutableArray *)matchDataArr{
    if (_matchDataArr == nil) {
        _matchDataArr = [[NSMutableArray alloc]init];
    }
    return _matchDataArr;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - WCFNavigationHeight - WCFTabBarHeight - 45)];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

#pragma mark - UISearchBarDelegate
/**
 *  执行delegate搜索好友
 */
- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    [self.matchDataArr removeAllObjects];
    if (searchText.length <= 0) {
//        [self sortAndRefreshWithList:self.friendList];
    } else {
        for (CCTGroupModel *model in self.dataArray) {
            //忽略大小写去判断是否包含
            if ([model.groupName rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [self.matchDataArr addObject:model];
            }
        }
    }
    [self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchFriendsBar.showsCancelButton = NO;
    [self.searchFriendsBar resignFirstResponder];
    self.searchFriendsBar.text = @"";
    [self.matchDataArr removeAllObjects];
    _isBeginSearch = NO;
    [self.tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (_isBeginSearch == NO) {
        _isBeginSearch = YES;
        [self.tableView reloadData];
    }
    self.searchFriendsBar.showsCancelButton = YES;
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
@end
