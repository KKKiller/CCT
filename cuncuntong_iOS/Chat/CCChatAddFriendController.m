//
//  CCChatAddFriendController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCChatAddFriendController.h"
#import "IQKeyboardManager.h"
#import "CCUserInfoModel.h"
@interface CCChatAddFriendController ()<UITextViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *villageLbl;
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIView *beizhuView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLbl;
@property (weak, nonatomic) IBOutlet UITextField *beizhuTextField;
@property (weak, nonatomic) IBOutlet UIButton *addFriendBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backTopCon;

@property (nonatomic, strong) CCUserInfoModel *infoModel;
@end

@implementation CCChatAddFriendController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self loadData];
}
- (void)setUI {
    self.backTopCon.constant = StatusBarHeight > 20 ? 40 : 20;
    [IQKeyboardManager sharedManager].enable = YES;
    self.navigationController.navigationBarHidden = YES;
    self.villageLbl.layer.borderColor = TEXTBLACK6.CGColor;
    self.villageLbl.layer.borderWidth = 0.5;
//    self.textView.delegate = self;
//    self.beizhuTextField.delegate = self;
//    self.textView.layer.borderColor = MTRGB(0x21c2d5).CGColor;
//    self.textView.layer.borderWidth = 0.5;
//    self.beizhuView.layer.borderWidth = 0.5;
//    self.beizhuView.layer.borderColor = MTRGB(0x21c2d5).CGColor;
    self.textView.hidden = YES;
    self.beizhuView.hidden = YES;
    self.placeHolderLbl.hidden = YES;
    
    [self.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.addFriendBtn addTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside];
    //同村
    self.villageLbl.hidden = YES;
    self.scrollView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self.view endEditing:YES];
    }];
    [self.scrollView addGestureRecognizer:tap];

    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
    self.navigationController.navigationBarHidden = NO;
}

/**
 返回

 @param sender Btn
 */
-(void)backMove:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_info") params:@{@"f_uid":STR(self.uid),@"uid":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
            self.infoModel = [CCUserInfoModel modelWithJSON:success[@"info"]];
            self.nameLbl.text = self.infoModel.realname;
            [self.headImgView setImageWithURL:URL(self.infoModel.portrait) placeholder:IMAGENAMED(@"head")];
            self.addressLbl.text = [NSString stringWithFormat:@"地址: %@", self.infoModel.addr];
            self.distanceLbl.text = [NSString stringWithFormat:@"距离: %d千米",self.infoModel.distance/1000];
        }else{
            SHOW(success[@"info"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)textViewDidChange:(UITextView *)textView {
//    self.placeHolderLbl.hidden = textView.text.length > 0;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)addFriend {
    NSDictionary *param = @{@"f_name":@"",@"f_uid":STR(self.uid),@"uid":USERID};
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_add") params:param target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
            SHOW(@"添加好友提交成功");
            [XMPPTOOL.xmppRoster subscribePresenceToUser:[XMPPTOOL getJIDWithUserId:self.uid]];
        }else{
            SHOW(success[@"info"]);
//            [XMPPTOOL.xmppRoster  unsubscribePresenceFromUser:[XMPPTOOL getJIDWithUserId:self.uid]];
//            [XMPPTOOL.xmppRoster subscribePresenceToUser:[XMPPTOOL getJIDWithUserId:self.uid]];
        }
    } failure:^(NSError *failure) {
    }];
}

@end


//            RCContactNotificationMessage *message = [[RCContactNotificationMessage alloc]init];
//            message.operation =  ContactNotificationMessage_ContactOperationRequest;
//            message.sourceUserId = USERID;
//            message.targetUserId = self.uid;
//            message.message = self.textView.text;
//            message.extra = self.textView.text;
//            message.mentionedInfo.mentionedContent = self.textView.text;
//            [[RCIM sharedRCIM] sendMessage:ConversationType_SYSTEM targetId:self.uid content:message pushContent:@"好友添加申请" pushData:nil success:^(long messageId) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self.navigationController popViewControllerAnimated:YES];
//
//                });
//            } error:^(RCErrorCode nErrorCode, long messageId) {
//                NSLog(@"%ld",nErrorCode);
//            }];
