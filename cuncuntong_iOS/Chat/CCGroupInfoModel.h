//
//  CCGroupInfoModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/20.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCGroupInfoModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *group_name;
@property (nonatomic, strong) NSString *avatar;
@end
