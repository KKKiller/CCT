//
//  CCLiveApplyChildController.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/15.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import "CCLiveApplyChildController.h"
#import "zhPopupController.h"
#import "CCLiveTimeModifyView.h"
#import "BRPickerView.h"

@interface CCLiveApplyChildController ()

@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;
@property (weak, nonatomic) IBOutlet UIButton *passBtn;
@property (strong, nonatomic) IBOutlet UIView *noteView;
@property (weak, nonatomic) IBOutlet UIButton *noteCancelBtn;
@property (nonatomic, strong) CCLiveTimeModifyView *modifyTimeView;

@end

@implementation CCLiveApplyChildController




- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
    
    self.tableView.tableFooterView = [UIView new];
    _rejectBtn.layer.borderColor = MTRGB(0xC8C8C8).CGColor;
    _rejectBtn.layer.borderWidth = 1;
    _noteCancelBtn.layer.borderColor = MTRGB(0xC8C8C8).CGColor;
    _noteCancelBtn.layer.borderWidth = 1;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self setTitle:@"直播申请"];
    

}




- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

/**
 拒绝

 @param sender Btn
 */
- (IBAction)rejectAct:(UIButton *)sender {
    self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
    self.zh_popupController.layoutType = zhPopupLayoutTypeCenter;
    self.zh_popupController.allowPan = YES;
    [self.zh_popupController presentContentView:self.noteView];
}

/**
 取消

 @param sender Btn
 */
- (IBAction)cancelAct:(UIButton *)sender {
    [self.zh_popupController dismiss];
}

/**
 确定

 @param sender Btn
 */
- (IBAction)confirmAct:(UIButton *)sender {
    [self.zh_popupController dismiss];
}

/**
 审核通过

 @param sender Btn
 */
- (IBAction)passAct:(UIButton *)sender {
    self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
    self.zh_popupController.layoutType = zhPopupLayoutTypeCenter;
    self.zh_popupController.allowPan = YES;
    [self.zh_popupController presentContentView:self.modifyTimeView];
}


- (CCLiveTimeModifyView *)modifyTimeView {
    if (!_modifyTimeView) {
        _modifyTimeView = [CCLiveTimeModifyView instanceView];
        _modifyTimeView.frame = CGRectMake(12, 0, App_Width - 24, 300);
        _modifyTimeView.layer.cornerRadius = 8;
        _modifyTimeView.layer.masksToBounds = YES;
        
        [_modifyTimeView.btn addTarget:self action:@selector(confirmTimeModifyClick:) forControlEvents:UIControlEventTouchUpInside];
        for (UITapGestureRecognizer *tap in _modifyTimeView.ges0Arr) {
            [tap addTarget:self action:@selector(riQiGesAct:)];
        }
        for (UITapGestureRecognizer *tap in _modifyTimeView.ges1Arr) {
            [tap addTarget:self action:@selector(time0GesAct:)];
        }
        for (UITapGestureRecognizer *tap in _modifyTimeView.ges2Arr) {
            [tap addTarget:self action:@selector(time1GesAct:)];
        }
        
    }
    return _modifyTimeView;
}



/**
 确定

 @param sender Btn
 */
- (void)confirmTimeModifyClick:(UIButton *)sender{
    [self.zh_popupController dismiss];
}

/**
 日期时间修改

 @param ges UITapGes
 */
- (void)riQiGesAct:(UITapGestureRecognizer *)ges{
    WEAKSELF
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *minDateStr = [dateFormatter stringFromDate:[NSDate date]];
    [BRDatePickerView showDatePickerWithTitle:@"修改日期" dateType:UIDatePickerModeDate defaultSelValue:nil minDateStr:minDateStr maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
        NSArray * arr = [selectValue componentsSeparatedByString:@"-"];
        [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UITapGestureRecognizer *ges = weakSelf.modifyTimeView.ges0Arr[idx];
            ((UILabel *)(ges.view)).text = obj;
        }];
    }];
}
- (void)time0GesAct:(UITapGestureRecognizer *)ges{
    WEAKSELF
    [BRDatePickerView showDatePickerWithTitle:@"修改时间" dateType:UIDatePickerModeTime defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
        NSArray * arr = [selectValue componentsSeparatedByString:@":"];
        [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UITapGestureRecognizer *ges = weakSelf.modifyTimeView.ges1Arr[idx];
            ((UILabel *)(ges.view)).text = obj;
        }];
    }];
}
- (void)time1GesAct:(UITapGestureRecognizer *)ges{
    WEAKSELF
    [BRDatePickerView showDatePickerWithTitle:@"修改时间" dateType:UIDatePickerModeTime defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
        NSArray * arr = [selectValue componentsSeparatedByString:@":"];
        [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UITapGestureRecognizer *ges = weakSelf.modifyTimeView.ges2Arr[idx];
            ((UILabel *)(ges.view)).text = obj;
        }];
    }];
    
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
