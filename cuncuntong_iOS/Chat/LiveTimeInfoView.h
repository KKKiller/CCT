//
//  LiveTimeInfoView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveTimeInfoView : UIView
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLbl;
@property (weak, nonatomic) IBOutlet UIButton *ABtn;
@property (weak, nonatomic) IBOutlet UIButton *BBtn;
@property (weak, nonatomic) IBOutlet UIButton *CBtn;

@end

NS_ASSUME_NONNULL_END
