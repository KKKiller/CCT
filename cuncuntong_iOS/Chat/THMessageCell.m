//
//  SDHomeTableViewCell.m
//  GSD_WeiXin(wechat)
//
//  Created by aier on 16/2/10.
//  Copyright © 2016年 GSD. All rights reserved.
//


#import "THMessageCell.h"
#import "ICFaceManager.h"
#define kDeleteButtonWidth      60.0f
#define kTagButtonWidth         0.0f
#define kCriticalTranslationX   30
#define kShouldSlideX           -2

@interface THMessageCell ()

@property (nonatomic, assign) BOOL isSlided;

@end

@implementation THMessageCell
{
    UIButton *_deleteButton;
    UIButton *_tagButton;
    UIView *_lineView;
    UIPanGestureRecognizer *_pan;
    UITapGestureRecognizer *_tap;
    
    BOOL _shouldSlide;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupView];
        [self setupGestureRecognizer];
    }
    return self;
}

//聊天消息
- (void)setData:(XMPPMessageArchiving_Contact_CoreDataObject *)data {
    _data = data;
    
    self.avatar.image = IMAGENAMED(@"head");
    NSString *uid = [XMPPTOOL getUidWithJid:data.bareJid];
    self.nameLabel.text = uid;
    [[CCUserInfoManager shareInstance] loadUserInfoWithUid:uid loadFinishBlk:^(CCUserInfo * _Nonnull info) {
        dispatch_async_on_main_queue(^{
            [self.avatar setImageURL:URL(info.portrait)];
            self.nameLabel.text = info.realname;
        });
    }];
    
    if ([data.mostRecentMessageBody containsString:@"picture_YY"]) {
        self.messageLabel.text = @"【图片】";
    }else if ([data.mostRecentMessageBody containsString:@"voice_YY"]){
        self.messageLabel.text = @"【语音】";
    }else if ([data.mostRecentMessageBody containsString:@"SelfLocation_YY"]){
        self.messageLabel.text = @"【定位】";
    }else if ([data.mostRecentMessageBody containsString:@"RedPacket_YY"]){
        self.messageLabel.text = @"【红包】";
    }else{
        [self.messageLabel  setAttributedText:[ICFaceManager transferMessageString:data.mostRecentMessageBody font:FFont(14) lineHeight:18]];
    }

    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *strDate = [dateFormatter stringFromDate:data.mostRecentMessageTimestamp];
    self.timeLabel.text = strDate;
    
    NSInteger unreadCount = [XMPPTOOL getUnreadCountWithUid:uid];
    self.unreadLbl.hidden = unreadCount == 0;
    NSString *numText = [NSString stringWithFormat:@"%@",@(unreadCount)];
    CGFloat width = 15;
    if (unreadCount > 9) {
        numText = [NSString stringWithFormat:@"%@",@(unreadCount)];
        if (unreadCount > 99) {
            numText = @"99+";
            width = 25;
        }
    }
    self.unreadLbl.text = numText;
    [self.unreadLbl mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
    }];
}
//好友添加请求
- (void)setFriendRequestModel:(LBFriendRequestModel *)friendRequestModel {
    _friendRequestModel = friendRequestModel;
    self.nameLabel.text = friendRequestModel.nickname;
    [self.avatar setImageWithURL:URL(friendRequestModel.avatar) placeholder:IMAGENAMED(@"head")];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *strDate = [dateFormatter stringFromDate:friendRequestModel.create_date];
    self.timeLabel.text = strDate;
    self.messageLabel.text = @"【请求添加您为好友】";
    self.unreadLbl.hidden = YES;
}

#pragma mark - private actions

- (void)setupView
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _avatar = [UIImageView new];
    _avatar.layer.cornerRadius = 5;
    _avatar.clipsToBounds = YES;
    
    _nameLabel = [UILabel new];
    _nameLabel.font = [UIFont systemFontOfSize:16];
    _nameLabel.textColor = [UIColor blackColor];
    
    _timeLabel = [UILabel new];
    _timeLabel.font = [UIFont systemFontOfSize:12];
    _timeLabel.textColor = [UIColor lightGrayColor];
    
    _messageLabel = [KILabel new];
    _messageLabel.font = [UIFont systemFontOfSize:14];
    _messageLabel.textColor = [UIColor lightGrayColor];
    
    _deleteButton = [UIButton new];
    _deleteButton.backgroundColor = [UIColor redColor];
    [_deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    
    _tagButton = [UIButton new];
    _tagButton.backgroundColor =[UIColor lightGrayColor];
    [_tagButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_tagButton setTitle:@"标记未读" forState:UIControlStateNormal];
    
    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = MTRGB(0xeeeeee);
    
    self.unreadLbl = [[UILabel alloc]initWithText:@"3" font:10 textColor:WHITECOLOR];
    self.unreadLbl.backgroundColor = [UIColor redColor];
    self.unreadLbl.layer.cornerRadius = 7.5;
    self.unreadLbl.layer.masksToBounds = YES;
    self.unreadLbl.textAlignment = NSTextAlignmentCenter;
    
    [self.contentView addSubview:_lineView];
    [self.contentView addSubview:_avatar];
    [self.contentView addSubview:_nameLabel];
    [self.contentView addSubview:_timeLabel];
    [self.contentView addSubview:_messageLabel];
    [self insertSubview:_deleteButton belowSubview:self.contentView];
    [self insertSubview:_tagButton belowSubview:self.contentView];
    [self.contentView addSubview:self.unreadLbl];
    
    [_deleteButton addTarget:self action:@selector(deleteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_tagButton addTarget:self action:@selector(tagBtnClick::) forControlEvents:UIControlEventTouchUpInside];

}
- (void)deleteBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(deleteBtnClick:)]) {
        [self.delegate deleteBtnClick:self.data];
    }
}
- (void)tagBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(tagBtnClick:)]) {
        [self.delegate tagBtnClick:self.data];
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [_deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(self);
        make.width.mas_equalTo(kDeleteButtonWidth);
    }];
    [_tagButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(_deleteButton);
        make.right.equalTo(_deleteButton.mas_left);
        make.width.mas_equalTo(kTagButtonWidth);
    }];
    UIView *superView = self.contentView;
    CGFloat margin = 10;
    [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(50);
        make.left.equalTo(superView).offset(margin);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_avatar);
        make.left.equalTo(_avatar.mas_right).offset(margin);
        make.right.equalTo(superView).offset(-80);
        make.height.mas_equalTo(26);
    }];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView).offset(-margin);
        make.centerY.equalTo(_nameLabel);
        make.height.mas_equalTo(16);
    }];
    [_messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_nameLabel);
        make.top.equalTo(_nameLabel.mas_bottom).offset(4);
        make.height.mas_equalTo(18);
        make.right.equalTo(superView.mas_right).offset(-margin);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(superView.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    [self.unreadLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-margin);
        make.width.height.mas_equalTo(15);
        make.bottom.equalTo(self.messageLabel.mas_bottom);
    }];
}
- (void)setupGestureRecognizer
{
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    _pan = pan;
    pan.delegate = self;
    pan.delaysTouchesBegan = YES;
    [self.contentView addGestureRecognizer:pan];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
    tap.delegate = self;
    tap.enabled = NO;
    [self.contentView addGestureRecognizer:tap];
    _tap = tap;
}

- (void)tapView:(UITapGestureRecognizer *)tap
{
    if (self.isSlided) {
        [self cellSlideAnimationWithX:0];
    }
}

- (void)panView:(UIPanGestureRecognizer *)pan
{
    CGPoint point = [pan translationInView:pan.view];
    
    if (self.contentView.left <= kShouldSlideX) {
        _shouldSlide = YES;
    }

    if (fabs(point.y) < 1.0) {
        if (_shouldSlide) {
            [self slideWithTranslation:point.x];
        } else if (fabs(point.x) >= 1.0) {
            [self slideWithTranslation:point.x];
        }
    }
    
    if (pan.state == UIGestureRecognizerStateEnded) {
        CGFloat x = 0;
        if (self.contentView.left < -kCriticalTranslationX && !self.isSlided) {
            x = -(kDeleteButtonWidth + kTagButtonWidth);
        }
        [self cellSlideAnimationWithX:x];
        _shouldSlide = NO;
    }
    
    [pan setTranslation:CGPointZero inView:pan.view];
}


- (void)cellSlideAnimationWithX:(CGFloat)x
{
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:2 options:UIViewAnimationOptionLayoutSubviews animations:^{
        self.contentView.left = x;
    } completion:^(BOOL finished) {
        self.isSlided = (x != 0);
    }];
}

- (void)slideWithTranslation:(CGFloat)value
{
    if (self.contentView.left < -(kDeleteButtonWidth + kTagButtonWidth) * 1.1 || self.contentView.left > 30) {
        value = 0;
    }
    self.contentView.left += value;
}

#pragma mark - properties

//- (void)setModel:(SDHomeTableViewCellModel *)model
//{
//    _model = model;
//    
//    self.avatar.image = [UIImage imageNamed:model.imageName];
//    self.nameLabel.text = model.name;
//    self.timeLabel.text = model.time;
//    self.messageLabel.text = model.message;
//}

- (void)setIsSlided:(BOOL)isSlided
{
    _isSlided = isSlided;
    
    _tap.enabled = isSlided;
}

#pragma mark - public actions

+ (CGFloat)fixedHeight
{
    return 70;
}

#pragma mark - gestureRecognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (self.contentView.left <= kShouldSlideX && otherGestureRecognizer != _pan && otherGestureRecognizer != _tap) {
        return NO;
    }
    return YES;
}


@end

