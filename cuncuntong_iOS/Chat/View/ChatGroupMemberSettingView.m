//
//  ChatGroupMemberSettingView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/11.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "ChatGroupMemberSettingView.h"

@implementation ChatGroupMemberSettingView
+ (instancetype)instanceView {
    ChatGroupMemberSettingView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}
@end
