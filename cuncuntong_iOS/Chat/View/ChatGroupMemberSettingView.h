//
//  ChatGroupMemberSettingView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/11.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatGroupMemberSettingView : UIView
+ (instancetype)instanceView;

@property (weak, nonatomic) IBOutlet UIImageView *masterImgView;
@property (weak, nonatomic) IBOutlet UILabel *masterNameLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *firstCollectionView;

@end

NS_ASSUME_NONNULL_END
