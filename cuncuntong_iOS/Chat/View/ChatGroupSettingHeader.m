//
//  ChatGroupSettingHeader.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "ChatGroupSettingHeader.h"
#import "ChatSettingHeadCollectionViewCell.h"
static NSString *cellID = @"ChatSettingHeadCell";

@implementation ChatGroupSettingHeader
+ (instancetype)instanceView {
    ChatGroupSettingHeader *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    [view.moreBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -view.moreBtn.imageView.size.width, 0, view.moreBtn.imageView.size.width)];
    [view.moreBtn setImageEdgeInsets:UIEdgeInsetsMake(0, view.moreBtn.titleLabel.bounds.size.width, 0, -view.moreBtn.titleLabel.bounds.size.width)];
    [view.moreBtn addTarget:view action:@selector(moreBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [view.getMasterBtn addTarget:view action:@selector(getMasterBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [view.sailSwitch addTarget:view action:@selector(sailSwitchClick:) forControlEvents:UIControlEventValueChanged];
    return view;
}
- (void)moreBtnClick {
    if ([self.delegate respondsToSelector:@selector(clickAtMoreBtn)]) {
        [self.delegate clickAtMoreBtn];
    }
}
- (void)getMasterBtnClick {
    if ([self.delegate respondsToSelector:@selector(clickGetManagerBtn)]) {
        [self.delegate clickGetManagerBtn];
    }
}
- (void)sailSwitchClick:(UISwitch *)sender {
    if ([self.delegate respondsToSelector:@selector(clickSailSwitchBtn:)]) {
        [self.delegate clickSailSwitchBtn:YES];
    }
}
- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    [self setupCollectionView];
    
}
- (void)setupCollectionView {
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatSettingHeadCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellID];
    self.collectionViewHeight.constant = 300;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.height = 480;
    self.moreBtn.hidden = NO;
    self.infoViewTop.constant = 60;
    [self.collectionView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return self.dataArray.count;
    return 12;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatSettingHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(70, 90);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(clickAtUser:)]) {
        [self.delegate clickAtUser:@""];
    }
}

@end
