//
//  ChatGroupSettingHeader.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatUserInfoModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol ChatGroupSettingHeaderDelegate <NSObject>

- (void)clickAtUser:(NSString *)uid;
- (void)clickAtMoreBtn;
- (void)clickGetManagerBtn;
- (void)clickSailSwitchBtn:(BOOL)enable;


@end
@interface ChatGroupSettingHeader : UIView<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *moreBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *tagLbl;
@property (weak, nonatomic) IBOutlet UIButton *getMasterBtn;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UISwitch *sailSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoViewTop;
+ (instancetype)instanceView;
@property (nonatomic, strong) NSArray<ChatUserInfoModel*> *dataArray;
@property (nonatomic, weak) id<ChatGroupSettingHeaderDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
