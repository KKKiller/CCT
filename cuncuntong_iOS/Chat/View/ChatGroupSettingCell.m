//
//  ChatGroupSettingCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "ChatGroupSettingCell.h"

@implementation ChatGroupSettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setType:(ChatGroupSettingCellType)type {
    _type = type;
    if (type == ChatGroupSettingCellTypeTitle) {
        self.arrowImgView.hidden = NO;
        self.descLbl.hidden = YES;
        self.switchBtn.hidden = YES;
    }else if (type == ChatGroupSettingCellTypeDesc){
        self.arrowImgView.hidden = NO;
        self.descLbl.hidden = NO;
        self.switchBtn.hidden = YES;
    }else{
        self.arrowImgView.hidden = YES;
        self.descLbl.hidden = YES;
        self.switchBtn.hidden = NO;
    }
}
@end
