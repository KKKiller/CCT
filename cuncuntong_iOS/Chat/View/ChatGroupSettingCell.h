//
//  ChatGroupSettingCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    ChatGroupSettingCellTypeTitle,
    ChatGroupSettingCellTypeDesc,
    ChatGroupSettingCellTypeSwitch,
} ChatGroupSettingCellType;
@interface ChatGroupSettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImgView;
@property (weak, nonatomic) IBOutlet UISwitch *switchBtn;

@property (nonatomic, assign) ChatGroupSettingCellType type;
@end

NS_ASSUME_NONNULL_END
