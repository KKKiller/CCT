//
//  ChatGroupMemberSettingCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/11.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatGroupMemberSettingCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIButton *removeBtn;

@end

NS_ASSUME_NONNULL_END
