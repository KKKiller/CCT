//
//  ChatGroupSettingController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "ChatGroupSettingController.h"
#import "ChatGroupSettingHeader.h"
#import "ChatGroupSettingCell.h"
#import "CCLiveManagerController.h"
#import "CCLiveApplyController.h"
#import "CCLiveManagerController.h"
#import "ChatGroupMemberSettingController.h"
#import "CCTGroupModel.h"
#import "ChatSubGroupController.h"
static NSString *cellID = @"ChatSettingCell";

@interface ChatGroupSettingController ()<UITableViewDataSource,UITableViewDelegate,ChatGroupSettingHeaderDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ChatGroupSettingHeader *header;
@property (nonatomic, strong) UIButton *footer;
@property (nonatomic, strong) CCTGroupModel *groupModel;


@end

@implementation ChatGroupSettingController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    self.title = self.navTitle;
    self.isManager = YES;
    [self loadData];
    [self.tableView reloadData];
    self.header.dataArray = @[];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MAINCOLOR;
    
}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrlNew:BASELIVEURL_WITHOBJC(@"group/user/ban/add") params:@{@"groupId":self.groupId,@"members":@"111,222,333"} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {

        }
    } failure:^(NSError *failure) {

    }];
    
    [[MyNetWorking sharedInstance]GetUrlNew:BASELIVEURL_WITHOBJC(@"group/members") params:@{@"groupId":self.groupId} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {
         
            
        }
    } failure:^(NSError *failure) {
        
    }];
    [[MyNetWorking sharedInstance]GetUrlNew:BASELIVEURL_WITHOBJC(@"group/id/find") params:@{@"groupId":self.groupId} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {
            self.groupModel = [CCTGroupModel modelWithJSON:success];
            [self.tableView reloadData];
        }
    } failure:^(NSError *failure) {
        
    }];
}
    
    //停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataArray[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatGroupSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    NSString *title = self.dataArray[indexPath.section][indexPath.row];
    cell.titleLbl.text = title;
    if ([title isEqualToString:@"群聊名称"]) {
        cell.type = ChatGroupSettingCellTypeDesc;
        cell.descLbl.text = @"群名字";
    }else if ([title isEqualToString:@"我在本群的昵称"]){
        cell.type = ChatGroupSettingCellTypeDesc;
        cell.descLbl.text = @"群昵称";
    }else if ([title isEqualToString:@"全群禁言"]){
        cell.type = ChatGroupSettingCellTypeSwitch;
    }else if([title isEqualToString:@"置顶聊天"]){
        cell.type = ChatGroupSettingCellTypeSwitch;
    }else{
        cell.type = ChatGroupSettingCellTypeTitle;
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 10)];
    v.backgroundColor = MTRGB(0xf2f2f2);
    return v;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *title = self.dataArray[indexPath.section][indexPath.row];
    ChatGroupSettingCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if ([title isEqualToString:@"群聊名称"]) {
        [self modifyGroupName];
    }else if ([title isEqualToString:@"我在本群的昵称"]){
        
    }else if ([title isEqualToString:@"直播申请"]){
        CCLiveApplyController *Vc = [[UIStoryboard storyboardWithName:@"Live" bundle:nil] instantiateViewControllerWithIdentifier:@"CCLiveApplyController"];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if ([title isEqualToString:@"直播管理"]){
        CCLiveManagerController *Vc = [[UIStoryboard storyboardWithName:@"Live" bundle:NULL] instantiateViewControllerWithIdentifier:@"CCLiveManagerController"];
        [self.navigationController pushViewController:Vc animated:YES];
        
    }else if ([title isEqualToString:@"群管理"]){
        ChatGroupMemberSettingController *Vc = [[UIStoryboard storyboardWithName:@"Live" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatGroupMemberSettingController"];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if ([title isEqualToString:@"子群"]){
        ChatSubGroupController *vc = [[ChatSubGroupController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"收费设置"]){
        
    }else if ([title isEqualToString:@"群分享"]){
        
    }else if ([title isEqualToString:@"群二维码"]){
        
    }else if ([title isEqualToString:@"发布群公告"]){
        
    }else if ([title isEqualToString:@"全群禁言"]){
        cell.switchBtn.on = !cell.switchBtn.isOn;
    }else if ([title isEqualToString:@"置顶聊天"]){
        cell.switchBtn.on = !cell.switchBtn.isOn;
    }else if ([title isEqualToString:@"清空聊天记录"]){
        SHOW(@"清空聊天记录");
    }
}
- (void)modifyGroupName{
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:@"修改群名称" message:nil preferredStyle:
                                  UIAlertControllerStyleAlert];
    [alertVc addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"请输入群名称";
    }];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        // 通过数组拿到textTF的值
        NSString *text = [[alertVc textFields] objectAtIndex:0].text;
        [[MyNetWorking sharedInstance]PostUrlNew:BASELIVEURL_WITHOBJC(@"group/update") params:@{@"groupId":self.groupId,@"groupName":text} success:^(NSDictionary *success) {
            if ([success[@"status"] integerValue] == 201) {
                SHOW(@"修改成功");
            }
        } failure:^(NSError *failure) {
            
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    // 添加行为
    [alertVc addAction:action2];
    [alertVc addAction:action1];
    [self presentViewController:alertVc animated:YES completion:nil];
}
- (void)clickAtUser:(NSString *)uid{
    
}
- (void)clickAtMoreBtn{
    
}
- (void)clickGetManagerBtn{
    
}
- (void)clickSailSwitchBtn:(BOOL)enable{
    
}
- (void)quitBtnClick {
    [[MyNetWorking sharedInstance]PostUrlNew:BASELIVEURL_WITHOBJC(@"group/quit") params:@{@"groupId":self.groupId,@"members":USERID} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {
            SHOW(@"退群成功");
        }
    } failure:^(NSError *failure) {
        
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 50;
        _tableView.tableFooterView = self.footer;
        _tableView.tableHeaderView = self.header;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"ChatGroupSettingCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        NSArray *array;
        if (self.isManager) {
            //,@"我在本群的昵称"
            array = @[@[@"群聊名称"],@[@"直播申请",@"直播管理",@"群管理"],@[@"子群"],@[@"收费设置",@"群分享",@"群二维码"],@[@"发布群公告",@"全群禁言",@"置顶聊天",@"清空聊天记录"]];
        }else{
            //,@"我在本群的昵称"
            array = @[@[@"群聊名称"],@[@"群分享",@"群二维码"],@[@"聊天置顶",@"清空聊天记录"]];
        }
        _dataArray = [NSMutableArray arrayWithArray:array];
    }
    return _dataArray;
}
- (ChatGroupSettingHeader *)header {
    if (!_header) {
        _header = [ChatGroupSettingHeader instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 200);
    }
    return _header;
}
- (UIButton *)footer {
    if (!_footer) {
        _footer = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, App_Width, 60)];
        [_footer setTitle:@"退群" forState:UIControlStateNormal];
        [_footer setTitleColor:MTRGB(0xFD7C7C ) forState:UIControlStateNormal];
        [_footer addTarget:self action:@selector(quitBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _footer.titleLabel.font = FFont(15);
    }
    return _footer;
}
@end
