//
//  SDHomeTableViewCell.h
//  GSD_WeiXin(wechat)
//
//  Created by aier on 16/2/10.
//  Copyright © 2016年 GSD. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "LBFriendRequestModel.h"
@protocol THMEssageCellDelegate<NSObject>
- (void)deleteBtnClick:(XMPPMessageArchiving_Contact_CoreDataObject *)data;
- (void)tagBtnClick:(XMPPMessageArchiving_Contact_CoreDataObject *)data;
@end
@interface THMessageCell : UITableViewCell

@property (nonatomic, strong) UIImageView *avatar;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) KILabel *messageLabel;
@property (nonatomic, strong) UILabel *unreadLbl;
@property (nonatomic, strong) XMPPMessageArchiving_Contact_CoreDataObject *data;
@property (nonatomic, strong) LBFriendRequestModel *friendRequestModel;

@property (nonatomic, weak) id<THMEssageCellDelegate> delegate;


@end
