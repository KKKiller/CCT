//
//  CCRedCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCRedCell.h"

@implementation CCRedCell

//+ (CGSize)sizeForMessageModel:(RCMessageModel *)model
//      withCollectionViewWidth:(CGFloat)collectionViewWidth
//         referenceExtraHeight:(CGFloat)extraHeight {
//    return CGSizeMake(collectionViewWidth, 100 + extraHeight);
//}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.redImgView];
        [self.redImgView addSubview:self.smallRed];
        [self.redImgView addSubview:self.titleLbl];
        [self.redImgView addSubview:self.seeRed];
        [self.redImgView addSubview:self.bottomLbl];
        self.layer.masksToBounds = YES;
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)tapAtRed {
//    if ([self.delegate respondsToSelector:@selector(didTapMessageCell:)]) {
//        [self.delegate didTapMessageCell:self.model];
//        NSLog(@"%@",NSStringFromClass([self.model class]));
//        NSLog(@"%@",self.model.targetId);
//        NSLog(@"%@",self.model.userInfo.userId);
//    }
}

- (UIImageView *)redImgView {
    if (!_redImgView) {
        _redImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 80)];
        _redImgView.image = IMAGENAMED(@"red_packet_bg");
        _redImgView.contentMode = UIViewContentModeScaleAspectFill;
//        _redImgView.userInteractionEnabled = YES;
//        UITapGestureRecognizer *tap =
//        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAtRed)];
//        [_redImgView addGestureRecognizer:tap];
    }
    return _redImgView;
}
- (UIImageView *)smallRed {
    if (!_smallRed) {
        _smallRed = [[UIImageView alloc]initWithFrame:CGRectMake(10, 12, 30, 35)];
        _smallRed.image = IMAGENAMED(@"red_packet_icon");
        _smallRed.backgroundColor = [UIColor purpleColor];
        _smallRed.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _smallRed;
}
- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(50, 12, self.redImgView.width - 35, 15)];
        _titleLbl.font = FFont(12);
        _titleLbl.text = @"恭喜发财,大吉大利";
        _titleLbl.textColor = WHITECOLOR;
        
    }
    return _titleLbl;
}
- (UILabel *)seeRed {
    if (!_seeRed) {
        _seeRed = [[UILabel alloc]initWithFrame:CGRectMake(50, 30, 100, 15)];
        _seeRed.font = FFont(10);
        _seeRed.textColor = WHITECOLOR;
        _seeRed.text = @"查看红包";
    }
    return _seeRed;
}
- (UILabel *)bottomLbl {
    if (!_bottomLbl) {
        _bottomLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, self.redImgView.height - 15, 100, 10)];
        _bottomLbl.textColor = TEXTBLACK9;
        _bottomLbl.font = FFont(10);
        _bottomLbl.text = @"村村通红包";
    }
    return _bottomLbl;
}
@end
