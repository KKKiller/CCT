//
//  TDBecomeMasterController.h
//  CunCunTong
//
//  Created by TAL on 2019/6/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDBecomeMasterController : BaseViewController
@property (strong, nonatomic) NSString   *groupId;

@end

NS_ASSUME_NONNULL_END
