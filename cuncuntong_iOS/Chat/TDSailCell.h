//
//  TDSailCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TDSailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *rankImgView;
@property (weak, nonatomic) IBOutlet UILabel *rankLbl;
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *userCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@end

NS_ASSUME_NONNULL_END
