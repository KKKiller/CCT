//
//  CCChatListController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCChatListController.h"
#import "IQKeyboardManager.h"
#import "CCChatController.h"
#import "RCDUserInfo.h"
#import "CCUserInfoModel.h"
#import "RCDChatListCell.h"
#import "CCChatAddMsgController.h"
#import "CCGroupInfoModel.h"
#import "THChatController.h"
#import "CCChatAddFriendController.h"
@interface CCChatListController ()<RCIMUserInfoDataSource,RCIMGroupInfoDataSource>
@property(nonatomic, assign) BOOL isClick;

@end

@implementation CCChatListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[RCIM sharedRCIM] setUserInfoDataSource:self];
    [[RCIM sharedRCIM]setGroupInfoDataSource:self];
    [RCIM sharedRCIM].enableTypingStatus = YES;
    [RCIM sharedRCIM].enableMessageRecall =YES;
    
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE),
                                        @(ConversationType_DISCUSSION),
                                        @(ConversationType_CHATROOM),
                                        @(ConversationType_GROUP),
                                        @(ConversationType_APPSERVICE),
                                        @(ConversationType_SYSTEM)]];
    self.isShowNetworkIndicatorView = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.22f green:0.49f blue:1.00f alpha:1.00f];
    self.navigationController.navigationBar.titleTextAttributes =  @{NSFontAttributeName:[UIFont systemFontOfSize:18*KEY_RATE],NSForegroundColorAttributeName: [UIColor whiteColor]};
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self setExtraCellLineHidden:self.conversationListTableView];
    
//    [App_Delegate getVillageChat];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = @"聊聊";
    [self refreshConversationTableViewIfNeeded];
//    self.conversationListTableView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0);
    self.edgesForExtendedLayout = UIRectEdgeNone;
//    self.navigationController.navigationBar.translucent = NO;
}
- (void)onSelectedTableRow:(RCConversationModelType)conversationModelType
         conversationModel:(RCConversationModel *)model
               atIndexPath:(NSIndexPath *)indexPath {
    
    if (model.conversationType == RC_CONVERSATION_MODEL_TYPE_NORMAL) {
        if ([model.lastestMessage isKindOfClass:[RCContactNotificationMessage class]]) {
            RCContactNotificationMessage *msg =
            (RCContactNotificationMessage *)model.lastestMessage;
            if ([msg.operation isEqualToString:ContactNotificationMessage_ContactOperationRequest]) {
//                SHOW(@"请求");
                CCChatAddMsgController *vc = [[CCChatAddMsgController alloc]init];
                vc.uid = model.senderUserId;
                vc.addMsg  = msg.message;
                if ([msg.extra containsString:@"group"]) {
                    vc.groupId = msg.extra;
                }
                [self.navigationController pushViewController:vc
                                                     animated:YES];
            }else if ([msg.operation isEqualToString:ContactNotificationMessage_ContactOperationAcceptResponse]){
//                SHOW(@"同意");
                if (![model.senderUserId isEqualToString:USERID]) {
                    CCChatController *conversationVC = [[CCChatController alloc]init];
                    conversationVC.conversationType = model.conversationType;
                    conversationVC.targetId = model.senderUserId;
                    conversationVC.title = model.conversationTitle;
                    [self.navigationController pushViewController:conversationVC animated:YES];
                }else{
                    SHOW(@"通知性消息,无效点击");
                }
            }else if ([msg.operation isEqualToString:ContactNotificationMessage_ContactOperationRejectResponse]){
//                SHOW(@"拒绝");
                CCChatAddFriendController *vc = [[CCChatAddFriendController alloc]init];
                vc.uid = model.senderUserId;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                RCTextMessage *message = (RCTextMessage *)model.lastestMessage;
                if ([message.content isEqualToString:@"拒绝了您的好友请求"]) {
                    CCChatAddFriendController *vc = [[CCChatAddFriendController alloc]init];
                    vc.uid = model.senderUserId;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            
            }
        }else{
            if (![model.targetId isEqualToString:USERID]) {
                CCChatController *conversationVC = [[CCChatController alloc]init];
                conversationVC.conversationType = model.conversationType;
                conversationVC.targetId = model.targetId;
                conversationVC.title = model.conversationTitle;
                [self.navigationController pushViewController:conversationVC animated:YES];
            }else{
                SHOW(@"通知性消息,无效点击");
            }
        }
    
    }else if (model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION){ //加好友
        RCConversationModel *model =
        self.conversationListDataSource[indexPath.row];
        
        //好友消息
        if ([model.objectName isEqualToString:RCContactNotificationMessageIdentifier]) {
            RCContactNotificationMessage *msg =
            (RCContactNotificationMessage *)model.lastestMessage;
            if ([msg.operation isEqualToString:ContactNotificationMessage_ContactOperationRequest]) {
//                SHOW(@"请求");
                CCChatAddMsgController *vc = [[CCChatAddMsgController alloc]init];
                vc.uid = model.senderUserId;
                vc.addMsg  = msg.message;
                [self.navigationController pushViewController:vc
                                                     animated:YES];
            }else if ([msg.operation isEqualToString:ContactNotificationMessage_ContactOperationAcceptResponse]){
//                SHOW(@"同意");
                CCChatController *conversationVC = [[CCChatController alloc]init];
                conversationVC.conversationType = model.conversationType;
                conversationVC.targetId = model.senderUserId;
                conversationVC.title = model.conversationTitle;
                [self.navigationController pushViewController:conversationVC animated:YES];
            }else if ([msg.operation isEqualToString:ContactNotificationMessage_ContactOperationRejectResponse]){
//                SHOW(@"拒绝");
                CCChatAddFriendController *vc = [[CCChatAddFriendController alloc]init];
                vc.uid = model.senderUserId;
                [self.navigationController pushViewController:vc animated:YES];
            }
            }
    }else{
        if (![model.targetId isEqualToString:USERID]) {
            CCChatController *conversationVC = [[CCChatController alloc]init];
            conversationVC.conversationType = model.conversationType;
            conversationVC.targetId = model.targetId;
//            if ([model.targetId containsString:@"vill"]) {
//                conversationVC.conversationType = ConversationType_GROUP;
//                conversationVC.targetId = [model.targetId substringFromIndex:5];
//            }
            conversationVC.title = model.conversationTitle;
            [self.navigationController pushViewController:conversationVC animated:YES];
        }else{
            SHOW(@"通知性消息,无效点击");
        }
    }
    dispatch_after(
                   dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       [self refreshConversationTableViewIfNeeded];
                   });
}


- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *))completion {
    RCUserInfo *modelCache = [[RCIM sharedRCIM]getUserInfoCache:userId];
    if (modelCache) {
        completion(modelCache);
    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/tsinfo") params:@{@"ts_uid":STR(userId)} target:nil success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            RCUserInfo *model = [[RCUserInfo alloc]init];
            model.name = success[@"data"][@"info"][@"realname"];
            model.portraitUri = success[@"data"][@"info"][@"portrait"];
            if (!modelCache) {
                completion(model);
            }
            [[RCIM sharedRCIM] refreshUserInfoCache:model withUserId:userId];
            [self refreshConversationTableViewIfNeeded];
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)getGroupInfoWithGroupId:(NSString *)groupId
                     completion:(void (^)(RCGroup *groupInfo))completion{
    if ([groupId containsString:@"vill"]) {
        RCGroup *group = [[RCGroup alloc]init];
        group.groupId = groupId;
        group.groupName = @"同村";
        completion(group);
        return;
    }

    NSString *id = [groupId substringFromIndex:6];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_info") params:@{@"gid":STR(id)} target:nil success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
            RCGroup *group = [[RCGroup alloc]init];
            group.groupId = groupId;
            group.groupName = success[@"info"][@"group_name"];
            group.portraitUri = success[@"info"][@"avatar"];
            completion(group);
            [[RCIM sharedRCIM] refreshGroupInfoCache:group withGroupId:groupId];
            [self refreshConversationTableViewIfNeeded];

        }else{
            
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)setExtraCellLineHidden:(UITableView *)tableView {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}


#pragma mark - 自定义cell
//*********************插入自定义Cell*********************//
//自定义cell
- (RCConversationBaseCell *)rcConversationListTableView:(UITableView *)tableView
                                  cellForRowAtIndexPath:
(NSIndexPath *)indexPath {
    RCConversationModel *model = self.conversationListDataSource[indexPath.row];
    
    __block NSString *userName = nil;
    __block NSString *portraitUri = nil;
    RCContactNotificationMessage *_contactNotificationMsg = nil;
    
    __weak CCChatListController *weakSelf = self;
    //此处需要添加根据userid来获取用户信息的逻辑，extend字段不存在于DB中，当数据来自db时没有extend字段内容，只有userid
    if (nil == model.extend) {
        // Not finished yet, To Be Continue...
        if (model.conversationType == ConversationType_SYSTEM &&
            [model.lastestMessage
             isMemberOfClass:[RCContactNotificationMessage class]]) {
                _contactNotificationMsg =
                (RCContactNotificationMessage *)model.lastestMessage;
                if (_contactNotificationMsg.sourceUserId == nil) {
                    RCDChatListCell *cell =
                    [[RCDChatListCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:@""];
                    cell.lblDetail.text = @"好友请求";
                    [cell.ivAva setImageWithURL:URL(portraitUri) placeholder:IMAGENAMED(@"system_notice")];
                    return cell;
                }
                NSDictionary *_cache_userinfo = [[NSUserDefaults standardUserDefaults]
                                                 objectForKey:_contactNotificationMsg.sourceUserId];
                if (_cache_userinfo) {
                    userName = _cache_userinfo[@"username"];
                    portraitUri = _cache_userinfo[@"portraitUri"];
                } else {
                    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_info") params:@{@"f_uid":STR(_contactNotificationMsg.sourceUserId),@"uid":USERID} target:self success:^(NSDictionary *success) {
                        if ([success[@"stat"] integerValue] == 1) {
                            NSDictionary *user = success[@"info"];
                            RCDUserInfo *rcduserinfo_ = [RCDUserInfo new];
                            rcduserinfo_.name = user[@"realname"];
                            rcduserinfo_.userId = [NSString stringWithFormat:@"%@",user[@"rid"]];
                            rcduserinfo_.portraitUri = user[@"portrait"];
                            
                            model.extend = rcduserinfo_;

                            NSDictionary *userinfoDic = @{
                                                          @"username" : rcduserinfo_.name,
                                                          @"portraitUri" : rcduserinfo_.portraitUri
                                                          };
                            [[NSUserDefaults standardUserDefaults]
                             setObject:userinfoDic
                             forKey:_contactNotificationMsg.sourceUserId];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            [weakSelf.conversationListTableView reloadData];
//                            [weakSelf.conversationListTableView
//                             reloadRowsAtIndexPaths:@[ indexPath ]
//                             withRowAnimation:
//                             UITableViewRowAnimationAutomatic];
                        }
                    } failure:^(NSError *failure) {
                        NSLog(@"%@",failure);
                    }];
                }
            }
        
    } else {
        RCDUserInfo *user = (RCDUserInfo *)model.extend;
        userName = user.name;
        portraitUri = user.portraitUri;
    }
    
    RCDChatListCell *cell =
    [[RCDChatListCell alloc] initWithStyle:UITableViewCellStyleDefault
                           reuseIdentifier:@""];
    NSString *operation = _contactNotificationMsg.operation;
    NSString *operationContent;
    if ([operation isEqualToString:@"Request"]) {
        operationContent = [NSString stringWithFormat:@"来自%@的好友请求", userName];
    } else if ([operation isEqualToString:@"AcceptResponse"]) {
        operationContent = [NSString stringWithFormat:@"%@通过了你的好友请求", userName];
    }else if ([operation isEqualToString:@"RejectResponse"]){
        operationContent = [NSString stringWithFormat:@"%@拒绝了你的好友请求", userName];
    }
    cell.lblDetail.text = operationContent;
    [cell.ivAva setImageWithURL:URL(portraitUri) placeholder:IMAGENAMED(@"system_notice")];
    cell.labelTime.text = [RCKitUtility ConvertMessageTime:model.sentTime/1000];
    cell.model = model;
    return cell;
}
#pragma mark - 收到消息监听
- (void)didReceiveMessageNotification:(NSNotification *)notification {
    __weak typeof(&*self) blockSelf_ = self;
    //处理好友请求
    RCMessage *message = notification.object;
    if ([message.content isMemberOfClass:[RCContactNotificationMessage class]] ) {
        //![message.senderUserId isEqualToString:USERID]
        if (message.conversationType != ConversationType_SYSTEM) {
            NSLog(@"好友消息要发系统消息！！！");
        }
        RCContactNotificationMessage *_contactNotificationMsg =
        (RCContactNotificationMessage *)message.content;
        if (_contactNotificationMsg.sourceUserId == nil ||
            _contactNotificationMsg.sourceUserId.length == 0) {
            return;
        }
        //该接口需要替换为从消息体获取好友请求的用户信息
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_info") params:@{@"f_uid":STR(_contactNotificationMsg.sourceUserId)} target:nil success:^(NSDictionary *success) {
            if ([success[@"stat"] integerValue] == 1) {
                NSDictionary *user = success[@"info"];
                RCDUserInfo *rcduserinfo_ = [RCDUserInfo new];
                rcduserinfo_.name = user[@"realname"];
                rcduserinfo_.userId = [NSString stringWithFormat:@"%@",user[@"rid"]];
                rcduserinfo_.portraitUri = user[@"portrait"];
             
             RCConversationModel *customModel = [RCConversationModel new];
             customModel.conversationModelType =
             RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
             customModel.extend = rcduserinfo_;
             customModel.conversationType = message.conversationType;
             customModel.targetId = message.targetId;
             customModel.sentTime = message.sentTime;
             customModel.receivedTime = message.receivedTime;
             customModel.senderUserId = message.senderUserId;
             customModel.lastestMessage = _contactNotificationMsg;
             //[_myDataSource insertObject:customModel atIndex:0];
             
             // local cache for userInfo
             NSDictionary *userinfoDic = @{
                                           @"username" : rcduserinfo_.name,
                                           @"portraitUri" : rcduserinfo_.portraitUri
                                           };
             [[NSUserDefaults standardUserDefaults]
              setObject:userinfoDic
              forKey:_contactNotificationMsg.sourceUserId];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 //调用父类刷新未读消息数
                 [blockSelf_
                  refreshConversationTableViewWithConversationModel:
                  customModel];
                 [self notifyUpdateUnreadMessageCount];
                 
                 //当消息为RCContactNotificationMessage时，没有调用super，如果是最后一条消息，可能需要刷新一下整个列表。
                 //原因请查看super didReceiveMessageNotification的注释。
                 NSNumber *left =
                 [notification.userInfo objectForKey:@"left"];
                 if (0 == left.integerValue) {
                     [super refreshConversationTableViewIfNeeded];
                 }
             });
            }
        } failure:^(NSError *failure) {
            
        }];
        
    } else {
        //调用父类刷新未读消息数
        [super didReceiveMessageNotification:notification];
    }
}
//*********************插入自定义Cell*********************//

//插入自定义会话model
- (NSMutableArray *)willReloadTableData:(NSMutableArray *)dataSource {
    
    for (int i = 0; i < dataSource.count; i++) {
        RCConversationModel *model = dataSource[i];
        
        if ([model.senderUserId isEqualToString:USERID] && model.conversationModelType != RC_CONVERSATION_MODEL_TYPE_NORMAL) {
            [[RCIMClient sharedRCIMClient]
             removeConversation:model.conversationType
             targetId:model.targetId];
            [self refreshConversationTableViewIfNeeded];
        }
        
        //筛选请求添加好友的系统消息，用于生成自定义会话类型的cell
        if (model.conversationType == ConversationType_SYSTEM &&
            [model.lastestMessage
             isMemberOfClass:[RCContactNotificationMessage class]]) {
                model.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
            }
        if ([model.lastestMessage
             isKindOfClass:[RCGroupNotificationMessage class]]) {
            RCGroupNotificationMessage *groupNotification =
            (RCGroupNotificationMessage *)model.lastestMessage;
            
            
            //删除消息
            if ([groupNotification.operation isEqualToString:@"Quit"]) {
                NSData *jsonData =
                [groupNotification.data dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *dictionary = [NSJSONSerialization
                                            JSONObjectWithData:jsonData
                                            options:NSJSONReadingMutableContainers
                                            error:nil];
                NSDictionary *data =
                [dictionary[@"data"] isKindOfClass:[NSDictionary class]]
                ? dictionary[@"data"]
                : nil;
                NSString *nickName =
                [data[@"operatorNickname"] isKindOfClass:[NSString class]]
                ? data[@"operatorNickname"]
                : nil;
                if ([nickName isEqualToString:[RCIM sharedRCIM].currentUserInfo.name]) {
                    [[RCIMClient sharedRCIMClient]
                     removeConversation:model.conversationType
                     targetId:model.targetId];
                    [self refreshConversationTableViewIfNeeded];
                }
                
            }
        }
    }
    
    return dataSource;
}
@end
