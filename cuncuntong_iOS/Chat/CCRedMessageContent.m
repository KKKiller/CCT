//
//  CCRedMessageContent.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCRedMessageContent.h"

@implementation CCRedMessageContent
+ (instancetype)messageWithContent:(NSString *)content {
    CCRedMessageContent *text = [[CCRedMessageContent alloc] init];
    if (text) {
        text.content = content;
    }
    return text;
}
/////消息是否存储，是否计入未读数
//+ (RCMessagePersistent)persistentFlag {
//    return (MessagePersistent_ISPERSISTED | MessagePersistent_ISCOUNTED);
//}
///// 会话列表中显示的摘要
//- (NSString *)conversationDigest {
//    return self.content;
//}
//
///// NSCoding
//- (instancetype)initWithCoder:(NSCoder *)aDecoder {
//    self = [super init];
//    if (self) {
//        self.content = [aDecoder decodeObjectForKey:@"content"];
//        self.redId = [aDecoder decodeObjectForKey:@"redid"];//redid
//        self.senderName = [aDecoder decodeObjectForKey:@"realname"];//realname
//        self.senderAvatar = [aDecoder decodeObjectForKey:@"portrait"];//portrait
//    }
//    return self;
//}
//
///// NSCoding
//- (void)encodeWithCoder:(NSCoder *)aCoder {
//    [aCoder encodeObject:self.content forKey:@"content"];
//    [aCoder encodeObject:self.senderAvatar forKey:@"portrait"];
//    [aCoder encodeObject:self.senderName forKey:@"realname"];
//
//    [aCoder encodeObject:self.redId forKey:@"redid"];
//
//}
//
/////将消息内容编码成json
//- (NSData *)encode {
//    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
//    [dataDict setObject:self.content forKey:@"content"];
//    if (self.redId) {
//        [dataDict setObject:self.redId forKey:@"redid"];
//    }
//    if (self.senderName) {
//        [dataDict setObject:self.senderName forKey:@"realname"];
//    }
//    if (self.senderAvatar) {
//        [dataDict setObject:self.senderAvatar forKey:@"portrait"];
//    }
//    
//    if (self.senderUserInfo) {
//        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
//        if (self.senderUserInfo.name) {
//            [userInfoDic setObject:self.senderUserInfo.name
//                 forKeyedSubscript:@"name"];
//        }
//        if (self.senderUserInfo.portraitUri) {
//            [userInfoDic setObject:self.senderUserInfo.portraitUri
//                 forKeyedSubscript:@"icon"];
//        }
//        if (self.senderUserInfo.userId) {
//            [userInfoDic setObject:self.senderUserInfo.userId
//                 forKeyedSubscript:@"id"];
//        }
//        [dataDict setObject:userInfoDic forKey:@"user"];
//    }
//    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
//                                                   options:kNilOptions
//                                                     error:nil];
//    return data;
//}
//
/////将json解码生成消息内容
//- (void)decodeWithData:(NSData *)data {
//    if (data) {
//        __autoreleasing NSError *error = nil;
//        
//        NSDictionary *dictionary =
//        [NSJSONSerialization JSONObjectWithData:data
//                                        options:kNilOptions
//                                          error:&error];
//        
//        if (dictionary) {
//            self.content = dictionary[@"content"];
//            self.senderAvatar = dictionary[@"portrait"];
//            self.senderName = dictionary[@"realname"];
//            self.redId = dictionary[@"redid"];
//            
//            NSDictionary *userinfoDic = dictionary[@"user"];
//            [self decodeUserInfo:userinfoDic];
//        }
//    }
//}
//
//
//
/////消息的类型名
//+ (NSString *)getObjectName {
//    return @"RedPacketMessage";
//}
@end
