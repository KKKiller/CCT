//
//  CCChatVillageController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCChatVillageController : BaseViewController<UITableViewDataSource, UITableViewDelegate,
UISearchBarDelegate, UISearchControllerDelegate>

@property(nonatomic, strong) UISearchBar *searchFriendsBar;
@property(nonatomic, strong) UITableView *friendsTabelView;

@property(nonatomic, strong) NSDictionary *allFriendSectionDic;


@property(nonatomic, strong) NSArray *seletedUsers;

@property(nonatomic, strong) NSString *titleStr;

@property(nonatomic, strong) void (^selectUserList)(NSArray<RCUserInfo *> *selectedUserList);

@property (nonatomic, assign) BOOL notOnTab;
@property (nonatomic, strong) NSString *groupId;

@end
