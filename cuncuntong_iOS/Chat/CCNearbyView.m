//
//  CCNearbyView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/30.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCNearbyView.h"

@implementation CCNearbyView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
    }
    return self;
}

- (void)setModel:(CCNearbyModel *)model {
    _model = model;
    [self.headImgView setImageWithURL:URL(model.portrait) placeholder:IMAGENAMED(@"head")];
    self.nameLbl.text = model.realname;
    self.distanceLbl.text = [NSString stringWithFormat:@"%d千米",[model.distance intValue] /1000];
    self.typeLbl.text = model.relationship;
//    self.typeLbl.text = model.type == 1 ? @"同村" : model.type == 2 ? @"同乡" : @"同市";
    UIColor *color = [model.relationship isEqualToString:@"同村"] ? MTRGB(0xe54287) : [model.relationship isEqualToString:@"同乡"] ? MTRGB(0x398adf) : MTRGB(0x3cb8c3);
    self.typeLbl.textColor = color;
    self.typeLbl.layer.borderColor = color.CGColor;
    self.typeLbl.hidden = [TOOL stringEmpty:model.relationship];
}
- (void)setUI {
    self.headImgView = [[UIImageView alloc]init];
    [self addSubview:self.headImgView];
    self.headImgView.layer.cornerRadius = 22.5;
    self.headImgView.layer.masksToBounds = YES;
    self.headImgView.image=  IMAGENAMED(@"head");
    
    self.nameLbl = [[UILabel alloc]initWithText:@"陌生人" font:14 textColor:TEXTBLACK3];
    [self addSubview:self.nameLbl];
    self.nameLbl.numberOfLines = 1;
    self.nameLbl.textAlignment = NSTextAlignmentCenter;
    
    self.distanceLbl = [[UILabel alloc]initWithText:@"千米" font:12 textColor:TEXTBLACK6];
    [self addSubview:self.distanceLbl];
    self.distanceLbl.textAlignment = NSTextAlignmentCenter;
    
    self.typeLbl = [[UILabel alloc]initWithText:@"同村" font:12 textColor:TEXTBLACK6];
    [self addSubview:self.typeLbl];
    self.typeLbl.layer.cornerRadius = 3;
    self.typeLbl.layer.masksToBounds =YES;
    self.typeLbl.layer.borderWidth = 0.5;
    self.typeLbl.layer.borderColor = TEXTBLACK6.CGColor;
    self.typeLbl.textAlignment = NSTextAlignmentCenter;
}
- (void)layoutSubviews {
    [self.headImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(20);
        make.centerX.equalTo(self.mas_centerX);
        make.width.height.mas_equalTo(45);
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(5);
        make.right.equalTo(self.mas_right).offset(-5);
        make.top.equalTo(self.headImgView.mas_bottom).offset(5);
    }];
    [self.distanceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.nameLbl);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(3);
    }];
    [self.typeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headImgView.mas_top).offset(-10);
        make.left.equalTo(self.headImgView.mas_right).offset(-10);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(16);
    }];
}
@end
