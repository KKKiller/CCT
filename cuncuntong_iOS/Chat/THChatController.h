//
//  THChatController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/19.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "XHDisplayTextViewController.h"
#import "XHDisplayLocationViewController.h"
#import "XHAudioPlayerHelper.h"
#import "XHDisplayMediaViewController.h"
#import "MultiSelectViewController.h"
#import "ChatGroupSettingController.h"
#import "XMPP.h"
#import "XHLocationHelper.h"
#import "ICMessageFrame.h"
#import "XZConstants.h"
#import "ICChatConst.h"
#import "ICMessageConst.h"
#import "ICChatBoxViewController.h"
#import "ICVoiceHud.h"
#import "CCPostRedController.h"
#import "CCOpenRedView.h"
#import "CCRedMessageContent.h"
#import "CCRedLIstController.h"
#import "CCLiveController.h"

NS_ASSUME_NONNULL_BEGIN

@interface THChatController : XHMessageTableViewController
@property (nonatomic, strong) XMPPJID *toJid;
@property (nonatomic, strong) NSString *toUserId;
@property (nonatomic, strong) NSString *titleStr;

@property (nonatomic, strong) NSString *groupId;


@end

NS_ASSUME_NONNULL_END
