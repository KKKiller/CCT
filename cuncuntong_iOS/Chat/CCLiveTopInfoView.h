//
//  CCLiveTopInfoView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/10.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCLiveTopInfoView : UIView
+ (instancetype)instanceView;
@end

NS_ASSUME_NONNULL_END
