//
//  CCLiveController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/10.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCLiveController.h"
#import "CCLiveTopInfoView.h"
#if TARGET_IPHONE_SIMULATOR
#else
#import <AlivcLivePusher/AlivcLivePusher.h>
#import <AliyunPlayerSDK/AliVcMediaPlayer.h>
#endif
@interface CCLiveController ()
@property (nonatomic, strong) UIView *liveContainerV;
@property (nonatomic, strong) CCLiveTopInfoView *topInfoView;
@property (nonatomic, strong) UIButton *stopLiveBtn;
#if TARGET_IPHONE_SIMULATOR
#else
@property (nonatomic, strong) AliVcMediaPlayer *mediaPlayer;
@property (nonatomic, strong) AlivcLivePusher *livePusher;
@property (nonatomic, strong) NSString *liveId;
@property (nonatomic, strong) NSString *liveUrl;

#endif

@end

@implementation CCLiveController
#if TARGET_IPHONE_SIMULATOR
#else

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self layoutUI];

//    NSURL * url = [NSURL URLWithString:@"rtmp://live.hkstv.hk.lxdns.com/live/hks1"];
//    [self.mediaPlayer prepareToPlay:url];
//    [self.mediaPlayer play];
    [self play];
}
- (void)play {
    if (self.isPusher) {
        [self createLiveUrl];
    }else{
        [self getLiveUrl];
    }
}
- (void)createLiveUrl {
    [[MyNetWorking sharedInstance] PostUrlNew:BASELIVEURL_WITHOBJC(@"group/live/create") params:@{@"anchorId":USERID,@"groupId":STR(self.groupId)} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {
            self.liveUrl = success[@"pushUrl"];
            self.liveId = success[@"liveId"];
            [self playWithUrl:self.liveUrl];
        }
    } failure:^(NSError *failure) {

    }];
}
- (void)getLiveUrl {
    [[MyNetWorking sharedInstance]GetUrlNew:BASELIVEURL_WITHOBJC(@"group/live/query") params:@{@"groupId":STR(self.groupId)} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {
            NSString *url = success[@"pullUrl"];
            [self playWithUrl:url];
        }
    } failure:^(NSError *failure) {

    }];
}
- (void)playWithUrl:(NSString *)url{
    if (self.isPusher) {
        [self.livePusher startPreview:self.liveContainerV];
        int result = [self.livePusher startPushWithURL:url];
        NSLog(@"push reslut = %@",@(result));
    }else{
        [self.mediaPlayer prepareToPlay:[NSURL URLWithString:url]];
        [self.mediaPlayer play];
    }
}
- (void)stop {
    if (self.isPusher) {
        [self.livePusher stopPreview];
        [self.livePusher stopPush];
        [self.livePusher destory];
        [self stopPushLive];
    }else{
        [self.mediaPlayer stop];
    }
}
- (void)stopPushLive {
    [[MyNetWorking sharedInstance] PostUrlNew:BASELIVEURL_WITHOBJC(@"group/live/remove") params:@{@"groupId":STR(self.groupId),@"groupLiveId":STR(self.liveId)} success:^(NSDictionary *success) {
        NSLog(@"%@",success);
    } failure:^(NSError *failure) {
    }];
}
- (void)setupUI {
    [self.view addSubview:self.topInfoView];
    [self.view addSubview:self.liveContainerV];
    [self.view addSubview:self.stopLiveBtn];
}
- (void)layoutUI {
    [self.topInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    [self.liveContainerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.topInfoView.mas_bottom);
    }];
    [self.stopLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(30);
        make.bottom.equalTo(self.view.mas_bottom).offset(-15);
        make.right.equalTo(self.view).offset(15);
    }];
}

- (void)stopLive {
    UIAlertController *alert  = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认停止直播吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self stop];
        if (self.stopPushLiveBlk) {
            self.stopPushLiveBlk();
        }
    }] ;
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - 懒加载
- (AlivcLivePusher *)livePusher {
    if (!_livePusher) {
        AlivcLivePushConfig *config = [[AlivcLivePushConfig alloc]initWithResolution:AlivcLivePushResolution480P];
        config.enableAutoResolution = YES;
        config.previewDisplayMode = ALIVC_LIVE_PUSHER_PREVIEW_ASPECT_FILL;
        _livePusher = [[AlivcLivePusher alloc] initWithConfig:config];
    }
    return _livePusher;
}
- (AliVcMediaPlayer *)mediaPlayer {
    if (!_mediaPlayer) {
        //创建播放器
        _mediaPlayer = [[AliVcMediaPlayer alloc] init];
        [_mediaPlayer create:self.liveContainerV];
        //设置播放类型，0为点播、1为直播，默认使用自动
        _mediaPlayer.mediaType = MediaType_AUTO;
        //设置超时时间，单位为毫秒
        _mediaPlayer.timeout = 25000;
        //缓冲区超过设置值时开始丢帧，单位为毫秒。直播时设置，点播设置无效。范围：500～100000
        _mediaPlayer.dropBufferDuration = 8000;
        _mediaPlayer.scalingMode = scalingModeAspectFitWithCropping;
    }
    return _mediaPlayer;
}
- (UIView *)liveContainerV {
    if (!_liveContainerV) {
        _liveContainerV = [[UIView alloc]init];
        _liveContainerV.backgroundColor = [UIColor grayColor];
    }
    return _liveContainerV;
}
- (CCLiveTopInfoView *)topInfoView {
    if (!_topInfoView) {
        _topInfoView = [CCLiveTopInfoView instanceView];
    }
    return _topInfoView;
}
- (UIButton *)stopLiveBtn {
    if (!_stopLiveBtn) {
        _stopLiveBtn = [[UIButton alloc]init];
        _stopLiveBtn.backgroundColor = MTRGB(0x6C6C6C );
        [_stopLiveBtn setTitle:@"停止直播  " forState:UIControlStateNormal];
        _stopLiveBtn.layer.cornerRadius = 15;
        _stopLiveBtn.layer.masksToBounds = YES;
        _stopLiveBtn.titleLabel.font = FFont(12);
        [_stopLiveBtn addTarget:self action:@selector(stopLive) forControlEvents:UIControlEventTouchUpInside];
    }
    return _stopLiveBtn;
}
#endif

@end
