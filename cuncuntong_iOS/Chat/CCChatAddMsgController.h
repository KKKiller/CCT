//
//  CCChatAddMsgController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
typedef void (^ChatAddMsgFinishBlk)(BOOL isAccept);
@interface CCChatAddMsgController : BaseViewController
@property (nonatomic, strong) NSString *uid; //请求添加我的人
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *addMsg;
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic, strong) ChatAddMsgFinishBlk finishBlk;

@end
