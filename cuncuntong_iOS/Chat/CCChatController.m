//
//  CCChatController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCChatController.h"
#import "RCDUIBarButtonItem.h"
#import "RCDPrivateSettingsTableViewController.h"
#import "RCDGroupSettingsTableViewController.h"
#import "RCDGroupInfo.h"
#import "CCOpenRedView.h"
#import "CCPostRedController.h"
#import "CCRedLIstController.h"
#import "CCGroupSettingController.h"
#import "CCRedCell.h"
#import "CCRedMessageContent.h"
#import "CCGroupInfoModel.h"
@interface CCChatController ()<RCMessageCellDelegate>
@property(nonatomic, strong) RCGroup *groupInfo;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) CCRedMessageContent *red;

@end

@implementation CCChatController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavRightBtn];
    [self.chatSessionInputBarControl.pluginBoardView insertItemWithImage:IMAGENAMED(@"red_keyboard")
                                        title:@"红包"
                                          tag:201];
    //刷新个人或群组的信息
//    [self refreshUserInfoOrGroupInfo];
    if (self.conversationType == ConversationType_GROUP) {
        //群组改名之后，更新当前页面的Title
        self.groupInfo = [[RCGroup alloc]init];
        self.groupInfo.groupId = self.targetId;
        [self loadGroupInfo];
    }
    [self notifyUpdateUnreadMessageCount];
    [self addNOTI];
    [self registerClass:[CCRedCell class]
        forMessageClass:[CCRedMessageContent class]];
    [self.conversationMessageCollectionView registerClass:[CCRedCell class] forCellWithReuseIdentifier:@"CCRedCell"];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}
- (void)loadGroupInfo {
    NSLog(@"%@",self.targetId);
    NSString *groupId = nil;
    if ([self.targetId containsString:@"group_"]) {
        groupId = [self.targetId substringFromIndex:6];
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_info") params:@{@"gid":groupId} target:nil success:^(NSDictionary *success) {
            if ([success[@"stat"] integerValue] == 1) {
                RCGroup *group = [[RCGroup alloc]init];
                group.groupId = self.targetId;
                group.groupName = success[@"info"][@"group_name"];
                group.portraitUri = success[@"info"][@"avatar"];
                self.groupInfo = group;
            }else{
                SHOW(success[@"info"]);
            }
        } failure:^(NSError *failure) {
            
        }];
    }else if([self.targetId containsString:@"vill_"]){
        groupId = [self.targetId substringFromIndex:5];
        RCGroup *group = [[RCGroup alloc]init];
        group.groupId = self.targetId;
        group.groupName = @"同村群";
        self.groupInfo = group;
    }
    
}
- (void)setNavRightBtn {
    if (self.conversationType == ConversationType_GROUP) {
        [self setRightNavigationItem:[UIImage imageNamed:@"Group_Setting"]
                           withFrame:CGRectMake(10, 3.5, 21, 19.5)];
    } else {
        [self setRightNavigationItem:[UIImage imageNamed:@"Private_Setting"]
                           withFrame:CGRectMake(15, 3.5, 16, 18.5)];
    }
}
- (void)setRightNavigationItem:(UIImage *)image withFrame:(CGRect)frame {
    RCDUIBarButtonItem *rightBtn = [[RCDUIBarButtonItem alloc]
                                    initContainImage:image
                                    imageViewFrame:frame
                                    buttonTitle:nil
                                    titleColor:nil
                                    titleFrame:CGRectZero
                                    buttonFrame:CGRectMake(0, 0, 25, 25)
                                    target:self
                                    action:@selector(rightBarButtonItemClicked:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}
- (void)rightBarButtonItemClicked:(UIButton *)sender {
    if (self.conversationType == ConversationType_PRIVATE){
        RCDPrivateSettingsTableViewController *settingsVC = [RCDPrivateSettingsTableViewController privateSettingsTableViewController];
        settingsVC.userId = self.targetId;
        [self.navigationController pushViewController:settingsVC animated:YES];
    }else if (self.conversationType == ConversationType_GROUP){
    CCGroupSettingController *vc = [[CCGroupSettingController alloc]init];
        vc.groupInfo = _groupInfo;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
}
-(void)pluginBoardView:(RCPluginBoardView*)pluginBoardView clickedItemWithTag:(NSInteger)tag{
    if(tag == 201){ //红包
        [self postRed];
    }else{
        [super pluginBoardView:pluginBoardView clickedItemWithTag:tag];
    }
}


#pragma mark - 通知
- (void)addNOTI {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateTitleForGroup:)
                                                 name:@"UpdeteGroupInfo"
                                               object:nil];
    //清除历史消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(clearHistoryMSG:)
                                                 name:@"ClearHistoryMsg"
                                               object:nil];
}
//清楚历史消息
- (void)clearHistoryMSG:(NSNotification *)notification {
    [self.conversationDataRepository removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.conversationMessageCollectionView reloadData];
    });
}
//更新群组名字
- (void)updateTitleForGroup:(NSNotification *)notification {
    self.title = notification.object;
}

/**
 *  更新左上角未读消息数
 */
- (void)notifyUpdateUnreadMessageCount {
    __weak typeof(&*self) __weakself = self;
    int count = [[RCIMClient sharedRCIMClient] getUnreadCount:@[
                                                                @(ConversationType_PRIVATE),
                                                                @(ConversationType_DISCUSSION),
                                                                @(ConversationType_APPSERVICE),
                                                                @(ConversationType_PUBLICSERVICE),
                                                                @(ConversationType_GROUP)
                                                                ]];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *backString = nil;
        if (count > 0 && count < 1000) {
            backString = [NSString stringWithFormat:@"返回(%d)", count];
        } else if (count >= 1000) {
            backString = @"返回(...)";
        } else {
            backString = @"返回";
        }
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = CGRectMake(0, 6, 87, 23);
        UIImageView *backImg = [[UIImageView alloc]
                                initWithImage:[UIImage imageNamed:@"navigator_btn_back"]];
        backImg.frame = CGRectMake(-6, 4, 10, 17);
        [backBtn addSubview:backImg];
        UILabel *backText =
        [[UILabel alloc] initWithFrame:CGRectMake(9, 4, 85, 17)];
        backText.text = backString; // NSLocalizedStringFromTable(@"Back",
        // @"RongCloudKit", nil);
        //   backText.font = [UIFont systemFontOfSize:17];
        [backText setBackgroundColor:[UIColor clearColor]];
        [backText setTextColor:[UIColor whiteColor]];
        [backBtn addSubview:backText];
        [backBtn addTarget:__weakself
                    action:@selector(leftBarButtonItemPressed)
          forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftButton =
        [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        [__weakself.navigationItem setLeftBarButtonItem:leftButton];
    });
}
- (void)leftBarButtonItemPressed {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - 红包cell
- (RCMessageBaseCell *)rcConversationCollectionView:(UICollectionView *)collectionView
                             cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RCMessageModel *model = self.conversationDataRepository[indexPath.row];
    if ([model isKindOfClass:[CCRedMessageContent class]]) {
        RCMessageBaseCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CCRedCell" forIndexPath:indexPath];
        [cell setDataModel:model];
        return cell;
    }
    return [super rcConversationCollectionView:collectionView cellForItemAtIndexPath:indexPath];
}
//-(CGSize)rcConversationCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    //返回自定义cell的实际高度（这里请返回消息的实际大小）
//    return CGSizeMake(300, 140);
//}

- (void)didTapMessageCell:(RCMessageModel *)model {
    [super didTapMessageCell:model];
    if ([model.content isKindOfClass:[CCRedMessageContent class]]) {
        CCRedMessageContent *msg = (CCRedMessageContent *)model.content;
        NSLog(@"红包id%@",msg.redId);
        self.red = msg;
        [self.view endEditing:YES];
        [self openRed];
    }
    
}
//发红包
- (void)postRed {
    CCPostRedController *vc = [[CCPostRedController alloc]init];
    vc.toId = self.targetId;
    vc.isGroup = self.conversationType == ConversationType_GROUP ;
    [self.navigationController pushViewController:vc animated:YES];
}
//弹出红包
- (void)openRed {
    CCOpenRedView *view = [CCOpenRedView instanceView];
    [view.closeBtn addTarget:self action:@selector(closeRed) forControlEvents:UIControlEventTouchUpInside];
    [view.openBtn addTarget:self action:@selector(getRed) forControlEvents:UIControlEventTouchUpInside];
    [view.showDetailBtn addTarget:self action:@selector(redDetail) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, 240, 320);
    view.isOver = NO;
    view.nameLbl.text = self.red.senderName;
    view.descLbl.text = self.red.content;
    [view.headImgView setImageWithURL:URL(self.red.senderAvatar) placeholder:IMAGENAMED(@"head")];
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:self.view];
}
- (void)closeRed {
    [self.animateModel closeWithStraight];
}
//抢红包
- (void)getRed {
    if ([self.red.senderName isEqualToString:[UserManager sharedInstance].realname]) {
        [self redDetail];
    }else{
        //群红包
        if([self.targetId containsString:@"vill"] || [self.targetId containsString:@"group"]){
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"qiang") params:@{@"id":USERID,@"redid":STR(self.red.redId)} target:self success:^(NSDictionary *success) {
                //红包列表
                [self redDetail];
            } failure:^(NSError *failure) {
                
            }];
        }else{
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"pqiang") params:@{@"id":USERID,@"redid":STR(self.red.redId)} target:self success:^(NSDictionary *success) {
                //红包列表
                [self redDetail];
            } failure:^(NSError *failure) {
                //        SHOW(@"不能抢自己发的红包");
            }];
        }
        
    }
}
- (void)redDetail {
    [self closeRed];
    CCRedLIstController *vc = [[CCRedLIstController alloc]init];
    vc.redid = self.red.redId;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
