//
//  LBFirendRequestModel.h
//  L8
//
//  Created by 周吾昆 on 2018/6/16.
//  Copyright © 2018年 周吾昆. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LBFriendRequestModel : NSObject<NSCoding>
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSDate *create_date;
@property (nonatomic, strong) NSString *is_accept; // 0未处理 1同意  2拒绝
- (NSMutableDictionary *)changeModelToDic;
@end
