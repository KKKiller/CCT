//
//  CCChatVillageController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCChatVillageController.h"
#import "DefaultPortraitView.h"
#import "RCDContactTableViewCell.h"
#import "RCDUserInfo.h"
#import "UIImageView+WebCache.h"
#import "RCDUtilities.h"
#import "RCDUIBarButtonItem.h"
#import "CCChatAddFriendController.h"
#import "THChatController.h"
@interface CCChatVillageController ()<UITableViewDelegate,UITableViewDataSource>
@property(strong, nonatomic) NSMutableArray *matchFriendList;
@property(nonatomic, assign) BOOL hasSyncFriendList;
@property(nonatomic, assign) BOOL isBeginSearch;
@property(nonatomic, strong) NSMutableDictionary *resultDic;
@property (nonatomic, strong) NSMutableArray *friendList;
@property (nonatomic, strong) RCDUserInfo *admin;
@end

@implementation CCChatVillageController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    self.friendList = [NSMutableArray array];
    
    self.friendsTabelView.backgroundColor = BACKGRAY;
    [NOTICENTER addObserver:self selector:@selector(getAllFriendList) name:kFriendChangeNotification object:nil];
    [self getAllFriendList];
    if (self.notOnTab) {
        self.title = @"群成员";
    }
}

#pragma mark - 获取好友并且排序
//获取好友
- (void )getAllFriendList {
    if (self.groupId && [self.groupId containsString:@"group_"]) { //群成员
        NSString *group = [self.groupId containsString:@"vill_"] ? [self.groupId substringFromIndex:5] : [self.groupId containsString:@"group_"] ? [self.groupId substringFromIndex:6] : self.groupId;
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_members") params:@{@"gid":group} target:self success:^(NSDictionary *success) {
                if ([success[@"stat"] integerValue] == 1) {
                    [self setData:success];
                }else{
                    SHOW(success[@"info"]);
                }
            } failure:^(NSError *failure) {
                
            }];
    }else{
        
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/myvillage") params:@{@"uid":USERID} target:self success:^(NSDictionary *success) {
            if ([success[@"stat"] integerValue] == 1) {
                [self setData:success];
                [TOOL saveDataWithData:success Name:@"village"];
                [self sendMessageToVillageForFirstTime];
            }else{
                SHOW(success[@"info"]);
            }
            
        } failure:^(NSError *failure) {
        }];
    }
    
}
- (void)setData:(NSDictionary *)success {
    [self.friendList removeAllObjects];
    NSArray *admins = success[@"info"][@"admins"];
    if (admins.count > 0) {
        NSDictionary *dict = admins[0];
        self.admin = [[RCDUserInfo alloc]init];
        self.admin.name = dict[@"realname"];
        self.admin.portraitUri = dict[@"portrait"];
        self.admin.userId = [NSString stringWithFormat:@"%@",dict[@"rid"]];
    }
    NSArray *members = success[@"info"][@"members"];
    for (NSDictionary *dict in members) {
        RCDUserInfo *user = [[RCDUserInfo alloc]init];
        user.portraitUri = dict[@"portrait"];
        user.name = dict[@"realname"];
        user.userId = [NSString stringWithFormat:@"%@", dict[@"rid"]];
        [self.friendList addObject:user];
    }
    [self sortAndRefreshWithList:self.friendList];
}


- (void)sendMessageToVillageForFirstTime {
    NSString *key = [NSString stringWithFormat:@"sendVillMessage%@",USERID];

    if(![Def boolForKey:key]){
        
        NSString *village = [UserManager sharedInstance].location;
        NSString *villageId = [NSString stringWithFormat:@"vill_%@",village];
        NSString *text = [NSString stringWithFormat:@"%@加入了同村群",[UserManager sharedInstance].realname];
        RCTextMessage *message = [RCTextMessage messageWithContent:text];
        [[RCIM sharedRCIM] sendMessage:ConversationType_GROUP targetId:villageId content:message pushContent:nil pushData:nil success:^(long messageId) {
            [Def setBool:YES forKey:key];
        } error:^(RCErrorCode nErrorCode, long messageId) {
            
        }];
    }
}



//排序
- (void)sortAndRefreshWithList:(NSArray *)friendList {
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    if(friendList.count > 100){
        friendList = [friendList subarrayWithRange:NSMakeRange(0, 100)];
    }
        self.resultDic = [RCDUtilities sortedArrayWithPinYinDic:friendList];
//        dispatch_async(dispatch_get_main_queue(), ^{
            self.allFriendSectionDic = self.resultDic [@"infoDic"];
            [self.friendsTabelView reloadData];
//        });
//    });
}

- (void)setUI {
   
    
    [self.friendsTabelView
     setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1]];
    self.view.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
    
    [self.view addSubview:self.searchFriendsBar];
    [self.view addSubview:self.friendsTabelView];
    // initial data
    self.matchFriendList = [[NSMutableArray alloc] init];
    self.allFriendSectionDic = [[NSDictionary alloc] init];
    
    self.friendsTabelView.tableFooterView = [UIView new];
    self.friendsTabelView.backgroundColor = HEXCOLOR(0xf0f0f6);
    self.friendsTabelView.separatorColor = HEXCOLOR(0xdfdfdf);
    
    self.friendsTabelView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.friendsTabelView.bounds.size.width, 0.01f)];
    
    //设置右侧索引
    self.friendsTabelView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.friendsTabelView.sectionIndexColor = HEXCOLOR(0x555555);
    
    if ([self.friendsTabelView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.friendsTabelView setSeparatorInset:UIEdgeInsetsMake(0, 14, 0, 0)];
    }
    if ([self.friendsTabelView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.friendsTabelView setLayoutMargins:UIEdgeInsetsMake(0, 14, 0, 0)];
    }
    
    UIImage* searchBarBg = [self GetImageWithColor:[UIColor clearColor] andHeight:32.0f];
    //设置顶部搜索栏的背景图片
    [self.searchFriendsBar setBackgroundImage:searchBarBg];
    //设置顶部搜索栏的背景色
    [self.searchFriendsBar setBackgroundColor:HEXCOLOR(0xf0f0f6)];
    
    //设置顶部搜索栏输入框的样式
    UITextField *searchField = [self.searchFriendsBar valueForKey:@"_searchField"];
    searchField.layer.borderWidth = 0.5f;
    searchField.layer.borderColor = [HEXCOLOR(0xdfdfdf) CGColor];
    searchField.layer.cornerRadius = 5.f;
    self.searchFriendsBar.placeholder = @"搜索";
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     self.navigationController.navigationBarHidden = NO;
    self.tabBarController.navigationItem.title = @"同村";
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//    self.navigationController.navigationBar.translucent = NO;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchFriendsBar resignFirstResponder];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_isBeginSearch == YES) {
        [self sortAndRefreshWithList:self.friendList];
        _isBeginSearch = NO;
        self.searchFriendsBar.showsCancelButton = NO;
        [self.searchFriendsBar resignFirstResponder];
        self.searchFriendsBar.text = @"";
        [self.matchFriendList removeAllObjects];
        [self.friendsTabelView setContentOffset:CGPointMake(0,0) animated:NO];
    }
}



#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = 0;
//    if (section == 0) {
//        if (_isBeginSearch == YES) {
//            rows = 0;
//        }
//        else{
//            rows = self.admin ? 1 : 0;
//        }
//    } else {
        NSString *letter = self.resultDic[@"allKeys"][section];
        rows = [self.allFriendSectionDic[letter] count];
//    }
    return rows;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.resultDic[@"allKeys"] count] ;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.frame = CGRectMake(0, 0, self.view.frame.size.width, 22);
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.frame = CGRectMake(13, 3, 45, 15);
    title.font = [UIFont systemFontOfSize:14.f];
    title.textColor = HEXCOLOR(0x999999);
    
    [view addSubview:title];
    
//    if (section == 0) {
//        title.text = self.admin ? @"群主" : @"";
//    } else {
        title.text = self.resultDic[@"allKeys"][section];
//    }
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *isDisplayID = [[NSUserDefaults standardUserDefaults] objectForKey:@"isDisplayID"];
    static NSString *reusableCellWithIdentifier = @"RCDContactTableViewCell";
    RCDContactTableViewCell *cell = [self.friendsTabelView
                                     dequeueReusableCellWithIdentifier:reusableCellWithIdentifier];
    if (cell == nil) {
        cell = [[RCDContactTableViewCell alloc] init];
    }
    
//    if (indexPath.section == 0 && indexPath.row < 3) {
//
//        cell.nicknameLabel.text =self.admin.name;
//        [cell.portraitView setImageWithURL:URL(self.admin.portraitUri) placeholder:IMAGENAMED(@"contact")];
//    }
//    if (indexPath.section != 0) {
        NSString *letter = self.resultDic[@"allKeys"][indexPath.section];
        
        NSArray *sectionUserInfoList = self.allFriendSectionDic[letter];
        RCDUserInfo *userInfo = sectionUserInfoList[indexPath.row];
        if (userInfo) {
            if ([isDisplayID isEqualToString:@"YES"]) {
                cell.userIdLabel.text = userInfo.userId;
            }
            cell.nicknameLabel.text = userInfo.name;
            if ([userInfo.portraitUri isEqualToString:@""]) {
                DefaultPortraitView *defaultPortrait = [[DefaultPortraitView alloc]
                                                        initWithFrame:CGRectMake(0, 0, 100, 100)];
                [defaultPortrait setColorAndLabel:userInfo.userId Nickname:userInfo.name];
                UIImage *portrait = [defaultPortrait imageFromView];
                cell.portraitView.image = portrait;
            } else {
                [cell.portraitView setImageWithURL:URL(userInfo.portraitUri) placeholder:IMAGENAMED(@"contact")];
//                [cell.portraitView
//                 sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri]
//                 placeholderImage:[UIImage imageNamed:@"contact"]];
            }
        }
//    }
//    if ([RCIM sharedRCIM].globalConversationAvatarStyle == RC_USER_AVATAR_CYCLE &&
//        [RCIM sharedRCIM].globalMessageAvatarStyle == RC_USER_AVATAR_CYCLE) {
//        cell.portraitView.layer.masksToBounds = YES;
//        cell.portraitView.layer.cornerRadius = 20.f;
//    } else {
        cell.portraitView.layer.masksToBounds = YES;
        cell.portraitView.layer.cornerRadius = 5.f;
//    }
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.portraitView.contentMode = UIViewContentModeScaleAspectFill;
    cell.nicknameLabel.font = [UIFont systemFontOfSize:15.f];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        return 0;
//    }
    return 21.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1f;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.resultDic[@"allKeys"];
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RCDUserInfo *userInfo = nil;
    if (self.admin) {
        if (indexPath.section == 0 ) {
            userInfo = self.admin;
        }else{
            NSString *letter = self.resultDic[@"allKeys"][indexPath.section -1];
            NSArray *sectionUserInfoList = self.allFriendSectionDic[letter];
            userInfo = sectionUserInfoList[indexPath.row];
        }
    }else{
        NSString *letter = self.resultDic[@"allKeys"][indexPath.section];
        NSArray *sectionUserInfoList = self.allFriendSectionDic[letter];
        userInfo = sectionUserInfoList[indexPath.row];
    }
    
    
    
    if (![userInfo.userId isEqualToString:USERID]) {
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_mine") params:@{@"uid":USERID,@"f_uid":STR(userInfo.userId)} target:self success:^(NSDictionary *success) {
            if ([success[@"stat"] integerValue] == 3) { //是我的好友
                THChatController *vc = [[THChatController alloc]init];
                vc.toJid = [XMPPTOOL getJIDWithUserId: userInfo.userId];
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                CCChatAddFriendController *vc = [[CCChatAddFriendController alloc]init];
                vc.uid = userInfo.userId;
                [self.navigationController pushViewController:vc animated:YES];
            }
        } failure:^(NSError *failure) {
        }];
        
    }else{
        SHOW(@"不能跟自己聊天");
    }

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.searchFriendsBar resignFirstResponder];
}

#pragma mark - UISearchBarDelegate
/**
 *  执行delegate搜索好友
 */
- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    [self.matchFriendList removeAllObjects];
    if (searchText.length <= 0) {
        [self sortAndRefreshWithList:self.friendList];
    } else {
        for (RCUserInfo *userInfo in self.friendList) {
            //忽略大小写去判断是否包含
            if ([userInfo.name rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound
                || [[RCDUtilities hanZiToPinYinWithString:userInfo.name] rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [self.matchFriendList addObject:userInfo];
            }
        }
        [self sortAndRefreshWithList:self.matchFriendList];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchFriendsBar.showsCancelButton = NO;
    [self.searchFriendsBar resignFirstResponder];
    self.searchFriendsBar.text = @"";
    [self.matchFriendList removeAllObjects];
    [self sortAndRefreshWithList:self.friendList];
    _isBeginSearch = NO;
    [self.friendsTabelView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (_isBeginSearch == NO) {
        _isBeginSearch = YES;
        [self.friendsTabelView reloadData];
    }
    self.searchFriendsBar.showsCancelButton = YES;
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - 私有方法
/**
 *  添加好友
 *
 *  @param sender sender description
 */
- (void)pushAddFriend:(id)sender {
    //    RCDSearchFriendViewController *searchFirendVC =
    //    [RCDSearchFriendViewController searchFriendViewController];
    //    [self.navigationController pushViewController:searchFirendVC animated:YES];
}


- (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height
{
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

#pragma mark - 懒加载
- (UISearchBar *)searchFriendsBar {
    if (!_searchFriendsBar) {
        _searchFriendsBar=[[UISearchBar alloc]initWithFrame:CGRectMake(2, WCFNavigationHeight, App_Width-4, 28)];
        [_searchFriendsBar sizeToFit];
        [_searchFriendsBar setPlaceholder:NSLocalizedStringFromTable(@"ToSearch", @"RongCloudKit", nil)];
        [_searchFriendsBar.layer setBorderWidth:0.5];
        [_searchFriendsBar.layer setBorderColor:[UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1].CGColor];
        [_searchFriendsBar setDelegate:self];
        [_searchFriendsBar setKeyboardType:UIKeyboardTypeDefault];
    }
    return _searchFriendsBar;
}

- (UITableView *)friendsTabelView {
    if (!_friendsTabelView) {
        CGFloat height = self.notOnTab ? WCFTabBarHeight : 0;
        _friendsTabelView=[[UITableView alloc]initWithFrame:CGRectMake(0.0, CGRectGetMaxY(self.searchFriendsBar.frame), RCDscreenWidth, RCDscreenHeight-43.5-114 + height) style:UITableViewStyleGrouped];
        [_friendsTabelView setDelegate:self];
        [_friendsTabelView setDataSource:self];
        [_friendsTabelView setSectionIndexBackgroundColor:[UIColor clearColor]];
        [_friendsTabelView setSectionIndexColor:[UIColor darkGrayColor]];
        [_friendsTabelView setBackgroundColor:[UIColor colorWithRed:240.0/255 green:240.0/255 blue:240.0/255 alpha:1]];
        //        _friendsTabelView.style = UITableViewStyleGrouped;
        //        _friendsTabelView.tableHeaderView=self.searchFriendsBar;
        //cell无数据时，不显示间隔线
        UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
        [_friendsTabelView setTableFooterView:v];
    }
    return _friendsTabelView;
}


#pragma mark - 侧滑功能

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    RCDUserInfo *userInfo = nil;
    if (indexPath.section == 0) {
        userInfo = self.admin;
    }else{
        NSString *letter = self.resultDic[@"allKeys"][indexPath.section -1];
        NSArray *sectionUserInfoList = self.allFriendSectionDic[letter];
        userInfo = sectionUserInfoList[indexPath.row];
    }
    //添加一个删除按钮
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:@"踢出" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        //踢出好友
        NSString *villigeId = [UserManager sharedInstance].location;
        if ([self.admin.userId isEqualToString:USERID]) {
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_member_rm") params:@{@"gid":villigeId,@"uid":STR(userInfo.userId)} target:self success:^(NSDictionary *success) {
                if ([success[@"stat"] integerValue] == 1) {
                    [self getAllFriendList];
                    SHOW(@"踢出成功");
                }else{
                    SHOW(success[@"info"]);
                }
            } failure:^(NSError *failure) {
                
            }];
        }else{
            SHOW(@"只有群主才有权限");
        }

    }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    UITableViewRowAction *muteAction =[UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:@"拉黑" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        //禁言好友
        
        if ([self.admin.userId isEqualToString:USERID]) {
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/myvillage_rm") params:@{@"uid":STR(userInfo.userId)} target:self success:^(NSDictionary *success) {
                if ([success[@"stat"] integerValue] == 1) {
                    SHOW(@"拉黑成功");
                }else{
                    SHOW(success[@"info"]);
                }
            } failure:^(NSError *failure) {
                
            }];
        }else{
            SHOW(@"只有群主才有权限");
        }
    }];
    muteAction.backgroundColor = MTRGB(0xd59621);
    //将设置好的按钮方到数组中返回
    return @[deleteAction,muteAction];
}
@end
