//
//  TSOrderTradingController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCTDSailController.h"
#import "TSOrderDetailController.h"
#import "TSOrderInfo.h"
#import "TDSailCell.h"
#import "CCTGroupModel.h"
#import "UIImageView+WebCache.h"
#import "TDBecomeMasterController.h"
static NSString *cellID = @"TDSailCell";
@interface CCTDSailController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;

@end

@implementation CCTDSailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    [self initRefreshView];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = CGRectMake(0, 0, App_Width, CGRectGetMaxY(self.view.bounds) - 50);
    [self addBottomBtn];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x387DFF);

}

- (void)addBottomBtn{
    UIView *bottmV = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame), App_Width, 50)];
    bottmV.backgroundColor = [UIColor whiteColor];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(80, 5, App_Width - 160, 40);
    btn.backgroundColor = MTRGB(0x5191FF);
    btn.layer.cornerRadius = 18;
    btn.layer.masksToBounds = YES;
    [btn setTitle:@"我要成为群主" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [btn addTarget:self action:@selector(becomeQunZhu) forControlEvents:UIControlEventTouchUpInside];
    [bottmV addSubview:btn];
    [self.view addSubview:bottmV];
}

- (void)becomeQunZhu{
    NSLog(@"我要成为群主");
}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
#pragma mark - 获取数据
- (void)loadData {
    WEAKSELF
    [[MyNetWorking sharedInstance] GetUrlNew :BASELIVEURL_WITHOBJC(@"group/auction/list") params:@{@"userId":USERID} success:^(NSDictionary *success) {
        NSLog(@"%@", success);
        [weakSelf.dataArray removeAllObjects];
        NSNumber *code = success[@"code"];
        if ([code.stringValue isEqualToString:@"201"]){
            for (NSDictionary *dic in success[@"groups"]){
                CCTGroupModel *model = [CCTGroupModel modelWithDictionary:dic];
                [weakSelf.dataArray appendObject:model];
            }
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
        }else {
            [MBProgressHUD showMessage:success[@"reason"]];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
        }
    } failure:^(NSError *failure) {
        NSLog(@"%@", failure.description);
    }];
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.dataArray.count;
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TDSailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    CCTGroupModel *model = self.dataArray[indexPath.row];
    
    cell.rankImgView.hidden = YES;
    cell.rankLbl.hidden = YES;
//    if (self.dataArray.count > indexPath.row) {
        if (indexPath.row <= 2){
            cell.rankImgView.hidden = NO;
            cell.rankImgView.image = indexPath.row == 0 ? IMAGENAMED(@"first") : indexPath.row == 1 ? IMAGENAMED(@"second") : IMAGENAMED(@"third");
        }else {
            cell.rankLbl.hidden = NO;
            cell.rankLbl.text = [NSString stringWithFormat:@"%ld", indexPath.row];
        }
    cell.titleLbl.text = model.groupName;
    cell.userCountLbl.text = [NSString stringWithFormat:@"%@  ", model.membersCount];
    cell.priceLbl.text = model.entryPrice;
    [cell.headImgView  sd_setImageWithURL:[NSURL URLWithString:model.groupAvatar]
                         placeholderImage:[UIImage imageNamed:@"icon_person"]];
    
    //    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCTGroupModel *model = self.dataArray[indexPath.row];
        TDBecomeMasterController *vc = [[UIStoryboard storyboardWithName:@"Live" bundle:nil] instantiateViewControllerWithIdentifier:@"BecomeMasterController"];
        vc.groupId = model.groupId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshData];
    }];
    
//    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf loadData];
//    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
        CGFloat yy = system >= 11.0 ? 64 : 0;
        _tableView = [[UITableView alloc]init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 64;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"TDSailCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 64 - 49)];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
