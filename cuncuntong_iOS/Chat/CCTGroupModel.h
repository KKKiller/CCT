//
//  CCTGroupModel.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/5/4.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCTGroupModel : NSObject
@property(nonatomic, strong) NSString *entryPrice;
@property(nonatomic, strong)NSString *groupLiveId;
@property(nonatomic, strong)NSString *groupName;
@property(nonatomic, strong)NSString *childGroup;
@property(nonatomic, strong)NSString *membersCount;
@property(nonatomic, strong)NSString *groupAdmin;
@property(nonatomic, strong)NSString *groupOwner;
@property(nonatomic, strong)NSString *groupId;
@property(nonatomic, strong)NSString *groupApply;
@property(nonatomic, strong)NSString *groupAvatar;
@property(nonatomic, strong)NSString *groupLiveRoomId;
@property(nonatomic, strong)NSString *groupIntroduce;
@end

NS_ASSUME_NONNULL_END
