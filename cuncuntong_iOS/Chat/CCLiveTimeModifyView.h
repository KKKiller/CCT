//
//  CCLiveTimeModifyView.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/3/2.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCLiveTimeModifyView : UIView
@property (strong, nonatomic) IBOutletCollection(UITapGestureRecognizer) NSArray *ges0Arr;
@property (strong, nonatomic) IBOutletCollection(UITapGestureRecognizer) NSArray *ges1Arr;
@property (strong, nonatomic) IBOutletCollection(UITapGestureRecognizer) NSArray *ges2Arr;
@property (weak, nonatomic) IBOutlet UIButton *btn;
+ (instancetype)instanceView;
@end

NS_ASSUME_NONNULL_END
