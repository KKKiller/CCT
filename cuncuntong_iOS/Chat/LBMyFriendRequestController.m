//
//  LBMyFriendRequestController.m
//  L8
//
//  Created by 周吾昆 on 2018/6/16.
//  Copyright © 2018年 周吾昆. All rights reserved.
//

#import "LBMyFriendRequestController.h"
#import "LBAddFriendRequestCell.h"
#import "LBFriendRequestModel.h"
#import "MTEmptyView.h"
#import "RCDSearchFriendViewController.h"

@interface LBMyFriendRequestController ()<UITableViewDelegate,UITableViewDataSource,LBAddFriendRequestCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) MTEmptyView *emptyView;
@property (nonatomic, assign) NSInteger pageIndex;

@end

@implementation LBMyFriendRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"好友请求";
    [self.view addSubview:self.tableView];
    [self setRightBtn:@"添加好友"];
    [self reloadData];
}

- (void)reloadData {
    [self.dataArray removeAllObjects];
    NSArray *array = [XMPPTOOL getFriendRequestArray];
    for (NSDictionary *dict in array) {
        LBFriendRequestModel *model = [LBFriendRequestModel modelWithJSON:dict];
        [self.dataArray addObject:model];
    }
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count != 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LBAddFriendRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LBAddFriendRequestCell"];
    cell.friendRequestModel = self.dataArray[indexPath.row];
    cell.delegate = self;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    LBFriendRequestModel *friendRequestModel = self.dataArray[indexPath.row];
    
}
//同意
- (void)agreeButtonClick:(LBFriendRequestModel *)model {
    [XMPPTOOL.xmppRoster acceptPresenceSubscriptionRequestFrom:[XMPPTOOL getJIDWithUserId:model.uid] andAddToRoster:YES];
    model.is_accept = @"1";
//    [XMPPTOOL updateFirendRequestWithModel:model];
    [self reloadData];
    
    //调用接口建立好友关系
}
//拒绝
- (void)refuseButtonClick:(LBFriendRequestModel *)model {
    [XMPPTOOL.xmppRoster rejectPresenceSubscriptionRequestFrom:[XMPPTOOL getJIDWithUserId:model.uid]];
    model.is_accept = @"2";
//    [XMPPTOOL updateFirendRequestWithModel:model];
    [self reloadData];
}
- (void)rightBtnClick {
    RCDSearchFriendViewController *vc = [[RCDSearchFriendViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}


- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 60;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 60;
        [_tableView registerClass:[LBAddFriendRequestCell class] forCellReuseIdentifier:@"LBAddFriendRequestCell"];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (MTEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[MTEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, self.tableView.height)];
        _emptyView.imgView.image = IMAGENAMED(@"noData");
        _emptyView.tipsLbl.text = @"空空如也";
        [self.tableView addSubview:_emptyView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            
        }];
        [_emptyView addGestureRecognizer:tap];
    }
    return _emptyView;
}
@end
