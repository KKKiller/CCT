//
//  CCLiveController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/10.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^stopPushLiveBlk)(void);
@interface CCLiveController : BaseViewController
@property (nonatomic, strong) NSString *groupId;

@property (nonatomic, assign) BOOL isPusher; //是否是推流，默认为拉流
@property (nonatomic, strong) stopPushLiveBlk stopPushLiveBlk; 

- (void)play;
- (void)stop;
@end

NS_ASSUME_NONNULL_END
