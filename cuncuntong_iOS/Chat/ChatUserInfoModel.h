//
//  ChatUserInfoModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatUserInfoModel : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatar;


@end

NS_ASSUME_NONNULL_END
