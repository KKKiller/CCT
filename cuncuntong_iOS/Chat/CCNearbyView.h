//
//  CCNearbyView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/30.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCNearbyModel.h"
@interface CCNearbyView : UIView
@property (nonatomic, strong) UIImageView *headImgView;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *distanceLbl;
@property (nonatomic, strong) UILabel *typeLbl;

@property (nonatomic, strong) CCNearbyModel *model;
@end
