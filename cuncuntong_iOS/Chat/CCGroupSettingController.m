//
//  CCGroupSettingController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCGroupSettingController.h"
#import "DefaultPortraitView.h"
#import "RCDPrivateSettingsCell.h"
#import "RCDPrivateSettingsUserInfoCell.h"
#import "UIImageView+WebCache.h"
#import "RCDBaseSettingTableViewCell.h"
#import "UIColor+RCColor.h"
#import "RCDUserInfo.h"
#import "CCChatVillageController.h"
#import "RCDCreateGroupViewController.h"
#import "CCGroupInfoModel.h"
#import "RCDContactSelectedTableViewController.h"
#import "ComplaintsOneController.h"
static NSString *CellIdentifier = @"RCDBaseSettingTableViewCell";

@interface CCGroupSettingController ()<UIActionSheetDelegate>
//@property(strong, nonatomic) RCDUserInfo *userInfo;
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic, assign) BOOL edit;

@end

@implementation CCGroupSettingController{
    NSString *portraitUrl;
    NSString *nickname;
    BOOL enableNotification;
    RCConversation *currentConversation;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self startLoadView];
    self.groupId = self.groupInfo.groupId;
    if (self.edit) {
        [self loadGroupInfo];
    }
    
}
- (void)loadGroupInfo {
    if ([self.groupInfo.groupId containsString:@"group"]) {
        NSString *groupId = [self.groupInfo.groupId substringFromIndex:6];
        
        
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_info") params:@{@"gid":STR(groupId)} target:self success:^(NSDictionary *success) {
            if ([success[@"stat"] integerValue] == 1) {
                RCGroup *group = [[RCGroup alloc]init];
                group.groupId = groupId;
                group.groupName = success[@"info"][@"group_name"];
                group.portraitUri = success[@"info"][@"avatar"];
                self.groupInfo = group;
                 [self.tableView reloadData];
            }else{
                
            }
        } failure:^(NSError *failure) {
            
        }];
        
        
        
//        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/mygroup") params:@{@"uid":USERID} target:nil success:^(NSDictionary *success) {
//            if ([success[@"stat"] integerValue] == 1) {
//                NSArray *array = success[@"info"];
//                for (NSDictionary *dict in array) {
//                    CCGroupInfoModel *model = [CCGroupInfoModel modelWithJSON:dict];
//                    
//                    if ([groupId isEqualToString:model.id]) {
//                        RCGroup *group = [[RCGroup alloc]init];
//                        group.groupId = groupId;
//                        group.groupName = model.group_name;
//                        NSString *base = @"http://www.cct369.com/village/public/";
//                        if (![model.avatar containsString:@"cm.wanshitong.net"]) {
//                            model.avatar = [NSString stringWithFormat:@"%@%@",base,model.avatar];
//                        }
//                        group.portraitUri = model.avatar;
//                        self.groupInfo = group;
//                    }
//                }
//                [self.tableView reloadData];
//            }else{
//                SHOW(success[@"info"]);
//            }
//        } failure:^(NSError *failure) {
//            
//        }];
    }else if([self.groupInfo.groupId containsString:@"vill"]){
        
        RCGroup *group = [[RCGroup alloc]init];
        group.groupId = self.groupInfo.groupId;
        group.groupName = @"同村";
        self.groupInfo = group;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.groupId = self.groupInfo.groupId;
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 6, 87, 23);
    UIImageView *backImg = [[UIImageView alloc]
                            initWithImage:[UIImage imageNamed:@"navigator_btn_back"]];
    backImg.frame = CGRectMake(-6, 4, 10, 17);
    [backBtn addSubview:backImg];
    UILabel *backText =
    [[UILabel alloc] initWithFrame:CGRectMake(9,4, 85, 17)];
    backText.text = @"返回"; // NSLocalizedStringFromTable(@"Back",
    // @"RongCloudKit", nil);
    //   backText.font = [UIFont systemFontOfSize:17];
    [backText setBackgroundColor:[UIColor clearColor]];
    [backText setTextColor:[UIColor whiteColor]];
    [backBtn addSubview:backText];
    [backBtn addTarget:self
                action:@selector(leftBarButtonItemPressed:)
      forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton =
    [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    [self.navigationItem setLeftBarButtonItem:leftButton];
    
    UIButton *right = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    [right setTitle:@"举报" forState:UIControlStateNormal];
    [right addTarget:self action:@selector(jubao) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithCustomView:right];
    [self.navigationItem setRightBarButtonItem:rightBtn];
    
    
    
    self.title = @"设置";
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = HEXCOLOR(0xf0f0f6);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (void)leftBarButtonItemPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)jubao {
    //    [BRStringPickerView showStringPickerWithTitle:@"举报内容" dataSource:@[@"发布不适当内容对我造成骚扰",@"存在欺诈骗钱行为",@"该账号对我进行骚扰",@"存在侵权行为",@"发布仿冒品信息",@"该账号存在其他违规行为"] defaultSelValue:@"发布不适当内容对我造成骚扰" isAutoSelect:NO resultBlock:^(NSString *selectValue) {
    //        [TOOL showLoading:self.view];
    //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //            [TOOL hideLoading:self.view];
    //            SHOW(@"举报成功");
    //        });
    //    }];
    ComplaintsOneController *vc = [[ComplaintsOneController alloc] init];
    vc.aid = @"123";
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return section == 0 ? 1 : section == 1 ? 3 : 4;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section {
    if (section == 1 || section == 2) {
        return 20.f;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? 86 : 43;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //初始化
    static NSString *InfoCellIdentifier = @"RCDPrivateSettingsUserInfoCell";
    RCDPrivateSettingsUserInfoCell *infoCell =
    (RCDPrivateSettingsUserInfoCell *)[tableView
                                       dequeueReusableCellWithIdentifier:InfoCellIdentifier];
    if(!infoCell) {
        infoCell = [[RCDPrivateSettingsUserInfoCell alloc]init];
    }
    RCDBaseSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell) {
        cell = [[RCDBaseSettingTableViewCell alloc]init];
    }
    
    infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    //section == 0
    
    if (indexPath.section == 0) {
        RCDPrivateSettingsUserInfoCell *infoCell;
        if (self.groupInfo != nil) {
            portraitUrl = self.groupInfo.portraitUri;
            if (self.groupInfo.groupName.length > 0) {
                infoCell = [[RCDPrivateSettingsUserInfoCell alloc] initWithIsHaveDisplayName:YES];
                infoCell.NickNameLabel.text = self.groupInfo.groupName;
                infoCell.displayNameLabel.text = [NSString stringWithFormat:@"群组名: %@",self.groupInfo.groupName];
            } else {
                infoCell = [[RCDPrivateSettingsUserInfoCell alloc] initWithIsHaveDisplayName:NO];
                infoCell.NickNameLabel.text = self.groupInfo.groupName;
            }
        } else {
            infoCell = [[RCDPrivateSettingsUserInfoCell alloc] initWithIsHaveDisplayName:NO];
            infoCell.NickNameLabel.text = @"";
            portraitUrl = @"";
        }
        if ([portraitUrl isEqualToString:@""]) {
            DefaultPortraitView *defaultPortrait = [[DefaultPortraitView alloc]
                                                    initWithFrame:CGRectMake(0, 0, 100, 100)];
            [defaultPortrait setColorAndLabel:self.groupId Nickname:self.groupInfo.groupName];
            UIImage *portrait = [defaultPortrait imageFromView];
            infoCell.PortraitImageView.image = portrait;
        } else {
            [infoCell.PortraitImageView
             sd_setImageWithURL:[NSURL URLWithString:portraitUrl]
             placeholderImage:[UIImage imageNamed:@"group"]];
        }
        infoCell.PortraitImageView.layer.masksToBounds = YES;
        infoCell.PortraitImageView.layer.cornerRadius = 5.f;
        infoCell.PortraitImageView.contentMode = UIViewContentModeScaleAspectFill;
        infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return infoCell;
    }
    
    //section == 1
    
    if (indexPath.section == 1) {
        RCDBaseSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(!cell){
            cell = [[RCDBaseSettingTableViewCell alloc]init];
        }
        NSString *text = indexPath.row == 0 ? @"添加群成员" :  indexPath.row == 1 ? @"查看全部群成员" :   indexPath.row == 2 ? @"设置群组名称与头像" : @"查找聊天记录";
        cell.leftLabel.text = text;
        [cell setCellStyle:DefaultStyle];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    //section == 2
    
    
    if (indexPath.section == 2) {
        switch (indexPath.row) {
            case 0: {
                [cell setCellStyle:SwitchStyle];
                cell.leftLabel.text = @"消息免打扰";
                cell.switchButton.hidden = NO;
                cell.switchButton.on = !enableNotification;
                [cell.switchButton removeTarget:self
                                         action:@selector(clickIsTopBtn:)
                               forControlEvents:UIControlEventValueChanged];
                
                [cell.switchButton addTarget:self
                                      action:@selector(clickNotificationBtn:)
                            forControlEvents:UIControlEventValueChanged];
                
            } break;
                
            case 1: {
                [cell setCellStyle:SwitchStyle];
                cell.leftLabel.text = @"会话置顶";
                cell.switchButton.hidden = NO;
                cell.switchButton.on = currentConversation.isTop;
                [cell.switchButton addTarget:self
                                      action:@selector(clickIsTopBtn:)
                            forControlEvents:UIControlEventValueChanged];
            } break;
                
            case 2: {
                [cell setCellStyle:SwitchStyle];
                cell.leftLabel.text = @"清除聊天记录";
                cell.switchButton.hidden = YES;
            } break;
             
            case 3: {
                [cell setCellStyle:SwitchStyle];
                cell.leftLabel.text = @"退出该群";
                cell.switchButton.hidden = YES;
            } break;
            default:
                break;
        }
        
        return cell;
    }
    return nil;
    
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {//添加好友
            if ([self.groupId containsString:@"vill"]) {
                SHOW(@"同村群不能拉人");
            }else if([self.groupId containsString:@"group_"]){
                NSString *group = [self.groupId substringFromIndex:6];
                RCDContactSelectedTableViewController *vc = [[RCDContactSelectedTableViewController alloc]init];
                vc.groupName = self.groupInfo.groupName;
                vc.groupId = group;
                
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        else if (indexPath.row == 1) {//全部群成员
            CCChatVillageController *vc = [[CCChatVillageController alloc]init];
            vc.notOnTab = YES;
            vc.groupId = self.groupInfo.groupId;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2){//设置群
            RCDCreateGroupViewController *vc = [[RCDCreateGroupViewController alloc]init];
            self.edit = YES;
            vc.isEdit = YES;
            vc.group_id = self.groupInfo.groupId;
            [self.navigationController pushViewController:vc animated:YES];
        }else if(indexPath.row == 3){//搜索聊天记录
            
        }
        //搜索历史
        //    RCDSearchHistoryMessageController *searchViewController = [[RCDSearchHistoryMessageController alloc] init];
        //    searchViewController.conversationType = ConversationType_PRIVATE;
        //    searchViewController.targetId = self.userId;
        //    [self.navigationController pushViewController:searchViewController animated:YES];
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 2) {
            UIActionSheet *actionSheet =
            [[UIActionSheet alloc] initWithTitle:@"确定清除聊天记录？"
                                        delegate:self
                               cancelButtonTitle:@"取消"
                          destructiveButtonTitle:@"确定"
                               otherButtonTitles:nil];
            
            [actionSheet showInView:self.view];
            actionSheet.tag = 100;
        }else if (indexPath.row == 3){
            NSString *group = @"";
            if ([self.groupInfo.groupId containsString:@"vill_"]) {
                group = [self.groupInfo.groupId substringFromIndex:5];
            }else if([self.groupInfo.groupId containsString:@"group_"]){
                group = [self.groupInfo.groupId substringFromIndex:6];
            }
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_member_rm") params:@{@"gid":STR(group),@"uid":USERID} target:self success:^(NSDictionary *success) {
                if ([success[@"stat"] integerValue] == 1) {
                    SHOW(@"退出成功");
                    for (UIViewController *vc in self.navigationController.viewControllers) {
                        if ([vc isKindOfClass:[CCChatTabbarController class]]) {
                            [self.navigationController popToViewController:vc animated:YES];
                        }
                    }
                }else{
                    SHOW(success[@"info"]);
                }
            } failure:^(NSError *failure) {
                
            }];
        }
    }
}

#pragma mark -UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 100) {
        if (buttonIndex == 0) {
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            RCDPrivateSettingsCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            UIActivityIndicatorView *activityIndicatorView =
            [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            float cellWidth = cell.bounds.size.width;
            UIView *loadingView = [[UIView alloc]initWithFrame:CGRectMake(cellWidth - 50, 15, 40, 40)];
            [loadingView addSubview:activityIndicatorView];
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView startAnimating];
                [cell addSubview:loadingView];
            });
            
            
            
            [[RCIMClient sharedRCIMClient]deleteMessages:ConversationType_GROUP targetId:self.groupId success:^{
                [self performSelectorOnMainThread:@selector(clearCacheAlertMessage:)
                                       withObject:@"清除聊天记录成功！"
                                    waitUntilDone:YES];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"ClearHistoryMsg" object:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [loadingView removeFromSuperview];
                });
                
            } error:^(RCErrorCode status) {
                [self performSelectorOnMainThread:@selector(clearCacheAlertMessage:)
                                       withObject:@"清除聊天记录失败！"
                                    waitUntilDone:YES];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [loadingView removeFromSuperview];
                });
            }];
            
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"ClearHistoryMsg"
             object:nil];
        }
    }
}

- (void)clearCacheAlertMessage:(NSString *)msg {
    UIAlertView *alertView =
    [[UIAlertView alloc] initWithTitle:nil
                               message:msg
                              delegate:nil
                     cancelButtonTitle:@"确定"
                     otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - 本类的私有方法
- (void)startLoadView {
    currentConversation =
    [[RCIMClient sharedRCIMClient] getConversation:ConversationType_GROUP
                                          targetId:self.groupId];
    [[RCIMClient sharedRCIMClient]
     getConversationNotificationStatus:ConversationType_PRIVATE
     targetId:self.groupId
     success:^(RCConversationNotificationStatus nStatus) {
         enableNotification = NO;
         if (nStatus == NOTIFY) {
             enableNotification = YES;
         }
         dispatch_async(dispatch_get_main_queue(), ^{
             
             [self.tableView reloadData];
         });
     }
     error:^(RCErrorCode status){
         
     }];
    
//    [self loadUserInfo:self.groupId];
}

- (void)loadUserInfo:(NSString *)userId {
    //用户信息
//    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/tsinfo") params:@{@"ts_uid":STR(userId)} target:nil success:^(NSDictionary *success) {
//        if ([success[@"data"][@"stat"] integerValue] == 1) {
//            RCDUserInfo *model = [[RCDUserInfo alloc]init];
//            model.name = success[@"data"][@"info"][@"realname"];
//            model.portraitUri = success[@"data"][@"info"][@"portrait"];
//            self.userInfo = model;
//            [self.tableView reloadData];
//        }
//    } failure:^(NSError *failure) {
//        
//    }];
}
//消息免打扰
- (void)clickNotificationBtn:(id)sender {
    UISwitch *swch = sender;
//    [[RCIMClient sharedRCIMClient]
//     setConversationNotificationStatus:ConversationType_PRIVATE
//     targetId:self.userId
//     isBlocked:swch.on
//     success:^(RCConversationNotificationStatus nStatus) {
//         SHOW(@"设置成功");
//     }
//     error:^(RCErrorCode status){
//         
//     }];
    [[RCIMClient sharedRCIMClient]
     setConversationNotificationStatus:ConversationType_GROUP
     targetId:self.groupId
     isBlocked:swch.on
     success:^(RCConversationNotificationStatus nStatus) {
         NSLog(@"成功");
         
     }
     error:^(RCErrorCode status) {
         NSLog(@"失败");
     }];
}
//置顶
- (void)clickIsTopBtn:(id)sender {
    UISwitch *swch = sender;
//    [[RCIMClient sharedRCIMClient] setConversationToTop:ConversationType_PRIVATE
//                                               targetId:self.userId
//                                                  isTop:swch.on];
    [[RCIMClient sharedRCIMClient] setConversationToTop:ConversationType_GROUP
                                               targetId:self.groupId
                                                  isTop:swch.on];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        SHOW(@"设置成功");
    });
}


@end
