//
//  CCAddFriendView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/30.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAddFriendView : UIView
@property (weak, nonatomic) IBOutlet UITextField *beizhuField;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIView *containerV;
@property (weak, nonatomic) IBOutlet UILabel *beizhuLbl;
+ (instancetype)instanceView;
@end
