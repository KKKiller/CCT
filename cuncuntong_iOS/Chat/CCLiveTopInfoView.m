//
//  CCLiveTopInfoView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/10.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCLiveTopInfoView.h"

@implementation CCLiveTopInfoView

+ (instancetype)instanceView {
    CCLiveTopInfoView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}

@end
