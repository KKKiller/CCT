//
//  LBFirendRequestModel.m
//  L8
//
//  Created by 周吾昆 on 2018/6/16.
//  Copyright © 2018年 周吾昆. All rights reserved.
//

#import "LBFriendRequestModel.h"

@implementation LBFriendRequestModel
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_uid forKey:@"uid"];
    [aCoder encodeObject:_nickname forKey:@"nickname"];
    [aCoder encodeObject:_avatar forKey:@"avatar"];
    [aCoder encodeObject:_is_accept forKey:@"is_accept"];
    [aCoder encodeObject:_create_date forKey:@"create_date"];

}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        _uid = [aDecoder decodeObjectForKey:@"uid"];
        _nickname = [aDecoder decodeObjectForKey:@"nickname"];
        _avatar = [aDecoder decodeObjectForKey:@"avatar"];
        _is_accept = [aDecoder decodeObjectForKey:@"is_accept"];
        _create_date = [aDecoder decodeObjectForKey:@"create_date"];

    }
    return self;
}

- (NSMutableDictionary *)changeModelToDic{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:self.uid forKey:@"uid"];
    [dic setObject:self.nickname forKey:@"nickname"];
    [dic setObject:self.avatar forKey:@"avatar"];
    [dic setObject:self.is_accept forKey:@"is_accept"];
    [dic setObject:self.create_date forKey:@"create_date"];
    return dic;
}
@end
