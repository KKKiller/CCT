//
//  CCChatAddMsgController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCChatAddMsgController.h"
#import "CunCunTong-Swift.h"
#import <IQKeyboardManager.h>
#import "CCAddFriendView.h"
@interface CCChatAddMsgController ()
@property (strong, nonatomic) PathDynamicModal *animateModel;

@property (nonatomic, strong) UIImageView *headImgView;
@property (nonatomic, strong) UIImageView *msgBack;
@property (nonatomic, strong) UILabel *msgLbl;
@property (nonatomic, strong) UIButton *okBtn;
@property (nonatomic, strong) UIButton *cancleBtn;
@end

@implementation CCChatAddMsgController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"好友请求"];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor = BACKGRAY;
    [self setUI];
//    self.addMsg = [TOOL stringEmpty:self.addMsg] ? @"请求添加您为好友" : self.addMsg;
//    self.msgLbl.text = self.addMsg;
    [self loadAvatar];
}
- (void)loadAvatar {
    if ([self.groupId containsString:@"group"]) {
        [self getGroupInfo];
        return;
    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_info") params:@{@"f_uid":STR(self.uid),@"uid":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
            [self.headImgView setImageWithURL:URL(success[@"info"][@"portrait"]) placeholder:IMAGENAMED(@"head")];
            NSString *name = success[@"info"][@"realname"];
            [self addTitle:[NSString stringWithFormat:@"来自%@的好友请求",name]];
//            self.msgLbl.text = [NSString stringWithFormat:@"%@请求添加您为好友",name];
        }
    } failure:^(NSError *failure) {
        NSLog(@"%@",failure);
    }];
}

- (void)okBtnClick {
    [self addBtnClick];
//    if([self.groupId containsString:@"group"]){
//        [self agreeGroupAddInvitation];
//        return;
//    }
//    CCAddFriendView *view = [CCAddFriendView instanceView];
//    view.layer.cornerRadius = 4;
//    view.layer.masksToBounds = YES;
//    view.addBtn.layer.cornerRadius = 4;
//    view.addBtn.layer.masksToBounds = YES;
//    [view.addBtn addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    view.containerV.layer.borderWidth = 0.5;
//    view.containerV.layer.borderColor = MTRGB(0x21c2d5).CGColor;
//    view.frame = CGRectMake(0, 0, App_Width - 40, 170);
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
//        [self.view endEditing:YES];
//    }];
//    view.userInteractionEnabled = YES;
//    [view addGestureRecognizer:tap];
//
//    self.animateModel = [[PathDynamicModal alloc]init];
//    self.animateModel.closeBySwipeBackground = YES;
//    self.animateModel.closeByTapBackground = YES;
//    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}
- (void)cancleBtnClick {
    if ([self.groupId containsString:@"group"]) {
        [self refuseGroup];
        return;
    }
    NSDictionary *param = @{@"f_name":STR(self.name),@"f_uid":STR(self.uid),@"uid":USERID,@"flag":@"2"};
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_doadd") params:param target:self success:^(NSDictionary *success) {
        [XMPPTOOL removeFirendRequestWithUid:self.uid];
        if ([success[@"stat"] integerValue] == 1) {
            SHOW(@"拒绝好友成功");
            if (self.finishBlk) {
                self.finishBlk(NO);
            }
            [XMPPTOOL.xmppRoster rejectPresenceSubscriptionRequestFrom:[XMPPTOOL getJIDWithUserId:self.uid]];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            SHOW(success[@"info"]);
        }
    } failure:^(NSError *failure) {
    }];
}
//添加好友
- (void)addBtnClick {
    if ([self.groupId containsString:@"group"]) {
        [self agreeGroupAddInvitation];
        return;
    }
    NSDictionary *param = @{@"f_name":STR(self.name),@"f_uid":STR(self.uid),@"uid":USERID,@"flag":@"1"};
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_doadd") params:param target:self success:^(NSDictionary *success) {
        [XMPPTOOL removeFirendRequestWithUid:self.uid];
        if ([success[@"stat"] integerValue] == 1) {
            SHOW(@"添加好友成功");
            if (self.finishBlk) {
                self.finishBlk(YES);
            }
            [XMPPTOOL.xmppRoster acceptPresenceSubscriptionRequestFrom:[XMPPTOOL getJIDWithUserId:self.uid] andAddToRoster:YES];
            [NOTICENTER postNotificationName:kFriendChangeNotification object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            SHOW(success[@"info"]);
        }
        
    } failure:^(NSError *failure) {
    }];
}


- (void)agreeGroupAddInvitation {
    if (self.groupId.length < 6) {
        return;
    }
    NSDictionary *dict = @{@"gid":[self.groupId substringFromIndex:6],@"uids[0]":USERID};
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_members_add") params:dict  target:self success:^(NSDictionary *success) {
            if ([success[@"stat"] integerValue] == 1) {
                SHOW(@"加入成功");
//                NSString *groupId = [NSString stringWithFormat:@"group_%@",self.uid];
                    dispatch_async_on_main_queue(^{
                        for (UIViewController *vc in self.navigationController.viewControllers) {
                            if ([vc isKindOfClass:[CCChatTabbarController class]]) {
                                [self.navigationController popToViewController:vc animated:YES];
                            }
                        }
                    });
                
            }else{
                SHOW(success[@"info"]);
            }
        } failure:^(NSError *failure) {
            
        }];
}
- (void)getGroupInfo {
    NSString *groupId = [self.groupId substringFromIndex:6];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/group_info") params:@{@"gid":STR(groupId)} target:nil success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
            [self.headImgView setImageWithURL:URL(success[@"info"][@"avatar"]) placeholder:IMAGENAMED(@"head")];
            NSString *name = success[@"info"][@"group_name"];
            [self addTitle:[NSString stringWithFormat:@"来自%@的群聊邀请",name]];
            
        }else{
            
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)refuseGroup {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setUI {
    self.headImgView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 20 , 40, 40)];
    self.headImgView.image = IMAGENAMED(@"head");
    self.headImgView.layer.cornerRadius = 20;
    self.headImgView.layer.masksToBounds = YES;
    [self.view addSubview:self.headImgView];
    
    
    self.msgBack = [[UIImageView alloc]init];
    [self.view addSubview:self.msgBack];
    self.msgBack.image = IMAGENAMED(@"dialog_box_left");
    
    self.msgLbl = [[UILabel alloc]initWithText:[NSString stringWithFormat:@"%@请求添加您为好友",self.name] font:14 textColor:TEXTBLACK6];
    [self.view addSubview:self.msgLbl];
    
    self.cancleBtn = [[UIButton alloc]initWithTitle:@"拒绝" textColor:WHITECOLOR backImg:nil font:14];
    [self.cancleBtn setBackgroundColor:MTRGB(0x9b9b9b)];
    [self.cancleBtn addTarget:self action:@selector(cancleBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.cancleBtn];
    self.cancleBtn.layer.cornerRadius = 3;
    self.cancleBtn.layer.masksToBounds = YES;
    
    self.okBtn = [[UIButton alloc]initWithTitle:@"同意" textColor:WHITECOLOR backImg:nil font:14];
    [self.okBtn setBackgroundColor:MTRGB(0x21C2D5)];
    [self.okBtn addTarget:self action:@selector(okBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.okBtn];
    self.okBtn.layer.cornerRadius = 3;
    self.okBtn.layer.masksToBounds = YES;
    
    [self layoutSubview];
}
- (void)layoutSubview {
    [self.msgLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headImgView.mas_right).offset(25);
        make.right.equalTo(self.view.mas_right).offset(-45);
        make.top.equalTo(self.headImgView.mas_top).offset(10);
    }];
    [self.msgBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.msgLbl).offset(-15);
        make.top.equalTo(self.msgLbl).offset(-10);
        make.bottom.right.equalTo(self.msgLbl).offset(10);
    }];
    [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.msgBack.mas_left).offset(10);
        make.top.equalTo(self.msgBack.mas_bottom).offset(10);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(40);
    }];
    [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.msgBack.mas_right).offset(-10);
        make.top.equalTo(self.cancleBtn);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(40);
    }];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

@end
