//
//  CCNearbyModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/30.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCNearbyModel : NSObject
@property (nonatomic, strong) NSString *rid;
@property (nonatomic, strong) NSString *portrait;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *relationship; //1同村 2同乡 3同市
@property (nonatomic, strong) NSString *num;
@end
