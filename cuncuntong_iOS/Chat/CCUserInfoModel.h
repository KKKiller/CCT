//
//  CCUserInfoModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCUserInfoModel : NSObject
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *portrait;
@property (nonatomic, strong) NSString *addr;
@property (nonatomic, assign) int distance;
@end
