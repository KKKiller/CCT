//
//  CCLiveAuthorityApplyChildController.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/16.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import "CCLiveAuthorityApplyChildController.h"
#import "zhPopupController.h"

@interface CCLiveAuthorityApplyChildController ()
@property (strong, nonatomic) IBOutlet UIView *popView;
@property (weak, nonatomic) IBOutlet UILabel *lab;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIView *btnBgView;

@end

@implementation CCLiveAuthorityApplyChildController

- (void)viewDidLoad {
    [super viewDidLoad];
    _cancelBtn.layer.borderColor = MTRGB(0xC8C8C8).CGColor;
    _cancelBtn.layer.borderWidth = 1;
    _popView.frame = CGRectMake(20, 0, App_Width - 40, 170);
    _popView.layer.cornerRadius = 5;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (IBAction)dismissAct:(UIButton *)sender {
    [self.zh_popupController dismiss];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelAct:(UIButton *)sender {
    [self.zh_popupController dismiss];
}
- (IBAction)confirmAct:(UIButton *)sender {
    _btnBgView.hidden = YES;
    _confirmBtn.hidden = NO;
    _lab.text = @"您已申请直播关闭，请耐心等待处理...";
    
}

/**
 关闭直播

 @param sender Btn
 */
- (IBAction)closeLive:(UIButton *)sender {
    self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
    self.zh_popupController.layoutType = zhPopupLayoutTypeCenter;
    self.zh_popupController.allowPan = YES;
    
    [self.zh_popupController presentContentView:self.popView];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
