//
//  CCChatNearbyController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/30.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCChatNearbyController.h"
#import "CCNearbyView.h"
#import "CCNearbyModel.h"
#import "CCChatAddFriendController.h"
#import <CoreLocation/CoreLocation.h>
@interface CCChatNearbyController ()<CLLocationManagerDelegate>
{
    CLLocationManager *locationmanager;//定位服务
    NSString *currentCity;//当前城市
    NSString *logi;
    NSString *lati;
}
@property (nonatomic, strong) UIImageView *topImgView;
@property (nonatomic, strong) UILabel *textLbl;
@property (nonatomic, strong) UIButton *refreshBtn;
@property (nonatomic, strong) UILabel *lineLbl;
@property (nonatomic, strong) UIView *nearbyContainerV;

@property (nonatomic, strong) UIImageView *head1;
@property (nonatomic, strong) UIImageView *head2;
@property (nonatomic, strong) UIImageView *head3;
@property (nonatomic, strong) UILabel *distance1;
@property (nonatomic, strong) UILabel *distance2;
@property (nonatomic, strong) UILabel *distance3;

@property (nonatomic, strong) CCNearbyModel *near1;
@property (nonatomic, strong) CCNearbyModel *near2;
@property (nonatomic, strong) CCNearbyModel *near3;

@property (nonatomic, strong) NSMutableArray *userModelArray;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL isGetMore;
@end

@implementation CCChatNearbyController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.userModelArray = [NSMutableArray array];
    [self setUI];
    [self addUserView];
    [self setTopHead];
    [self getLocation];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.navigationItem.title = @"同乡";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
- (void)refreshUsers {
    if (logi) {
        if (self.userModelArray.count == 8) {
            self.isGetMore = YES;
            [self loadDataWithLati:logi logi:lati];
            
        }else{
            SHOW(@"没有更多了");
        }
    }else{
        SHOW(@"定位失败,请在设置中打开定位权限");
    }
}
- (void)loadDataWithLati:(NSString *)latitude logi:(NSString *)logitude {
    
    self.page++;
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/near") params:@{@"uid":USERID,@"lat":STR(latitude),@"lng":STR(logitude),@"page":@(self.page)} target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
            NSArray *users = success[@"info"][@"users"];
            NSDictionary *nears = success[@"info"][@"counts"];
            //附近的人圈圈
            if ([nears isKindOfClass:[NSDictionary class]]) {
                self.near1 = [CCNearbyModel modelWithJSON:nears[@"d100"]];
                self.near2 = [CCNearbyModel modelWithJSON:nears[@"d200"]];
                self.near3 = [CCNearbyModel modelWithJSON:nears[@"d300"]];
            }
            //附近的人下部分
            [self.userModelArray removeAllObjects];
            for (NSDictionary *dict in users) {
                CCNearbyModel *model = [CCNearbyModel modelWithJSON:dict];
                [self.userModelArray addObject:model];
            }
            if (self.userModelArray.count < 8) {
                self.page = 0;
                if (self.isGetMore) {
                    SHOW(@"没有更多了");
                }
            }
            [self setUsersData:self.userModelArray];
            
        }else{
            SHOW(success[@"info"]);
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)setUsersData:(NSArray *)array {
    for (CCNearbyView *view  in self.nearbyContainerV.subviews) {
        if ([view isKindOfClass:[CCNearbyView class]]) {
            view.hidden = YES;
        }
    }
    for (int i = 0; i<array.count; i++) {
        for (CCNearbyView *view in self.nearbyContainerV.subviews) {
//            view.hidden = YES;
            if (view.tag - 1000 == i) {
                view.model = array[i];
                view.hidden = NO;
            }
        }
    }
    self.head1.hidden = !self.near1;
    self.head2.hidden = !self.near2;
    self.head3.hidden = !self.near3;

    [self.head1 setImageWithURL:URL(self.near1.portrait) placeholder:IMAGENAMED(@"head")];
    [self.head2 setImageWithURL:URL(self.near2.portrait) placeholder:IMAGENAMED(@"head")];
    [self.head3 setImageWithURL:URL(self.near3.portrait) placeholder:IMAGENAMED(@"head")];
}
//底部附近的人
- (void)addUserView {
    CGFloat height = 120;
    CGFloat width = (App_Width - 10) / 4;
    for (int i =0; i<8; i++) {
        NSInteger row = i % 4;
        NSInteger col = i / 4;
        CCNearbyView *view = [[CCNearbyView alloc]initWithFrame:CGRectMake(width * row, height * col, width, height)];
        view.hidden = YES;
        [self.nearbyContainerV addSubview:view];
        view.tag = 1000 + i;
        view.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            if (self.userModelArray.count > i) {
                CCNearbyModel *model = self.userModelArray[i];
                [self tapAtUser:model.rid];
            }
        }];
        [view addGestureRecognizer:tap];
    }
}

- (void)tapAtUser:(NSString *)uid {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_mine") params:@{@"uid":USERID,@"f_uid":STR(uid)} target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 3) { //是我的好友
            if (![uid isEqualToString:USERID]) {
            CCChatController *vc = [[CCChatController alloc]initWithConversationType:ConversationType_PRIVATE targetId:uid];
            [self.navigationController pushViewController:vc animated:YES];
            }
        }else{
            CCChatAddFriendController *vc = [[CCChatAddFriendController alloc]init];
            vc.uid = uid;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSError *failure) {
    }];
}

- (void)setUI {
    self.topImgView = [[UIImageView alloc]init];
    self.topImgView.image = IMAGENAMED(@"nearby_topBack");
    self.topImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.topImgView.layer.masksToBounds = YES;
    [self.view addSubview:self.topImgView];
    self.topImgView.userInteractionEnabled = YES;
    
    self.textLbl = [[UILabel alloc]initWithText:@"你可能认识" font:14 textColor:TEXTBLACK6];
    [self.view addSubview:self.textLbl];
    
    self.refreshBtn = [[UIButton alloc]initWithTitle:@"换一批" textColor:MAINCOLOR backImg:nil font:14];
    [self.view addSubview:self.refreshBtn];
    [self.refreshBtn addTarget:self action:@selector(refreshUsers) forControlEvents:UIControlEventTouchUpInside];
    
    self.lineLbl = [[UILabel alloc]initWithShallowLine];
    [self.view addSubview:self.lineLbl];
    
    self.nearbyContainerV = [[UIView alloc]init];
    [self.view addSubview:self.nearbyContainerV];
    
    [self layoutSubview];
}
- (void)layoutSubview {
    [self.topImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(64+20);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(250);
    }];
    [self.textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.top.equalTo(self.topImgView.mas_bottom).offset(-30);
    }];
    [self.refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(40);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.centerY.equalTo(self.textLbl.mas_centerY);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.top.equalTo(self.textLbl.mas_bottom).offset(10);
        make.height.mas_equalTo(0.5);
    }];
    [self.nearbyContainerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.lineLbl.mas_bottom).offset(10);
    }];
}

- (void)setTopHead {
    self.head1 = [[UIImageView alloc]init];
    [self.topImgView addSubview:self.head1];
    self.head1.layer.cornerRadius = 25;
    self.head1.layer.masksToBounds = YES;
    self.head1.image =IMAGENAMED(@"head");
    self.head1.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self tapAtUser:self.near1.rid];
    }];
    [self.head1 addGestureRecognizer:tap1];
    
    self.head2 = [[UIImageView alloc]init];
    [self.topImgView addSubview:self.head2];
    self.head2.layer.cornerRadius = 20;
    self.head2.layer.masksToBounds = YES;
    self.head2.image =IMAGENAMED(@"head");
    self.head2.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self tapAtUser:self.near2.rid];
    }];
    [self.head2 addGestureRecognizer:tap2];
    
    self.head3 = [[UIImageView alloc]init];
    self.head3.layer.cornerRadius = 15;
    self.head3.layer.masksToBounds = YES;
    [self.topImgView addSubview:self.head3];
    self.head3.image =IMAGENAMED(@"head");
    self.head3.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self tapAtUser:self.near3.rid];
    }];
    [self.head3 addGestureRecognizer:tap3];
    
    [self.head1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(50);
        make.top.equalTo(self.topImgView.mas_top).offset(100);
        make.centerX.equalTo(self.topImgView.mas_centerX).offset(-65);
    }];
    [self.head2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.top.equalTo(self.topImgView.mas_top).offset(30);
        make.centerX.equalTo(self.topImgView.mas_centerX).offset(50);
    }];
    [self.head3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(30);
        make.top.equalTo(self.topImgView.mas_top).offset(120);
        make.centerX.equalTo(self.topImgView.mas_centerX).offset(100);
    }];
}



-(void)getLocation
{
    //判断定位功能是否打开
    if ([CLLocationManager locationServicesEnabled]) {
        locationmanager = [[CLLocationManager alloc]init];
        locationmanager.delegate = self;
        [locationmanager requestAlwaysAuthorization];
        currentCity = [NSString new];
        [locationmanager requestWhenInUseAuthorization];
        
        //设置寻址精度
        locationmanager.desiredAccuracy = kCLLocationAccuracyBest;
        locationmanager.distanceFilter = 5.0;
        [locationmanager startUpdatingLocation];
    }
}
#pragma mark CoreLocation delegate (定位失败)
//定位失败后调用此代理方法
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //设置提示提醒用户打开定位服务
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"允许定位提示" message:@"请在设置中打开定位" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"打开定位" style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark 定位成功后则执行此代理方法
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [locationmanager stopUpdatingHeading];
    //旧址
    CLLocation *currentLocation = [locations lastObject];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    //打印当前的经度与纬度
    if (!logi) {
        NSLog(@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
        logi = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
        lati = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
        [self loadDataWithLati:lati logi:logi];
        [self updateLocation];
    }
    //反地理编码
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (placemarks.count > 0) {
            CLPlacemark *placeMark = placemarks[0];
            currentCity = placeMark.locality;
            if (!currentCity) {
                currentCity = @"无法定位当前城市";
            }
            
            /*看需求定义一个全局变量来接收赋值*/
            NSLog(@"----%@",placeMark.country);//当前国家
            NSLog(@"%@",currentCity);//当前的城市
            //            NSLog(@"%@",placeMark.subLocality);//当前的位置
            //            NSLog(@"%@",placeMark.thoroughfare);//当前街道
            //            NSLog(@"%@",placeMark.name);//具体地址
            
        }
    }];
    
}
- (void)updateLocation {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/update_pos") params:@{@"lat":STR(lati),@"lng":STR(logi),@"uid":USERID} target:nil success:^(NSDictionary *success) {
    } failure:^(NSError *failure) {
        
    }];
}
@end
