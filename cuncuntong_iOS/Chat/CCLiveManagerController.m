//
//  CCLiveManagerController.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/25.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import "CCLiveManagerController.h"
#import "CCLiveMangerCell.h"
#import "CCLiveManagePopView.h"
#import "zhPopupController.h"
#import "CCLiveManagerPopTView.h"
#import "CCLiveTimeSetView.h"
#import "BRPickerView.h"
#import "MultiSelectViewController.h"
#import "CCLiveDealWaintingController.h"
//#import "MultiSelectItem.h"

@interface CCLiveManagerController ()<UITableViewDelegate, UITableViewDataSource>
@property(nonatomic, assign) BOOL istrue;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) CCLiveManagePopView *popview;
@property (nonatomic, strong) CCLiveManagerPopTView *popTview;
@property (nonatomic, strong) CCLiveTimeSetView *timeSetView;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic, assign) NSInteger num;
@property (weak, nonatomic) IBOutlet UIView *footerView;

@end

@implementation CCLiveManagerController

/**
 缴纳押金

 @param sender Btn
 */
- (IBAction)jiaoNaYaJin:(UIButton *)sender {
    NSLog(@"缴纳押金");
}

/**
 取消直播

 @param sender Btn
 */
- (IBAction)cancelLive:(UIButton *)sender {
    NSLog(@"取消直播");
}

/**
 邀请他人直播

 @param sender Btn
 */
- (IBAction)selectAct:(UIButton *)sender {
    
    self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
    self.zh_popupController.layoutType = zhPopupLayoutTypeBottom;
    self.zh_popupController.allowPan = NO;
    [self.zh_popupController presentContentView:self.popview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = 128;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"new_application_normal"] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemClick)];
    _bottomView.frame = CGRectMake(0, SCREEN_HEIGHT - WCFNavigationHeight - 144, App_Width, 144);
    _bottomView.backgroundColor = [UIColor redColor];
    [self.tableView addSubview:_bottomView];
    
    WEAKSELF
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.bottomView.frame = CGRectMake(0, 81 + 20, App_Width, 144);
        [weakSelf.footerView addSubview:weakSelf.bottomView];
        weakSelf.num = 100;
        [weakSelf.tableView reloadData];
    });
    

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.bottomView.frame = CGRectMake(0, SCREEN_HEIGHT - WCFNavigationHeight - 144, App_Width, 144);
        _bottomView.backgroundColor = [UIColor redColor];
        [weakSelf.tableView addSubview:_bottomView];
        weakSelf.num = 200;
        [weakSelf.tableView reloadData];
    });
    // Do any additional setup after loading the view.
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;

}

//-(void)bottomAddToTableViewOrAsFotterView{
////    if ((128 * 2 + 144) < (SCREEN_HEIGHT - WCFNavigationHeight)){
////        
////    }
//}

- (void)rightBarButtonItemClick{
    CCLiveDealWaintingController *Vc = [[UIStoryboard storyboardWithName:@"Live" bundle:nil] instantiateViewControllerWithIdentifier:@"CCLiveDealWaintingController"];
    [self.navigationController pushViewController:Vc animated:YES];
    /*
    if (!_istrue){
        self.navigationItem.rightBarButtonItem.image = [UIImage imageNamed:@"new_application_normal"];
        _istrue = YES;
    }else {
       self.navigationItem.rightBarButtonItem.image = [[UIImage imageNamed:@"new_application"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        _istrue = NO;
    }
     */
    
}






#pragma mark UITableViewDelegate UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCLiveMangerCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CCLiveMangerCell class]) forIndexPath:indexPath];
    cell.nameLabe.text = @"海阔天空";
    cell.degreeLab.text = @"B级";
    cell.startDateLab.text = @"2018-02-19";
    cell.startTimelab.text = @"12:30";
    cell.btn.tag = 10000 + indexPath.row;
    [cell.btn addTarget:self action:@selector(mangerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return _footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_num == 100){
        return 81 + 20 + 144;
    }
    return 81;
}

- (void)mangerBtnClick:(UIButton *)sender{
    self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
    self.zh_popupController.layoutType = zhPopupLayoutTypeBottom;
    self.zh_popupController.allowPan = NO;
    [self.zh_popupController presentContentView:self.popTview];
}

#pragma mark 懒加载
- (CCLiveManagePopView *)popview {
    if (!_popview) {
        _popview = [CCLiveManagePopView instanceView];
        _popview.frame = CGRectMake(0, 0, App_Width, 153);
        [_popview.liveBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_popview.videoBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_popview.cancelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _popview;
}

- (CCLiveManagerPopTView *)popTview {
    if (!_popTview) {
        _popTview = [CCLiveManagerPopTView instanceView];
        _popTview.frame = CGRectMake(0, 0, App_Width, 100);
        [_popTview.modifyBtn addTarget:self action:@selector(tbtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_popTview.deleteBtn addTarget:self action:@selector(tbtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _popTview;
}

- (CCLiveTimeSetView *)timeSetView {
    if (!_timeSetView) {
        _timeSetView = [CCLiveTimeSetView instanceView];
        _timeSetView.frame = CGRectMake(12, 0, App_Width - 24, 300);
        _timeSetView.layer.cornerRadius = 8;
        _timeSetView.layer.masksToBounds = YES;
        
        [_timeSetView.btn addTarget:self action:@selector(confirmTimeModifyClick:) forControlEvents:UIControlEventTouchUpInside];
        for (UITapGestureRecognizer *tap in _timeSetView.gesColl) {
            [tap addTarget:self action:@selector(tagGesAct:)];
        }
        
    }
    return _timeSetView;
}

- (void)btnClick:(UIButton *)sender{
    [self.zh_popupController dismiss];
    if (sender.tag == 8000){
        MultiSelectViewController *Vc = [MultiSelectViewController new];
        MultiSelectItem *item0 = [[MultiSelectItem alloc]init];
        item0.name = @"中国";
        MultiSelectItem *item1 = [[MultiSelectItem alloc]init];
        item1.name = @"人迷";
        
        Vc.items = @[item0, item1];
        [self.navigationController pushViewController:Vc animated:YES];
        
    }else if (sender.tag == 8001){
        
    }
    
}
- (void)tbtnClick:(UIButton *)sender{
    [self.zh_popupController dismiss];
    switch (sender.tag) {
        case 9000:
            self.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
            self.zh_popupController.layoutType = zhPopupLayoutTypeCenter;
            self.zh_popupController.allowPan = YES;
            [self.zh_popupController presentContentView:self.timeSetView];
            break;
        case 9001:
            SHOW(@"删除");
            
        default:
            break;
    }
    
}

- (void)tagGesAct:(UITapGestureRecognizer *)sender{
    WEAKSELF
    if ([self.timeSetView.gesColl indexOfObject:sender] <= 2){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *minDateStr = [dateFormatter stringFromDate:[NSDate date]];
        [BRDatePickerView showDatePickerWithTitle:@"修改日期" dateType:UIDatePickerModeDate defaultSelValue:nil minDateStr:minDateStr maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
            NSArray * arr = [selectValue componentsSeparatedByString:@"-"];
            [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                UITapGestureRecognizer *ges = weakSelf.timeSetView.gesColl[idx];
                ((UILabel *)(ges.view)).text = obj;
            }];
        }];
    }else{
        [BRDatePickerView showDatePickerWithTitle:@"修改时间" dateType:UIDatePickerModeTime defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
            NSArray * arr = [selectValue componentsSeparatedByString:@":"];
            [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                UITapGestureRecognizer *ges = weakSelf.timeSetView.gesColl[idx + 3];
                ((UILabel *)(ges.view)).text = obj;
            }];
        }];
    }
    
}

- (void)confirmTimeModifyClick:(UIButton *)sender{
    [self.zh_popupController dismiss];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
