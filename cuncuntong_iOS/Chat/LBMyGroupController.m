//
//  LBMyGroupController.m
//  L8
//
//  Created by 周吾昆 on 2018/6/16.
//  Copyright © 2018年 周吾昆. All rights reserved.
//

#import "LBMyGroupController.h"
#import "LBAddFriendRequestCell.h"
#import "LBGroupModel.h"
#import "CCChatController.h"
@interface LBMyGroupController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation LBMyGroupController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的群组";
    [self.view addSubview:self.tableView];
    [self initRefreshView];
    [self.tableView.mj_header beginRefreshing];
}

- (void)loadData {
//    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"group/getMyJoinGroup") params:@{@"uid":USERID,@"page":@"1",@"limit":@"1000"} success:^(NSDictionary *response,BOOL success) {
//        [self.dataArray removeAllObjects];
//        for (NSDictionary *dict in response[@"data"][@"list"]) {
//            LBGroupModel *model = [LBGroupModel modelWithJSON:dict];
//            [self.dataArray addObject:model];
//        }
//        [self.tableView reloadData];
//        [self endRefresh];
//    } failure:^(NSError *failure) {
//        [self endRefresh];
//    }];
}
- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LBAddFriendRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LBAddFriendRequestCell"];
    cell.groupModel = self.dataArray[indexPath.row];
    cell.hiddenButton = YES;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    LBGroupModel *model = self.dataArray[indexPath.row];
//    NSString *targetId = [NSString stringWithFormat:@"lao8_group_%@",model.id];
//    CCChatController *vc = [[CCChatController alloc]initWithConversationType:ConversationType_GROUP targetId:targetId];
//    vc.title = model.group;
//    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 侧滑功能

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    LBGroupModel *model = self.dataArray[indexPath.row];
    //添加一个删除按钮
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:@"退出群组" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
//        [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"group/removeMemberToGroup") params:@{@"gid":model.id,@"uid":USERID,@"token":TOKEN} success:^(NSDictionary *response, BOOL success) {
//            if (success) {
//                [self loadData];
//            }
//        } failure:^(NSError *failure) {
//
//        }];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    //将设置好的按钮方到数组中返回
    return @[deleteAction];
}
- (void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 60;
        _tableView.rowHeight = 60;
        [_tableView registerClass:[LBAddFriendRequestCell class] forCellReuseIdentifier:@"LBAddFriendRequestCell"];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

@end
