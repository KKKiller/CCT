//
//  CCLiveApplyController.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/15.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import "CCLiveApplyController.h"
#import "BRPickerView.h"
#import "PlaceholderTextView.h"

@interface CCLiveApplyController ()
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *timeBV;
@property (weak, nonatomic) IBOutlet UILabel *expectLab;
@property (weak, nonatomic) IBOutlet UILabel *startLab;
@property (weak, nonatomic) IBOutlet UILabel *endLab;

@end

@implementation CCLiveApplyController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    for (UIView *bv in _timeBV) {
        bv.layer.borderColor = MTRGB(0xC8C8C8).CGColor;
        bv.layer.borderWidth = 1;
        bv.layer.cornerRadius = 6;
        bv.layer.masksToBounds = YES;
        if ([bv isKindOfClass:[PlaceholderTextView class]]){
            bv.layer.borderColor = MTRGB(0xC8C8C8).CGColor;
            bv.layer.borderWidth = 1;
            ((PlaceholderTextView *)bv).placehoder = @"必填";
            ((PlaceholderTextView *)bv).placeholderTextColor = MTRGB(0x999999);
        }
//        self.noteTextView.placehoder = @"必填";
//        self.noteTextView.placeholderTextColor = MTRGB(0x999999);
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - Table view data source


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    if (indexPath.row == 6){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *minDateStr = [dateFormatter stringFromDate:[NSDate date]];
        [BRDatePickerView showDatePickerWithTitle:@"期望直播日期" dateType:UIDatePickerModeDate defaultSelValue:nil minDateStr:minDateStr maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
            weakSelf.expectLab.text = selectValue;
        }];
    }else if (indexPath.row == 7){
        [BRDatePickerView showDatePickerWithTitle:@"开始时段" dateType:UIDatePickerModeTime defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
//            weakSelf.expectLab.text = selectValue;
        }];
    }else if (indexPath.row == 8){
        [BRDatePickerView showDatePickerWithTitle:@"结束时段" dateType:UIDatePickerModeTime defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
            //            weakSelf.expectLab.text = selectValue;
        }];
    }
    
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
