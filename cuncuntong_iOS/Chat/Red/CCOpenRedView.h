//
//  CCOpenRedView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCOpenRedView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;

@property (weak, nonatomic) IBOutlet UIButton *openBtn;
@property (weak, nonatomic) IBOutlet UIButton *showDetailBtn;
@property (weak, nonatomic) IBOutlet UIImageView *showDetailArrow;
@property (weak, nonatomic) IBOutlet UILabel *openTitle;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;


@property (nonatomic, assign) BOOL isOver;
+ (instancetype)instanceView;
@end
