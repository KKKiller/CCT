//
//  CCRedListCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCRedListCell.h"
#import <YYKit.h>
@implementation CCRedListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.selectionStyle = UITableViewCellSeparatorStyleNone;
}
- (void)setModel:(CCRedGetterModel *)model{
    _model = model;
    self.nameLbl.text = model.realname;
    [self.headImgView setImageWithURL:URL(model.portrait) placeholder:IMAGENAMED(@"head")];
    self.moneyLbl.text = [NSString stringWithFormat:@"%@元",model.money];
    self.timeLbl.text = [TOOL convertIntavalTime:model.create];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
