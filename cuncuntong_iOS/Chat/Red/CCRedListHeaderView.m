//
//  CCRedListHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCRedListHeaderView.h"

@implementation CCRedListHeaderView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}
- (void)setRedData:(NSDictionary *)redData {
    _redData = redData;
    self.descLbl.text = [redData[@"text"] length] > 0 ?  redData[@"text"] : @"恭喜发财,大吉大利";
    self.redCountLbl.text = [NSString stringWithFormat:@"  共%@个红包",redData[@"numb"]];
}
- (void)setSenderInfo:(NSDictionary *)senderInfo {
    [self.headImg setImageWithURL:URL(senderInfo[@"portrait"]) placeholder:IMAGENAMED(@"head")];
    self.nameLbl.text = senderInfo[@"realname"];
}

@end
