//
//  CCPostRedController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCRedMessageContent.h"
typedef void (^PostRedSuccessBlock)(CCRedMessageContent *redContent );
@interface CCPostRedController : BaseViewController
@property (nonatomic, assign) BOOL isGroup;
@property (nonatomic, strong) NSString *toId; //发送给
@property (nonatomic, strong) PostRedSuccessBlock postRedSuccessBlock;

@end
