//
//  CCRedListCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCRedGetterModel.h"
@interface CCRedListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;


@property (nonatomic, strong) CCRedGetterModel *model;
@end
