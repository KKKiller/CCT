//
//  CCInputKeywordView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCInputKeywordView.h"

@implementation CCInputKeywordView
- (void)awakeFromNib {
    [super awakeFromNib];
    [self.tap addTarget:self action:@selector(tapGes)];
}
+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}
- (void)setBalance:(CGFloat)balance {
    _balance = balance;
    self.balanceLbl.text = [NSString stringWithFormat:@"余额%.2f元",balance];
}
- (void)setRedMoney:(CGFloat)redMoney {
    _redMoney = redMoney;
    self.priceLbl.text = [NSString stringWithFormat:@"¥%.2f元",redMoney];
}
- (void)tapGes {
    [self endEditing:YES];
}
@end
