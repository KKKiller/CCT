//
//  CCPostRedController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCPostRedController.h"
#import "CCInputKeywordView.h"
#import "RechargeViewController.h"
#import "CCRedMessageContent.h"
#import <IQKeyboardManager.h>
#import "BRPickerView.h"
@interface CCPostRedController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UITextField *numField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redNumTopMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redNumHeight;
@property (weak, nonatomic) IBOutlet UITextField *nomeyField;
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *msgPlaceHolder;
@property (weak, nonatomic) IBOutlet UITextField *msgField;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (strong, nonatomic) PathDynamicModal *animateModel;

@property (nonatomic, strong) CCInputKeywordView *keywordView;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, assign) CGFloat balance;


@property (nonatomic, assign) BOOL isRandomRed; //随机红包

@end

@implementation CCPostRedController

- (void)viewDidLoad {
    [super viewDidLoad];

    [IQKeyboardManager sharedManager].enable = YES;
    
    self.isRandomRed = YES;
    [self.closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.typeBtn addTarget:self action:@selector(typeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.sendBtn addTarget:self action:@selector(sendBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.redNumHeight.constant = self.isGroup ? 40 : 0;
    self.redNumTopMargin.constant = self.isGroup ? 15 : 0;
    self.typeBtn.hidden = !self.isGroup;
    self.typeLbl.hidden = !self.isGroup;
    [self.nomeyField addTarget:self action:@selector(passConTextChange) forControlEvents:UIControlEventEditingChanged];
    self.msgField.delegate = self;
    self.nomeyField.delegate = self;

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)closeBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)typeBtnClick:(UIButton *)sender {
    self.isRandomRed = !self.isRandomRed;
    NSString *title = self.isRandomRed ?  @"改为普通红包" : @"改为随机红包";
    [sender setTitle:title forState:UIControlStateNormal];
    NSString *desc = self.isRandomRed ?  @"每人抽到的金额随机" :@"每人抽到的金额相同";
    self.typeLbl.text = desc;
}
- (void)sendBtnClick {
    if ([self.nomeyField.text floatValue] <= 0) {
        SHOW(@"输入金额有误,请重新输入");
        return;
    }
    if (self.isGroup && [self.numField.text integerValue] <= 0) {
        SHOW(@"输入数量有误,请重新输入");
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": STR([UserManager sharedInstance].secret)} target:self success:^(NSDictionary *success) {
        self.balance = [success[@"data"][@"money"] floatValue];
        [self pay];
    } failure:^(NSError *failure) {
    }];
    
}

- (void)pay {
    [self.view endEditing:YES];
    CCInputKeywordView *view = [CCInputKeywordView instanceView];
    self.keywordView = view;
    [view.closeBtn addTarget:self action:@selector(closeRed) forControlEvents:UIControlEventTouchUpInside];
    [view.payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [view.chargeBtn addTarget:self action:@selector(chargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, 240, 270);
    view.keywordField.returnKeyType = UIReturnKeyDone;
    view.keywordField.delegate = self;
    
    view.redMoney = [self.nomeyField.text floatValue];
    view.balance = self.balance;
    
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:self.view];
}
- (void)closeRed {
    [self.animateModel closeWithStraight];
}
//发红包
- (void)payBtnClick {
    if (self.keywordView.keywordField.text.length == 0) {
        SHOW(@"请输入登录密码");
        return;
    }
    if (self.keywordView.keywordField.text.length == 0) {
        SHOW(@"请输入登录密码");
        return;
    }
    
    [self isPasswordValid];
}
- (void)isPasswordValid {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/login") params:@{@"mobile": [UserManager sharedInstance].mobile, @"passwd": self.keywordView.keywordField.text} target:self success:^(NSDictionary *success) {
        self.password = self.keywordView.keywordField.text;
        [self sendRed];
    } failure:^(NSError *   failure) {
    }];
}

- (void)sendRed {
    NSString *msg = self.msgField.text;
    if (msg.length == 0) {
        msg = @"恭喜发财,大吉大利";
    }
    if (self.isGroup) {
        NSString *groupId = [self.toId containsString:@"group_"] ? [self.toId substringFromIndex:6] : [self.toId containsString:@"vill_"] ? [self.toId substringFromIndex:5] : self.toId;
        NSDictionary *dict = @{@"rid":USERID,@"passwd":self.password,@"vid":STR(groupId),@"money":self.nomeyField.text,@"text":msg,@"numb":self.numField.text,@"mode":self.isRandomRed ? @"0" : @"1"};
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"red_add") params:dict target:self success:^(NSDictionary *success) {
            SHOW(@"发送成功");

//            [[RCIM sharedRCIM]sendMessage:ConversationType_GROUP targetId:self.toId content:message pushContent:@"红包" pushData:nil success:^(long messageId) {
//            } error:^(RCErrorCode nErrorCode, long messageId) {
//            }];
            [self closeRed];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSError *failure) {
        }];
    }else{
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"pred_add") params:@{@"rid":USERID,@"passwd":self.password,@"to_rid":STR(self.toId),@"money":self.nomeyField.text,@"text":msg} target:self success:^(NSDictionary *success) {
            SHOW(@"发送成功");
            CCRedMessageContent *message = [CCRedMessageContent messageWithContent:msg];
            message.money = [self.nomeyField.text floatValue];
            message.senderAvatar = success[@"data"][@"sender"][@"portrait"];
            message.senderName =success[@"data"][@"sender"][@"realname"];
            message.content = msg;
            message.redId =  [NSString stringWithFormat:@"%@",success[@"data"][@"redid"]];
            if (self.postRedSuccessBlock) {
                self.postRedSuccessBlock(message);
            }
            [self closeRed];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSError *failure) {
        }];
    }
}
//充值
- (void)chargeBtnClick {
//    [BRStringPickerView showStringPickerWithTitle:@"选择支付方式" dataSource:@[@"余额",@"微信支付",@"支付宝支付"] defaultSelValue:@"余额" isAutoSelect:NO resultBlock:^(id selectValue) {
//        if ([selectValue isEqualToString:@"余额"]) {
//            self.keywordView.balanceLbl.text = [NSString stringWithFormat:@"%@%@元",selectValue,[UserManager sharedInstance].money];
//        }else{
//            self.keywordView.balanceLbl.text =  selectValue;
//        }
//    }];
    [self closeRed];

    RechargeViewController *vc = [[RechargeViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)passConTextChange {
    self.moneyLbl.text = [NSString stringWithFormat:@"¥%@",self.nomeyField.text];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField isEqual:self.msgField]) {
        
        self.msgPlaceHolder.text = @"";
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
