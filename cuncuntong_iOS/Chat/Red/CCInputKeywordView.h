//
//  CCInputKeywordView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCInputKeywordView : UIView
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *tap;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UIButton *chargeBtn;
@property (weak, nonatomic) IBOutlet UITextField *keywordField;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@property (nonatomic, assign) CGFloat redMoney;
@property (nonatomic, assign) CGFloat balance;
+ (instancetype)instanceView ;
@end
