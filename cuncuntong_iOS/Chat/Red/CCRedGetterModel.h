//
//  CCRedGetterModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCRedGetterModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *portrait;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, assign) NSTimeInterval create;

@end
