//
//  TSMyOrderController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCTDController.h"
#import "CCTDJoinController.h"
#import "CCTDCreateController.h"
#import "CCTDSailController.h"
#import "TSOrderPageView.h"

static const CGFloat kLearningPageHeight = 45.0;

@interface CCTDController ()<UIScrollViewDelegate,TSOrderPageViewDelegate>
@property (strong, nonatomic) UIView *navView;
@property (strong, nonatomic) CCTDJoinController *joinVc;
@property (strong, nonatomic) CCTDCreateController *createVc;
@property (strong, nonatomic) CCTDSailController *sailVc;

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) TSOrderPageView *pageView; 
@property (nonatomic, assign) CGFloat kLearningTopHeight;


@end

@implementation CCTDController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"同道";
    CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
    self.kLearningTopHeight = system >= 11.0 || !App_Delegate.isDefaultRoot ? 64 : 0;
    [self.view addSubview:self.navView];
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.pageView];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [App_Delegate.transportTabbarVc updateRightTitle];
     self.navigationController.navigationBarHidden = YES;
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
    

//    self.navigationController.navigationBar.barTintColor = MTRGB(0x387DFF);
//    self.tabBarController.navigationItem.title = @"同道";
    
//    [self.tabBarController.navigationItem.rightBarButtonItem.customView setAlpha:0];
//    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, WCFNavigationHeight, MDK_SCREEN_WIDTH, 1)];
//    bottomLine.backgroundColor  = MTRGB(0x5191FF);
//    [self.view addSubview:bottomLine];
//    UINavigationBar *navigationBar = self.navigationController.navigationBar;
//    [navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//    [navigationBar setShadowImage:[UIImage imageWithColor:WHITECOLOR]];



}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [self.tabBarController.navigationItem.rightBarButtonItem.customView setAlpha:1];
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}

- (void)backAct{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshData {
    [self.joinVc refreshData];
    [self.sailVc refreshData];
}


#pragma mark - 代理
- (void)pageViewClickAtPageIndex:(NSInteger)index {
    CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
    CGFloat offset = system >= 11.0 || !App_Delegate.isDefaultRoot ? 0 : WCFNavigationHeight;
    [self.scrollView setContentOffset:CGPointMake(App_Width*index, -offset) animated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat startX = (App_Width - kLearningPageViewPageWidth*3 - kLearningPageViewMargin*2) * 0.5;
    CGFloat xOffset = (kLearningPageViewMargin + kLearningPageViewPageWidth) * (scrollView.contentOffset.x / App_Width);
    self.pageView.barView.x = startX + xOffset;
    /*
    NSArray *colorArray = [TOOL colorWithR1:56 g1:125 b1:255 r2:34 g2:34 b2:34 currentOffset:(float)scrollView.contentOffset.x totlalLength:(float)App_Width];
    if(scrollView.contentOffset.x <= App_Width){
        self.pageView.firstLbl.textColor = [colorArray firstObject];
        self.pageView.secondLbl.textColor = [colorArray lastObject];
        self.pageView.thirdLbl.textColor = TEXTBLACK3;
    }else{
        self.pageView.firstLbl.textColor = TEXTBLACK3;
        self.pageView.secondLbl.textColor = [colorArray firstObject];
        self.pageView.thirdLbl.textColor = [colorArray lastObject];
    }
     */
    
}

#pragma mark - 懒加载

- (UIView *)navView{
    if (!_navView){
        _navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, MDK_SCREEN_WIDTH, WCFNavigationHeight)];
        _navView.backgroundColor = MTRGB(0x5191FF);
        UILabel *titlelab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
        titlelab.textAlignment = UITextAlignmentCenter;
        titlelab.textColor = [UIColor whiteColor];
        titlelab.text = @"同道";
        titlelab.font = [UIFont systemFontOfSize:18];
        titlelab.centerX = _navView.centerX;
        titlelab.centerY = WCFNavigationHeight - 24;
        [_navView addSubview:titlelab];
        
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        leftBtn.frame = CGRectMake(5, 0, 30, 30);
        [leftBtn setImage:IMAGENAMED(@"btn_back") forState:UIControlStateNormal];
        [leftBtn addTarget:self action:@selector(backAct) forControlEvents:UIControlEventTouchUpInside];
        leftBtn.centerY = titlelab.centerY;
        [_navView addSubview:leftBtn];
    }
    return _navView;
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        CGFloat y =  WCFNavigationHeight + kLearningPageHeight;
        CGFloat height = App_Height - y - WCFTabBarHeight;
        CGFloat hh = App_Height;
        kLog(@"hh%f %f %f",hh,height,y);
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, y,App_Width, height)];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(App_Width * 3, 0);
        _scrollView.pagingEnabled= YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        
        [self addChildViewController:self.joinVc];
        [self.scrollView addSubview:self.joinVc.view];
        [self.scrollView addSubview:self.createVc.view];
        [self.scrollView addSubview:self.sailVc.view];
        
    }
    return _scrollView;
}
- (CCTDJoinController *)joinVc {
    if (_joinVc == nil) {
        _joinVc = [[CCTDJoinController alloc]init];
        CGFloat y =  kLearningPageHeight + WCFNavigationHeight;
        CGFloat height = App_Height - y - WCFTabBarHeight;
        CGRect frame = CGRectMake(0, 0, App_Width, height);
        _joinVc.view.frame = frame;
        [self addChildViewController:_joinVc];
    }
    return _joinVc;
}
- (CCTDCreateController *)createVc {
    if (_createVc == nil) {
        _createVc = [[CCTDCreateController alloc]init];
        CGFloat y =  kLearningPageHeight + WCFNavigationHeight;
        CGFloat height = App_Height - y - WCFTabBarHeight;
        CGRect frame = CGRectMake(App_Width, 0, App_Width, height);
        _createVc.view.frame = frame;
        [self addChildViewController:_createVc];
    }
    return _createVc;
}
- (CCTDSailController *)sailVc {
    if (_sailVc == nil) {
        _sailVc = [[CCTDSailController alloc]init];
        CGFloat y =  kLearningPageHeight + WCFNavigationHeight;
        CGFloat height = App_Height - y - WCFTabBarHeight;
        CGRect frame = CGRectMake(App_Width*2, 0, App_Width, height);
        _sailVc.view.frame = frame;
        _sailVc.tableView.frame = CGRectMake(0, 0, App_Width, height);
        [self addChildViewController:_sailVc];
    }
    return _sailVc;
}

- (TSOrderPageView *)pageView {
    if (_pageView == nil) {
        _pageView = [[TSOrderPageView alloc]init];
        _pageView.delegate = self;
        _pageView.frame = CGRectMake(0, WCFNavigationHeight, App_Width, kLearningPageHeight);
//        _pageView.backgroundColor =
        [self.view addSubview:_pageView];
        _pageView.firstLbl.text = @"加入";
        _pageView.secondLbl.text = @"创建";
        _pageView.thirdLbl.text = @"拍卖";
//         _pageView.barView.backgroundColor = MTRGB(0x387DFF);
        _pageView.backgroundColor = MTRGB(0x5191FF);
        _pageView.firstLbl.textColor = WHITECOLOR;
        _pageView.secondLbl.textColor = WHITECOLOR;
        _pageView.thirdLbl.textColor = WHITECOLOR;
//        _pageView.bottomLineLbl.backgroundColor  = WHITECOLOR;
        _pageView.barView.backgroundColor = WHITECOLOR;
    }
    return _pageView;
}


@end
