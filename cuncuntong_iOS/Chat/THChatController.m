//
//  THChatController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/19.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "THChatController.h"
#import "GetImgBase64String.h"
#import "NSData+Base64.h"
@interface THChatController ()<ICChatBoxViewControllerDelegate>
@property (nonatomic, strong) NSArray *emotionManagers;
@property (nonatomic, strong) XHMessageTableViewCell *currentSelectedCell;
@property (nonatomic, strong) UIView *unOpaqueView;
@property (nonatomic, strong) ICChatBoxViewController *chatBoxVC;
@property (nonatomic, assign) BOOL isKeyBoardAppear;     // 键盘是否弹出来了
@property (nonatomic, strong) CCRedMessageContent *red;
@property (strong, nonatomic) PathDynamicModal *animateModel;

@property (nonatomic, strong) ICVoiceHud *voiceHud;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) XHLocationHelper *locationHelper;

@property (nonatomic, strong) CCLiveController *liveVc;
@property (nonatomic, strong) UIButton *startLiveBtn;

@property (nonatomic, assign) BOOL liveVcShowing;
@end

@implementation THChatController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setNav];
    XMPPTOOL.currentJid  = self.toJid;
    [NOTICENTER addObserver:self selector:@selector(getNewMessage:) name:kReceiveMessageNotification object:nil];
    self.messageSender = REALNAME;
    //键盘
    [self addChildViewController:self.chatBoxVC];
    
    [self.view addSubview:self.chatBoxVC.view];

    //历史消息
    [self loadHistoryMessages];
    self.startLiveBtn.hidden = NO;
    
    [self.view insertSubview:self.unOpaqueView belowSubview:self.chatBoxVC.view];

}

- (void)letKeyBoardDismiss:(UITapGestureRecognizer *)sender{
    [self.chatBoxVC.chatBox resignFirstResponder];
    WEAKSELF
    if (_chatBoxVC.view.top != APP_Frame_Height - WCFNavigationHeight - WCFTabBarHeight){
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.chatBoxVC.view.top = APP_Frame_Height - WCFNavigationHeight - WCFTabBarHeight;
            weakSelf.messageTableView.height = App_Height - WCFNavigationHeight - WCFTabBarHeight;
            weakSelf.chatBoxVC.chatBox.status =     ICChatBoxStatusNothing;
            [weakSelf.unOpaqueView setHidden:YES];
            [weakSelf.chatBoxVC.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (![obj isKindOfClass:[ICChatBox class]]){
                    obj.alpha = 0;
                }
            }];
        }];
    }
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [_chatBoxVC.view setFrame:CGRectMake(0,APP_Frame_Height - WCFNavigationHeight - WCFTabBarHeight, App_Frame_Width, App_Height)];
    [self.messageTableView setFrame:CGRectMake(0, 0, App_Width, App_Height - WCFNavigationHeight - WCFTabBarHeight)];
}
- (void)setNav {
    self.title = self.titleStr ?: @"聊天";

    UIButton *settingBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [settingBtn setImage:IMAGENAMED(@"share_more") forState:UIControlStateNormal];
    [settingBtn addTarget:self action:@selector(chatSetting) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *getMasterBtn =[UIButton buttonWithType:(UIButtonTypeCustom)];
    [getMasterBtn setTitle:@"抢群主" forState:(UIControlStateNormal)];
    [getMasterBtn setTitleColor:MTRGB(0xFF4242) forState:(UIControlStateNormal)];
    getMasterBtn.layer.masksToBounds=YES;
    getMasterBtn.layer.cornerRadius= 12;
    getMasterBtn.titleLabel.font=[UIFont systemFontOfSize:12];
    getMasterBtn.backgroundColor = MTRGB(0xFFF18B);
    [getMasterBtn addTarget:self action:@selector(getMaster) forControlEvents:UIControlEventTouchUpInside];
    
    settingBtn.frame = CGRectMake(0, 0, 25, 12);
    getMasterBtn.frame=CGRectMake(0, 0, 45, 12);
    
    UIBarButtonItem *setting = [[UIBarButtonItem alloc] initWithCustomView:settingBtn];
    UIBarButtonItem *getMaster = [[UIBarButtonItem alloc] initWithCustomView:getMasterBtn];
    [getMasterBtn setTintColor:[UIColor redColor]];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: setting, getMaster,nil]];
    
}
//聊天设置 群设置
- (void)chatSetting{
    ChatGroupSettingController *vc = [[ChatGroupSettingController alloc]init];
    vc.groupId = self.groupId;
    [self.navigationController pushViewController:vc animated:YES];
}
//抢群主
- (void)getMaster {
    if (self.liveVcShowing) {
        [self hideLiveView];
    }else{
        [self showLiveView];
    }
}
- (void)startLive {
    if (self.liveVcShowing) {
        [self hideLiveView];
    }else{
        [self showLiveView];
        self.liveVc.isPusher = YES;
        [self.liveVc play];
    }
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].enable = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.messageTableView reloadData];
    });
}




#pragma mark - 收消息
- (void)getNewMessage:(NSNotification *)noti {
    XMPPMessage *message = noti.object;
    NSLog(@"收到新消息 %@",message.body);
    if (![message.from isEqualToJID:XMPPTOOL.userJid]) {
        [self addMessage:[self convertXMPPMessageToXHMessage:message]];
    }
}
//消息转换
- (XHMessage *)convertXMPPMessageToXHMessage:(XMPPMessage *)message{
    NSString *body = message.body;
    XHMessage *msg = [[XHMessage alloc]initWithText:nil sender:nil timestamp:[NSDate date]];
    msg.uid = [XMPPTOOL getUidWithJid:message.from];
    msg = [self configXHMessage:msg body:body];
    return msg;
}
//特殊消息类型解析
- (XHMessage*)configXHMessage:(XHMessage *)msg body:(NSString *)body{
    msg.bubbleMessageType = XHBubbleMessageTypeReceiving;
    if ([body containsString:@"<picture_YY>"]) {
        body = [body stringByReplacingOccurrencesOfString:@"<picture_YY>" withString:@""];
        NSRange range = [body rangeOfString:@"@"];
        if (range.length > 0) {
            body = [body substringToIndex:range.location];
        }
        NSData * imageData =[[NSData alloc] initWithBase64EncodedString:body options:NSDataBase64DecodingIgnoreUnknownCharacters];
        msg.photo = [UIImage imageWithData:imageData] ?: IMAGENAMED(@"SubPlaceholder");
        msg.messageMediaType = XHBubbleMessageMediaTypePhoto;
    }else if ([body containsString:@"<voice_YY>"]){
        NSArray *array = [body componentsSeparatedByString:@"<voice_YY>"];
        body = array[1];
        NSRange range = [body rangeOfString:@"@"];
        if (range.length > 0) {
            body = [body substringToIndex:range.location];
        }
        NSData * voiceData =[[NSData alloc] initWithBase64EncodedString:body options:NSDataBase64DecodingIgnoreUnknownCharacters];
        msg.voiceData = voiceData;
        msg.voiceDuration = [NSString stringWithFormat:@"%.1f",[[array[2] substringFromIndex:1] floatValue]];
        msg.messageMediaType = XHBubbleMessageMediaTypeVoice;
        
    }else if ([body containsString:@"<RedPacket_YY>"]){
         body = [body stringByReplacingOccurrencesOfString:@"<RedPacket_YY>" withString:@""];
        NSData *jsonData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData               options:NSJSONReadingMutableContainers       error:nil];
        msg.messageMediaType = XHBubbleMessageMediaTypeRed;
        msg.redid = dic[@"redid"];
        msg.content = dic[@"content"];
        msg.realname = dic[@"realname"];
        msg.portrait = dic[@"portrait"];
    }else if ([body containsString:@"<SelfLocation_YY>"]){
        body = [body stringByReplacingOccurrencesOfString:@"<SelfLocation_YY>" withString:@""];
        NSData *jsonData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData               options:NSJSONReadingMutableContainers       error:nil];
        
        msg.messageMediaType = XHBubbleMessageMediaTypeLocalPosition;
        msg.localPositionPhoto = IMAGENAMED(@"Fav_Cell_Loc");
        msg.location = [[CLLocation alloc]initWithLatitude:[dic[@"lat"] doubleValue] longitude:[dic[@"lng"] doubleValue]];
        msg.geolocations = dic[@"address"];
    }else{
        msg.text = body;
    }
    return msg;
}

//历史消息
- (void)loadHistoryMessages{
    NSManagedObjectContext *context = [XmppTools sharedManager].messageArchivingCoreDataStorage.mainThreadManagedObjectContext;
    
    // 2.FetchRequest【查哪张表】
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"XMPPMessageArchiving_Message_CoreDataObject"];
    //创建查询条件
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr = %@ and streamBareJidStr = %@", self.toJid.bare, XMPPTOOL.userJid.bare];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    //    fetchRequest.fetchOffset = 0;
    //    fetchRequest.fetchLimit = 10;
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(fetchedObjects.count > 0){
        if ([self.messages count] > 0) {
            [self.messages removeAllObjects];
        }
        NSMutableArray *historyMessages = [NSMutableArray array];
        for (XMPPMessageArchiving_Message_CoreDataObject *obj in fetchedObjects) {
            XHMessage *message = [[XHMessage alloc]init];
            message = [self configXHMessage:message body:obj.body];
            message.uid = [XMPPTOOL getUidWithJid:obj.message.from];
            message.timestamp = obj.timestamp;
            if (obj.message.from) {
                message.bubbleMessageType = XHBubbleMessageTypeReceiving;
            }else{
                message.bubbleMessageType = XHBubbleMessageTypeSending;
                message.uid = USERID;
            }
            [historyMessages addObject:message];
        }
        [self.messages addObjectsFromArray:historyMessages];
        [self.messageTableView reloadData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self scrollToBottomAnimated:NO];
        });
    }
}
//下拉加载更多消息
- (BOOL)shouldLoadMoreMessagesScrollToTop {
    return YES;
}




#pragma mark - 发送消息
- (void)sendMessage:(XHMessage *)message{
    if (message.messageMediaType == XHBubbleMessageMediaTypeText) {
        [self sendTextMessage:message.text];
    }else if (message.messageMediaType == XHBubbleMessageMediaTypePhoto){
        [self sendPhotoWithPhoto:message.photo];
    }else if(message.messageMediaType == XHBubbleMessageMediaTypeVoice){
        [self sendRecordMessageWithVoicePath:message.voicePath time:[message.voiceDuration floatValue]];
    }else if (message.messageMediaType == XHBubbleMessageMediaTypeLocalPosition){
        [self sendLocationMessage:message];
    }else if (message.messageMediaType == XHBubbleMessageMediaTypeRed){
        [self sendRedMessage:message];
    }
}
//发送文本消息
- (void)sendTextMessage:(NSString *)text {
    XMPPMessage *xmppMessage = [XMPPMessage messageWithType:@"chat" to:self.toJid elementID:[TOOL getCurrentTimeStr]];
    [xmppMessage addBody:text];
    [XMPPTOOL.xmppStream sendElement:xmppMessage];
}
//图片消息
- (void)sendPhotoWithPhoto:(UIImage *)photo
{
    NSString *base64Str = [GetImgBase64String getImgBase64StringWithImg:photo];
    NSString *timestamp = [NSString stringWithFormat:@"@%.0f",[[NSDate date] timeIntervalSince1970]*1000];
    base64Str = [NSString stringWithFormat:@"%@%@",base64Str,timestamp];
    XMPPMessage *message = [XMPPMessage messageWithType:CHATTYPE to:self.toJid elementID:[TOOL getCurrentTimeStr]];
    [message addBody:[NSString stringWithFormat:@"<picture_YY>%@<picture_YY>",base64Str]];
    [[XmppTools sharedManager].xmppStream sendElement:message];
}
//语音消息
- (void)sendRecordMessageWithVoicePath:(NSString *)path time:(float)time
{
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSString *base64str = [data base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    NSString *timestamp = [NSString stringWithFormat:@"@%.0f",[[NSDate date] timeIntervalSince1970]*1000];
    base64str = [NSString stringWithFormat:@"%@%@",base64str,timestamp];
    XMPPMessage *message = [XMPPMessage messageWithType:CHATTYPE to:self.toJid elementID:[TOOL getCurrentTimeStr]];
    [message addBody:[NSString stringWithFormat:@"<voice_YY>%@<voice_YY>&%.1f",base64str,time]];
    NSLog(@"path------%@",path);
    [[XmppTools sharedManager].xmppStream sendElement:message];
    
}
//定位消息
- (void)sendLocationMessage:(XHMessage *)message
{
    NSDictionary *dict = @{
   @"lat":[NSString stringWithFormat:@"%.f",message.location.coordinate.latitude],
   @"lng":[NSString stringWithFormat:@"%.f",message.location.coordinate.longitude],
@"address":message.geolocations,@"titleAddress":message.geolocations};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:0];
    NSString *dataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    XMPPMessage *geoMessage = [XMPPMessage messageWithType:CHATTYPE to:self.toJid elementID:[TOOL getCurrentTimeStr]];
    NSString *body = [NSString stringWithFormat:@"<SelfLocation_YY>%@<SelfLocation_YY>",dataStr];
    [geoMessage addBody:body];
    [[XmppTools sharedManager].xmppStream sendElement:geoMessage];
}
//红包消息
- (void)sendRedMessage:(XHMessage *)message
{
    NSDictionary *dict = @{
                           @"redid":message.redid,
                           @"content":message.content,
                           @"realname":message.realname,
                           @"portrait":message.portrait
                           };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:0];
    NSString *dataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    XMPPMessage *redMessage = [XMPPMessage messageWithType:CHATTYPE to:self.toJid elementID:[TOOL getCurrentTimeStr]];
    NSString *body = [NSString stringWithFormat:@"<RedPacket_YY>%@<RedPacket_YY>",dataStr];
    [redMessage addBody:body];
    [[XmppTools sharedManager].xmppStream sendElement:redMessage];
}
//是否显示时间轴Label的回调方法
- (BOOL)shouldDisplayTimestampForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.messages.count > indexPath.row) {
        XHMessage *message =  [self.messages objectAtIndex:indexPath.row];
        XHMessage *lastMessage = [self.messages objectAtIndex:indexPath.row >0 ? indexPath.row - 1 : 0];
        if ([message.timestamp timeIntervalSince1970] - [lastMessage.timestamp timeIntervalSince1970] > 20) {
            return YES;
        }
    }
    return NO;
}
    
//配置Cell的样式或者字体
- (void)configureCell:(XHMessageTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
}
    
//协议回掉是否支持用户手动滚动
- (BOOL)shouldPreventScrollToBottomWhileUserScrolling {
    return YES;
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [super scrollViewWillBeginDragging:scrollView];
//    [self.chatBoxVC.chatBox resignFirstResponder];
//    WEAKSELF
//    if (_chatBoxVC.view.top != APP_Frame_Height - WCFNavigationHeight - WCFTabBarHeight){
//        [UIView animateWithDuration:0.2 animations:^{
//            weakSelf.chatBoxVC.view.top = APP_Frame_Height - WCFNavigationHeight - WCFTabBarHeight;
//            weakSelf.messageTableView.height = App_Height - WCFNavigationHeight - WCFTabBarHeight;
//            [weakSelf.chatBoxVC.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                if (![obj isKindOfClass:[ICChatBox class]]){
//                    obj.alpha = 0;
//                }
//            }];
//        }];
//    }
}

#pragma mark - 懒加载
- (UIView *)unOpaqueView{
    if (!_unOpaqueView) {
        _unOpaqueView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, App_Width, App_Height)];
        _unOpaqueView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(letKeyBoardDismiss:)];
        [_unOpaqueView addGestureRecognizer:tap];
        [_unOpaqueView setHidden:YES];
    }
    return _unOpaqueView;
    
    
}

    
#pragma mark - Cell 代理回调 点击
//单击消息
- (void)multiMediaMessageDidSelectedOnMessage:(id<XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath onMessageTableViewCell:(XHMessageTableViewCell *)messageTableViewCell {
    [self.chatBoxVC.chatBox resignFirstResponder];
    UIViewController *disPlayViewController;
    switch (message.messageMediaType) {
        case XHBubbleMessageMediaTypeVideo:
        case XHBubbleMessageMediaTypePhoto: {
            DLog(@"message : %@", message.photo);
            DLog(@"message : %@", message.videoConverPhoto);
            XHDisplayMediaViewController *messageDisplayTextView = [[XHDisplayMediaViewController alloc] init];
            messageDisplayTextView.message = message;
            disPlayViewController = messageDisplayTextView;
            break;
        }
        break;
        case XHBubbleMessageMediaTypeVoice: {
            DLog(@"message : %@", message.voicePath);
            // Mark the voice as read and hide the red dot.
            message.isRead = YES; messageTableViewCell.messageBubbleView.voiceUnreadDotImageView.hidden = YES;
            [[XHAudioPlayerHelper shareInstance] setDelegate:(id<NSFileManagerDelegate>)self];
            if (_currentSelectedCell) {
                [_currentSelectedCell.messageBubbleView.animationVoiceImageView stopAnimating];
            }
            if (_currentSelectedCell == messageTableViewCell) { [messageTableViewCell.messageBubbleView.animationVoiceImageView stopAnimating];
                [[XHAudioPlayerHelper shareInstance] stopAudio];
                self.currentSelectedCell = nil;
            } else {
                self.currentSelectedCell = messageTableViewCell;
                [messageTableViewCell.messageBubbleView.animationVoiceImageView startAnimating];
                [[XHAudioPlayerHelper shareInstance] managerAudioWithFileName:message.voicePath data:message.voiceData  toPlay:YES];
            }
            break;
        }
        case XHBubbleMessageMediaTypeEmotion:
        DLog(@"facePath : %@", message.emotionPath);
        break;
        case XHBubbleMessageMediaTypeLocalPosition: {
            XHDisplayLocationViewController *displayLocationViewController = [[XHDisplayLocationViewController alloc] init];
            displayLocationViewController.message = message;
            disPlayViewController = displayLocationViewController;
            break;
        }
        case XHBubbleMessageMediaTypeRed: {
            DLog(@"facePath : %@", message.localPositionPhoto);
            self.red = [[CCRedMessageContent alloc]init];
            self.red.redId = message.redid;
            self.red.content = message.content;
            self.red.senderName = message.realname;
            self.red.senderAvatar = message.portrait;
            [self openRed];
            break;
        }
        default:
        break;
    }
    if (disPlayViewController) {
        [self.navigationController pushViewController:disPlayViewController animated:YES];
    }
}
//双击消息
- (void)didDoubleSelectedOnTextMessage:(id<XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath {
    DLog(@"text : %@", message.text);
    XHDisplayTextViewController *displayTextViewController = [[XHDisplayTextViewController alloc] init];
    displayTextViewController.message = message;
    [self.navigationController pushViewController:displayTextViewController animated:YES];
}
//点击头像
- (void)didSelectedAvatarOnMessage:(id<XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath {
    
}
//文本气泡
- (void)menuDidSelectedAtBubbleMessageMenuSelecteType:(XHBubbleMessageMenuSelecteType)bubbleMessageMenuSelecteType {
}

#pragma mark - 键盘回调 发送消息
- (ICChatBoxViewController *) chatBoxVC
{
    if (_chatBoxVC == nil) {
        _chatBoxVC = [[ICChatBoxViewController alloc] init];
        /*
        [_chatBoxVC.view setFrame:CGRectMake(0,APP_Frame_Height-HEIGHT_TABBAR - 64, App_Frame_Width, APP_Frame_Height)];
        */
        
        _chatBoxVC.delegate = self;
    }
    return _chatBoxVC;
}
//发送文本
- (void) chatBoxViewController:(ICChatBoxViewController *)chatboxViewController
               sendTextMessage:(NSString *)messageStr
{
    if (messageStr && messageStr.length > 0) {
        XHMessage *textMessage = [[XHMessage alloc] initWithText:messageStr sender:REALNAME timestamp:[NSDate date]];
        textMessage.uid = USERID;
        [self addMessage:textMessage];
        [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeText];
        [self sendMessage:textMessage];
    }
}
//发送图片
- (void) chatBoxViewController:(ICChatBoxViewController *)chatboxViewController
              sendImageMessage:(UIImage *)image
                     imagePath:(NSString *)imgPath{
    XHMessage *photoMessage = [[XHMessage alloc] initWithPhoto:image thumbnailUrl:nil originPhotoUrl:nil sender:REALNAME timestamp:[NSDate date]];
    photoMessage.uid = USERID;
    photoMessage.messageMediaType = XHBubbleMessageMediaTypePhoto;
    [self addMessage:photoMessage];
    [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypePhoto];
    [self sendMessage:photoMessage];
    
}
//发送定位
- (void)chatBoxViewControllerSendLocation:(ICChatBoxViewController *)chatboxViewController {
    [self.locationHelper getCurrentGeolocationsCompled:^(NSArray *placemarks) {
        CLPlacemark *placemark = [placemarks lastObject];
        if (placemark) {
            NSDictionary *addressDictionary = placemark.addressDictionary;
            NSArray *formattedAddressLines = [addressDictionary valueForKey:@"FormattedAddressLines"];
            NSString *geoLocations = [formattedAddressLines lastObject];
            if (geoLocations) {
                XHMessage *geoLocationsMessage = [[XHMessage alloc] initWithLocalPositionPhoto:IMAGENAMED(@"Fav_Cell_Loc") geolocations:geoLocations location:placemark.location sender:REALNAME timestamp:[NSDate date]];
                geoLocationsMessage.uid = USERID;
                geoLocationsMessage.messageMediaType = XHBubbleMessageMediaTypeLocalPosition;

                [self addMessage:geoLocationsMessage];
                [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeLocalPosition];
                [self sendMessage:geoLocationsMessage];
            }
        }else{
            SHOW(@"定位失败");
        }
    }];
}
//发红包
- (void)chatBoxViewControllerSendRed:(ICChatBoxViewController *)chatboxViewController {
    [self postRed];
}
//高度变化
- (void) chatBoxViewController:(ICChatBoxViewController *)chatboxViewController
        didChangeChatBoxHeight:(CGFloat)height
{
    self.chatBoxVC.view.top = self.view.bottom-height - WCFNavigationHeight;
    self.messageTableView.height = App_Height - WCFNavigationHeight - height;
    if (height == WCFTabBarHeight) {
        [self.messageTableView reloadData];
        _isKeyBoardAppear  = NO;
        [_unOpaqueView setHidden:YES];
    } else {
        [self.messageTableView scrollToBottom];
        _isKeyBoardAppear  = YES;
        [_unOpaqueView setHidden:NO];
        
        
        
        
    }
}

#pragma mark - 红包
//发红包
- (void)postRed {
    CCPostRedController *vc = [[CCPostRedController alloc]init];
    vc.toId = [XMPPTOOL getUidWithJid:self.toJid];
    vc.isGroup = NO ;
    __weak typeof(self) weakSelf = self;
    vc.postRedSuccessBlock = ^(CCRedMessageContent *redContent) {
        weakSelf.red = redContent;
        XHMessage *redMessage = [[XHMessage alloc] init];
        redMessage.timestamp = [NSDate date];
        redMessage.uid = USERID;
        redMessage.messageMediaType = XHBubbleMessageMediaTypeRed;
        redMessage.redid = redContent.redId;
        redMessage.content = redContent.content;
        redMessage.portrait = redContent.senderAvatar;
        redMessage.realname = redContent.senderName;
        [weakSelf addMessage:redMessage];
        [weakSelf finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeRed];
        [weakSelf sendMessage:redMessage];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
//弹出红包
- (void)openRed {
    CCOpenRedView *view = [CCOpenRedView instanceView];
    [view.closeBtn addTarget:self action:@selector(closeRed) forControlEvents:UIControlEventTouchUpInside];
    [view.openBtn addTarget:self action:@selector(getRed) forControlEvents:UIControlEventTouchUpInside];
    [view.showDetailBtn addTarget:self action:@selector(redDetail) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, 240, 320);
    view.isOver = NO;
    view.nameLbl.text = self.red.senderName;
    view.descLbl.text = self.red.content;
    [view.headImgView setImageWithURL:URL(self.red.senderAvatar) placeholder:IMAGENAMED(@"head")];
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}
- (void)closeRed {
    [self.animateModel closeWithStraight];
}
//抢红包
- (void)getRed {
    if ([self.red.senderName isEqualToString:[UserManager sharedInstance].realname]) {
        [self redDetail];
    }else{
        //群红包
//        if([self.targetId containsString:@"vill"] || [self.targetId containsString:@"group"]){
//            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"qiang") params:@{@"id":USERID,@"redid":STR(self.red.redId)} target:self success:^(NSDictionary *success) {
//                //红包列表
//                [self redDetail];
//            } failure:^(NSError *failure) {
//
//            }];
//        }else{
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"pqiang") params:@{@"id":USERID,@"redid":STR(self.red.redId)} target:self success:^(NSDictionary *success) {
                //红包列表
                [self redDetail];
            } failure:^(NSError *failure) {
            }];
//        }
        
    }
}
- (void)redDetail {
    [self closeRed];
    CCRedLIstController *vc = [[CCRedLIstController alloc]init];
    vc.redid = self.red.redId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 发送语音
- (ICVoiceHud *)voiceHud
{
    if (!_voiceHud) {
        _voiceHud = [[ICVoiceHud alloc] initWithFrame:CGRectMake(0, 0, 155, 155)];
        _voiceHud.hidden = YES;
        [self.view addSubview:_voiceHud];
        _voiceHud.center = CGPointMake(App_Frame_Width/2, APP_Frame_Height/2);
    }
    return _voiceHud;
}
- (void) chatBoxViewController:(ICChatBoxViewController *)chatboxViewController sendVoiceMessage:(NSString *)voicePath duration:(NSTimeInterval)duration
{
    [self timerInvalue]; // 销毁定时器
    self.voiceHud.hidden = YES;
    XHMessage *voiceMessage = [[XHMessage alloc] initWithVoicePath:voicePath voiceUrl:nil voiceDuration:[NSString stringWithFormat:@"%.1f",duration] sender:REALNAME timestamp:[NSDate date]];
    voiceMessage.sender = REALNAME;
    voiceMessage.uid = USERID;
    voiceMessage.messageMediaType = XHBubbleMessageMediaTypeVoice;
    [self addMessage:voiceMessage];
    [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeVoice];
    [self sendMessage:voiceMessage];
}
- (void)voiceDidCancelRecording
{
    [self timerInvalue];
    self.voiceHud.hidden = YES;
}
- (void)voiceDidStartRecording
{
    [self timerInvalue];
    self.voiceHud.hidden = NO;
    [self timer];
}

// 向外或向里移动
- (void)voiceWillDragout:(BOOL)inside
{
    if (inside) {
        [_timer setFireDate:[NSDate distantPast]];
        _voiceHud.image  = [UIImage imageNamed:@"voice_1"];
    } else {
        [_timer setFireDate:[NSDate distantFuture]];
        self.voiceHud.animationImages  = nil;
        self.voiceHud.image = [UIImage imageNamed:@"cancelVoice"];
    }
}
- (void)progressChange
{
    AVAudioRecorder *recorder = [[ICRecordManager shareManager] recorder] ;
    [recorder updateMeters];
    float power= [recorder averagePowerForChannel:0];//取得第一个通道的音频，注意音频强度范围时-160到0,声音越大power绝对值越小
    CGFloat progress = (1.0/160)*(power + 160);
    self.voiceHud.progress = progress;
}
- (void)didAudioPlayerStopPlay:(AVAudioPlayer *)audioPlayer {
    if (!_currentSelectedCell) {
        return;
    }
    [_currentSelectedCell.messageBubbleView.animationVoiceImageView stopAnimating];
    self.currentSelectedCell = nil;
}
- (void)voiceRecordSoShort
{
    [self timerInvalue];
    self.voiceHud.animationImages = nil;
    self.voiceHud.image = [UIImage imageNamed:@"voiceShort"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.voiceHud.hidden = YES;
    });
}

- (void)timerInvalue
{
    [_timer invalidate];
    _timer  = nil;
}
- (NSTimer *)timer
{
    if (!_timer) {
        _timer =[NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(progressChange) userInfo:nil repeats:YES];
    }
    return _timer;
}

- (XHLocationHelper *)locationHelper {
    if (!_locationHelper) {
        _locationHelper = [[XHLocationHelper alloc] init];
    }
    return _locationHelper;
}
#pragma mark - 直播
- (CCLiveController *)liveVc {
    if (!_liveVc) {
        WEAKSELF
        _liveVc = [[CCLiveController alloc]init];
        _liveVc.view.frame = CGRectMake(0, 0, App_Width, 0);
        //直播
        [self addChildViewController:_liveVc];
        [self.view addSubview:_liveVc.view];
        _liveVc.stopPushLiveBlk = ^{
            [weakSelf hideLiveView];
        };
    }
    return _liveVc;
}
- (void)showLiveView {
    [UIView animateWithDuration:0.25 animations:^{
        CGFloat height = App_Width * (3.0 / 4.0) + 40;
        self.liveVc.view.height = height;
        self.messageTableView.frame = CGRectMake(0, height, App_Width, App_Height - 64 - 49 - height);
        self.liveVcShowing = YES;
    }];
}
- (void)hideLiveView {
    [UIView animateWithDuration:0.25 animations:^{
        self.liveVc.view.height = 0;
        self.messageTableView.frame = CGRectMake(0, 0, App_Width, App_Height - 64 - 49);
        self.liveVcShowing = NO;
    }];
}
- (UIButton *)startLiveBtn {
    if (!_startLiveBtn) {
        _startLiveBtn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width - 100, (App_Height - 64 - 49)*0.5 - 50, 100, 53)];
        [_startLiveBtn addTarget:self action:@selector(startLive) forControlEvents:UIControlEventTouchUpInside];
        [_startLiveBtn setImage:IMAGENAMED(@"live_broadcast") forState:UIControlStateNormal];
        [self.view addSubview:_startLiveBtn];
    }
    return _startLiveBtn;
}

- (void)dealloc{
     [[XHAudioPlayerHelper shareInstance] setDelegate:nil];
}
@end
