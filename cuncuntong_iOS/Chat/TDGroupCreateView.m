//
//  TDGroupCreateView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "TDGroupCreateView.h"

@implementation TDGroupCreateView

+ (instancetype)instanceView {
    TDGroupCreateView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    self.descTextView.placehoder = @"请填写群介绍，至少15个字.";
    self.descTextView.placeholderTextColor = RGB(192, 192, 192);

    self.detailTextView.placehoder = @"请填写你对创建本群有何资源优势？能否在1个月内吸收300以上会员入群";
    self.detailTextView.placeholderTextColor = RGB(192, 192, 192);
    
}

@end
