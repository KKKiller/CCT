//
//  CCRedMessageContent.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

@interface CCRedMessageContent : NSObject
@property(nonatomic, strong) NSString *content;
@property (nonatomic, assign) CGFloat money;
@property (nonatomic, strong) NSString *redId;
@property (nonatomic, strong) NSString *senderName;
@property (nonatomic, strong) NSString *senderAvatar;
+ (instancetype)messageWithContent:(NSString *)content;

@end
