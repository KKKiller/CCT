//
//  JoinView.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/14.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import "JoinView.h"

@implementation JoinView


+ (instancetype)instanceView {
    JoinView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}

@end
