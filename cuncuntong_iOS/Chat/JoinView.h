//
//  JoinView.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/2/14.
//  Copyright © 2019年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JoinView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lab1;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *lab2;
@property (weak, nonatomic) IBOutlet UIImageView *imgV;

+ (instancetype)instanceView;

@end

NS_ASSUME_NONNULL_END
