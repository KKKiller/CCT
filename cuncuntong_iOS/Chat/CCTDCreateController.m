//
//  TSOrderSendingController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/3.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCTDCreateController.h"
#import "TDGroupCreateView.h"
#import "UIViewController+Extension.h"
@interface CCTDCreateController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) TDGroupCreateView *createView;
@property (nonatomic, strong) NSData *imageData;


@end

@implementation CCTDCreateController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.createView];
}

#pragma mark - 创建


- (void)getImageUrl{
    WEAKSELF
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"common/upload") params:nil target:nil imgaeData:_imageData success:^(NSDictionary *success) {
       NSString *avatarUrl = success[@"data"][@"url"];
        [weakSelf createGroup:avatarUrl];
        
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}

- (void)createGroup:(NSString *)avatarUrl{
    [[MyNetWorking sharedInstance] PostUrlNew:BASELIVEURL_WITHOBJC(@"group/create") params:@{@"admins":USERID,@"groupApply":_createView.detailTextView.text,@"groupAvatar":avatarUrl,@"groupIntro":_createView.descTextView.text,@"groupName":_createView.nameField.text, @"members":USERID, @"owner":USERID} success:^(NSDictionary *success) {
        if ([success[@"status"] integerValue] == 201) {
            SHOW(@"创建成功");
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        SHOW(@"创建失败");
        NSLog(@"%@", failure.description);
        [TOOL hideLoading:self.view];
    }];
}

- (void)create {
    [self.view endEditing:YES];
    NSString *nameStr = [_createView.nameField.text  copy];
    nameStr = [nameStr
               stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *introduceStr = _createView.descTextView.text;
    NSString *detailStr = _createView.detailTextView.text;
    
    if(!_imageData){
        SHOW(@"请选择图片");
        return;
    }
    //群组名称需要大于2位
    if ([nameStr length] == 0) {
        [self Alert:@"群组名称不能为空"];
        return;
    }
    //群组名称需要大于2个字
    else if ([nameStr length] < 2) {
        [self Alert:@"群组名称过短"];
        return;
    }
    //群组名称需要小于10个字
    else if ([nameStr length] > 15) {
        [self Alert:@"群组名称不能超过15个字"];
        return;
    }else  if ([introduceStr length] == 0) {
        [self Alert:@"群介绍不能为空"];
        return;
    }
    //群介绍至少15个字
    else if ([introduceStr length] < 15) {
        [self Alert:@"群介绍至少15个字"];
        return;
    }
    else if ([detailStr length] == 0) {
        [self Alert:@"群优势不能为空"];
        return;
    }
    [self getImageUrl];
}
#pragma mark - 选头像
- (void)chooseAvatar {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:0];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"从相册选取" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:imagePickerController animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        }];
    }];
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"拍照" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{
           
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [self presentViewController:alertController animated:YES completion:nil];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [alertController addAction:photoAction];
    } else {
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        _imageData = [TOOL imageCompress:UIImagePNGRepresentation(image)];
        _createView.headImgView.image = image;
        
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }];
    
}


- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - WCFTabBarHeight - 45 - WCFNavigationHeight)];
        _scrollView.contentSize = CGSizeMake(App_Width, 680);
    }
    return _scrollView;
}
- (TDGroupCreateView *)createView {
    if (!_createView) {
        _createView = [TDGroupCreateView instanceView];
        _createView.frame = CGRectMake(0, 0, App_Width, 680);
        [_createView.uploadImgBtn addTarget:self action:@selector(chooseAvatar) forControlEvents:UIControlEventTouchUpInside];
        [_createView.createBtn addTarget:self action:@selector(create) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _createView;
}
@end
