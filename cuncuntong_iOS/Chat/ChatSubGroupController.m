//
//  ChatSubGroupController.m
//  CunCunTong
//
//  Created by TAL on 2019/6/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "ChatSubGroupController.h"
static NSString *CellId = @"subGroupCell";

@interface ChatSubGroupController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CCEmptyView *emptyView;
@property (strong, nonatomic) UITableView *tableView;

@end

@implementation ChatSubGroupController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"子群"];
    [self setRightBtn:@"添加子群"];
    
    self.pageIndex = 1;
    [self initRefreshView];
    [self.view addSubview:self.tableView];
    [self.tableView.mj_header beginRefreshing];
}

- (void)rightBtnClick {
    
}

- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
//    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/my_ordercomplete") params:@{@"order_user":USERID,@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
//        if ([success[@"data"][@"stat"] integerValue] == 1) {
//            if(self.pageIndex == 1){
//                [self.dataArray removeAllObjects];
//            }
//            for (NSDictionary *dict in success[@"data"][@"info"]) {
////                CCOrderModel *model = [CCOrderModel modelWithJSON:dict];
////                [self.dataArray addObject: model];
//            }
//        }
//        [self endLoding:success[@"data"][@"info"]];
//    } failure:^(NSError *failure) {
//        [self endLoding:nil];
//    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO : YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (self.dataArray.count > indexPath.row) {
        
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        
//        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height -108 - 49)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 110;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, -44, 0);
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyOrderCell" bundle:nil] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
@end
