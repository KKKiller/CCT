//
//  MTEmptyView.h
//  Mentor
//
//  Created by 我是MT on 16/4/2.
//  Copyright © 2016年 馒头科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTEmptyView : UIView
@property (strong, nonatomic) UIImageView *imgView;
@property (strong, nonatomic) UILabel *tipsLbl;
@property (strong, nonatomic) UILabel *getMoreLbl;
@property (strong, nonatomic) UIButton *getMoreBtn;

@property (assign, nonatomic) BOOL isNoNet;
typedef void (^reloadBlock)(void);
typedef void (^getMoreBtnBlock)(void);

- (instancetype)setImage:(UIImage *)image tipsText:(NSString *)tipsText getMoreBtnText:(NSString *)getMoreBtnText reloadBlock:(reloadBlock)reloadBlock getMoreBtnBlock:(getMoreBtnBlock)getMoreBtnBlock;
@end
