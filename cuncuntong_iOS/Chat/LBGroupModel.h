//
//  LBGroupModel.h
//  L8
//
//  Created by 周吾昆 on 2018/6/16.
//  Copyright © 2018年 周吾昆. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LBGroupModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *group;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, assign) BOOL owner;

@end
