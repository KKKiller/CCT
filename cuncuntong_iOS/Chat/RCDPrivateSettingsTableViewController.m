//
//  RCDPrivateSettingsTableViewController.m
//  RCloudMessage
//
//  Created by Jue on 16/5/18.
//  Copyright © 2016年 RongCloud. All rights reserved.
//

#import "RCDPrivateSettingsTableViewController.h"
#import "DefaultPortraitView.h"
//#import "RCDHttpTool.h"
#import "RCDPrivateSettingsCell.h"
#import "RCDPrivateSettingsUserInfoCell.h"
#import "UIImageView+WebCache.h"
#import "RCDBaseSettingTableViewCell.h"
//#import "RCDataBaseManager.h"
#import "UIColor+RCColor.h"
#import "CCChatContactsController.h"
//#import "RCDSearchHistoryMessageController.h"
//#import "RCDSettingBaseViewController.h"
#import "RCDUserInfo.h"
#import "ComplaintsOneController.h"
#import "BRStringPickerView.h"
static NSString *CellIdentifier = @"RCDBaseSettingTableViewCell";

@interface RCDPrivateSettingsTableViewController ()

@property(strong, nonatomic) RCDUserInfo *userInfo;

@end

@implementation RCDPrivateSettingsTableViewController {
  NSString *portraitUrl;
  NSString *nickname;
  BOOL enableNotification;
  RCConversation *currentConversation;
}

+ (instancetype)privateSettingsTableViewController {
    return [[[self class] alloc]init];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self startLoadView];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 6, 87, 23);
    UIImageView *backImg = [[UIImageView alloc]
                            initWithImage:[UIImage imageNamed:@"navigator_btn_back"]];
    backImg.frame = CGRectMake(-6, 4, 10, 17);
    [backBtn addSubview:backImg];
    UILabel *backText =
    [[UILabel alloc] initWithFrame:CGRectMake(9,4, 85, 17)];
    backText.text = @"返回"; // NSLocalizedStringFromTable(@"Back",
    // @"RongCloudKit", nil);
    //   backText.font = [UIFont systemFontOfSize:17];
    [backText setBackgroundColor:[UIColor clearColor]];
    [backText setTextColor:[UIColor whiteColor]];
    [backBtn addSubview:backText];
    [backBtn addTarget:self
                action:@selector(leftBarButtonItemPressed:)
      forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton =
    [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    [self.navigationItem setLeftBarButtonItem:leftButton];
    
    UIButton *right = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    [right setTitle:@"举报" forState:UIControlStateNormal];
    [right addTarget:self action:@selector(jubao) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithCustomView:right];
    [self.navigationItem setRightBarButtonItem:rightBtn];
    
    self.title = @"设置";
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = HEXCOLOR(0xf0f0f6);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (void)jubao {
//    [BRStringPickerView showStringPickerWithTitle:@"举报内容" dataSource:@[@"发布不适当内容对我造成骚扰",@"存在欺诈骗钱行为",@"该账号对我进行骚扰",@"存在侵权行为",@"发布仿冒品信息",@"该账号存在其他违规行为"] defaultSelValue:@"发布不适当内容对我造成骚扰" isAutoSelect:NO resultBlock:^(NSString *selectValue) {
//        [TOOL showLoading:self.view];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [TOOL hideLoading:self.view];
//            SHOW(@"举报成功");
//        });
//    }];
    ComplaintsOneController *vc = [[ComplaintsOneController alloc] init];
    vc.aid = @"123";
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)leftBarButtonItemPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return section == 0 ? 1 : section == 1 ? 4 : 3;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForHeaderInSection:(NSInteger)section {
  if (section == 1 || section == 2) {
    return 20.f;
  }
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? 86 : 43;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *InfoCellIdentifier = @"RCDPrivateSettingsUserInfoCell";
    RCDPrivateSettingsUserInfoCell *infoCell =
    (RCDPrivateSettingsUserInfoCell *)[tableView
                                       dequeueReusableCellWithIdentifier:InfoCellIdentifier];
    if(!infoCell) {
        infoCell = [[RCDPrivateSettingsUserInfoCell alloc]init];
    }
    RCDBaseSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell) {
        cell = [[RCDBaseSettingTableViewCell alloc]init];
    }
    
    infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
      RCDPrivateSettingsUserInfoCell *infoCell;
      if (self.userInfo != nil) {
        portraitUrl = self.userInfo.portraitUri;
        if (self.userInfo.displayName.length > 0) {
          infoCell = [[RCDPrivateSettingsUserInfoCell alloc] initWithIsHaveDisplayName:YES];
          infoCell.NickNameLabel.text = self.userInfo.displayName;
          infoCell.displayNameLabel.text = [NSString stringWithFormat:@"昵称: %@",self.userInfo.name];
        } else {
          infoCell = [[RCDPrivateSettingsUserInfoCell alloc] initWithIsHaveDisplayName:NO];
          infoCell.NickNameLabel.text = self.userInfo.name;
        }
      } else {
        infoCell = [[RCDPrivateSettingsUserInfoCell alloc] initWithIsHaveDisplayName:NO];
        infoCell.NickNameLabel.text = @"";
        portraitUrl = @"";
      }
      if ([portraitUrl isEqualToString:@""]) {
        DefaultPortraitView *defaultPortrait = [[DefaultPortraitView alloc]
                                                initWithFrame:CGRectMake(0, 0, 100, 100)];
        [defaultPortrait setColorAndLabel:self.userId Nickname:nickname];
        UIImage *portrait = [defaultPortrait imageFromView];
        infoCell.PortraitImageView.image = portrait;
      } else {
        [infoCell.PortraitImageView
         sd_setImageWithURL:[NSURL URLWithString:portraitUrl]
         placeholderImage:[UIImage imageNamed:@"icon_person"]];
      }
      infoCell.PortraitImageView.layer.masksToBounds = YES;
      infoCell.PortraitImageView.layer.cornerRadius = 5.f;
      infoCell.PortraitImageView.contentMode = UIViewContentModeScaleAspectFill;
      infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
      return infoCell;
    }
//  if (indexPath.section == 1) {
//    RCDBaseSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if(!cell){
//      cell = [[RCDBaseSettingTableViewCell alloc]init];
//    }
//    cell.leftLabel.text = @"查找聊天记录";
//    [cell setCellStyle:DefaultStyle];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    return cell;
//  }
  if (indexPath.section == 1) {
    switch (indexPath.row) {
      case 0: {
        [cell setCellStyle:SwitchStyle];
        cell.leftLabel.text = @"消息免打扰";
        cell.switchButton.hidden = NO;
        cell.switchButton.on = !enableNotification;
        [cell.switchButton removeTarget:self
                                 action:@selector(clickIsTopBtn:)
                       forControlEvents:UIControlEventValueChanged];
        
        [cell.switchButton addTarget:self
                              action:@selector(clickNotificationBtn:)
                    forControlEvents:UIControlEventValueChanged];
        
      } break;
        
      case 1: {
        [cell setCellStyle:SwitchStyle];
        cell.leftLabel.text = @"会话置顶";
        cell.switchButton.hidden = NO;
        cell.switchButton.on = currentConversation.isTop;
        [cell.switchButton addTarget:self
                              action:@selector(clickIsTopBtn:)
                    forControlEvents:UIControlEventValueChanged];
      } break;
        
      case 2: {
        [cell setCellStyle:SwitchStyle];
        cell.leftLabel.text = @"清除聊天记录";
        cell.switchButton.hidden = YES;
      } break;
            
    case 3: {
        [cell setCellStyle:SwitchStyle];
        cell.leftLabel.text = @"加入黑名单";
        cell.switchButton.hidden = NO;
        cell.switchButton.on = [Def boolForKey:self.userId];
        [cell.switchButton addTarget:self
                              action:@selector(clickBlack:)
                    forControlEvents:UIControlEventValueChanged];
    } break;
      default:
        break;
    }
    
    return cell;
  }
  return nil;

}
- (void)clickBlack:(UISwitch *)swith {
    BOOL vv = [Def boolForKey:self.userId];
    [Def setBool:!vv forKey:self.userId];
    [self.tableView reloadData];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_rm") params:@{@"uid":USERID,@"f_uid":STR(self.userId)} target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 1) {
        }else{
            
        }
        
        for (BaseViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[CCChatContactsController class]]) {
                [self.navigationController popToViewController:vc animated:NO];
            }
        }
        SHOW(@"设置成功");
    } failure:^(NSError *failure) {
        
    }];
    
}
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//  if (indexPath.section == 1) {
      //搜索历史
//    RCDSearchHistoryMessageController *searchViewController = [[RCDSearchHistoryMessageController alloc] init];
//    searchViewController.conversationType = ConversationType_PRIVATE;
//    searchViewController.targetId = self.userId;
//    [self.navigationController pushViewController:searchViewController animated:YES];
//  }
  if (indexPath.section == 1) {
    if (indexPath.row == 2) {
      UIActionSheet *actionSheet =
          [[UIActionSheet alloc] initWithTitle:@"确定清除聊天记录？"
                                      delegate:self
                             cancelButtonTitle:@"取消"
                        destructiveButtonTitle:@"确定"
                             otherButtonTitles:nil];

      [actionSheet showInView:self.view];
      actionSheet.tag = 100;
    }
  }
}

#pragma mark -UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet
    clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (actionSheet.tag == 100) {
    if (buttonIndex == 0) {
      NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
      RCDPrivateSettingsCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
      UIActivityIndicatorView *activityIndicatorView =
      [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
      float cellWidth = cell.bounds.size.width;
      UIView *loadingView = [[UIView alloc]initWithFrame:CGRectMake(cellWidth - 50, 15, 40, 40)];
      [loadingView addSubview:activityIndicatorView];
      dispatch_async(dispatch_get_main_queue(), ^{
        [activityIndicatorView startAnimating];
        [cell addSubview:loadingView];
      });

      
      
      [[RCIMClient sharedRCIMClient]deleteMessages:ConversationType_PRIVATE targetId:_userId success:^{
        [self performSelectorOnMainThread:@selector(clearCacheAlertMessage:)
                               withObject:@"清除聊天记录成功！"
                            waitUntilDone:YES];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"ClearHistoryMsg" object:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
          [loadingView removeFromSuperview];
        });
        
      } error:^(RCErrorCode status) {
        [self performSelectorOnMainThread:@selector(clearCacheAlertMessage:)
                               withObject:@"清除聊天记录失败！"
                            waitUntilDone:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
          [loadingView removeFromSuperview];
        });
      }];
      

      [[NSNotificationCenter defaultCenter]
          postNotificationName:@"ClearHistoryMsg"
                        object:nil];
    }
  }
}

- (void)clearCacheAlertMessage:(NSString *)msg {
  UIAlertView *alertView =
  [[UIAlertView alloc] initWithTitle:nil
                             message:msg
                            delegate:nil
                   cancelButtonTitle:@"确定"
                   otherButtonTitles:nil, nil];
  [alertView show];
}

#pragma mark - 本类的私有方法
- (void)startLoadView {
  currentConversation =
      [[RCIMClient sharedRCIMClient] getConversation:ConversationType_PRIVATE
                                            targetId:self.userId];
  [[RCIMClient sharedRCIMClient]
      getConversationNotificationStatus:ConversationType_PRIVATE
      targetId:self.userId
      success:^(RCConversationNotificationStatus nStatus) {
        enableNotification = NO;
        if (nStatus == NOTIFY) {
          enableNotification = YES;
        }
        [self.tableView reloadData];
      }
      error:^(RCErrorCode status){

      }];

  [self loadUserInfo:self.userId];
}

- (void)loadUserInfo:(NSString *)userId {
    //用户信息
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/tsinfo") params:@{@"ts_uid":STR(userId)} target:nil success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            RCDUserInfo *model = [[RCDUserInfo alloc]init];
            model.name = success[@"data"][@"info"][@"realname"];
            model.portraitUri = success[@"data"][@"info"][@"portrait"];
            self.userInfo = model;
            [self.tableView reloadData];
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (void)clickNotificationBtn:(id)sender {
  UISwitch *swch = sender;
  [[RCIMClient sharedRCIMClient]
      setConversationNotificationStatus:ConversationType_PRIVATE
      targetId:self.userId
      isBlocked:swch.on
      success:^(RCConversationNotificationStatus nStatus) {
          dispatch_async(dispatch_get_main_queue(), ^{
              
              SHOW(@"设置成功");
          });
      }
      error:^(RCErrorCode status){

      }];
}

- (void)clickIsTopBtn:(id)sender {
  UISwitch *swch = sender;
  [[RCIMClient sharedRCIMClient] setConversationToTop:ConversationType_PRIVATE
                                             targetId:self.userId
                                                isTop:swch.on];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        SHOW(@"设置成功");
    });
}


@end
