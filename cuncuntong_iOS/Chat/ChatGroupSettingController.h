//
//  ChatGroupSettingController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/20.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatGroupSettingController : BaseViewController
@property (nonatomic, assign) BOOL isManager;
@property (nonatomic, strong) NSString *navTitle;
@property (nonatomic, strong) NSString *groupId;

@end

NS_ASSUME_NONNULL_END
