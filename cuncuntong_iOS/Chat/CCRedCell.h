//
//  CCRedCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/13.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

@interface CCRedCell : UIView
@property (nonatomic, strong) UIImageView *redImgView;
@property (nonatomic, strong) UIImageView *smallRed;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *seeRed;//查看红包
@property (nonatomic, strong) UILabel *bottomLbl;
@end
