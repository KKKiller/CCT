//
//  MyUtil.h
//  Demo
//
//  Created by 梅启元 on 16/8/1.
//  Copyright © 2016年 薛晓林. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MyUtil : NSObject

+ (UIButton *)createButtonFrame:(CGRect)frame
                        bgColor:(UIColor *)bgColor
                          title:(NSString *)title
                     titleColor:(UIColor *)titleColor
                        bgImage:(NSString *)bgImage
               bgHighlightImage:(NSString *)bgHighlightImage
                  bgSelectImage:(NSString *)bgSelectImage
                          image:(NSString *)image
                 highlightImage:(NSString *)highlightImage
                    selectImage:(NSString *)selectImage
                       titleFont:(CGFloat)titleFont
                         target:(id)target
                         action:(SEL)action;

+ (UILabel *)createLabelFrame:(CGRect)frame
                      bgColor:(UIColor *)bgColor
                        text:(NSString *)text
                    textColor:(UIColor *)textColor
                         font:(CGFloat)font
                textAlignment:(NSTextAlignment)textAlignment;

+ (UIImageView *)createImageViewFrame:(CGRect)frame
                                image:(NSString *)image;


+ (UIView *)createViewFrame:(CGRect)frame
                    bgColor:(UIColor *)bgColor;

+ (UITextField *)createTextFieldFrame:(CGRect)frame
                              bgColor:(UIColor *)bgcolor
                          placeholder:(NSString *)placeholder
                        textAlignment:(NSTextAlignment)textAlignment
                                 font:(CGFloat)font
                            textColor:(UIColor *)textColor;

+ (CGSize)calculateLabelText:(NSString *)text
                   sizeMarkW:(CGFloat)sizeW
                   sizeMarkH:(CGFloat)sizeH
                    textFont:(CGFloat)font;


+(NSMutableAttributedString *)adjustLineSpacingStr:(NSString *)str :(CGFloat)lineSpacing;


+ (BOOL)validateTelephoneNumber:(NSString *)number;


+ (BOOL)validateVerCode:(NSString *)verCode;

+ (BOOL)validateMoney:(NSString *)money;


@end
