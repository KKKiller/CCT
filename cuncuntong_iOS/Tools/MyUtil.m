
//
//  MyUtil.m
//  Demo
//
//  Created by 梅启元 on 16/8/1.
//  Copyright © 2016年 薛晓林. All rights reserved.
//

#import "MyUtil.h"

@implementation MyUtil

+ (UIButton *)createButtonFrame:(CGRect)frame bgColor:(UIColor *)bgColor title:(NSString *)title titleColor:(UIColor *)titleColor bgImage:(NSString *)bgImage bgHighlightImage:(NSString *)bgHighlightImage bgSelectImage:(NSString *)bgSelectImage image:(NSString *)image highlightImage:(NSString *)highlightImage selectImage:(NSString *)selectImage titleFont:(CGFloat)titleFont target:(id)target action:(SEL)action
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = frame;
    btn.backgroundColor = bgColor;
    btn.titleLabel.font = [UIFont systemFontOfSize:titleFont];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:bgImage] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:bgHighlightImage] forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[UIImage imageNamed:bgSelectImage] forState:UIControlStateSelected];
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:highlightImage] forState:UIControlStateHighlighted];
    [btn setImage:[UIImage imageNamed:selectImage] forState:UIControlStateSelected];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

+ (UILabel *)createLabelFrame:(CGRect)frame bgColor:(UIColor *)bgColor text:(NSString *)text textColor:(UIColor *)textColor font:(CGFloat)font textAlignment:(NSTextAlignment)textAlignment
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.backgroundColor = bgColor;
    label.text = text;
    label.textColor = textColor;
    label.font = [UIFont systemFontOfSize:font];
    label.textAlignment =textAlignment;
    return label;
}

+ (UIImageView *)createImageViewFrame:(CGRect)frame image:(NSString *)image
{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    imageView.image = [UIImage imageNamed:image];
    return imageView;
}

+ (UIView *)createViewFrame:(CGRect)frame bgColor:(UIColor *)bgColor
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    view.backgroundColor = bgColor;
    return view;
}

+ (UITextField *)createTextFieldFrame:(CGRect)frame bgColor:(UIColor *)bgcolor placeholder:(NSString *)placeholder textAlignment:(NSTextAlignment)textAlignment font:(CGFloat)font textColor:(UIColor *)textColor
{
    UITextField *textField = [[UITextField alloc]initWithFrame:frame];
    textField.backgroundColor = bgcolor;
    textField.placeholder = placeholder;
    textField.textAlignment = textAlignment;
    textField.font = [UIFont systemFontOfSize:font];
    textField.textColor = textColor;
    return textField;
}

+ (CGSize)calculateLabelText:(NSString *)text sizeMarkW:(CGFloat)sizeW sizeMarkH:(CGFloat)sizeH textFont:(CGFloat)font
{
    CGSize Size = [text boundingRectWithSize:CGSizeMake(sizeW, sizeH) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont  systemFontOfSize:font]} context:nil].size;
    return Size;
}
+(CGSize)contentSize:(NSString *)str font:(CGFloat)font
{
    CGSize titleSize = [str boundingRectWithSize:CGSizeMake(KEY_WIDTH, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil].size;
    return titleSize;
}
+(NSMutableAttributedString *)adjustLineSpacingStr:(NSString *)str :(CGFloat)lineSpacing
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:lineSpacing];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];

    return attributedString;
}

+ (UIAlertController *)alert:(NSString *)title titleContent:(NSString *)content mag1:(NSString *)msg1 msg2:(NSString *)msg2 Click:(void(^)())click
{
    
    UIAlertController *alert = [self alert:title titleContent:content mag1:msg1];
    
    UIAlertAction *actionB = [UIAlertAction actionWithTitle:msg2 style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        click();
    }];
    [alert addAction:actionB];
    return alert;
    
}

+ (UIAlertController *)alert:(NSString *)title titleContent:(NSString *)content mag1:(NSString *)msg1
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:content preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionA = [UIAlertAction actionWithTitle:msg1 style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:actionA];
    return alert;
}



+ (BOOL)validateTelephoneNumber:(NSString *)number
{
    NSString *passWordRegex = @"^((13[0-9])|(15[0-3,5-9])|(18[0-3,5-9])|(17[0-3,5-9]))\\d{8}$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:number];
}

+ (BOOL)validateVerCode:(NSString *)verCode
{
    NSString *passWordRegex = @"\\d{6}";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:verCode];
}

+ (BOOL)validateMoney:(NSString *)money
{
    NSString *passWordRegex = @"^(([1-9]\\d{0,9})|0)(\\.\\d{1,2})?$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:money];
}



@end
