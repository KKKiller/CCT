//
//  HFTools.m
//  HotFitness
//
//  Created by zhang_rongwu on 15/7/17.
//  Copyright (c) 2015年 HeGuangTongChen. All rights reserved.
//

#import "MTTools.h"
#import "UIView+Action.h"
#import <mach/mach.h>
#import <sys/sysctl.h>
#import <mach/mach_host.h>

#import <sys/types.h>
#import <mach/mach_host.h>
#import <mach/task_info.h>
#import <mach/task.h>
@interface MTTools ()


@end

@implementation MTTools
+ (instancetype)tool {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    return instance;
}
- (void)showLoading:(UIView *)view {
    [view makeToastActivity];
}
- (void)hideLoading:(UIView *)view {
    [view hideToastActivity];
}
- (NSString *)getPhone { //解密后的userId
    if ([Def valueForKey:@"phone"]) {
        NSString *phone = [Def valueForKey:@"phone"];
        if ([phone isKindOfClass:[NSString class]]) {
            return phone;
        }
    }
    return @"";
}
- (NSString *)getSecret { //解密后的userId
    if ([Def valueForKey:@"secret"]) {
        NSString *secret = [Def valueForKey:@"secret"];
        if ([secret isKindOfClass:[NSString class]]) {
            return secret;
        }
    }
    return @"";
}
- (NSString *)getPassword {
    if ([Def valueForKey:@"password"]) {
        NSString *password = [Def valueForKey:@"password"];
        if ([password isKindOfClass:[NSString class]]) {
            return password;
        }
    }
    return @"";
}
- (NSString *)getUserId { //解密后的userId
    if ([Def valueForKey:@"userid"]) {
        NSString *secret = [Def valueForKey:@"userid"];
        if ([secret isKindOfClass:[NSString class]]) {
            return secret;
        }
    }
    return @"1572478";//18039290408  608024
}
- (NSString *)getUserName {
    if ([Def valueForKey:@"user_name"]) {
        return [Def valueForKey:@"user_name"];
    }
    return @"村村通";
}
- (NSString *)getUserAvatar{
    if ([Def valueForKey:@"user_avatar"]) {
        return [Def valueForKey:@"user_avatar"];
    }
    return @"";
}
- (BOOL)isLogin {
    return ![SECRET isEqualToString:@""];
}

- (void)showMessage:(NSString *)msg{
    if ([TOOL stringEmpty:msg]) {
        msg = @"";
    }
    [MBProgressHUD showMessage:msg];
}

-(BOOL)stringEmpty:(NSString *)string {
    if (![string isKindOfClass:[NSString class]]) {
        return YES;
    }
    
    if (nil == string ||[string isEqualToString:@"null"]||[string isEqualToString:@"(null)"]|| [string isEqualToString:@"<null>"]|| [@"" isEqualToString:string] || string == NULL || [string isKindOfClass:[NSNull class]] ||[string isEqual:@" "]|| [[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return YES;
    }
    return NO;
}
- (BOOL)isObjEmpty:(id)obj {
    if ([obj isKindOfClass:[NSNull class]] ||[obj isEqual:@"null"]|| [obj isEqual:@"(null)"]|| obj == nil || obj == [NSNull null] || [obj isEqual:[NSNull null]] || [obj isEqual:@"<null>"]|| [obj isEqual:@""]||[obj isEqual:@" "]) {
        return YES;
    }
    return NO;
}
- (NSString *)stringNotEmpty:(NSString *)str {
    if([self stringEmpty:str]){
        return @"";
    }
    return str;
}
- (NSURL *)stringToURL:(NSString *)string{
    if ([TOOL isObjEmpty:string]) {
        return [NSURL URLWithString:@""];
    }
    if (![string containsString:@"http"]) {
        string = BASEURL_WITHOBJC(string);
    }
    return [NSURL URLWithString:string];
}

- (NSURL *)stringToLTTURL:(NSString *)string{
    
    if ([TOOL isObjEmpty:string]) {
        return [NSURL URLWithString:@""];
    }
    if (![string containsString:@"http"]) {
        string = BASEURL_WITHOBJC(string);
    }
    return [NSURL URLWithString:string];
}

#pragma mark - 缓存数据
- (void)saveDataWithData:(NSDictionary *)dict Name:(NSString *)name {
    if (!dict || !name) {
        NSAssert(NO, @"dict为空");
        return;
    }
    NSString* string =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    name =  [NSString stringWithFormat:@"%@.plist",name];
    NSString* fileName = [string  stringByAppendingPathComponent:name];
    [dict writeToFile:fileName atomically:YES];
}

- (void)saveArrayWithData:(NSArray *)array Name:(NSString *)name {
    if (!array || !name) {
        NSAssert(NO, @"dict为空");
        return;
    }
    NSString* string =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    name =  [NSString stringWithFormat:@"%@.plist",name];
    NSString* fileName = [string  stringByAppendingPathComponent:name];
    BOOL success = [array writeToFile:fileName atomically:YES];
    
}


- (NSDictionary *)getCachaDataWithName:(NSString *)name{
    NSFileManager* manager = [NSFileManager defaultManager];  //设置文件管理器
    NSString* string = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    name =  [NSString stringWithFormat:@"%@.plist",name];
    NSString* fileName = [string  stringByAppendingPathComponent:name];
    if ([manager fileExistsAtPath:fileName]) {
        NSDictionary* dic = [[NSDictionary alloc] initWithContentsOfFile:fileName];  //读取出来
        return dic;
    }
    return nil;
}
- (NSArray *)getCachaArrayWithName:(NSString *)name{
    NSFileManager* manager = [NSFileManager defaultManager];  //设置文件管理器
    NSString* string = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    name =  [NSString stringWithFormat:@"%@.plist",name];
    NSString* fileName = [string  stringByAppendingPathComponent:name];
    if ([manager fileExistsAtPath:fileName]) {
        NSArray* dic = [[NSArray alloc] initWithContentsOfFile:fileName];  //读取出来
        return dic;
    }
    return nil;
}

- (NSArray *)colorWithR1:(CGFloat)r1 g1:(CGFloat)g1 b1:(CGFloat)b1 r2:(CGFloat)r2 g2:(CGFloat)g2 b2:(CGFloat)b2 currentOffset:(CGFloat)currentOffset totlalLength:(CGFloat)length {
    CGFloat ratio = (float)currentOffset / (float)length;
    if (ratio > 1) {
        ratio = ratio - 1;
    }
    CGFloat r3 = r1 - (r1-r2)*ratio;
    CGFloat g3 = g1 - (g1 - g2)*ratio;
    CGFloat b3 = b1 - (b1 - b2)*ratio;
    CGFloat r4 = r2 + (r1-r2)*ratio;
    CGFloat g4 = g2 +  (g1 - g2)*ratio;
    CGFloat b4 = b2 + (b1 - b2)*ratio;
    return @[RGBCOLOR(r3, g3, b3),RGBCOLOR(r4, g4, b4)];
}
- (NSDateFormatter *)formatter1 {
    if (! _formatter1) {
        _formatter1 = [[NSDateFormatter alloc] init];
        _formatter1.dateFormat = @"YYYY-MM-dd HH:mm:SS";
    }
    return _formatter1;
}
- (NSString *)convertTextTime:(NSString *)time {
//    if (!time || [time isEqualToString:@"0"] || time.length < 10) {
//        return @"";
//    }
    //时间戳转NSDate
//    NSInteger timestampval = [[time substringToIndex:10] integerValue];
//    NSTimeInterval timestamp = (NSTimeInterval)timestampval;
    NSTimeInterval timestampval = 0;
    NSString *showTime;
    if (time.length == 10) {
        showTime = [self convertIntavalTime:[time doubleValue]];
        timestampval = [time doubleValue];
    }else{
        NSDate *inputDate = [self.formatter1 dateFromString:time];
        timestampval = [inputDate timeIntervalSince1970];
        showTime = [self.formatter1 stringFromDate:inputDate];

    }
    
    
    
    //输入时间按格式展示
    //获取本地时间
    NSDate * nowDate = [NSDate date];
    NSTimeInterval nowtime = [nowDate timeIntervalSince1970];
    //时间间隔
    NSTimeInterval timeInteval = nowtime - timestampval;
    NSInteger hour = timeInteval/(60 * 60);
    NSInteger minute = (timeInteval - hour*60*60)/60;
    NSInteger second = (timeInteval - hour*60*60 - minute*60);
    if (hour < 1){
        if (0< minute && minute <= 1){
            return [NSString stringWithFormat:@"%zd秒前",second];
        }else if (minute > 1){
            return [NSString stringWithFormat:@"%zd分钟前",minute];
        }else{
            return @"刚刚";
        }
    }else if (hour >= 1 && hour < 24){
        return [NSString stringWithFormat:@"%zd小时前",hour];
    }else if(hour >= 24  && hour < 24 * 30){
        return [NSString stringWithFormat:@"%zd天前",hour/24];
    }else{
        return showTime;
    }
}
- (NSString *)convertIntavalTime:(NSTimeInterval )timestamp {
//    if (!time || [time isEqualToString:@"0"] || time.length < 10) {
//        return @"";
//    }
    if (timestamp <= 0) {
        return @"";
    }
    //时间戳转NSDate
    
//    NSInteger timestampval = [[time substringToIndex:10] integerValue];
//    NSTimeInterval timestamp = (NSTimeInterval)timestampval;
    NSDate *inputDate = [NSDate dateWithTimeIntervalSince1970:timestamp];
    //输入时间按格式展示
    NSString *showTime = [self.formatter stringFromDate:inputDate];
    return showTime;
}

- (NSString *)getCurrentTimeStr {
    NSString *timeStr = [self.formatter2 stringFromDate:[NSDate date]];
    return timeStr;
}

- (NSString *)param:(NSString *)param htmlStr:(NSString *)html
{
    if (!html) {
        return nil;
    }
    NSError *error;
    NSString *regTags=[[NSString alloc] initWithFormat:@"(^|&|\\?)+%@=+([^&]*)(&|$)",param];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regTags
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *matches = [regex matchesInString:html
                                      options:0
                                        range:NSMakeRange(0, [html length])];
    for (NSTextCheckingResult *match in matches) {
        NSString *tagValue = [html substringWithRange:[match rangeAtIndex:2]];  // 分组2所对应的串
        return tagValue;
    }
    return @"";
}
- (NSData *)imageCompress:(NSData *)imgData {
    CGFloat M = 1024 * 1024;
    kLog(@"压缩前图片大小%f",(unsigned long)imgData.length / M);
    UIImage *img = [UIImage imageWithData:imgData];
    if (imgData.length > M * 0.3 && imgData.length < M) { //大于500k小于1M
        imgData = UIImageJPEGRepresentation(img, 0.8);
    }else if (imgData.length >= M  && imgData.length < M * 2){ //大于1M小于2M
        imgData = UIImageJPEGRepresentation(img, 0.3);
    }else if ( imgData.length >= M * 2 && imgData.length < M * 3){  //大于2M,小于3M
        imgData = UIImageJPEGRepresentation(img, 0.2);
    }else if (imgData.length >= M * 3 && imgData.length < M * 6){  //大于3M
        imgData = UIImageJPEGRepresentation(img, 0.2);
    }else if (imgData.length >= M * 6 && imgData.length < 10*M){  //大于6M
        imgData = UIImageJPEGRepresentation(img, 0.1);
    }else if (imgData.length >= M * 10){  //大于6M
        imgData = UIImageJPEGRepresentation(img, 0.1);
    }
    kLog(@"压缩后图片大小%f",(unsigned long)imgData.length / M);
    return imgData;
}
- (NSDateFormatter *)formatter {
    if (! _formatter) {
        _formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"YY-MM-dd HH:mm:ss";
    }
    return _formatter;
}
- (NSDateFormatter *)formatter2 {
    if (! _formatter2) {
        _formatter2 = [[NSDateFormatter alloc] init];
        _formatter2.dateFormat = @"YYYYMMddHHmmssSSS";
    }
    return _formatter2;
}
//获取html里面的图片,最大图片
- (NSArray *)imagesFromhtmlString:(NSString *)htmlStr {
//    kLog(@"%@",htmlStr);
    if ([htmlStr containsString:@"<img"]) {
        NSRegularExpression *regularExpretion = [NSRegularExpression regularExpressionWithPattern:@"src=\"(.*?)\""                                                                                   options:0                                                                                     error:nil];
        NSArray *matches = [regularExpretion matchesInString:htmlStr options:NSMatchingReportProgress range:NSMakeRange(0, htmlStr.length)];
        NSMutableArray *results = [NSMutableArray array];
        for (NSTextCheckingResult *result in matches) {
            NSRange matchRange = [result range];
            NSString *url = [htmlStr substringWithRange:matchRange];
            kLog(@"%@",url);
            url = [url substringFromIndex:5];
            url = [url substringToIndex:url.length - 1];
            if ([url containsString:@"http"]) {
                [results addObject:url];
            }
        }
        return  results.copy;
    }
    return nil;
}


//图片拼接
- (UIImage *)composeImg:(UIImage *)image {
    UIImage *logoImg = [UIImage imageNamed:@"group_logo.png"];
    
    //以1.png的图大小为底图
    UIImage *img = image;
    CGImageRef imgRef1 = img.CGImage;
    CGFloat w1 = CGImageGetWidth(imgRef1);
    CGFloat h1 = CGImageGetHeight(imgRef1);
    
    //以1.png的图大小为画布创建上下文
    UIGraphicsBeginImageContext(CGSizeMake(w1, h1));
    [img drawInRect:CGRectMake(0, 0, w1, h1)];//先把1.png 画到上下文中
    [logoImg drawInRect:CGRectMake(w1-w1*0.3, h1-h1*0.3, w1*0.25, w1*0.25)];//再把小图放在上下文中
    UIImage *resultImg = UIGraphicsGetImageFromCurrentImageContext();//从当前上下文中获得最终图片
    UIGraphicsEndImageContext();//关闭上下文
    
//    NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
//    NSString *filePath = [path stringByAppendingPathComponent:@"hongbao0.png"];
//    [UIImagePNGRepresentation(resultImg) writeToFile:filePath atomically:YES];//保存图片到沙盒
//    NSLog(@"%@",filePath);
    
//    CGImageRelease(imgRef1);
    return resultImg;
}


- (double)distanceBetweenOrderBy:(double) lat1 :(double) lat2 :(double) lng1 :(double) lng2{
    CLLocation *curLocation = [[CLLocation alloc] initWithLatitude:lat1 longitude:lng1];
    CLLocation *otherLocation = [[CLLocation alloc] initWithLatitude:lat2 longitude:lng2];
    double  distance  = [curLocation distanceFromLocation:otherLocation];
    return  distance;
}

- (UIImage *)fixOrientation:(UIImage *)aImage {
    // No-op if the orientation is already correct
    if (aImage.imageOrientation ==UIImageOrientationUp)
        return aImage;
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform =CGAffineTransformIdentity;
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width,0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width,0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height,0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx =CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                            CGImageGetBitsPerComponent(aImage.CGImage),0,
                                            CGImageGetColorSpace(aImage.CGImage),
                                            CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx,CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
        default:
            CGContextDrawImage(ctx,CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg =CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}
@end
