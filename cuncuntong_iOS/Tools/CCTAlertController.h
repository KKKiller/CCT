//
//  CCTAlertController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/31.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCTAlertController : UIViewController

typedef void(^CCTBlock)();

@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *btnConfirmBackgroundColor;
@property (nonatomic, strong) UIColor *btnCancelBackgroundColor;
@property (nonatomic, strong) UIColor *messageColor;

//+ (instancetype)sharedInstance;

- (void)alertViewControllerWithMessage:(NSString *)message block:(CCTBlock)block;

@end
