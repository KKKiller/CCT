//
//  NSMutableAttributedString+Style.h
//  HotFitness
//
//  Created by 周吾昆 on 15/12/3.
//  Copyright © 2015年 HeGuangTongChen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableAttributedString (Style)

- (instancetype)initWithText:(NSString *)text color:(UIColor *)color1 subStrIndex:(NSRange)range subStrColor:(UIColor *)color2;

-(instancetype)initWithText:(NSString *)text font:(UIFont *)font lineSpacing:(NSInteger)lineSpacing textAlignment:(NSTextAlignment)alignment;

- (instancetype)initWithText:(NSString *)text color:(UIColor *)color1 font:(UIFont *)font subStrIndex:(NSRange)range subStrColor:(UIColor *)color2 font2:(UIFont *)font2;
@end
