//
//  UIButton+HFButton.h
//  HotFitness
//
//  Created by 周吾昆 on 15/12/11.
//  Copyright © 2015年 HeGuangTongChen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (HFButton)
- (instancetype)initWithTitle:(NSString *)title textColor:(UIColor *)color backImg:(NSString *)image  font:(NSInteger)font;
@end
