//
//  UIView+Border.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "UIView+Border.h"

@implementation UIView (Border)
- (void)drawArc:(CGFloat)percent color:(UIColor *)color width:(CGFloat)width
{
    if (percent == 0 || percent > 1) {
        return;
    }
    
    if (percent == 1) {
        float endAngle =2*M_PI*percent - M_PI_2;
        UIColor *drawColor = (color == nil) ? [UIColor grayColor] : color;
        [self drawBorderWithColor:color endAngle:endAngle width:width];
    }else{
        float endAngle =2*M_PI*percent - M_PI_2;
        UIColor *drawColor = (color == nil) ? [UIColor grayColor] : color;
        [self drawBorderWithColor:color endAngle:endAngle width: width];
    }
}

-(void)drawBorderWithColor:(UIColor *)color endAngle:(CGFloat)endAngle width:(CGFloat)width
{
    //获取颜色值
    CGFloat R=0, G=0, B=0,A=1;
    const CGFloat *cs=CGColorGetComponents(color.CGColor);
    R = cs[0];
    G = cs[1];
    B = cs[2];
    A = cs[3];
    
    //起点从正上方90度位置顺时针画圆
//    self.width = self.width>0 ? self.width:width;
    CGSize viewSize = self.bounds.size;
    CGFloat radius = viewSize.width / 2 - width/2;
    CGPoint center = CGPointMake(viewSize.width / 2, viewSize.height / 2);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, R,G, B, A);
    CGContextSetLineWidth(context, width/2);
    CGContextAddArc(context, center.x, center.y, radius,-M_PI_2,endAngle, 0);
    CGContextDrawPath(context, kCGPathStroke);
}

@end
