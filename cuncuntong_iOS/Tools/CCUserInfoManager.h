//
//  CCUserInfoManager.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface CCUserInfo :NSObject<NSCoding>
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *portrait;

- (instancetype)initWithUid:(NSString *)id realname:(NSString *)realname portrait:(NSString *)portrait;

@end
typedef void(^loadFinishBlk) (CCUserInfo *info);
@interface CCUserInfoManager : NSObject
@property (nonatomic, strong) YYCache *cache;

+ (instancetype )shareInstance;
- (void)loadUserInfoWithUid:(NSString *)uid loadFinishBlk:(loadFinishBlk)finishBlk;
- (CCUserInfo *)getInfoWithUid:(NSString *)uid;
- (void)saveUserInfo:(CCUserInfo *)info;
@end

NS_ASSUME_NONNULL_END
