//
//  UIViewController+Extension.m
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/4/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "UIViewController+Extension.h"

@implementation UIViewController (Extension)
- (void)Alert:(NSString *)alertContent {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:alertContent
                                                   delegate:self
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
    [alert show];
}
@end
