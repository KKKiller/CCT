//
//  MBProgressHUD+MYMBP.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (MYMBP)

+ (MBProgressHUD *)showMessage: (NSString *)message;

@end
