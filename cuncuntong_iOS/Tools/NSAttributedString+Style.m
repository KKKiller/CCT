//
//  NSAttributedString+Style.m
//  Mentor
//
//  Created by 我是MT on 17/2/17.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import "NSAttributedString+Style.h"

@implementation NSAttributedString (Style)
- (instancetype)initWithString:(NSString *)string font:(NSInteger)font textColor:(UIColor *)color{
    if (self = [super init]) {
        if (!string) {
            string = @"";
        }
        self = [[NSAttributedString alloc]initWithString:string attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font],NSForegroundColorAttributeName : color}];
    }
    return self;
}
- (instancetype)initWithString:(NSString *)string font:(NSInteger)font textColor:(UIColor *)color lineSpacing:(CGFloat)lineSpacing{
    if (self = [super init]) {
        if (!string) {
            string = @"";
        }
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        paragraphStyle.lineSpacing = lineSpacing;
        
        self = [[NSAttributedString alloc]initWithString:string attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font],NSForegroundColorAttributeName : color,NSParagraphStyleAttributeName:paragraphStyle}];
    }
    return self;
}
@end
