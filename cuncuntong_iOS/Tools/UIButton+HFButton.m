//
//  UIButton+HFButton.m
//  HotFitness
//
//  Created by 周吾昆 on 15/12/11.
//  Copyright © 2015年 HeGuangTongChen. All rights reserved.
//

#import "UIButton+HFButton.h"

@implementation UIButton (HFButton)
- (instancetype)initWithTitle:(NSString *)title textColor:(UIColor *)color backImg:(NSString *)image  font:(NSInteger)font{
    if(self = [super init]){
        if (title) {
            [self setTitle:title forState:UIControlStateNormal];
        }
        if (color) {
            [self setTitleColor:color forState:UIControlStateNormal];
        }
        if (image) {
            [self setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
        }
        if (font) {
            self.titleLabel.font = [UIFont systemFontOfSize:font];
        }
//        self.layer.cornerRadius = 2;
        self.layer.masksToBounds = YES;
    }
    return self;
}
@end
