//
//  UILabel+HFLabel.h
//  HotFitness
//
//  Created by 周吾昆 on 15/8/13.
//  Copyright (c) 2015年 HeGuangTongChen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HFLabel)

//- (instancetype)initWithText:(NSString *)text alpha:(CGFloat)alpha font:(NSInteger)font textColor:(UIColor *)color;
- (instancetype)initWithText:(NSString *)text font:(NSInteger)font textColor:(UIColor *)color;

- (instancetype)initWithDeepLine; //深色分割线
- (instancetype)initWithShallowLine; //浅色分隔线

//文字带下划线
//- (instancetype)initWithText:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color underlineRange:(NSRange )range;
//文字带下划线,带不同颜色
//- (instancetype)initWithText:(NSString *)text font:(UIFont *)font textColor1:(UIColor *)color1  textColor2:(UIColor *)color2 differColorRange:(NSRange )range1 underlineRange:(NSRange )range2;
@end
