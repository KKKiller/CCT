//
//  CCTAlertController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/31.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "CCTAlertController.h"

@interface CCTAlertController ()

@property (nonatomic, strong) UIView *coverView;
@property (nonatomic, strong) UIView *alertView;
@property (nonatomic, copy) CCTBlock block;

@end

@implementation CCTAlertController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)alertViewControllerWithMessage:(NSString *)message block:(CCTBlock)block{
    
    self.block = block;
    
    UIView *coverView = [[UIView alloc] init];
    self.coverView = coverView;
    [self.view addSubview:coverView];
    [coverView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    coverView.backgroundColor = [UIColor blackColor];
    coverView.alpha = 0.7;
    
    UIView *alertView = [[UIView alloc] init];
    alertView.backgroundColor = self.backgroundColor;
    alertView.layer.cornerRadius = 6.0;
    alertView.backgroundColor = [UIColor whiteColor];
    self.alertView = alertView;
    [self.view addSubview:alertView];
    [alertView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(45 * KEY_RATE);
        make.right.mas_equalTo(-45 * KEY_RATE);
        make.top.mas_equalTo(135 * KEY_RATE);
        make.height.mas_equalTo(160 * KEY_RATE);
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [alertView addSubview:titleLabel];
    titleLabel.text = @"提示";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(8 * KEY_RATE);
        make.left.right.equalTo(alertView);
        make.height.mas_equalTo(45 * KEY_RATE);
    }];
    
    UILabel *messageLabel = [[UILabel alloc] init];
    messageLabel.textColor = self.messageColor;
    [alertView addSubview:messageLabel];
    messageLabel.text = message;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.numberOfLines = 0;
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(5);
        make.top.mas_equalTo(titleLabel.mas_bottom).with.offset(3);
        make.height.mas_equalTo(44);
    }];
    
    //创建确定 取消按钮
    UIButton * btnCancel = [[UIButton alloc] init];
    [alertView addSubview:btnCancel];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCancel setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancel setBackgroundColor:self.btnCancelBackgroundColor];
    [btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnCancel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(alertView);
        make.right.equalTo(alertView);
        make.width.mas_equalTo((KEY_WIDTH - 90 * KEY_RATE) / 2);
        make.height.mas_equalTo(42 * KEY_RATE);
    }];
    btnCancel.tag = 0;
    [btnCancel addTarget:self action:@selector(didClickBtnConfirm:) forControlEvents:UIControlEventTouchUpInside];
    //确定按钮
    UIButton * btnConfirm = [[UIButton alloc] init];
    btnConfirm.tag = 1;
    [alertView addSubview:btnConfirm];
    [btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnConfirm setTitle:@"确定" forState:UIControlStateNormal];
    [btnConfirm setBackgroundColor:self.btnConfirmBackgroundColor];
    [btnConfirm setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnConfirm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.equalTo(alertView);
        make.width.equalTo(btnCancel.mas_width);
        make.height.mas_equalTo(42 * KEY_RATE);
    }];
    [btnConfirm addTarget:self action:@selector(didClickBtnConfirm:) forControlEvents:UIControlEventTouchUpInside];

}

/** 点击确定 or 取消触发事件 */
-(void)didClickBtnConfirm:(UIButton *)sender{
    
    if (sender.tag == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    _block();
}

-(UIColor *)backgroundColor{
    
    if (_backgroundColor == nil) {
        _backgroundColor = [UIColor colorWithRed:249/255.0 green:249/255.0 blue:249/255.0 alpha:1];
    }
    return _backgroundColor;
}

/** 确定按钮背景色 */
-(UIColor *)btnConfirmBackgroundColor{
    
    if (_btnConfirmBackgroundColor == nil) {
        _btnConfirmBackgroundColor = [UIColor whiteColor];
    }
    return _btnConfirmBackgroundColor;
}

/** 取消按钮背景色 */
-(UIColor *)btnCancelBackgroundColor{
    
    if (_btnCancelBackgroundColor == nil) {
        _btnCancelBackgroundColor = [UIColor whiteColor];
    }
    return _btnCancelBackgroundColor;
}

/** message字体颜色 */
-(UIColor *)messageColor{
    
    if (_messageColor == nil) {
        _messageColor = [UIColor blackColor];
    }
    return _messageColor;
}

- (void)dealloc {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
