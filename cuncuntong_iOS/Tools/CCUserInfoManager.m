//
//  CCUserInfoManager.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCUserInfoManager.h"
@implementation CCUserInfo
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_portrait forKey:@"portrait"];
    [aCoder encodeObject:_realname forKey:@"realname"];
    [aCoder encodeObject:_id forKey:@"id"];
    
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        _realname = [aDecoder decodeObjectForKey:@"realname"];
        _portrait = [aDecoder decodeObjectForKey:@"portrait"];
        _id = [aDecoder decodeObjectForKey:@"id"];
        
    }
    return self;
}
- (instancetype)initWithUid:(NSString *)id realname:(nonnull NSString *)realname portrait:(nonnull NSString *)portrait {
    if (self = [super init]) {
        _id = id;
        _realname = realname;
        _portrait = portrait;
    }
    return self;
}
@end

@implementation CCUserInfoManager
+ (instancetype)shareInstance {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    return instance;
}
- (void)loadUserInfoWithUid:(NSString *)uid loadFinishBlk:(nonnull loadFinishBlk)finishBlk{
    //用户信息
    CCUserInfo *info = [self getInfoWithUid:uid];
    if (info) {
        finishBlk(info);
    }else{
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/tsinfo") params:@{@"ts_uid":STR(uid)} target:nil success:^(NSDictionary *success) {
            if ([success[@"data"][@"stat"] integerValue] == 1) {
                CCUserInfo *info = [CCUserInfo modelWithJSON:success[@"data"][@"info"]];
                if (!info) {
                    finishBlk(info);
                }
                [self saveUserInfo:info];
            }
        } failure:^(NSError *failure) {
            
        }];
    }
}
- (CCUserInfo *)getInfoWithUid:(NSString *)uid{
    return (CCUserInfo*)[self.cache objectForKey:uid] ?: nil;
}
- (void)saveUserInfo:(CCUserInfo *)info{
    [self.cache setObject:info forKey:info.id];
}
- (YYCache *)cache {
    if (!_cache) {
        _cache = [YYCache cacheWithName:@"userInfo"];
    }
    return _cache;
}
@end
