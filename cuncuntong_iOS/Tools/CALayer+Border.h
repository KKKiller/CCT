//
//  CALayer+Border.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (Border)
- (void)drawArc:(CGFloat)percent color:(UIColor *)color width:(CGFloat)width;

@end
