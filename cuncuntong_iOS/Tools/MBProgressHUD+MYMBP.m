//
//  MBProgressHUD+MYMBP.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MBProgressHUD+MYMBP.h"

@implementation MBProgressHUD (MYMBP)

+ (MBProgressHUD *)showMessage:(NSString *)message
{
    return [self showMessage:message toView:nil];
}

+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view {
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText; //提示框模式
    hud.labelText = message;
    hud.label.numberOfLines = 0;
    hud.opaque = YES;
    hud.labelFont = FFont(14);
    hud.margin = 12.f; //提示框距文字边距
    hud.animationType = MBProgressHUDAnimationZoom;
    hud.alpha = 0.8; //透明度
    hud.removeFromSuperViewOnHide = YES;
    hud.userInteractionEnabled = NO;
    [hud hide:YES afterDelay:3.5];
    return hud;
}


+ (MBProgressHUD *)showLoadingWithMessage:(NSString *)message toView:(UIView *)view {
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = message;
    hud.labelFont = [UIFont systemFontOfSize:15 * KEY_RATE];
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    hud.mode = MBProgressHUDModeText;
    // YES代表需要蒙版效果
    //hud.dimBackground = YES;
    [hud hide:YES afterDelay:2];
    
    
    return hud;
}


@end
