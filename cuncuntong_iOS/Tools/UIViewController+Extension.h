//
//  UIViewController+Extension.h
//  CunCunTong
//
//  Created by 小哥电脑 on 2019/4/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Extension)
- (void)Alert:(NSString *)alertContent;
@end

NS_ASSUME_NONNULL_END
