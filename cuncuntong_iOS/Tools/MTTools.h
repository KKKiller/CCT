//
//  HFTools.h
//  HotFitness
//
//  Created by zhang_rongwu on 15/7/17.
//  Copyright (c) 2015年 HeGuangTongChen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTools : NSObject
@property (strong, nonatomic) NSDateFormatter *formatter; //YY-MM-dd HH:mm
@property (strong, nonatomic) NSDateFormatter *formatter1; //YYYY-MM-dd HH:mm
@property (strong, nonatomic) NSDateFormatter *formatter2; //YYYYMMddHHmmssSSS

+ (instancetype)tool;

- (void)showLoading:(UIView *)view;
- (void)hideLoading:(UIView *)view;

- (BOOL)isLogin;
- (NSString *)getPhone;
- (NSString *)getPassword;
- (NSString *)getSecret;
- (NSString *)getUserId;
- (NSString *)getUserName;
- (NSString *)getUserAvatar;
//str
- (NSURL *)stringToURL:(NSString *)string;
- (NSString *)stringNotEmpty:(NSString *)str;
-(BOOL)stringEmpty:(NSString *)string;
- (BOOL)isObjEmpty:(id)obj;
- (NSURL *)stringToLTTURL:(NSString *)string;

- (void)showMessage:(NSString *)msg;
- (NSString *)convertTextTime:(NSString *)time;
- (void)saveDataWithData:(NSDictionary *)dict Name:(NSString *)name;
- (NSDictionary *)getCachaDataWithName:(NSString *)name;
- (NSArray *)getCachaArrayWithName:(NSString *)name;
- (void)saveArrayWithData:(NSArray *)array Name:(NSString *)name;
- (NSArray *)colorWithR1:(CGFloat)r1 g1:(CGFloat)g1 b1:(CGFloat)b1 r2:(CGFloat)r2 g2:(CGFloat)g2 b2:(CGFloat)b2 currentOffset:(CGFloat)currentOffset totlalLength:(CGFloat)length;
- (NSString *)getCurrentTimeStr;

- (NSString *)convertIntavalTime:(NSTimeInterval )timestamp;
- (NSString *)param:(NSString *)param htmlStr:(NSString *)html;
- (NSData *)imageCompress:(NSData *)imgData;
- (NSArray *)imagesFromhtmlString:(NSString *)htmlStr ;

- (UIImage *)composeImg:(UIImage *)image;
- (UIImage *)fixOrientation:(UIImage *)aImage;
@end

