//
//  UIImage+ImgSize.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/29.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UIImage (ImgSize)

+ (CGSize)getImageSizeWithURL:(id)URL;

@end
