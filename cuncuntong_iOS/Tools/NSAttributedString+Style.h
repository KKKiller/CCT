//
//  NSAttributedString+Style.h
//  Mentor
//
//  Created by 我是MT on 17/2/17.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (Style)
- (instancetype)initWithString:(NSString *)string font:(NSInteger)font textColor:(UIColor *)color;
- (instancetype)initWithString:(NSString *)string font:(NSInteger)font textColor:(UIColor *)color lineSpacing:(CGFloat)lineSpacing;

@end
