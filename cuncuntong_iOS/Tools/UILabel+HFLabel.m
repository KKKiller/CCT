//
//  UILabel+HFLabel.m
//  HotFitness
//
//  Created by 周吾昆 on 15/8/13.
//  Copyright (c) 2015年 HeGuangTongChen. All rights reserved.
//

#import "UILabel+HFLabel.h"

@implementation UILabel (HFLabel)


- (instancetype)initWithText:(NSString *)text alpha:(CGFloat)alpha font:(NSInteger)font textColor:(UIColor *)color {
    if(self = [super init]){
        self.text = text;
        self.alpha = alpha;
        self.numberOfLines = 0;
        [self sizeToFit];
        self.font =[UIFont systemFontOfSize:font];
        if (color != nil) {
            self.textColor = color;
        }
    }
    return self;
}

- (instancetype)initWithText:(NSString *)text font:(NSInteger)font textColor:(UIColor *)color {
    if(self = [super init]){
        self.text = text;
//        self.backgroundColor = WHITECOLOR;
//        self.layer.shouldRasterize = YES;
        self.textColor = color;
        self.numberOfLines = 0;
        [self sizeToFit];
        self.font = [UIFont systemFontOfSize:font];

    }
    return self;
}


- (instancetype)initWithText:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color underlineRange:(NSRange )range{
    if(self = [super init]){
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithString:text];
        [attrStr addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:range];
        self.font = font;
        self.textColor = color;
        self.attributedText = attrStr;
    }
    return  self;
}
- (instancetype)initWithText:(NSString *)text font:(UIFont *)font textColor1:(UIColor *)color1  textColor2:(UIColor *)color2 differColorRange:(NSRange )range1 underlineRange:(NSRange )range2{
    if(self = [super init]){
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithString:text];
        [attrStr addAttribute:NSForegroundColorAttributeName value:color1 range:range1];
        [attrStr addAttribute:NSForegroundColorAttributeName value:color2 range:range2];
        [attrStr addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:range2];
        self.font = font;
        self.attributedText = attrStr;
    }
    return  self;
}

- (instancetype)initWithDeepLine {
    if (self = [super init]) {
        self.alpha = 0.2;
        self.backgroundColor = [UIColor blackColor];
    }
    return self;
}
- (instancetype)initWithShallowLine {
    if (self = [super init]) {
        self.backgroundColor = MTRGB(0xe5e5e5);
    }
    return self;
}
@end
