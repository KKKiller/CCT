//
//  NSMutableAttributedString+Style.m
//  HotFitness
//
//  Created by 周吾昆 on 15/12/3.
//  Copyright © 2015年 HeGuangTongChen. All rights reserved.
//

#import "NSMutableAttributedString+Style.h"

@implementation NSMutableAttributedString (Style)

- (instancetype)initWithText:(NSString *)text  color:(UIColor *)color1 subStrIndex:(NSRange)range subStrColor:(UIColor *)color2 {
    if (self = [super init]) {
        self = [[NSMutableAttributedString alloc]initWithString:text];
        [self addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(0, range.location)];
        [self addAttribute:NSForegroundColorAttributeName value:color2 range:range];
    }
    return self;
}
    
- (instancetype)initWithText:(NSString *)text font:(UIFont *)font lineSpacing:(NSInteger)lineSpacing textAlignment:(NSTextAlignment)alignment {
    if (self = [super init]) {
        if (text.length > 0) {
            self = [[NSMutableAttributedString alloc]initWithString:text];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
            paragraphStyle.alignment = alignment;
            paragraphStyle.lineSpacing = lineSpacing;
            [self addAttribute:NSParagraphStyleAttributeName value:paragraphStyle
                         range:NSMakeRange(0, text.length)];
            [self addAttribute:NSFontAttributeName
                            value:font
                            range:NSMakeRange(0, text.length)];
            }
        }
    return self;
}
- (instancetype)initWithText:(NSString *)text color:(UIColor *)color1 font:(UIFont *)font subStrIndex:(NSRange)range subStrColor:(UIColor *)color2 font2:(UIFont *)font2 {
    if (self = [super init]) {
        self = [[NSMutableAttributedString alloc]initWithString:text];
        [self addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(0, self.length)];
        [self addAttribute:NSFontAttributeName value:font range:NSMakeRange(range.location, range.length)];
        [self addAttribute:NSForegroundColorAttributeName value:color2 range:range];
        [self addAttribute:NSFontAttributeName value:font2 range:range];
    }
    return self;
}

@end

/** 计算高度
NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithText:self.replyText font:FFont(15) lineSpacing:5 textAlignment:NSTextAlignmentLeft];
[attrStr addAttribute:NSForegroundColorAttributeName value:TEXTBLACK3 range:NSMakeRange(0, self.replyText.length - 1)];
h = [attrStr boundingRectWithSize:CGSizeMake(App_Width - 30, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.height;
*/
