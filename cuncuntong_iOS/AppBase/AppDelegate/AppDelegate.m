//
//  AppDelegate.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/22.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "AppDelegate.h"
#import "TabBarController.h"
#import "LoginViewController.h"
#import "DetailViewController.h"
#import "AddVillageController.h"
#import <UserNotifications/UserNotifications.h>
#import "WXApi.h"
#import "TSOrderDetailController.h"
#import "TSTransportLineontroller.h"
#import "AppDelegate+setup.h"


@interface AppDelegate ()<JPUSHRegisterDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [Def setBool:YES forKey:@"t"];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self setRootViewController];
    [self.window makeKeyAndVisible];
    [self setupSDK:launchOptions];
    [self setupLocalNotification:application];
    [self getShareBaseUrl];

    return YES;
}
- (void)getShareBaseUrl {
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"misc/shareurl") params:nil success:^(NSDictionary *success) {
        self.shareBaseUrl = success[@"data"][@"urls"][@"domain"];
        
    } failure:^(NSError *failure) {
        NSLog(@"%@",failure);
    }];
}


- (void)setRootViewController {
    self.defaultHome = [Def valueForKey:@"defaultHome"];
    if (ISLOGIN) {
        if ([self.defaultHome isEqualToString:@"生活圈"]) {
            self.shareTabbarVc = [[CCShareTabbarController alloc] init];
            self.window.rootViewController = self.shareTabbarVc;
        }else if ([self.defaultHome isEqualToString:@"货运宝"]){
            self.transportTabbarVc = [[CCTransportTabbarController alloc] init];
            self.window.rootViewController = self.transportTabbarVc;
        }else{
            self.tabbarVc = [[TabBarController alloc] init];
            self.window.rootViewController = self.tabbarVc;
        }
        [self getUserDataFinishBlock:nil];
    }else{
        LoginViewController *vc = [[LoginViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        self.window.rootViewController = nav;
    }
}
- (BOOL)isDefaultRoot {
    if (!self.defaultHome || [self.defaultHome isEqualToString:@"default"]) {
        return YES;
    }
    return NO;
}
#pragma mark - 用户信息
- (void)getUserDataFinishBlock:(getUserDataFinishBlock)finishBlock {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/secret") params:@{@"secret": SECRET} success:^(NSDictionary *success) {
        if ([success[@"data"] isKindOfClass:[NSDictionary class]]) {
            [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
            [Def setValue:[UserManager sharedInstance].realname forKey:@"user_name"];
            [Def setValue:[UserManager sharedInstance].portrait forKey:@"user_avatar"];
            NSString *role = success[@"data"][@"freight_role"];
            [UserManager sharedInstance].tsRole = [role isEqualToString:@"未开通"] ? TSRoleNormal :  [role isEqualToString:@"货主"] ? TSRoleStation : TSRoleDriver ;
            if ([UserManager sharedInstance].tsRole == TSRoleDriver) {
                [self loadTSInfo];
            }else if ([UserManager sharedInstance].tsRole == TSRoleStation){
                [self loadStationInfo];
            }
            self.transportTabbarVc.isDriver = [UserManager sharedInstance].tsRole == TSRoleDriver;
            [self registJPushTags];
            if (finishBlock) {
                finishBlock(success);
            }
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)loadTSInfo {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_owner_info") params:@{@"r_id":USERID} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            NSInteger states = [success[@"data"][@"car_owner"][@"info_state"] integerValue];
            NSString *state = [NSString stringWithFormat:@"%@", @(states)];
            [UserManager sharedInstance].tsReviewState = [state isEqualToString:@"0"] ? TSRoleReviewStateing :   [state isEqualToString:@"1"] ? TSRoleReviewStatePass : TSRoleReviewStateRefuse;
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
    
}
- (void)loadStationInfo {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_apply_owner") params:@{@"r_id":USERID} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            NSString *state = [NSString stringWithFormat:@"%@", success[@"data"][@"state"]];
            [UserManager sharedInstance].tsReviewState = [state isEqualToString:@"0"] ? TSRoleReviewStateing :   [state isEqualToString:@"1"] ? TSRoleReviewStatePass : TSRoleReviewStateRefuse;
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}

#pragma mark - 本地通知
- (void)setupLocalNotification:(UIApplication *)application {
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge categories:nil];
    [application registerUserNotificationSettings:settings];
    self.needLocalNoti = YES;
}
- (void)sendLocalNotificationWithTitle:(NSString *)title content:(NSString *)content {
    NSDictionary *userInfo = @{@"type":@"chat"};
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter * center  = [UNUserNotificationCenter currentNotificationCenter];
        UNMutableNotificationContent * notiContent = [[UNMutableNotificationContent alloc]init];
        notiContent.body = content;
        notiContent.title = title;
        notiContent.userInfo = userInfo;
        notiContent.sound = [UNNotificationSound defaultSound];
        UNTimeIntervalNotificationTrigger * tirgger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
        //建立通知请求
        UNNotificationRequest * request = [UNNotificationRequest requestWithIdentifier:@"cct" content:notiContent trigger:tirgger];
        //将建立的通知请求添加到通知中心
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        }];
    } else {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = content;
        localNotification.alertTitle = title;
        // 3.设置通知动作按钮的标题
        localNotification.alertAction = @"查看";
        // 4.设置提醒的声音
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        // 5.设置通知的 传递的userInfo
        localNotification.userInfo = userInfo;
        // 6.立即触发一个通知
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    }
}
//ios<10
//app未在运行
- (void)localNotification:(NSDictionary *) launchOptions{
    
}
// 如果App还在运行（前台or后台）
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [application setApplicationIconBadgeNumber:0];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - JPush
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // 注册APNs成功并上报DeviceToken
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}
- (void)registJPushAlias{ //登录成功后为极光推送注册用户
    NSString *userName = [NSString stringWithFormat:@"%@%@",@"VILLAGE_",USERID];
    [JPUSHService setAlias:userName callbackSelector:nil object:nil];
}
- (void)registJPushTags {
    if (self.currentCityId && [UserManager sharedInstance].tsRole == TSRoleDriver && !self.registedJpushTag) {
        NSString *tag = [NSString stringWithFormat:@"freight_%@",self.currentCityId];
        [JPUSHService setTags:[NSSet setWithObject:tag] callbackSelector:nil object:nil];
        self.registedJpushTag = YES;
    }
}
// 添加处理APNs通知回调方法
#pragma mark- JPUSHRegisterDelegate

//iOS 10+
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    //语音播报
    if ([userInfo[@"type"] isEqualToString:@"freight"]) {
        NSString *text = userInfo[@"content"];
        if (![Def boolForKey:@"muteVoice"]) {
            [self readText:text];
        }
    }
    
    completionHandler(UNNotificationPresentationOptionAlert ); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

//iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    [self receiveNotificationToJump:userInfo];//极光推送 跳转
    completionHandler();  // 系统要求执行这个方法
}

//文章通知跳转
- (void)receiveNotificationToJump:(NSDictionary *)userInfo{
    if(userInfo && [userInfo isKindOfClass:[NSDictionary class]]){
        NSDictionary *dict = userInfo;//[@"extras"]
        if (dict) {
            if ([dict[@"type"] isEqualToString:@"freight"]) {
                NSString *freight_id = [NSString stringWithFormat:@"%@", dict[@"freight_id"]];
            }else if([dict[@"type"] isEqualToString:@"chat"]){
                [App_Delegate.tabbarVc gotoChat];
            }else{
                DetailViewController *vc = [[DetailViewController alloc]init];
                vc.url = dict[@"url"];
                vc.mini = dict[@"mini"];
                vc.village = dict[@"village"];
                vc.aid = [NSString stringWithFormat:@"%@",dict[@"aid"]];
                UINavigationController *nav = self.tabbarVc.selectedViewController;
                [nav pushViewController:vc animated:YES];
            }
        }
    }
}
- (void)pushToTSOrderDetail:(NSString *)orderId userId:(NSString *)userId{
    TSOrderDetailController *vc = [[TSOrderDetailController alloc]init];
    vc.orderId = orderId;
    vc.userId = userId;
    if (App_Delegate.transportTabbarVc) {
        [App_Delegate.transportTabbarVc.navigationController pushViewController:vc animated:YES];
    }else{
        [App_Delegate.tabbarVc pushVc:vc];
    }
}






@end
#pragma mark - 同村群
//- (void)getVillageChat {
//    //首次进群给同村群发消息
//    NSString *key = [NSString stringWithFormat:@"sendVillMessage%@",USERID];
//    if (ISLOGIN && ![Def boolForKey:key]) {
//        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/myvillage") params:@{@"uid":USERID} target:nil success:^(NSDictionary *success) {
//            if ([success[@"stat"] integerValue] == 1) {
//                                [self sendMessageToVillageForFirstTime];
//            }
//        } failure:^(NSError *failure) {
//        }];
//    }
//}
//
//- (void)sendMessageToVillageForFirstTime {
//
//    NSString *village = [UserManager sharedInstance].location;
//    NSString *villageId = [NSString stringWithFormat:@"vill_%@",village];
//    NSString *text = [NSString stringWithFormat:@"%@加入了同村群",[UserManager sharedInstance].realname];
//    RCTextMessage *message = [RCTextMessage messageWithContent:text];
//    [[RCIM sharedRCIM] sendMessage:ConversationType_GROUP targetId:villageId content:message pushContent:nil pushData:nil success:^(long messageId) {
//        [Def setBool:YES forKey:[NSString stringWithFormat:@"sendVillMessage%@",USERID]];
//    } error:^(RCErrorCode nErrorCode, long messageId) {
//
//    }];
//}
//- (void)getNum {
//    //传入type字段即为设置type值
//    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/ios_set") params:@{@"r_id":@"27"} success:^(NSDictionary *success) {
//        if ([success[@"error"] integerValue] == 0) {
//            NSArray *array  = success[@"data"];
//            if (array.count > 0) {
//                NSDictionary *dict = array[0];
//                self.num = [dict[@"type"] integerValue];
//            }
//        }
//    } failure:^(NSError *failure) {
//    }];
//}
