//
//  AppDelegate.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/22.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

//+ (NSDictionary *)modelCustomPropertyMapper {
//    return @{@"articleId" : @"id"};
//}

//+ (NSDictionary *)modelContainerPropertyGenericClass {
//    return @{@"village" : [CCVillageModel class]};
//}

//¥
#import <UIKit/UIKit.h>
#import "TabBarController.h"
#import "CCShareTabbarController.h"
#import "CCBuyTabbarController.h"
#import "CCChatTabbarController.h"
#import "CCWineTabbarController.h"
#import "CCShareTabbarController.h"
#import "CCTransportTabbarController.h"
#import "CCHomeVillageModel.h"
#import "CCRedTabbarController.h"
#import <AMapLocationKit/AMapLocationKit.h>

typedef void(^finishStepUpdate)(NSInteger steps);
typedef void(^getUserDataFinishBlock)(NSDictionary *dict);

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString *defaultHome;
@property (nonatomic, strong) AMapLocationManager *locationManager;

@property (nonatomic, assign) NSInteger num;
@property (nonatomic, strong) TabBarController *tabbarVc;
@property (nonatomic, strong) CCShareTabbarController *shareTabbarVc;
@property (nonatomic, strong) CCBuyTabbarController *buyTabbarVc;
@property (nonatomic, strong) CCChatTabbarController *chatTabbarVc;
@property (nonatomic, strong) CCWineTabbarController *wineTabbarVc;
@property (nonatomic, strong) CCTransportTabbarController *transportTabbarVc;
@property (nonatomic, strong) CCRedTabbarController *redTabbarVc;

@property (nonatomic, assign) NSInteger selectedPicNum;
@property (nonatomic, assign) NSInteger maxPicNum;
@property (nonatomic, assign) NSInteger steps;
@property (nonatomic, assign) BOOL daojuPaying;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@property (nonatomic, strong) CCHomeVillageModel *villageModel;
@property (nonatomic, assign) BOOL rongInitFinished;
@property (nonatomic, strong) NSString *currentCityId;//货运通专用
@property (nonatomic, assign) BOOL registedJpushTag;
@property (nonatomic, assign) CGFloat rechargeMoney;
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *provinceId;
@property (nonatomic, strong) NSString *districtId;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *provinceName;
@property (nonatomic, strong) NSString *districtName;
@property (nonatomic, assign) BOOL needLocalNoti;
@property (nonatomic, strong) NSString *shareBaseUrl;

- (void)registJPushAlias;
- (void)registJPushTags;
- (void)setupRY;
- (void)getVillageChat;
//- (void)updateSetps:(finishStepUpdate)block;
//- (void)reportSteps:(NSInteger )step;
- (void)setRootViewController;
- (void)getUserDataFinishBlock:(getUserDataFinishBlock)finishBlock;
- (void)getCurrentCityId;
- (void)readText:(NSString *)text;
- (BOOL)isDefaultRoot;
- (void)updateMoney;
- (void)setupXMPP;
- (void)sendLocalNotificationWithTitle:(NSString *)text content:(NSString *)content;
@end





