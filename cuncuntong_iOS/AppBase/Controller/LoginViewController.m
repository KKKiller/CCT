//
//  LoginViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/23/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "LoginViewController.h"
#import "CCTTextField.h"
#import "Triangle.h"
#import "CCTButton.h"
#import "ResetViewController.h"
#import <JPUSHService.h>
#import "SetHomeViewController.h"
#import "AddVillageController.h"
#import "TabBarController.h"
#import "AppDelegate.h"
#import <IQKeyboardManager.h>
#import "BRStringPickerView.h"
@interface LoginViewController () <NSURLSessionDelegate>{
    
    Triangle *_view;
    UIScrollView *_scrollView;
    UIView *_contentView;
    UIButton *_verButton;
    CCTTextField *_userNameTF; //登录
    CCTTextField *_loginPassTF;
    CCTTextField *_mobileTF;
    
    CCTTextField *_verCodeTF; //注册
//    CCTTextField *_picCodeTF;
    CCTTextField *_passwordTF;
    CCTTextField *_invitationTF;
    NSInteger _registerStatus;
}

@property (nonatomic, strong) UIButton *returnBtn;
//@property (nonatomic, strong) UIImageView *phoneCodeImgV;
@property (nonatomic, strong) NSMutableData *phoneCodeData;
@property (nonatomic, strong) NSString *PHPSESSID;
@property (nonatomic, strong) NSHTTPCookie *cookie;
@property (nonatomic, strong) UIButton *notLoginBtn; //随便看看
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSMutableArray *countryCodeArray;

@end

@implementation LoginViewController

#pragma mark - Life Cycle
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//    [IQKeyboardManager sharedManager].enable = YES;
//    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    self.view.backgroundColor = WHITECOLOR;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
//    [IQKeyboardManager sharedManager].enable = NO;
//    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUI];
    _registerStatus = 1;
    self.countryCodeArray = [NSMutableArray array];
    [self loadCountry];
    self.tel = @"+86";
}

- (void)loadCountry {
    [[MyNetWorking sharedInstance]GetUrl:@"http://www.cct369.com/village/public/mfkit/red/getPhotNumber" params:nil success:^(NSDictionary *success) {
        if ([success[@"error"]integerValue] == 0) {
            for (NSDictionary *dict in success[@"data"]) {
                NSString *text = [NSString stringWithFormat:@"%@ +%@",dict[@"code_name"],dict[@"tel"]];
                [self.countryCodeArray addObject:text];
            }
        }
    } failure:^(NSError *failure) {
        
    }];
//    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getPhot") params:nil success:^(NSDictionary *success) {
//        if ([success[@"error"]integerValue] == 0) {
//            for (NSDictionary *dict in success[@"data"]) {
//                NSString *text = [NSString stringWithFormat:@"%@ +%@",dict[@"code_name"],dict[@"tel"]];
//                [self.countryCodeArray addObject:text];
//            }
//        }
//    } failure:^(NSError *failure) {
//
//    }];
}
#pragma mark - 登录逻辑
//手机号登录
- (void)login {
    
    if (_userNameTF.text.length == 0) {
        [MBProgressHUD showMessage:@"帐号不能为空"];
        return;
    }
    if (_loginPassTF.text.length == 0) {
        [MBProgressHUD showMessage:@"密码不能为空"];
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/login") params:@{@"mobile": _userNameTF.text, @"passwd": _loginPassTF.text,@"tel":STR(self.tel)} target:self success:^(NSDictionary *success) {
        [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
        [[NSUserDefaults standardUserDefaults] setValue:success[@"data"][@"secret"] forKey:@"secret"];
        [Def setValue:_loginPassTF.text forKey:@"password"];
        [self getUserData]; //用户信息
        
    } failure:^(NSError *   failure) {
        
    }];
}
//三方登录
- (void)thirdPartyLogin {
    CGFloat diameter = (KEY_WIDTH - (109 + 86) * KEY_RATE) / 3;
    NSArray *imgvArr = @[@"btn_weibo", @"btn_qq", @"btn_weixin"];
    for (NSInteger i = 0; i < 3; i++) {
        UIButton *thirdPartyBtn = [[UIButton alloc] init];
        [_contentView addSubview:thirdPartyBtn];
        thirdPartyBtn.hidden = YES;
        [thirdPartyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.with.offset((109 * KEY_RATE / 2) + i * (diameter + 43 * KEY_RATE));
            make.bottom.equalTo(@(-43 * KEY_RATE));
            make.height.width.equalTo([NSNumber numberWithFloat:diameter]);
        }];
        thirdPartyBtn.layer.cornerRadius = diameter/2;
        [thirdPartyBtn setImage:[UIImage imageNamed:imgvArr[i]] forState:UIControlStateNormal];
        thirdPartyBtn.tag = 20000 + i;
        [thirdPartyBtn addTarget:self action:@selector(thirdLogin:) forControlEvents:UIControlEventTouchUpInside];
        
        [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"third") params:nil success:^(NSDictionary *success) {
            if ([success[@"data"][@"flag"] isEqualToString:@"0"]) {
                thirdPartyBtn.hidden = YES;
            }else {
                thirdPartyBtn.hidden = NO;
            }
        } failure:^(NSError *failure) {
            thirdPartyBtn.hidden = NO;
        }];
        
    }
}

- (void)thirdLogin:(UIButton *)sender {
    
//    switch (sender.tag - 20000) {
//        case 0: { //微博
//            [MBProgressHUD showMessage:@"软件还未上线，无法使用"];
//        }
//            break;
//        case 1: { //QQ登录
//            {
//                UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
//                snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//                    if (response.responseCode == UMSResponseCodeSuccess) {
//
//
//                        UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQQ];
//
//                        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/qq")
//                                                        params:@{@"identify": snsAccount.usid,
//                                                                 @"realname": snsAccount.userName,
//                                                                 @"portrait": snsAccount.iconURL}
//                                                        target:self
//                                                       success:^(NSDictionary *success) {
//                                                           [[NSUserDefaults standardUserDefaults] setObject:success[@"data"][@"secret"] forKey:@"secret"];
//
//                                                           [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
//                                                           [self getUserData];
//
//                        } failure:^(NSError *failure) {
//
//                        }];
//                    }
//                });
//            }
//
//        }
//            break;
//        case 2: { //微信登录
//            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
//
//            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//
//                if (response.responseCode == UMSResponseCodeSuccess) {
//
//                    UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:snsPlatform.platformName];
//                    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/wx")
//                                                    params:@{@"identify": snsAccount.usid,
//                                                             @"realname": snsAccount.userName,
//                                                             @"portrait": snsAccount.iconURL}
//                                                    target:self
//                                                   success:^(NSDictionary *success) {
//                                                       [[NSUserDefaults standardUserDefaults] setObject:success[@"data"][@"secret"] forKey:@"secret"];
//
//                                                       [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
//                                                        [self getUserData];
//
//                                                   } failure:^(NSError *failure) {
//
//                                                   }];
//                }
//
//            });
//
//        }
//            break;
//        default:
//            break;
//    }
}

//获取用户信息
- (void)getUserData {
    NSString *secret = [Def valueForKey:@"secret"];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/secret") params:@{@"secret": secret} success:^(NSDictionary *success) {
        [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
        [Def setValue:[UserManager sharedInstance].userid forKey:@"userid"];
//        [JPUSHService setAlias:[UserManager sharedInstance].token callbackSelector:nil object:nil];
        [self selectVillage];
        [App_Delegate registJPushAlias];
        [App_Delegate getCurrentCityId];
        [App_Delegate setupRY];
//        [App_Delegate getVillageChat];
        NSString *phone = success[@"data"][@"mobile"] ?: success[@"data"][@"realname"];
        [Def setValue:phone forKey:@"phone"];
        [Def setValue:[UserManager sharedInstance].realname forKey:@"user_name"];
        [App_Delegate setupXMPP];
    } failure:^(NSError *failure) {
    }];
}

- (void)selectVillage {
    if ([[UserManager sharedInstance].isperfect isEqualToString:@"0"]) {
        [MBProgressHUD showMessage:@"必需至少选择一个村"];
        UIViewController *topVC = [UIApplication sharedApplication].keyWindow.rootViewController;
        while (topVC.presentedViewController) {
            topVC = topVC.presentedViewController;
        }
        AddVillageController *vc = [[AddVillageController alloc] init];
        vc.selectStatus = FirstLogin;
        [topVC presentViewController:vc animated:YES completion:nil];
//        return;
    }else{
        [App_Delegate getUserDataFinishBlock:nil];
            App_Delegate.tabbarVc = [[TabBarController alloc] init];
            App_Delegate.window.rootViewController = App_Delegate.tabbarVc;
    }
    
    
    
}
- (void)reg {
    NSString *url = BASEURL_WITHOBJC(@"account/register");
    [[MyNetWorking sharedInstance] PostUrl:url params:@{@"mobile": _mobileTF.text,@"tel":STR(self.tel)} target:self success:^(NSDictionary *success) {
    } failure:^(NSError *failure) {
        
    }];
}

#pragma mark - Target Action

- (void)switchStatus:(UIButton *)button {
    if (button.tag == 5000) {
        [_view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo([NSNumber numberWithFloat:KEY_WIDTH/4 - 9 * KEY_RATE]);
        }];
        [self.returnBtn removeFromSuperview];
        self.returnBtn = nil;
        _scrollView.contentOffset = CGPointZero;
        return;
    }
    if (_registerStatus == 2) {
        self.returnBtn.hidden = NO;
    }
    [_view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo([NSNumber numberWithFloat:KEY_WIDTH/4 * 3 - 9 * KEY_RATE]);
    }];
    _scrollView.contentOffset = CGPointMake(KEY_WIDTH * _registerStatus, 0);
}

- (void)reset {
    ResetViewController *vc = [[ResetViewController alloc] init];
    vc.navTitle = @"忘记密码";
    [self.navigationController pushViewController:vc animated:YES];
}


//判断图片验证码是否正确
-(void)openCountdown{
    
    if (![MyUtil validateTelephoneNumber:_mobileTF.text]) {
        [MBProgressHUD showMessage:@"请输入正确的手机号码"];
        return;
    }
//    if([TOOL stringEmpty:_picCodeTF.text]){
//        SHOW(@"请输入图片验证码");
//        return;
//    }
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *cookie = [[NSUserDefaults standardUserDefaults] objectForKey:@"photoCodeCookie"];
    [manager.requestSerializer setValue:cookie forHTTPHeaderField:@"Cookie"];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/regcode") params:@{@"mobile": _mobileTF.text,@"tel":STR(self.tel)} target:self success:^(NSDictionary *success) {//,@"piccode":STR(_picCodeTF.text)
        [MBProgressHUD showMessage:@"验证码已发送"];
        [self cutdownTimer];
    } failure:^(NSError *failure) {
    }];
}

- (void)cutdownTimer {
    __block NSInteger time = 59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮的样式
                [_verButton setTitle:@"重新发送" forState:UIControlStateNormal];
                _verButton.userInteractionEnabled = YES;
            });
        }else{
            int seconds = time % 60;
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮显示读秒效果
                [_verButton setTitle:[NSString stringWithFormat:@"重新发送(%.2d)", seconds] forState:UIControlStateNormal];
                _verButton.userInteractionEnabled = NO;
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}
#pragma mark - 验证 验证码
- (void)nextStep {
    
    if (![MyUtil validateTelephoneNumber:_mobileTF.text]) {
        [MBProgressHUD showMessage:@"请输入正确的手机号码"];
        return;
    }
//    if (_picCodeTF.text.length == 0) {
//        SHOW(@"请输入图片验证码");
//        return;
//    }
    if (![MyUtil validateVerCode:_verCodeTF.text]) {
        [MBProgressHUD showMessage:@"请输入正确的验证码"];
        return;
    }
    
    
    NSDictionary *dic = @{@"mobile": _mobileTF.text, @"code": _verCodeTF.text};
    
    _registerStatus = 2;
    
//    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/recheckcode") params:dic target: self success:^(NSDictionary *success) {
//        if ([success[@"data"][@"code"] isEqualToString:@"1"]) {
            _scrollView.contentOffset = CGPointMake(2 * KEY_WIDTH, 0);
//        }
        self.returnBtn.hidden = NO;
//    } failure:^(NSError *failure) {
//    }];
}

#pragma mark - 图片验证码
//- (void)getPhotoCode {
//    NSString *url = @"http://www.cct369.com/village/public/misc/captcha";
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
//    [[session dataTaskWithRequest:request]resume];
//}
////1.接收到服务器响应的时候调用该方法
//-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler{
//    self.phoneCodeData = [NSMutableData data];
//    NSDictionary *headers = [((NSHTTPURLResponse *)response) allHeaderFields];
//    NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:headers forURL:[NSURL URLWithString: @"http://www.cct369.com/village/public/misc/captcha"]];
//    NSDictionary* requestFields = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
//    NSString *cookie = [requestFields objectForKey:@"Cookie"] ;
//    [[NSUserDefaults standardUserDefaults] setObject:cookie forKey:@"photoCodeCookie"];
//    completionHandler(NSURLSessionResponseAllow);
//}
//
////2.接收到服务器返回数据的时候会调用该方法，如果数据较大那么该方法可能会调用多次
//-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data{
//    //拼接服务器返回的数据
//    [self.phoneCodeData appendData:data];
//}
////3.当请求完成(成功|失败)的时候会调用该方法，如果请求失败，则error有值
//-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
//    if(error == nil){
//        UIImage *img = [UIImage imageWithData:self.phoneCodeData.copy];
//        self.phoneCodeImgV.image = img;
//    }
//}

//完成,设置地址
- (void)setFinish {

    

    [self regist];
}




- (void)regist {
    NSDictionary *paramDic = @{@"mobile": _mobileTF.text, @"passwd": _passwordTF.text,@"code":STR(_verCodeTF.text),@"inviter_id":STR(_invitationTF.text)};
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/register") params:paramDic target: self success:^(NSDictionary *success) {
        
        SetHomeViewController *vc = [[SetHomeViewController alloc] init];
        vc.userName = _mobileTF.text;
        vc.passWord = _passwordTF.text;
        vc.verifyCode = _verCodeTF.text;
        //    vc.selectStatus = FirstLogin;
        vc.inviter_id = _invitationTF.text;
        //    vc.block = ^(NSMutableArray *array) {
        //    };
        [self.navigationController pushViewController:vc animated:YES];
        
        
        [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
        [JPUSHService setAlias:[UserManager sharedInstance].token callbackSelector:nil object:nil];
        
        [Def setValue:[UserManager sharedInstance].userid forKey:@"userid"];
        [Def setValue:[UserManager sharedInstance].secret forKey:@"secret"];

        TabBarController *tabbar = [[TabBarController alloc] init];
        App_Delegate.window.rootViewController = tabbar;
        [App_Delegate getUserDataFinishBlock:nil];
        
    } failure:^(NSError *failure) {
    }];
}




- (void)backward {
    _scrollView.contentOffset = CGPointMake(KEY_WIDTH, 0);
    [self.returnBtn removeFromSuperview];
    self.returnBtn = nil;
    _registerStatus = 1;
}



#pragma mark -
#pragma mark - UI

- (void)setUI {
    UIImageView *backImgv = [[UIImageView alloc] init];
    backImgv.userInteractionEnabled = YES;
    backImgv.image                  = [UIImage imageNamed:@"btn_denglu"];
    
    _view = [[Triangle alloc] init];
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.userInteractionEnabled         = YES;
    _scrollView.scrollEnabled                  = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    
    _contentView = [[UIView alloc] init];
    
    [self.view addSubview:backImgv];
    [self.view addSubview:_view];
    [self.view addSubview:_scrollView];
    [_scrollView addSubview:_contentView];
    
    [backImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(200 * KEY_RATE));
    }];
    
    [_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(backImgv.mas_bottom);
        make.left.equalTo(@(KEY_WIDTH/4-9*KEY_RATE));
        make.width.equalTo(@(18 * KEY_RATE));
        make.height.equalTo(@(12 * KEY_RATE));
    }];
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@0);
        make.top.equalTo(@(220 * KEY_RATE));
        make.height.equalTo(@(KEY_HEIGHT - 220 * KEY_RATE));
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_scrollView);// 和contentSize距离为0
        make.width.equalTo(@(3 * KEY_WIDTH));
        make.height.equalTo(_scrollView.mas_height);
    }];
    
    [self loginRegister:backImgv];
    
    [self loginConfig:backImgv];
    
    [self regStepOne:backImgv];
    
    [self thirdPartyLogin];
    
    self.returnBtn.hidden = YES;
    [self.view addSubview:self.notLoginBtn];
}

#pragma mark - 顶部UI
- (void)loginRegister:(UIImageView *)imgv {
    
    for (NSInteger i = 0; i < 2; i++) {
        UIButton *btn = [[UIButton alloc] init];
        [imgv addSubview:btn];
        [btn setTitle:@[@"登录", @"注册"][i] forState:UIControlStateNormal];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(imgv.mas_bottom).with.offset(-30 * KEY_RATE);
            make.left.with.offset(i * (KEY_WIDTH / 2));
            make.width.equalTo([NSNumber numberWithFloat:KEY_WIDTH/2]);
            make.height.equalTo(@(18 * KEY_RATE));
        }];
        btn.titleLabel.font = [UIFont systemFontOfSize:18];
        [btn addTarget:self action:@selector(switchStatus:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 5000 + i;
    }
}
- (void)countrySelect:(UIButton *)sender {
    [BRStringPickerView  showStringPickerWithTitle:@"选择国家" dataSource:self.countryCodeArray defaultSelValue:nil isAutoSelect:NO resultBlock:^(id selectValue) {
        [sender setTitle:selectValue forState:UIControlStateNormal];
        NSRange range = [selectValue rangeOfString:@"+"];
        self.tel = [selectValue substringFromIndex: range.location];
        
    }];
}


//Register Finish
- (void)regStepTwo:(UIImageView *)imgv {
    
    UIImageView *setPassImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 20 * KEY_RATE) image:@"img_mima"];
    
    UILabel *titleLabel  = [[UILabel alloc] init];
    titleLabel.text      = @"请设置您的登录密码";
    titleLabel.font      = [UIFont systemFontOfSize:14];
    titleLabel.textColor = RGB(34, 34, 34);
    
    _passwordTF             = [[CCTTextField alloc] init];
    _passwordTF.placeholder = @"6-20位，由组合字母、数字或符号";
    _passwordTF.leftView    = setPassImgv;
    
    CCTButton *finishButton = [[CCTButton alloc] init];
    [finishButton setTitle:@"完成注册并登录" forState:UIControlStateNormal];
    [finishButton addTarget:self action:@selector(setFinish) forControlEvents:UIControlEventTouchUpInside];
    
    [_contentView addSubview:titleLabel];
    [_contentView addSubview:_passwordTF];
    [_contentView addSubview:finishButton];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.equalTo(@(25*KEY_RATE + 2 * KEY_WIDTH));
        make.right.equalTo(@(-25*KEY_RATE));
        make.height.equalTo(@(40*KEY_RATE));
    }];
    
    [_passwordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom);
        make.left.equalTo(@(15 * KEY_RATE + 2 * KEY_WIDTH));
        make.right.equalTo(@(-15 * KEY_RATE));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    
    [finishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.equalTo(_passwordTF);
        make.top.equalTo(_passwordTF.mas_bottom).with.offset(36 * KEY_RATE);
    }];
    [self seperatorLine:1 textField:_passwordTF];
}
#pragma mark - 注册UI
//Register Step One
- (void)regStepOne:(UIImageView *)imgv {
    UIButton *countryCodeBtn = [[UIButton alloc]init];
    [countryCodeBtn setTitle:@"中国 +86" forState:UIControlStateNormal];
    [countryCodeBtn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
    countryCodeBtn.titleLabel.font = FFont(13);
    countryCodeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [countryCodeBtn addTarget:self action:@selector(countrySelect:) forControlEvents:UIControlEventTouchUpInside];
    [countryCodeBtn setImage:IMAGENAMED(@"arrow_down") forState:UIControlStateNormal];
    
    UIImageView *mobileImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 18.5 * KEY_RATE) image:@"img_denglu"];
    UIImageView *picImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 17 * KEY_RATE, 14 * KEY_RATE) image:@"img_duanxin"];
    UIImageView *verCodeImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 17 * KEY_RATE, 14 * KEY_RATE) image:@"img_duanxin"];
    UIImageView *invitationImgV = [MyUtil createImageViewFrame:CGRectMake(0, 0, 17 * KEY_RATE, 14 * KEY_RATE) image:@"img_duanxin"];
    
    _mobileTF             = [[CCTTextField alloc] init];
    _mobileTF.placeholder = @"请输入手机号码";
    _mobileTF.leftView    = mobileImgv;
    
    //    _picCodeTF             = [[CCTTextField alloc] init];
    //    _picCodeTF.placeholder = @"请输入图片验证码";
    //    _picCodeTF.leftView    = picImgv;
    
    _verCodeTF             = [[CCTTextField alloc] init];
    _verCodeTF.placeholder = @"请输入短信验证码";
    _verCodeTF.leftView    = verCodeImgv;
    _verCodeTF.clearButtonMode = UITextFieldViewModeNever;
    
    _invitationTF             = [[CCTTextField alloc] init];
    _invitationTF.placeholder = @"请输入邀请码(选填)";
    _invitationTF.leftView    = invitationImgV;
    _invitationTF.clearButtonMode = UITextFieldViewModeNever;
    
    
    //    self.phoneCodeImgV = [[UIImageView alloc]init];
    //    self.phoneCodeImgV.userInteractionEnabled = YES;
    //    [_contentView addSubview:self.phoneCodeImgV];
    //    [self getPhotoCode];
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
    //        [self getPhotoCode];
    //    }];
    //    [self.phoneCodeImgV addGestureRecognizer:tap];
    
    _verButton                 = [[UIButton alloc] init];
    _verButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_verButton setTitle:@"点击获取" forState:UIControlStateNormal];
    [_verButton setTitleColor:RGB(56, 126, 255) forState:UIControlStateNormal];
    [_verButton addTarget:self action:@selector(openCountdown) forControlEvents:UIControlEventTouchUpInside];
    
    CCTButton *nextStepButton = [[CCTButton alloc] init];
    [nextStepButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextStepButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
    
    [_contentView addSubview:countryCodeBtn];
    [_contentView addSubview:_mobileTF];
    //    [_contentView addSubview:_picCodeTF];
    [_contentView addSubview:_verCodeTF];
    [_contentView addSubview:_invitationTF];
    [_contentView addSubview:_verButton];
    [_contentView addSubview:nextStepButton];
    
    
    [countryCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgv.mas_bottom).with.offset(23 * KEY_RATE);
        make.left.equalTo(@(15 * KEY_RATE + KEY_WIDTH));
        make.width.equalTo(@(80));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    [_mobileTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgv.mas_bottom).with.offset(23 * KEY_RATE);
        //        make.left.equalTo(@(120 * KEY_RATE + KEY_WIDTH));
        make.left.equalTo(countryCodeBtn.mas_right).offset(4);
        //        make.left.equalTo(_contentView.mas_left).offset(50);
        make.right.equalTo(@(-15 * KEY_RATE - KEY_WIDTH));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    //    [_picCodeTF mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.equalTo(_mobileTF.mas_bottom).with.offset(1);
    //        make.left.equalTo(@(15 * KEY_RATE + KEY_WIDTH));
    //        make.right.equalTo(@(-15 * KEY_RATE -75 - KEY_WIDTH));
    //        make.height.equalTo(@(56 * KEY_RATE));
    //    }];
    [_verCodeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_mobileTF.mas_bottom).with.offset(1);
        make.left.equalTo(@(15 * KEY_RATE + KEY_WIDTH));
        make.right.equalTo(@(-15 * KEY_RATE - KEY_WIDTH));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    [_invitationTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_verCodeTF.mas_bottom).with.offset(1);
        make.left.equalTo(@(15 * KEY_RATE + KEY_WIDTH));
        make.right.equalTo(@(-15 * KEY_RATE - KEY_WIDTH));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    
    [_verButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_invitationTF.mas_right);
        make.centerY.equalTo(_verCodeTF.mas_centerY);
        make.height.equalTo(@(16 * KEY_RATE));
        make.width.equalTo(@(100 * KEY_RATE));
    }];
    //    [self.phoneCodeImgV mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.centerY.equalTo(_picCodeTF.mas_centerY);
    //        make.height.mas_equalTo(25);
    //        make.left.equalTo(_picCodeTF.mas_right).offset(10);
    //        make.width.mas_equalTo(65);
    //    }];
    [nextStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(countryCodeBtn);
        make.right.equalTo(_mobileTF);
        make.height.mas_equalTo(44);
        make.top.equalTo(_invitationTF.mas_bottom).with.offset(36 * KEY_RATE);
    }];
    [self seperatorLine:3 textField:countryCodeBtn];
    [self regStepTwo:imgv];
}

#pragma mark - 登录UI
- (void)loginConfig:(UIImageView *)imgv {
    UIButton *countryCodeBtn = [[UIButton alloc]init];
    [countryCodeBtn setTitle:@"中国 +86" forState:UIControlStateNormal];
    [countryCodeBtn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
    countryCodeBtn.titleLabel.font = FFont(13);
    countryCodeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [countryCodeBtn addTarget:self action:@selector(countrySelect:) forControlEvents:UIControlEventTouchUpInside];
    [countryCodeBtn setImage:IMAGENAMED(@"arrow_down") forState:UIControlStateNormal];
    
    UIImageView *userImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 18.5 * KEY_RATE) image:@"img_denglu"];
    UIImageView *passImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 20 * KEY_RATE) image:@"img_mima"];
    
    _userNameTF             = [[CCTTextField alloc] init];
    _userNameTF.placeholder = @"请输入手机号码/账号";
    _userNameTF.leftView    = userImgv;
    
    _loginPassTF                 = [[CCTTextField alloc] init];
    _loginPassTF.secureTextEntry = YES;
    _loginPassTF.placeholder     = @"请输入登录密码";
    _loginPassTF.leftView        = passImgv;
    _loginPassTF.clearButtonMode = UITextFieldViewModeNever;
    
    UIButton *forgetButton       = [[UIButton alloc] init];
    forgetButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [forgetButton setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgetButton setTitleColor:RGB(56, 126, 255) forState:UIControlStateNormal];
    [forgetButton addTarget:self action:@selector(reset) forControlEvents:UIControlEventTouchUpInside];
    
    CCTButton *loginButton = [[CCTButton alloc] init];
    [loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:countryCodeBtn];
    [_contentView addSubview:_userNameTF];
    [_contentView addSubview:_loginPassTF];
    [_contentView addSubview:forgetButton];
    [_contentView addSubview:loginButton];
    
    [countryCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgv.mas_bottom).with.offset(23 * KEY_RATE);
        make.left.equalTo(@(15 * KEY_RATE));
        make.width.equalTo(@(80));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    [_userNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgv.mas_bottom).with.offset(23 * KEY_RATE);
        make.left.equalTo(countryCodeBtn.mas_right).offset(4);
        make.right.equalTo(@(-15 * KEY_RATE - KEY_WIDTH));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
//    [_userNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(imgv.mas_bottom).with.offset(23 * KEY_RATE);
//        make.left.equalTo(@(15 * KEY_RATE));
//        make.right.equalTo(@(-15 * KEY_RATE - 2 * KEY_WIDTH));
//        make.height.equalTo(@(56 * KEY_RATE));
//    }];
    
    [_loginPassTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15 * KEY_RATE));
        make.right.equalTo(@(-15 * KEY_RATE - 2 * KEY_WIDTH));
        make.height.equalTo(@(56 * KEY_RATE));
        make.top.equalTo(_userNameTF.mas_bottom).with.offset(1);
    }];
    
    [forgetButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_loginPassTF.mas_right);
        make.centerY.equalTo(_loginPassTF.mas_centerY);
        make.height.equalTo(@(16 * KEY_RATE));
        make.width.equalTo(@(100 * KEY_RATE));
    }];
    
    [loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView.mas_left).offset(15);
        make.width.mas_equalTo(App_Width - 30);
//        make.right.equalTo(_userNameTF.mas_right).offset(-15);
        make.top.equalTo(_loginPassTF.mas_bottom).with.offset(36 * KEY_RATE);
        //        make.height.equalTo(_userNameTF.mas_height);
        make.height.mas_equalTo(44);
    }];
    [self seperatorLine:2 textField:countryCodeBtn];
}
- (void)seperatorLine:(NSInteger)count textField:(CCTTextField *)textField {
    for (NSInteger i = 0; i < count; i++) {
        UIView *seperatorView = [[UIView alloc] init];
        [_contentView addSubview:seperatorView];
        [seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(textField.mas_bottom).with.offset(i * (56 * KEY_RATE + 1));
            make.left.equalTo(textField.mas_left);
//            make.right.equalTo(textField.mas_right);
            make.width.mas_equalTo(App_Width - 30);
            make.height.equalTo(@1);
        }];
        seperatorView.backgroundColor = RGB(238, 238, 238);
        
    }
}
- (UIButton *)returnBtn {
    if (!_returnBtn) {
        _returnBtn = [[UIButton alloc] init];
        [self.view addSubview:_returnBtn];
        [_returnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10 * KEY_RATE);
            make.top.mas_equalTo(30 * KEY_RATE);
            make.width.height.mas_equalTo(40 * KEY_RATE);
        }];
        [_returnBtn setImage:[UIImage imageNamed:@"btn_return"] forState:UIControlStateNormal];
        [_returnBtn addTarget:self action:@selector(backward) forControlEvents:UIControlEventTouchUpInside];
    }
    return _returnBtn;
}
- (UIButton *)notLoginBtn {
    if (!_notLoginBtn) {
        _notLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake((App_Width - 100)*0.5, App_Height - 45, 100, 30)];
        [_notLoginBtn setTitle:@"随便看看" forState:UIControlStateNormal];
        [_notLoginBtn setTitleColor:MAINBLUE forState:UIControlStateNormal];
        [_notLoginBtn addTarget:self action:@selector(notLoginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _notLoginBtn;
}
- (void)notLoginBtnClick {
    App_Delegate.tabbarVc = [[TabBarController alloc] init];
    App_Delegate.window.rootViewController = App_Delegate.tabbarVc;
}
@end
