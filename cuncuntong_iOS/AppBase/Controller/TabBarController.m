//
//  TabBarController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/23/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "TabBarController.h"
#import "NavigationViewController.h"
#import "CCShareTabbarController.h"
#import "CCChatListController.h"
#import "CCChatTabbarController.h"
#import "UITabBar+badge.h"
#import "CrowdfundingViewController.h"
@interface TabBarController ()<UITabBarControllerDelegate>
@property (nonatomic, strong) NSMutableArray *tabNavs;
@property (nonatomic, assign) NSInteger lastSelectIndex;
@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.barTintColor = [UIColor whiteColor];
    [self initSelf];
    self.delegate = self;
}


- (void)initSelf {
    
    NSArray *vcArr = @[@"HomeViewController",
                       @"CrowdfundingViewController",
//                       @"CCShareController",
                       @"CCChatTabbarController",
                       @"CCShareTabbarController",
                       @"MineViewController"];
    
    NSArray *imageArr = @[@"btn_shouye0",
                          @"btn_3+N0",
//                          @"btn_share0",
                          @"btn_zanjia0",
                          @"tab_shq",
                          @"btn_wode0"];
    
    NSArray *selImageArr = @[@"btn_shouye1",
                             @"btn_3+N1",
//                             @"btn_share1",
                             @"btn_zanjia1",
                             @"tab_shq_select",
                             @"btn_wode1"];
    NSArray *titleArray = @[@"首页",@"3+N",@"聊聊",@"生活圈",@"我的"];
    self.tabNavs = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < vcArr.count; i++) {
        
        
            UIViewController *vc = [[NSClassFromString(vcArr[i]) alloc] init];
            NavigationViewController *nav = [[NavigationViewController alloc] initWithRootViewController:vc];
            nav.tabBarItem.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            nav.tabBarItem.image = [[UIImage imageNamed:imageArr[i]]
                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            nav.tabBarItem.selectedImage = [[UIImage imageNamed:selImageArr[i]]
                                            imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        nav.title = titleArray[i];
            [self.tabNavs addObject:nav];
        if ([vcArr[i]isEqualToString:@"HomeViewController"]) {
            self.homeVc = (HomeViewController *)vc;
        }
    }
    
    // 解决push/pop黑影
    self.view.backgroundColor = [UIColor whiteColor];
    self.tabBar.tintColor = MAINBLUE;
    self.viewControllers = self.tabNavs;
}
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    [self updateBadgeValueForTabBarItem];
//    if (self.selectedIndex == 1) {
//        if (!ISLOGIN) {
//            SHOW(@"请登录后查看");
//            return;
//        }
//        self.selectedIndex = self.lastSelectIndex;
//        NavigationViewController *nav = (NavigationViewController *) self.tabNavs[self.lastSelectIndex];
//        CrowdfundingViewController *vc = [[CrowdfundingViewController alloc]init];
//        [nav pushViewController:vc animated:YES];
//    }else
        if(self.selectedIndex == 2){
        if (!ISLOGIN) {
            SHOW(@"请登录后查看");
            return;
        }
            [self gotoChat];
    }else{
        self.lastSelectIndex = self.selectedIndex;
    }
}
- (void)gotoChat {
    self.selectedIndex = 2;
    self.selectedIndex = self.lastSelectIndex;
    NavigationViewController *nav = (NavigationViewController *) self.tabNavs[self.lastSelectIndex];
    CCChatTabbarController *vc = [[CCChatTabbarController alloc]init];
    [nav pushViewController:vc animated:YES];
}
- (void)pushVc:(BaseViewController *)vc {
    [self.selectedViewController pushViewController:vc animated:YES];
}
- (void)gotoHome {
    self.selectedIndex = 0;
}
- (void)gotoIndex:(NSInteger)index {
    self.selectedIndex = index;
}

- (void)updateBadgeValueForTabBarItem {
//    NSInteger friendRequestCount = [XMPPTOOL getFriendRequestArray].count;
    NSInteger unreadCount = XMPPTOOL.totalUnreadCount;
    if (unreadCount == 0) {
        [self.tabBar hideBadgeOnItemIndex:2];
    }else{
       [ self.tabBar showBadgeOnItemIndex:2 badgeValue:unreadCount tabbarCount:4];
    }
}
@end
