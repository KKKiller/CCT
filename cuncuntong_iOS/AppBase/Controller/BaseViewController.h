//
//  BaseViewController.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, assign) BOOL statusBarWhite;
- (void)addTitle:(NSString *)Title;

- (void)addReturnBtn:(NSString *)bgImg ;

- (void)hideNavLeftButton;

- (void)backMove;

- (void)setRightBtn:(NSString *)title;
- (void)setRightItemWithImagename:(NSString *)name;
- (void)setBackItemWithImagename:(NSString *)name;
- (void)backClick:(UIBarButtonItem *)item;
- (void)rightBtnClick;
- (void)rightBarButtomItemClick:(UIBarButtonItem *)item;
@end
