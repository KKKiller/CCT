//
//  ResetFinishController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/26/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "ResetFinishController.h"
#import "CCTTextField.h"
#import "CCTButton.h"
#import "AppDelegate+setup.h"
@interface ResetFinishController ()

@property (nonatomic, strong) CCTTextField *regPassTF;

@end

@implementation ResetFinishController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = _navTitle;
    UILabel *titleLabel = [[UILabel alloc] init];
    [self.view addSubview:titleLabel];
    
    titleLabel.text = @"请设置您的新密码";
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(64);
        make.left.equalTo(@(25*KEY_RATE));
        make.right.equalTo(@(-25*KEY_RATE));
        make.height.equalTo(@(40*KEY_RATE));
    }];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = RGB(34, 34, 34);
    
    
    _regPassTF = [[CCTTextField alloc] init];
    [self.view addSubview:_regPassTF];
    [_regPassTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom);
        make.left.equalTo(@(15 * KEY_RATE));
        make.right.equalTo(@(-15 * KEY_RATE));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    _regPassTF.placeholder = @"6-20位，由组合字母、数字或符号";
    
    UIImageView *setPassImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 20 * KEY_RATE) image:@"img_mima"];
    _regPassTF.leftView = setPassImgv;
    
    UIView *seperatorView = [[UIView alloc] init];
    [self.view addSubview:seperatorView];
    [seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_regPassTF.mas_bottom);
        make.left.equalTo(_regPassTF.mas_left);
        make.right.equalTo(_regPassTF.mas_right);
        make.height.equalTo(@1);
    }];
    seperatorView.backgroundColor = RGB(238, 238, 238);
    
    CCTButton *finishButton = [[CCTButton alloc] init];
    [self.view addSubview:finishButton];
    
    [finishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_regPassTF);
        make.height.mas_equalTo(44);
        make.top.equalTo(_regPassTF.mas_bottom).with.offset(36 * KEY_RATE);
    }];
    [finishButton setTitle:@"完成 " forState:UIControlStateNormal];

    [finishButton addTarget:self action:@selector(resetFinish) forControlEvents:UIControlEventTouchUpInside];
    
    [self navConfig];
}

- (void)resetFinish {
    if (_regPassTF.text.length < 6) {
        [MBProgressHUD showMessage:@"新密码不能少于6位"];
        return;
    }
    
    NSString *trimMobile = [_mobileString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *trimPass = [_regPassTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *reverseMobile = [self reverse:trimMobile];
    
    NSString *md5Pass   = [MD5UTils getmd5WithString:trimPass];
    NSString *verify = [NSString stringWithFormat:@"%@%@",reverseMobile, md5Pass];
    
    NSString *md5Verify = [MD5UTils getmd5WithString:verify];
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/retrieve")
                                    params:@{@"mobile": _mobileString, @"newpass": _regPassTF.text, @"verify": md5Verify.uppercaseString,@"code":STR(self.code),@"tle":STR([UserManager sharedInstance].tel)}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       [MBProgressHUD showMessage:@"修改成功"];
                                       [App_Delegate resetXMPP];
                                       [self.navigationController popToRootViewControllerAnimated:YES];
    } failure:^(NSError *failure) {
        
    }];
    
}

- (NSMutableString *)reverse:(NSString *)str {
    NSUInteger length = [str length];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:length];
    
    for (long i = length - 1; i >= 0; i--) {
        unichar c = [str characterAtIndex:i];
        [array addObject:[NSString stringWithFormat:@"%c",c]];
    }
    
    NSMutableString *mstr = [NSMutableString stringWithCapacity:length];
    
    for (int i = 0; i <= length - 1; i++) {
        [mstr appendString:array[i]];
    }
    return mstr;
}


- (void)navConfig {
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame = CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE);
    [backButton setImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backItem;
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.view.backgroundColor = WHITECOLOR;
}

#pragma mark - Target Action

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
