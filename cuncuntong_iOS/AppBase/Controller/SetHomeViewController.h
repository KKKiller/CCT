//
//  SetHomeViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 2016/10/12.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCPositionModel.h"
//选择地址
typedef NS_ENUM(NSInteger, SelectStatus) {
    FirstLogin,
    NotFirstLogin
};



typedef void(^selectPositionBlk)(CCPositionModel *positionModel);

@interface SetHomeViewController : BaseViewController

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *passWord;
@property (nonatomic, strong) NSString *verifyCode;
@property (nonatomic, strong) NSString *inviter_id;

@property (nonatomic, assign) BOOL selectPosition;
@property (nonatomic, strong) selectPositionBlk selectPositionBlk;

@end
