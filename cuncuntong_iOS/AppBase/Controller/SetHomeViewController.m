//
//  SetHomeViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 2016/10/12.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "SetHomeViewController.h"
#import <FMDB.h>
#import "PinYin4Objc.h"
#import "AddTitleView.h"
#import "SectionHeader.h"
#import "FollowVillageView.h"
#import "FollowVillageModel.h"
#import "CCTTextField.h"
#import "TabBarController.h"
#import <JPUSHService.h>
#import "AddVillageController.h"
#import "AppDelegate.h"

@interface SetHomeViewController ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, AddVillageDelegate, FollowDeleteDelegate> {//, UITextFieldDelegate
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    FMDatabase *_dataBase;
    NSString *_currentTitle;
    AddTitleView *_titleView;
    FollowVillageView *_followVillageView;
    BOOL _isSearch;

}

@property (nonatomic, strong) NSMutableDictionary * dictNames;
@property (nonatomic, strong) NSMutableDictionary * dicNameId;

@property (nonatomic, strong) NSArray *arrayIndexTitles;
@property (nonatomic, strong) NSMutableSet *identifySet;


@property (nonatomic, strong) UIView *coverView;

@property (nonatomic, strong) UITableView *secondTableView;

@property (nonatomic, strong) NSMutableArray *secondaryTitleArray;
@property(nonatomic,strong) NSIndexPath *secondSelectIndexPath;


@property (nonatomic, strong) SectionHeader *sectionHeader;

@property (nonatomic, strong) NSMutableArray *lastResultArray;

@property (nonatomic, strong) NSMutableArray *searchArray;

@property (nonatomic, strong) NSString *addressStr;

@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSMutableArray *followVillageArray;

@property (nonatomic, assign) NSInteger selectStep;
@property (nonatomic, strong) CCPositionModel *positonModel;

@end

static NSString *const mainCellIdentifier = @"mainCellIdentifier";

static NSString *const subCellIdentifier = @"subCellIdentifier";


@implementation SetHomeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BACKGRAY;
    self.addressStr = @"";
    self.positonModel = [[CCPositionModel alloc]init];
    
    [self uiConfig];
    [self getAreaByPid:@"0"];

    
}


- (void)uiConfig {
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"位置"];
    UIButton *sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    sureBtn.titleLabel.font = FontSize(14);
    [sureBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(addVillage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:sureBtn];
    self.navigationItem.rightBarButtonItem = right;
    CGFloat height = self.followVillageArray.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
    
    if (!self.selectPosition) {
        _titleView = [[AddTitleView alloc] init:self.followVillageArray.count];
        _titleView.delegate = self;
        [self.view addSubview:_titleView];
        [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self.view);
            make.height.mas_equalTo(64);
        }];
        [_titleView reloadTitleLabel:@""];
        
        _followVillageView = [[FollowVillageView alloc] initWithArray:self.followVillageArray];
        [self.view addSubview:_followVillageView];
        [_followVillageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            
            make.top.equalTo(@64);
            make.height.mas_equalTo(height);
        }];
        _followVillageView.delegate = self;
    }
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    if (self.selectPosition) {
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.view.mas_top).offset(64);
            make.height.mas_equalTo(KEY_HEIGHT -64 );
        }];
    }else{
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(_followVillageView.mas_bottom);
            make.height.mas_equalTo(KEY_HEIGHT - 64  - height);//50 * KEY_RATE
        }];
    }
    
    
    
    _tableView.sectionIndexColor = [UIColor lightGrayColor];
}


-(void)getAreaByPid:(NSString *)pid{
    if (![pid isKindOfClass:[NSString class]]) {
        pid = [NSString stringWithFormat:@"%@",pid];
    }
    NSDictionary *parDic = @{@"pid" : pid};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"main/area") params:parDic success:^(NSDictionary *success) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"success = %@",success);
        if ([[success objectForKey:@"data"]isKindOfClass:[NSDictionary class]]) {
            NSDictionary *data = [success objectForKey:@"data"];
            if ([[data objectForKey:@"village"]isKindOfClass:[NSArray class]]) {
                NSArray *village = [data objectForKey:@"village"];
                if ([pid integerValue] == 0 ) {
                    [self setDataByAreaArray:village];
                }else{
                    if (village.count == 0) {
                        if (self.selectPosition) {
                            if (self.selectPositionBlk) {
                                self.selectPositionBlk(self.positonModel);
                            }
                            if (self.navigationController) {
                                [self.navigationController popViewControllerAnimated:YES];
                            }else{
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }
                        }else{
                            [self chooseAVillageWithVillageId:pid villageName:_currentTitle];
                        }
                    }else{
                        self.secondaryTitleArray = [NSMutableArray arrayWithArray:village];
                        [_secondTableView reloadData];
                    }
                }
            }
        }
    } failure:^(NSError *failure) {
        NSLog(@"failure = %@",failure);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
-(void)setDataByAreaArray:(NSArray *)array{
    
    self.dictNames = [NSMutableDictionary dictionary];
    
    HanyuPinyinOutputFormat *outputFormat=[[HanyuPinyinOutputFormat alloc] init];
    [outputFormat setToneType:ToneTypeWithoutTone];
    [outputFormat setVCharType:VCharTypeWithUUnicode];
    [outputFormat setCaseType:CaseTypeUppercase];
    for (NSDictionary *na in array) {
        //        NSLog(@"%@",[na objectForKey:@"name"]);
        NSString *name = [NSString stringWithFormat:@"%@",[na objectForKey:@"name"]];
        
        NSString *pinyin = [[PinyinHelper getFirstHanyuPinyinStringWithChar:[name characterAtIndex:0] withHanyuPinyinOutputFormat:outputFormat] substringWithRange:NSMakeRange(0, 1)];
        
        NSMutableArray *array = [self.dictNames objectForKey:pinyin];
        
        if (array == nil) { // 键值对还不存在
            // 创建数组，保存人名
            NSMutableArray *array = [NSMutableArray arrayWithObject:name];
            // 往字典中添加一个键值对
            [self.dictNames setObject:array forKey:pinyin];
            
        } else
        {  // 键值对已经存在
            [array addObject:name];
        }
        
        NSArray *keyArrays = [self.dictNames allKeys];
        //        NSLog(@"count = %li",keyArrays.count);
        self.arrayIndexTitles = [keyArrays sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        //        NSLog(@"arrayIndexTitles = %@",self.arrayIndexTitles);
        
        self.dicNameId[name] = [na objectForKey:@"id"];
    }
    
    [_tableView reloadData];
    
}
#pragma mark - UITableViewDelegate && UITableViewDataSource
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _tableView) {
        if (_isSearch) {
            return 1;
        }
        return  self.arrayIndexTitles.count;// 15
    }
    return 1;// 1
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableView) {
        if (_isSearch) {
            return self.searchArray.count;
        }
        NSString *key = [self.arrayIndexTitles objectAtIndex:section];    // 取到section分组显示的索引名称
        return [[self.dictNames objectForKey:key] count];    // 返回该分组的人的个数
    }
    return self.secondaryTitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView == _tableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:mainCellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:mainCellIdentifier];
        }
        if (_isSearch) {
            cell.textLabel.text = self.searchArray[indexPath.row];
        }else {
            NSString *firstChar = [self.arrayIndexTitles objectAtIndex:indexPath.section];
            
            // 取到首字母对应的人名数组
            NSArray *arrayNames = [self.dictNames objectForKey:firstChar];
            
            // 取到人名，设置在cell上显示
            cell.textLabel.text = [arrayNames objectAtIndex:indexPath.row];
        }
        return cell;
        
        
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:subCellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:subCellIdentifier];
        }
        //        cell.textLabel.text = self.secondaryTitleArray[indexPath.row][1];
        
        NSDictionary *dictionary = [self.secondaryTitleArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [dictionary objectForKey:@"name"];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:15 * KEY_RATE];
        return cell;
    }
    
    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == _tableView) {
        return self.arrayIndexTitles;
    }
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == _tableView) {
        if (_isSearch) {
            return @"";
        }
        return  [self.arrayIndexTitles objectAtIndex:section];
    }
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45 * KEY_RATE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == _tableView) {
        return  35 * KEY_RATE;
    }
    return 45 * KEY_RATE;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView != _secondTableView) {
        return nil;
    }
    [self.sectionHeader titleLabelConfig:_currentTitle];
    return self.sectionHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == _tableView) {
        [self.secondaryTitleArray removeAllObjects];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(remove)];
        tap.delegate = self;
        [self.coverView addGestureRecognizer:tap];
        
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        _currentTitle = cell.textLabel.text;
        self.province = cell.textLabel.text;
        self.selectStep = 1;
        self.positonModel.provinceName = self.province;
        NSString *provinceId = [NSString stringWithFormat:@"%@",[self.dicNameId objectForKey:_currentTitle]];
        self.positonModel.provinceId = provinceId;
        [self getAreaByPid:provinceId];
    } else {
        self.secondSelectIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        _currentTitle = cell.textLabel.text;
        NSDictionary *dic = [self.secondaryTitleArray objectAtIndex:indexPath.row];
        NSString *locationId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
        if (self.selectStep == 1) {
            self.positonModel.cityId = locationId;
            self.positonModel.cityName = cell.textLabel.text;
            if ([self.positonModel.cityName isEqualToString:@"市辖区"]) {
                self.positonModel.cityName = @"北京市";
            }
        }else if (self.selectStep == 2){
            self.positonModel.districtId = locationId;
            self.positonModel.districtName = cell.textLabel.text;
        }
        self.addressStr = [self.addressStr stringByAppendingString:_currentTitle];
        ++self.selectStep;
        if (self.selectStep == 3 && self.selectPosition) {
            if (self.selectPositionBlk) {
                self.selectPositionBlk(self.positonModel);
            }
            if (self.navigationController) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }else{
            [self getAreaByPid:locationId];
        }
        
    }
}

#pragma mark - Lazy Initialization

- (UIView *)coverView {
    if (!_coverView) {
        _coverView = [[UIView alloc] init];
        [self.view addSubview:_coverView];
        [_coverView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _coverView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.secondTableView.hidden = NO;
    }
    return _coverView;
}

- (UITableView *)secondTableView {
    if (!_secondTableView) {
        _secondTableView = [[UITableView alloc] init];
        [self.coverView addSubview:_secondTableView];
        [_secondTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(17 * KEY_RATE);
            make.left.mas_equalTo(15 * KEY_RATE);
            make.right.mas_equalTo(-15 * KEY_RATE);
            make.height.mas_equalTo(540 * KEY_RATE);
        }];
        _secondTableView.delegate = self;
        _secondTableView.dataSource = self;
    }
    return _secondTableView;
}

- (NSMutableDictionary *)dicNameId {
    if (!_dicNameId) {
        _dicNameId = [[NSMutableDictionary alloc] init];
    }
    return _dicNameId;
}

- (NSMutableArray *)secondaryTitleArray {
    if (!_secondaryTitleArray) {
        _secondaryTitleArray = [[NSMutableArray alloc] init];
    }
    return _secondaryTitleArray;
}

- (SectionHeader *)sectionHeader {
    if (!_sectionHeader) {
        _sectionHeader = [[SectionHeader alloc]
                          initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 45 * KEY_RATE)];
    }
    return _sectionHeader;
}


- (void)remove {
    [_coverView removeFromSuperview];
    _coverView = nil;
    _secondTableView = nil;
}

- (NSMutableArray *)followVillageArray {
    
    if (!_followVillageArray) {
        _followVillageArray = [[NSMutableArray alloc] init];
    }
    return _followVillageArray;
}

- (NSMutableArray *)searchArray {
    if (!_searchArray) {
        _searchArray = [[NSMutableArray alloc] init];
    }
    return _searchArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.secondTableView]) {
        return NO;
    }
    return YES;
}

#pragma mark - Target Action
- (void)chooseAVillageWithVillageId:(NSString *)villageId villageName:(NSString *)villageName{
    [self remove];
    if (self.followVillageArray.count == 1) {
        [MBProgressHUD showMessage:@"最多可以添加一个家乡"];
        return;
    }

    FollowVillageModel *model = [[FollowVillageModel alloc] init];
//    model.villageId = _lastResultArray[indexPath.row][2];
//    model.val = _lastResultArray[indexPath.row][1];
    model.villageId = villageId;
    model.val = villageName;
    for (FollowVillageModel *m in self.followVillageArray) {
        if ([m.villageId isEqualToString:model.villageId]) {
            [MBProgressHUD showMessage:@"不能重复添加"];
            return;
        }
    }

    [self.followVillageArray addObject:model];
    [_titleView reloadTitleLabel:model.val];

    CGFloat height =self.followVillageArray.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
    [_followVillageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
    }];

    [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(KEY_HEIGHT - 64  - height);
    }];
    
    [_followVillageView reloadSelf:self.followVillageArray];
}
- (void)addVillage {
    
    if (self.followVillageArray.count == 0) {
        [MBProgressHUD showMessage:@"必须选择一个家乡"];
        return;
    }
    
    NSDictionary *paramDic = @{@"mobile": _userName, @"passwd": _passWord,@"code":STR(self.verifyCode),@"inviter_id":STR(self.inviter_id), @"location": [self.followVillageArray[0] villageId]};
    
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/register") params:paramDic target: self success:^(NSDictionary *success) {
            
            [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
            [JPUSHService setAlias:[UserManager sharedInstance].token callbackSelector:nil object:nil];
            
            [Def setValue:[UserManager sharedInstance].userid forKey:@"userid"];
            [Def setValue:[UserManager sharedInstance].secret forKey:@"secret"];
            [self selectVillage];
    
        } failure:^(NSError *failure) {
        }];
    
}

- (void)selectVillage {
    if ([[UserManager sharedInstance].isperfect isEqualToString:@"0"]) {
        [MBProgressHUD showMessage:@"必需至少选择一个村"];
        UIViewController *topVC = [UIApplication sharedApplication].keyWindow.rootViewController;
        while (topVC.presentedViewController) {
            topVC = topVC.presentedViewController;
        }
        AddVillageController *vc = [[AddVillageController alloc] init];
//        vc.selectStatus = FirstLogin;
        [topVC presentViewController:vc animated:YES completion:nil];
    }else{
        TabBarController *tabbar = [[TabBarController alloc] init];
        App_Delegate.window.rootViewController = tabbar;
    }
    
    
    
}

- (void)deleteFollow:(NSInteger )x {
    
    CCTAlertController *vc = [[CCTAlertController alloc]init];
    vc.messageColor = [UIColor redColor];
    [vc alertViewControllerWithMessage:@"确认要移除么?" block:^{
        
        [self.followVillageArray removeObjectAtIndex:x];
        
        [_titleView reloadTitleLabel:@""];
        
        CGFloat height = self.followVillageArray.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
        [_followVillageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
        }];
        [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(KEY_HEIGHT - 64 - 50 * KEY_RATE - height);
        }];
        
        [_followVillageView reloadSelf:self.followVillageArray];
    }];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
