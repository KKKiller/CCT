//
//  CCBaseWebviewController.m
//  CunCunTong
//
//  Created by 我是MT on 2017/5/31.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBaseWebviewController.h"
#import <WebKit/WebKit.h>
#import "ShareListView.h"
#import "RechargeViewController.h"
#import "CCShareDetailController.h"
#import "CCExchangeController.h"
#import "TSVerifyController.h"
#import "CCShareView.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "RedDetailController.h"
#import "RedCityHomeController.h"
#import "CCEnergyController.h"
@interface CCBaseWebviewController ()<ShareListViewDelegate,UIWebViewDelegate,CNContactPickerDelegate,MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) UIWebView *webview;
@property (nonatomic, strong) UIView *bannerView;
@property(nonatomic,strong)UIProgressView *progressView;
@property(nonatomic,strong)ShareListView *rightShareView;
@property (nonatomic, strong) NSString *share_url;
@property (nonatomic, strong) CCShareView *shareView;

@end

@implementation CCBaseWebviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setRequest];
    [self loadShareUrl];
    self.view.backgroundColor = WHITECOLOR;
    [self.view addSubview:self.shareView];
}

- (void)setNav {
    self.navigationController.navigationBarHidden = self.hiddenNav;
    self.bannerView.hidden = !self.hiddenNav;
    
    [self addReturnBtn:@"tui_"];
    [self addTitle:self.titleStr];
    if (!self.hiddenShare) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"xialacaidan"] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemClick)];
        
    }
    
}
- (void)setRequest {
    NSURL *url = [NSURL URLWithString:self.urlStr];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    [self.webview loadRequest:request];
    [TOOL showLoading:self.webview];
    NSLog(@"webView-URL%@",url);
}
- (void)loadShareUrl {
    if (self.shareURL) {
        self.share_url = self.shareURL;
    }else{
        [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"misc/shareurl") params:nil success:^(NSDictionary *success) {
            NSLog(@"%@",success);
            NSString *baseUrl = success[@"data"][@"urls"][@"domain"];
            NSString *p = [TOOL param:@"p" htmlStr:self.urlStr];
            self.share_url =  [NSString stringWithFormat:@"%@wxman/url.php?id=%@&rid=%@",baseUrl,p,USERID];
            NSLog(@"%@",self.share_url);
        } failure:^(NSError *failure) {
            NSLog(@"%@",failure);
        }];
    }
    
}

#pragma mark - 代理方法

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *urlStr = request.URL.absoluteString;
    kLog(@"%@",urlStr);
    if ([urlStr isEqualToString:@"http://www.cct369.com/village/public/adminh5/index#"]) {
        [super backMove];
        return NO;
    }
    //union://doshare?title=&description=&url=&thumb=
    if ([urlStr containsString:@"union://doshare"]) { //分享
        self.share_title =  [[TOOL param:@"title" htmlStr:urlStr] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        self.share_url =  [[TOOL param:@"url" htmlStr:urlStr] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        self.share_image =  [[TOOL param:@"thumb" htmlStr:urlStr] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [self rightBarButtonItemClick];
        return NO;
    }
    //招贤纳士
    if ([urlStr containsString:@"beans_share://detail"]) { //分享
//        NSString *idStr = [[TOOL param:@"id" htmlStr:urlStr] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        //设置网页地址
//        self.share_title =  @"邀你种金豆！";
//        self.share_url =  [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
//        self.share_image =  [[TOOL param:@"thumb" htmlStr:urlStr] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [self rightBarButtonItemClick];
        CCEnergyController *vc = [[CCEnergyController alloc]init];
        vc.showShare = YES;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    if ([urlStr containsString:@"creditsexchange://detail"]) { //信用分享
        NSString *idStr = [[TOOL param:@"id" htmlStr:urlStr] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //积分兑换
        CCExchangeController *vc = [[CCExchangeController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    //"account://save?mobile=xxx&password=xxx"
    if ([urlStr containsString:@"account://save"]) { //账户分享
        NSString *password =  [TOOL param:@"passwd" htmlStr:urlStr];
        [Def setValue:password forKey:@"managerPasswds"];
        [Def setValue:[TOOL param:@"w_group" htmlStr:urlStr] forKey:@"w_group"];
        NSURL *newURL = [NSURL URLWithString:@"http://www.cct369.com/village/public/adminh5/index"];
        [webView loadRequest:[NSURLRequest requestWithURL:newURL]];
        return NO;
    }
    //account://goback
    if ([urlStr containsString:@"account://goback"] || [urlStr containsString:@"account://gotoapp"]) { //退出
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    //account://pay
    if ([urlStr containsString:@"account://pay"]) { //支付
        RechargeViewController *vc = [[RechargeViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    //account://pay
    if ([urlStr containsString:@"share://detail?id="]) { //支付
        NSString *shareId = [urlStr substringFromIndex:18];
        CCShareDetailController *vc = [[CCShareDetailController alloc]init];
        vc.shareId = shareId;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    
    if ([urlStr containsString:@"carinfo://detail?id="]) {
        NSString *idStr =  [TOOL param:@"id" htmlStr:urlStr];
        TSVerifyController *vc = [[TSVerifyController alloc]init];
        vc.showUserId = idStr;
        vc.showRole = TSRoleDriver;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    if ([urlStr containsString:@"ownerinfo://detail?id"]) {
        NSString *idStr =  [TOOL param:@"id" htmlStr:urlStr];
        TSVerifyController *vc = [[TSVerifyController alloc]init];
        vc.showUserId = idStr;
        vc.showRole = TSRoleStation;
        vc.isStation = YES;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    if ([urlStr containsString:@"share_red://detail"]) {//红包分享
        [self share];
        return NO;
    }
    if ([urlStr containsString:@"infocastellan://detail"]) {//主城详情
        NSString *idStr =  [TOOL param:@"id" htmlStr:urlStr];
        RedCityHomeController *vc = [[RedCityHomeController alloc]init];
        vc.cityId = idStr;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    if ([urlStr containsString:@"buycastellan://detail"]) {//购买城主
        if (self.actionBlk) {
            self.actionBlk();
        }
        return NO;
    }
    //我的推广
    if ([urlStr containsString:@"generalize://detail"]) {//
        CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
        vc.titleStr = @"我的推广";
        vc.hiddenNav = YES;
        vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/wep/energystatistics?r_id=%@",USERID];
        [self.navigationController pushViewController:vc animated:YES];
        
        return NO;
    }
    
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
     [TOOL hideLoading:self.webview];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [TOOL hideLoading:self.webview];
}
#pragma mark - 进度条
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
//    if ([keyPath isEqualToString:@"estimatedProgress"]) {
//        self.progressView.hidden = self.webview.estimatedProgress == 1.0;
//        [self.progressView setProgress:(float)self.webview.estimatedProgress animated:YES];
//    }
//}
- (void)dealloc {
    //    [self.webview removeObserver:self forKeyPath:@"estimatedProgress"];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = self.hiddenNav;
    if(self.navColor){
        self.navigationController.navigationBar.barTintColor = self.navColor;
    }

}
#pragma mark -分享菜单方法
-(void)rightBarButtonItemClick{
    self.rightShareView.hidden = !self.rightShareView.hidden;
}
- (void)backMove {
    if ([self.webview canGoBack]) {
        [self.webview goBack];
    }else{
        [super backMove];
    }
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    self.rightShareView.hidden = YES;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.share_image]]] ?: [UIImage imageNamed:@"cct"];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:self.share_title ?: @"全球村村通"  descr:@"——来自全球村村通APP" thumImage:image];
    //设置网页地址
    shareObject.webpageUrl = self.share_url;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}
#pragma mark - ShareListViewDelegate
-(void)itemClick:(UIButton *)sender{
    NSLog(@"shareURL---%@",self.share_url);
    switch (sender.tag - 3000) {
            case 0: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatSession];
            }
            break;
            case 1: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine];
            }
            break;
            case 2: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_QQ];
            }
            break;
            case 3: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_Qzone];
            }
            break;
        default:
            break;
    }
    
}

#pragma mark - setters and getters

-(UIProgressView *)progressView{
    if (!_progressView) {
        //        CGFloat y = self.hiddenHtmlNav ? 20 : 64;
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, KEY_WIDTH, 0)];
        _progressView.progressTintColor = [UIColor greenColor];
        _progressView.transform = CGAffineTransformMakeScale(1.0f,2.0f);
    }
    return _progressView;
}

- (ShareListView *)rightShareView {
    
    if (!_rightShareView) {
        _rightShareView = [[ShareListView alloc]initWithShareTypes:@[[NSNumber numberWithInteger:Share_weixin],[NSNumber numberWithInteger:Share_pengyouquan],[NSNumber numberWithInteger:Share_qq],[NSNumber numberWithInteger:Share_qqkj]]];
        [self.view addSubview:_rightShareView];
        [self.rightShareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _rightShareView.hidden = YES;
        _rightShareView.delegate = self;
        [_rightShareView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                          initWithActionBlock:^(id  _Nonnull sender) {
                                              _rightShareView.hidden = YES;
                                          }]];
        
    }
    return _rightShareView;
}
- (UIWebView *)webview {
    if (_webview == nil) {
        CGFloat y = self.hiddenNav ? 20 : 64;
        _webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, y, App_Width, App_Height-y)];
//        _webview.navigationDelegate = self;
        _webview.delegate = self;
        [self.view addSubview:_webview];
        if (self.topOffset) {
            [self.webview.scrollView setContentInset:UIEdgeInsetsMake(self.topOffset, 0, 0, 0)];
        }
    }
    return _webview;
}
- (UIView *)bannerView {
    if (_bannerView == nil) {
        _bannerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 64)];
        [self.view addSubview:_bannerView];
        _bannerView.backgroundColor = MAINBLUE;
    }
    return _bannerView;
}

#pragma mark - 分享

- (CCShareView *)shareView {
    if (!_shareView) {
        _shareView = [CCShareView instanceView];
        _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.pengyouquanBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareView;
}

- (void)share {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height;
    }];
}
- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
    }];
}
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [self sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    // 设置短信内容
    NSString *url = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    NSString *shareText = [@"红包雨来了，每天几万个商家在全球村村通发红包，速度下载捡红包吧！！！" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, url];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
    
}


- (void)weixinBtnClick:(UIButton *)sender {
    [self shareCancelBtnClick];
    NSInteger tag = sender.tag;
    UMSocialPlatformType  platform = tag == 10 ? UMSocialPlatformType_WechatSession : UMSocialPlatformType_WechatTimeLine;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@""]]];
    NSString *title = @"红包雨来了";
    NSString *desc = @"每天几万个商家在全球村村通发红包，速度下载捡红包吧！！！";
    if(platform == UMSocialPlatformType_WechatTimeLine){
        desc = @"红包雨来了，每天几万个商家在全球村村通发红包，速度下载捡红包吧！！！";
    }
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: title  descr:desc thumImage:IMAGENAMED(@"red_share")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

@end
