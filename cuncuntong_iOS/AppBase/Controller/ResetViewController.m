//
//  ResetViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/26/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "ResetViewController.h"
#import "CCTTextField.h"
#import "CCTButton.h"
#import "ResetFinishController.h"
#import "AppDelegate+setup.h"

@interface ResetViewController () {
    
    UIButton *_verButton;
    
    CCTTextField *_userNameTF;
    
    CCTTextField *_verCodeTF;
}
@property (nonatomic, strong) NSString *resetUrl;
@property (nonatomic, assign) BOOL isChangePhone;


@end

@implementation ResetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = _navTitle;
    
    self.isChangePhone = [self.navTitle isEqualToString:@"更换手机号"];
    [self navConfig];
    [self uiConfig];
    
    if (self.phone) {
        _userNameTF.text = self.phone;
        _userNameTF.enabled = NO;
    }
    self.resetUrl = [self.navTitle isEqualToString:@"更换手机号"] ? @"account/reset_mobile_2" : @"account/retcode";
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = WHITECOLOR;
}
- (void)navConfig {
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame = CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE);
    [backButton setImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backItem;
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)uiConfig {
    
    _userNameTF = [[CCTTextField alloc] init];
    [self.view addSubview:_userNameTF];
    [_userNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(64 + 23 * KEY_RATE);
        make.left.equalTo(@(15 * KEY_RATE));
        make.right.equalTo(@(-15 * KEY_RATE));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    
    _userNameTF.placeholder = @"请输入手机号码/帐号";
    if ([self.navTitle isEqualToString:@"更换手机号"]) {
        _userNameTF.placeholder = @"请输入新手机号";
    }
    UIImageView *userImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 18.5 * KEY_RATE)
                                                   image:@"img_denglu"];
    _userNameTF.leftView = userImgv;
    
    
    for (NSInteger i = 0; i < 2; i++) {
        UIView *seperatorView = [[UIView alloc] init];
        [self.view addSubview:seperatorView];
        [seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_userNameTF.mas_bottom).with.offset(i * (56 * KEY_RATE + 1));
            make.left.equalTo(_userNameTF.mas_left);
            make.right.equalTo(_userNameTF.mas_right);
            make.height.equalTo(@1);
        }];
        seperatorView.backgroundColor = RGB(238, 238, 238);
        
    }

    _verCodeTF = [[CCTTextField alloc] init];
    [self.view addSubview:_verCodeTF];
    _verCodeTF.secureTextEntry = YES;
    _verCodeTF.clearButtonMode = UITextFieldViewModeNever;
    [_verCodeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15 * KEY_RATE));
        make.right.equalTo(@(-15 * KEY_RATE));
        make.height.equalTo(@(56 * KEY_RATE));
        make.top.equalTo(_userNameTF.mas_bottom).with.offset(1);
    }];
    _verCodeTF.placeholder = @"请输入短信验证码";
    
    UIImageView *passImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 17 * KEY_RATE, 14 * KEY_RATE) image:@"img_duanxin"];
    _verCodeTF.leftView = passImgv;
    
    _verButton = [[UIButton alloc] init];
    [self.view addSubview:_verButton];
    [_verButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_verCodeTF.mas_right);
        make.centerY.equalTo(_verCodeTF.mas_centerY);
        make.height.equalTo(@(16 * KEY_RATE));
        make.width.equalTo(@(100 * KEY_RATE));
    }];
    _verButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_verButton setTitle:@"点击获取" forState:UIControlStateNormal];
    [_verButton setTitleColor:RGB(56, 126, 255) forState:UIControlStateNormal];
    [_verButton addTarget:self action:@selector(getVerCode) forControlEvents:UIControlEventTouchUpInside];
    
    CCTButton *nextStepButton = [[CCTButton alloc] init];
    [self.view addSubview:nextStepButton];
    
    [nextStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_verCodeTF);
        make.height.mas_equalTo(44);
        make.top.equalTo(_verCodeTF.mas_bottom).with.offset(36 * KEY_RATE);
    }];
    NSString *text = self.isChangePhone ? @"提交" : @"下一步";
    [nextStepButton setTitle:text forState:UIControlStateNormal];
    nextStepButton.titleLabel.font = FFont(14);
    
    [nextStepButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Target Action

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getVerCode {
    
    // 判断手机号码
    if (![MyUtil validateTelephoneNumber:_userNameTF.text]) {
        [MBProgressHUD showMessage:@"请输入正确的手机号码"];
        return;
    }
    
    
    __block NSInteger time = 59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮的样式
                [_verButton setTitle:@"重新发送" forState:UIControlStateNormal];
                //                [_verButton setTitleColor:[UIColor colorFromHexCode:@"FB8557"] forState:UIControlStateNormal];
                _verButton.userInteractionEnabled = YES;
            });
        }else{
            int seconds = time % 60;
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮显示读秒效果
                [_verButton setTitle:[NSString stringWithFormat:@"重新发送(%.2d)", seconds] forState:UIControlStateNormal];
                //                [_verButton setTitleColor:[UIColor colorFromHexCode:@"979797"] forState:UIControlStateNormal];
                _verButton.userInteractionEnabled = NO;
            });
            time--;
        }
    });
    dispatch_resume(_timer);
    
    if (self.isChangePhone) {
        [[AFHTTPSessionManager manager].requestSerializer setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"mUserDefaultsCookie"]forHTTPHeaderField:@"Cookie"];
    }
    //获取验证码
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(self.resetUrl) params:@{@"mobile": _userNameTF.text,@"tle":STR([UserManager sharedInstance].tel)} target:self success:^(NSDictionary *success) {
        NSLog(@"%@",success);
    } failure:^(NSError *failure) {
    }];

}

- (void)nextStep {
    
    // 判断手机号码
    if (![MyUtil validateTelephoneNumber:_userNameTF.text]) {
        [MBProgressHUD showMessage:@"请输入正确的手机号码"];
        return;
    }
    // 判断验证码
    if (![MyUtil validateVerCode:_verCodeTF.text]) {
        [MBProgressHUD showMessage:@"请输入正确的验证码"];
        return;
    }
    if (self.isChangePhone) {//更换手机号
        [[AFHTTPSessionManager manager].requestSerializer setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"mUserDefaultsCookie"]forHTTPHeaderField:@"Cookie"];
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/reset_mobile") params:@{@"code": _verCodeTF.text} target:self success:^(NSDictionary *success) {
            NSLog(@"%@",success);
            if ([success[@"error"] integerValue] == 0) {
                SHOW(@"更换手机号成功");
                [App_Delegate resetXMPP];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                NSString *msg = success[@"msg"];
                SHOW(msg);
            }
        } failure:^(NSError *failure) {
        }];
    }else{
        //验证验证码是否正确  todo
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/recheckcode") params:@{@"mobile":_userNameTF.text,@"code":_verCodeTF.text} target: self success:^(NSDictionary *success) {
            if ([success[@"data"][@"code"] isEqualToString:@"1"]) {
                ResetFinishController *vc = [[ResetFinishController alloc] init];
                vc.navTitle = @"输入新密码";
                vc.code = _verCodeTF.text;
                vc.mobileString = _userNameTF.text;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                SHOW(@"验证码错误");
            }
    
        } failure:^(NSError *failure) {
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
