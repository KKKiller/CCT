//
//  CCBaseWebviewController.h
//  CunCunTong
//
//  Created by 我是MT on 2017/5/31.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
typedef void (^actionBlk)(void);
@interface CCBaseWebviewController : BaseViewController
@property (nonatomic, strong) NSString *urlStr;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, assign) BOOL hiddenNav; //html页面自带nav,需要隐藏
@property (nonatomic, assign) BOOL hiddenShare; //html页面自带nav,需要隐藏
@property (nonatomic, strong) NSString *share_title;
@property (nonatomic, strong) NSString *share_image;
@property (nonatomic, strong) NSString *shareURL;
@property (nonatomic, assign) CGFloat topOffset;
@property (nonatomic, strong) UIColor *navColor;
@property (nonatomic, strong) actionBlk actionBlk;

@end
