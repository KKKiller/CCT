//
//  BaseViewController.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].enable = YES;
    

    [self setBaseSelf];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.22f green:0.49f blue:1.00f alpha:1.00f];
    self.navigationController.navigationBar.titleTextAttributes =  @{NSFontAttributeName:[UIFont systemFontOfSize:18*KEY_RATE],NSForegroundColorAttributeName: [UIColor whiteColor]};
//     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    if (!self.navigationItem.leftBarButtonItem) {
        [self addReturnBtn:@"tui_"];
    }
    NSString *className = [self className];
    [MobClick beginLogPageView:className];
    if (_statusBarWhite) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSString *className = [self className];
     [MobClick endLogPageView:className];
}
- (void)setBaseSelf
{
//    self.view.backgroundColor = BACKGRAY;
    self.automaticallyAdjustsScrollViewInsets = NO;
     self.navigationController.navigationBar.translucent = YES;
}

//navigation上添加标题
- (void)addTitle:(NSString *)Title
{
    self.navigationItem.title = Title;
}

//navigation上添加返回按钮
- (void)addReturnBtn:(NSString *)bgImg{

//    UIButton *leftItem = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:bgImg] forState:UIControlStateNormal];
//    [leftItem addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
//     UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:leftItem];
//    self.navigationItem.leftBarButtonItem = left;
    
}
- (void)backMove
{
    if (self.navigationController) {
        if (([App_Delegate.window.rootViewController isEqual:App_Delegate.shareTabbarVc] || [App_Delegate.window.rootViewController isEqual:App_Delegate.transportTabbarVc]) && self.navigationController.viewControllers.count <= 1) {
            App_Delegate.defaultHome = @"defalut";
            [App_Delegate setRootViewController];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)hideNavLeftButton {
    self.leftButton.hidden = YES;
}

- (void)briberyMoney {
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    if (IOS7){
        if (self.navigationController.viewControllers.count>1)
            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        else
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    if(IOS7)
    {
        if (self.navigationController.viewControllers.count>1 && YES)
        {
            if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                self.navigationController.interactivePopGestureRecognizer.delegate = nil;
            }
        }
    }
    
}
- (void)setRightBtn:(NSString *)title {
    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
    [rightBtn setTitle:title forState:UIControlStateNormal];
    rightBtn.titleLabel.font = FFont(12);
    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    rightBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = right;
}
- (void)setRightItemWithImagename:(NSString *)name {
//    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
//    [rightBtn setTitle:title forState:UIControlStateNormal];
//    rightBtn.titleLabel.font = FFont(12);
//    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    rightBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithImage:IMAGENAMED(name) style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtomItemClick:)];
    self.tabBarController.navigationItem.rightBarButtonItem = rightItem;
}

- (void)setBackItemWithImagename:(NSString *)name {
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithImage:IMAGENAMED(name) style:UIBarButtonItemStylePlain target:self action:@selector(backClick:)];
    self.tabBarController.navigationItem.leftBarButtonItem = backItem;
}
- (void)rightBarButtomItemClick:(UIBarButtonItem *)item{
    //子类实现
}
- (void)backClick:(UIBarButtonItem *)item{
    
}
- (void)rightBtnClick {
    //子类实现
}
- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_leftButton addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        self.navigationItem.leftBarButtonItem = left;
    }
    return _leftButton;
}

@end
