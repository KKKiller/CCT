//
//  TabBarController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/23/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
@interface TabBarController : UITabBarController
- (void)gotoHome;
- (void)gotoChat;
- (void)gotoIndex:(NSInteger)index;
@property (nonatomic, strong) HomeViewController *homeVc;
- (void)pushVc:(BaseViewController *)vc;
- (void)updateBadgeValueForTabBarItem;
@end
