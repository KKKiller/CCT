//
//  ResetFinishController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/26/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface ResetFinishController : BaseViewController

@property (nonatomic, copy) NSString *navTitle;
@property (nonatomic, copy) NSString *mobileString;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *tel;

@end
