//
//  ResetViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/26/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface ResetViewController : BaseViewController

@property (nonatomic, copy) NSString *navTitle;
@property (nonatomic, strong) NSString *phone;
@end
