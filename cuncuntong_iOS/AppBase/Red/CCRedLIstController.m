//
//  CCRedLIstController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCRedLIstController.h"
#import "CCRedListHeaderView.h"
#import "CCRedListCell.h"
static NSString *cellID = @"CCRedListCell";
@interface  CCRedLIstController() <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) CCRedListHeaderView *header;

@end
@implementation CCRedLIstController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
- (void)refreshData { //点击tabbar刷新
    self.pageIndex = 1;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 获取数据
- (void)loadData {
    //我加入的社群
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.dataArray.count;
    return 20;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCRedListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.dataArray.count > indexPath.row) {
        
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        
    }
}

#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 50;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCRedListCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
        _tableView.tableHeaderView = self.header;
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
- (CCRedListHeaderView *)header {
    if (!_header) {
        _header = [CCRedListHeaderView instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 242);
    }
    return _header;
}

@end
