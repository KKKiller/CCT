//
//  CCPostRedController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCPostRedController.h"
#import "CCInputKeywordView.h"
@interface CCPostRedController ()
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UITextField *numField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redNumTopMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redNumHeight;
@property (weak, nonatomic) IBOutlet UITextField *nomeyField;
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *msgPlaceHolder;
@property (weak, nonatomic) IBOutlet UITextField *msgField;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (strong, nonatomic) PathDynamicModal *animateModel;

@property (nonatomic, strong) CCInputKeywordView *keywordView;


@property (nonatomic, assign) BOOL isRandomRed; //随机红包
@end

@implementation CCPostRedController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isGroup = YES;
    self.isRandomRed = YES;
    [self.closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.typeBtn addTarget:self action:@selector(typeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.sendBtn addTarget:self action:@selector(sendBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.redNumHeight.constant = self.isGroup ? 40 : 0;
    self.redNumTopMargin.constant = self.isGroup ? 15 : 0;
    self.typeBtn.hidden = !self.isGroup;
    self.typeLbl.hidden = !self.isGroup;
}
- (void)closeBtnClick {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)typeBtnClick:(UIButton *)sender {
    self.isRandomRed = !self.isRandomRed;
    NSString *title = self.isRandomRed ?  @"改为普通红包" : @"改为随机红包";
    [sender setTitle:title forState:UIControlStateNormal];
    NSString *desc = self.isRandomRed ?  @"每人抽到的金额随机" :@"每人抽到的金额相同";
    self.typeLbl.text = desc;
}
- (void)sendBtnClick {
//    if ([self.nomeyField.text integerValue] <= 0) {
//        SHOW(@"输入金额有误,请重新输入");
//        return;
//    }
//    if (self.isGroup && [self.numField.text integerValue] <= 0) {
//        SHOW(@"输入数量有误,请重新输入");
//        return;
//    }
    [self pay];
}
- (void)pay {
    CCInputKeywordView *view = [CCInputKeywordView instanceView];
    [view.closeBtn addTarget:self action:@selector(closeRed) forControlEvents:UIControlEventTouchUpInside];
    [view.payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [view.chargeBtn addTarget:self action:@selector(chargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, 240, 270);
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:self.view];
}
- (void)closeRed {
    [self.animateModel closedHandler];
}
- (void)payBtnClick {
    
}
- (void)chargeBtnClick {
    
}
@end
