//
//  MTConstants.h
//  Mentor
//
//  Created by 我是MT on 16/3/1.
//  Copyright © 2016年 馒头科技. All rights reserved.
//

#ifndef MTConstants_h
#define MTConstants_h

#pragma mark -  --------------------数据替代类-----------------------------
#define Def               [NSUserDefaults standardUserDefaults]
#define ISLOGIN           [TOOL isLogin]
#define TOOL              [MTTools tool]
#define XMPPTOOL              [XmppTools sharedManager]
#define USERID            [TOOL getUserId]
#define PHONE             [TOOL getPhone]
#define REALNAME          [TOOL getUserName]
#define SECRET            [TOOL getSecret]
#define PASSWORD            [TOOL getPassword]
#define NOTICENTER        [NSNotificationCenter defaultCenter]
#define VK(dict,key)      [dict valueForKey:key]
#define STR(str)          [TOOL stringNotEmpty:str]
#define URL(str)          [TOOL stringToURL:str]
#define LTTImgaeURL(str)          [TOOL stringToLTTURL:str]

#define isIphoneX ({\
BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
if (!UIEdgeInsetsEqualToEdgeInsets([UIApplication sharedApplication].delegate.window.safeAreaInsets, UIEdgeInsetsZero)) {\
isPhoneX = YES;\
}\
}\
isPhoneX;\
})

#define IMAGENAMED(NAME)  [UIImage imageNamed:NAME]
#define STRINGEMPTY(str)  [TOOL stringEmpty:str]
#define App_Delegate      ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define MTRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]
#define MTARGB(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:a]

#pragma mark - 颜色
#define BLACKCOLOR [UIColor blackColor]
#define WHITECOLOR [UIColor whiteColor]
#define BACKGRAY   MTRGB(0xeeeeee)
#define BACKGREEN  MTRGB(0xdbe4e2)
#define REDDOT     MTRGB(0xff546b)
#define PURPLECOLOR [UIColor purpleColor]
#define CLEARCOLOR  [UIColor clearColor]
#define ORANGECOLOR [UIColor orangeColor]
#define RGBCOLOR(r,g,b)          [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a)       [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define TEXTBLACK9           MTRGB(0x999999)
#define TEXTBLACK6           MTRGB(0x666666)
#define TEXTBLACK3           MTRGB(0x333333)
#define TEXTBLACK2           MTRGB(0x222222)
#define TEXTBLACK            MTRGB(0x000000)
#define TEXTLIGHTC           MTRGB(0xcccccc)
#define MAINBLUE             RGB(56, 126, 255)
#define MAINCOLOR            RGB(56, 126, 255)
#define MAINGREEN            MTRGB(0x50BB96)
#define TSGRAY            MTRGB(0xf7f7f7)
#define RED               MTRGB(0xFA6B64)


//RGB颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue)    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#pragma mark -  --------------------工具类-----------------------------



#define App_Height [[UIScreen mainScreen] bounds].size.height //主屏幕的高度
#define App_Width  [[UIScreen mainScreen] bounds].size.width  //主屏幕的宽度


#define RCDscreenHeight [[UIScreen mainScreen] bounds].size.height //主屏幕的高度
#define RCDscreenWidth  [[UIScreen mainScreen] bounds].size.width  //主屏幕的宽度
#define HEXCOLOR(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]
#define DEFAULTS [NSUserDefaults standardUserDefaults]
#define ShareApplicationDelegate [[UIApplication sharedApplication] delegate]
#define IOS_FSystenVersion ([[[UIDevice currentDevice] systemVersion] floatValue])

#define RCDDebugTestFunction 0

#define RCDPrivateCloudManualMode 0
//字体大小（常规/粗体）
#pragma mark - 字体
#define BOLDSYSTEMFONT(FONTSIZE) [UIFont boldSystemFontOfSize:FONTSIZE]
#define FFont(FONTSIZE)       [UIFont systemFontOfSize:FONTSIZE]

#define WEAKSELF __weak typeof (self) weakSelf = self;
#define STRONGSELF typeof(weakSelf) __strong strongSelf = weakSelf;

#pragma mark - 版本
#define FSystenVersion  ([[[UIDevice currentDevice] systemVersion] floatValue])
#define FAppVersion  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#pragma mark - 提示
#define ALERT(msg)  [[[UIAlertView alloc]initWithTitle:@"提示" message:msg delegate:nil \
cancelButtonTitle:@"确定" otherButtonTitles:nil,nil] show]
#define SHOW(msg) [TOOL showMessage:msg]

#define kJidParam @"@chat.wanshitong.net"
#define kXMPPHost @"chat.wanshitong.net" //@"211.149.193.220"
#define kXMPPPort 5222


#define XMPP_HOST @"chat.wanshitong.net" //@"211.149.193.220"
#define XMPP_PLATFORM @"IOS"
#define CHATTYPE @"chat"

#define kXMPP_ROSTER_CHANGE @"kXMPP_ROSTER_CHANGE"
#define kXMPP_CONNECTION_CHANGE @"kXMPP_CONNECTION_CHANGE"
#endif /* MTConstants_h */


// iPhone5 iPhone5s iPhoneSE

#define IS_iPhone_5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

// iPhone6 7 8

#define IS_iPhone_6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)

// iPhone6plus  iPhone7plus iPhone8plus

#define IS_iPhone6_Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)

// iPhoneX

#define IS_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)


#define IS_iPhoneXR ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)

