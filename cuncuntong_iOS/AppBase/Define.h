//
//  Define.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/23/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#ifndef Define_h
#define Define_h

#import <Masonry.h>
#import "MyNetWorking.h"
#import "AppDelegate.h"
#import <YYKit.h>
#import "MyUtil.h"
#import "CCTAlertController.h"
#import "UserManager.h"
#import "MBProgressHUD+MYMBP.h"
#import "MJRefresh.h"
#import <AlipaySDK/AlipaySDK.h>
#import "UMMobClick/MobClick.h"
//#import <UMSocial.h>
#import <UMSocialCore/UMSocialCore.h>

//#import <UMSocialWechatHandler.h>
//#import <UMSocialQQHandler.h>
//#import <UMSocialSinaSSOHandler.h>
#import "MD5UTils.h"
#import "MTConstants.h"
#import "MTTools.h"
#import "UIView+FrameEdit.h"
#import "UILabel+HFLabel.h"
#import "UIButton+HFButton.h"
#import "CCEmptyView.h"
#import "CCBaseWebviewController.h"
#import <RongIMKit/RongIMKit.h>
#import "CunCunTong-Swift.h"
#import "CCChatController.h"
#import <IQKeyboardManager.h>
#ifndef __OPTIMIZE__
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...){}
#endif

#define KEY_WIDTH [[UIScreen mainScreen] bounds].size.width
#define KEY_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define FontSize(object)  [UIFont systemFontOfSize:object*KEY_RATE]
#define BASEURL_WITHOBJC(objc) [NSString stringWithFormat:@"http://www.Cct369.com/village/public/%@",objc]
#define BASELIVEURL_WITHOBJC(objc) [NSString stringWithFormat:@"http://47.104.244.68/msg/%@",objc]
#define RGB(r,g,b) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0]
#define IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0)
#define KEY_RATE KEY_WIDTH/375

//状态栏高度
#define StatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
// 导航栏高度
#define WCFNavigationHeight (44+StatusBarHeight)
//TabBar高度
#define WCFTabBarHeight (StatusBarHeight > 20 ? 83 : 49)

#define LTTURL(objc) [NSString stringWithFormat:@"http://www.cct369.com/village/public/ltt/%@",objc]


#endif /* Define_h */
