//
//  AppDelegate+setup.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/10.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "AppDelegate.h"
#import <Bugly/Bugly.h>
#import <JPUSHService.h>
#import <RongIMKit/RongIMKit.h>
#import "WXApi.h"
#import "BDSSpeechSynthesizer.h"
#import "BDSSpeechSynthesizer.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppDelegate (setup)<WXApiDelegate,RCIMUserInfoDataSource,AMapLocationManagerDelegate,BuglyDelegate,BDSSpeechSynthesizerDelegate>
- (void)setupSDK:(NSDictionary *)launchOptions;
- (void)readText:(NSString *)text;
- (void)updateSetps:(finishStepUpdate)block;
- (void)resetXMPP;
@end

NS_ASSUME_NONNULL_END
