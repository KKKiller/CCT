//
//  MTDoublePageView.h
//  Mentor
//
//  Created by 我是MT on 2017/4/17.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MTPageViewDelegate<NSObject>
- (void)pageViewDidClickAtIndex:(NSUInteger)index;
@end
@interface MTPageView : UIView
@property (strong, nonatomic) NSArray<NSString *> *titleArray;
@property (weak, nonatomic) id<MTPageViewDelegate> delegate;
@property (assign, nonatomic) CGFloat barviewX;
@property (assign, nonatomic) CGFloat offset;
@property (strong, nonatomic) NSArray *colorArray;
@end
