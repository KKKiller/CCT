//
//  CCTButton.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/25/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "CCTButton.h"

@implementation CCTButton

- (instancetype)init {
    if (self = [super init]) {
        
        self.layer.cornerRadius = 5;
        self.backgroundColor = RGB(56, 126, 255);
    }
    return self;
}

@end
