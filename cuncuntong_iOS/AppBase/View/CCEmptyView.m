//
//  CCEmptyView.m
//  CunCunTong
//
//  Created by 我是MT on 2017/6/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCEmptyView.h"
@interface CCEmptyView()
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *textLbl;
@end
@implementation CCEmptyView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.imgView = [[UIImageView alloc]init];
        self.backgroundColor = BACKGRAY;
        [self addSubview:self.imgView];
        self.imgView.image = IMAGENAMED(@"empty");
        
        self.textLbl = [[UILabel alloc]initWithText:@"空空如也" font:14 textColor:TEXTBLACK9];
        [self addSubview:self.textLbl];
        
        [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.centerY.equalTo(self);
            make.width.height.mas_equalTo(100);
        }];
        [self.textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.imgView.mas_bottom).offset(10);
        }];
    }
    return self;
}

@end
