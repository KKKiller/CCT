//
//  Triangle.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/25/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "Triangle.h"

@implementation Triangle

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextBeginPath(context);

    CGContextMoveToPoint(context, 0, 12*KEY_RATE);

    CGContextAddLineToPoint(context, 9*KEY_RATE, 0);

    CGContextAddLineToPoint(context, 18*KEY_RATE, 12*KEY_RATE);

    CGContextClosePath(context);

    [[UIColor whiteColor] setFill];

    [[UIColor whiteColor] setStroke];
    
    CGContextDrawPath(context, kCGPathFillStroke);
}

@end
