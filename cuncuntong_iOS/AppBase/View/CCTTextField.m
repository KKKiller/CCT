//
//  CCTTextField.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/25/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "CCTTextField.h"

@implementation CCTTextField

- (instancetype)init {
    if (self = [super init]) {
        self.font = [UIFont systemFontOfSize:16];
        self.leftViewMode = UITextFieldViewModeAlways;
        self.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return self;
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    CGRect leftRect = [super leftViewRectForBounds:bounds];
    leftRect.origin.x += 19 * KEY_RATE;
    return leftRect;
}

//- (CGRect)rightViewRectForBounds:(CGRect)bounds {
//    CGRect rightRect = [super rightViewRectForBounds:bounds];
//    rightRect.origin.x -= 10;
//    return rightRect;
//}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect textRect = [super textRectForBounds:bounds];
    textRect.origin.x += 15 * KEY_RATE;
    return textRect;
}

//- (CGRect)placeholderRectForBounds:(CGRect)bounds {
//    CGRect placeRect = [super placeholderRectForBounds:bounds];
//    placeRect.origin.x += 15 * KEY_RATE;
//    return placeRect;
//}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect editingRect = [super editingRectForBounds:bounds];
    editingRect.origin.x += 15 * KEY_RATE;
    return editingRect;

}

@end
