//
//  MTDoublePageView.m
//  Mentor
//
//  Created by 我是MT on 2017/4/17.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import "MTPageView.h"
@interface MTPageView()
@property (strong, nonatomic) UILabel *firstLbl;
@property (strong, nonatomic) UILabel *secondLbl;
@property (strong, nonatomic) UILabel *thirdLbl;
@property (strong, nonatomic) UIView *bottomBarView;
@property (strong, nonatomic) UILabel *bottomLineLbl;
@property (assign, nonatomic) CGFloat titleMargin;
@property (assign, nonatomic) CGFloat maxTitleWidth;


@end
@implementation MTPageView
- (instancetype)initWithTitleArray:(NSArray *)titleArray{
    if (self = [super init]) {
        self.titleArray = titleArray;
    }
    return self;
}
- (void)setTitleArray:(NSArray *)titleArray {
    _titleArray = titleArray;
    NSAssert(titleArray.count == 2 || titleArray.count == 3, @"只支持2或者3个标题形式");
    self.firstLbl.text = [titleArray firstObject];
    self.secondLbl.text = titleArray[1];
    if(titleArray.count == 3){
        self.thirdLbl.text = titleArray[2];
    }
    [self setNeedsDisplay];
}
- (void)setBarviewX:(CGFloat)barviewX{
    _barviewX = barviewX;
    self.bottomBarView.x = barviewX;
}
- (void)setOffset:(CGFloat)offset{
    _offset = offset;
//    if (self.titleArray.count == 2) {
        CGFloat xoffset = (self.titleMargin*2 + self.maxTitleWidth)/App_Width * offset;
        self.bottomBarView.x = (App_Width*0.5 - self.titleMargin - self.maxTitleWidth) + xoffset; //起始位置+偏移
//    }else{
    
//    }
}
- (void)setColorArray:(NSArray *)colorArray{
    _colorArray = colorArray;
    if (self.titleArray.count == 2) {
        self.firstLbl.textColor = [colorArray firstObject];
        self.secondLbl.textColor = [colorArray lastObject];
    }
}
- (void)layoutSubviews{
    self.backgroundColor = WHITECOLOR;
    if (self.titleArray.count == 2) {
        self.titleMargin = 50;
        [self.firstLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_centerX).offset(-self.titleMargin);
            make.centerY.equalTo(self.mas_centerY);
            make.height.mas_equalTo(40);
        }];
        [self.secondLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_centerX).offset(self.titleMargin);
            make.centerY.equalTo(self.mas_centerY);
            make.height.mas_equalTo(40);
        }];
        NSInteger maxTitleNum = MAX([self.titleArray[0] length], [self.titleArray[1] length]);
        self.maxTitleWidth = maxTitleNum * 14;
        CGFloat barViewX = App_Width*0.5 - self.titleMargin - self.maxTitleWidth;
        self.bottomBarView.frame = CGRectMake(barViewX, self.height - 2, self.maxTitleWidth, 2);
    }else if(self.titleArray.count == 3){
        
    }
    [self.bottomLineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
}

#pragma mark - 懒加载
- (UILabel *)firstLbl {
    if (_firstLbl == nil) {
        _firstLbl = [[UILabel alloc]initWithText:@"" font:13 textColor:MAINCOLOR];
        [self addSubview:_firstLbl];
        _firstLbl.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            if ([self.delegate respondsToSelector:@selector(pageViewDidClickAtIndex:)]) {
                [self.delegate pageViewDidClickAtIndex:0];
            }
        }];
        [_firstLbl addGestureRecognizer:tap];
    }
    return _firstLbl;
}
- (UILabel *)secondLbl {
    if (_secondLbl == nil) {
        _secondLbl = [[UILabel alloc]initWithText:@"" font:13 textColor:TEXTBLACK2];
        [self addSubview:_secondLbl];
        _secondLbl.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            if ([self.delegate respondsToSelector:@selector(pageViewDidClickAtIndex:)]) {
                [self.delegate pageViewDidClickAtIndex:1];
            }
        }];
        [_secondLbl addGestureRecognizer:tap];
    }
    return _secondLbl;
}
- (UILabel *)thirdLbl {
    if (_thirdLbl == nil) {
        _thirdLbl = [[UILabel alloc]initWithText:@"" font:13 textColor:TEXTBLACK2];
        [self addSubview:_thirdLbl];
        _thirdLbl.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            if ([self.delegate respondsToSelector:@selector(pageViewDidClickAtIndex:)]) {
                [self.delegate pageViewDidClickAtIndex:2];
            }
        }];
        [_thirdLbl addGestureRecognizer:tap];
    }
    return _thirdLbl;
}
- (UILabel *)bottomLineLbl {
    if (_bottomLineLbl == nil) {
        _bottomLineLbl = [[UILabel alloc]initWithShallowLine];
        [self addSubview:_bottomLineLbl];
    }
    return _bottomLineLbl;
}
- (UIView *)bottomBarView {
    if (_bottomBarView == nil) {
        _bottomBarView = [[UIView alloc]init];
        _bottomBarView.backgroundColor = MAINCOLOR;
        [self addSubview:_bottomBarView];
    }
    return _bottomBarView;
}
@end
