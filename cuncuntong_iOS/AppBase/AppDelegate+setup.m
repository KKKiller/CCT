//
//  AppDelegate+setup.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/2/10.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "AppDelegate+setup.h"
//#import "HealthKitManage.h"
//#import "StepManager.h"
#import "WXApiObject.h"
#import <AVFoundation/AVFoundation.h>
#import "XMPP.h"
#import "BRAddressModel.h"
#import "CCRedMessageContent.h"
#import "CrowdfundingInfoViewController.h"

#define kWCRAppID  @"ac51a4a11d"
NSString* APP_ID = @"15013045";
NSString* API_KEY = @"O4hrGqbvKp1rwreusAKlcOvI";
NSString* SECRET_KEY = @"4kMS1qMmawK8hf5rgrwriyo3YU88GP6u";
static NSString *appKey = @"c700a7294bba367d991de8d5"; //23e8ccd492a492e1ae7e881c
static NSString *channel = @"Publish channel";

@implementation AppDelegate (setup)
- (void)setupSDK:(NSDictionary *)launchOptions {
    [self setupYM:launchOptions];
    [self setupJpush:launchOptions];
    [self setupWX];
    [self setupXMPP];
    [self setGaodeMap];
    [self setupBaiduAI];
    [self setupBugly];
    [self setupPGY];

}
#pragma mark - XMPP
- (void)setupXMPP {
    if (ISLOGIN) {
        NSString *url = [NSString stringWithFormat:@"https://chat.wanshitong.net/plugins/userService/userservice?type=add&secret=qtSasMlDC61rbx8M&username=%@&password=%@&name=%@",USERID,PASSWORD,REALNAME];
        url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSLog(@"授权接口%@", url);
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:
         ^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
             
             XMPPJID *myjid = [XMPPJID jidWithString:[NSString stringWithFormat:@"%@%@",USERID,kJidParam]];
             [[XmppTools sharedManager]loginWithUser:myjid withPwd:PASSWORD withSuccess:^{
                 NSLog(@"xmpp初始化成功");
             } withFail:^(NSString *error) {
                 
             }];
         }];
    }
}
- (void)resetXMPP {
    NSString *url = [NSString stringWithFormat:@"https://chat.wanshitong.net/plugins/userService/userservice?type=delete&secret=qtSasMlDC61rbx8M&username=%@",REALNAME];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:
     ^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
         [self setupXMPP];
     }];
    
}
#pragma mark - 友盟
- (void)setupYM:(NSDictionary *)launchOptions{
    //友盟分享
    UMConfigInstance.appKey = @"594e433ac895765192000f17";
    UMConfigInstance.channelId = @"App Store";
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
    [MobClick setLogEnabled:YES];
    [[UMSocialManager defaultManager] setUmSocialAppkey:@"594e433ac895765192000f17"];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wxc7b620dfb1bcb6f6" appSecret:@"1f3139c99e27067f858465206040c571" redirectURL:@"http://www.umeng.com/social"];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatTimeLine appKey:@"wxc7b620dfb1bcb6f6" appSecret:@"1f3139c99e27067f858465206040c571" redirectURL:@"http://www.umeng.com/social"];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1105621973" appSecret:@"6u7pQGcVupGDyBgh" redirectURL:@"http://www.umeng.com/social"];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Qzone appKey:@"1105621973" appSecret:@"6u7pQGcVupGDyBgh" redirectURL:@"http://www.umeng.com/social"];
}
#pragma mark - Jpush
- (void)setupJpush:(NSDictionary *)launchOptions {
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
        entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
        [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    }else if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                          UIUserNotificationTypeSound |
                                                          UIUserNotificationTypeAlert)
                                              categories:nil];
    }
    [JPUSHService setupWithOption:launchOptions appKey:appKey
                          channel:channel
                 apsForProduction:TRUE
            advertisingIdentifier:nil];
    
    if (ISLOGIN) {
        [self registJPushAlias];
    }
}
#pragma mark - 百度语音
- (void)readText:(NSString *)text {
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    NSInteger error =  [[BDSSpeechSynthesizer sharedInstance] speakSentence:text withError:nil];
    NSLog(@"%zd",error);
    
    //    });
}
- (void)setupBaiduAI {
    [BDSSpeechSynthesizer setLogLevel:BDS_PUBLIC_LOG_VERBOSE];
    [[BDSSpeechSynthesizer sharedInstance] setSynthesizerDelegate:self];
    [self configureOnlineTTS];
    [self configureOfflineTTS];
    
}
-(void)configureOnlineTTS{
    
    [[BDSSpeechSynthesizer sharedInstance] setApiKey:API_KEY withSecretKey:SECRET_KEY];
    [[AVAudioSession sharedInstance]setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    //    [[BDSSpeechSynthesizer sharedInstance] setSynthParam:@(BDS_SYNTHESIZER_SPEAKER_DYY) forKey:BDS_SYNTHESIZER_PARAM_SPEAKER];
    [[BDSSpeechSynthesizer sharedInstance] setSynthParam:@(15) forKey:BDS_SYNTHESIZER_PARAM_ONLINE_REQUEST_TIMEOUT];
    
}
-(void)configureOfflineTTS{
    
    NSError *err = nil;
    // 在这里选择不同的离线音库（请在XCode中Add相应的资源文件），同一时间只能load一个离线音库。根据网络状况和配置，SDK可能会自动切换到离线合成。
    NSString* offlineEngineSpeechData = [[NSBundle mainBundle] pathForResource:@"Chinese_And_English_Speech_Female" ofType:@"dat"];
    
    NSString* offlineChineseAndEnglishTextData = [[NSBundle mainBundle] pathForResource:@"Chinese_And_English_Text" ofType:@"dat"];
    
    err = [[BDSSpeechSynthesizer sharedInstance] loadOfflineEngine:offlineChineseAndEnglishTextData speechDataPath:offlineEngineSpeechData licenseFilePath:nil withAppCode:APP_ID];
    if(err){
        return;
    }
}
#pragma mark - 高德地图
- (void)setGaodeMap {
    self.lat = @"";
    self.lng = @"";
    //        self.lat = @"35.71";
    //        self.lng = @"114.98";
    if ([Def valueForKey:@"lat"]) {
        self.lat = [Def valueForKey:@"lat"];
        self.lng = [Def valueForKey:@"lng"];
        [self getCurrentCityId];
    }
    
    [AMapServices sharedServices].apiKey = @"47682c60cbc44ddff3a2ccde110baa8f";
    [self getLocation];
}
#pragma mark - 定位
- (void)getLocation{
    self.locationManager = [[AMapLocationManager alloc] init];
    [self.locationManager setDelegate:self];    //设置不允许系统暂停定位
    [self.locationManager setPausesLocationUpdatesAutomatically:NO];        //设置允许在后台定位
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    [self.locationManager setLocationTimeout:6];
    [self.locationManager setReGeocodeTimeout:3];
    self.locationManager.locatingWithReGeocode = YES;
    [self.locationManager startUpdatingLocation];
    
}
#pragma mark CoreLocation delegate (定位失败)
- (void)amapLocationManager:(AMapLocationManager *)manager didFailWithError:(NSError *)error{
    //设置提示提醒用户打开定位服务
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"允许定位提示" message:@"请在设置中打开定位" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
    //    [alert addAction:cancelAction];
    //    [self presentViewController:alert animated:YES completion:nil];
}
- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location reGeocode:(AMapLocationReGeocode *)reGeocode{
    
    //旧址
    CLLocation *currentLocation = location;
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    //打印当前的经度与纬度
    NSLog(@"当前定位地址 %f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
    NSString *lng = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    self.lng = lng;
    self.lat = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    
    //    self.lat = @"35.71";
    //    self.lng = @"114.98";
    //    [Def setValue:@"北京市" forKey:@"province"];
    //    [Def setValue:@"北京市" forKey:@"cityName"];
    //    self.lat = @"39.846414";
    //    self.lng = @"116.382766";
    [Def setValue:self.lng forKey:@"lng"];
    [Def setValue:self.lat forKey:@"lat"];
    
    
    if (reGeocode && reGeocode.city && (![[Def valueForKey:@"cityName"] isEqualToString:reGeocode.city] || ![[Def valueForKey:@"districtName"] isEqualToString:reGeocode.district])) {
        NSString *province = reGeocode.province;
        NSString *city = reGeocode.city;
        NSString *dictrict = reGeocode.district;
        [Def setValue:STR(province) forKey:@"province"];
        [Def setValue:STR(city) forKey:@"cityName"];
        [Def setValue:STR(dictrict) forKey:@"districtName"];
        [self getCurrentCityId];
    }
    self.cityName = reGeocode.city;
    self.provinceName =reGeocode.province;
    self.districtName = reGeocode.district;
    if (!self.cityId) {
        [self getLocationCityId];
    }
}

//app使用,通过本地文件查找
- (void)getLocationCityId{
    NSString *province = [Def valueForKey:@"province"];
    NSString *city = [Def valueForKey:@"cityName"];
    NSString *district = [Def valueForKey:@"districtName"];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"region" ofType:@"json"];
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray *array = dict[@"regionList"];
    
    for (NSDictionary *dic in array) {
        // 此处用 YYModel 进行解析
        BRProvinceModel *proviceModel = [BRProvinceModel modelWithJSON:dic];
        if ([proviceModel.name isEqualToString:province]) {
            self.provinceId = proviceModel.id;
            for (BRProvinceModel *model in proviceModel.subList) {
                if ([model.name isEqualToString:city] || [model.name isEqualToString:@"市辖区"]) {
                    self.cityId = model.id;
                    
                    for (BRProvinceModel *districtModel in model.subList) {
                        if ([districtModel.name isEqualToString:district]) {
                            self.districtId = districtModel.id;
                            break;
                        }
                    }
                    break;
                }
            }
            break;
        }
    }
}
//货运通专用
- (void)getCurrentCityId{
    NSString *province = [Def valueForKey:@"province"];
    NSString *city = [Def valueForKey:@"cityName"];
    if (province && city) {
        if ([province containsString:@"北京"] || [province containsString:@"上海"]||[province containsString:@"天津"]|| [province containsString:@"重庆"]) {
            city = @"市辖区";
        }
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"car/cargoaddressid") params:@{@"r_id":USERID,@"province":province,@"city":city} target:nil success:^(NSDictionary *success) {
            if ([success[@"error"] integerValue] == 0) {
                self.currentCityId = success[@"data"][@"id"];
                [Def setValue:self.currentCityId forKey:@"cityId"];
                [self registJPushTags];
            }else{//调用失败使用货运站专用接口
                [self stationGetCityIdWithProvince:province city:city];
            }
        } failure:^(NSError *failure) {
        }];
    }
}
//货运站专用
- (void)stationGetCityIdWithProvince:(NSString *)province city:(NSString *)city {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/addressid") params:@{@"r_id":USERID,@"province":province,@"city":city} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.currentCityId = success[@"data"][@"id"];
            [Def setValue:self.currentCityId forKey:@"cityId"];
            [self registJPushTags];
        }
    } failure:^(NSError *failure) {
    }];
}

#pragma mark - 支付回调
- (void)setupWX{
    [WXApi registerApp:@"wx2cb5e062972b4bb4"];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([[RCIM sharedRCIM] openExtensionModuleUrl:url]) {
        return YES;
    }
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (result == FALSE) {
        if ([url.host isEqualToString:@"safepay"]) { //支付宝
            //跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                SHOW(@"充值成功");
                [self updateMoney];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"PaySuccess" object:nil];
                UIViewController *vc = ((UINavigationController *)self.tabbarVc.selectedViewController).topViewController;
                if ([vc isKindOfClass:[CrowdfundingInfoViewController class]]) {
                    [vc dismissViewControllerAnimated:YES completion:nil];
                }else{
                    //                    [self.tabbarVc.selectedViewController popViewControllerAnimated:YES];
                }
            }];
        }
        if ([url.host isEqualToString:@"pay"]) {
            return [WXApi handleOpenURL:url delegate:self];
        }
        return YES;
        
    }
    return result;
}


// NOTE: 9.0以后使用新接口 支付宝
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    if ([[RCIM sharedRCIM] openExtensionModuleUrl:url]) {
        return YES;
    }
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (result == FALSE) {
        if ([url.host isEqualToString:@"safepay"]) {
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                if ([resultDic[@"resultStatus"] intValue] == 9000) {//成功
                    SHOW(@"充值成功");
                    [self updateMoney];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"PaySuccess" object:nil];
                    UIViewController *vc = ((UINavigationController *)self.tabbarVc.selectedViewController).topViewController;
                    if ([vc isKindOfClass:[CrowdfundingInfoViewController class]]) {
                        [vc dismissViewControllerAnimated:YES completion:nil];
                    }else{
                        //                        [self.tabbarVc.selectedViewController popViewControllerAnimated:YES];
                    }
                }else{
                    NSString *str = resultDic[@"memo"];
                    SHOW(str);
                }
            }];
        }
        if ([url.host isEqualToString:@"pay"]) {
            return [WXApi handleOpenURL:url delegate:self];
        }
        return YES;
        
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [WXApi handleOpenURL:url delegate:self];
}
//微信支付回调
-(void)onResp:(BaseResp*)resp{
    if ([resp isKindOfClass:[PayResp class]]){
        PayResp *response = (PayResp*)resp;
        UIViewController *vc = ((UINavigationController *)self.tabbarVc.selectedViewController).topViewController;
        switch(response.errCode){
            case WXSuccess:
                [[NSNotificationCenter defaultCenter]postNotificationName:@"PaySuccess" object:nil];
                SHOW(@"充值成功");
                [self updateMoney];
                if ([vc isKindOfClass:[CrowdfundingInfoViewController class]]) {
                    [vc dismissViewControllerAnimated:YES completion:nil];
                }else{
                    //                    [self.tabbarVc.selectedViewController popViewControllerAnimated:YES];
                }
                //服务器端查询支付通知或查询API返回的结果再提示成功
                break;
            default:
                break;
        }
    }
}

- (void)updateMoney {
    CGFloat money  = [[UserManager sharedInstance].money floatValue];
    money = money + self.rechargeMoney;
    [UserManager sharedInstance].money = [NSString stringWithFormat:@"%f",money];
    self.rechargeMoney = 0;
}


#pragma mark - Bugly
- (void)setupBugly {
    BuglyConfig * config = [[BuglyConfig alloc] init];
    config.reportLogLevel = BuglyLogLevelWarn;
#if DEBUG
    config.debugMode = YES;
#endif
    config.channel = @"Bugly";
    config.delegate = self;
    config.consolelogEnable = NO;
    config.viewControllerTrackingEnable = NO;
    
    [Bugly startWithAppId:kWCRAppID
#if DEBUG
        developmentDevice:YES
#endif
                   config:config];
    
    [Bugly setUserIdentifier:[NSString stringWithFormat:@"User: %@", [UIDevice currentDevice].name]];
    [Bugly setUserValue:[NSProcessInfo processInfo].processName forKey:@"Process"];
}

- (NSString *)attachmentForException:(NSException *)exception {
    NSLog(@"(%@:%d) %s %@",[[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__,exception);
    return @"This is an attachment";
}

- (void)setupPGY {
    //    [[PgyManager sharedPgyManager] startManagerWithAppId:@"50a752de1d90941eb8b242818896fb72"];
    //    [[PgyUpdateManager sharedPgyManager] startManagerWithAppId:@"50a752de1d90941eb8b242818896fb72"];
    //    [[PgyUpdateManager sharedPgyManager] checkUpdate];
}

- (void)setupRY {
#if DEBUG
    //    return;
#endif
    
    //融云
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"gchat/gettoken") params:@{@"rid":USERID} success:^(NSDictionary *success) {
        if ([success[@"data"] isKindOfClass:[NSDictionary class]]) {
            
            NSString *token = success[@"data"][@"token"];
            [[RCIM sharedRCIM] initWithAppKey:@"p5tvi9dsp6034"];
            //红包
//            [[RCIM sharedRCIM] registerMessageType:[CCRedMessageContent class]];
            
            [RCIM sharedRCIM].enablePersistentUserInfoCache = YES; //缓存数据
            [[RCIM sharedRCIM] connectWithToken:token    success:^(NSString *userId) {
                NSLog(@"登陆成功。当前登录的用户ID：%@", userId);
                RCUserInfo *model = [[RCUserInfo alloc]init];
                model.userId = userId;
                model.name = [TOOL getUserName];
                model.portraitUri = [TOOL getUserAvatar];
                [RCIM sharedRCIM].currentUserInfo = model;
//                [self getVillageChat];
                self.rongInitFinished = YES;
            } error:^(RCConnectErrorCode status) {
                NSLog(@"登陆的错误码为:%ld", status);
            } tokenIncorrect:^{
                //token过期或者不正确。
                //如果设置了token有效期并且token过期，请重新请求您的服务器获取新的token
                //如果没有设置token有效期却提示token错误，请检查您客户端和服务器的appkey是否匹配，还有检查您获取token的流程。
                NSLog(@"token错误");
            }];
        }
        
    } failure:^(NSError *failure) {
        
    }];
    
    
    
}
#pragma mark - 运动步数

//- (void)updateSetps:(finishStepUpdate)block {
//    HealthKitManage *manage = [HealthKitManage shareInstance];
//    [manage authorizeHealthKit:^(BOOL success, NSError *error) {
//        if (success) {
//            NSLog(@"success");
//            [Def setBool:YES forKey:@"healths"];
//            [manage getStepCount:^(double value, NSError *error) {
//                NSLog(@"1count-->%.0f", value);
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.steps = (int)value;
//                    [self reportSteps:self.steps];
//                    if (block) {
//                        block(self.steps);
//                    }
//                });
//            }];
//        }
//        else {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请打开手机里的“健康”系统APP，在数据来源里为全球村村通开启步数权限，这样才能正常获取到您的步数信息哦～" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
//            [alert show];
//        }
//    }];
//}
//- (void)reportSteps:(NSInteger)step{
//    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/energy_sport_steps_sync") params:@{@"rid":USERID,@"steps":[NSString stringWithFormat:@"%ld",step]} success:^(NSDictionary *success) {
//        NSLog(@"%@",success);
//    } failure:^(NSError *failure) {
//        
//    }];
//}
@end
