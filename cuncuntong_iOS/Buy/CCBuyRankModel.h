//
//  CCRankModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCBuyRankModel : NSObject
@property (nonatomic, strong) NSString *portrait;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *rank;
@end
