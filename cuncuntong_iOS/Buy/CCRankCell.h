//
//  CCRankCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCBuyRankModel.h"
@interface CCRankCell : UITableViewCell
//@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) CCBuyRankModel *model;
@end
