//
//  CCBuyHomeCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/19.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCBuyHomeModel.h"
@interface CCBuyHomeCell : UITableViewCell
@property (nonatomic, strong) UIButton *buyBtn;
@property (nonatomic, strong) CCBuyHomeModel *model;
@end
