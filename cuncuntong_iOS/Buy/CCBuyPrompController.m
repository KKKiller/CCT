//
//  CCBuyPosterController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/23.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyPrompController.h"
#import "CCBuyPrompModel.h"
@interface CCBuyPrompController ()

@end

@implementation CCBuyPrompController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"我的推广"];
    for (UIView *v in self.view.subviews) {
        if (v.tag >= 200  && [v isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel *)v;
            lbl.text = @"待发展";
        }
    }
    [self loadData];
}
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"happybuy/my_promote") params:@{@"uid":USERID,@"goodid":STR(self.goodid),@"qi_id":STR(self.qi_id)} target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            CCBuyPrompModel *model = [CCBuyPrompModel modelWithJSON:success[@"data"][@"info"]];
            [self configData:model];
            NSLog(@"%@",model);
        }else{
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)configData:(CCBuyPrompModel *)model {
    CCBuyPrompModel *emptyModel = [[CCBuyPrompModel alloc]init];
    
    CCBuyPrompModel *model1 = model.xiaxid1_info ?: emptyModel;
    CCBuyPrompModel *model2 = model.xiaxid2_info ?: emptyModel;
    CCBuyPrompModel *model3 = model.xiaxid3_info ?: emptyModel;
    
    CCBuyPrompModel *model4 = model.xiaxid1_info.xiaxid1_info ?: emptyModel;
    CCBuyPrompModel *model5 = model.xiaxid1_info.xiaxid2_info ?: emptyModel;
    CCBuyPrompModel *model6 = model.xiaxid1_info.xiaxid3_info ?: emptyModel;
    
    CCBuyPrompModel *model7 = model.xiaxid2_info.xiaxid1_info ?: emptyModel;
    CCBuyPrompModel *model8 = model.xiaxid2_info.xiaxid2_info ?: emptyModel;
    CCBuyPrompModel *model9 = model.xiaxid2_info.xiaxid3_info ?: emptyModel;
    
    CCBuyPrompModel *model10 = model.xiaxid3_info.xiaxid1_info ?: emptyModel;
    CCBuyPrompModel *model11 = model.xiaxid3_info.xiaxid2_info ?: emptyModel;
    CCBuyPrompModel *model12 = model.xiaxid3_info.xiaxid3_info ?: emptyModel;
    
    NSArray *array = @[model,model1,model2,model3,model4,model5,model6,model7,model8,model9,model10,model11,model12];
    for (UIView *v in self.view.subviews) {
        if (v.tag >= 100 && v.tag < 200 && [v isKindOfClass:[UIImageView class]]) {
            NSInteger index = v.tag - 100;
            UIImageView *imgV = (UIImageView *)v;
            imgV.layer.masksToBounds = YES;
            imgV.layer.cornerRadius = index < 4 ? 30 : 15;;
            CCBuyPrompModel *modelData = array[index];
            if (modelData.portrait) {
                [imgV setImageWithURL:URL(modelData.portrait) placeholder:IMAGENAMED(@"head")];
            }
        }
        if (v.tag >= 200  && [v isKindOfClass:[UILabel class]]) {
            NSInteger index = v.tag - 200;
            UILabel *lbl = (UILabel *)v;
            CCBuyPrompModel *modelData = array[index];
            if (modelData.realname) {
                if (modelData.realname.length == 11) {
                    modelData.realname = [modelData.realname  stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
                }
                lbl.text = modelData.realname;
            }else{
                lbl.text = @"待发展";
            }
        }
    }
}
@end
