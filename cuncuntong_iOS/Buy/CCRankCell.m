//
//  CCRankCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCRankCell.h"
@interface CCRankCell()
@property (nonatomic, strong) UIImageView *headImgView;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UIImageView *rankImgView;
@property (nonatomic, strong) UILabel *rankLbl;
@property (nonatomic, strong) UILabel *lineLbl;
@end
@implementation CCRankCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle: style reuseIdentifier:reuseIdentifier]) {
        [self setUI];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (void)setModel:(CCBuyRankModel *)model {
    _model = model;
    [self.headImgView setImageWithURL:URL(model.portrait) placeholder:IMAGENAMED(@"head")];
    self.nameLbl.text = model.realname;
    self.rankLbl.hidden = [model.rank integerValue] < 4;
    self.rankImgView.hidden = [model.rank integerValue] >= 4;
    self.rankLbl.text = model.rank;
    if ([model.rank integerValue] < 4) {
        NSString *rank = [NSString stringWithFormat:@"buy_rank%@",model.rank];
        self.rankImgView.image = IMAGENAMED(rank);
    }
//    if (model.rank) {
//        self.rankLbl.hidden = NO;
//        self.rankImgView.hidden = YES;
//    }
//    self.rankLbl.textColor = MTRGB(0xff7700);
}
//- (void)setIndex:(NSInteger)index {
//    _index = index;
//    self.rankLbl.hidden = index < 3;
//    self.rankImgView.hidden = index >= 3;
//    if (index < 3) {
//        NSString *rank = [NSString stringWithFormat:@"buy_rank%@",@(index
//                          +1)];
//        self.rankImgView.image = IMAGENAMED(rank);
//    }else{
//        self.rankLbl.text = [NSString stringWithFormat:@"%@",@(index + 1)];
//    }
//    self.rankLbl.textColor = TEXTBLACK6;
//}
- (void)setUI {
    self.headImgView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.headImgView];
    self.headImgView.layer.cornerRadius = 20;
    self.headImgView.layer.masksToBounds = YES;
    
    self.nameLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.nameLbl];
    
    self.rankImgView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.rankImgView];
    
    self.rankLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.rankLbl];
    
    self.lineLbl = [[UILabel alloc]initWithShallowLine];
    [self.contentView addSubview:self.lineLbl];
}
- (void)layoutSubviews {
    [self.headImgView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(15);
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.headImgView.mas_right).offset(15);
    }];
    [self.rankImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-25);
    }];
    [self.rankLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.right.equalTo(self.contentView.mas_right).offset(-25);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
}
@end
