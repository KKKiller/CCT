//
//  CCBuyHomeModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCBuyHomeModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSArray<NSString *> *imgurl;
@property (nonatomic, strong) NSString *max_money;
@property (nonatomic, strong) NSString *sfk_money;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;

@end
