//
//  CCBuyTabbarController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/19.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyTabbarController.h"
#import "NavigationViewController.h"
#import "CCBuyMineController.h"
@interface CCBuyTabbarController ()
@property (nonatomic, strong) UIButton *leftButton;

@end

@implementation CCBuyTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.barTintColor = MTRGB(0xf0f0f0);
    self.title = @"开心购";
    [self initSelf];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    App_Delegate.buyTabbarVc = self;
}

- (void)initSelf {
    
    NSArray *vcArr = @[@"CCBuyHomeController",
                       @"CCBuyMineController",
                       @"CCBuyRuleController"];
    
    NSArray *imageArr = @[@"buy_home",
                          @"buy_my",
                          @"buy_rule"];
    
    NSArray *selImageArr = @[@"buy_home_select",
                             @"buy_my_select",
                             @"buy_rule_select"];
    NSArray *titleArr = @[@"首页",
                          @"我的申购",
                          @"申报规则"];
    
    NSMutableArray *tabArrs = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < vcArr.count; i++) {
        
        UIViewController *vc = [[NSClassFromString(vcArr[i]) alloc] init];
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        vc.tabBarItem.image = [[UIImage imageNamed:imageArr[i]]
                               imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selImageArr[i]]
                                       imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.title = titleArr[i];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x6c6c6c)} forState:UIControlStateNormal];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0xfd7722)} forState:UIControlStateSelected];
        [tabArrs addObject:vc];
    }
    
    // 解决push/pop黑影
    self.view.backgroundColor = [UIColor whiteColor];
//    self.tabBar.tintColor = [UIColor greenColor];
    self.viewControllers = tabArrs;
    
}

-(void)backMove {
    [App_Delegate.tabbarVc gotoHome];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)gotoOrder {
    self.selectedIndex = 1;
    CCBuyMineController *vc =  self.viewControllers[1];
    [vc refreshData];
}
- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_leftButton addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        self.navigationItem.leftBarButtonItem = left;
    }
    return _leftButton;
}
@end
