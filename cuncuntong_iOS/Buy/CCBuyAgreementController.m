//
//  CCBuyAgreementController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyAgreementController.h"
#import "UIView+FrameEdit.h"
#import "CCBuyPayController.h"
@interface CCBuyAgreementController ()

@end

@implementation CCBuyAgreementController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITECOLOR;
    [self addTitle:@"开心购商城服务协议"];
    NSString *text = @"    1、本商城所有商品申购后，申购款不予退返，不能接受此规定的请不要参与。无理取闹坚持退款的，退款比例最高不超50%，且信用值降为0，终生不能享受全球村村通的任何服务，参与全球村村通的任何项目。\n\n    2、申购人通过转发海报或其它方式直接发展申购用户，以及通过下级申购者间接发展申购用户，每发展一个会获得需补款金额的1/12奖励。\n\n    3、申购人所发展的申购用户，将按照最有利于申购人的排位规则进行排位。当申购人的9个推广位全部排满之后，通过扫码发展的申购用户将做为溢出无主用户参与公排。\n\n    4、无主用户的公排按申购先后顺序进行，能否获得公排机会取决于无主用户的数量和排名先后。本商城并不保证每人都能获得公排的机会。\n\n    5、因市场变化，本商城会对商城的产品进行调整，如申购者对调整的商品不满意，可以选择获得现金置换。现金置换按购物额的75%。\n\n    6、本商城对部分商品补款数额比较大的部分用户会赠送一些永久性原始股权，每年享受公司分红。\n\n   7、本协议与纸质文本协议具有同等法律效力。";

    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 64 + 10, App_Width - 20, App_Height - 64 - 120)];
    [self.view addSubview:textView];
    textView.font = FFont(14);
    textView.text = text;
    textView.textColor = TEXTBLACK3;
    textView.editable = NO;
    
    
    
    UIButton *btn = [[UIButton alloc]initWithTitle:@"立即申购" textColor:WHITECOLOR backImg:nil font:14];
    btn.frame = CGRectMake(15, App_Height - 60, App_Width - 30, 40);
    btn.backgroundColor = MTRGB(0xff7700);
    btn.layer.cornerRadius = 4;
    btn.layer.masksToBounds= YES;
    [self.view addSubview:btn];
    [btn addTarget:self action:@selector(buy) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *tips  = [[UILabel alloc]initWithText:@"注意:点击申购，即代表同意开心购商城服务协议" font:12 textColor:MTRGB(0xff7700)];
    tips.numberOfLines = 0;
    [self.view addSubview:tips];
    tips.frame = CGRectMake(btn.x, btn.y - 30, App_Width - 30, 15);
    
}

- (void)buy {
    CCBuyPayController *vc = [[CCBuyPayController alloc]init];
    vc.buyModel = self.buyModel;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
