//
//  CCMyBuyCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMyBuyModel.h"

@protocol CCMyBuyCellDelegate <NSObject>

- (void)clickAtBtn:(NSInteger)index orderId:(NSString *)orderId goodid:(NSString *)goodid;

@end
@interface CCMyBuyCell : UITableViewCell
@property (nonatomic, weak) id<CCMyBuyCellDelegate> delegate;
@property (nonatomic, strong) CCMyBuyModel *model;
@end
