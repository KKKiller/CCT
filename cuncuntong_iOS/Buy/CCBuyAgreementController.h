//
//  CCBuyAgreementController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCBuyHomeModel.h"
@interface CCBuyAgreementController : BaseViewController
@property (nonatomic, strong) CCBuyHomeModel *buyModel;
@end
