//
//  CCBuyDetailController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyDetailController.h"
#import "ZYBannerView.h"
#import "CCBuyAgreementController.h"
#import "CCBuyPayController.h"
#import "PBViewController.h"
#import "YYControl.h"
#import "ListView.h"
@interface CCBuyDetailController ()<ZYBannerViewDelegate,ZYBannerViewDataSource,PBViewControllerDataSource,PBViewControllerDelegate,UIWebViewDelegate,UIScrollViewDelegate,ListDelegate>
@property (weak, nonatomic) IBOutlet ZYBannerView *banner;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerHeight;
@property (weak, nonatomic) IBOutlet UIWebView *detailWebView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) ListView *listView;

@property (nonatomic, strong) NSArray *contentImgArray;
@property (nonatomic, assign) BOOL showContentImg;
@property (nonatomic, strong) NSString *share_url;

@end

@implementation CCBuyDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"详情"];
    self.navigationController.navigationBarHidden = NO;

    self.banner.dataSource = self;
    self.banner.delegate = self;
    self.scrollView.delegate = self;
    [self.buyBtn addTarget:self action:@selector(buyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.bannerHeight.constant = App_Width;
    self.detailWebView.scrollView.scrollEnabled = NO;
    self.detailWebView.delegate = self;
    if (self.wineModel) {
        [self.buyBtn setTitle:@"购买" forState:UIControlStateNormal];
        UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE)];
        [detailButton setImage:[UIImage imageNamed:@"xialacaidan"] forState:UIControlStateNormal];
        UIBarButtonItem * detailButtonItem = [[UIBarButtonItem alloc] initWithCustomView:detailButton];
        self.navigationItem.rightBarButtonItem = detailButtonItem;
        detailButton.titleLabel.font = FontSize(12);
        [detailButton addTarget:self action:@selector(listMenu) forControlEvents:UIControlEventTouchUpInside];
        self.share_url = [NSString stringWithFormat:@"%@village/public/wine/sharedetail?id=%@",App_Delegate.shareBaseUrl,self.wineModel.id];
    }
    
    [self loadData];
}

- (void)loadData {
    if (self.wineModel) {//酒厂
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/wine_detail") params:@{@"id":self.wineModel.id} target:self success:^(NSDictionary *success) {
            if ([success[@"code"]  isEqual: @(1)]) {
                self.wineModel = [CCWineModel modelWithJSON:success[@"msg"]];
                self.titleLbl.text = self.wineModel.name;
                self.contentImgArray = [TOOL imagesFromhtmlString:self.wineModel.content];
                NSLog(@"content: %@",self.buyModel.content);
                if (self.wineModel.content) {
                    NSString *onloadStr = @" \"this.onclick = function(){window.location.href = 'mt:src=' + th is.src;}; \"";
                    NSString *replaceStr =  [NSString stringWithFormat:@"<img onload=%@",onloadStr];
                    NSString *content = [self.wineModel.content stringByReplacingOccurrencesOfString:@"<img" withString:replaceStr];
                    [self.detailWebView loadHTMLString:content baseURL:nil];
                    
                }
            }else{
                SHOW(success[@"msg"]);
            }
        } failure:^(NSError *failure) {
            
        }];
    }else{ //快乐购
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"happybuy/prdetail") params:@{@"id":STR(self.buyModel.id)} target:self success:^(NSDictionary *success) {
            if ([success[@"data"][@"stat"] integerValue] == 1) {
                self.buyModel = [CCBuyHomeModel modelWithJSON:success[@"data"][@"info"]];
                self.titleLbl.text = self.buyModel.title;
                self.contentImgArray = [TOOL imagesFromhtmlString:self.buyModel.content];
                NSLog(@"content: %@",self.buyModel.content);
                if (self.buyModel.content) {
                    NSString *onloadStr = @" \"this.onclick = function(){window.location.href = 'mt:src=' + th is.src;}; \"";
                    NSString *replaceStr =  [NSString stringWithFormat:@"<img onload=%@",onloadStr];
                    NSString *content = [self.buyModel.content stringByReplacingOccurrencesOfString:@"<img" withString:replaceStr];
                    [self.detailWebView loadHTMLString:content baseURL:nil];
                    
                }
            }else{
                SHOW(success[@"data"][@"info"]);
            }
        } failure:^(NSError *failure) {
        }];
    }
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    self.navigationController.navigationBarHidden = NO;
}
- (void)buyBtnClick{
    if(self.wineModel){
        CCBuyPayController *vc = [[CCBuyPayController alloc]init];
        vc.wineModel = self.wineModel;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        CCBuyAgreementController *vc = [[CCBuyAgreementController alloc]init];
        vc.buyModel = self.buyModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (IBAction)backBtnClick:(id)sender {
    [self backMove];
}
- (NSInteger)numberOfItemsInBanner:(ZYBannerView *)banner {
    if (self.wineModel) {
        return 1;
    }else{
        return self.buyModel.imgurl.count;
    }
}
- (UIView *)banner:(ZYBannerView *)banner viewForItemAtIndex:(NSInteger)index {
    YYControl *imgV = [[YYControl alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Width)];
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    imgV.layer.masksToBounds = YES;
    imgV.clipsToBounds = YES;
    imgV.exclusiveTouch = YES;
    imgV.image = IMAGENAMED(@"ph");
    imgV.userInteractionEnabled = NO;
    NSString *url = nil;
    if (self.wineModel) {
        url = self.wineModel.image;
    }else{
        if (self.buyModel.imgurl.count > index) {
            url = self.buyModel.imgurl[index];
        }
    }
    UIImageView *imgVV = [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:imgVV];
    [imgVV setImageWithURL:URL(url) placeholder:IMAGENAMED(@"ph") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        imgV.image = image;
    }];
    return imgV;
}
- (void)banner:(ZYBannerView *)banner didSelectItemAtIndex:(NSInteger)index {
    PBViewController *pbViewController = [PBViewController new];
    pbViewController.pb_dataSource = self;
    pbViewController.pb_delegate = self;
    pbViewController.pb_startPage = index;
    self.showContentImg = NO;
    [self presentViewController:pbViewController animated:YES completion:nil];
}

#pragma mark - 图片放大
- (NSInteger)numberOfPagesInViewController:(PBViewController *)viewController {
    return self.showContentImg ? self.contentImgArray.count : self.buyModel.imgurl.count;
}
- (void)viewController:(PBViewController *)viewController presentImageView:(YYAnimatedImageView *)imageView forPageAtIndex:(NSInteger)index progressHandler:(void (^)(NSInteger, NSInteger))progressHandler {
    NSString *url = self.showContentImg ? self.contentImgArray[index] : self.buyModel.imgurl[index];
    [imageView setImageURL:URL(url)];
}
- (void)viewController:(PBViewController *)viewController didSingleTapedPageAtIndex:(NSInteger)index presentedImage:(UIImage *)presentedImage {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    NSString *js = @"function imgAutoFit() { \
    var imgs = document.getElementsByTagName('img'); \
    for (var i = 0; i < imgs.length; ++i) {\
    var img = imgs[i];   \
    img.style.maxWidth = %f;   \
    } \
    }";
    js = [NSString stringWithFormat:js, [UIScreen mainScreen].bounds.size.width - 20];
    
    [webView stringByEvaluatingJavaScriptFromString:js];
    [webView stringByEvaluatingJavaScriptFromString:@"imgAutoFit()"];
    
    
    CGFloat height =  [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue] + 30;
    webView.height = height;
    self.scrollView.contentSize = CGSizeMake(0, height + App_Width + 65 + 64 +30);
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *url = request.URL.absoluteString;
    if([url containsString:@"mt:src"]){
        if (url.length > 7) {
            NSString *urls = [url substringFromIndex:7];
            [self webviewClickAtImage:urls];
        }
        return NO;
        }
    return YES;
}
- (void)webviewClickAtImage:(NSString *)url {
    for (int i = 0; i<self.contentImgArray.count; i++) {
        if ([self.contentImgArray[i] isEqualToString:url]) {
            PBViewController *pbViewController = [PBViewController new];
            pbViewController.pb_dataSource = self;
            pbViewController.pb_delegate = self;
            pbViewController.pb_startPage = i;
            self.showContentImg = YES;
            [self presentViewController:pbViewController animated:YES completion:nil];
        }
    }
}

#pragma mark - 分享
- (void)listMenu {
    self.listView.hidden = !self.listView.hidden;
}
- (void)itemClick:(UIButton *)sender {
    switch (sender.tag - 3000) {
            case 0: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatSession];
            }
            break;
            case 1: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine];
            }
            break;
            case 2: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_QQ];
            }
            break;
            case 3: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_Qzone];
            }
            break;
            case 4:
            case 5:
            SHOW(@"暂不支持此功能");
            break;
        default:
            break;
    }
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    self.listView.hidden = YES;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UIImage *image = self.contentImgArray ? [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.contentImgArray[0]]]] : IMAGENAMED(@"cct");
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: self.wineModel.name  descr:[NSString stringWithFormat:@"每箱%@元,先到先得,速速抢购\n喝了这杯酒,分红年年有",self.wineModel.price] thumImage:image];
    //设置网页地址
    shareObject.webpageUrl = self.share_url;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

- (ListView *)listView {
    
    if (!_listView) {
        _listView = [[ListView alloc] init];
        [_listView initColStatus:NO];
        [self.view addSubview:_listView];
        [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _listView.hidden = YES;
        _listView.delegate = self;
        [_listView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                         initWithActionBlock:^(id  _Nonnull sender) {
                                             _listView.hidden = YES;
                                         }]];
        
    }
    return _listView;
}
@end
