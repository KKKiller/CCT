//
//  CCMyBuyCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyBuyCell.h"
#import "NSMutableAttributedString+Style.h"
@interface  CCMyBuyCell()
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) UIView *sepV;
@property (nonatomic, strong) UIButton *buyBtn;
@property (nonatomic, strong) UIButton *myTGBtn;
@property (nonatomic, strong) UIButton *myPosterBtn;
@property (nonatomic, strong) UIButton *myRankBtn;

@end
@implementation CCMyBuyCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUI];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}



- (void)setModel:(CCMyBuyModel *)model {
    _model = model;
    if (model.good_info.imgurl.count > 0) {
        [self.imgView setImageWithURL:URL(model.good_info.imgurl[0]) placeholder:IMAGENAMED(@"ph")];
    }
    self.titleLbl.text = model.good_info.title;
    NSString *priceText = [NSString stringWithFormat:@"当前购买需要补款 %@元",model.bkj];
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc]initWithText:priceText color:MTRGB(0xff7700) subStrIndex:NSMakeRange(0, 8) subStrColor:MTRGB(0x333333)];
    self.priceLbl.attributedText = attr1;
    
    
    self.priceLbl.hidden = model.finished;
    CGFloat margin = model.finished ? 20 : 0;
    [self.titleLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgView.mas_top).offset(margin);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        make.left.equalTo(self.imgView.mas_right).offset(15);
    }];
    UIColor *backColor = model.finished ? MTRGB(0x9b9b9b) : MTRGB(0x333333);
    UIColor *titleColor = model.finished ? WHITECOLOR : MTRGB(0xff7700);
    NSString *title = model.finished ? @"已完成" :  @"立即补款";
    [self.buyBtn setTitleColor:titleColor forState:UIControlStateNormal];
    [self.buyBtn setBackgroundColor:backColor];
    [self.buyBtn setTitle:title forState:UIControlStateNormal];
}


- (void)setUI {
    self.imgView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.imgView];
    self.imgView.layer.cornerRadius = 4;
    self.imgView.layer.masksToBounds = YES;
    self.imgView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.titleLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.titleLbl];
    self.titleLbl.numberOfLines = 1;
    
    self.priceLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:MTRGB(0xff7700)];
    [self.contentView addSubview:self.priceLbl];
    
    
    self.buyBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.buyBtn];
    self.buyBtn.layer.cornerRadius = 4;
    self.buyBtn.layer.masksToBounds = YES;
    [self.buyBtn setBackgroundColor:MTRGB(0x333333)];
    [self.buyBtn setTitleColor:MTRGB(0xff7700) forState:UIControlStateNormal];
    [self.buyBtn setTitle:@"立即补款" forState:UIControlStateNormal];
    self.buyBtn.titleLabel.font = FFont(16);
    self.buyBtn.tag = 100;
    [self.buyBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.myTGBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.myTGBtn];
    self.myTGBtn.layer.cornerRadius = 4;
    self.myTGBtn.layer.masksToBounds = YES;
    self.myTGBtn.layer.borderColor = MAINBLUE.CGColor;
    self.myTGBtn.layer.borderWidth = 0.5;
    [self.myTGBtn setBackgroundColor:WHITECOLOR];
    [self.myTGBtn setTitleColor:MAINBLUE forState:UIControlStateNormal];
    [self.myTGBtn setTitle:@"我的推广" forState:UIControlStateNormal];
    self.myTGBtn.titleLabel.font = FFont(13);
    self.myTGBtn.tag = 101;
    [self.myTGBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.myPosterBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.myPosterBtn];
    self.myPosterBtn.layer.cornerRadius = 4;
    self.myPosterBtn.layer.masksToBounds = YES;
    self.myPosterBtn.layer.borderColor = MAINBLUE.CGColor;
    self.myPosterBtn.layer.borderWidth = 0.5;
    [self.myPosterBtn setBackgroundColor:WHITECOLOR];
    [self.myPosterBtn setTitleColor:MAINBLUE forState:UIControlStateNormal];
    [self.myPosterBtn setTitle:@"我的海报" forState:UIControlStateNormal];
    self.myPosterBtn.titleLabel.font = FFont(13);
    self.myPosterBtn.tag = 102;
    [self.myPosterBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.myRankBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.myRankBtn];
    self.myRankBtn.layer.cornerRadius = 4;
    self.myRankBtn.layer.masksToBounds = YES;
    self.myRankBtn.layer.borderColor = MAINBLUE.CGColor;
    self.myRankBtn.layer.borderWidth = 0.5;
    [self.myRankBtn setBackgroundColor:WHITECOLOR];
    [self.myRankBtn setTitleColor:MAINBLUE forState:UIControlStateNormal];
    [self.myRankBtn setTitle:@"我的排名" forState:UIControlStateNormal];
    self.myRankBtn.titleLabel.font = FFont(13);
    [self.myRankBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.sepV = [[UIView alloc]init];
    [self.contentView addSubview:self.sepV];
    self.sepV.backgroundColor = MTRGB(0xfafafa);
}
- (void)btnClick:(UIButton *)sender {
        [self.delegate clickAtBtn:sender.tag - 100 orderId:self.model.orderid goodid:self.model.goodid] ;
}
- (void)layoutSubviews {
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView).offset(15);
        make.width.mas_equalTo(110 * KEY_RATE);
        make.height.mas_equalTo(120 * KEY_RATE);
    }];
//    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.imgView.mas_top).offset(0);
//        make.right.equalTo(self.contentView.mas_right).offset(-5);
//        make.left.equalTo(self.imgView.mas_right).offset(15);
//    }];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgView.mas_top).offset(25);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        make.left.equalTo(self.titleLbl.mas_left).offset(0);
    }];
    
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imgView.mas_right).offset(15);
        make.centerY.equalTo(self.imgView.mas_centerY).offset(10);
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(25);
    }];
    [self.myTGBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.titleLbl);
        make.top.equalTo(self.buyBtn.mas_bottom).offset(10);
    }];
    [self.myPosterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.myTGBtn.mas_right).offset(5);
        make.top.equalTo(self.buyBtn.mas_bottom).offset(10);
    }];
    [self.myRankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.myPosterBtn.mas_right).offset(5);
        make.top.equalTo(self.buyBtn.mas_bottom).offset(10);
    }];
    [self.sepV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
}
@end
