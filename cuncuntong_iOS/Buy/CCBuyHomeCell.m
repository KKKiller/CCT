//
//  CCBuyHomeCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/19.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyHomeCell.h"
#import "NSMutableAttributedString+Style.h"
@interface CCBuyHomeCell()
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UIView *titleBackV;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *topPriceLbl;
@property (nonatomic, strong) UILabel *bottomPriceLbl;
@property (nonatomic, strong) UILabel *lineLbl1;
@property (nonatomic, strong) UILabel *lineLbl2;
@property (nonatomic, strong) UIView *sepV;
@end
@implementation CCBuyHomeCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
         [self setUI];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}



- (void)setModel:(CCBuyHomeModel *)model {
    _model = model;
    if (model.imgurl.count > 0) {
        [self.imgView setImageWithURL:URL(model.imgurl[0]) placeholder:IMAGENAMED(@"ph")];
    }
    self.titleLbl.text = model.title;
    NSString *topPrice = [NSString stringWithFormat:@"最高价: ¥%@元",model.max_money];
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc]initWithText:topPrice color:MTRGB(0x333333) subStrIndex:NSMakeRange(0, 4) subStrColor:MTRGB(0x151515)];
    self.topPriceLbl.attributedText = attr1;
    
    NSString *bottomPrice = [NSString stringWithFormat:@"最低价: ¥%@元",model.sfk_money];
    NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc]initWithText:bottomPrice color:MTRGB(0xc31b37) subStrIndex:NSMakeRange(0, 4) subStrColor:MTRGB(0x000000)];
    self.bottomPriceLbl.attributedText = attr2;
}


- (void)setUI {
    self.imgView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.imgView];
    self.imgView.layer.cornerRadius = 4;
    self.imgView.layer.masksToBounds = YES;
    self.imgView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.titleBackV = [[UIView alloc]init];
    [self.contentView addSubview:self.titleBackV];
    self.titleBackV.backgroundColor = MTRGB(0x4A4A4A);
    self.titleBackV.alpha = 0.5;
    
    self.titleLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:WHITECOLOR];
    [self.contentView addSubview:self.titleLbl];
    self.titleLbl.textAlignment = NSTextAlignmentCenter;
    
    self.topPriceLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:MTRGB(0x333333)];
    [self.contentView addSubview:self.topPriceLbl];
    self.topPriceLbl.numberOfLines = 1;
    
    self.bottomPriceLbl = [[UILabel alloc]initWithText:@"" font:16 textColor:MTRGB(0xc31b37)];
    [self.contentView addSubview:self.bottomPriceLbl];
    self.bottomPriceLbl.numberOfLines = 1;
    
    self.lineLbl1 = [[UILabel alloc]initWithText:@"…………………………………………………………………" font:12 textColor:MTRGB(0x9b9b9b)];
    [self.contentView addSubview:self.lineLbl1];
    
    self.lineLbl2 = [[UILabel alloc]initWithText:@"…………………………………………………………………" font:12 textColor:MTRGB(0x9b9b9b)];
    [self.contentView addSubview:self.lineLbl2];
    
    self.buyBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.buyBtn];
    self.buyBtn.layer.cornerRadius = 4;
    self.buyBtn.layer.masksToBounds = YES;
    [self.buyBtn setBackgroundColor:MTRGB(0x333333)];
    [self.buyBtn setTitleColor:MTRGB(0xff7700) forState:UIControlStateNormal];
    [self.buyBtn setTitle:@"立即申购" forState:UIControlStateNormal];
    self.buyBtn.titleLabel.font = FFont(18);
    
    self.sepV = [[UIView alloc]init];
    [self.contentView addSubview:self.sepV];
    self.sepV.backgroundColor = MTRGB(0xfafafa);
}
- (void)layoutSubviews {
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView).offset(15);
        make.width.mas_equalTo(170 * KEY_RATE);
        make.height.mas_equalTo(170 * (416.0/ 500.0) * KEY_RATE);
    }];
    [self.titleBackV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.imgView);
        make.height.mas_equalTo(31 *KEY_RATE);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.titleBackV);
         make.right.equalTo(self.titleBackV.mas_right).offset(-5);
        make.left.equalTo(self.titleBackV.mas_left).offset(5);
    }];
    [self.topPriceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgView.mas_top).offset(5);
        make.left.equalTo(self.imgView.mas_right).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        
    }];
    [self.self.lineLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.topPriceLbl);
        make.height.mas_equalTo(12);
        make.top.equalTo(self.topPriceLbl.mas_bottom);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
    }];
    [self.bottomPriceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.imgView.mas_centerY);
        make.left.equalTo(self.imgView.mas_right).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        
    }];
    [self.self.lineLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bottomPriceLbl);
        make.height.mas_equalTo(12);
        make.top.equalTo(self.bottomPriceLbl.mas_bottom);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
    }];
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.topPriceLbl.mas_left);
        make.bottom.equalTo(self.imgView.mas_bottom).offset(-2);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(30);
    }];
    [self.sepV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
}
@end
