//
//  CCBuyRuleController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/19.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyRuleController.h"

@interface CCBuyRuleController ()
@end

@implementation CCBuyRuleController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 49)];
    [self.view addSubview:scrollView];
    scrollView.contentSize = CGSizeMake(App_Width, 3577.0 / 1080.0 * App_Width);
    
    UIImageView  *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 3577.0 / 1080.0 * App_Width)];
    imgView.image = IMAGENAMED(@"rule");
    [scrollView addSubview:imgView];
    if([[UIDevice currentDevice].systemVersion floatValue] >= 11.0){
        scrollView.frame = CGRectMake(0,64,App_Width,App_Height - 49 - 64);
    }
}

@end
