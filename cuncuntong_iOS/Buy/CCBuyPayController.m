//
//  CCBuyPayController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyPayController.h"
#import "NSMutableAttributedString+Style.h"
#import "CunCunTong-Swift.h"
#import "CCBuyPayView.h"
#import "CCSelectCityController.h"
#import "RechargeViewController.h"
#import "CCTTextField.h"
#import "CCPayController.h"
#import "MYHomeController.h"
@interface CCBuyPayController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *chooseAddressTap;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UITextField *detailField;
    
@property (weak, nonatomic) IBOutlet UILabel *tipsLbl;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;

@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet CCTTextField *numField;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moneyTopMargin;
@property (weak, nonatomic) IBOutlet UIView *numBack;
@property (weak, nonatomic) IBOutlet UILabel *numBottomLbl;
@property (nonatomic, strong) NSString *locationId;
@end

@implementation CCBuyPayController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"支付"];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc]initWithText:@"特别提示：申购不支持反悔退款，\n请确认了规则后再付款。" color:TEXTBLACK6 subStrIndex:NSMakeRange(0, 4) subStrColor:MTRGB(0xff7700)];
    self.tipsLbl.attributedText = attr;
    self.tipsLbl.hidden = self.wineModel != nil;
    
    [self.payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseAddressTap addTarget:self action:@selector(chooseAddress)];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self.view endEditing:YES];
    }];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:tap];
    if (self.buyModel) {
        self.moneyLbl.text = [NSString stringWithFormat:@"金额: %@元",self.buyModel.sfk_money];
    } else if(self.wineModel){
        NSString *price = [NSString stringWithFormat:@"金额: %@元",self.wineModel.price];
        self.moneyLbl.text = price;
    }
    self.moneyTopMargin.constant = self.wineModel ? 80 : 30;
    self.addBtn.hidden = !self.wineModel;
    self.minusBtn.hidden = !self.wineModel;
    self.numField.hidden = !self.wineModel;
    self.numBottomLbl.hidden = !self.wineModel;
    self.numLbl.hidden = !self.wineModel;
    self.numBack.hidden = !self.wineModel;
    self.numField.delegate = self;
    [self.addBtn addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.minusBtn addTarget:self action:@selector(minusBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [IQKeyboardManager sharedManager].enable = YES;
}
- (void)payBtnClick {
    if(self.nameField.text.length == 0) {
        SHOW(@"请输入姓名");
        return;
    }
    if(self.phoneField.text.length == 0) {
        SHOW(@"请输入电话");
        return;
    }
    if([self.addressLbl.text isEqualToString:@"点击选择地址"]) {
        SHOW(@"请选择地址");
        return;
    }
    [self pay];
}
- (void)chooseAddress {
    CCSelectCityController *vc = [[CCSelectCityController alloc]init];
    vc.selectLocBlock = ^(NSString *location, NSString *longAddress, NSString *locationId) {
//        _addressLbl.text = location;
        self.addressLbl.text = longAddress;
        self.locationId = locationId;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)pay {
    CCBuyPayView *view = [CCBuyPayView instanceView];
    [view.payBtn addTarget:self action:@selector(payMoney) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, App_Width - 50, App_Width - 50);
    if (self.wineModel) {
        NSInteger currentNum = [self.numField.text integerValue];
        view.totalPrice = [NSString stringWithFormat:@"%.2f", [self.wineModel.price floatValue] * currentNum];
        view.wineModel = self.wineModel;
    }else{
        view.buyModel = self.buyModel;
    }
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:self.view];
}
- (void)payMoney {
    if (self.wineModel) {
        NSDictionary *param = @{@"uid":USERID,
                                @"wineid":STR(self.wineModel.id),
                                @"vid":STR(self.locationId),
                                @"count":self.numField.text,
                                @"name":self.nameField.text,
                                @"phone":self.phoneField.text,
                                @"address":[NSString stringWithFormat:@"%@%@",self.addressLbl.text,self.detailField.text]};
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"wine/pay") params:param target:self  success:^(NSDictionary *success) {
            if([success[@"code"] integerValue] == 1 ){
                BaseViewController *jindou;
                for (BaseViewController *vc in self.navigationController.viewControllers) {
                    if ([vc isKindOfClass:[MYHomeController class]]) {
                        jindou = vc;
                        break;
                    }
                }
                if (jindou) {
                    SHOW(@"购买成功");
                    [self.navigationController popToViewController:jindou animated:YES];
                    return ;
                }
                for (BaseViewController *vc in self.navigationController.viewControllers) {
                    if ([vc isKindOfClass:[CCWineTabbarController class]]) {
                        [self.navigationController popToViewController:vc animated:NO];
                        [App_Delegate.wineTabbarVc gotoOrder];
                    }
                }
                SHOW(success[@"msg"]);
            }else if([success[@"msg"] isEqualToString:@"余额不足，请充值"]){
                CCPayController *vc =[[ CCPayController alloc]init];
                NSInteger currentNum = [self.numField.text integerValue];
                vc.money = [NSString stringWithFormat:@"%.2f", [self.wineModel.price floatValue] * currentNum];
                vc.paySuccessBlock = ^{
                    [self payMoney];
                };
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                SHOW(success[@"msg"]);
            }
            [self.animateModel closeWithStraight];
        } failure:^(NSError *failure) {
        }];
    }else{
        
        NSDictionary *param = @{@"uid":USERID,
                                @"goodid":STR(self.buyModel.id),
                                @"order_name":self.nameField.text,
                                @"order_phone":self.phoneField.text,
                                @"order_address":[NSString stringWithFormat:@"%@%@",self.addressLbl.text,self.detailField.text]};
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"happybuy/ljsg_pay") params:param target:self  success:^(NSDictionary *success) {
            if([success[@"data"][@"stat"] integerValue] == 1 ){
                BaseViewController *jindou;
                for (BaseViewController *vc in self.navigationController.viewControllers) {
                    if ([vc isKindOfClass:[MYHomeController class]]) {
                        jindou = vc;
                        break;
                    }
                }
                if (jindou) {
                    SHOW(@"购买成功");
                    [self.navigationController popToViewController:jindou animated:YES];
                    return ;
                }
                for (BaseViewController *vc in self.navigationController.viewControllers) {
                    if ([vc isKindOfClass:[CCBuyTabbarController class]]) {
                        [self.navigationController popToViewController:vc animated:NO];
                        [App_Delegate.buyTabbarVc gotoOrder];
                    }
                }
                SHOW(success[@"data"][@"info"]);
            }else if([success[@"data"][@"info"] isEqualToString:@"余额不足，请充值"]){
                CCPayController *vc =[[ CCPayController alloc]init];
                NSInteger currentNum = [self.numField.text integerValue];
                vc.money = [NSString stringWithFormat:@"%.2f", [self.wineModel.price floatValue] * currentNum];
                vc.paySuccessBlock = ^{
                    [self payMoney];
                };
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                SHOW(success[@"msg"]);
            }
            [self.animateModel closeWithStraight];
        } failure:^(NSError *failure) {
        }];
    }
}


- (void)addBtnClick {
    NSInteger currentNum = [self.numField.text integerValue];
    self.numField.text = [NSString stringWithFormat:@"%@",@(currentNum+1)];
    self.moneyLbl.text = [NSString stringWithFormat:@"金额: %.2f元",(currentNum + 1) * [self.wineModel.price floatValue]];
}
- (void)minusBtnClick {
    NSInteger currentNum = [self.numField.text integerValue];
    if (currentNum >= 2) {
        self.numField.text = [NSString stringWithFormat:@"%@",@(currentNum-1)];
        self.moneyLbl.text = [NSString stringWithFormat:@"金额: %.2f元",(currentNum - 1) * [self.wineModel.price floatValue]];
    }else{
        SHOW(@"不能再少了");
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    if ([textField.text integerValue] > 0) {
        self.moneyLbl.text = [NSString stringWithFormat:@"金额: %.2f元",[self.wineModel.price floatValue] * [textField.text integerValue]];
    }
}
@end
