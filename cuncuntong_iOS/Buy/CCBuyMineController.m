//
//  CCBuyMineController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/19.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyMineController.h"
#import "CCMyBuyCell.h"
#import "CCMyRankController.h"
#import "CCBuyPrompController.h"
#import "CCBuyPayView.h"
#import "CunCunTong-Swift.h"
#import "RechargeViewController.h"
#import "CCBuyDetailController.h"
#import "CCPayController.h"
static NSString *cellID = @"CCMyBuyCell";
@interface CCBuyMineController ()<UITableViewDataSource,UITableViewDelegate,CCMyBuyCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, assign) NSInteger pageIndex;
@property (strong, nonatomic) PathDynamicModal *animateModel;

@property (nonatomic, strong) NSString *selectedOrderid;
@property (nonatomic, strong) CCMyBuyModel *selectedModel;
@end
@implementation CCBuyMineController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initRefreshView];
    [self refreshData];
}


- (void)refreshData { //点击tabbar刷新
    self.pageIndex = 1;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"happybuy/my_purchase") params:@{@"uid":USERID,@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"data"][@"info"]) {
                CCMyBuyModel *model = [CCMyBuyModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
        }else{
            SHOW(success[@"data"][@"info"]);
        }
        [self endLoding:success[@"data"][@"info"]];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
//        self.tableView.contentInset = UIEdgeInsetsZero;
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyBuyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.delegate = self;
    if (self.dataArray.count > indexPath.row) {
        CCMyBuyModel *model =self.dataArray[indexPath.row];
        cell.model = model;
        
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCBuyDetailController *vc = [[CCBuyDetailController alloc]init];
        CCMyBuyModel *model = self.dataArray[indexPath.row];
        vc.buyModel = model.good_info;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)clickAtBtn:(NSInteger)index orderId:(NSString *)orderId goodid:(NSString *)goodid{
    self.selectedOrderid = orderId;
    for (CCMyBuyModel *model in self.dataArray) {
        if ([model.orderid isEqualToString:orderId]) {
            self.selectedModel = model;
            break;
        }
    }
    if (index == 0) { //立即补款
        if (!self.selectedModel.finished) {
            [self pay];
        }
        
    }else if (index == 1){ //我的推广
        CCBuyPrompController *vc = [[CCBuyPrompController alloc]init];
        vc.goodid = goodid;
        vc.qi_id = self.selectedModel.id;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (index == 2){//我的海报
        CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
        for (CCMyBuyModel *model in self.dataArray) {
            if ([model.orderid isEqualToString:orderId] && model.arurl) {
                NSString *url = [NSString stringWithFormat:@"%@&u=%@",model.arurl,USERID];
                vc.urlStr = url;
                vc.shareURL = url;
//                vc.isPoster = YES;
                if (model.good_info.imgurl.count > 0) {
                    vc.share_image = model.good_info.imgurl[0];
                }
                vc.share_title =  model.good_info.title;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
        }
    }else{//我的排行
        CCMyRankController *vc = [[CCMyRankController alloc]init];
        vc.goodid = goodid;
        vc.qi_id = self.selectedModel.id;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)pay {
    CCBuyPayView *view = [CCBuyPayView instanceView];
    [view.payBtn addTarget:self action:@selector(payMoney) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, App_Width - 50, App_Width - 50);
    for (CCMyBuyModel *model in self.dataArray) {
        if ([model.orderid isEqualToString:self.selectedOrderid]) {
            view.titleLbl.text = model.good_info.title;
            if (model.good_info.imgurl.count > 0) {
                [view.imgView setImageWithURL:URL(model.good_info.imgurl[0]) placeholder:IMAGENAMED(@"ph")];
            }
            view.priceLbl.text = [NSString stringWithFormat:@"¥%@元",model.bkj];
            self.animateModel = [[PathDynamicModal alloc]init];
            self.animateModel.closeBySwipeBackground = YES;
            self.animateModel.closeByTapBackground = YES;
            [self.animateModel showWithModalView:view inView:self.view];
            break;
        }
    }
}
- (void)payMoney {
    for (CCMyBuyModel *model in self.dataArray) {
        if ([model.orderid isEqualToString:self.selectedOrderid]) {
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"happybuy/bkj_pay") params:@{@"orderid":model.orderid,@"uid":USERID,@"bkj":model.bkj} target:self  success:^(NSDictionary *success) {
                if([success[@"data"][@"stat"] integerValue] == 1 ){
                    [self refreshData];
                    SHOW(success[@"data"][@"info"]);
                }else if([success[@"data"][@"info"] isEqualToString:@"余额不足，请充值"]){
//                    RechargeViewController *vc = [[RechargeViewController alloc]init];
//                    [self.navigationController pushViewController:vc animated:YES];
                    CCPayController *vc =[[ CCPayController alloc]init];
                    vc.money = model.bkj;
                    vc.paySuccessBlock = ^{
                        [self pay];
                    };
                    [self.navigationController pushViewController:vc animated:YES];
                }
                [self.animateModel closeWithStraight];
            } failure:^(NSError *failure) {
            }];
            break;
        }
    }
}

#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 49)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 130;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CCMyBuyCell class] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 64 - 49)];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
