//
//  CCBuyDetailController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCBuyHomeModel.h"
#import "CCWineModel.h"
@interface CCBuyDetailController : BaseViewController
@property (nonatomic, strong) CCBuyHomeModel *buyModel;
@property (nonatomic, strong) CCWineModel *wineModel;

@end
