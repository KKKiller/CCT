//
//  CCBuyPrompModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CCBuyPrompModel;
@interface CCBuyPrompModel : NSObject
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *portrait;
@property (nonatomic, strong) CCBuyPrompModel *xiaxid1_info;
@property (nonatomic, strong) CCBuyPrompModel *xiaxid2_info;
@property (nonatomic, strong) CCBuyPrompModel *xiaxid3_info;
@end
