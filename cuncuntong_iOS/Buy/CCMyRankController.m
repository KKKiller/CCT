//
//  CCMyRankController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyRankController.h"
#import "CCRankCell.h"
static NSString *cellID = @"CCRankCell";
static CGFloat bannerHeight = 150;
@interface CCMyRankController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) CCBuyRankModel *myRankModel;
@end
@implementation CCMyRankController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"我的排名"];
    [self initRefreshView];
    [self refreshData];
}


- (void)refreshData { //点击tabbar刷新
    self.pageIndex = 1;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"happybuy/my_rank") params:@{@"uid":USERID,@"goodid":STR(self.goodid),@"qi_id":STR(self.qi_id)} target:nil success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            self.myRankModel = [CCBuyRankModel modelWithJSON:success[@"data"][@"info"][@"first"]];
            
            for (NSDictionary *dict in success[@"data"][@"info"][@"second"]) {
                CCBuyRankModel *model = [CCBuyRankModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
        }else{
            SHOW(success[@"data"][@"info"]);
        }
        [self endLoding:nil];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section == 0 ? 1 : self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCRankCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
   
    if (indexPath.section == 0) {
//        cell.index = indexPath.row;
        cell.model = self.myRankModel;
    }else{
//        cell.index = indexPath.row;
        if (self.dataArray.count > indexPath.row) {
            cell.model = self.dataArray[indexPath.row];
        }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return section == 0 ? 10 : CGFLOAT_MIN;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        
    }
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 60;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CCRankCell class] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}



@end
