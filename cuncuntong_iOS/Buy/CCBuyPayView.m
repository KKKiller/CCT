//
//  CCBuyPayView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyPayView.h"

@implementation CCBuyPayView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}
- (void)setBuyModel:(CCBuyHomeModel *)buyModel {
    _buyModel = buyModel;
    self.titleLbl.text = buyModel.title;
    self.priceLbl.text = [NSString stringWithFormat:@"¥%@元",buyModel.sfk_money];
    if (buyModel.imgurl.count > 0) {
        [self.imgView setImageWithURL:URL(buyModel.imgurl[0]) placeholder:IMAGENAMED(@"SubPlaceholder")];
    }
}
- (void)setWineModel:(CCWineModel *)wineModel {
    _wineModel = wineModel;
    self.titleLbl.text = wineModel.name;
    self.priceLbl.text = [NSString stringWithFormat:@"¥%@元",self.totalPrice];
    [self.imgView setImageWithURL:URL(wineModel.image) placeholder:IMAGENAMED(@"SubPlaceholder")];
}
@end
