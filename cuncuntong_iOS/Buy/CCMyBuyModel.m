//
//  CCMyBuyModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyBuyModel.h"

@implementation CCMyBuyModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"good_info" : [CCBuyHomeModel class]};
}
- (BOOL)finished {
    return [self.order_stat isEqualToString:@"2"];
}
@end
