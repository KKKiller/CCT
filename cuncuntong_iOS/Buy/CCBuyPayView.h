//
//  CCBuyPayView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCBuyHomeModel.h"
#import "CCWineModel.h"
@interface CCBuyPayView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
+ (instancetype)instanceView;

@property (nonatomic, strong) CCBuyHomeModel *buyModel;
@property (nonatomic, strong) NSString *totalPrice;
@property (nonatomic, strong) CCWineModel *wineModel;
@end
