//
//  CCBuyPrompModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCBuyPrompModel.h"

@implementation CCBuyPrompModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"xiaxid1_info" : [CCBuyPrompModel class],@"xiaxid2_info" : [CCBuyPrompModel class],@"xiaxid3_info" : [CCBuyPrompModel class]};
}
@end
