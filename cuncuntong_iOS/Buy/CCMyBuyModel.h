//
//  CCMyBuyModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/22.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCBuyHomeModel.h"
@interface CCMyBuyModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *orderid;
@property (nonatomic, strong) NSString *goodid;
@property (nonatomic, strong) NSString *order_stat; //0 未付款 1付首付 2补了全款
@property (nonatomic, strong) NSString *bkj; //补款价
@property (nonatomic, strong) CCBuyHomeModel *good_info;
@property (nonatomic, assign) BOOL finished;
@property (nonatomic, strong) NSString *arurl;
@property (nonatomic, strong) NSString *qi_id;

@end
