//
//  CCRedListHeaderView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCRedListHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *redCountLbl;
+ (instancetype)instanceView ;
@end
