//
//  CCOpenRedView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCOpenRedView.h"

@implementation CCOpenRedView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}

- (void)setIsOver:(BOOL)isOver {
    _isOver = isOver;
    self.openBtn.hidden = isOver;
    self.openTitle.hidden = isOver;
    self.showDetailBtn.hidden = !isOver;
    self.showDetailArrow.hidden = !isOver;
}
@end
