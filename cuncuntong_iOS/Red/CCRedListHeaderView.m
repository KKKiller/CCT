//
//  CCRedListHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/8/6.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCRedListHeaderView.h"

@implementation CCRedListHeaderView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}

@end
