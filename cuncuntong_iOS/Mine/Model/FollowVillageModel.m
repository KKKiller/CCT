//
//  FollowVillageModel.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/13.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "FollowVillageModel.h"

@implementation FollowVillageModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"villageId" : @"id"};
}

@end
