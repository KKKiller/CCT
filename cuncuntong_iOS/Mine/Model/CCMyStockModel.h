//
//  CCMyStockModel.h
//  TestDemo
//
//  Created by 周吾昆 on 2017/9/8.
//  Copyright © 2017年 周吾昆. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCMyStockModel : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *count;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) CGFloat percentage;
- (instancetype)initWithName:(NSString *)name count:(NSString *)count color:(UIColor *)color;
@end
