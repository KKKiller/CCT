//
//  CCMyStockModel.m
//  TestDemo
//
//  Created by 周吾昆 on 2017/9/8.
//  Copyright © 2017年 周吾昆. All rights reserved.
//

#import "CCMyStockModel.h"

@implementation CCMyStockModel
- (instancetype)initWithName:(NSString *)name count:(NSString *)count color:(UIColor *)color{
    if (self = [super init]) {
        _name = name;
        _count = count;
        _color = color;
    }
    return self;
}
@end
