//
//  CommentModel.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/14.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"articleId" : @"id"};
}
@end
