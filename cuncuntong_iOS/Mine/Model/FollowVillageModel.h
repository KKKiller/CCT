//
//  FollowVillageModel.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/13.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FollowVillageModel : NSObject

@property (nonatomic, copy) NSString *villageId;
@property (nonatomic, copy) NSString *val;

@end
