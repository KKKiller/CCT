//
//  SystemNewsModel.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/29.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemNewsModel : NSObject
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *msg;
@property(nonatomic,copy)NSString *create;
@property (nonatomic, assign) BOOL unread_id;
@property (nonatomic, strong) NSString *id;

@end
