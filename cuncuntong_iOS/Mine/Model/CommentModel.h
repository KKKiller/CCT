//
//  CommentModel.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/14.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject

@property (nonatomic, copy) NSString *articleId;
@property (nonatomic, copy) NSString *mini;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *village;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *create;
@property (nonatomic, copy) NSString *iscol;

@end
