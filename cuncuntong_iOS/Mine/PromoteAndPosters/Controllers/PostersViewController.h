//
//  PostersViewController.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import <WebKit/WebKit.h>
/**
 Description 我的海报
 */
@interface PostersViewController : BaseViewController
@property(nonatomic,strong)WKWebView *webView;
@end
