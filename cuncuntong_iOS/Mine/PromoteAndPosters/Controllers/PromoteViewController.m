//
//  PromoteViewController.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "PromoteViewController.h"
#import "Define.h"

#import "PromoteHeaderView.h"
#import "PromoteFooterView.h"
#import "PromoteFirstTableViewCell.h"
#import "CCPromoteTopBtn.h"
static NSString * const CellID1 = @"CellID1";
static NSString * const CellID2 = @"CellID2";

@interface PromoteViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)PromoteHeaderView *pHeaderView;
@property(nonatomic,strong)PromoteFooterView *pFooterView;

@property(nonatomic,strong)NSMutableDictionary *dictionary_promote;
@property (nonatomic, strong) CCPromoteTopBtn *topView;
@end

@implementation PromoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"我的推广"];
    [self addReturnBtn:@"tui_"];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView.mj_header beginRefreshing];
    
    self.tableView.tableHeaderView = self.pHeaderView;
    self.tableView.tableFooterView = self.pFooterView;
    
    self.topView = [CCPromoteTopBtn  instanceView];
    self.topView.frame = CGRectMake(0, 64, App_Width, 60);
    [self.view addSubview:self.topView];
    
    [self.topView.jindouBtn  addTarget:self action:@selector(jindouBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView.circleBtn  addTarget:self action:@selector(circleBtnClick) forControlEvents:UIControlEventTouchUpInside];
    WEAKSELF
    [self.topView.jindou addActionBlock:^(id  _Nonnull sender) {
        [weakSelf jindouBtnClick];
    }];
    [self.topView.circle addActionBlock:^(id  _Nonnull sender) {
        [weakSelf circleBtnClick];
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)jindouBtnClick {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.topOffset = -40;
    vc.titleStr = @"种金豆";
    vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/web/beans?rid=%@",USERID];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)circleBtnClick {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.topOffset = -40;
    vc.titleStr = @"生活圈";
    vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/web/live?rid=%@&type=merchant",USERID];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)loadData{
    NSDictionary *parDic = @{@"rid" : USERID};
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"center/tjlog") params:parDic success:^(NSDictionary *success) {
        NSLog(@"success = %@",success);
        self.dictionary_promote = [success objectForKey:@"data"];
        [self.tableView.mj_header endRefreshing];
    } failure:^(NSError *failure) {
        [self.tableView.mj_header endRefreshing];
    }];
}

#pragma mark - uitableview datasouce
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        PromoteFirstTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID1];
//        if ([[self.dictionary_promote objectForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
//            cell.dictionary_data = [self.dictionary_promote objectForKey:@"data"];
//        }else{
//            cell.label_1.text =@"推荐的用户";// @"第1级";
//        }
        cell.label_1.text =@"邀请二维码";
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID2];
        cell.textLabel.font = [UIFont systemFontOfSize:13];
        if ([[self.dictionary_promote objectForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dic = [self.dictionary_promote objectForKey:@"data"];
            NSDictionary *dic2 = [dic objectForKey:[NSString stringWithFormat:@"%li",indexPath.row + 1]];
            if ([[dic2 objectForKey:@"num"] integerValue] > 0) {
                cell.textLabel.text = [NSString stringWithFormat:@"第%li级   %li人",indexPath.row + 1,[[dic2 objectForKey:@"num"] integerValue]];
            }else{
                cell.textLabel.text = [NSString stringWithFormat:@"第%li级",indexPath.row +1];
            }
            
        }else{
            cell.textLabel.text = [NSString stringWithFormat:@"第%li级",indexPath.row +1];
        }
        cell.textLabel.text = @"邀请二维码";
        return cell;
    }
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 1.0f;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row == 0) {
//
//        if ([[self.dictionary_promote objectForKey:@"data"]isKindOfClass:[NSDictionary class]]) {
//            NSDictionary *d1 = [self.dictionary_promote objectForKey:@"data"];
//            NSDictionary *dic = [d1 objectForKey:@"1"];
//            NSArray *array = [dic objectForKey:@"readers"];
//
//            if (array.count > 0) {
//                NSString *realname = @"";
//                for (int i = 0;i<array.count;i++) {
//                    NSDictionary *di = [array objectAtIndex:i];
//                    NSString *reader = [di objectForKey:@"realname"];
//                    if (i%2) {
//                        realname = [realname stringByAppendingString:reader];
//                    }else{
//                        realname = [realname stringByAppendingString:[NSString stringWithFormat:@"  %@\n",reader]];
//                    }
//                }
//                CGSize size = [realname boundingRectWithSize:CGSizeMake(KEY_WIDTH - 120, MAXFLOAT)options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size;
//                return size.height + 20;
//        }
//            return 40;
//
//        }else{
//            return 40;
//        }
//    }else{
        return 40;
//    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  App_Width + 20;
}
#pragma mark - setters and getters

-(void)setDictionary_promote:(NSMutableDictionary *)dictionary_promote{
    _dictionary_promote = dictionary_promote;
    
    NSString *num = [NSString stringWithFormat:@"%@",[_dictionary_promote objectForKey:@"total"]];
    NSString *money = [NSString stringWithFormat:@"%@",[_dictionary_promote objectForKey:@"money"]];
    NSString *phone = [NSString stringWithFormat:@"%@",[_dictionary_promote objectForKey:@"mobile"]];
    [self.pHeaderView PromoteHeaderViewNum:num andMoney:money phone:phone];
    
    [self.tableView reloadData];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64 + 60, KEY_WIDTH, KEY_HEIGHT - 64 - 60) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[PromoteFirstTableViewCell class] forCellReuseIdentifier:CellID1];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellID2];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    }
    return _tableView;
}

-(PromoteHeaderView *)pHeaderView{
    if (!_pHeaderView) {
        _pHeaderView = [[PromoteHeaderView alloc]init];
    }
    return _pHeaderView;
}

-(PromoteFooterView *)pFooterView{
    if (!_pFooterView) {
        _pFooterView = [[PromoteFooterView alloc]init];
    }
    return _pFooterView;
}
@end
