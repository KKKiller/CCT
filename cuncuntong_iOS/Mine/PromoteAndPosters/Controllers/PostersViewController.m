//
//  PostersViewController.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "PostersViewController.h"
#import "ShareListView.h"

@interface PostersViewController ()<ShareListViewDelegate>
@property (nonatomic, strong) UIImageView *imgView;


@property(nonatomic,strong)UIProgressView *progressView;
@property(nonatomic,strong)ShareListView *shareView;
@property(nonatomic,strong)NSString *share_url;
@end

@implementation PostersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"我的海报"];
    [self addReturnBtn:@"tui_"];
    
    self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Width * (826.0/804.0))];
    [self.view addSubview:self.imgView];
    self.imgView.image = IMAGENAMED(@"myPoster");
    
    
//    self.view.backgroundColor =  RGB(238, 238, 238);
//    [self navConfig];
//    self.navigationItem.title = @"我的海报";
//    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"xialacaidan"] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemClick)];
//
//    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/article?p=21252B49A345854072778171E626CFFC&u=%@",USERID];//http://www.cct369.com/village/public/article?p=21252B49A345854072778171E626CFFC&u=
//    //http://www.cct369.com/village/public/center/qrcode?rid=%@
//    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
//    
//    [self.view addSubview:self.webView];
//    
//    [self.view addSubview:self.progressView];
//    [self.view insertSubview:self.webView belowSubview:self.progressView];
//    
//    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
}


//- (void)viewWillAppear:(BOOL)animated {
//    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
//    
//}

/**
 Description 下拉菜单
 */


//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
//    
//    if ([keyPath isEqualToString:@"estimatedProgress"]) {
//        self.progressView.hidden = self.webView.estimatedProgress == 1.0;
//        [self.progressView setProgress:(float)_webView.estimatedProgress animated:YES];
//    }
//}
//- (void)dealloc {
//    
//    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
//}
//
//- (void)navConfig {
//    UIImage * backImage = [[UIImage imageNamed:@"arrow_blue"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    //添加左边按钮
//    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE)];
//    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//    [backButton setImage:backImage forState:UIControlStateNormal];
//    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = backButtonItem;
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//}
//- (void)back {
//    [self.navigationController popViewControllerAnimated:YES];
//}
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

#pragma mark - 下拉菜单方法
-(void)rightBarButtonItemClick{
    self.shareView.hidden = !self.shareView.hidden;
}
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    self.shareView.hidden = YES;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UIImage *image = IMAGENAMED(@"myPoster");
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: @"我的海报"  descr:@"——来自全球村村通APP" thumImage:image];
    //设置网页地址
    shareObject.webpageUrl = self.share_url;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}
#pragma mark - ShareListViewDelegate
-(void)itemClick:(UIButton *)sender{
    switch (sender.tag - 3000) {
            case 0: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatSession];
            }
            break;
            case 1: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine];
            }
            break;
            case 2: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_QQ];
            }
            break;
            case 3: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_Qzone];
            }
            break;
        default:
            break;
    }

}

#pragma mark - setters and getters
//-(WKWebView *)webView{
//    if (!_webView) {
//        _webView = [[WKWebView alloc]init];
//        _webView.frame = CGRectMake(0, 64, KEY_WIDTH, KEY_HEIGHT - 64);//self.view.frame;
//    }
//    return _webView;
//}
//
//-(NSString *)share_url{
//    if (!_share_url) {
//        //http://www.cct369.com/village/public/article?p=21252B49A345854072778171E626CFFC&u=
//        //http://www.cct369.com/village/public/center/qrcode?rid=
//        _share_url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/article?p=21252B49A345854072778171E626CFFC&u=%@",USERID];
//    }
//    return _share_url;
//}
//
//-(UIProgressView *)progressView{
//    if (!_progressView) {
//        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, KEY_WIDTH, 0)];
//        _progressView.progressTintColor = [UIColor greenColor];
//        _progressView.transform = CGAffineTransformMakeScale(1.0f,2.0f);
//    }
//    return _progressView;
//}

- (ShareListView *)shareView {
    
    if (!_shareView) {
        _shareView = [[ShareListView alloc]initWithShareTypes:@[[NSNumber numberWithInteger:Share_weixin],[NSNumber numberWithInteger:Share_pengyouquan],[NSNumber numberWithInteger:Share_qq],[NSNumber numberWithInteger:Share_qqkj]]];
        [self.view addSubview:_shareView];
        [self.shareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _shareView.hidden = YES;
        _shareView.delegate = self;
        [_shareView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                          initWithActionBlock:^(id  _Nonnull sender) {
                                              _shareView.hidden = YES;
                                          }]];
        
    }
    return _shareView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
