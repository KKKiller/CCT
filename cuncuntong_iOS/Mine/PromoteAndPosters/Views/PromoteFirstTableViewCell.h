//
//  PromoteFirstTableViewCell.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 第一级 cell
 */
@interface PromoteFirstTableViewCell : UITableViewCell
@property(nonatomic,strong)UILabel *label_1;
@property(nonatomic,strong)UITextView *textView;

@property(nonatomic,strong)NSDictionary *dictionary_data;
@end
