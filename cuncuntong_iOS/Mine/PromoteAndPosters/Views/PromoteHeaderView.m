//
//  PromoteHeaderView.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "PromoteHeaderView.h"
#import "Define.h"

@implementation PromoteHeaderView

-(instancetype)init{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, KEY_WIDTH, 40);
        self.backgroundColor = [UIColor whiteColor];
//        [self addSubview:self.label_num];
//        [self addSubview:self.label_money];
        [self addSubview:self.inviterPhoneLbl];
    }
    return self;
}

-(void)PromoteHeaderViewNum:(NSString *)num andMoney:(NSString *)money phone:(NSString *)phone{
    
    
//    if ([num integerValue]>0) {
//        NSMutableAttributedString *nums = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"总人数:  %@人",num]];
//        [nums addAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(6, num.length)];
//        self.label_num.attributedText = nums;
//    }else{
//        self.label_num.text = @"总人数:  0人";
//    }
//
//    if ([money floatValue]) {
//        NSMutableAttributedString *moneys = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"总收益:  %@元",money]];
//        [moneys addAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(6, money.length)];
//        self.label_money.attributedText = moneys;
//    }else{
//        self.label_money.text = @"总收益:  0元";
//    }
    if (phone.length > 0) {
        self.inviterPhoneLbl.text =  [NSString stringWithFormat:@"上级邀请人: %@",phone];
    }
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
//    self.label_num.frame = CGRectMake(10, 10, KEY_WIDTH - 20, 30);
//    self.label_money.frame = CGRectMake(10, CGRectGetMaxY(self.label_num.frame), KEY_WIDTH - 20, 30);
    self.inviterPhoneLbl.frame = CGRectMake(10, 10, KEY_WIDTH - 20, 30);
}

#pragma mark - setters and getters
-(UILabel *)label_num{
    if (!_label_num) {
        _label_num = [[UILabel alloc]init];
        _label_num.font = FontSize(15);
        _label_num.text = @"总人数:";
    }
    return _label_num;
}
-(UILabel *)label_money{
    if (!_label_money) {
        _label_money = [[UILabel alloc]init];
        _label_money.font = FontSize(15);
        _label_money.text = @"总收益:";
    }
    return _label_money;
}
- (UILabel *)inviterPhoneLbl {
    if (_inviterPhoneLbl == nil) {
        _inviterPhoneLbl = [[UILabel alloc]init];
        _inviterPhoneLbl.font = FontSize(15);
        _inviterPhoneLbl.text  = @"上级邀请人:";
    }
    return _inviterPhoneLbl;
}
@end
