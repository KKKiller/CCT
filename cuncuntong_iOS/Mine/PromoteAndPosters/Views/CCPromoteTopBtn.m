//
//  CCPromoteTopBtn.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/20.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCPromoteTopBtn.h"

@implementation CCPromoteTopBtn
+ (instancetype)instanceView {
    CCPromoteTopBtn *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    return view;
}

@end
