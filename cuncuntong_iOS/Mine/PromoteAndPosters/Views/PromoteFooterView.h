//
//  PromoteFooterView.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoteFooterView : UIView
@property(nonatomic,strong)UITextView *textView;
@property(nonatomic,strong)UILabel *label;
@property(nonatomic,strong)NSMutableAttributedString *attributeStr;
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *userIdLbl;
@end
