//
//  PromoteHeaderView.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Description 推广 header view
 */
@interface PromoteHeaderView : UIView

/**
 Description 总人数
 */
@property(nonatomic,strong)UILabel *label_num;

/**
 Description 总收入
 */
@property(nonatomic,strong)UILabel *label_money;

@property (nonatomic, strong) UILabel *inviterPhoneLbl;
/**
 Description 显示 人数 和 收益

 @param num 人数
 @param money 收益
 */
-(void)PromoteHeaderViewNum:(NSString *)num andMoney:(NSString *)money phone:(NSString *)phone;
@end
