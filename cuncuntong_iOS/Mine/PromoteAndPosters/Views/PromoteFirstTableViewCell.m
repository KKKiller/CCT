//
//  PromoteFirstTableViewCell.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "PromoteFirstTableViewCell.h"

@implementation PromoteFirstTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.label_1];
        [self.contentView addSubview:self.textView];
    }
    return self;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.label_1.frame = CGRectMake(15, 5, 90, 25);
    
//    self.textView.frame = CGRectMake(CGRectGetMaxX(self.label_1.frame) + 5, 5, KEY_WIDTH - CGRectGetMaxX(self.label_1.frame) -15, 25);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - setter and getter

-(void)setDictionary_data:(NSDictionary *)dictionary_data{
    _dictionary_data = dictionary_data;
    NSDictionary *dic = [_dictionary_data objectForKey:@"1"];
//    NSLog(@"%@",dic);
    if ([[dic objectForKey:@"num"] integerValue] > 0) {
        self.label_1.text = [NSString stringWithFormat:@"第1级   %li人",[[dic objectForKey:@"num"] integerValue]];
    }else{
        self.label_1.text = [NSString stringWithFormat:@"第1级"];
    }
//    NSString *s = [NSString stringWithFormat:@"第1级  %@人",[dic objectForKey:@"num"]];
//    self.label_1.text = s;
    
    NSArray *array = [dic objectForKey:@"readers"];
    if (array.count > 0) {
        NSString *realname = @"";
        for (int i = 0;i<array.count;i++) {
            NSDictionary *di = [array objectAtIndex:i];
            NSString *reader = [di objectForKey:@"realname"];
            if (i%2) {
               realname = [realname stringByAppendingString:reader];
            }else{
                realname = [realname stringByAppendingString:[NSString stringWithFormat:@"  %@\n",reader]];
            }
        }
//        NSLog(@"realname = %@",realname);
        
        CGSize size = [realname boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.textView.frame), MAXFLOAT)options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size;
        CGRect  frame = self.textView.frame;
        frame.size.height = size.height + 10;
        
//        NSLog(@"height = %f",size.height);
        self.textView.frame = CGRectMake(CGRectGetMaxX(self.label_1.frame) + 5, 5, KEY_WIDTH - CGRectGetMaxX(self.label_1.frame) -15, size.height + 10);
        self.textView.text = realname;
    }
}

-(UILabel *)label_1{
    if (!_label_1) {
        _label_1 = [[UILabel alloc]init];
        _label_1.font = FontSize(13);
        _label_1.text = @"第1级";
    }
    return _label_1;
}

-(UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.userInteractionEnabled = NO;
        _textView.font = [UIFont systemFontOfSize:12];
        _textView.textColor = [UIColor grayColor];
//        _textView.backgroundColor = [UIColor blackColor];
    }
    return _textView;
}

@end
