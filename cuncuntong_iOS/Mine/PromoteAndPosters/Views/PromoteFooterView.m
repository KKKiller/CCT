//
//  PromoteFooterView.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "PromoteFooterView.h"
#import "Define.h"

@implementation PromoteFooterView

-(instancetype)init{
    self = [super init];
    if (self) {
        CGFloat imgW = App_Width - 40;
        self.frame = CGRectMake(0, 0, KEY_WIDTH,  imgW + 60);
        self.imgView.frame = CGRectMake(20, 0, imgW,App_Width + 20);
        self.backgroundColor = [UIColor whiteColor];
//        self.label.frame = CGRectMake(0, imgW + 20, CGRectGetWidth(self.frame), 30);
//        self.textView.frame = CGRectMake(12, CGRectGetMaxY(self.label.frame), CGRectGetWidth(self.frame)-24, CGRectGetHeight(self.frame) - 30);
//        [self addSubview:self.label];
//        [self addSubview:self.textView];
        [self addSubview:self.imgView];
        [self addSubview:self.userIdLbl];
        self.userIdLbl.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame), App_Width, 40);
//        self.textView.attributedText = self.attributeStr;
    }
    return self;
}

#pragma mark - setters and getters
-(NSMutableAttributedString *)attributeStr{
    if (!_attributeStr) {

        NSString *s1 = @"点击生成海报,把海报发给朋友。让其扫码下载，教会其注册和继续推广的方法";
        _attributeStr = [[NSMutableAttributedString alloc]initWithString:@"点击生成海报,把海报发给朋友。让其扫码下载，教会其注册和继续推广的方法\n\n推荐一人下载，有望得到推广佣金0.03*3^12=1.5万元\n\n推荐一人下载，有望得到推广佣金0.03*3^13=13万元\n\n推荐一人下载，有望得到推广佣金0.03*3^16=129万元\n\n推荐一人下载，有望得到推广佣金0.03*3^18=1100万元"];
        [_attributeStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],
                                       NSForegroundColorAttributeName:[UIColor colorWithRed:72/255.0f green:56/255.0f blue:191/255.0f alpha:1.0f]} range:NSMakeRange(0, s1.length)];
    }
    return _attributeStr;
}

-(UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.userInteractionEnabled = NO;
        _textView.font = [UIFont systemFontOfSize:11];
        _textView.textAlignment = NSTextAlignmentCenter;
    }
    return _textView;
}

-(UILabel *)label{
    if (!_label) {
        _label = [[UILabel alloc]init];
        _label.font = FontSize(18);
        _label.text = @"10亿推广佣金抢占攻略";
        _label.textAlignment = NSTextAlignmentCenter;
    }
    return _label;
}
-(UILabel *)userIdLbl{
    if (!_userIdLbl) {
        _userIdLbl = [[UILabel alloc]init];
        _userIdLbl.font = FontSize(18);
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"注册邀请码：%@",USERID]];
        [attr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18],
                                       NSForegroundColorAttributeName:[UIColor colorWithRed:72/255.0f green:56/255.0f blue:191/255.0f alpha:1.0f]} range:NSMakeRange(0, 6)];
        _userIdLbl.attributedText = attr;
        _userIdLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _userIdLbl;
}
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [[UIImageView alloc]init];
        NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/center/qrcode2?rid=%@&just_qrcode",USERID];
        [_imgView setImageWithURL:URL(url) placeholder:nil];
    }
    return _imgView;
}
@end
