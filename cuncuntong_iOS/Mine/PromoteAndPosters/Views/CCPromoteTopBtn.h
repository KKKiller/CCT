//
//  CCPromoteTopBtn.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/20.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCPromoteTopBtn : UIView
@property (weak, nonatomic) IBOutlet UIButton *jindouBtn;
@property (weak, nonatomic) IBOutlet UIButton *circleBtn;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *jindou;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *circle;
+ (instancetype)instanceView ;
@end
