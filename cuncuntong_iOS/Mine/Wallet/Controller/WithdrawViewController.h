//
//  WithdrawViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@protocol WithdrawViewControllerDelegate <NSObject>
@optional

/**
 提现成功回调

 @param money 提现的金额
 */
-(void)WithdrawViewControllerSuccessOnMoney:(CGFloat)money;

@end
@interface WithdrawViewController : BaseViewController

@property (nonatomic, assign) CGFloat maxMoney;

@property(nonatomic,weak)id<WithdrawViewControllerDelegate>wDelegate;

@property (nonatomic, strong) NSString *aliName;
@property (nonatomic, strong) NSString *aliAccount;
@end
