//
//  RecordViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, RecordType) {
    RecordTypeRecharge,
    RecordTypeWithdraw,
    RecordShare
};

@interface RecordViewController : BaseViewController

@property (nonatomic, assign) RecordType recordType;

@end
