//
//  WithdrawViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "WithdrawViewController.h"
#import "CCTButton.h"
#import "MoneyTextField.h"
#import "RecordViewController.h"

@interface WithdrawViewController ()<UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *_dataSource;
    UITableView *_tableView;
    NSArray *_iconArray;
    NSArray *_titleArray;
    MoneyTextField *_moneyTF;
    NSInteger _currentPayType;
    MoneyTextField *_accountTF;
    MoneyTextField *_nameTF;
}

@end

static NSString *const cellIdentifier = @"cellIdentifier";

@implementation WithdrawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    UIImage * backImage = [[UIImage imageNamed:@"arrow_blue"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    //添加左边按钮
//    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE)];
//    
//    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//    
//    [backButton setImage:backImage forState:UIControlStateNormal];
//    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = backButtonItem;
//    
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//    self.navigationItem.title = @"提现";
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"提现"];
    
    [self setData];
    [self uiConfig];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)setData {
    _iconArray = @[@"btn_zfb"];//, @"cz_btn_weixin"];
    _titleArray = @[@"支付宝提现"];//, @"微信提现"];
    
    _dataSource = [[NSMutableArray alloc] initWithArray:@[@YES, @NO]];
    _currentPayType = 0;
}

- (void)uiConfig {
    
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [self tableFooter];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT-64);
    }];
    _tableView.backgroundColor = RGB(238, 238, 238);
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = _titleArray[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    cell.imageView.image = [UIImage imageNamed:_iconArray[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.accessoryType = [_dataSource[indexPath.row] boolValue] ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50 * KEY_RATE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    for (NSInteger i = 0; i < _dataSource.count; i++) {
        _dataSource[i] = @NO;
    }
    _dataSource[indexPath.row] = @YES;
    _currentPayType = indexPath.row;
    [_moneyTF resignFirstResponder];
    [tableView reloadData];
    [_moneyTF becomeFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  10 * KEY_RATE;
}

- (UIView *)tableFooter {
    UIView *footer = [[UIView alloc] init];
    footer.frame = CGRectMake(0, 0, KEY_WIDTH, 280 * KEY_RATE);
    
    _accountTF = [[MoneyTextField alloc] init];
    [footer addSubview:_accountTF];
    _accountTF.backgroundColor = [UIColor whiteColor];
    [_accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(footer.mas_top).with.offset(10 * KEY_RATE);
        make.left.right.equalTo(footer);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    UILabel *subTitleLabel = [[UILabel alloc] init];
    subTitleLabel.frame = CGRectMake(0, 0, 63 * KEY_RATE, 50 * KEY_RATE);
    subTitleLabel.text = @"帐号";
    subTitleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    _accountTF.leftView = subTitleLabel;
    _accountTF.placeholder = @"填写您要提现的帐号";
    if (self.aliAccount.length > 0) {
        _accountTF.text = self.aliAccount;
        _accountTF.enabled = NO;
    }
    
    
    _moneyTF = [[MoneyTextField alloc] init];
    [footer addSubview:_moneyTF];
    _moneyTF.backgroundColor = [UIColor whiteColor];
    [_moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_accountTF.mas_bottom).with.offset(10 * KEY_RATE);
        make.left.right.equalTo(footer);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(0, 0, 63 * KEY_RATE, 50 * KEY_RATE);
    titleLabel.text = @"金额";
    titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    _moneyTF.leftView = titleLabel;
    _moneyTF.placeholder = [NSString stringWithFormat:@"最多可提现%.2f元",_maxMoney];
    
    _nameTF = [[MoneyTextField alloc] init];
    [footer addSubview:_nameTF];
    _nameTF.backgroundColor = [UIColor whiteColor];
    [_nameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_moneyTF.mas_bottom).with.offset(10 * KEY_RATE);
        make.left.right.equalTo(footer);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    UILabel *titleLabel2 = [[UILabel alloc] init];
    titleLabel2.frame = CGRectMake(0, 0, 63 * KEY_RATE, 50 * KEY_RATE);
    titleLabel2.text = @"姓名";
    titleLabel2.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    _nameTF.leftView = titleLabel2;
    _nameTF.placeholder = @"提款账号的姓名";
    if (self.aliName.length > 0) {
        _nameTF.text = self.aliName;
        _nameTF.enabled = NO;
    }
    
    CCTButton *confirmButton = [[CCTButton alloc] init];
    [footer addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameTF.mas_bottom).with.offset(30 * KEY_RATE);
        make.left.mas_equalTo(15 * KEY_RATE);
        make.right.mas_equalTo(-15 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    [confirmButton setTitle:@"确认" forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(recharge) forControlEvents:UIControlEventTouchUpInside];
    
    YYLabel *rechargeRecord = [[YYLabel alloc] init];
    [footer addSubview:rechargeRecord];
    
    [rechargeRecord mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(footer);
        make.top.equalTo(confirmButton.mas_bottom).with.offset(14 * KEY_RATE);
        make.height.equalTo(@(14 * KEY_RATE));
    }];
    rechargeRecord.font = [UIFont systemFontOfSize:11];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"提现完成后可以在提现记录中查看提现详情"];
    [attrStr setTextHighlightRange:NSMakeRange(8, 4) color:RGB(255, 46, 80) backgroundColor:[UIColor whiteColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        RecordViewController *vc = [[RecordViewController alloc] init];
        vc.recordType = RecordTypeWithdraw;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [attrStr addAttribute:NSForegroundColorAttributeName value:RGB(136, 136, 136) range:NSMakeRange(0, 8)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:RGB(136, 136, 136) range:NSMakeRange(12, 7)];
    rechargeRecord.attributedText = attrStr;
    [rechargeRecord setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *textLbl = [[UILabel alloc]initWithText:@"提示: 每账户每月可提现3次,提现银行计提1%转账手续费,异常操作提现手续费一律按2%,提现一般24小时内兑付到账,高峰期会有延迟,双休及节假日提现的,顺延到工作日兑付。\n如提现被驳回，请查看“我的”—“帮助中心”，对照驳回原因校正提现帐号后再重新提现！" font:12 textColor:TEXTBLACK6];
    [footer addSubview:textLbl];
    
    [textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(footer.mas_left).offset(15);
        make.right.equalTo(footer.mas_right).offset(-15);
        make.top.equalTo(rechargeRecord.mas_bottom).offset(30);
    }];
    
    return footer;
}


- (void)recharge {
    if (_accountTF.text.length == 0) {
        [MBProgressHUD showMessage:@"帐号不能为空"];
        return;
    }
    
    if (![MyUtil validateMoney:_moneyTF.text]) {
        [MBProgressHUD showMessage:@"请正确填写金额"];
        return;
    }
    if(_nameTF.text.length == 0){
        SHOW(@"姓名不能为空");
        return;
    }
    if ([_moneyTF.text floatValue] > _maxMoney) {
        [MBProgressHUD showMessage:[NSString stringWithFormat:@"最多只能提现%.2f元",_maxMoney]];
        return;
    }
    
    //提现 起始 金额
    if ([_moneyTF.text floatValue] < 1.0) {
        [MBProgressHUD showMessage:[NSString stringWithFormat:@"提现起始金额为1元"]];
        return;
    }
    //余额提现
        NSDictionary *paramDic = @{@"id": USERID,
                                   @"channel": [NSString stringWithFormat:@"%ld", _currentPayType],
                                   @"money": _moneyTF.text,
                                   @"account": _accountTF.text,
                                   @"accountname":_nameTF.text};
        
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/extract") params:paramDic target:self success:^(NSDictionary *success) {
            //提现成功
            SHOW(@"提现申请成功");
            if ([self.wDelegate respondsToSelector:@selector(WithdrawViewControllerSuccessOnMoney:)]) {
                [self.wDelegate WithdrawViewControllerSuccessOnMoney:[_moneyTF.text doubleValue]];
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSError *failure) {
            
        }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
