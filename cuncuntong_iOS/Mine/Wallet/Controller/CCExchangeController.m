//
//  CCExchangeController.m
//  CunCunTong
//
//  Created by 我是MT on 2017/6/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCExchangeController.h"


@interface CCExchangeController ()
@property (nonatomic, strong) UILabel *textLbl;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *exchangeBtn;
@property (nonatomic, assign) NSInteger credit;

@end

@implementation CCExchangeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"积分兑换"];
    [self addReturnBtn:@"tui_"];
    [self setUI];
    [self loadData];
    self.view.backgroundColor = WHITECOLOR;
    self.navigationController.navigationBarHidden = NO;
}

- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"misc/credit") params:@{@"rid":USERID} target:self success:^(NSDictionary *success) {
        self.credit = [success[@"data"][@"credit"] integerValue];
        self.textLbl.text =  [NSString stringWithFormat:@"您当前共%ld积分",self.credit];
    } failure:^(NSError *failure) {
    }];
}

- (void)exchangeBtnClick {
    if ([self.textField.text integerValue] < 3000) {
        SHOW(@"3000积分起兑");
        return;
    }
    if ([self.textField.text integerValue] % 3000 != 0) {
        SHOW(@"请输入3000的整数倍积分");
        return;
    }
    if ([self.textField.text integerValue] > self.credit) {
        SHOW(@"积分不足");
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"misc/exchange") params:@{@"rid":USERID,@"credit":self.textField.text} target:self success:^(NSDictionary *success) {
        SHOW(@"积分兑换成功");
        self.refreshBlock();
    } failure:^(NSError *failure) {
    }];
    
}
- (void)setUI {
    self.textLbl = [[UILabel alloc]initWithText:@"您当前共*积分" font:14 textColor:TEXTBLACK6];
    [self.view addSubview:self.textLbl];
    
    self.textField = [[UITextField alloc]init];
    [self.view addSubview:self.textField];
    self.textField.placeholder = @"请输入要兑换的积分";
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.font = FFont(14);
    
    self.exchangeBtn = [[UIButton alloc]initWithTitle:@"兑换" textColor:WHITECOLOR backImg:nil font:14];
    self.exchangeBtn.layer.cornerRadius = 3;
    self.exchangeBtn.layer.masksToBounds = YES;
    self.exchangeBtn.backgroundColor = MAINBLUE;
    [self.view addSubview:self.exchangeBtn];
    [self.exchangeBtn addTarget:self action:@selector(exchangeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *tipsLbl = [[UILabel alloc]initWithText:@"1. 会员股份由积分兑换，积分靠阅读获得，阅读一篇文章一个积分，重复阅读没有积分，每天上限10分。\n\n2. 3000积分兑换一份会员股，兑换积分请输入3000的整数倍。" font:12 textColor:TEXTBLACK6];
    [self.view addSubview:tipsLbl];
    
    [self.textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(15);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.top.equalTo(self.view.mas_top).offset(64 + 30);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.textLbl);
        make.top.equalTo(self.textLbl.mas_bottom).offset(20);
        make.height.mas_equalTo(40);
    }];
    [self.exchangeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.textLbl);
        make.top.equalTo(self.textField.mas_bottom).offset(20);
        make.height.mas_equalTo(40);
    }];
    [tipsLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.textLbl);
        make.top.equalTo(self.exchangeBtn.mas_bottom).offset(30);
    }];
}

@end
