//
//  CCExchangeController.h
//  CunCunTong
//
//  Created by 我是MT on 2017/6/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
//积分兑换
typedef void (^RefreshBlock)();
@interface CCExchangeController : BaseViewController
@property (nonatomic, copy) RefreshBlock refreshBlock;
@end
