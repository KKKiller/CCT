//
//  CCYajinWithdrwController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/15.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
@protocol WithdrawYajinControllerDelegate <NSObject>
@optional

-(void)WithdrawViewControllerSuccessOnMoney:(CGFloat)money;

@end
@interface CCYajinWithdrwController : BaseViewController
@property (nonatomic, assign) CGFloat maxMoney;
@property (nonatomic, weak) id<WithdrawYajinControllerDelegate> delegate;
@end

