//
//  WalletViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/30.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface WalletViewController : BaseViewController
@property (nonatomic, strong) NSString *aliName;
@property (nonatomic, strong) NSString *aliAccount;
@end
