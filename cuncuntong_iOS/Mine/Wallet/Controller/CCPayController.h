//
//  CCPayController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/11/19.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^PaySuccessBlock) ();
@interface CCPayController : BaseViewController
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) PaySuccessBlock paySuccessBlock;
@end
