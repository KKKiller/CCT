//
//  RecordViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "RecordViewController.h"
#import "RecordCell.h"

@interface RecordViewController ()<UITableViewDelegate, UITableViewDataSource> {
    NSInteger _page;
    UITableView *_tableView;
}

@property (nonatomic, strong) NSMutableArray *dataSource;

@end

static NSString *const cellIdentifier = @"cellIdentifier";

@implementation RecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addReturnBtn:@"tui_"];
    NSString *title = self.recordType == RecordTypeRecharge ? @"充值记录" : self.recordType == RecordTypeWithdraw ?  @"提现记录" : @"收支记录";
    [self addTitle:title];
    [self uiConfig];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

//- (void)navConfig {
//    UIImage * backImage = [[UIImage imageNamed:@"arrow_blue"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    //添加左边按钮
//    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE)];
//    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//    [backButton setImage:backImage forState:UIControlStateNormal];
//    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = backButtonItem;
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//}
- (void)uiConfig {
    
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT - 64);
    }];
    _tableView.tableFooterView = [UIView new];
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(reloadDataSource)];
    [_tableView.mj_header beginRefreshing];
    
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[RecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if(self.dataSource.count > indexPath.row){
        cell.model = self.dataSource[indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70 * KEY_RATE;
}

- (void)reloadDataSource {
    _page = 0;
    [self.dataSource  removeAllObjects];
    switch (_recordType) {
        case RecordTypeRecharge: {
            self.navigationItem.title = @"充值记录";
    NSDictionary *paramDic = @{@"id": USERID,
                               @"page": [NSString stringWithFormat:@"%ld",_page]};
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/rdetail") params:paramDic target:self success:^(NSDictionary *success) {
                for (NSDictionary *dic in success[@"data"][@"detail"]) {
                    RecordModel *model = [[RecordModel alloc] init];
                    [model modelSetWithDictionary:dic];
                    [self.dataSource addObject:model];
                }
                [_tableView reloadData];
                [_tableView.mj_header endRefreshing];
            } failure:^(NSError *failure) {
                [_tableView.mj_header endRefreshing];
            }];
        }
            break;
        case RecordTypeWithdraw: {
            self.navigationItem.title = @"提现记录";
            NSDictionary *paramDic = @{@"id": USERID, @"page": [NSString stringWithFormat:@"%ld",_page]};
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/edetail") params:paramDic target:self success:^(NSDictionary *success) {
                for (NSDictionary *dic in success[@"data"][@"detail"]) {
                    RecordModel *model = [[RecordModel alloc] init];
                    [model modelSetWithDictionary:dic];
                    [self.dataSource addObject:model];
                }
               [_tableView reloadData];
                [_tableView.mj_header endRefreshing];
            } failure:^(NSError *failure) {
                [_tableView.mj_header endRefreshing];
            }];
        }
            break;
        case RecordShare: {
            self.navigationItem.title = @"收支记录";
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/user_record") params:@{@"rid":USERID,@"page":[NSString stringWithFormat:@"%ld",_page]} target:self success:^(NSDictionary *success) {
                for (NSDictionary *dic in success[@"data"][@"info"]) {
                    RecordModel *model = [[RecordModel alloc] init];
                    model.create = STR(dic[@"create"]);
                    model.text = STR(dic[@"payname"]);
                    model.money = [NSString stringWithFormat:@"%@", dic[@"money"]];
                    [self.dataSource addObject:model];
                }
                [_tableView reloadData];
                [_tableView.mj_header endRefreshing];
            } failure:^(NSError *failure) {
                [_tableView.mj_header endRefreshing];

            }];
        }
            break;
        default:
            break;
    }
}

- (void)loadMore {
    _page++;
    switch (_recordType) {
        case RecordTypeRecharge: {
            self.navigationItem.title = @"充值记录";
            NSDictionary *paramDic = @{@"id": USERID, @"page": [NSString stringWithFormat:@"%ld",_page]};
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/rdetail") params:paramDic target:self success:^(NSDictionary *success) {
                [_tableView.mj_footer endRefreshing];
                for (NSDictionary *dic in success[@"data"][@"detail"]) {
                    RecordModel *model = [[RecordModel alloc] init];
                    [model modelSetWithDictionary:dic];
                    [self.dataSource addObject:model];
                }
                [_tableView reloadData];
            } failure:^(NSError *failure) {
                _page--;
                [_tableView.mj_footer endRefreshing];
            }];
        }
            break;
        case RecordTypeWithdraw: {
            self.navigationItem.title = @"提现记录";
            NSDictionary *paramDic = @{@"id": USERID, @"page": [NSString stringWithFormat:@"%ld",_page]};
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/edetail") params:paramDic target:self success:^(NSDictionary *success) {
                [_tableView.mj_footer endRefreshing];
                for (NSDictionary *dic in success[@"data"][@"detail"]) {
                    RecordModel *model = [[RecordModel alloc] init];
                    [model modelSetWithDictionary:dic];
                    [self.dataSource addObject:model];
                }
                [_tableView reloadData];
            } failure:^(NSError *failure) {
                _page--;
                [_tableView.mj_footer endRefreshing];
            }];
        }
            break;
        case RecordShare: {
            self.navigationItem.title = @"收支记录";
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/user_record") params:@{@"rid":USERID,@"page":[NSString stringWithFormat:@"%ld",_page]} target:self success:^(NSDictionary *success) {
                for (NSDictionary *dic in success[@"data"][@"info"]) {
                    RecordModel *model = [[RecordModel alloc] init];
                    model.create = STR(dic[@"create"]);
                    model.text = STR(dic[@"payname"]);
                    model.money = [NSString stringWithFormat:@"%@", dic[@"money"]];
                    [self.dataSource addObject:model];
                }
                [_tableView reloadData];
                [_tableView.mj_header endRefreshing];
                [_tableView.mj_footer endRefreshing];
            } failure:^(NSError *failure) {
                [_tableView.mj_header endRefreshing];
                [_tableView.mj_footer endRefreshing];
                
            }];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Target-Action

- (NSMutableArray *)dataSource {
    if (nil == _dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
