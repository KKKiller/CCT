//
//  RechargeViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
//充值
typedef void(^Block)();

@interface RechargeViewController : BaseViewController

@property (nonatomic, copy) Block block;

@property (nonatomic, strong) NSString *money;
@property (nonatomic, assign) BOOL shareTopCharge;
@property (nonatomic, strong) NSString *placeHolder;

//@property (nonatomic, strong) NSString *oorder_cid;//产品id
//@property (nonatomic, strong) NSString *order_user; //用户id
//@property (nonatomic, strong) NSString *order_address;//地址
//@property (nonatomic, strong) NSString *order_name;//收货人
//@property (nonatomic, strong) NSString *order_phone;//电话
//@property (nonatomic, strong) NSString *order_count;//订单数量
//@property (nonatomic, strong) NSString *order_total;//总价


@end
