//
//  CCYajinWithdrwController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/15.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCYajinWithdrwController.h"
#import "MoneyTextField.h"
#import "CCTButton.h"
@interface CCYajinWithdrwController ()
@property (nonatomic, strong) MoneyTextField *moneyTF;
@end

@implementation CCYajinWithdrwController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    
    [self addTitle:@"押金提现"];
    self.view.backgroundColor = BACKGRAY;
    _moneyTF = [[MoneyTextField alloc] init];
    [self.view addSubview:_moneyTF];
    _moneyTF.backgroundColor = [UIColor whiteColor];
    [_moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(110 * KEY_RATE);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(0, 0, 63 * KEY_RATE, 50 * KEY_RATE);
    titleLabel.text = @"金额";
    titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    _moneyTF.leftView = titleLabel;
    _moneyTF.placeholder = [NSString stringWithFormat:@"最多可提现%.2f元",_maxMoney];
    
       CCTButton *confirmButton = [[CCTButton alloc] init];
    [self.view addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_moneyTF.mas_bottom).with.offset(30 * KEY_RATE);
        make.left.mas_equalTo(15 * KEY_RATE);
        make.right.mas_equalTo(-15 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    [confirmButton setTitle:@"确认" forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(recharge) forControlEvents:UIControlEventTouchUpInside];
    

    
    YYLabel *rechargeRecord = [[YYLabel alloc] init];
    [self.view addSubview:rechargeRecord];
    rechargeRecord.font = FFont(12);
    rechargeRecord.textColor = TEXTBLACK9;
    [rechargeRecord mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view).with.offset(30);
        make.top.equalTo(confirmButton.mas_bottom).with.offset(13 * KEY_RATE);
        make.height.equalTo(@(14 * KEY_RATE));
    }];
    rechargeRecord.text = @"提示:押金退款到余额";
}

- (void)recharge {
    NSDictionary *paramDic = @{@"uid": USERID,
                               @"yajin": _moneyTF.text};
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/yjtx") params:paramDic target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            SHOW(@"提现成功");
            [self.delegate WithdrawViewControllerSuccessOnMoney:[_moneyTF.text floatValue]];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            SHOW(success[@"data"][@"info"]);
        }
        
    } failure:^(NSError *failure) {
        
    }];
}

@end
