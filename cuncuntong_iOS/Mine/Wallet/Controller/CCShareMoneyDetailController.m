//
//  CCShareMoneyDetailController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/10/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareMoneyDetailController.h"
static NSString *cellID = @"cellId";
@interface CCShareMoneyDetailController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end
@implementation CCShareMoneyDetailController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addTitle:@"收支记录"];
    [self.tableView reloadData];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/user_record") params:@{@"rid":USERID,@"size":@"1000"} target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            self.dataArray = success[@"data"][@"info"];
            [self.tableView reloadData];
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.textLabel.numberOfLines = 0;
    if (self.dataArray.count > indexPath.row) {
        NSDictionary *dict = self.dataArray[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"时间: %@\n名称: %@\n金额: %@元",dict[@"create"],dict[@"payname"],dict[@"money"]];
    }
    return cell;
}

#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height-64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 85;
        _tableView.backgroundColor = BACKGRAY;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

@end
