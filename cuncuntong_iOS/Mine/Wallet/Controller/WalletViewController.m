//
//  WalletViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/30.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "WalletViewController.h"
#import "RechargeViewController.h"
#import "WithdrawViewController.h"
#import "RecordViewController.h"
#import "PromoteViewController.h"
#import "PostersViewController.h"
#import "NSMutableAttributedString+Style.h"
#import "CCShareMoneyDetailController.h"
#import "CCChangePhoneController.h"
#import "CCExchangeController.h"
@interface WalletViewController ()<UITableViewDelegate, UITableViewDataSource,WithdrawViewControllerDelegate> {
    UITableView *_tableView;
    NSMutableArray *_dataSource;
    UILabel *_priceLabel;
    NSDictionary *_dataDic;
}
@property (nonatomic, strong) NSString *poster1;
@property (nonatomic, strong) NSString *poster2;
@property (nonatomic, strong) NSString *poster3;
@property (nonatomic, strong) NSString *domain;
@property (nonatomic, strong) UIView *header;
@property (nonatomic, strong) UILabel *shareMoneyLbl;
@property (nonatomic, strong) UIButton *showShareDetailBtn;
@property (nonatomic, strong) UIButton *extractBtn;
@property (nonatomic, assign) CGFloat sale_money;
@end

static NSString *const cellIdentifier = @"cellIdentifier";

@implementation WalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self uiConfig];
    [self loadShareUrl];
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
    _tableView.bounces = NO;
    _tableView.backgroundColor = RGB(245, 245, 245);
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self setData];
    [self loadShareSaleMoney];
}
- (void)setData {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": [UserManager sharedInstance].secret ?:@""} target:self success:^(NSDictionary *success) {
        _priceLabel.text = success[@"data"][@"money"];
        _dataDic = success;
    } failure:^(NSError *failure) {
    }];
    
}

- (void)loadShareSaleMoney {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/userinfo") params:@{@"rid": USERID} target:self success:^(NSDictionary *success) {
        CGFloat money = 0;
        if (success[@"data"][@"sale_money"]) {
            money = [[NSString stringWithFormat:@"%@", success[@"data"][@"sale_money"]] floatValue];
        }
        if(money < 0){
            money = 0;
        }
        self.sale_money = money;
//            self.header.height = 190 * KEY_RATE;
//            _tableView.tableHeaderView = self.header;
            [self shareMoneyUI:money];
    } failure:^(NSError *failure) {
    }];
}
- (void)shareMoneyUI:(CGFloat)money {
//    NSString *moneyStr = [NSString stringWithFormat:@"共享钱包余额: ¥%.2f",money];
    if (!self.showShareDetailBtn) {
//        self.shareMoneyLbl = [[UILabel alloc]initWithText:moneyStr font:25 textColor:WHITECOLOR];
//        [self.header addSubview:self.shareMoneyLbl];
//        [self.shareMoneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(self.header.mas_centerX).offset(-35);
//            make.top.equalTo(self.header.mas_top).offset(120);
//        }];
//
//        UIButton *detailBtn = [[UIButton alloc]initWithTitle:@"查看收支记录" textColor:WHITECOLOR backImg:nil font:15];
//        detailBtn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
//        detailBtn.layer.cornerRadius = 5 * KEY_RATE;
//        detailBtn.titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
//        [detailBtn addTarget:self action:@selector(shareDetail) forControlEvents:UIControlEventTouchUpInside];
//        self.showShareDetailBtn = detailBtn;
//        [self.header addSubview:detailBtn];
//        [detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(self.header.mas_centerX).offset(0);
//            make.width.mas_equalTo(100);
//            make.height.mas_equalTo(30);
//            make.top.equalTo(self.header.mas_top).offset(120);
//        }];
        
//        UIButton *extractBtn = [[UIButton alloc]initWithTitle:@"提取共享余额" textColor:WHITECOLOR backImg:nil font:15];
//        extractBtn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
//        extractBtn.layer.cornerRadius = 5 * KEY_RATE;
//        extractBtn.titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
//        [extractBtn addTarget:self action:@selector(extractShareMoney) forControlEvents:UIControlEventTouchUpInside];
//        self.extractBtn = extractBtn;
//        [self.header addSubview:extractBtn];
//        [extractBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(self.header.mas_centerX).offset(60);
//            make.width.mas_equalTo(100);
//            make.height.mas_equalTo(30);
//            make.top.equalTo(self.showShareDetailBtn.mas_top).offset(0);
//        }];
    }
//    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithText:moneyStr color:WHITECOLOR font:FFont(22) subStrIndex:NSMakeRange(0, 6) subStrColor:WHITECOLOR font2:FFont(12)];
//    self.shareMoneyLbl.attributedText = attrStr;
    
}
- (void)shareDetail {
    RecordViewController *vc = [[RecordViewController alloc]init];
    vc.recordType = RecordShare;
    [self.navigationController pushViewController:vc animated:YES];
}
//提取共享余额
- (void)extractShareMoney {
    if(self.sale_money == 0){
        SHOW(@"共享钱包无余额");
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/share_money_tx") params:@{@"uid": USERID,@"sale_money":[NSString stringWithFormat:@"%f",self.sale_money]} target:self success:^(NSDictionary *success) {
        if([success[@"data"][@"stat"] integerValue] == 0){
            NSString *msg = success[@"data"][@"info"];
            SHOW(msg);
        }else{
            [self setData];
            [self loadShareSaleMoney];
            NSString *msg = [NSString stringWithFormat:@"已提取共享余额%.2f元至钱包",self.sale_money];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)loadShareUrl {
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"misc/shareurl") params:nil success:^(NSDictionary *success) {
        NSLog(@"%@",success);
        self.poster1 = success[@"data"][@"urls"][@"poster1"];
        self.poster2 = success[@"data"][@"urls"][@"poster2"];
        self.poster3 = success[@"data"][@"urls"][@"poster3"];
        self.domain = success[@"data"][@"urls"][@"domain"];
        
    } failure:^(NSError *failure) {
    }];
    
}
- (void)uiConfig {
    
    [self addReturnBtn:@"tui_"];
    _dataSource = [[NSMutableArray alloc] initWithArray:@[@"支付宝绑定",@"充值记录", @"提现记录", @"积分兑换", @"查看收支记录"]];//,@"我的推广",@"我的海报(一)",@"我的海报(二)",@"我的海报(三)"
    
    CGRect frame = [[UIScreen mainScreen] bounds];
    _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorColor = [UIColor colorWithHexString:@"#EEEEEE"];
    [self.view addSubview:_tableView];

    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 280 * KEY_RATE)];
    
    headerView.backgroundColor = RGB(245, 245, 245);
    
    UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 220 * KEY_RATE)];
    headerImageView.image = [UIImage imageNamed:@"bg_wdqb"];
    headerImageView.backgroundColor = BACKGRAY;
    headerImageView.userInteractionEnabled = YES;
    [headerView addSubview:headerImageView];
    _tableView.tableHeaderView = headerView;
    
    self.header = headerView;
    [self headerConfig:headerView imageView:headerImageView];
}

- (void)headerConfig:(UIView *)hv imageView:(UIImageView *)img{
    
    UILabel *titleLab = [UILabel new];
//    titleLab.backgroundColor = [UIColor blueColor];
    titleLab.font = [UIFont systemFontOfSize:18 * KEY_RATE weight:0.5];
    titleLab.text = @"我的钱包";
    titleLab.textColor = WHITECOLOR;
    [hv addSubview:titleLab];
//    WEAKSELF
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(hv.mas_top).offset(StatusBarHeight + 10*KEY_RATE);
        make.centerX.equalTo(hv);
    }];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:IMAGENAMED(@"icon_back_white") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(backAct:) forControlEvents:UIControlEventTouchUpInside];
    [hv addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLab);
        make.height.width.equalTo(@(35 * KEY_RATE));
        make.left.equalTo(hv.mas_left).offset(16 * KEY_RATE);
    }];
    
    _priceLabel = [[UILabel alloc] init];
    [hv addSubview:_priceLabel];
    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(hv);
        make.top.equalTo(titleLab.mas_bottom).offset(20 * KEY_RATE);
    }];
    _priceLabel.textAlignment = NSTextAlignmentCenter;
    _priceLabel.font = [UIFont systemFontOfSize:50 * KEY_RATE];
    _priceLabel.textColor = [UIColor whiteColor];
    _priceLabel.text = @"0.00";
    
    UILabel *unitLabel = [[UILabel alloc] init];
    [hv addSubview:unitLabel];
    [unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(hv);
        make.top.equalTo(_priceLabel.mas_bottom);
        make.height.equalTo(@(20 * KEY_RATE));
    }];
    unitLabel.textAlignment = NSTextAlignmentCenter;
    unitLabel.textColor = RGB(221, 221, 221);
    unitLabel.font = [UIFont systemFontOfSize:12 * KEY_RATE];
    unitLabel.text = @"余额（元）";

    
    UIView *shadowV = [UIView new];
    shadowV.layer.cornerRadius = 8;
    shadowV.backgroundColor = WHITECOLOR;
    [hv addSubview:shadowV];
    [shadowV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(hv.mas_left).offset(12);
        make.right.equalTo(hv.mas_right).offset(-12);
        make.top.equalTo(img.mas_bottom).offset(-30);
        make.height.equalTo(@(60 * KEY_RATE));
    }];
    
    UIView *lineV = [UIView new];
    lineV.backgroundColor = RGB(220, 220, 220);
    [shadowV addSubview:lineV];
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(shadowV);
        make.height.equalTo(@(20 * KEY_RATE));
        make.width.equalTo(@(1 * KEY_RATE));
        
    }];
    
    UIButton *chargeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    chargeBtn.titleLabel.font = FontSize(13*KEY_RATE);
    [chargeBtn setTitleColor:BLACKCOLOR forState:UIControlStateNormal];
    [chargeBtn setTitle:@"充值" forState:UIControlStateNormal];
    chargeBtn.tag = 2000;
    [chargeBtn addTarget:self action:@selector(payClick:) forControlEvents:UIControlEventTouchUpInside];
    [shadowV addSubview:chargeBtn];
    [chargeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(shadowV);
        make.right.equalTo(lineV);
    }];
    
    UIButton *withdrawalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [withdrawalBtn setTitleColor:BLACKCOLOR forState:UIControlStateNormal];
    [withdrawalBtn setTitle:@"提现" forState:UIControlStateNormal];
    withdrawalBtn.titleLabel.font = FontSize(13 * KEY_RATE);
    [withdrawalBtn addTarget:self action:@selector(payClick:) forControlEvents:UIControlEventTouchUpInside];
    [shadowV addSubview:withdrawalBtn];
    [withdrawalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(shadowV);
        make.left.equalTo(lineV);
    }];


//    CGFloat originX = (KEY_WIDTH / 2) - 100 * KEY_RATE;
//
//    for (int i = 0; i < 2; i++) {
//        UIButton *btn = [[UIButton alloc] init];
//        [btn setTitle:@[@"充值", @"提现"][i] forState:UIControlStateNormal];
//        [imgv addSubview:btn];
//
//        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(unitLabel.mas_bottom).with.offset(14*KEY_RATE);
//            make.width.equalTo(@(60 * KEY_RATE));
//            make.height.equalTo(@(30 * KEY_RATE));
//            make.left.equalTo([NSNumber numberWithFloat:originX + i * 140 * KEY_RATE]);
//        }];
//        btn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
//        btn.layer.cornerRadius = 5 * KEY_RATE;
//        btn.titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
//        [btn addTarget:self action:@selector(payClick:) forControlEvents:UIControlEventTouchUpInside];
//        btn.tag = 2000 + i;
//
//    }
}

-(void)backAct:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - WithdrawViewControllerDelegate
//提现成功 减少金额
-(void)WithdrawViewControllerSuccessOnMoney:(CGFloat)money{
    CGFloat oldMoney = [_priceLabel.text doubleValue];
    _priceLabel.text = [NSString stringWithFormat:@"%.2f",oldMoney - money];
}

#pragma mark - UITableViewDelegate && UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.backgroundColor = RGB(245, 245, 245);
    cell.textLabel.text = _dataSource[indexPath.row];
    cell.textLabel.textColor = RGB(51, 51, 51);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.imageView.image = [UIImage imageNamed:@[@"ali",@"btn_czjl", @"btn_txjl",@"btn_tg",@"btn_hb",@"btn_hb",@"btn_hb"][indexPath.row]];
    if (indexPath.row == 0) {
        if (self.aliAccount.length > 0) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@(%@)",_dataSource[indexPath.row],self.aliAccount];
        }
    }
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50 * KEY_RATE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RecordViewController *vc = [[RecordViewController alloc] init];
    if (indexPath.row == 0) {
        CCChangePhoneController *vc = [[CCChangePhoneController alloc]init];
        vc.navTitle = @"验证账户";
        vc.ali = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1) {
        vc.recordType = RecordTypeRecharge;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }else if (indexPath.row == 2){
        vc.recordType = RecordTypeWithdraw;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 3){
//        我的推广
//        PromoteViewController *prVc = [[PromoteViewController alloc]init];
//        [self.navigationController pushViewController:prVc animated:YES];
        //积分兑换
        CCExchangeController *vc = [[CCExchangeController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //收支记录
        RecordViewController *vc = [[RecordViewController alloc]init];
        vc.recordType = RecordShare;
        [self.navigationController pushViewController:vc animated:YES];
////        我的海报
//        NSString *shareURL = indexPath.row == 3 ? self.poster1 : indexPath.row == 4 ? self.poster2 : self.poster3;
//        NSString *p = indexPath.row == 3 ? @"21252B49A345854072778171E626CFFC" : indexPath.row == 4 ? @"AC07FFC042C9238E6F9D63B10061BC32" : @"E89DA8A048A610A50F401339DCBCD017";
//
//
////        NSString *url = indexPath.row == 3 ? (self.poster1 ?: @"http://www.cct369.com/village/public/article?p=21252B49A345854072778171E626CFFC&u=" ): indexPath.row == 4 ? (self.poster2 ?: @"http://www.cct369.com/village/public/article?p=AC07FFC042C9238E6F9D63B10061BC32&u=") : (self.poster3 ?: @"http://www.cct369.com/village/public/article?p=E89DA8A048A610A50F401339DCBCD017&u=");
//        NSString *url = [NSString stringWithFormat:@"%@village/public/article?p=%@&u=%@",self.domain,p,USERID];
//        CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
//        vc.titleStr = @"我的海报";
//        vc.shareURL = [NSString stringWithFormat:@"%@&rid=%@",shareURL,USERID];
//        vc.urlStr = url;
//        vc.share_title = indexPath.row == 3 ? @"长沙一姑娘帐户天天冒钱,三个月提现18万,惊呆了!" : indexPath.row == 4 ? @"绝对没想到,这居然是真的!!" : @"推广全球村村通,让你轻松做富翁";
////        vc.urlStr = [NSString stringWithFormat:@"%@%@",url,USERID];
//        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}
#pragma mark - Target-Action

- (void)payClick:(UIButton *)sender {
    if (sender.tag - 2000 == 0) {
        RechargeViewController *vc = [[RechargeViewController alloc] init];
        vc.block = ^() {
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": [UserManager sharedInstance].secret} target:self success:^(NSDictionary *success) {
                _priceLabel.text = success[@"data"][@"money"];
            } failure:^(NSError *failure) {
            }];
        };
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    WithdrawViewController *vc = [[WithdrawViewController alloc] init];
    vc.maxMoney = [_dataDic[@"data"][@"money"] floatValue];
    vc.wDelegate = self;
    vc.aliName = self.aliName;
    vc.aliAccount = self.aliAccount;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
