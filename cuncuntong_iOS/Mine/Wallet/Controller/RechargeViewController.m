//
//  RechargeViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "RechargeViewController.h"
#import "CCTButton.h"
#import "MoneyTextField.h"
#import "RecordViewController.h"
#import "WXApi.h"
#import "WXApiObject.h"


@interface RechargeViewController ()<UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *_dataSource;
    UITableView *_tableView;
    NSArray *_iconArray;
    NSArray *_titleArray;
    MoneyTextField *_moneyTF;
    NSInteger _currentPayType;
}

@end

static NSString *const cellIdentifier = @"cellIdentifier";

@implementation RechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"充值"];
    // Do any additional setup after loading the view.
//    UIImage * backImage = [[UIImage imageNamed:@"arrow_blue"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    //添加左边按钮
//    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE)];
//    
//    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//    
//    [backButton setImage:backImage forState:UIControlStateNormal];
//    
//    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = backButtonItem;
//    
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//    self.navigationItem.title = @"充值";
    
    [self setData];
    [self uiConfig];
    if (self.money) {
        _moneyTF.text = self.money;
    }
    
    [NOTICENTER addObserver:self selector:@selector(paySuccess) name:@"PaySuccess" object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)paySuccess {
    if (self.block) {
        [self.navigationController popViewControllerAnimated:NO];
        self.block();
    }
}
- (void)setData {
    _iconArray = @[@"btn_zfb", @"cz_btn_weixin"];
    _titleArray = @[@"支付宝充值", @"微信充值"];
    _dataSource = [[NSMutableArray alloc] initWithArray:@[@YES, @NO]];
    _currentPayType = 0;
}

- (void)uiConfig {
    
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.tableFooterView =[self tableFooter];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT-64);
    }];
    _tableView.backgroundColor = RGB(238, 238, 238);
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = _titleArray[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    cell.imageView.image = [UIImage imageNamed:_iconArray[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.accessoryType = [_dataSource[indexPath.row] boolValue] ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50 * KEY_RATE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    for (NSInteger i = 0; i < _dataSource.count; i++) {
        _dataSource[i] = @NO;
    }
    _dataSource[indexPath.row] = @YES;
    _currentPayType = indexPath.row;
    [_moneyTF resignFirstResponder];
    [tableView reloadData];
    [_moneyTF becomeFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  10 * KEY_RATE;
}

- (UIView *)tableFooter {
    UIView *footer = [[UIView alloc] init];
    footer.frame = CGRectMake(0, 0, KEY_WIDTH, 170 * KEY_RATE);
    
    _moneyTF = [[MoneyTextField alloc] init];
    [footer addSubview:_moneyTF];
    _moneyTF.backgroundColor = [UIColor whiteColor];
    [_moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(footer.mas_top).with.offset(10 * KEY_RATE);
        make.left.right.equalTo(footer);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(0, 0, 63 * KEY_RATE, 50 * KEY_RATE);
    titleLabel.text = @"金额";
    titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    _moneyTF.leftView = titleLabel;
    if (self.placeHolder) {
        _moneyTF.placeholder = self.placeHolder;
        _moneyTF.font = FFont(14);
    }else{
        _moneyTF.placeholder = @"建议输入整数金额";
    }
    
    CCTButton *confirmButton = [[CCTButton alloc] init];
    [footer addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_moneyTF.mas_bottom).with.offset(10 * KEY_RATE);
        make.left.mas_equalTo(15 * KEY_RATE);
        make.right.mas_equalTo(-15 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    [confirmButton setTitle:@"确认" forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(withDraw) forControlEvents:UIControlEventTouchUpInside];
    
    YYLabel *rechargeRecord = [[YYLabel alloc] init];
    [footer addSubview:rechargeRecord];
    
    [rechargeRecord mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(footer);
        make.top.equalTo(confirmButton.mas_bottom).with.offset(13 * KEY_RATE);
        make.height.equalTo(@(14 * KEY_RATE));
    }];
    rechargeRecord.font = [UIFont systemFontOfSize:11];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"完成充值后可以在充值记录中查看充值详情"];
    [attrStr setTextHighlightRange:NSMakeRange(8, 4) color:RGB(255, 46, 80) backgroundColor:[UIColor whiteColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        RecordViewController *vc = [[RecordViewController alloc] init];
        vc.recordType = RecordTypeRecharge;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [attrStr addAttribute:NSForegroundColorAttributeName value:RGB(136, 136, 136) range:NSMakeRange(0, 8)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:RGB(136, 136, 136) range:NSMakeRange(12, 7)];
    rechargeRecord.attributedText = attrStr;
    [rechargeRecord setTextAlignment:NSTextAlignmentCenter];
    
    return footer;
}

- (void)withDraw {
    if ([self.money isEqualToString:@"0"]) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"PaySuccess" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (![MyUtil validateMoney:_moneyTF.text]) {
        [MBProgressHUD showMessage:@"请正确填写金额"];
        return;
    }
    App_Delegate.rechargeMoney = [_moneyTF.text floatValue];
    if (self.shareTopCharge) {
        [self shareCharge];
    }else{
        [self charge];
    }
   
    
}

- (void)charge {
    if (_currentPayType == 0) { //支付宝
        
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/alipay") params:@{@"id": USERID, @"money": _moneyTF.text} target:self success:^(NSDictionary *success) {
            
            [[AlipaySDK defaultService] payOrder:success[@"data"][@"paystring"] fromScheme:@"cuncuntong" callback:^(NSDictionary *resultDic) {
                [MBProgressHUD showMessage:@"充值成功"];
                [App_Delegate updateMoney];
                if (self.block) {
                    self.block();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } failure:^(NSError *failure) {
            
            
        }];
        
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/wcpay")
                                    params:@{@"id": USERID, @"money": _moneyTF.text}
                                    target:self
                                   success:^(NSDictionary *success) {//wx2cb5e062972b4bb4
                                       [WXApi registerApp:success[@"data"][@"appid"]];
                                       PayReq *request = [[PayReq alloc] init];
                                       request.partnerId = success[@"data"][@"partnerid"];
                                       request.prepayId  = success[@"data"][@"prepayid"];
                                       request.package   = @"Sign=WXPay";
                                       request.nonceStr  = success[@"data"][@"noncestr"];
                                       request.timeStamp = (UInt32)[success[@"data"][@"timestamp"] integerValue];
                                       request.sign      = success[@"data"][@"sign"];
                                       
                                       
                                       
                                       [WXApi sendReq:request];
                                       
                                   } failure:^(NSError *failure) {
                                       
                                   }];
}


- (void)shareCharge {
    if (_currentPayType == 0) { //支付宝
        
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/publicpayment") params:@{@"userid": USERID, @"money": _moneyTF.text,@"ontype":@"tuiguanchongzhi"} target:self success:^(NSDictionary *success) {
            
            [[AlipaySDK defaultService] payOrder:success[@"data"][@"paystring"] fromScheme:@"cuncuntong" callback:^(NSDictionary *resultDic) {
                [MBProgressHUD showMessage:@"充值成功"];
                [App_Delegate updateMoney];
                if (self.block) {
                    self.block();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } failure:^(NSError *failure) {
            
            
        }];
        
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/publicpwx")
                                    params:@{@"userid": USERID, @"money": _moneyTF.text,@"ontype":@"tuiguanchongzhi"}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       [WXApi registerApp:success[@"data"][@"appid"]];
                                       PayReq *request = [[PayReq alloc] init];
                                       request.partnerId = success[@"data"][@"partnerid"];
                                       request.prepayId  = success[@"data"][@"prepayid"];
                                       request.package   = @"Sign=WXPay";
                                       request.nonceStr  = success[@"data"][@"noncestr"];
                                       request.timeStamp = (UInt32)[success[@"data"][@"timestamp"] integerValue];
                                       request.sign      = success[@"data"][@"sign"];
                                       
                                       
                                       
                                       [WXApi sendReq:request];
                                       
                                   } failure:^(NSError *failure) {
                                       
                                   }];
}

@end
