//
//  CCPayController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/11/19.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCPayController.h"
#import "CCTButton.h"
#import "MoneyTextField.h"
#import "RecordViewController.h"
#import "WXApi.h"
#import "WXApiObject.h"

static NSString *const cellIdentifier = @"cellIdentifier";
@interface CCPayController ()<UITableViewDelegate, UITableViewDataSource>
{
        NSMutableArray *_dataSource;
        UITableView *_tableView;
        NSArray *_iconArray;
        NSArray *_titleArray;
        MoneyTextField *_moneyTF;
        NSInteger _currentPayType;
    }
@property (nonatomic, assign) CGFloat balance;
@property (nonatomic, strong) NSArray *priceArray;
@property (nonatomic, strong) UIButton *payBtn;
@end

@implementation CCPayController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"选择支付方式"];
    [self setData];
    [self uiConfig];
    [self getBalance];
    [NOTICENTER addObserver:self selector:@selector(paySuccess) name:@"PaySuccess" object:nil];
}

- (void)getBalance {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": [UserManager sharedInstance].secret} target:self success:^(NSDictionary *success) {
        self.balance = [success[@"data"][@"money"] floatValue];
        self.priceArray = @[[NSString stringWithFormat:@"总金额: %@元",self.money],[NSString stringWithFormat:@"总余额: %.2f元",self.balance],[NSString stringWithFormat:@"需支付: %.2f元",[self.money floatValue]- self.balance]];
        [_tableView reloadData];
    } failure:^(NSError *failure) {
    }];
}

- (void)setData {
    _iconArray = @[@"btn_zfb", @"cz_btn_weixin"];
    _titleArray = @[@"支付宝支付", @"微信支付"];
    _dataSource = [[NSMutableArray alloc] initWithArray:@[@YES, @NO]];
    _currentPayType = 0;
}

- (void)uiConfig {
    
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 44;
    [self.view addSubview:_tableView];
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];


    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT-64);
    }];
    _tableView.backgroundColor = RGB(238, 238, 238);
    
    self.payBtn = [[UIButton alloc]init];
    [_tableView addSubview:self.payBtn];
    self.payBtn.frame = CGRectMake((App_Width - 200)*0.5, 250, 200, 40);
    [self.payBtn setTitle:@"支付" forState:UIControlStateNormal];
    [self.payBtn setBackgroundColor:MAINBLUE];
    self.payBtn.titleLabel.textColor = WHITECOLOR;
    [self.payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.payBtn.layer.cornerRadius = 4;
    self.payBtn.layer.masksToBounds = YES;
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if (indexPath.row < 2) {
        cell.textLabel.text = _titleArray[indexPath.row];
        cell.imageView.image = [UIImage imageNamed:_iconArray[indexPath.row]];
        cell.accessoryType = [_dataSource[indexPath.row] boolValue] ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
    }else{
        cell.imageView.image = nil;
        if (self.priceArray) {
            cell.textLabel.text =  self.priceArray[indexPath.row - 2] ?: @"";
        }
        cell.accessoryType =  UITableViewCellAccessoryNone;

    }
    cell.textLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  10 * KEY_RATE;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 2) {
        for (NSInteger i = 0; i < _dataSource.count; i++) {
            _dataSource[i] = @NO;
        }
        _dataSource[indexPath.row] = @YES;
        _currentPayType = indexPath.row;
        [_moneyTF resignFirstResponder];
        [tableView reloadData];
    }
//    if(indexPath.row == 0){//支付宝
//        [self aliPay];
//    }else{ //微信
//        [self wxPay];
//    }
}
- (void)payBtnClick {
    if (_currentPayType == 0) { //支付宝
        [self aliPay];
    }else{ //微信
        [self wxPay];
    }
}
- (void)aliPay {
    NSString *money = [NSString stringWithFormat:@"%.2f", [self.money floatValue] - self.balance];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/alipay") params:@{@"id": USERID, @"money": money} target:self success:^(NSDictionary *success) {
        
        [[AlipaySDK defaultService] payOrder:success[@"data"][@"paystring"] fromScheme:@"cuncuntong" callback:^(NSDictionary *resultDic) {
//            [MBProgressHUD showMessage:@"充值成功"];
            if (self.paySuccessBlock) {
                self.paySuccessBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } failure:^(NSError *failure) {
        
        
    }];
}

- (void)wxPay {
    NSString *money = [NSString stringWithFormat:@"%.2f", [self.money floatValue] - self.balance];

    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/wcpay")
                                    params:@{@"id": USERID, @"money": money}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       [WXApi registerApp:success[@"data"][@"appid"]];

                                       PayReq *request = [[PayReq alloc] init];
                                       request.partnerId = success[@"data"][@"partnerid"];
                                       request.prepayId  = success[@"data"][@"prepayid"];
                                       request.package   = @"Sign=WXPay";
                                       request.nonceStr  = success[@"data"][@"noncestr"];
                                       request.timeStamp = (UInt32)[success[@"data"][@"timestamp"] integerValue];
                                       request.sign      = success[@"data"][@"sign"];
                                       [WXApi sendReq:request];
                                   } failure:^(NSError *failure) {
                                       
                                   }];
}

- (void)paySuccess {
    if (self.paySuccessBlock) {
        self.paySuccessBlock();
    }
}
- (void)dealloc {
    [NOTICENTER removeObserver:self];
}
@end
