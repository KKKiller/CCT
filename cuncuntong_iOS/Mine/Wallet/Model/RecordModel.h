//
//  RecordModel.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordModel : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *create;
@property (nonatomic, copy) NSString *money;

@end
