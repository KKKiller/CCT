//
//  RecordCell.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecordModel.h"

@interface RecordCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) RecordModel *model;

@end
