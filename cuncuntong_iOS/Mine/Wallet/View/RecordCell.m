//
//  RecordCell.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "RecordCell.h"

@implementation RecordCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _titleLabel = [[UILabel alloc] init];
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self).with.offset(16 * KEY_RATE);
            make.height.mas_equalTo(16 * KEY_RATE);
            make.width.mas_equalTo(200 * KEY_RATE);
        }];
        _titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
        
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.font = [UIFont systemFontOfSize:11 * KEY_RATE];
        [self addSubview:_dateLabel];
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(16 * KEY_RATE);
            make.top.equalTo(_titleLabel.mas_bottom).with.offset(10 * KEY_RATE);
            make.width.mas_equalTo(150 * KEY_RATE);
            make.height.mas_equalTo(10 *KEY_RATE);
        }];
        _dateLabel.textColor = RGB(136, 136, 136);
        
        _priceLabel = [[UILabel alloc] init];
        [self addSubview:_priceLabel];
        _priceLabel.font = [UIFont systemFontOfSize:18 * KEY_RATE];
        _priceLabel.textColor = RGB(255, 46, 80);
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.right.mas_equalTo(-16 * KEY_RATE);
            make.width.mas_equalTo(130 * KEY_RATE);
            make.height.mas_equalTo(18 * KEY_RATE);
        }];
        _priceLabel.textAlignment = NSTextAlignmentRight;
    }
    return self;
}

- (void)setModel:(RecordModel *)model {
    _model = model;
    _titleLabel.text = [_model.text isEqualToString:@""] ? @"空" : _model.text;
    _dateLabel.text = _model.create;
    _priceLabel.text = _model.money;
//    if ([_model.money floatValue] > 0) {
//    } else {
//        _priceLabel.textColor = [UIColor greenColor];
//    }

}

@end
