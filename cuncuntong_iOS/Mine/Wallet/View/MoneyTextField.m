//
//  MoneyTextField.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MoneyTextField.h"

@implementation MoneyTextField

- (instancetype)init {
    if (self = [super init]) {
        self.font = [UIFont systemFontOfSize:16];
        self.leftViewMode = UITextFieldViewModeAlways;
    }
    return self;
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    CGRect leftRect = [super leftViewRectForBounds:bounds];
    leftRect.origin.x += 15 * KEY_RATE;
    return leftRect;
}

//- (CGRect)textRectForBounds:(CGRect)bounds {
//    CGRect textRect = [super textRectForBounds:bounds];
//    textRect.origin.x += 15 * KEY_RATE;
//    return textRect;
//}

@end
