//
//  CCManagerLoginController.m
//  CunCunTong
//
//  Created by 我是MT on 2017/5/29.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCManagerLoginController.h"

@interface CCManagerLoginController ()
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *loginBtn;
@end

@implementation CCManagerLoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUI];
    self.view.backgroundColor = BACKGRAY;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([Def valueForKey:@"managerPasswd"]) {
        [self.navigationController popViewControllerAnimated:NO];
    }
}
- (void)setNav{
    [self addTitle:@"管理员登录"];
    [self addReturnBtn:@"tui_"];
}
- (void)setUI {
    self.textField = [[UITextField alloc]initWithFrame:CGRectMake(30, 64 + 60, App_Width - 60, 40)];
    [self.view addSubview:self.textField];
    self.textField.placeholder = @"请输入管理员密码";
    self.textField.font = FFont(14);
    self.textField.backgroundColor = WHITECOLOR;
    self.textField.layer.cornerRadius = 3;
    self.textField.layer.masksToBounds = YES;
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.textField.leftViewMode = UITextFieldViewModeAlways;
    UIImageView *lock = [[UIImageView alloc]initWithFrame:CGRectMake(6, 10, 12, 18)];
    lock.contentMode = UIViewContentModeScaleAspectFit;
    lock.image = [UIImage imageNamed:@"img_mima"];
    [self.textField addSubview:lock];
    UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 40)];
    self.textField.leftView = leftView;
    
    self.loginBtn = [[UIButton alloc]initWithFrame:CGRectMake(30, 64 + 150, App_Width - 60, 40)];
    [self.view addSubview:self.loginBtn];
    [self.loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    [self.loginBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    [self.loginBtn setBackgroundColor:RGB(56, 126, 255)];
    self.loginBtn.layer.cornerRadius = 3;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.titleLabel.font = FFont(14);
    [self.loginBtn addTarget:self action:@selector(managerLogin) forControlEvents:UIControlEventTouchUpInside];
    self.loginBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)managerLogin {
    if (self.textField.text.length == 0) {
        SHOW(@"请输入管理员密码");
        return;
    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"misc/zadmin_autologin") params:@{@"rid":USERID,@"passwd":self.textField.text,@"inajax":@"1"} target:self success:^(NSDictionary *success) {
        SHOW(@"登录成功");
        NSString *managerPasswd = success[@"passwd"];
        [Def setValue:managerPasswd forKey:@"managerPasswd"];
        CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
        vc.urlStr =  [NSString stringWithFormat:@"http://www.cct369.com/village/public/misc/zadmin_autologin?rid=%@&passwd=%@",USERID,managerPasswd];
        vc.hiddenNav = YES;
        vc.titleStr = @"管理员";
        [self.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *failure) {
        SHOW(@"密码错误或当前账号未成功申请管理员");
    }];
}
@end
