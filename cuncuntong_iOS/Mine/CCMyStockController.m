//
//  CCMyStockController.m
//  CunCunTong
//
//  Created by 我是MT on 2017/5/29.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyStockController.h"
#import "CCExchangeController.h"
#import "CCMyStockView.h"
#import "CCMyStockCharView.h"
#import "CCScanController.h"
@interface CCMyStockController ()
@property (nonatomic, strong) UILabel *firstStockLbl;
@property (nonatomic, strong) UILabel *secondStockLbl;
@property (nonatomic, strong) UILabel *thirdStockLbl;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CCMyStockCharView *chartView;
@property (nonatomic, strong) UILabel *descTitleLbl;
@property (nonatomic, strong) UILabel *descLbl;
@property (nonatomic, strong) NSArray *stockArray;
@property (nonatomic, strong) NSArray *stockColor;
@end

@implementation CCMyStockController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self loadData];
}

- (void)loadData {
    [self setData:[TOOL getCachaDataWithName:@"Stock"]];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"center/stock") params:@{@"rid":USERID} target:self success:^(NSDictionary *success) {
        [self setData:success[@"data"][@"data"] ];
        [TOOL saveDataWithData:success Name:@"Stock"];
        
    } failure:^(NSError *failure) {
    }];
}
- (void)setData:(NSDictionary *)dict {
    if (dict) {
        CCMyStockModel *jinzhongZiModel = [[CCMyStockModel alloc]initWithName:@"金种子股份" count:dict[@"stock"] color:self.stockColor[0]];
        CCMyStockModel *huiYuanModel = [[CCMyStockModel alloc]initWithName:@"会员股份" count:dict[@"stock_member"] color:self.stockColor[1]];
        CCMyStockModel *jiuModel1 = [[CCMyStockModel alloc]initWithName:@"原始酒厂股份" count:dict[@"stock_jiu"] color:self.stockColor[2]];
        CCMyStockModel *jiuModel2 = [[CCMyStockModel alloc]initWithName:@"县代理酒厂股份" count:dict[@"stock_wine_area"] color:self.stockColor[3]];
        CCMyStockModel *jiuModel3 = [[CCMyStockModel alloc]initWithName:@"村代理酒厂股份" count:dict[@"stock_wine_village"] color:self.stockColor[4]];
        CCMyStockModel *jiuModel4 = [[CCMyStockModel alloc]initWithName:@"会员酒厂股份" count:dict[@"stock_wine_user"] color:self.stockColor[5]];
        if (self.isProfileStock) {
            self.stockArray = @[jinzhongZiModel,huiYuanModel,jiuModel1,jiuModel2,jiuModel3,jiuModel4];
        }else{
            self.stockArray = @[jiuModel1,jiuModel2,jiuModel3,jiuModel4];
        }
        [self setUI];
        
    }
}

- (void)setNav{
    [self addTitle:@"我的股份"];
    [self addReturnBtn:@"tui_"];
}



- (void)exchangeBtnClick {
    CCExchangeController *vc = [[CCExchangeController alloc]init];
    vc.refreshBlock = ^{
        [self loadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)setUI {
    self.view.backgroundColor = WHITECOLOR;
    [self.view addSubview:self.scrollView];
    [self addMyStockViews];
    [self.scrollView addSubview:self.chartView];
    if (self.isProfileStock) {
        [self.scrollView addSubview:self.descTitleLbl];
        [self.scrollView addSubview:self.descLbl];
    }
    
}
- (void)addMyStockViews {
    for (int i = 0; i < self.stockArray.count; i++) {
        CGFloat margin = 20;
        CGFloat width = (self.view.frame.size.width - margin * 3) * 0.5;
        CGFloat height = 140;
        CGFloat offset =  self.isProfileStock ? 0 : -64;
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0) {
            offset = 0;
        }
        CCMyStockView *stockView = [[CCMyStockView alloc]initWithFrame:CGRectMake(i % 2 * (width + margin) + margin , i/2 * (height + margin) + margin + offset, width, height)];
        stockView.tag = 100 + i;
        [self.scrollView addSubview:stockView];
        [stockView.exchangeBtn addTarget:self action:@selector(exchangeBtnClick) forControlEvents:UIControlEventTouchUpInside];
        CCMyStockModel *model = self.stockArray[i];
        stockView.model = model;
    }
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        CGFloat height = self.isProfileStock ?   1110 : 620;
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        _scrollView.contentSize=CGSizeMake(0, height);
    }
    return _scrollView;
}
- (CCMyStockCharView *)chartView {
    if (!_chartView) {
        CGFloat offset = self.isProfileStock ? 0 : -64;
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0) {
            offset = 0;
        }
        CGFloat y = ((self.stockArray.count+1) / 2)* 160 + offset+20;
        _chartView = [[CCMyStockCharView alloc]initWithFrame:CGRectMake(0, y , self.view.frame.size.width, 220*KEY_RATE)];
    }
    return _chartView;
}
- (UILabel *)descLbl {
    if (!_descLbl) {
        _descLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, self.descTitleLbl.frame.origin.y + 20*KEY_RATE, self.view.frame.size.width - 40, 320)];
        _descLbl.text = @"1. 全球村村通旨在打造为人民大众创造财富的企业集群，所有注册全球村村通的会员，通过文章阅读积累积分达到3000的，均会配赠1股，配赠股份占全部股份的10%。\n\n2. 全球村村通在完成C轮融资后，还将为低保弱势群体每人配发股份，配赠股份占全部股份的10%。\n\n3. 全球村村通每年年终结算后，按配股发利润。\n\n4. 酒厂县、村代理除了正常经营利润，还有酒厂股份，每年分红，村长管理员可优先获得村长代理资格（未确定代理的情况下）。消费村村通酒水可获得会员酒厂股份，每年享受分红，消费越多，分红越多。";
        _descLbl.font = [UIFont systemFontOfSize:14];
        _descLbl.numberOfLines = 0;
        
    }
    return _descLbl;
}
- (UILabel *)descTitleLbl {
    if (!_descTitleLbl) {
        _descTitleLbl = [[UILabel alloc]initWithFrame:CGRectMake(15, self.chartView.frame.origin.y + self.chartView.frame.size.height + 30, 40, 20)];
        _descTitleLbl.textColor = [UIColor blueColor];
        _descTitleLbl.font = [UIFont systemFontOfSize:16];
        _descTitleLbl.text = @"说明";
    }
    return _descTitleLbl;
}
- (NSArray *)stockColor {
    if (!_stockColor) {
        _stockColor = @[MTRGB(0xecf1f5),MTRGB(0xf6f06b),MTRGB(0x55bb55),MTRGB(0x66ade2),MTRGB(0xb055bb),[UIColor blueColor]];
    }
    return _stockColor;
}
@end
