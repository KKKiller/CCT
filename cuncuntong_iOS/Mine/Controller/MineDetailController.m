//
//  MineDetailController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MineDetailController.h"
#import "MineDetailCell.h"
#import "MineDetailModel.h"
#import "DetailViewController.h"

@interface MineDetailController ()<UITableViewDelegate, UITableViewDataSource> {
    UITableView *_tableView;
    NSInteger _page;
}

@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@end

static NSString *const cellIdentifier = @"cellIdentifier";

@implementation MineDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addReturnBtn:@"tui_"];
    if (_mineDetailType == MineDetailTypeRead) {
        [self addTitle:@"阅读记录"];
    }else {
        [self addTitle:@"收藏列表"];
    }
    [self uiConfig];
}

- (void)reloadDataSource {
    _page = 0;
    [self.dataSource removeAllObjects];
    
    NSDictionary *paramDic = @{@"id": USERID, @"page": [NSString stringWithFormat:@"%ld", _page]};
    
    
    NSString *url;
    NSString *key ;
    if (_mineDetailType == MineDetailTypeRead) {
         url = @"center/record";
        key = @"article";

    } else {
        url = @"center/article";
        key = @"cols";

    }
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(url) params:paramDic target:self success:^(NSDictionary *success) {
        
        for (NSDictionary *dic in success[@"data"][key]) {
            MineDetailModel *model = [MineDetailModel modelWithDictionary:dic];
            [self.dataSource addObject:model];
        }
        self.emptyView.hidden = self.dataSource.count != 0;
        [_tableView reloadData];
        [_tableView.mj_header endRefreshing];
    } failure:^(NSError *failure) {
        [_tableView.mj_header endRefreshing];
    }];
    
}

- (void)loadMore {
    _page ++;
    NSDictionary *paramDic = @{@"id": USERID, @"page": [NSString stringWithFormat:@"%ld", _page]};
    NSString *url;
    if (_mineDetailType == MineDetailTypeRead) {
        url = @"center/record";
    } else {
        url = @"center/article";
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(url) params:paramDic target:self success:^(NSDictionary *success) {
                
        for (NSDictionary *dic in success[@"data"][@"article"]) {
            MineDetailModel *model = [MineDetailModel modelWithDictionary:dic];
            [self.dataSource addObject:model];
        }
        [_tableView reloadData];
        [_tableView.mj_footer endRefreshing];
    } failure:^(NSError *failure) {
        _page--;
        [_tableView.mj_footer endRefreshing];
    }];
    

}

- (void)uiConfig {
    _tableView = [[UITableView alloc] init];
    _tableView.delegate   = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT - 64);
    }];
    _tableView.tableFooterView = [UIView new];
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(reloadDataSource)];
    [_tableView.mj_header beginRefreshing];
    
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];

}

#pragma mark - UITableViewDelegate && UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MineDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[MineDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = [MyUtil calculateLabelText:[self.dataSource[indexPath.row] title] sizeMarkW:(KEY_WIDTH - 2 * 10 * KEY_RATE) sizeMarkH:0 textFont:18 * KEY_RATE].height;
    
    return height + 2 * 10 * KEY_RATE +  2 * 10 * KEY_RATE + 15 * KEY_RATE;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    DetailViewController *vc = [[DetailViewController alloc] init];
    MineDetailModel *model = self.dataSource[indexPath.row];
//    vc.url     = model.url;
//    vc.village = model.village;
//    vc.aid     = model.articleId;
//    vc.iscol   = model.iscol;
//    vc.mini    = model.mini;
//    vc.block = ^(BOOL isCol) {
//        model.iscol = [NSString stringWithFormat:@"%i",isCol];
//        [self reloadDataSource];
//    };
//    [self.navigationController pushViewController:vc animated:YES];
    DetailViewController *vc = [[DetailViewController alloc] init];
    vc.block = ^(BOOL isCol) {
        model.iscol = [NSString stringWithFormat:@"%i",isCol];
        [self reloadDataSource];
    };
    vc.articleId = model.articleId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
@end
