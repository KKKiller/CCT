//
//  MineViewController.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MineViewController.h"
#import "MineView.h"
#import "MineCell.h"
#import "NewsViewController.h"
#import "SettingViewController.h"
#import "WalletViewController.h"
#import "FollowVillageController.h"
#import "AccountViewController.h"
#import "MineDetailController.h"
#import "CommentViewController.h"
#import "IntegralMallController.h"
#import "MineCrowdfundingViewController.h"
#import "CCMyStockController.h"
#import "CCManagerLoginController.h"
#import "AddVillageController.h"
#import "LoginViewController.h"
#import "CCMyCreditController.h"
#import "PromoteViewController.h"
#import "HFStretchableTableHeaderView.h"
@interface MineViewController ()<UITableViewDelegate,UITableViewDataSource,MineDelegate>
{
    UITableView *_tableView;
    NSArray *_titleArr;
    NSArray *_imageArr;
    MineView *_mineView; //header
    UIButton *_messageItem;
    UIImageView *_imageView;
    UIView *_headerView;
}

@property (nonatomic,strong) UILabel     * name;
@property (nonatomic, strong) UIView *unLoginView;
@property (nonatomic, strong) NSString *aliName;
@property (nonatomic, strong) NSString *aliAccount;
@property (nonatomic,strong) UIImageView * avatar;
@property (nonatomic, strong) HFStretchableTableHeaderView* stretchableTableHeaderView;
@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self uiConfig];
    [self prepareData];
    
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setNav];
    self.leftButton.hidden = !ISLOGIN;
    self.navigationController.navigationBarHidden = YES;
//     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    if (ISLOGIN) {
        [self loadData];
    }else{
        self.unLoginView.hidden = ISLOGIN;
    }
//    self.edgesForExtendedLayout = UIRectEdgeTop;

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)loadData {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/secret")
                                    params:@{@"secret": SECRET}
                                    target:nil
                                   success:^(NSDictionary *success) {
                                       self.name.text = success[@"data"][@"realname"];
                                       [_mineView resetTheNumber:@[success[@"data"][@"read"],
                                                                   success[@"data"][@"col"],
                                                                   success[@"data"][@"comment"],
                                                                   success[@"data"][@"point"]]];
                                       [[UserManager sharedInstance] modelSetWithDictionary:success[@"data"]];
                                       NSString *role = success[@"data"][@"freight_role"];
                                       [UserManager sharedInstance].tsRole = [role isEqualToString:@"未开通"] ? TSRoleNormal :  [role isEqualToString:@"货主"] ? TSRoleStation : TSRoleDriver ;
                                       if ([UserManager sharedInstance].tsRole == TSRoleDriver) {
                                           [self loadTSInfo];
                                       }else if ([UserManager sharedInstance].tsRole == TSRoleStation){
                                           [self loadStationInfo];
                                       }
                                       [self setLingdang];
                                       NSString *phone = success[@"data"][@"mobile"] ?: success[@"data"][@"realname"];
                                       [Def setValue:phone forKey:@"phone"];
                                       [Def setValue:success[@"data"][@"realname"] forKey:@"realname"];
                                       self.aliName = success[@"data"][@"alipay_name"];
                                       self.aliAccount = success[@"data"][@"alipay"];
                                       [Def setValue:self.aliName forKey:@"aliName"];
                                       [Def setValue:self.aliAccount forKey:@"aliAccount"];
                                       if (self.aliName) {
                                           [Def setValue:self.aliName forKey:@"aliName"];                                           
                                       }
                                       [self.avatar setImageWithURL:[NSURL URLWithString:[[UserManager sharedInstance] portrait]]
                                                        placeholder:[UIImage imageNamed:@"placeholder"]];
                                   } failure:^(NSError *failure) {
                                   }];
}

- (void)loadTSInfo {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_owner_info") params:@{@"r_id":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            NSInteger states = [success[@"data"][@"car_owner"][@"info_state"] integerValue];
            NSString *state = [NSString stringWithFormat:@"%@", @(states)];
            [UserManager sharedInstance].tsReviewState = [state isEqualToString:@"0"] ? TSRoleReviewStateing :   [state isEqualToString:@"1"] ? TSRoleReviewStatePass : TSRoleReviewStateRefuse;
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
        
}
- (void)loadStationInfo {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"freight/get_apply_owner") params:@{@"r_id":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            NSString *state = [NSString stringWithFormat:@"%@", success[@"data"][@"state"]];
            [UserManager sharedInstance].tsReviewState = [state isEqualToString:@"0"] ? TSRoleReviewStateing :   [state isEqualToString:@"1"] ? TSRoleReviewStatePass : TSRoleReviewStateRefuse;
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _titleArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_titleArr[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cellID";
    MineCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        cell = [[MineCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    if (indexPath.section==0 &&!(indexPath.row == 0)) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
//    if (indexPath.section == 0 && indexPath.row == 0) {
//        cell.detailTextLabel.text = @"更多好礼等你来拿!";
//    }else{
//        cell.detailTextLabel.text = @"";
//    }
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14 * KEY_RATE];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setIcon:_imageArr[indexPath.section][indexPath.row] title:_titleArr[indexPath.section][indexPath.row]];
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50*KEY_RATE;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*KEY_RATE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
//            case 0: { //积分商城
//                IntegralMallController *vc = [[IntegralMallController alloc] init];
//                [self.navigationController pushViewController:vc animated:YES];
//            }
                break;
            case 0: { //我的钱包
                WalletViewController *vc = [[WalletViewController alloc] init];
                vc.aliAccount = self.aliAccount;
                vc.aliName = self.aliName;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 1: { //我的信用
                CCMyCreditController *vc = [[CCMyCreditController alloc]init];
                self.navigationController.navigationBarHidden = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 2: { //我的众投
                MineCrowdfundingViewController *vc = [[MineCrowdfundingViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 3: { //我的股份
//                CCMyStockController *vc = [[CCMyStockController alloc]init];
//                vc.isProfileStock = YES;
//                [self.navigationController pushViewController:vc animated:YES];
                CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
                vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/web/stock_page?rid=%@",USERID];
                vc.titleStr = @"我的股份";
                vc.hiddenShare = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
//            case 4: { //我的推广
////                PromoteViewController *prVc = [[PromoteViewController alloc]init];
//                CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
//                vc.titleStr = @"我的推广";
////                vc.topOffset = -40;
//                vc.hiddenNav = YES;
//                vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/wep/energystatistics?r_id=%@",USERID];
//                [self.navigationController pushViewController:vc animated:YES];
//
//            }
            case 4: { //月光宝盒
                CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
                vc.titleStr = @"月光宝盒";
                vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/kof/mineInferior/%@",USERID];
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 5:{ //管理登录
                //"account://save?mobile=xxx&password=xxx"
                 CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
                NSString *url = nil;
                if ([Def valueForKey:@"managerPasswds"]) {
                    url =  [NSString stringWithFormat:@"http://www.cct369.com/village/public/misc/zadmin_autologin?rid=%@&passwd=%@&w_group=%@",USERID,[Def valueForKey:@"managerPasswds"],[Def valueForKey:@"w_group"]];
                }else{
                    url =  [NSString stringWithFormat:@"http://www.cct369.com/village/public/misc/zadmin_autologin?rid=%@",USERID];
                }
                vc.urlStr = url;
                vc.hiddenNav = YES;
                vc.titleStr = @"管理员";
                [self.navigationController pushViewController:vc animated:YES];
            }
                 break;
            case 6:{ //账户信息
                AccountViewController *vc = [[AccountViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
                 break;
            case 7:{//帮助中心
                CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
                NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/article?p=488BC8C1BAED05737949687ADE1158C8&u=%@",USERID];
                vc.titleStr = @"帮助中心";
                vc.urlStr = url;
                [self.navigationController pushViewController:vc animated:YES];

            }
                 break;
            default:
                break;
        }
        return;
    }else{
        switch (indexPath.row) {
            case 0: //微信咨询
                [self wechatConsult];
                 break;
            case 1: //客服热线
                [self phoneConsult];
                break;
            default:
                break;
        }
    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat offset = scrollView.contentOffset.y;
//    if (offset <= 0) {
//        CGFloat x = (150 / App_Width) * offset;
//        _imageView.frame = CGRectMake(x, offset, App_Width - 2*x, (150+(135/2))*KEY_RATE - offset);
//    }
//}

#pragma mark - Target-Action
- (void)messageDetail:(UIButton *)btn
{
    NewsViewController *vc = [[NewsViewController alloc]init];
    vc.callback = ^() {
        if (![[UserManager sharedInstance].actpoint isEqualToString:@"0"] || ![[UserManager sharedInstance].syspoint isEqualToString:@"0"]) {
            [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang1"] forState:UIControlStateNormal];
        }else {
            [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang0"] forState:UIControlStateNormal];
        }
    };
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)settings {
    
    SettingViewController *vc = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
//微信咨询
- (void)wechatConsult{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"公众号'村村通公社'已复制到粘贴板,请至微信关注后咨询" preferredStyle:1];
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    UIPasteboard * pastboard = [UIPasteboard generalPasteboard];
                                    pastboard.string = @"村村通公社";
                                }];
    [alertController addAction:chooseOne];
    [self presentViewController:alertController animated:YES completion:nil];
}
//电话咨询
- (void)phoneConsult {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"010-82886232" message:nil preferredStyle:1];
    //18039290408
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:010-82886232"];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - MineDelegate
- (void)pushToDetail:(UIButton *)sender {
    switch (sender.tag - 1000) {
        case 0: {
            MineDetailController *vc = [[MineDetailController alloc] init];
            vc.mineDetailType = MineDetailTypeRead;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1: {
            MineDetailController *vc = [[MineDetailController alloc] init];
            vc.mineDetailType = MineDetailTypeCol;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2: {
            CommentViewController *vc = [[CommentViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}

- (void)tapAvatar {
    AccountViewController *vc = [[AccountViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];

}

#pragma mark - UI
- (void)setLingdang {
    if (![[UserManager sharedInstance].actpoint isEqualToString:@"0"] || ![[UserManager sharedInstance].syspoint isEqualToString:@"0"]) {
        [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang1"] forState:UIControlStateNormal];
    }else {
        [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang0"] forState:UIControlStateNormal];
    }
}

- (void)setNav{

    self.view.backgroundColor = [UIColor colorWithRed:0.97f green:0.97f blue:0.97f alpha:1.00f];
    UIButton *leftItem = [[UIButton alloc] initWithFrame:CGRectMake(MDK_SCREEN_WIDTH - 80, StatusBarHeight + 10, 30, 30)];
    [leftItem setImage :[UIImage imageNamed:@"icon_cilun"] forState:UIControlStateNormal];
    [leftItem addTarget:self action:@selector(settings) forControlEvents:UIControlEventTouchUpInside];
    leftItem.titleLabel.font = [UIFont systemFontOfSize:18];

    [_headerView addSubview:leftItem];
    
    if (ISLOGIN) {
        [leftItem setHidden:NO];
    }else{
        [leftItem setHidden:YES];
    }
    
    _messageItem = [[UIButton alloc] initWithFrame:CGRectMake(KEY_WIDTH - 40, StatusBarHeight + 10, 30, 30)];
    [_messageItem addTarget:self action:@selector(messageDetail:) forControlEvents:UIControlEventTouchUpInside];
//    _messageItem.backgroundColor = [WHITECOLOR colorWithAlphaComponent:0.3];
//    _messageItem.layer.cornerRadius = 15;
//    _messageItem.layer.masksToBounds = YES;
    [_headerView addSubview:_messageItem];
    
}

- (void)uiConfig {
    
    _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KEY_WIDTH, (150+(135/2))*KEY_RATE)];
    [self.view addSubview:_headerView];
    
    _imageView = [[UIImageView alloc]init];
    _imageView.image = [UIImage imageNamed:@"bg_grzx"];
    [_headerView addSubview:_imageView];
    
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_headerView);
        make.top.equalTo(@0);
        make.bottom.equalTo(@0);
    }];
    
    _mineView = [[MineView alloc]initWithFrame:CGRectMake(0,
                                                          150 * KEY_RATE,
                                                          KEY_WIDTH,
                                                          (135/2)*KEY_RATE) :@[@"0",@"0",@"0",@"0"]];
    _mineView.delegate = self;
    _mineView.backgroundColor = CLEARCOLOR;
    [_headerView addSubview:_mineView];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatar)];
    [self.avatar addGestureRecognizer:tap];
    [_headerView addSubview:self.avatar];
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageView.mas_left).mas_offset(20);
        make.top.equalTo(@(StatusBarHeight + 40*KEY_RATE));
        make.size.mas_equalTo(CGSizeMake(60*KEY_RATE, 60*KEY_RATE));
    }];
    self.avatar.layer.borderColor = BACKGRAY.CGColor;
    self.avatar.layer.borderWidth = 1;
    
    [_headerView addSubview:self.name];
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_avatar.mas_centerY);
        make.left.equalTo(_avatar.mas_right).offset(10);
        make.right.equalTo(_headerView.mas_right).offset(-20);
    }];
    
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KEY_WIDTH, KEY_HEIGHT-WCFTabBarHeight)
                                             style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor colorWithRed:0.97f green:0.97f blue:0.97f alpha:1.00f];
//    _tableView.tableHeaderView = _headerView;
    _stretchableTableHeaderView = [HFStretchableTableHeaderView new];
    [_stretchableTableHeaderView stretchHeaderForTableView:_tableView withView:_headerView];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_stretchableTableHeaderView scrollViewDidScroll:scrollView];
}

- (void)viewDidLayoutSubviews
{
    [_stretchableTableHeaderView resizeView];
}


- (void)prepareData {
    _titleArr = @[@[@"我的钱包",@"我的信用",@"我的众投",@"我的股份",@"月光宝盒",@"管理登录",@"账户信息",@"帮助中心"],@[@"微信咨询",@"客服热线(9:00-17:00)"]];//@"积分商城",
    _imageArr = @[@[@"img_wdqb",@"credit",@"img_zhongtou",@"img_gufen",@"yueguangbaohe",@"img_admin" ,@"img_zhxx",@"help"],@[@"cz_btn_weixin",@"img_kfrx"]];//@"img_jfsc",
}
-(UILabel *)name
{
    if (!_name) {
        _name               = [[UILabel alloc]init];
        _name.textAlignment = NSTextAlignmentLeft;
        _name.textColor     = [UIColor whiteColor];
        _name.font          = [UIFont systemFontOfSize:16*KEY_RATE];
    }
    return _name;
}
- (UIImageView *)avatar
{
    if (!_avatar) {
        _avatar                    = [[UIImageView alloc]init];
        _avatar.layer.cornerRadius = 30*KEY_RATE;
        _avatar.clipsToBounds      = YES;
        _avatar.userInteractionEnabled = YES;
    }
    return _avatar;
}
- (UIView *)unLoginView {
    if (!_unLoginView) {
        _unLoginView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - WCFTabBarHeight)];
        [self.view addSubview:_unLoginView];
        _unLoginView.backgroundColor = WHITECOLOR;
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake((App_Width - 200)*0.5, 100, 200, 40)];
        [_unLoginView addSubview:btn];
        [btn setTitle:@"登录" forState:UIControlStateNormal];
        btn.layer.cornerRadius = 4;
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = MAINBLUE;
        [btn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(loginBtn) forControlEvents:UIControlEventTouchUpInside];
    }
    return _unLoginView;
}

- (void)loginBtn {
    LoginViewController *vc = [[LoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    App_Delegate.window.rootViewController = nav;
}

@end
