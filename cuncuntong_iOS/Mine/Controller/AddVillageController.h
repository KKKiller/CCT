//
//  AddVillageController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/31.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "SetHomeViewController.h"
//typedef NS_ENUM(NSInteger, SelectStatus) {
//    FirstLogin,
//    NotFirstLogin
//};


typedef void(^callbacks)(NSMutableArray *array);
typedef void(^setHomeselectLocationBlock)(NSString *location, NSString *locationId); //账户资料进来


@interface AddVillageController : BaseViewController

@property (nonatomic, strong) NSMutableArray *followVillageArray;

@property (nonatomic, copy) callbacks block;
@property (nonatomic, copy) setHomeselectLocationBlock selectLocBlock;
@property (nonatomic, assign) SelectStatus selectStatus;


@end
