//
//  NewsViewController.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsCell.h"
#import "NewsModel.h"
#import "SystemNewsController.h"
#import "ActivityNotificationController.h"

@interface NewsViewController ()<UITableViewDelegate,UITableViewDataSource> {
    UITableView *_tableView;
}

@property (nonatomic, strong) NSMutableArray *dataSource;
@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSelf];
    [self prepareData];
    [self uiConfig];
}
- (void)setSelf {
    
    [self addTitle:@"消息"];
    [self addReturnBtn:@"tui_"];
    
}
- (void)prepareData
{
    for (int  i = 0; i<2;i++) {
        NewsModel *model = [[NewsModel alloc]init];
        model.title = i == 0?@"系统消息":@"活动广播";
        model.icon = i == 0?@"btn_xtxx":@"btn_hdgb";
        model.hasNews = i == 0? [UserManager sharedInstance].syspoint: [UserManager sharedInstance].actpoint;
        [self.dataSource addObject:model];
    }
    
}
- (void)uiConfig
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, KEY_WIDTH, KEY_HEIGHT - 64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80*KEY_RATE;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cellID";
    NewsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        cell = [[NewsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setModel:self.dataSource[indexPath.row]];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        SystemNewsController *vc = [[SystemNewsController alloc]init];
        [UserManager sharedInstance].syspoint = @"0";
        [_tableView reloadData];
        NewsModel *model = self.dataSource[indexPath.row];
        model.hasNews = [UserManager sharedInstance].syspoint;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    ActivityNotificationController *vc = [[ActivityNotificationController alloc]init];
    [UserManager sharedInstance].actpoint = @"0";
    [_tableView reloadData];
    NewsModel *model = self.dataSource[indexPath.row];
    model.hasNews = [UserManager sharedInstance].actpoint;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)backMove {
    [super backMove];
    _callback();
}

@end
