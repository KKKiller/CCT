//
//  MineDetailController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, MineDetailType) {
    MineDetailTypeRead,
    MineDetailTypeCol
};

@interface MineDetailController : BaseViewController

@property (nonatomic, assign) MineDetailType mineDetailType;

@end
