//
//  FollowVillageController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/31.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "FollowVillageController.h"
#import "AddVillageController.h"
#import "VillageCell.h"
#import "FollowVillageModel.h"

@interface FollowVillageController () <UICollectionViewDelegate, UICollectionViewDataSource> {
    
    UICollectionView           *_collectionView;
    UICollectionViewFlowLayout *_collectionViewLayout;
}

@property (nonatomic, strong) NSMutableArray *dataSource;

@end

static NSString *const cellIdentifier = @"cellIdentifier";

@implementation FollowVillageController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(238, 238, 238);
    
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"关注的村"];
    [self saveButtonConfig];
    [self setData];
    [self uiConfig];
}

- (void)saveButtonConfig {
    UIButton *saveBtn = [[UIButton alloc] init];
    saveBtn.frame = CGRectMake(0, 0, 30 * KEY_RATE, 20 * KEY_RATE);
    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    saveBtn.titleLabel.font = FontSize(14);
    [saveBtn addTarget:self action:@selector(saveTheChange) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *saveItem = [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItem = saveItem;
}

- (void)uiConfig {
    
    UIButton *addBtn = [[UIButton alloc] init];
    [self.view addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.equalTo(@(49));
    }];
    addBtn.backgroundColor = [UIColor whiteColor];
    [addBtn setTitle:@"+添加更多村庄" forState:UIControlStateNormal];
    addBtn.titleLabel.font = [UIFont systemFontOfSize:15 * KEY_RATE];
    [addBtn setTitleColor:RGB(56, 126, 255) forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(addVillage) forControlEvents:UIControlEventTouchUpInside];
    
    _collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];


    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, KEY_WIDTH, KEY_HEIGHT - 64 - 49) collectionViewLayout:_collectionViewLayout];
    
    //定义每个UICollectionView 的大小
    _collectionViewLayout.itemSize = CGSizeMake(100 * KEY_RATE, 40 * KEY_RATE);
    //定义每个UICollectionView 横向的间距
    _collectionViewLayout.minimumLineSpacing = 10;
    //定义每个UICollectionView 纵向的间距
    _collectionViewLayout.minimumInteritemSpacing = 10;
    //定义每个UICollectionView 的边距距
    _collectionViewLayout.sectionInset = UIEdgeInsetsMake(24 * KEY_RATE, 20 * KEY_RATE, 5, 20 * KEY_RATE);//上左下右
    [self.view addSubview:_collectionView];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = RGB(238, 238, 238);
    [_collectionView registerClass:[VillageCell class] forCellWithReuseIdentifier:cellIdentifier];
}

- (void)setData {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/village")
                                    params:@{@"id": USERID}
                                    target:self
                                   success:^(NSDictionary *success) {
       
        for (NSDictionary *dic in success[@"data"][@"village"]) {
            FollowVillageModel *model = [[FollowVillageModel alloc] init];
            [model modelSetWithDictionary:dic];
            [self.dataSource addObject:model];
        }
        [_collectionView reloadData];
    
    } failure:^(NSError *failure) {
    }];
}

#pragma mark - UICollectionViewDelegate && UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    VillageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.titleLabel.text = [self.dataSource[indexPath.row] val];
    cell.backgroundColor = RGB(56, 126, 255);
    
    cell.layer.cornerRadius = 5 * KEY_RATE;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CCTAlertController *vc = [[CCTAlertController alloc]init];
    vc.messageColor = [UIColor redColor];
    [vc alertViewControllerWithMessage:@"确认要移除么?" block:^{
        
        if (self.dataSource.count == 1) {
            
            [MBProgressHUD showMessage:@"至少要关注一个村"];
            return;
        }
        
        [self.dataSource removeObjectAtIndex:indexPath.item];
        [collectionView reloadData];
    }];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - Lazyload

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - Target Action

- (void)addVillage {
    AddVillageController *vc = [[AddVillageController alloc] init];
    vc.followVillageArray = self.dataSource;
    vc.block = ^(NSMutableArray *array) {
        self.dataSource = array;
        [_collectionView reloadData];
    };
    vc.selectStatus = NotFirstLogin;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)saveTheChange {
    
    
    NSMutableString *mStr = [NSMutableString string];
    
    for (FollowVillageModel *model in self.dataSource) {
        [mStr appendFormat:model.villageId,nil];
        [mStr appendFormat:@",",nil];    }
    
    NSDictionary *paramDic = @{@"id": USERID, @"vid": mStr};
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/updv") params:paramDic target:self success:^(NSDictionary *success) {
        
        [MBProgressHUD showMessage:@"保存成功"];
        
    } failure:^(NSError *failure) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
