//
//  CommentViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/14.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "CommentViewController.h"
#import "CommentCell.h"
#import "DetailViewController.h"

@interface CommentViewController ()<UITableViewDelegate, UITableViewDataSource> {
    UITableView *_tableView;
    NSInteger _page;
}

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) CCEmptyView *emptyView;

@end

static NSString *const cellIdentifier = @"cellIdentifier";

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"评论列表"];
    [self uiConfig];
}

- (void)reloadDataSource {
    _page = 0;
    [self.dataSource removeAllObjects];
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"main/comment")
                                    params:@{@"id":USERID, @"page": [NSString stringWithFormat:@"%ld", _page]}
                                    target:self
                                   success:^(NSDictionary *success) {
       
                                       for (NSDictionary *dic in success[@"data"][@"comment"]) {
                                           CommentModel *model = [CommentModel modelWithDictionary:dic];
                                           [self.dataSource addObject:model];
                                       }
                                       self.emptyView.hidden = self.dataSource.count != 0;
                                       [_tableView reloadData];
                                       [_tableView.mj_header endRefreshing];
    } failure:^(NSError *failure) {
        [_tableView.mj_header endRefreshing];
        
    }];
}

- (void)loadMore {
    _page++;
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"main/comment")
                                    params:@{@"id":USERID, @"page": [NSString stringWithFormat:@"%ld", _page]}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       
                                       for (NSDictionary *dic in success[@"data"][@"comment"]) {
                                           CommentModel *model = [CommentModel modelWithDictionary:dic];
                                           [self.dataSource addObject:model];
                                       }
                                       [_tableView reloadData];
                                       [_tableView.mj_footer endRefreshing];
                                   } failure:^(NSError *failure) {
                                       _page--;
                                       [_tableView.mj_footer endRefreshing];
                                   }];

}

- (void)uiConfig {
    _tableView = [[UITableView alloc] init];
    _tableView.delegate   = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT - 64);
    }];
    _tableView.tableFooterView = [UIView new];
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(reloadDataSource)];
    [_tableView.mj_header beginRefreshing];
    
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[CommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailViewController *vc = [[DetailViewController alloc] init];
    CommentModel *model = self.dataSource[indexPath.row];
    vc.url     = model.url;
    vc.village = model.village;
    vc.aid     = model.articleId;
    vc.iscol   = model.iscol;
    vc.mini    = model.mini;
    vc.block = ^(BOOL isCol) {
        model.iscol = [NSString stringWithFormat:@"%i",isCol];
        [self reloadDataSource];
    };
    [self.navigationController pushViewController:vc animated:YES];

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 112*KEY_RATE;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
@end
