//
//  ActivityNotificationController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/22.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "ActivityNotificationController.h"
#import "ActivityModel.h"
#import "MineDetailCell.h"
#import "DetailViewController.h"

@interface ActivityNotificationController ()<UITableViewDataSource, UITableViewDelegate> {
    NSInteger _page;
    BOOL _isFirst;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) UIImageView *emptyView;
@property (nonatomic, strong) UILabel *emptyLabel;


@end

@implementation ActivityNotificationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _isFirst = YES;
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"活动广播"];
    [self uiConfig];
}

- (void)prepareData {
    _page = 0;
    [self.dataSource removeAllObjects];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"main/activity")
                                    params:@{@"id": USERID,
                                             @"page":[NSString stringWithFormat:@"%ld", _page]}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       
                                       for (NSDictionary *dic in success[@"data"][@"article"]) {
                                           ActivityModel *model = [ActivityModel modelWithDictionary:dic];
                                           [self.dataSource addObject:model];
                                       }
                                       
                                       if (self.dataSource.count == 0) {
                                           [_tableView addSubview:self.emptyView];
                                           [_tableView addSubview:self.emptyLabel];
                                           [self.emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
                                               make.centerX.equalTo(_tableView);
                                               make.width.mas_equalTo(90 * KEY_RATE);
                                               make.height.mas_equalTo(80 * KEY_RATE);
                                               make.top.equalTo(self.view.mas_top).with.offset(130);
                                           }];
                                           [self.emptyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                                               make.centerX.left.right.equalTo(_tableView);
                                               make.top.equalTo(self.emptyView.mas_bottom).with.offset(25);
                                               make.height.mas_equalTo(30 * KEY_RATE);
                                           }];
                                           _isFirst = NO;
                                       }else {
                                           [self.emptyLabel removeFromSuperview];
                                           self.emptyLabel = nil;
                                           [self.emptyView removeFromSuperview];
                                           self.emptyView = nil;
                                           _isFirst = YES;
                                       }
                                       [_tableView reloadData];
                                       [self.tableView.mj_header endRefreshing];
    } failure:^(NSError *failure) {
        [self.tableView.mj_header endRefreshing];
        _isFirst = YES;
    }];

}

- (void)loadMore {
    _page++;
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"main/activity")
                                    params:@{@"id": USERID,
                                             @"page":[NSString stringWithFormat:@"%ld", _page]}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       
                                       for (NSDictionary *dic in success[@"data"][@"article"]) {
                                           ActivityModel *model = [ActivityModel modelWithDictionary:dic];
                                           [self.dataSource addObject:model];
                                       }
                                       
                                     [_tableView reloadData];
                                       [self.tableView.mj_footer endRefreshing];
                                   } failure:^(NSError *failure) {
                                       _page--;
                                       [self.tableView.mj_footer endRefreshing];
                                   }];}

- (void)uiConfig {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT - 64);
    }];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(prepareData)];
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
    [self.tableView.mj_header beginRefreshing];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellId";
    MineDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[MineDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.model = self.dataSource[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = [MyUtil calculateLabelText:[self.dataSource[indexPath.row] title] sizeMarkW:(KEY_WIDTH - 2 * 10 * KEY_RATE) sizeMarkH:0 textFont:18 * KEY_RATE].height;
    
    return height + 2 * 10 * KEY_RATE +  2 * 10 * KEY_RATE + 15 * KEY_RATE;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailViewController *vc = [[DetailViewController alloc] init];
    ActivityModel *model = self.dataSource[indexPath.row];
    vc.url     = model.url;
    vc.village = model.village;
    vc.aid     = model.articleId;
    vc.iscol   = model.iscol;
    vc.mini    = model.mini;
    vc.block = ^(BOOL isCol) {
        model.iscol = [NSString stringWithFormat:@"%i",isCol];
    };
    [self.navigationController pushViewController:vc animated:YES];
}



- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
    }
    return _tableView;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isFirst) {
        [self.emptyView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top).with.offset(130-scrollView.contentOffset.y);
        }];
    }
}

- (UIImageView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[UIImageView alloc] init];
        _emptyView.image = [UIImage imageNamed:@"img_zwhd"];
    }
    return _emptyView;
}

- (UILabel *)emptyLabel {
    if (!_emptyLabel) {
        _emptyLabel = [[UILabel alloc] init];
        _emptyLabel.text = @"暂无系统消息";
        _emptyLabel.font = FontSize(18);
        _emptyLabel.textColor = [UIColor colorWithHexString:@"#666666"];
        _emptyLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _emptyLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
