//
//  ChangeNameController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/20.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "ChangeNameController.h"

@interface ChangeNameController () {
    UITextField *_nameTF;
    UILabel *_countLabel;
}

@end

@implementation ChangeNameController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"姓名"];
    [self naviConfig];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    [self uiConfig];
}

- (void)naviConfig {
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 20 * KEY_RATE)];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    btn.titleLabel.font = FontSize(14);
    [btn addTarget:self action:@selector(saveName) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)uiConfig {
    _countLabel = [[UILabel alloc] init];
    _countLabel.textAlignment = NSTextAlignmentRight;
    _countLabel.textColor     = [UIColor colorWithHexString:@"548FFE"];
    _countLabel.font          = FontSize(14);
    _countLabel.text          = [NSString stringWithFormat:@"%d/10",0];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 50 * KEY_RATE)];

    
    _nameTF = [[UITextField alloc] init];
    _nameTF.backgroundColor = [UIColor whiteColor];
    
    _nameTF.leftView = view;
    _nameTF.leftViewMode = UITextFieldViewModeAlways;
    
    [_nameTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addSubview:_countLabel];
    [self.view addSubview:_nameTF];
    
    [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(64 + 15 * KEY_RATE);
        make.right.mas_equalTo(-15 * KEY_RATE);
        make.height.mas_equalTo(20 * KEY_RATE);
        make.width.mas_equalTo(100 * KEY_RATE);
    }];
    
    [_nameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(_countLabel.mas_bottom).with.offset(10 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == _nameTF) {
        if (textField.text.length <= 10) {
            _countLabel.text = [NSString stringWithFormat:@"%ld/10", textField.text.length];
        }
        
        if (textField.text.length > 10) {
            textField.text = [textField.text substringToIndex:10];
        }
    }
}


- (void)saveName {
    
    _callback(_nameTF.text);
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
