//
//  NewsViewController.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^Callback)();

@interface NewsViewController : BaseViewController

@property (nonatomic, copy) Callback callback;

@end
