//
//  AddVillageController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/31.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "AddVillageController.h"
#import <FMDB.h>
#import "PinYin4Objc.h"
#import "AddTitleView.h"
#import "SectionHeader.h"
#import "FollowVillageView.h"
#import "FollowVillageModel.h"
#import "CCTTextField.h"
#import "TabBarController.h"

@interface AddVillageController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, AddVillageDelegate, FollowDeleteDelegate, UITextFieldDelegate> {
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    FMDatabase *_dataBase;
    NSString *_currentTitle;
    AddTitleView *_titleView;
    FollowVillageView *_followVillageView;
    BOOL _isSearch;
}
// 保存所有人名称的字典，人名按钮首字符放在不同键值对当中
@property (nonatomic, strong) NSMutableDictionary * dictNames;
@property (nonatomic, strong) NSMutableDictionary * dicNameId;

// 索引标题数组
@property (nonatomic, strong) NSArray *arrayIndexTitles;
@property (nonatomic, strong) NSMutableSet *identifySet;


@property (nonatomic, strong) UIView *coverView;

@property (nonatomic, strong) UITableView *secondTableView;

@property (nonatomic, strong) NSMutableArray *secondaryTitleArray;

@property(nonatomic,strong) NSIndexPath *secondSelectIndexPath;

@property (nonatomic, strong) SectionHeader *sectionHeader;

@property (nonatomic, strong) NSMutableArray *lastResultArray;

@property (nonatomic, strong) NSMutableArray *searchArray;
@end

static NSString *const mainCellIdentifier = @"mainCellIdentifier";

static NSString *const subCellIdentifier = @"subCellIdentifier";

@implementation AddVillageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = RGB(238, 238, 238);
    [self uiConfig];
//    [self dbConfig];
//    [self setData];
    [self getAreaByPid:@"0"];
    _isSearch = NO;
    
}

- (void)setData {
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"main/hot") params:nil success:^(NSDictionary *success) {
       
        NSLog(@"success = %@",success);
    } failure:^(NSError *failure) {
        NSLog(@"failure = %@",failure);
    }];
}

/**
 x 获取地区列表

 @param pid pid=0即获取省列表，可根据返回列表的ID获取市列表，以此类推
 */
-(void)getAreaByPid:(NSString *)pid{
    NSDictionary *parDic = @{@"pid" : pid};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"main/area") params:parDic success:^(NSDictionary *success) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        NSLog(@"success = %@",success);
        if ([[success objectForKey:@"data"]isKindOfClass:[NSDictionary class]]) {
            NSDictionary *data = [success objectForKey:@"data"];
            if ([[data objectForKey:@"village"]isKindOfClass:[NSArray class]]) {
                NSArray *village = [data objectForKey:@"village"];
                if ([pid integerValue] == 0 ) {
                    [self setDataByAreaArray:village];
                }else{
                    if (self.selectLocBlock && village.count == 0) {
                        
                        self.selectLocBlock(_currentTitle, pid);
                        [self dismissViewControllerAnimated:YES completion:nil];
                        return ;
                    }
                    self.secondaryTitleArray = [NSMutableArray arrayWithArray:village];
                    [_secondTableView reloadData];
                    [self secondTableViewSelect];
                }
            }
        }
    } failure:^(NSError *failure) {
        NSLog(@"failure = %@",failure);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

/**
 x 设置数据

 @param array 传入的数组
 */
-(void)setDataByAreaArray:(NSArray *)array{
    
    self.dictNames = [NSMutableDictionary dictionary];
    
    HanyuPinyinOutputFormat *outputFormat=[[HanyuPinyinOutputFormat alloc] init];
    [outputFormat setToneType:ToneTypeWithoutTone];
    [outputFormat setVCharType:VCharTypeWithUUnicode];
    [outputFormat setCaseType:CaseTypeUppercase];
    for (NSDictionary *na in array) {
//        NSLog(@"%@",[na objectForKey:@"name"]);
        NSString *name = [NSString stringWithFormat:@"%@",[na objectForKey:@"name"]];
        
        NSString *pinyin = [[PinyinHelper getFirstHanyuPinyinStringWithChar:[name characterAtIndex:0] withHanyuPinyinOutputFormat:outputFormat] substringWithRange:NSMakeRange(0, 1)];
        
        NSMutableArray *array = [self.dictNames objectForKey:pinyin];
        
        if (array == nil) { // 键值对还不存在
            // 创建数组，保存人名
            NSMutableArray *array = [NSMutableArray arrayWithObject:name];
            // 往字典中添加一个键值对
            [self.dictNames setObject:array forKey:pinyin];
            
        } else
        {  // 键值对已经存在
            [array addObject:name];
        }
        
        NSArray *keyArrays = [self.dictNames allKeys];
//        NSLog(@"count = %li",keyArrays.count);
        self.arrayIndexTitles = [keyArrays sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
//        NSLog(@"arrayIndexTitles = %@",self.arrayIndexTitles);
        
        self.dicNameId[name] = [na objectForKey:@"id"];
    }
    
    [_tableView reloadData];

}

/**
   x secondTableView 选择判断
 */
-(void)secondTableViewSelect{
    if (self.secondaryTitleArray.count == 0) {
        [self remove];
        
        if (self.followVillageArray.count == 6) {
            [MBProgressHUD showMessage:@"最多可以添加六个"];
            return;
        }
        
        FollowVillageModel *model = [[FollowVillageModel alloc] init];
        
        NSDictionary *dic = _lastResultArray[self.secondSelectIndexPath.row];
        model.villageId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
        model.val = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
        //        model.villageId = _lastResultArray[self.secondSelectIndexPath.row][2];
        //        model.val = _lastResultArray[self.secondSelectIndexPath.row][1];
        
        for (FollowVillageModel *m in self.followVillageArray) {
            if ([m.villageId isEqualToString:model.villageId]) {
                [MBProgressHUD showMessage:@"不能重复添加"];
                return;
            }
        }
        
        [self.followVillageArray addObject:model];
        [_titleView titleLabelConfig:self.followVillageArray.count];
        
        // 更新布局
        CGFloat height =self.followVillageArray.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
        [_followVillageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
        }];
        
        [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(KEY_HEIGHT - 64 - 50 * KEY_RATE - height + 50);
        }];
        
        [_followVillageView reloadSelf:self.followVillageArray];
        
        
        return;
    }   else {
        _lastResultArray = [self.secondaryTitleArray copy];
    }
    
}


// Open database and search.
//- (void)dbConfig {
//    NSString *dbPath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"sqlite"];
//    _dataBase = [FMDatabase databaseWithPath:dbPath];
//    
//    if ([_dataBase open]) {
//        BOOL result = [_dataBase executeUpdate:@"create table if not exists village (id integer PRIMARY KEY AUTOINCREMENT NOT NULL,identify integer NOT NULL, name text, 'order' text, parent text, children text, pid integer);"];
//        if (result) {
//            [self select];
//        }else{
//            
//        }
//    }
//    [_dataBase close];
//}

- (void)select{
    //创建查询结果对象
    
    self.dictNames = [NSMutableDictionary dictionary];

    FMResultSet *resultSet = [_dataBase executeQuery:@"select * from village where pid = 0 order by identify asc"];
    
    //遍历结果
    while ([resultSet next]) {
        NSString *name = [resultSet stringForColumn:@"name"];
        
        HanyuPinyinOutputFormat *outputFormat=[[HanyuPinyinOutputFormat alloc] init];
        [outputFormat setToneType:ToneTypeWithoutTone];
        [outputFormat setVCharType:VCharTypeWithUUnicode];
        [outputFormat setCaseType:CaseTypeUppercase];
        
        NSString *pinyin = [[PinyinHelper getFirstHanyuPinyinStringWithChar:[name characterAtIndex:0] withHanyuPinyinOutputFormat:outputFormat] substringWithRange:NSMakeRange(0, 1)];
        
        NSMutableArray *array = [self.dictNames objectForKey:pinyin];
        
        if (array == nil) { // 键值对还不存在
            // 创建数组，保存人名
            NSMutableArray *array = [NSMutableArray arrayWithObject:name];
            // 往字典中添加一个键值对
            [self.dictNames setObject:array forKey:pinyin];
            
        } else
        {  // 键值对已经存在
            [array addObject:name];
        }
        NSArray *keyArrays = [self.dictNames allKeys];
        
        self.arrayIndexTitles = [keyArrays sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
    
        self.dicNameId[name] = @[[resultSet stringForColumn:@"children"], [resultSet stringForColumn:@"identify"]];
        
//        NSLog(@"dicNameId = %@",self.dicNameId);
        
    }
    [resultSet close];
    
//    [self getAreaByPid:@"0"];
    [_tableView reloadData];

}

- (void)uiConfig {
    
    _titleView = [[AddTitleView alloc] init:self.followVillageArray.count];
    _titleView.delegate = self;
    if(self.selectLocBlock){
        _titleView.titleLabel.text = @"选择地址";
    }
    [self.view addSubview:_titleView];
    [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(64);
    }];
    
//    CCTTextField *searchTF = [[CCTTextField alloc] init];
//    searchTF.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:searchTF];
//    
//    
//    [searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self.view);
//        make.top.equalTo(@64);
////        make.height.equalTo(@(50 * KEY_RATE));
//        make.height.equalTo(@(0 * KEY_RATE));
//    }];
//    
//    UIImageView *searchImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 18 * KEY_RATE, 18 * KEY_RATE) image:@"btn_sousuo"];
//    searchTF.leftView = searchImgv;
//    searchTF.placeholder = @"输入地点名称";
//    searchTF.delegate = self;
//    [searchTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    CGFloat height = self.followVillageArray.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
    _followVillageView = [[FollowVillageView alloc] initWithArray:self.followVillageArray];
    [self.view addSubview:_followVillageView];
    [_followVillageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
//        make.top.equalTo(searchTF.mas_bottom);
        make.top.equalTo(@64);
        make.height.mas_equalTo(height);
    }];
    _followVillageView.delegate = self;
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(_followVillageView.mas_bottom);
        make.height.mas_equalTo(KEY_HEIGHT - 64 - 0 * KEY_RATE - height );
    }];
    

    
    _tableView.sectionIndexColor = [UIColor lightGrayColor];
}
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _tableView) {
        if (_isSearch) {
            return 1;
        }
        return  self.arrayIndexTitles.count;// 15
    }
        return 1;// 1
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableView) {
        if (_isSearch) {
            return self.searchArray.count;
        }
        NSString *key = [self.arrayIndexTitles objectAtIndex:section];    // 取到section分组显示的索引名称
        return [[self.dictNames objectForKey:key] count];    // 返回该分组的人的个数
    }
    return self.secondaryTitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    if (tableView == _tableView) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:mainCellIdentifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:mainCellIdentifier];
            }
        if (_isSearch) {
            cell.textLabel.text = self.searchArray[indexPath.row];
        }else {
            NSString *firstChar = [self.arrayIndexTitles objectAtIndex:indexPath.section];
            
            // 取到首字母对应的人名数组
            NSArray *arrayNames = [self.dictNames objectForKey:firstChar];
            
            // 取到人名，设置在cell上显示
            cell.textLabel.text = [arrayNames objectAtIndex:indexPath.row];
        }
            return cell;


    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:subCellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:subCellIdentifier];
        }
//        cell.textLabel.text = self.secondaryTitleArray[indexPath.row][1];
        
        NSDictionary *dictionary = [self.secondaryTitleArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [dictionary objectForKey:@"name"];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:15 * KEY_RATE];
        return cell;
    }
    

}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == _tableView) {
        return self.arrayIndexTitles;
    }
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == _tableView) {
        if (_isSearch) {
            return @"";
        }
        return  [self.arrayIndexTitles objectAtIndex:section];
    }
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45 * KEY_RATE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == _tableView) {
        return  35 * KEY_RATE;
    }
    return 45 * KEY_RATE;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView != _secondTableView) {
        return nil;
    }
    [self.sectionHeader titleLabelConfig:_currentTitle];
    return self.sectionHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == _tableView) {
//        [_dataBase open];
        
        [self.secondaryTitleArray removeAllObjects];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(remove)];
        tap.delegate = self;
        [self.coverView addGestureRecognizer:tap];
        
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        _currentTitle = cell.textLabel.text;
        
//        NSLog(@"_currentTitle = %@",[self.dicNameId objectForKey:_currentTitle]);
        [self getAreaByPid:[self.dicNameId objectForKey:_currentTitle]];
        
//        NSString *sql = [NSString stringWithFormat:@"select * from village where identify in (%@) and pid = (%@) order by identify asc",self.dicNameId[cell.textLabel.text][0], self.dicNameId[cell.textLabel.text][1]];
//        
//        FMResultSet *resultSet = [_dataBase executeQuery:sql];
//        while ([resultSet next]) {
//            
//            NSString *name = [resultSet stringForColumn:@"name"];
//            NSString *identify = [resultSet stringForColumn:@"identify"];
//            NSString *children = [resultSet stringForColumn:@"children"];
//            HanyuPinyinOutputFormat *outputFormat=[[HanyuPinyinOutputFormat alloc] init];
//            [outputFormat setToneType:ToneTypeWithoutTone];
//            [outputFormat setVCharType:VCharTypeWithUUnicode];
//            [outputFormat setCaseType:CaseTypeUppercase];
//            
//            NSString *pinyin = [[PinyinHelper getFirstHanyuPinyinStringWithChar:[name characterAtIndex:0] withHanyuPinyinOutputFormat:outputFormat] substringWithRange:NSMakeRange(0, 1)];
//            [self.secondaryTitleArray addObject:@[pinyin, name, identify, children]];
//        }
//        
//        [self.secondaryTitleArray sortUsingComparator:^NSComparisonResult(NSArray *_Nonnull obj1, NSArray *_Nonnull obj2) {
//            
//            return [obj1[0] localizedCompare:obj2[0]];
//        }];
//        [_secondTableView reloadData];
//        [resultSet close];
//        [_dataBase close];
    } else {
//        [_dataBase open];
        
        self.secondSelectIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        _currentTitle = cell.textLabel.text;
        
        NSDictionary *dic = [self.secondaryTitleArray objectAtIndex:indexPath.row];
        [self getAreaByPid:[dic objectForKey:@"id"]];
//        NSString *sql = [NSString stringWithFormat:@"select * from village where identify in (%@) and pid = (%@) order by identify asc",self.secondaryTitleArray[indexPath.row][3], self.secondaryTitleArray[indexPath.row][2]];
//        
//        [self.secondaryTitleArray removeAllObjects];
//
//        FMResultSet *resultSet = [_dataBase executeQuery:sql];
//        while ([resultSet next]) {
//            
//            NSString *name = [resultSet stringForColumn:@"name"];
//            NSString *identify = [resultSet stringForColumn:@"identify"];
//            NSString *children = [resultSet stringForColumn:@"children"];
//            HanyuPinyinOutputFormat *outputFormat=[[HanyuPinyinOutputFormat alloc] init];
//            [outputFormat setToneType:ToneTypeWithoutTone];
//            [outputFormat setVCharType:VCharTypeWithUUnicode];
//            [outputFormat setCaseType:CaseTypeUppercase];
//            
//            NSString *pinyin = [[PinyinHelper getFirstHanyuPinyinStringWithChar:[name characterAtIndex:0] withHanyuPinyinOutputFormat:outputFormat] substringWithRange:NSMakeRange(0, 1)];
//            if (pinyin==nil) {
//                pinyin = @"";
//            }
//            [self.secondaryTitleArray addObject:@[pinyin, name, identify, children]];
//        }
//        [resultSet close];
//        [_dataBase close];
//        
//        [self.secondaryTitleArray sortUsingComparator:^NSComparisonResult(NSArray *_Nonnull obj1, NSArray *_Nonnull obj2) {
//            
//            return [obj1[0] localizedCompare:obj2[0]];
//        }];
//        [_secondTableView reloadData];

        // Justice whether the secondaryTitleArray's count is zero
        // If true, return
        
        
//        if (self.secondaryTitleArray.count == 0) {
//            [self remove];
//            
//            if (self.followVillageArray.count == 6) {
//                [MBProgressHUD showMessage:@"最多可以添加六个"];
//                return;
//            }
//            
//            
//            
//            FollowVillageModel *model = [[FollowVillageModel alloc] init];
//            model.villageId = _lastResultArray[indexPath.row][2];
//            model.val = _lastResultArray[indexPath.row][1];
//            
//            for (FollowVillageModel *m in self.followVillageArray) {
//                if ([m.villageId isEqualToString:model.villageId]) {
//                    [MBProgressHUD showMessage:@"不能重复添加"];
//                    return;
//                }
//            }
//            
//            [self.followVillageArray addObject:model];
//            [_titleView titleLabelConfig:self.followVillageArray.count];
//            
//            // 更新布局
//            CGFloat height =self.followVillageArray.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
//            [_followVillageView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(height);
//            }];
//            
//            [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(KEY_HEIGHT - 64 - 50 * KEY_RATE - height + 50);
//            }];
//            
//            [_followVillageView reloadSelf:self.followVillageArray];
//            
//            
//            return;
//        }   else {
//            _lastResultArray = [self.secondaryTitleArray copy];
//        }

   }
}

#pragma mark - Lazy Load

- (UIView *)coverView {
    if (!_coverView) {
        _coverView = [[UIView alloc] init];
        [self.view addSubview:_coverView];
        [_coverView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _coverView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.secondTableView.hidden = NO;
    }
    return _coverView;
}

- (UITableView *)secondTableView {
    if (!_secondTableView) {
        _secondTableView = [[UITableView alloc] init];
        [self.coverView addSubview:_secondTableView];
        [_secondTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(17 * KEY_RATE);
            make.left.mas_equalTo(15 * KEY_RATE);
            make.right.mas_equalTo(-15 * KEY_RATE);
            make.height.mas_equalTo(540 * KEY_RATE);
        }];
        _secondTableView.delegate = self;
        _secondTableView.dataSource = self;
    }
    return _secondTableView;
}

- (NSMutableDictionary *)dicNameId {
    if (!_dicNameId) {
        _dicNameId = [[NSMutableDictionary alloc] init];
    }
    return _dicNameId;
}

- (NSMutableArray *)secondaryTitleArray {
    if (!_secondaryTitleArray) {
        _secondaryTitleArray = [[NSMutableArray alloc] init];
    }
    return _secondaryTitleArray;
}

- (SectionHeader *)sectionHeader {
    if (!_sectionHeader) {
        _sectionHeader = [[SectionHeader alloc]
                          initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 45 * KEY_RATE)];
    }
    return _sectionHeader;
}


- (void)remove {
    [_coverView removeFromSuperview];
    _coverView = nil;
    _secondTableView = nil;
}

- (NSMutableArray *)followVillageArray {
    
    if (!_followVillageArray) {
        _followVillageArray = [[NSMutableArray alloc] init];
    }
    return _followVillageArray;
}

- (NSMutableArray *)searchArray {
    if (!_searchArray) {
        _searchArray = [[NSMutableArray alloc] init];
    }
    return _searchArray;
}

-(NSIndexPath *)secondSelectIndexPath{
    if (!_secondSelectIndexPath) {
        _secondSelectIndexPath = [NSIndexPath indexPathForRow:-1 inSection:0];
    }
    return _secondSelectIndexPath;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.secondTableView]) {
        return NO;
    }
    return YES;
}

#pragma mark - Target Action
- (void)addVillage {
    if(self.selectLocBlock){
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    NSMutableString *mStr = [NSMutableString string];
    for (FollowVillageModel *model in self.followVillageArray) {
        
        [mStr appendFormat:model.villageId,nil];
        [mStr appendFormat:@",",nil];
    }
    NSDictionary *paramDic = @{@"id": USERID, @"vid": mStr};
//    [self dismissViewControllerAnimated:YES completion:^{
//        _block(nil);
//    
//    }];
//    return;
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/updv") params:paramDic target:self success:^(NSDictionary *success) {
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            if (_selectStatus == FirstLogin) {
                
                TabBarController *tabbar = [[TabBarController alloc] init];
                [UIApplication sharedApplication].keyWindow.rootViewController = tabbar;
                [App_Delegate getUserDataFinishBlock:nil];
            }else {
                _block(self.followVillageArray);
            }
        }];
        [MBProgressHUD showMessage:@"保存成功"];
    } failure:^(NSError *failure) {
    }];
}

- (void)deleteFollow:(NSInteger )x {
    
    CCTAlertController *vc = [[CCTAlertController alloc]init];
    vc.messageColor = [UIColor redColor];
    [vc alertViewControllerWithMessage:@"确认要移除么?" block:^{
        
        if (self.followVillageArray.count == 1) {
            
            [MBProgressHUD showMessage:@"至少要关注一个村"];
            return;
        }
        [self.followVillageArray removeObjectAtIndex:x];
        
        [_titleView titleLabelConfig:self.followVillageArray.count];

//         更新布局
        CGFloat height = self.followVillageArray.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
        [_followVillageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
        }];
        [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(KEY_HEIGHT - 64 - 50 * KEY_RATE - height);
        }];
        
        [_followVillageView reloadSelf:self.followVillageArray];
    }];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark 模糊查询
- (void)textFieldDidChange:(UITextField *)textField {
    _isSearch = textField.text.length == 0?  NO: YES;
    self.searchArray = [self searchData:textField.text];
    [_tableView reloadData];
}

- (NSMutableArray *)searchData:(NSString *)str {
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 0; i < self.dicNameId.allKeys.count; i++) {
        if ([self.dicNameId.allKeys[i] rangeOfString:str].location != NSNotFound) {
            [array addObject:self.dicNameId.allKeys[i]];
        }
    }
    return array;
}
@end
