//
//  AccountViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/1.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "AccountViewController.h"
#import <FMDB.h>
#import "SectionHeader.h"
#import "ChangeNameController.h"
#import "AddVillageController.h"
#import "ResetViewController.h"
#import "CCChangePhoneController.h"
@interface AccountViewController ()<UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate> {
    UITableView *_tableView;
    UIButton    *_userIcon;
    FMDatabase *_dataBase;
    NSString *_currentTitle;
    NSString *_localId;
}

@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSMutableArray *contentArray;
@property (nonatomic, strong) NSMutableArray *childrenIdArray;
@property (nonatomic, strong) UIView *coverView;
@property (nonatomic, strong) UITableView *addressTableView;
@property (nonatomic, strong) SectionHeader *sectionHeader;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSMutableArray *firstArray;

@end

static NSString *const cellIdentifier = @"cellIdentifier";
static NSString *const addressCellId = @"addressCellId";

@implementation AccountViewController   

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"账户资料"];
    [self naviConfig];
    [self uiConfig];
//    [self dbConfig];
    _currentTitle = @"省";
    _localId = [UserManager sharedInstance].location;
}

- (void)naviConfig {
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 20 * KEY_RATE)];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    btn.titleLabel.font = FontSize(14);
    [btn addTarget:self action:@selector(updateInfo) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)updateInfo {
    
    NSDictionary *dic = @{@"id": [UserManager sharedInstance].secret,
                          @"portrait": [UserManager sharedInstance].path,
                          @"realname": self.contentArray[0],
                          @"location": _localId};
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/update") params:dic target:self success:^(NSDictionary *success) {
        [MBProgressHUD showMessage:@"保存成功"];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *failure) {
    }];
}


- (void)uiConfig {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 100 * KEY_RATE)];
    header.backgroundColor = [UIColor whiteColor];
    _tableView                  = [[UITableView alloc] init];
    _tableView.delegate         = self;
    _tableView.dataSource       = self;
    _tableView.tableFooterView  = [UIView new];
    _tableView.tableHeaderView  = header;
    _tableView.separatorColor   = [UIColor colorWithHexString:@"#EEEEEE"];

    _userIcon                       = [[UIButton alloc] init];
    _userIcon.layer.cornerRadius    = 30 * KEY_RATE;
    _userIcon.layer.masksToBounds   = YES;
    _userIcon.layer.borderWidth     = 0.5;
    _userIcon.layer.borderColor     = [UIColor lightGrayColor].CGColor;
    [_userIcon addTarget:self action:@selector(showActionSheet)
        forControlEvents:UIControlEventTouchUpInside];
    [_userIcon setBackgroundImageWithURL:[NSURL URLWithString: [[UserManager sharedInstance] portrait]]
                                forState:UIControlStateNormal placeholder:[UIImage imageNamed:@"touxiang"]];

    UIView *line         = [[UIView alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image        = [UIImage imageNamed:@"img_xztx"];
    
    [self.view addSubview:_tableView];
    [header addSubview:_userIcon];
    [header addSubview:line];
    [header addSubview:imageView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(KEY_HEIGHT - 64);
    }];
    [_userIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(header);
        make.size.mas_equalTo(60 * KEY_RATE);
    }];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(header);
        make.height.equalTo(@0.5);
    }];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(200 * KEY_RATE);
        make.top.mas_equalTo(60 * KEY_RATE);
        make.size.mas_equalTo(18 * KEY_RATE);
    }];

}

//- (void)dbConfig {
//    NSString *dbPath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"sqlite"];
//    _dataBase = [FMDatabase databaseWithPath:dbPath];
//    
//        BOOL result = [_dataBase executeUpdate:@"create table if not exists village (id integer PRIMARY KEY AUTOINCREMENT NOT NULL,identify integer NOT NULL, name text, 'order' text, parent text, children text, pid integer);"];
//    if(result){
//        FMResultSet *resultSet = [_dataBase executeQuery:@"select * from village where pid = 0 order by identify asc"];
//        while ([resultSet next]) {
//            [self.childrenIdArray addObject:@[[resultSet stringForColumn:@"name"],
//                                              [resultSet stringForColumn:@"children"],
//                                              [resultSet stringForColumn:@"identify"]]];
//            _firstArray = [self.childrenIdArray mutableCopy];
//        }
//    }
//
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tableView) {
        return self.titleArray.count;
    }
    return self.childrenIdArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (tableView == _tableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        cell.textLabel.text = self.titleArray[indexPath.row];
        cell.textLabel.font = FontSize(16);
        cell.detailTextLabel.textColor = RGB(51, 51, 51);
        cell.detailTextLabel.font = FontSize(16);
        cell.detailTextLabel.numberOfLines = 0;
        if (self.contentArray.count > indexPath.row) {
            cell.detailTextLabel.text = self.contentArray[indexPath.row];
        }
        
        if (indexPath.row != 0) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [tableView setLayoutMargins:UIEdgeInsetsZero];
        }
        return cell;
//    }
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addressCellId];
//    if (!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addressCellId];
//    }
//    cell.textLabel.text = self.childrenIdArray[indexPath.row][0];
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.textLabel.font = [UIFont systemFontOfSize:15 * KEY_RATE];
//    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50 * KEY_RATE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == _tableView) {
        return 0.0001;
    }
    return 45 * KEY_RATE;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    if (tableView == _tableView) {
        if (indexPath.row == 0){
            ChangeNameController *vc = [[ChangeNameController alloc] init];
            vc.callback = ^(NSString *str) {
                self.contentArray[0] = str;
                [_tableView reloadData];
                [UserManager sharedInstance].realname = str;
            };
            [self.navigationController pushViewController:vc animated:YES];
//            return;
        }else if(indexPath.row == 1){
            AddVillageController *vc = [[AddVillageController alloc]init];
            vc.selectLocBlock = ^(NSString *location, NSString *locationId) {
                self.contentArray[1] = location;
                _localId = locationId;
                [_tableView reloadData];
            };
            [self presentViewController:vc animated:YES completion:nil];
            
        }else if (indexPath.row == 2){//更改手机号
            CCChangePhoneController *vc = [[CCChangePhoneController alloc]init];
            vc.navTitle = @"更换手机号";
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 3){//修改密码
            ResetViewController *vc = [[ResetViewController alloc] init];
            vc.navTitle = @"修改密码";
            vc.phone = [Def valueForKey:@"phone"];
            [self.navigationController pushViewController:vc animated:YES];
        }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView != _addressTableView) {
        return nil;
    }
    [self.sectionHeader titleLabelConfig:_currentTitle];
    return self.sectionHeader;
}

- (void)showActionSheet {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:0];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"从相册选取" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }];
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"拍照" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [self presentViewController:alertController animated:YES completion:nil];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [alertController addAction:photoAction];
    } else {
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
      
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        NSData *data = [[NSData alloc] initWithData:UIImageJPEGRepresentation([self compressImage:image], 1)];
        
        [_userIcon setImage:[UIImage imageWithData:UIImageJPEGRepresentation([self compressImage:image], 1)] forState:UIControlStateNormal];

        // Upload userIcon
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"common/upload") params:nil target:nil imgaeData:data success:^(NSDictionary *success) {
            
            
            // Change the  portrait property and path property of singleton UserManager
            [UserManager sharedInstance].portrait = success[@"data"][@"url"];
            [UserManager sharedInstance].path = success[@"data"][@"path"];
            
        } failure:^(NSError *failure) {
        }];
        
    }];
}

- (UIImage *)compressImage:(UIImage *)image {
    
    CGSize size = {120 * KEY_RATE, 120 * KEY_RATE};
    UIGraphicsBeginImageContext(size);
    CGRect rect = {{0,0}, size};
    [image drawInRect:rect];
    UIImage *compressedImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return compressedImg;
}

#pragma mark - Lazy Load

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = @[@"用户昵称", @"所在县",@"变更手机号",@"修改密码"];
    }
    return _titleArray;
}

- (NSMutableArray *)contentArray {
    if (!_contentArray) {
        _contentArray = [NSMutableArray arrayWithObjects:[UserManager sharedInstance].realname,
                         [UserManager sharedInstance].locationText, nil];
    }
    return _contentArray;
}

- (NSMutableArray *)childrenIdArray {
    if (!_childrenIdArray) {
        _childrenIdArray = [[NSMutableArray alloc] init];
    }
    return _childrenIdArray;
}

- (UIView *)coverView {
    if (!_coverView) {
        _coverView = [[UIView alloc] init];
        [self.view addSubview:_coverView];
        [_coverView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _coverView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.addressTableView.hidden = NO;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(remove)];
        tap.delegate = self;
        [self.coverView addGestureRecognizer:tap];
    }
    return _coverView;
}


- (UITableView *)addressTableView {
    if (!_addressTableView) {
        _addressTableView = [[UITableView alloc] init];
        [self.coverView addSubview:_addressTableView];
        [_addressTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(17 * KEY_RATE);
            make.left.mas_equalTo(15 * KEY_RATE);
            make.right.mas_equalTo(-15 * KEY_RATE);
            make.height.mas_equalTo(540 * KEY_RATE);
        }];
        _addressTableView.separatorColor = [UIColor colorWithHexString:@"#EEEEEE"];
        _addressTableView.delegate = self;
        _addressTableView.dataSource = self;
    }
    return _addressTableView;
}

- (SectionHeader *)sectionHeader {
    if (!_sectionHeader) {
        _sectionHeader = [[SectionHeader alloc]
                          initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 45 * KEY_RATE)];
    }
    return _sectionHeader;
}

- (NSString *)address {
    if (!_address) {
        _address = [[NSString alloc] init];
    }
    return _address;
}

- (void)remove {
    [self.coverView removeFromSuperview];
    self.coverView = nil;
    self.addressTableView = nil;
    self.childrenIdArray = [_firstArray mutableCopy];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.addressTableView]) {
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
