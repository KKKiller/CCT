//
//  ChangeNameController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/20.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^callback)(NSString *str);

@interface ChangeNameController : BaseViewController

@property (nonatomic, copy) callback callback;

@end
