//
//  SystemNewsController.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "SystemNewsController.h"
#import "SystemNewsModel.h"
#import "SystemNewsCell.h"
#import "TSTextController.h"
@interface SystemNewsController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSInteger _page;
    BOOL _isFirst;
}

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) UIImageView *emptyView;
@property (nonatomic, strong) UILabel *emptyLabel;

@end

@implementation SystemNewsController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isFirst = YES;
    [self setSelf];
    [self uiConfig];
}

- (void)setSelf {
    
    [self addTitle:@"系统消息"];
    [self addReturnBtn:@"tui_"];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
    [_tableView.mj_header beginRefreshing];

}
- (void)prepareData {
    
    _page = 0;
    [self.dataSource removeAllObjects];
    
    NSString *url = @"main/sysmsg";
    NSDictionary *param;
    if (self.type) {
        param = @{@"id":USERID, @"page": [NSString stringWithFormat:@"%ld", _page],@"msg_type":@"1"};
    }else{
        param = @{@"id":USERID, @"page": [NSString stringWithFormat:@"%ld", _page]};
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(url) params:param target:self success:^(NSDictionary *success) {
        
        for (NSDictionary *dic in success[@"data"][@"sysmsg"]) {
            SystemNewsModel *model = [SystemNewsModel modelWithDictionary:dic];
            [self.dataSource addObject:model];
        }
        
        if (self.dataSource.count == 0) {
            [_tableView addSubview:self.emptyView];
            [_tableView addSubview:self.emptyLabel];
            [self.emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_tableView);
                make.width.mas_equalTo(90 * KEY_RATE);
                make.height.mas_equalTo(80 * KEY_RATE);
                make.top.equalTo(self.view.mas_top).with.offset(130);
            }];
            [self.emptyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.left.right.equalTo(_tableView);
                make.top.equalTo(self.emptyView.mas_bottom).with.offset(25);
                make.height.mas_equalTo(30 * KEY_RATE);
            }];
            _isFirst = NO;
        }else {
                [self.emptyLabel removeFromSuperview];
                self.emptyLabel = nil;
            [self.emptyView removeFromSuperview];
            self.emptyView = nil;
            _isFirst = YES;
        }
        [_tableView reloadData];
        [_tableView.mj_header endRefreshing];
    } failure:^(NSError *failure) {
        [_tableView.mj_header endRefreshing];
        _isFirst = YES;
    }];
}

- (void)loadMore {
    _page++;
    
    NSString *url = @"main/sysmsg";
    NSDictionary *param;
    if (self.type) {
        param = @{@"id":USERID, @"page": [NSString stringWithFormat:@"%ld", _page],@"msg_type":@"1"};
    }else{
        param = @{@"id":USERID, @"page": [NSString stringWithFormat:@"%ld", _page]};
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(url)
                                    params:param
                                    target:self
                                   success:^(NSDictionary *success) {
                
        for (NSDictionary *dic in success[@"data"][@"sysmsg"]) {
            SystemNewsModel *model = [SystemNewsModel modelWithDictionary:dic];
            [self.dataSource addObject:model];
        }
        [_tableView reloadData];
                                       [_tableView.mj_footer endRefreshing];

    } failure:^(NSError *failure) {
        _page--;
        [_tableView.mj_footer endRefreshing];
    }];

}



- (void)uiConfig
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, KEY_WIDTH, KEY_HEIGHT-64)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    _tableView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.view addSubview:_tableView];
    
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(prepareData)];
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cellID";
    SystemNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[SystemNewsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setModel:self.dataSource[indexPath.row]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 112*KEY_RATE;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SystemNewsModel *model = self.dataSource[indexPath.row];
    TSTextController *vc = [[TSTextController alloc]init];
    vc.navTitle = @"消息详情";
    vc.text = model.msg;
    [self.navigationController pushViewController:vc animated:YES];
    [self readMessage:model];
}


- (void)readMessage:(SystemNewsModel *)model {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"main/readmsg")
                                    params:@{@"r_id":USERID,@"msg_id":model.id}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       
                                   } failure:^(NSError *failure) {
                                   }];
}

- (NSMutableArray *)dataSource {
    
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isFirst) {
        [self.emptyView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top).with.offset(130-scrollView.contentOffset.y);
        }];
    }
}

- (UIImageView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[UIImageView alloc] init];
        _emptyView.image = [UIImage imageNamed:@"img_zwhd"];
    }
    return _emptyView;
}

- (UILabel *)emptyLabel {
    if (!_emptyLabel) {
        _emptyLabel = [[UILabel alloc] init];
        _emptyLabel.text = @"暂无系统消息";
        _emptyLabel.font = FontSize(18);
        _emptyLabel.textColor = [UIColor colorWithHexString:@"#666666"];
        _emptyLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _emptyLabel;
}

@end
