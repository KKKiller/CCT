//
//  IntegralMallController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 2016/10/14.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "IntegralMallController.h"
#import <WebKit/WebKit.h>

@interface IntegralMallController () {
    WKWebView *_webView;
    UIProgressView *_progressView;
}

@end

@implementation IntegralMallController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"积分商城"];
    self.automaticallyAdjustsScrollViewInsets = NO;
    CGRect frame = [UIScreen mainScreen].bounds;
    frame.origin.y += 64;
    frame.size.height -= 64;
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, KEY_WIDTH, 0)];
    _progressView.progressTintColor = [UIColor greenColor];
    _progressView.transform = CGAffineTransformMakeScale(1.0f,2.0f);
    [self.view addSubview:_progressView];
    
    _webView = [[WKWebView alloc] initWithFrame:frame];
    
    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/point?id=%@", USERID];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    
    [self.view insertSubview:_webView belowSubview:_progressView];
    
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
}

#pragma mark - Target-Action

- (void)back {
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:0];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        _progressView.hidden = _webView.estimatedProgress == 1.0;
        [_progressView setProgress:(float)_webView.estimatedProgress animated:YES];
    }
}

- (void)dealloc {
    
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

- (void)backMove {
    if ([_webView canGoBack]) {
        [_webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
