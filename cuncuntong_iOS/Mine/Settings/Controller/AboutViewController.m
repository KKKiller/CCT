//
//  AboutViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/30.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =WHITECOLOR;
    // Do any additional setup after loading the view.
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"关于"];
    [self setUI];
}

- (void)setUI {
    UIImageView *backImgv = [[UIImageView alloc] init];
    backImgv.image = [UIImage imageNamed:@"bg_qqcct"];
    [self.view addSubview:backImgv];
    [backImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.equalTo(@(214 * KEY_RATE));
    }];
    
    UIImageView *iconImgv = [[UIImageView alloc] init];
    iconImgv.image = [UIImage imageNamed:@"logo"];
    [self.view addSubview:iconImgv];
    [iconImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(@(70 * KEY_RATE + 64));
        make.size.equalTo(@(80 * KEY_RATE));
    }];
    
    UILabel *descLabel = [[UILabel alloc] init];
    descLabel.text = @"全球村村通，让村庄连接全世界";
    descLabel.textColor = RGB(51, 51, 51);
    [self.view addSubview:descLabel];
    descLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    descLabel.textAlignment = NSTextAlignmentCenter;
    [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(60 * KEY_RATE));
        make.top.mas_equalTo(iconImgv.mas_bottom);
    }];
    
    UILabel *copyRightLabel = [[UILabel alloc] init];
    [self.view addSubview:copyRightLabel];
    copyRightLabel.text = @"copyright @ 2016 All Rright reserved";
    copyRightLabel.textColor = RGB(136, 136, 136);
    copyRightLabel.font = [UIFont systemFontOfSize:12 * KEY_RATE];
    copyRightLabel.textAlignment = NSTextAlignmentCenter;
    [copyRightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).with.offset(-135 * KEY_RATE);
        make.height.equalTo(@(20 * KEY_RATE));
    }];
    
    UILabel *targetLabel = [[UILabel alloc] init];
    [self.view addSubview:targetLabel];
    targetLabel.textAlignment = NSTextAlignmentCenter;
    targetLabel.text = @"全 球 村 村 通";
    targetLabel.textColor = RGB(136, 136, 136);
    targetLabel.font = [UIFont systemFontOfSize:12 * KEY_RATE];
    [targetLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(copyRightLabel.mas_top).with.offset(-15 * KEY_RATE);
        make.height.equalTo(@(20 * KEY_RATE));
    }];
    
    
    UILabel *versionLabel = [[UILabel alloc] init];
    [self.view addSubview:versionLabel];
    versionLabel.textAlignment = NSTextAlignmentCenter;
//    versionLabel.text = @"版本 V 1.0";
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    versionLabel.text = [NSString stringWithFormat:@"版本 %@", app_Version];
    versionLabel.textColor = RGB(68, 68, 68);
    versionLabel.font = [UIFont systemFontOfSize:18 * KEY_RATE];
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(targetLabel.mas_top);
        make.height.equalTo(@(24 * KEY_RATE));
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
