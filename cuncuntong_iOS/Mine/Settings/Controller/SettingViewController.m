//
//  SettingViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/30.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "SettingViewController.h"
#import "AboutViewController.h"
#import "FeedBackController.h"
#import "ResetViewController.h"
#import "LoginViewController.h"
#import "JPUSHService.h"
@interface SettingViewController () <UITableViewDelegate, UITableViewDataSource> {
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setSelf];
    [self setData];
    [self uiConfig];
    self.view.backgroundColor = [UIColor redColor];
    
}

- (void)setSelf {
    
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"设置"];
}

- (void)setData {
    NSArray *array;
    
    if ([[UserManager sharedInstance].isthird isEqualToString:@"1"]) {
        
//        array = @[@"关于全球村村通",@"检查新版本",@"意见反馈"];
        array = @[@"关于全球村村通",@"意见反馈"];
    }else {
//        array = @[@[@"修改密码"],@[@"关于全球村村通",@"检查新版本",@"意见反馈"]];
        array = @[@[@"修改密码"],@[@"关于全球村村通",@"意见反馈"]];
    }
    _dataArray = [[NSMutableArray alloc] initWithArray:array];
    
}

- (void)uiConfig {
    CGRect frame = [UIScreen mainScreen].bounds;
    frame.origin.y += 64;
    frame.size.height -= 64;
    _tableView = [[UITableView alloc] initWithFrame:frame
                                              style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorColor = [UIColor colorWithHexString:@"EEEEEE"];
    [self.view addSubview:_tableView];
}

#pragma mark - UITabelViewDelegate && UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([[UserManager sharedInstance].isthird isEqualToString:@"1"]) {
        
        return 1;
    }
    return _dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[UserManager sharedInstance].isthird isEqualToString:@"1"]) {
        return _dataArray.count;
    }
    return [_dataArray[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId = @"cellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    if ([[UserManager sharedInstance].isthird isEqualToString:@"1"]) {
        cell.textLabel.text = _dataArray[indexPath.row];
    }else {
        cell.textLabel.text = _dataArray[indexPath.section][indexPath.row];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = FontSize(15);
    return cell;

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if ([[UserManager sharedInstance].isthird isEqualToString:@"0"] && section == 0) {
        return nil;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 60 * KEY_RATE)];
    
    UIButton *quitButton = [[UIButton alloc] init];
    [view addSubview:quitButton];
    quitButton.backgroundColor = [UIColor whiteColor];
    [quitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(view);
        make.height.equalTo(@(50 * KEY_RATE));
    }];
    [quitButton setTitle:@"退出登录" forState:UIControlStateNormal];
    [quitButton setTitleColor:RGB(56, 126, 255) forState:UIControlStateNormal];
    [quitButton addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    quitButton.layer.borderWidth = 0.5;
    quitButton.titleLabel.font = FontSize(16);
    quitButton.layer.borderColor = [UIColor colorWithHexString:@"EEEEEE"].CGColor;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 50 * KEY_RATE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if ([[UserManager sharedInstance].isthird isEqualToString:@"1"]) {
        return 60 * KEY_RATE;
    }
    if (section == 0) return 10 * KEY_RATE;
    return 60 * KEY_RATE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([[UserManager sharedInstance].isthird isEqualToString:@"1"]) {
        if (indexPath.row == 0) {
            AboutViewController *vc = [[AboutViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            return;
        }else {
            FeedBackController *vc = [[FeedBackController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            return;
        }
    }
    if (indexPath.section == 0) {
        ResetViewController *vc = [[ResetViewController alloc] init];
        vc.navTitle = @"修改密码";
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        if (indexPath.row == 0) {
            AboutViewController *vc = [[AboutViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            return;
        }else {
            FeedBackController *vc = [[FeedBackController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            return;
        }
    }
}

#pragma mark - Target-Action
- (void)logout {
    
//    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"secret"];
    [Def removeObjectForKey:@"secret"];
    [Def removeObjectForKey:@"userid"];
    [Def removeObjectForKey:@"managerPasswds"];
    [Def removeObjectForKey:@"w_group"];
    [[RCIM sharedRCIM] logout];
    LoginViewController *vc = [[LoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [UIApplication sharedApplication].keyWindow.rootViewController = nav;
    [JPUSHService setTags:[NSSet set] callbackSelector:nil object:nil];
    App_Delegate.registedJpushTag = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
