//
//  FeedBackController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/30.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "FeedBackController.h"
#import "CCTTextView.h"
#import "CCTButton.h"
#import "MoneyTextField.h"

@interface FeedBackController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    UIButton *_addImageBtn;
    NSString *_path;
    CCTTextView *_textView;
    MoneyTextField *_mobileTF;
}

@end

@implementation FeedBackController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"意见反馈"];
    [self setUI];
    self.view.backgroundColor = RGB(232, 232, 232);
}

- (void)setUI {
     _textView = [[CCTTextView alloc] init];
    
    [self.view addSubview:_textView];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(16 * KEY_RATE));
        make.right.equalTo(@(-16 * KEY_RATE));
        make.top.equalTo(@(80 * KEY_RATE));
        make.height.equalTo(@(190 * KEY_RATE));
    }];
    _textView.layer.cornerRadius = 5 * KEY_RATE;
//    _textView.myPlaceholder = @"感谢反馈，请填写您的问题或意见...";
    
    _addImageBtn = [[UIButton alloc] init];
    [self.view addSubview:_addImageBtn];
    [_addImageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textView.mas_left).with.offset(16 * KEY_RATE);
        make.bottom.equalTo(_textView.mas_bottom).with.offset(-18 * KEY_RATE);
        make.size.mas_equalTo(50 * KEY_RATE);
    }];
    [_addImageBtn setImage:[UIImage imageNamed:@"btn_yjfk"] forState:UIControlStateNormal];
    
    [_addImageBtn addTarget:self action:@selector(addImage) forControlEvents:UIControlEventTouchUpInside];
    
    _mobileTF = [[MoneyTextField alloc] init];
    [self.view addSubview:_mobileTF];
    [_mobileTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_textView.mas_bottom).with.offset(10 * KEY_RATE);
        make.left.right.equalTo(_textView);
        make.height.equalTo(@(46 * KEY_RATE));
    }];
    _mobileTF.backgroundColor = [UIColor whiteColor];
    UIView *leftView = [[UIView alloc] init];
    _mobileTF.placeholder = @"填写您的手机号，方便我们更快向您反馈哦！";
    _mobileTF.leftView = leftView;
    _mobileTF.font = [UIFont systemFontOfSize:15 * KEY_RATE];
    
    CCTButton *submitBtn = [[CCTButton alloc] init];
    [self.view addSubview:submitBtn];
    [submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_mobileTF.mas_bottom).with.offset(35 * KEY_RATE);
        make.left.right.equalTo(_mobileTF);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    [submitBtn setTitle:@"提交反馈" forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitFeedback) forControlEvents:UIControlEventTouchUpInside];
}

- (void)addImage {
    [self showActionSheet];
}

- (void)showActionSheet {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:0];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"从相册选取" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }];
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"拍照" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action)
                                   {}];
    [self presentViewController:alertController animated:YES completion:nil];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [alertController addAction:photoAction];
    }
    else {
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        NSData *data = [[NSData alloc] initWithData:UIImageJPEGRepresentation([self compressImage:image], 1)];
        
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"common/upload") params:nil target:nil imgaeData:data success:^(NSDictionary *success) {
            _path = success[@"data"][@"path"];
            [_addImageBtn setImage:[UIImage imageWithData:UIImageJPEGRepresentation([self compressImage:image], 1)] forState:UIControlStateNormal];
        } failure:^(NSError *failure) {
        }];

        
    }];
    
    
}

- (UIImage *)compressImage:(UIImage *)image
{
    CGSize size = {120 * KEY_RATE, 120 * KEY_RATE};
    UIGraphicsBeginImageContext(size);
    CGRect rect = {{0,0}, size};
    [image drawInRect:rect];
    UIImage *compressedImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return compressedImg;
}


#pragma mark - Target Action
- (void)submitFeedback {
    
    if (_textView.text.length == 0) {
        [MBProgressHUD showMessage:@"必需输入反馈内容"];
        return;
    }
    if (_path.length == 0) {
        _path = @"";
    }
    if (_mobileTF.text.length == 0) {
        _mobileTF.text = @"";
    }
    
    NSDictionary *paramDic = @{@"id": USERID,
                               @"content": _textView.text,
                               @"picture": _path,
                               @"mobile": _mobileTF.text};
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/feedback") params:paramDic target:self success:^(NSDictionary *success) {
        [MBProgressHUD showMessage:@"反馈成功，我们会及时处理"];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *failure) {
        
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
