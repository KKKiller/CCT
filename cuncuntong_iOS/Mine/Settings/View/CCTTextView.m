//
//  CCTTextView.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/30.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "CCTTextView.h"

@interface CCTTextView()

@property (nonatomic, weak) UILabel *placeholderLabel;

@end

@implementation CCTTextView

- (instancetype)init {
    
    if (self = [super init]) {
        
//        self.backgroundColor             = [UIColor clearColor];
        UILabel *placeholderLabel        = [[UILabel alloc] init];
        placeholderLabel.backgroundColor = [UIColor clearColor];
        placeholderLabel.numberOfLines   = 0;
        [self addSubview:placeholderLabel];
        self.placeholderLabel            = placeholderLabel;
        self.myPlaceholderColor          = [UIColor lightGrayColor];
        self.font                        = [UIFont systemFontOfSize:15 * KEY_RATE];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:self];
    }
    return self;
}

- (void)textDidChange {
    
    self.placeholderLabel.hidden = self.hasText;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame;
    frame.origin.x = 5 * KEY_RATE;
    frame.origin.y = 8 * KEY_RATE;
    frame.size.width = self.frame.size.width - frame.origin.x * 2.0;
    
    CGSize maxSize = CGSizeMake(self.placeholderLabel.width, MAXFLOAT);
    
    frame.size.height = [self.myPlaceholder boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.placeholderLabel.font} context:nil].size.height;
    
    self.placeholderLabel.frame = frame;
}

- (void)setMyPlaceholder:(NSString *)myPlaceholder {
    _myPlaceholder = [myPlaceholder copy];
    self.placeholderLabel.text = myPlaceholder;
    [self setNeedsLayout];
}

- (void)setMyPlaceholderColor:(UIColor *)myPlaceholderColor {
    _myPlaceholderColor = myPlaceholderColor;
    self.placeholderLabel.textColor = myPlaceholderColor;
}

- (void)setFont:(UIFont *)font {
    [super setFont:font];
    self.placeholderLabel.font = font;
    [self setNeedsLayout];
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self textDidChange];
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    [super setAttributedText:attributedText];
    [self textDidChange];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:UITextViewTextDidChangeNotification];
}

@end
