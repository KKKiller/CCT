//
//  CCTTextView.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/30.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCTTextView : UITextView

@property (nonatomic, copy) NSString *myPlaceholder;

@property (nonatomic, copy) UIColor *myPlaceholderColor;

@end
