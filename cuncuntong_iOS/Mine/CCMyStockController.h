//
//  CCMyStockController.h
//  CunCunTong
//
//  Created by 我是MT on 2017/5/29.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCMyStockController : BaseViewController
@property (nonatomic, assign) BOOL isProfileStock;
@end


/*
 - (void)setUI {
 self.view.backgroundColor = BACKGRAY;
 
 UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, App_Width, 40*3)];
 topView.backgroundColor = WHITECOLOR;
 [self.view addSubview:topView];
 
 self.firstStockLbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, App_Width, 40)];
 [topView addSubview:self.firstStockLbl];
 self.firstStockLbl.text = @"金种子股份: * 份";
 self.firstStockLbl.textColor = [UIColor orangeColor];
 self.firstStockLbl.font = FFont(16);
 
 self.secondStockLbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 40, App_Width, 40)];
 [topView addSubview:self.secondStockLbl];
 self.secondStockLbl.text = @"酒厂股份: * 份";
 self.secondStockLbl.textColor = [UIColor orangeColor];
 self.secondStockLbl.font = FFont(16);
 
 self.thirdStockLbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 40*2, App_Width, 40)];
 [topView addSubview:self.thirdStockLbl];
 self.thirdStockLbl.text = @"会员股份: * 份";
 self.thirdStockLbl.textColor = [UIColor orangeColor];
 self.thirdStockLbl.font = FFont(16);
 
 UILabel *lineLbl1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, App_Width, 0.5)];
 [topView addSubview:lineLbl1];
 lineLbl1.backgroundColor = BACKGRAY;
 
 UILabel *lineLbl2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 40*2, App_Width, 0.5)];
 [topView addSubview:lineLbl2];
 lineLbl2.backgroundColor = BACKGRAY;
 
 
 UIButton *exchangeBtn = [[UIButton alloc]initWithTitle:@"积分兑换" textColor:WHITECOLOR backImg:nil font:14];
 [self.view addSubview:exchangeBtn];
 exchangeBtn.layer.cornerRadius = 3;
 exchangeBtn.layer.masksToBounds = YES;
 exchangeBtn.backgroundColor = MAINBLUE;
 [exchangeBtn addTarget:self action:@selector(exchangeBtnClick) forControlEvents:UIControlEventTouchUpInside];
 exchangeBtn.frame = CGRectMake(App_Width - 15 - 70, 64 + 87.5, 70, 25);
 
 
 UILabel *textLbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 50+40*4, App_Width-30, 220)];
 textLbl.numberOfLines = 0;
 textLbl.font = FFont(12);
 textLbl.textAlignment = NSTextAlignmentLeft;
 textLbl.textColor = TEXTBLACK6;
 [self.view addSubview:textLbl];
 textLbl.text = @"1. 全球村村通旨在打造为人民大众创造财富的企业集群，所有注册全球村村通的会员，通过文章阅读积累积分达到3000的，均会配赠1股，配赠股份占全部股份的10%。\n\n2. 全球村村通在完成C轮融资后，还将为低保弱势群体每人配发股份，配赠股份占全部股份的10%。\n\n3. 全球村村通每年年终结算后，按配股发利润。";
 }
*/
