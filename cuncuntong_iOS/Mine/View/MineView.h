//
//  MineView.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MineDelegate <NSObject>

- (void)pushToDetail:(UIButton *)sender;

@end

@interface MineView : UIView
- (instancetype)initWithFrame:(CGRect)frame :(NSArray *)numArr;
- (void)resetTheNumber:(NSArray *)array;
@property (nonatomic, weak) id<MineDelegate> delegate;


@end
