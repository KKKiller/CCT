//
//  AddTitleView.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/8.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "AddTitleView.h"

@interface AddTitleView()



@end

@implementation AddTitleView

- (instancetype)init:(NSInteger)count {
    
    if (self = [super init]) {
        self.backgroundColor = [UIColor colorWithRed:0.22f green:0.49f blue:1.00f alpha:1.00f];
        [self titleLabelConfig:count];
        [self sureBtnConfig];
    }
    return self;
}

- (void)titleLabelConfig:(NSInteger)count {
    self.titleLabel.text = [NSString stringWithFormat:@"感兴趣的村（%ld/6）",count];

}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.mas_equalTo(35);
            make.height.mas_equalTo(17);
            _titleLabel.font = [UIFont boldSystemFontOfSize:17];
            _titleLabel.textAlignment = NSTextAlignmentCenter;
            _titleLabel.textColor = [UIColor whiteColor];
        }];

    }
    return _titleLabel;
}

- (void)sureBtnConfig {
    UIButton *sureBtn = [[UIButton alloc] init];
    [self addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.height.mas_equalTo(20 * KEY_RATE);
        make.width.mas_equalTo(50 * KEY_RATE);
        make.right.equalTo(self).with.offset(-15);
    }];
    sureBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:15 * KEY_RATE];
    [sureBtn setTitle:@"确认" forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
}

- (void)reloadTitleLabel:(NSString *)home {
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15 * KEY_RATE);
    }];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.text = home;
}

- (void)sure {
    if ([_delegate respondsToSelector:@selector(addVillage)]) {
        [_delegate addVillage];
    }
}

@end
