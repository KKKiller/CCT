//
//  SystemNewsCell.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SystemNewsModel.h"
@interface SystemNewsCell : UITableViewCell
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *content;
@property(nonatomic,strong)UILabel *time;
@property(nonatomic,strong)SystemNewsModel *model;
@property (nonatomic, strong) UIView *unreadView;

@end
