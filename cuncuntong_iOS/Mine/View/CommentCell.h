//
//  CommentCell.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/14.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"

@interface CommentCell : UITableViewCell

@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *content;
@property(nonatomic,strong)UILabel *time;
@property(nonatomic,strong)CommentModel *model;

@end
