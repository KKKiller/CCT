//
//  CCMyStockCharView.h
//  TestDemo
//
//  Created by 周吾昆 on 2017/9/8.
//  Copyright © 2017年 周吾昆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PNPieChart.h>
#import "CCMyStockModel.h"
@interface CCMyStockCharView : UIView
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic) PNPieChart *pieChart;
@property (nonatomic, strong) NSMutableArray<UILabel*> *stockLblArray;
@property (nonatomic, strong) NSMutableArray<UILabel*> *stockNumLblArray;
@property (nonatomic, strong) UILabel *stockLbl1;
@property (nonatomic, strong) UILabel *stockLbl2;
@property (nonatomic, strong) UILabel *stockLbl3;
@property (nonatomic, strong) UILabel *stockLbl4;
@property (nonatomic, strong) UILabel *stockLbl5;

@end
