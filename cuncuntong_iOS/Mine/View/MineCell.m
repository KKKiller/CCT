//
//  MineCell.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MineCell.h"

@implementation MineCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(15*KEY_RATE, 14*KEY_RATE, 22*KEY_RATE, 22*KEY_RATE)];
        self.title = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+15*KEY_RATE, 17*KEY_RATE, 180*KEY_RATE, 16*KEY_RATE)];
        self.title.font  = [UIFont systemFontOfSize:16*KEY_RATE];
        self.title.textColor = RGB(51, 51, 51);
        [self addSubview:self.icon];
        [self addSubview:self.title];
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 49*KEY_RATE, KEY_WIDTH, 1*KEY_RATE)];
        line.backgroundColor = [UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.00f];
        [self addSubview:line];
        
    }
    return self;
}
-(void)setIcon:(NSString *)image title:(NSString *)title
{
    self.title.text = title;
    self.icon.image = [UIImage imageNamed:image];
}
@end
