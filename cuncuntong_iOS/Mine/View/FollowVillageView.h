//
//  FollowVillageView.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/9.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FollowDeleteDelegate <NSObject>

- (void)deleteFollow:(NSInteger )x;

@end


@interface FollowVillageView : UIView

- (instancetype)initWithArray:(NSMutableArray *)array;

- (void)reloadSelf:(NSMutableArray *)array;

@property (nonatomic, weak) id<FollowDeleteDelegate> delegate;

@end
