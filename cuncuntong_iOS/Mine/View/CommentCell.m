//
//  CommentCell.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/14.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
        
        UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(10*KEY_RATE, 12*KEY_RATE, KEY_WIDTH-20*KEY_RATE, 100*KEY_RATE)];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.cornerRadius = 3;
        bgView.clipsToBounds = YES;
        [self addSubview:bgView];
        
        self.title = [[UILabel alloc]initWithFrame:CGRectMake(18*KEY_RATE, 18*KEY_RATE, 200*KEY_RATE, 16*KEY_RATE)];
        self.title.textColor = [UIColor  colorWithHexString:@"#333333"];
        self.title.font = [UIFont systemFontOfSize:16*KEY_RATE];
        [bgView addSubview:self.title];
        
        self.time = [[UILabel alloc]initWithFrame:CGRectMake(110*KEY_RATE, 20*KEY_RATE, KEY_WIDTH-142*KEY_RATE, 16*KEY_RATE)];
        self.time.textAlignment = NSTextAlignmentRight;
        self.time.textColor = RGB(136, 136, 136);
        self.time.centerY = self.title.centerY;
        self.time.font = FontSize(12);
        [bgView addSubview:self.time];
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(8*KEY_RATE, 49*KEY_RATE, 340*KEY_RATE, 1*KEY_RATE)];
        line.backgroundColor = [UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.00f];
        [bgView addSubview:line];
        
        self.content = [[UILabel alloc]initWithFrame:CGRectMake(18*KEY_RATE, 64*KEY_RATE, KEY_WIDTH-36*KEY_RATE, 14*KEY_RATE)];
        self.content.font = [UIFont systemFontOfSize:14*KEY_RATE];
        self.content.textColor = [UIColor colorWithHexString:@"#666666"];
        [bgView addSubview:self.content];
        
        [bgView.layer setShadowOpacity:0.5];
    }
    return self;
}
- (void)setModel:(CommentModel *)model
{
    self.title.text = model.title;
    self.time.text = model.create;
    self.content.text = model.content;
    
}
@end
