//
//  MineDetailCell.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MineDetailCell.h"

@implementation MineDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _backView = [[UIView alloc] init];
        [self addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(15);
            make.top.equalTo(self).with.offset(10);
            make.right.equalTo(self).with.offset(-15);
            make.bottom.equalTo(self);
        }];
        
        _contentLabel = [[UILabel alloc] init];
        [self addSubview:_contentLabel];

        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(_backView).with.offset(10*KEY_RATE);
            make.right.equalTo(_backView).with.offset(-10*KEY_RATE);
            make.bottom.equalTo(_backView).with.offset(-20 * KEY_RATE);
        }];
        
        _contentLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
        _contentLabel.numberOfLines = 0;
        
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.textAlignment = NSTextAlignmentRight;
        _dateLabel.font = [UIFont systemFontOfSize:12 * KEY_RATE];
        [self addSubview:_dateLabel];
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_backView).with.offset(-10);
            make.top.equalTo(_contentLabel.mas_bottom);
            make.width.mas_equalTo(KEY_WIDTH - 2 * 10 * KEY_RATE);
            make.height.mas_equalTo(12 * KEY_RATE);
        }];
        self.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
        _dateLabel.textColor = [UIColor colorWithHexString:@"#888888"];

        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 5.0f;

        
    }
    return self;
}

- (void)setModel:(MineDetailModel *)model {
    _model = model;
    _contentLabel.attributedText = [self getAttributedStringWithString:_model.title lineSpace:5];
    _dateLabel.text = _model.datetime;
}

-(NSAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpace; // 调整行间距
    NSRange range = NSMakeRange(0, [string length]);
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributedString;
}

@end
