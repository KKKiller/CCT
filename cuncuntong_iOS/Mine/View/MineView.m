//
//  MineView.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "MineView.h"

@implementation MineView

- (instancetype)initWithFrame:(CGRect)frame :(NSArray *)numArr
{

    if (self = [super initWithFrame:frame]) {
        [self uiConfig:frame:numArr];
    }
    return self;
}
- (void)uiConfig:(CGRect)frame :(NSArray *)numArr
{
    CGFloat width = frame.size.width;
    CGFloat height = frame.size.height;
    NSArray *titleArr = @[@"阅读",@"收藏",@"评论",@"积分"];
    for (int i = 0; i < 4; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i*(width/4), 0, width/4, height)];
        btn.titleLabel.numberOfLines = 2;
        [btn setTitle:[NSString stringWithFormat:@"%@\n%@",numArr[i],titleArr[i]] forState:UIControlStateNormal];
        btn.tag = 1000 + i;
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn.titleLabel.font = [UIFont systemFontOfSize:14 * KEY_RATE];
        [btn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        [self addSubview:btn];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake((i*(width/4))-1, 0, 1, height)];
        line.backgroundColor = CLEARCOLOR;
        [self addSubview:line];
    }
}

- (void)resetTheNumber:(NSArray *)array {
    NSArray *titleArr = @[@"阅读",@"收藏",@"评论",@"积分"];
    for (int i = 0; i < 4; i++) {
        UIButton *btn = [self viewWithTag:1000 + i];
        [btn setTitle:[NSString stringWithFormat:@"%@\n%@",array[i],titleArr[i]] forState:UIControlStateNormal];
    }
}

- (void)btnClick:(UIButton *)sender {
    if (sender.tag == 1003) {
        return;
    }
    if ([_delegate respondsToSelector:@selector(pushToDetail:)]) {
        [_delegate pushToDetail:sender];
    }
}
@end
