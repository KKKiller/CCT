//
//  SectionHeader.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/9.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeader : UIView

- (instancetype)initWithFrame:(CGRect)frame;

- (void)titleLabelConfig:(NSString *)title;

@end
