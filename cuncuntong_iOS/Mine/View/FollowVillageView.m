//
//  FollowVillageView.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/9.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "FollowVillageView.h"
#import "FollowVillageModel.h"

@implementation FollowVillageView

- (instancetype)initWithArray:(NSMutableArray *)array {
    
    if (self = [super init]) {
        CGFloat height =array.count > 3 ? 108 * KEY_RATE: 59 * KEY_RATE;
        self.frame = CGRectMake(0, 0, KEY_WIDTH, height);
        [self setData:array];
    }
    return self;
}

- (void)setData:(NSMutableArray *)array {
    
    [self removeAllSubviews];
    
    for (int i = 0; i < array.count; i++) {
        
        for (int i = 0; i< array.count; i++) {
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(20*KEY_RATE+120*KEY_RATE*(i%3), 14*KEY_RATE+ (40 * KEY_RATE + 10)*(i/3), 100*KEY_RATE, 40*KEY_RATE)];
            btn.backgroundColor = [UIColor blueColor];
            FollowVillageModel *model = array[i];
            [btn setTitle:[model val] forState:UIControlStateNormal];
            btn.layer.cornerRadius = 5 * KEY_RATE;
            btn.titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
            btn.backgroundColor = RGB(56, 126, 255);
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = 9999+i;
            [self addSubview:btn];
            
            
        }
        
        
    }
}

- (void)reloadSelf:(NSMutableArray *)array {
    [self setData:array];
}

- (void)btnClick:(UIButton *)sender {
    
    NSInteger x = sender.tag - 9999;
    
    if ([_delegate respondsToSelector:@selector(deleteFollow:)]) {
        
        [_delegate deleteFollow:x];
    }
}



@end
