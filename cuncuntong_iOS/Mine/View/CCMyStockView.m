//
//  CCMyStockView.m
//  TestDemo
//
//  Created by 周吾昆 on 2017/9/8.
//  Copyright © 2017年 周吾昆. All rights reserved.
//

#import "CCMyStockView.h"
#import <Masonry.h>
@implementation CCMyStockView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
    }
    return self;
}
- (void)setModel:(CCMyStockModel *)model {
    _model = model;
    self.titleLbl.text = model.name;
    self.countLbl.text = [NSString stringWithFormat:@"%.1f份",[model.count floatValue]];
    self.exchangeBtn.hidden = ![model.name isEqualToString:@"会员股份"];
    if ([model.count integerValue] > 0) {
        self.bigCricle.backgroundColor = model.color;
    }
}

- (void)setUI {
    self.backgroundColor = MTRGB(0xecf1f5);
    self.layer.borderColor = MTRGB(0xadadad).CGColor;
    self.layer.borderWidth = 0.5;
    
    self.titleLbl = [[UILabel alloc]init];
    [self addSubview:self.titleLbl];
    self.titleLbl.font = FFont(14);
    self.titleLbl.textAlignment = NSTextAlignmentCenter;
    self.titleLbl.layer.borderColor = MTRGB(0xadadad).CGColor;
    self.titleLbl.layer.borderWidth = 0.5;
    self.titleLbl.backgroundColor = [UIColor whiteColor];
    self.titleLbl.text = @"酒厂股份";
    
    self.bigCricle = [[UIView alloc]init];
    [self addSubview:self.bigCricle];
    self.bigCricle.backgroundColor = MTRGB(0xDBE6EC);
    self.bigCricle.layer.cornerRadius = 35;
    self.bigCricle.layer.masksToBounds = YES;
    
    self.smallCircle = [[UIView alloc]init];
    [self addSubview:self.smallCircle];
    self.smallCircle.backgroundColor = MTRGB(0xecf1f5);
    self.smallCircle.layer.cornerRadius = 30;
    self.smallCircle.layer.masksToBounds = YES;
    
    self.countLbl = [[UILabel alloc]init];
    [self addSubview:self.countLbl];
    self.countLbl.textColor = TEXTBLACK9;
    self.countLbl.textAlignment = NSTextAlignmentCenter;
    self.countLbl.text = @"20份";
    
    self.exchangeBtn = [[UIButton alloc]init];
    [self addSubview:self.exchangeBtn];
    self.exchangeBtn.backgroundColor = MTRGB(0xdd4443);
    [self.exchangeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.exchangeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.exchangeBtn setTitle:@"积分兑换" forState:UIControlStateNormal];
}
- (void)layoutSubviews {
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(30);
    }];
    [self.bigCricle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(70);
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-15);
    }];
    [self.smallCircle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(60);
        make.center.equalTo(self.bigCricle);
    }];
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.smallCircle.mas_centerX);
        make.centerY.equalTo(self.smallCircle.mas_centerY);
    }];
    [self.exchangeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.titleLbl.mas_top);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(60);
    }];
}

@end
