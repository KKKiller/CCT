//
//  NewsCell.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"
@interface NewsCell : UITableViewCell
@property(nonatomic,strong)NewsModel *model;
@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *cycle;
@end
