//
//  MineDetailCell.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineDetailModel.h"

@interface MineDetailCell : UITableViewCell

@property (nonatomic, strong) MineDetailModel *model;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIView *backView;

@end
