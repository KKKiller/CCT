//
//  VillageCell.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "VillageCell.h"

@implementation VillageCell
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:self.titleLabel];
    }
    return self;

}
@end
