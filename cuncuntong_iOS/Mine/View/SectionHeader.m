//
//  SectionHeader.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/9.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "SectionHeader.h"

@interface SectionHeader()

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation SectionHeader

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIImageView *icon = [[UIImageView alloc] init];
        [self addSubview:icon];
        [icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18 * KEY_RATE);
            make.top.mas_equalTo(15 * KEY_RATE);
            make.width.mas_equalTo(15 * KEY_RATE);
            make.height.mas_equalTo(19 * KEY_RATE);
        }];
        icon.image = [UIImage imageNamed:@"btn_weizhi"];
        UIView *sepView = [[UIView alloc] init];
        [self addSubview:sepView];
        [sepView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
        sepView.backgroundColor = RGB(238, 238, 238);
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(icon);
            make.left.equalTo(icon.mas_right).with.offset(10 * KEY_RATE);
            make.height.mas_equalTo(16 * KEY_RATE);
            make.width.mas_equalTo(100 * KEY_RATE);
        }];
    }
    return self;
}

- (void)titleLabelConfig:(NSString *)title {
    self.titleLabel.text = title;

}


- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [self addSubview:_titleLabel];
        _titleLabel.font = [UIFont systemFontOfSize:16 * KEY_RATE];
    }
    return _titleLabel;
}



@end
