//
//  CCMyStockView.h
//  TestDemo
//
//  Created by 周吾昆 on 2017/9/8.
//  Copyright © 2017年 周吾昆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMyStockModel.h"
@interface CCMyStockView : UIView
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIView *bigCricle;
@property (nonatomic, strong) UIView *smallCircle;
@property (nonatomic, strong) UILabel *countLbl;
@property (nonatomic, strong) UIButton *exchangeBtn;
@property (nonatomic, strong) CCMyStockModel *model;
@end
