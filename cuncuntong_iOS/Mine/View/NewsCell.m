//
//  NewsCell.m
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/8/26.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "NewsCell.h"

@implementation NewsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(15*KEY_RATE, 18*KEY_RATE, 45*KEY_RATE, 45*KEY_RATE)];
        self.title = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+15*KEY_RATE, 25*KEY_RATE, 100*KEY_RATE, 16*KEY_RATE)];
        self.title.font = [UIFont systemFontOfSize:16*KEY_RATE];
        self.title.textColor = RGB(34, 34, 34);
        self.title.centerY = self.icon.centerY;
        
        
        self.cycle = [[UILabel alloc]initWithFrame:CGRectMake(142*KEY_RATE, 28*KEY_RATE, 10*KEY_RATE, 10*KEY_RATE)];
        self.cycle.layer.cornerRadius = 5*KEY_RATE;
        self.cycle.clipsToBounds = YES;
        self.cycle.centerY = self.title.centerY;
        self.cycle.backgroundColor = [UIColor colorWithRed:1.00f green:0.18f blue:0.31f alpha:1.00f];
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 79*KEY_RATE, KEY_WIDTH, 1*KEY_RATE)];
        line.backgroundColor = [UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.00f];
        
        [self addSubview:line];
        [self addSubview:self.cycle];
        [self addSubview:self.icon];
        [self addSubview:self.title];
    }
    return self;
}
- (void)setModel:(NewsModel *)model
{
    self.title.text = model.title;
    self.icon.image = [UIImage imageNamed:model.icon];
//    self.content.text = model.content;
    if ([model.hasNews integerValue] == 0) {
        self.cycle.hidden = YES;
    }else {
        self.cycle.hidden = NO;
    }
}
@end
