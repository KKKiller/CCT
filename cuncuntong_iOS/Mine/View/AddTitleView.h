//
//  AddTitleView.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/8.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddVillageDelegate <NSObject>

- (void)addVillage;

@end

@interface AddTitleView : UIView
@property (nonatomic, copy) UILabel *titleLabel;

- (instancetype)init:(NSInteger)count;

- (void)titleLabelConfig:(NSInteger)count;

- (void)reloadTitleLabel:(NSString *)home;

@property (nonatomic, weak) id<AddVillageDelegate> delegate;

@end
