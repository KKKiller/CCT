//
//  VillageCell.h
//  cuncuntong_iOS
//
//  Created by 朱帅 on 16/9/5.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VillageCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@end
