//
//  CCMyStockCharView.m
//  TestDemo
//
//  Created by 周吾昆 on 2017/9/8.
//  Copyright © 2017年 周吾昆. All rights reserved.
//

#import "CCMyStockCharView.h"
#import <PNColor.h>
@implementation CCMyStockCharView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.stockLblArray = [NSMutableArray arrayWithCapacity:5];
        self.stockNumLblArray = [NSMutableArray arrayWithCapacity:5];
        [self setUI];
    }
    return self;
}
- (void)setUI {
    
    self.titleLbl = [[UILabel alloc]init];
    self.titleLbl.frame = CGRectMake(0, 5, self.frame.size.width, 20);
    [self addSubview:self.titleLbl];
    self.titleLbl.textAlignment = NSTextAlignmentCenter;
    self.titleLbl.font = [UIFont systemFontOfSize:14];
    self.titleLbl.textColor = [UIColor purpleColor];
    self.titleLbl.text = @"股票分配图";
    
    CGFloat height = 30;
    for (int i = 0; i<5; i++) {
        UILabel *stockNameLbl = [[UILabel alloc]init];
        stockNameLbl.font = [UIFont systemFontOfSize:12];
        [self addSubview:stockNameLbl];
        stockNameLbl.frame = CGRectMake(self.frame.size.width - 150*KEY_RATE, i*height  + 45, 100*KEY_RATE, height);
        [self.stockLblArray addObject:stockNameLbl];
        
        UILabel *numLbl = [[UILabel alloc]init];
        [self addSubview:numLbl];
         numLbl.font = [UIFont systemFontOfSize:12];
        numLbl.textColor = MTRGB(0x48b3aa);
        numLbl.frame = CGRectMake(self.frame.size.width - 50*KEY_RATE, i*height  + 45, 40*KEY_RATE, height);
        [self.stockNumLblArray addObject:numLbl];
    }
    [self setStock];
}

- (void)setStock {
    [self setLblText];
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:11 color:MTRGB(0xf9bb65)],
                       [PNPieChartDataItem dataItemWithValue:5 color:MTRGB(0xf6f06b)],
                       [PNPieChartDataItem dataItemWithValue:10 color:MTRGB(0x65c2ba)],
                       [PNPieChartDataItem dataItemWithValue:64 color:MTRGB(0x4cb3aa)],
                       [PNPieChartDataItem dataItemWithValue:10 color:MTRGB(0x68db66)],
                       ];
    
    self.pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(15, 40, 180.0 *KEY_RATE, 180.0 *KEY_RATE) items:items];
    [self addSubview:self.pieChart];
    self.pieChart.descriptionTextColor = [UIColor whiteColor];
    self.pieChart.descriptionTextFont = [UIFont fontWithName:@"Avenir-Medium" size:11.0];
    self.pieChart.descriptionTextShadowColor = [UIColor clearColor];
    self.pieChart.showAbsoluteValues = NO;
    self.pieChart.showOnlyValues = NO;
    [self.pieChart strokeChart];
    
    self.pieChart.legendStyle = PNLegendItemStyleStacked;
    self.pieChart.legendFont = [UIFont boldSystemFontOfSize:12.0f];
    
    UIView *v = [[UIView alloc]init];
    [self addSubview:v];
    v.backgroundColor = BLACKCOLOR;
    v.alpha = 0.1;
    v.layer.cornerRadius = 32.5;
    v.layer.masksToBounds = YES;
    [v mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.pieChart);
        make.width.height.mas_equalTo(65);
    }];
    UILabel *lbl = [[UILabel alloc]initWithText:@"股份" font:18 textColor:TEXTBLACK3];
    [self addSubview:lbl];
    lbl.textAlignment = NSTextAlignmentCenter;
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.pieChart);
        make.width.height.mas_equalTo(40);
    }];

}
- (void)setLblText {
    NSArray *titleArray = @[@"股东原始股份",@"县代理股份",@"村代理股份",@"会员酒厂股份",@"总公司股份"];
    NSArray *perArray = @[@"11%",@"5%",@"10%",@"64%",@"10%"];
    for (int i = 0;i < 5; i++) {
        UILabel *nameLbl = self.stockLblArray[i];
        UILabel *numLbl = self.stockNumLblArray[i];
        nameLbl.text = titleArray[i];
        numLbl.text = perArray[i];
    }
}
@end
