//
//  CCPositionModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/10.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCPositionModel : NSObject
@property (nonatomic, strong) NSString *provinceId;
@property (nonatomic, strong) NSString *provinceName;
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *districtId;
@property (nonatomic, strong) NSString *districtName;
@end
