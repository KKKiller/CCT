//
//  CCMYHomeController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCBaseWebviewController.h"

typedef NS_ENUM(NSInteger, TreeGrowingStage) {
    TreeGrowingStageJindou = 0,//小金豆
    TreeGrowingStageFaya,//金豆苗
    TreeGrowingStageChutu,//小树苗
    TreeGrowingStageYoumiao,//小树
    TreeGrowingStageZhigan,//中树
    TreeGrowingStageMaosheng,//大树
    TreeGrowingStageKaihua,//开花
    TreeGrowingStageQingguo,//青果
    TreeGrowingStageHuangguo,//黄果
    TreeGrowingStageYouhuayouguo,//有花有苗
};

@interface MYHomeController : BaseViewController

@end
