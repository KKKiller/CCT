//
//  CCRankController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCRankController : BaseViewController
@property (nonatomic, strong) NSString *locationID;
@property (nonatomic, assign) NSInteger type; //0全国  1省 2城市
@property (nonatomic, assign) BOOL isOpen;
@end
