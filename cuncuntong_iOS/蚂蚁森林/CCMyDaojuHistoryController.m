//
//  CCMyDaojuHistoryController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/8/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyDaojuHistoryController.h"
#import "CCPKCell.h"
#import "MYPKModel.h"
static NSString *kCellId = @"CCPKCell";

@interface CCMyDaojuHistoryController()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) UILabel *header;

@end

@implementation CCMyDaojuHistoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"道具使用"];
    [self.view addSubview:self.tableView];
    [self loadData];
    [self initRefreshView];
    self.header.text = [NSString stringWithFormat:@"      我已推荐%ld人种豆",self.userInfo.invite_num];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)loadData {
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/userinfo_logs") params:@{@"rid":STR(USERID),@"page":[NSString stringWithFormat:@"%ld",self.pageIndex]} success:^(NSDictionary *success) {
        if (self.pageIndex == 1) {
            [self.dataArray removeAllObjects];
        }
        if ([success[@"error"] integerValue] == 0) {
            for (NSDictionary *dict in success[@"data"]) {
                MYPKModel *model = [MYPKModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
        }
        [self endLoding:self.dataArray];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
        
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [TOOL hideLoading:self.view];
    [self.tableView reloadData];
    if (array.count % 15 == 0) {
        [self.tableView.mj_footer endRefreshing];
    }else{
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCPKCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
    MYPKModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}



- (UITableView *)tableView {
    if (!_tableView) {
        
        CGFloat y = kSystemVersion >= 11.0 ? 0 : 64;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, y, App_Width, App_Height - y) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.header;
        _tableView.estimatedRowHeight = 44;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        [_tableView registerNib:[UINib nibWithNibName:@"CCPKCell" bundle:nil] forCellReuseIdentifier:kCellId];
        
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (UILabel *)header {
    if (!_header) {
        _header = [[UILabel alloc]initWithText:@"我已推荐%@人种豆" font:18 textColor:TEXTBLACK3];
        _header.frame = CGRectMake(0, 0, App_Width, 40);
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(15, 10, 5, 20)];
        v.backgroundColor = MTRGB(0x97cd00);
        [_header addSubview:v];
    }
    return _header;
}
@end

