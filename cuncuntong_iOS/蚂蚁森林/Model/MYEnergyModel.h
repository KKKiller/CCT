//
//  MYEnergyModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface MYSubEnergyModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, assign) NSInteger energy;
@property (nonatomic, strong) NSString *status; //被偷
@end
@interface MYEnergyModel : NSObject
//运动能量
@property (nonatomic, assign) NSInteger sport_energy;
@property (nonatomic, assign) NSInteger sport_energy_remain;
@property (nonatomic, assign) NSInteger sport_energy_today_remain;
@property (nonatomic, assign) NSInteger sport_energy_today;
@property (nonatomic, strong) NSString *sport_energy_status;//被偷
//阅读能量
@property (nonatomic, assign) NSInteger read_energy;
@property (nonatomic, assign) NSInteger read_energy_remain;
@property (nonatomic, assign) NSInteger read_energy_today;
@property (nonatomic, assign) NSInteger read_energy_today_remain;
@property (nonatomic, strong) NSString *read_energy_status;
//红包能量
//@property (nonatomic, assign) NSInteger red_energy;
//@property (nonatomic, assign) NSInteger red_energy_remain;
//@property (nonatomic, assign) NSInteger red_energy_today;
//@property (nonatomic, assign) NSInteger red_energy_today_remain;
//@property (nonatomic, strong) NSString *red_energy_status;
//信用
@property (nonatomic, assign) NSInteger credit_energy;
@property (nonatomic, strong) NSString *credit_energy_status;//被偷
@property (nonatomic, assign) NSInteger credit_energy_remain;


//邀请能量
@property (nonatomic, assign) NSInteger invite_energys;
@property (nonatomic, strong) NSMutableArray<MYSubEnergyModel*> *inviteIdArray;
//购物能量
@property (nonatomic, assign) NSInteger buy_energys;
@property (nonatomic, strong) NSMutableArray<MYSubEnergyModel*> *buyIdArray;
//红包能量
@property (nonatomic, assign) NSInteger red_energys;
@property (nonatomic, strong) NSMutableArray<MYSubEnergyModel*> *redIdArray;

@property (nonatomic, assign) NSInteger sp_energys;
@property (nonatomic, strong) NSMutableArray<MYSubEnergyModel*> *spIdArray;



//能量机能量
@property (nonatomic, assign) NSInteger nlj_energys;
@property (nonatomic, strong) NSMutableArray<MYSubEnergyModel*> *nljIdArray;



//分享
@property (nonatomic, assign) NSInteger share_energy_today;
@property (nonatomic, assign) NSInteger share_energy_today_remain;
@property (nonatomic, assign) NSInteger share_energy;
@property (nonatomic, assign) NSInteger share_energy_remain;
@property (nonatomic, strong) NSString *share_energy_status;

//女儿国签到
@property (nonatomic, assign) NSInteger sign_energy_today;//女儿国今日签到 能量数
@property (nonatomic, assign) NSInteger sign_energy_today_remain;//等待时间

@property (nonatomic, strong) NSMutableArray *tjArray;

@property (nonatomic, assign) BOOL existBean;
@property (nonatomic, strong) NSMutableArray *beanArray;
- (instancetype)initWithDict:(NSDictionary *)dict;
@end
