//
//  MYUserInfoModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYUserInfoModel : NSObject
@property (nonatomic, strong) NSString *rid;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, assign) NSInteger bean_num;
@property (nonatomic, strong) NSString *energy;
@property (nonatomic, strong) NSString *village_id;
@property (nonatomic, strong) NSString *area_id;
@property (nonatomic, strong) NSString *city_id;
@property (nonatomic, strong) NSString *province_id;
@property (nonatomic, strong) NSString *rail_expire;//防盗网过期时间
@property (nonatomic, assign) BOOL isFangDaoWangEnable;
@property (nonatomic, strong) NSString *fangDaoWangShowTime;
@property (nonatomic, assign) NSTimeInterval fangDaoWangTimeLeft;

@property (nonatomic, strong) NSString *is_praised;
@property (nonatomic, strong) NSString *inviter_id;
@property (nonatomic, strong) NSString *visit_time;
@property (nonatomic, strong) NSString *visit_timeFormat;
@property (nonatomic, assign) NSInteger invite_num;

@end


@interface MYPositionModel:NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *num;
@end
