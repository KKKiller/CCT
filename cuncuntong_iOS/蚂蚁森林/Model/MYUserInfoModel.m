//
//  MYUserInfoModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYUserInfoModel.h"

@implementation MYUserInfoModel
- (NSString *)visit_time {
    if (!_visit_timeFormat) {
        _visit_timeFormat = [TOOL convertTextTime:_visit_time];
    }
    return _visit_timeFormat;
}
- (BOOL)isFangDaoWangEnable {
    if (STRINGEMPTY(self.rail_expire)) {
        return NO;
    }
    NSDate *inputDate = [TOOL.formatter1 dateFromString:self.rail_expire];
    NSTimeInterval timestampval = [inputDate timeIntervalSince1970];
    //获取本地时间
    NSDate * nowDate = [NSDate date];
    NSTimeInterval current = [nowDate timeIntervalSince1970];
    self.fangDaoWangTimeLeft = current < timestampval;
    return self.fangDaoWangTimeLeft > 0;
}
- (NSString *)fangDaoWangShowTime {
    if (!self.isFangDaoWangEnable) {
        return nil;
    }
    if (self.rail_expire.length > 10) {
        return [self.rail_expire substringToIndex:10];
    }
    return nil;
}

@end

@implementation MYPositionModel


@end
