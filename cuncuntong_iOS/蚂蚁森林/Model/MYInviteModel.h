//
//  MYInviteModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/5.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYInviteModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *rid;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, assign) NSInteger bean_num;
@property (nonatomic, strong) NSString *energy;
@property (nonatomic, strong) NSString *village_id;
@property (nonatomic, strong) NSString *area_id;
@property (nonatomic, strong) NSString *city_id;
@property (nonatomic, strong) NSArray *province_id;
@property (nonatomic, strong) NSString *rail_expire;
@property (nonatomic, assign) BOOL is_praised;
//@property (nonatomic, strong) NSString *is_praised;
@property (nonatomic, strong) NSString *inviter_id;
@property (nonatomic, strong) NSString *praise_num;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString *steps;
@end
