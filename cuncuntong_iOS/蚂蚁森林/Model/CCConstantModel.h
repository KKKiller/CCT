//
//  CCConstantModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/15.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYHomeController.h"
@interface CCConstantModel : NSObject
@property (nonatomic, assign) NSInteger faya;
@property (nonatomic, assign) NSInteger chutu;
@property (nonatomic, assign) NSInteger youmiao;
@property (nonatomic, assign) NSInteger zhigan;
@property (nonatomic, assign) NSInteger maosheng;
@property (nonatomic, assign) NSInteger kaihua;
@property (nonatomic, assign) NSInteger qingguo;
@property (nonatomic, assign) NSInteger huangguo;
@property (nonatomic, assign) NSInteger chengshu;

@property (nonatomic, assign) NSInteger bean_price;
- (TreeGrowingStage)getCurrentStageWithEnergy:(NSInteger )energy;
@end
