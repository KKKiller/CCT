//
//  CCConstantModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/15.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCConstantModel.h"

@implementation CCConstantModel
- (TreeGrowingStage)getCurrentStageWithEnergy:(NSInteger)energy {
    TreeGrowingStage stage = -1;
    if (energy >= 0 && energy <self.faya) {
        stage = TreeGrowingStageJindou;
    }else if(energy >= self.faya && energy < self.chutu){
        stage = TreeGrowingStageFaya;
    }else if(energy >= self.chutu && energy < self.youmiao){
        stage = TreeGrowingStageChutu;
    }else if(energy >= self.youmiao && energy < self.zhigan){
        stage = TreeGrowingStageYoumiao;
    }else if(energy >= self.zhigan && energy < self.maosheng){
        stage = TreeGrowingStageZhigan;
    }else if(energy >= self.maosheng && energy < self.kaihua){
        stage = TreeGrowingStageMaosheng;
    }else if(energy >= self.kaihua && energy < self.qingguo){
        stage = TreeGrowingStageKaihua;
    }else if(energy >= self.qingguo && energy < self.huangguo){
        stage = TreeGrowingStageQingguo;
    } else if(energy >= self.huangguo && energy < self.chengshu){
        stage = TreeGrowingStageQingguo;
    }else if(energy >= self.chengshu ){
        stage = TreeGrowingStageHuangguo;
    }
    return stage;
}
@end

