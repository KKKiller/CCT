//
//  MYEnergyModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYEnergyModel.h"
@implementation MYSubEnergyModel

@end
@implementation MYEnergyModel
- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _read_energy = [dict[@"read_energy"] integerValue];
        _read_energy_today = [dict[@"read_energy_today"] integerValue];
        _read_energy_remain = [dict[@"read_energy_remain"] integerValue];
        _read_energy_status = dict[@"read_energy_status"];
        _read_energy_today_remain = [dict[@"read_energy_today_remain"] integerValue];
        
//        _red_energy = [dict[@"red_energy"] integerValue];
//        _red_energy_today = [dict[@"red_energy_today"] integerValue];
//        _red_energy_remain = [dict[@"red_energy_remain"] integerValue];
//        _red_energy_status = dict[@"red_energy_status"];
//        _red_energy_today_remain = [dict[@"red_energy_today_remain"] integerValue];
        
        
        _sport_energy = [dict[@"sport_energy"] integerValue];
        _sport_energy_today = [dict[@"sport_energy_today"] integerValue];
        _sport_energy_remain = [dict[@"sport_energy_remain"] integerValue];
        _sport_energy_status = dict[@"sport_energy_status"];
        _sport_energy_today_remain = [dict[@"sport_energy_today_remain"] integerValue];
        
        _credit_energy = [dict[@"credit_energy"] integerValue];
        _credit_energy_remain = [dict[@"credit_energy_remain"] integerValue];
        //分享
        _share_energy_today = [dict[@"share_energy_today"] integerValue];
        _share_energy_today_remain = [dict[@"share_energy_today_remain"] integerValue];
        _share_energy = [dict[@"share_energy"] integerValue];
        _share_energy_remain = [dict[@"share_energy_remain"] integerValue];
        _share_energy_status = dict[@"share_energy_status"];
        
        //女儿国签到
        _sign_energy_today = [dict[@"sign_energy_today"] integerValue];
        _sign_energy_today_remain = [dict[@"sign_energy_today_remain"] integerValue];

        _inviteIdArray  =[NSMutableArray array];
        NSArray *array1 = dict[@"invite_energy"];
        for (NSDictionary *dic in array1) {
            _invite_energys += [dic[@"energy"] integerValue];
            MYSubEnergyModel *subModel = [MYSubEnergyModel modelWithJSON:dic];
            [_inviteIdArray addObject:subModel];
        }
        
        _buyIdArray = [NSMutableArray array];
        NSArray *array2 = dict[@"buy_energy"];
        for (NSDictionary *dic in array2) {
            _buy_energys += [dic[@"energy"] integerValue];
            MYSubEnergyModel *subModel = [MYSubEnergyModel modelWithJSON:dic];
            [_buyIdArray addObject:subModel];
        }
        
        _redIdArray = [NSMutableArray array];
        NSArray *array4 = dict[@"red_energy"];
        for (NSDictionary *dic in array4) {
            _red_energys += [dic[@"energy"] integerValue];
            MYSubEnergyModel *subModel = [MYSubEnergyModel modelWithJSON:dic];
            if (subModel.energy > 0) {
                [_redIdArray addObject:subModel];                
            }
        }
        
        _nljIdArray = [NSMutableArray array];
        NSArray *array5 = dict[@"emachine_energy"];
        for (NSDictionary *dic in array5) {
            _nlj_energys += [dic[@"energy"] integerValue];
            MYSubEnergyModel *subModel = [MYSubEnergyModel modelWithJSON:dic];
            if (subModel.energy > 0) {
                [_nljIdArray addObject:subModel];
            }
        }
        
        
        _spIdArray = [NSMutableArray array];
        NSArray *array3 = dict[@"share_recharge"];
        for (NSDictionary *dic in array3) {
            _sp_energys += [dic[@"energy"] integerValue];
            MYSubEnergyModel *subModel = [MYSubEnergyModel modelWithJSON:dic];
            [_spIdArray addObject:subModel];
        }
        
        _existBean = [dict containsObjectForKey:@"bean"] && [dict[@"bean"] isKindOfClass:[NSArray class]];
        _beanArray = [NSMutableArray array];
        NSArray *beans = dict[@"bean"];
        for (NSDictionary *dict in beans) {
            [_beanArray addObject:[NSString stringWithFormat:@"%@",dict[@"id"]]];
        }

        NSArray *tuijian = dict[@"tj_energy"];
        _tjArray = [NSMutableArray array];
        for (NSDictionary *dict in tuijian) {
            MYSubEnergyModel *model = [MYSubEnergyModel modelWithJSON:dict];
            [_tjArray addObject:model];
        }
        
        
//        _nlj_energys = 100;
        
//        _read_energy_today = 100;
//        _read_energy = 100;
//        _read_energy_remain = 100;
//        _read_energy_today_remain = 100;
//        _sport_energy_today = 100;
//        _sport_energy = 100;
//        _sport_energy_remain = 100;
//        _share_energy = 200;
//        _share_energy_remain = 110;
//        _share_energy_status = @"未收取";
        
    }
    return self;
}
- (NSInteger )getCurrentHour{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //现在时间,你可以输出来看下是什么格式
    
    NSDate *datenow = [NSDate date];
    
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    NSString *hour = [currentTimeString substringWithRange:NSMakeRange(10, 2)];
    
    return [hour integerValue];
    
}
@end

