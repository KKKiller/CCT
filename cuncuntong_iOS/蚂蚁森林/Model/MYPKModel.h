//
//  MYPKModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/5.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYPKModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *rid;
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSString *add_time;
@property (nonatomic, strong) NSString *name;

@end
