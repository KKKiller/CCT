//
//  CCMYHomeController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHomeController.h"
#import "MYHomeHeaderView.h"
#import "MYHomeFooterView.h"
#import "MYHomeSectionHeaderView.h"
#import "MYHomeCell.h"
#import "CCDaojuView.h"
#import "CCGongLueController.h"
#import "CCMyShopController.h"
#import "CCMyReadController.h"
#import "CCMySportController.h"
#import "CCMyInviteController.h"
#import "CCDouGuanController.h"
#import "CCDouSaiController.h"
#import "CCPriceController.h"
#import "CCPKController.h"
#import "CCMyEnergyTagView.h"
#import <ContactsUI/ContactsUI.h>
 #import <MessageUI/MessageUI.h>
#import "CCShareView.h"
#import "MYUserInfoModel.h"
#import "MYRankModel.h"
#import "CCRankController.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "CCConstantModel.h"
#import "CCPayController.h"
#import "RechargeViewController.h"
#import "CCMyShopController.h"
#import "CCMyReadController.h"
#import "CCMyDaojuHistoryController.h"
#import "DetailViewController.h"
#import "BRPickerView.h"
#import "CCShareADController.h"
#import "CCEnergyController.h"
// CGFloat kHeaderHeight  = App_Width + 80;
//static CGFloat kHeaderFullHeight  = 650 + 280;

static CGFloat kFooterHeight  = 80;

@interface MYHomeController ()<UITableViewDelegate,UITableViewDataSource,CNContactPickerDelegate,MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCDaojuView *daojuView;
@property (nonatomic, strong) MYHomeHeaderView *headerView;
@property (nonatomic, strong) MYHomeFooterView *footerView;
@property (nonatomic, strong) CCShareView *shareView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *imgArray;
@property (nonatomic, assign) NSInteger loadIndex;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) CCMyEnergyTagView *energyTagView;
@property (nonatomic, strong) MYUserInfoModel  *userInfoModel;
@property (nonatomic, strong) MYPositionModel *provinceModel;
@property (nonatomic, strong) MYPositionModel *areaModel;
@property (nonatomic, strong) MYEnergyModel *energyModel;
@property (nonatomic, strong) NSString *allCount;
@property (nonatomic, strong) NSMutableArray *allArray;//全国
@property (nonatomic, strong) NSMutableArray *provinceArray;//省
@property (nonatomic, strong) NSMutableArray *areaArray;//城市
@property (nonatomic, assign) NSInteger sectionCount;
@property (nonatomic, strong) NSMutableArray *visitors;
@property (nonatomic, assign) BOOL isViewAppear;//  正在显示
@property (nonatomic, assign) BOOL pickJindouToast;
@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, assign) BOOL hasShowAnimation;
@property (nonatomic, assign) BOOL needRefreshAniamtion;
@property (nonatomic, assign) BOOL viewDidAppear;//显示过
@property (nonatomic, assign) BOOL hasShowToast;
@property (nonatomic, assign) TreeGrowingStage currentStage;
@property (nonatomic, assign) NSInteger currentDaojuType;
@property (nonatomic, strong) CCConstantModel *constantModel;
@end

@implementation MYHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.daojuView];
    [self.view addSubview:self.backBtn];
    [self.view addSubview:self.energyTagView];
    [self.view addSubview:self.shareView];
    [self addTopBtns];
    self.allArray = [NSMutableArray array];
    self.provinceArray = [NSMutableArray array];
    self.areaArray = [NSMutableArray array];
    self.visitors = [NSMutableArray array];
    self.navigationController.navigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    self.currentStage = -1;
    [NOTICENTER addObserver:self selector:@selector(pushToPkVc:) name:@"pushToPkVc" object:nil];
    [NOTICENTER addObserver:self selector:@selector(rechargeSuccess) name:@"PaySuccess" object:nil];
    if ([Def boolForKey:@"healths"]) {
//        [App_Delegate updateSetps:nil];
    }
    
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadPositionData]; //排行榜信息，个人信息
    self.isViewAppear = YES;
    self.navigationController.navigationBarHidden = YES;
}

-(void)dealloc{
    NSLog(@"销毁了");
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.isViewAppear = NO;
    [self.timer invalidate];
    self.timer = nil;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.viewDidAppear = YES;
    [self startStageAnimation];
}
//成长动画
- (void)startStageAnimation{
    if (!self.hasShowAnimation && self.currentStage >= 0 && self.viewDidAppear) {
        self.hasShowAnimation = YES;
        [self.headerView.treeView beginBirdFlyAnimation];//小鸟动画
        if (self.energyModel.beanArray.count > 0) {
            if (self.currentStage == TreeGrowingStageKaihua) {
                self.currentStage = TreeGrowingStageYouhuayouguo;
            }else{
                self.currentStage = TreeGrowingStageHuangguo;
            }
        }
        [self.headerView.treeView beginTreeGrwoingAnimationWithStage:self.currentStage];//成长动画
        [self.headerView.treeView bubbleAnimation];//气泡动画
    }
}
- (void)refreshAnimation {
    self.hasShowAnimation = NO;
    self.currentStage = [self.constantModel getCurrentStageWithEnergy:[self.userInfoModel.energy integerValue]];
    [self startStageAnimation];
}
//还差多少能量提示
- (void)showEnergyToast {
    if ([self.userInfoModel.energy intValue] < self.constantModel.chengshu && self.energyModel.beanArray && self.energyModel.beanArray.count == 0) {
        NSInteger need = self.constantModel.chengshu - [self.userInfoModel.energy integerValue];
        NSString *text = [NSString stringWithFormat:@"再赚取%ld克能量就可结金豆啦\n再接再厉吧",need];
        SHOW(text);
    }else if (self.energyModel.beanArray.count > 0 && !self.pickJindouToast){
        self.pickJindouToast = YES;
        SHOW(@"金豆已成熟，赶快收入豆罐吧");
    }
}
- (void)loadPositionData {
//    [TOOL showLoading:self.view];
    WEAKSELF
    self.hasShowAnimation = NO;
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/userinfo") params:@{@"rid":USERID} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            weakSelf.userInfoModel = [MYUserInfoModel modelWithJSON:success[@"data"][@"userinfo"]];
//            [self loadConstant];
            //保护罩动画
            if (weakSelf.userInfoModel.isFangDaoWangEnable) {
                [weakSelf.headerView.treeView showBaohuzhaoAnimationWithFinishBlk:nil];
                weakSelf.headerView.treeView.baohuzhaoTime = weakSelf.userInfoModel.fangDaoWangShowTime;
            }
            weakSelf.energyTagView.userInfo = weakSelf.userInfoModel;
            NSArray *areas = success[@"data"][@"areas"];
            [weakSelf getPersonCountWithType:@"all" id:@"0" dataModel:nil];
            for (NSDictionary *dict in areas) {
                NSString *id = [NSString stringWithFormat:@"%@", dict[@"id"]];
                if ([id isEqualToString:weakSelf.userInfoModel.province_id]) {
                    weakSelf.provinceModel = [MYPositionModel modelWithJSON:dict];
                    [weakSelf getPersonCountWithType:@"province_id" id:id dataModel:weakSelf.provinceModel];
                }
                if ([id isEqualToString:weakSelf.userInfoModel.area_id]) {
                    weakSelf.areaModel = [MYPositionModel modelWithJSON:dict];
                    [weakSelf getPersonCountWithType:@"area_id" id:id dataModel:weakSelf.areaModel];
                }
            }
            [weakSelf.visitors removeAllObjects];
            NSArray *visitors = success[@"data"][@"visitors"];
            for (NSDictionary *dict in visitors) {
                MYUserInfoModel *model = [MYUserInfoModel modelWithJSON:dict];
                [weakSelf.visitors addObject:model];
            }
            //最近访问的人
            weakSelf.headerView.visitors = weakSelf.visitors;
            if (weakSelf.visitors.count > 0) {
                weakSelf.headerView.height = App_Width  * 1746.0 / 1242.0 + 60 + 300 + 100;
            }
            weakSelf.isOpen = [success[@"data"][@"is_open"] boolValue];
            weakSelf.headerView.treeView.openJindouBtn.hidden = weakSelf.isOpen;
            weakSelf.energyTagView.hidden = !weakSelf.isOpen;
            [weakSelf loadRankListData];
            [weakSelf loadEnergy];

        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        [TOOL hideLoading:weakSelf.view];
    }];
}

- (void)loadRankListData {
    WEAKSELF
    //全国排行榜
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:@{@"type":@"all",@"page":@"1"}];
    if (self.isOpen) {
        [dict1 setValue:USERID forKey:@"rid"];
    }
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/rank") params:dict1 success:^(NSDictionary *success) {
        if ([success[@"error"]integerValue] == 0) {
            [weakSelf.allArray removeAllObjects];
            for (NSDictionary *dict in success[@"data"][@"data"]) {
                MYRankModel *model = [MYRankModel modelWithJSON:dict];
                [weakSelf.allArray addObject:model];
            }
        }else{
            SHOW(@"服务器出错，请退出页面重进或稍后再试");
        }
        [weakSelf endLoading];
    } failure:^(NSError *failure) {
        [weakSelf endLoading];
    }];
    
    //省排行榜
    NSMutableDictionary *dict2 = [NSMutableDictionary dictionaryWithDictionary:@{@"type":@"province_id",@"type_val":weakSelf.provinceModel.id ?:@"",@"page":@"1"}];
    if (weakSelf.isOpen) {
        [dict1 setValue:USERID forKey:@"rid"];
    }
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/rank") params:dict2 success:^(NSDictionary *success) {
        if ([success[@"error"]integerValue] == 0) {
            [weakSelf.provinceArray removeAllObjects];
            for (NSDictionary *dict in success[@"data"][@"data"]) {
                MYRankModel *model = [MYRankModel modelWithJSON:dict];
                [weakSelf.provinceArray addObject:model];
            }
        }
        [weakSelf endLoading];
    } failure:^(NSError *failure) {
        [weakSelf endLoading];
    }];
    
    //城市排行榜
    NSMutableDictionary *dict3 = [NSMutableDictionary dictionaryWithDictionary:@{@"type":@"area_id",@"type_val":weakSelf.areaModel.id ?:@"",@"page":@"1"}];
    if (weakSelf.isOpen) {
        [dict1 setValue:USERID forKey:@"rid"];
    }
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/rank") params:dict3 success:^(NSDictionary *success) {
        if ([success[@"error"]integerValue] == 0) {
            [weakSelf.areaArray removeAllObjects];
            for (NSDictionary *dict in success[@"data"][@"data"]) {
                MYRankModel *model = [MYRankModel modelWithJSON:dict];
                [weakSelf.areaArray addObject:model];
            }
        }
        [weakSelf endLoading];
    } failure:^(NSError *failure) {
        [weakSelf endLoading];
    }];
}
// 检查是否有金豆
- (void)loadEnergy {
    WEAKSELF
    if(!self.isOpen){
        return;
    }
//    [self setBeadData:[TOOL getCachaDataWithName:@"beanCheck"]];
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/check") params:@{@"rid":USERID} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [weakSelf setBeadData:success];
//            [TOOL saveDataWithData:success Name:@"beanCheck"];
            if(weakSelf.needRefreshAniamtion){
                weakSelf.needRefreshAniamtion = NO;
                [weakSelf refreshAnimation];
            }
            [weakSelf loadConstant];

        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)setBeadData:(NSDictionary*)success {
    if (!success[@"data"]) {
        return;
    }
    self.energyModel = [[MYEnergyModel alloc]initWithDict:success[@"data"]];
    self.headerView.treeView.energyModel = self.energyModel;
    [self.tableView reloadData];
    if (!self.hasShowToast) {
        self.hasShowToast =YES;
        [self showEnergyToast];
    }
    if (self.energyModel.beanArray.count > 0) {//展示有花有果
        [self refreshAnimation];
    }
    if (self.energyModel.existBean && self.energyModel.beanArray.count > 1) {
        [self.headerView.treeView showJindouAnimation];
        [self pickBeanWithId:self.energyModel.beanArray[0]];
        [self.energyModel.beanArray removeFirstObject];
        self.headerView.treeView.energyModel = self.energyModel;
    }
}
- (void)pickBeanWithId:(NSString *)id {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/bean_pick") params:@{@"rid":USERID,@"id":id,@"ta_id":@"0"} success:^(NSDictionary *success) {
        NSLog(@"%@",success);
    } failure:^(NSError *failure) {
        
    }];
}
//结果常量
- (void)loadConstant {
    WEAKSELF
    [self setConstangData:[TOOL getCachaDataWithName:@"constant"]];
    
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/constants") params:@{} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            if (![TOOL getCachaDataWithName:@"constant"]) {
                [weakSelf setConstangData:success[@"data"]];
            }
            [TOOL saveDataWithData:success[@"data"] Name:@"constant"];
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)setConstangData:(NSDictionary *)data {
    CCConstantModel *model = [CCConstantModel modelWithJSON:data];
    self.constantModel = model;
    [Def setValue:@(self.constantModel.bean_price) forKey:@"beadPrice"];
    self.currentStage = [model getCurrentStageWithEnergy:[self.userInfoModel.energy integerValue]];
    [self startStageAnimation];
    if (!self.hasShowToast) {
        self.hasShowToast = YES;
        [self showEnergyToast];
    }
}

- (void)stealEnergy:(NSString *)type {
    WEAKSELF
    NSString *id = @"0";
    NSInteger index = 0;
    if ([type isEqualToString:@"邀请"] ) {
        if (self.energyModel.inviteIdArray.count > 0) {
            index = arc4random() %  self.energyModel.inviteIdArray.count;
            if (self.energyModel.inviteIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.inviteIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"购物"] ) {
        if (self.energyModel.buyIdArray.count > 0) {
            index = arc4random() %  self.energyModel.buyIdArray.count;
            if (self.energyModel.buyIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.buyIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"红包"] ) {
        if (self.energyModel.redIdArray.count > 0) {
            index = arc4random() %  self.energyModel.redIdArray.count;
            if (self.energyModel.redIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.redIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"能量机"] ) {
        if (self.energyModel.nljIdArray.count > 0) {
            index = arc4random() %  self.energyModel.nljIdArray.count;
            if (self.energyModel.nljIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.nljIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"生活圈充值"] ) {
        if (self.energyModel.spIdArray.count > 0) {
            index = arc4random() %  self.energyModel.spIdArray.count;
            if (self.energyModel.spIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.spIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"推荐"] ) {
        if (self.energyModel.tjArray.count > 0) {
            MYSubEnergyModel *model = self.energyModel.tjArray[0];
            id = model.id;
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"信用值"] ) {
        if (self.energyModel.credit_energy > 0 && self.energyModel.credit_energy_remain > 0) {
            SHOW(@"未到收取时间");
            return;
        }
    }
    if ([type isEqualToString:@"分享"] ) {
        if (self.energyModel.share_energy_today > 0 && self.energyModel.share_energy_today_remain > 0) {
            SHOW(@"未到收取时间");
            return;
        }
    }
    if ([type isEqualToString:@"运动"]) {
        if (self.energyModel.sport_energy_today > 0 && self.energyModel.sport_energy == 0) {
            SHOW(@"今日能量需明日才可收取");
            return;
        }
        if (self.energyModel.sport_energy > 0 && self.energyModel.sport_energy_remain > 0) {
            SHOW(@"未到收取时间");
            return;
        }
    }
    if ([type isEqualToString:@"阅读"]) {
        if (self.energyModel.read_energy_today > 0 && self.energyModel.read_energy == 0) {
            SHOW(@"今日能量需明日才可收取");
            return;
        }
        if (self.energyModel.read_energy > 0 && self.energyModel.read_energy_remain > 0) {
            SHOW(@"未到收取时间");
            return;
        }
    }
    if ([type isEqualToString:@"签到"]){
        if (self.energyModel.sign_energy_today > 0 && self.energyModel.sign_energy_today == 0) {
            SHOW(@"今日能量需明日才可收取");
            return;
        }
        if (self.energyModel.sign_energy_today > 0 && self.energyModel.sign_energy_today_remain > 0) {
            SHOW(@"未到收取时间");
            return;
        }
    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/energy_pick") params:@{@"rid":USERID,@"type":type,@"ta_id":STR(USERID),@"id":id} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            if ([type isEqualToString:@"邀请"]) {
                [weakSelf.energyModel.inviteIdArray removeObjectAtIndex:index];
            }
            if ([type isEqualToString:@"购物"]) {
                [weakSelf.energyModel.buyIdArray removeObjectAtIndex:index];
            }
            if ([type isEqualToString:@"生活圈充值"]) {
                [weakSelf.energyModel.spIdArray removeObjectAtIndex:index];
            }
            if ([type isEqualToString:@"能量机"]) {
                [weakSelf.energyModel.nljIdArray removeObjectAtIndex:index];
            }
            SHOW(@"收取成功");
            [weakSelf loadPositionData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)getPersonCountWithType:(NSString *)type id:(NSString *)id dataModel:(MYPositionModel *)model{
    WEAKSELF
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/stat") params:@{@"type":type,@"type_val":STR(id)} success:^(NSDictionary *success) {
        if([success[@"error"] integerValue] == 0){
            NSString *num = [NSString stringWithFormat:@"%@", success[@"data"][@"num"]];
            if(model){
                model.num = STR(num);
            }else{
                weakSelf.allCount = STR(num);
            }
            [weakSelf.tableView reloadData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)endLoading {
//    self.loadIndex++;
//    if (self.loadIndex == 3) {
//        self.loadIndex = 0;
        [TOOL hideLoading:self.view];
        [self.tableView reloadData];
//    }
}


#pragma mark - 数据源

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count  = self.provinceArray.count == 0 ? 1 : self.areaArray.count == 0 ? 2 : 3;
    self.sectionCount = count;
    return count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger areaCount = self.areaArray.count;
    NSInteger provinceCount = self.provinceArray.count;
    NSInteger allCount = self.allArray.count;
    if (section == 0) {
        return areaCount > 0 ? areaCount : provinceCount > 0 ? provinceCount : allCount;
    }else if (section == 1){
        return areaCount > 0 ? provinceCount : allCount;
    }else if (section == 2){
        return allCount;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MYHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"homeCell"];
    MYRankModel *model = [self getModelWithIndexPath:indexPath];
    if (model) {
        model.index = indexPath.row;
        cell.model = model;
    }
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MYHomeSectionHeaderView *header = [[MYHomeSectionHeaderView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 50)];
    if (self.sectionCount == 1) {
        header.titleLbl.text = [NSString stringWithFormat:@"全国排行榜(%@人)",STR(self.allCount)];
    }else if (self.sectionCount == 2){
        if (section == 0) {
            header.titleLbl.text = [NSString stringWithFormat:@"%@金豆排行榜(%@人)",self.provinceModel.name,STR(self.provinceModel.num)];
        }else if (section == 1){
            header.titleLbl.text = [NSString stringWithFormat:@"全国排行榜(%@人)",STR(self.allCount)];
        }
    }else{
        if (section == 0) {
            header.titleLbl.text = [NSString stringWithFormat:@"%@金豆排行榜(%@人)",self.areaModel.name,STR(self.areaModel.num)];
        }else if (section == 1){
            header.titleLbl.text = [NSString stringWithFormat:@"%@金豆排行榜(%@人)",self.provinceModel.name,STR(self.provinceModel.num)];
        }else if (section == 2){
            header.titleLbl.text = [NSString stringWithFormat:@"全国排行榜(%@人)",STR(self.allCount)];
        }
    }
    header.moreBtn.tag = section;
    [header.moreBtn addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    return header;
}
- (void)moreBtnClick:(UIButton *)sender {
    NSString *locationId;
    NSInteger tag = sender.tag;
    NSInteger type = 0;
    if (self.sectionCount == 1 || self.sectionCount == tag + 1) {
        //全国
    }else if ((self.sectionCount == 2 && tag == 0) || (self.sectionCount == 3 && tag == 1)){
        locationId = self.provinceModel.id;
        type = 1;
    }else{
        locationId = self.areaModel.id;
        type = 2;
    }
    CCRankController *vc = [[CCRankController alloc]init];
    vc.locationID = locationId;
    vc.isOpen = self.isOpen;
    vc.type = type;
    [self.navigationController pushViewController: vc animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MYRankModel *model = [self getModelWithIndexPath:indexPath];
    if (!self.isOpen) {
        SHOW(@"请先开通金豆");
        return;
    }
    if ([model.rid isEqualToString:USERID]) {
        SHOW(@"不能PK自己哦～");
        return;
    }
    CCPKController  *vc = [[CCPKController alloc]init];
    vc.uid = model.rid;
    [self.navigationController pushViewController:vc animated:YES];
}
- (MYRankModel *)getModelWithIndexPath:(NSIndexPath *)indexPath {
    MYRankModel *model;
    if (self.sectionCount == 1 || self.sectionCount == indexPath.section + 1) {
        //全国
        model = self.allArray[indexPath.row];
    }else if ((self.sectionCount == 2 && indexPath.section ==  0) || (self.sectionCount == 3 && indexPath.section == 1)){
        //省
        model = self.provinceArray[indexPath.row];
    }else{
        //城市
        model = self.areaArray[indexPath.row];
    }
    return model;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, -20, App_Width, App_Height + 20) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 70;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.tableHeaderView = self.headerView;
        _tableView.tableFooterView = self.footerView;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"MYHomeCell" bundle:nil] forCellReuseIdentifier:@"homeCell"];
//        [_tableView registerClass:[MYHomeCell class] forCellReuseIdentifier:@"homeCell"];
    }
    return _tableView;
}
- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, isIphoneX ? 40 : 30, 30, 30)];
        [_backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [_backBtn setImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    }
    return _backBtn;
}
- (MYHomeHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[MYHomeHeaderView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Width  * 1746.0 / 1242.0 + 60 + 80 + 100)];
        [_headerView.treeView.daojuBtn addTarget:self action:@selector(daojuBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.treeView.gonglueBtn addTarget:self action:@selector(gonglueBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.treeView.sportBtn addTarget:self action:@selector(sportBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.treeView.doushiBtn addTarget:self action:@selector(doushiBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.treeView.douguanBtn addTarget:self action:@selector(douguanBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.goBtn addTarget:self action:@selector(dousai) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.treeView.openJindouBtn addTarget:self action:@selector(doushiBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.treeView.nengliangjiBtn addTarget:self action:@selector(nengliangjiBtnClick) forControlEvents:UIControlEventTouchUpInside];
        WEAKSELF
        _headerView.treeView.jindouBlk = ^{
            if (weakSelf.energyModel.beanArray.count > 0) {
                
                [weakSelf pickBeanWithId:weakSelf.energyModel.beanArray[0]];
                [weakSelf.energyModel.beanArray removeFirstObject];
                
                [weakSelf refreshAnimation];
                weakSelf.headerView.treeView.energyModel = weakSelf.energyModel;
                [weakSelf loadPositionData];
            }
        };
        [_headerView.treeView.sportTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"运动"];
        }];
        [_headerView.treeView.readTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"阅读"];
        }];
        [_headerView.treeView.redTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"红包"];
        }];
        [_headerView.treeView.nljTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"能量机"];
        }];
        [_headerView.treeView.inviteTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"邀请"];
        }];
        [_headerView.treeView.shopTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"购物"];
        }];
        [_headerView.treeView.gongxiang_tap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"生活圈充值"];
        }];
        [_headerView.treeView.creditTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"信用值"];
        }];
        [_headerView.treeView.share_tap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"分享"];
        }];
        [_headerView.treeView.tuijianTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"推荐"];
        }];
        [_headerView.treeView.signTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"签到"];
        }];
       
        UITapGestureRecognizer *bannerTap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            [weakSelf showBannerPlan];
        }];
        
        [_headerView.treeView.mayiBanner addGestureRecognizer:bannerTap];
    }
    return _headerView;
}
- (void)showBannerPlan {
    self.navigationController.navigationBarHidden = NO;
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/article?p=F97804429F5B3B3A2E2D8A6FB8779D23&u=%@",USERID];
    vc.titleStr = @"健康奖励计划";
    [self.navigationController pushViewController:vc animated:YES];
//    return;
//    UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 300, 830.0 / 640.0 * 300)];
//    view.userInteractionEnabled = YES;
//    view.image = IMAGENAMED(@"mayiPlan");
//    self.animateModel = [[PathDynamicModal alloc]init];
//    self.animateModel.closeBySwipeBackground = YES;
//    self.animateModel.closeByTapBackground = YES;
//    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}
- (MYHomeFooterView *)footerView {
    if (!_footerView) {
        _footerView = [[MYHomeFooterView alloc]initWithFrame:CGRectMake(0, 0, App_Width, kFooterHeight)];
        [_footerView.shareBtn addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    }
    return _footerView;
}
- (CCMyEnergyTagView *)energyTagView {
    WEAKSELF
    if (!_energyTagView) {
        CGFloat width = 80;
        _energyTagView = [CCMyEnergyTagView instanceView];
        _energyTagView.frame = CGRectMake(App_Width -width, 30, width, 30);
        _energyTagView.imgView.layer.borderColor = WHITECOLOR.CGColor;
        _energyTagView.imgView.layer.borderWidth = 1;
        
        _energyTagView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            CCMyDaojuHistoryController *vc = [[CCMyDaojuHistoryController alloc]init];
            vc.userInfo = weakSelf.userInfoModel;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
        [_energyTagView addGestureRecognizer:tap];
    }
    return _energyTagView;
}
- (CCDaojuView *)daojuView {
    if (!_daojuView) {
        CGFloat height = 301;
        _daojuView = [[CCDaojuView alloc]initWithFrame:CGRectMake(0, App_Height , App_Width, height)];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(daojuTap:)];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(daojuTap:)];
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(daojuTap:)];

        [_daojuView.fangDaoWang addGestureRecognizer:tap];
        [_daojuView.feiLiao addGestureRecognizer:tap1];
        [_daojuView.jiaoShui addGestureRecognizer:tap2];
        [_daojuView.closeBtn addTarget:self action:@selector(daojuClose) forControlEvents:UIControlEventTouchUpInside];
    }
    return _daojuView;
}
#pragma mark - 按钮点击
- (void)addTopBtns {
    CGFloat width = 37;
    UIButton *buyBtn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width - self.energyTagView.width - width - 30, self.energyTagView.y, width, width)];
    [buyBtn setImage:IMAGENAMED(@"home_buy") forState:UIControlStateNormal];
    [buyBtn addTarget:self action:@selector(shopBtnClick) forControlEvents:UIControlEventTouchUpInside];
    buyBtn.layer.cornerRadius = width * 0.5;
    buyBtn.layer.masksToBounds = YES;
    [self.view addSubview:buyBtn];
    
    UIButton *readBtn = [[UIButton alloc]initWithFrame:CGRectMake( CGRectGetMinX(buyBtn.frame)-width - 10, self.energyTagView.y, width, width)];
    [readBtn setImage:IMAGENAMED(@"home_read") forState:UIControlStateNormal];
    [readBtn addTarget:self action:@selector(readBtnClick) forControlEvents:UIControlEventTouchUpInside];
    readBtn.layer.cornerRadius = width * 0.5;
    readBtn.layer.masksToBounds = YES;
    [self.view addSubview:readBtn];
    
    UIButton *shareBtn = [[UIButton alloc]initWithFrame:CGRectMake( CGRectGetMinX(readBtn.frame)-width - 10, self.energyTagView.y, width, width)];
    [shareBtn setImage:IMAGENAMED(@"share_btn") forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    shareBtn.layer.cornerRadius = width * 0.5;
    shareBtn.layer.masksToBounds = YES;
    [self.view addSubview:shareBtn];
}
//PK
- (void)pushToPkVc:(NSNotification *)noti {
    if (!self.isOpen) {
        SHOW(@"请先开通金豆");
        return;
    }
    
    MYUserInfoModel *model = (MYUserInfoModel *)noti.object;
    if ([model.rid isEqualToString:USERID]) {
        SHOW(@"不能PK自己哦～");
        return;
    }
    CCPKController *vc = [[CCPKController alloc]init];
    vc.uid = model.rid;
    [self.navigationController pushViewController:vc animated:YES];
}
//攻略
- (void)gonglueBtnClick {
    CCGongLueController *vc = [[CCGongLueController alloc]init];
    vc.userInfo = self.userInfoModel;
    [self.navigationController pushViewController:vc animated:YES];
}
//shop
- (void)shopBtnClick {
    CCMyShopController *vc = [[CCMyShopController alloc]init];
    vc.userInfo = self.userInfoModel;
    [self.navigationController pushViewController:vc animated:YES];
}
//read
- (void)readBtnClick {
    CCMyReadController *vc = [[CCMyReadController alloc]init];
    vc.userInfo = self.userInfoModel;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)shareBtnClick {
    CCShareADController *vc = [[CCShareADController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
//运动
- (void)sportBtnClick {
    CCMySportController *vc = [[CCMySportController alloc]init];
    vc.userInfo = self.userInfoModel;
    [self.navigationController pushViewController:vc animated:YES];
}
//豆市
- (void)doushiBtnClick {
    CCPriceController *vc = [[CCPriceController alloc]init];
    vc.userInfo = self.userInfoModel;
    vc.isOpen = self.isOpen;
    [self.navigationController pushViewController:vc animated:YES];
}
//能量机
- (void)nengliangjiBtnClick {
    CCEnergyController *vc = [[CCEnergyController alloc]init];
    vc.userInfo = self.userInfoModel;
    vc.fromJindou = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
//豆罐
- (void)douguanBtnClick {
    if (!self.isOpen) {
        SHOW(@"请先开通金豆");
        return;
    }
    CCDouGuanController *vc = [[CCDouGuanController alloc]init];
    vc.userInfo = self.userInfoModel;
    vc.isOpen = self.isOpen;
    [self.navigationController pushViewController:vc animated:YES];
}
//豆赛
- (void)dousai {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.urlStr = @"http://www.cct369.com/village/public/share/beansentry";
    vc.titleStr = @"豆赛";
//    CCDouSaiController *vc = [[CCDouSaiController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 分享
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    WEAKSELF
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [weakSelf sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    // 设置短信内容
    NSString *url = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    NSString *shareText = [@"想长寿，种金豆，金豆收一斗，能活九十九，走路能赚钱，种豆真好玩！注册地址：" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, url];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
    
}


- (CCShareView *)shareView {
    if (!_shareView) {
        _shareView = [CCShareView instanceView];
         _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.pengyouquanBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareView;
}
//分享
- (void)share {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height;
    }];
}
- (void)weixinBtnClick:(UIButton *)sender {
    [self shareCancelBtnClick];
    NSInteger tag = sender.tag;
    UMSocialPlatformType  platform = tag == 10 ? UMSocialPlatformType_WechatSession : UMSocialPlatformType_WechatTimeLine;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@""]]];
    NSString *text = platform == UMSocialPlatformType_WechatSession ? @"邀你种金豆！" : @"种金豆卖钱换奖品，还有每月三千保障金！";
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: text  descr:@"种金豆卖钱换奖品，还有每月三千保障金！" thumImage:IMAGENAMED(@"weixinIcon")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            [self updateInviteData];
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
    }];
}

- (void)updateInviteData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/invite") params:@{@"rid":USERID,} success:^(NSDictionary *success) {
        
    } failure:^(NSError *failure) {
        
    }];
    
}

#pragma mark - 道具
- (void)daojuBtnClick {
//    self.currentStage++;
//    [self startStageAnimation];
//    return;
    if (!self.isOpen) {
        SHOW(@"请先开通金豆");
        return;
    }
    if(self.daojuView.y < App_Height ){
        return;
    }
    [UIView animateWithDuration:0.75 animations:^{
        self.daojuView.y = self.daojuView.y - self.daojuView.height;
    }];
}
- (void)daojuClose {
    [UIView animateWithDuration:0.75 animations:^{
        self.daojuView.y = App_Height;
    }];
}
- (void)daojuTap:(UITapGestureRecognizer *)gesture {
    WEAKSELF
    NSInteger tag = gesture.view.tag;
    UIImage *img = tag == 100 ? IMAGENAMED(@"fangdaowang") : tag == 200 ? IMAGENAMED(@"huafei") : IMAGENAMED(@"jiaoshui");
    UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 165, 221)];
    view.userInteractionEnabled = YES;
    view.image = img;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [weakSelf.animateModel closeWithStraight];
        [weakSelf daojuClose];
        [weakSelf buyDaoju:tag];
    }];
    [view addGestureRecognizer:tap];
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}
- (void)showDaojuAnimationWithType:(NSInteger)tag {
    WEAKSELF
    UIImageView *successImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 309, 251)];
    if (tag == 100) {
        [self.headerView.treeView showBaohuzhaoAnimationWithFinishBlk:nil];
        SHOW(@"购买成功");
    }else if (tag == 200){
        [self.headerView.treeView showShifeiAnimationWithFinishBlk:^{
            [weakSelf.headerView.treeView showTreeShakeAnimation];
//            successImg.image = IMAGENAMED(@"popup_shifei");
//            [self.animateModel showWithModalView:successImg inView:App_Delegate.window];
        }];
    }else if (tag == 300){
        [self.headerView.treeView showJiaoshuiAnimationWithFinishBlk:^{
            [weakSelf.headerView.treeView showTreeShakeAnimation];
//            successImg.image = IMAGENAMED(@"popup_jiaoshui");
//            [self.animateModel showWithModalView:successImg inView:App_Delegate.window];
        }];
    }
}
//买道具
- (void)buyDaoju:(NSInteger)type {
    WEAKSELF
    [TOOL showLoading:self.view];
    self.currentDaojuType = type;
    NSString *typeStr = type == 100 ? @"防护栏" : type == 200 ? @"肥料" : type == 300 ? @"水" : @"" ;
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/prop_buy") params:@{@"rid":USERID,@"num":@"1",@"type":typeStr} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [weakSelf showDaojuAnimationWithType:weakSelf.currentDaojuType];
            [weakSelf useDaoju:weakSelf.currentDaojuType];
            if (type != 100) {
                weakSelf.userInfoModel.energy = [NSString stringWithFormat:@"%d",[weakSelf.userInfoModel.energy intValue] + 1000];
                    [weakSelf showEnergyToast];
                weakSelf.energyTagView.userInfo = weakSelf.userInfoModel;
            }
        }else{ //余额不足
            SHOW(@"余额不足，请充值");
            App_Delegate.daojuPaying = YES;
            weakSelf.navigationController.navigationBarHidden = NO;
            RechargeViewController *vc = [[RechargeViewController alloc]init];
            vc.money = @"1";
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
        [TOOL hideLoading:weakSelf.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:weakSelf.view];
    }];
}
- (void)rechargeSuccess {
    if(App_Delegate.daojuPaying){
        App_Delegate.daojuPaying = NO;
        [self.tableView scrollToTop];
        [self buyDaoju:self.currentDaojuType];
    }
}
//使用道具
- (void)useDaoju:(NSInteger)type {
    WEAKSELF
    NSString *typeStr = type == 100 ? @"防护栏" : type == 200 ? @"肥料" : @"水";
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/prop_use") params:@{@"rid":USERID,@"ta_id":@"0",@"type":typeStr} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSLog(@"%@",@"道具使用成功");
            weakSelf.needRefreshAniamtion = YES;
            [weakSelf loadPositionData];
        }
    } failure:^(NSError *failure) {
    }];
}
//我购买的道具
- (void)myDaoju {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/prop_list") params:@{@"rid":USERID} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            
        }
    } failure:^(NSError *failure) {
    }];
}
@end
