//
//  CCDouGuanController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCDouGuanController.h"
#import "CCPriceController.h"
#import "CCDouGuanCell.h"
@interface CCDouGuanController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UIImageView *backImgView;

@property (nonatomic, strong) UIView *emptyView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIButton *bottomBtn;
@end

@implementation CCDouGuanController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.statusBarWhite = YES;
    self.backImgView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:self.backImgView];
    self.backImgView.image = IMAGENAMED(@"douguan");
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, isIphoneX ? 40 : 20, 30, 30)];
    [backBtn setBackgroundImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    if (!self.isOpen) {
        [self.view addSubview:self.emptyView];
    }else{
        [self.view addSubview:self.containerView];
        [self.view addSubview:self.bottomBtn];
    }
    
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
    NSString *title = [NSString stringWithFormat:@"共%ld颗金豆",self.userInfo.bean_num];
    self.titleLbl.text = title;
}
#pragma mark  UICollectionView数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  self.userInfo.bean_num == 0 ? 1 : self.userInfo.bean_num;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
     CCDouGuanCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CCDouGuanCell" forIndexPath:indexPath];
    cell.layer.borderColor =  BACKGRAY.CGColor;
    cell.layer.borderWidth = 0.5;
    cell.isGrowing = self.userInfo.bean_num == 0;
    return cell;
}


- (void)goToDoushi {
    CCPriceController *vc = [[CCPriceController alloc]init];
    vc.userInfo = self.userInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc]initWithFrame:CGRectMake(20, 200, App_Width - 40, App_Width - 40)];
        _containerView.backgroundColor = WHITECOLOR;
        _containerView.layer.cornerRadius = 10;
        _containerView.layer.masksToBounds = YES;
        NSString *title = [NSString stringWithFormat:@"共%ld颗金豆",self.userInfo.bean_num];
        self.titleLbl = [[UILabel alloc]initWithText:title font:15 textColor:TEXTBLACK3];
        self.titleLbl.textAlignment = NSTextAlignmentCenter;
        [_containerView addSubview:self.titleLbl];
        self.titleLbl.frame = CGRectMake(10, 0, _containerView.width - 20, 40);
        if (self.isOpen) {
            [_containerView addSubview:self.collectionView];
            self.collectionView.frame = CGRectMake(0, 40, _containerView.width, _containerView.height - 40);
        }
        
    }
    return _containerView;
}
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CGFloat width =  (App_Width - 40 - 20) / 3.0;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(width, width - 10);
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame: CGRectMake(0, 40, _containerView.width, _containerView.height - 40) collectionViewLayout:layout];
        collectionView.backgroundColor = WHITECOLOR;
        self.collectionView = collectionView;
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        [self.collectionView registerNib:[UINib nibWithNibName:@"CCDouGuanCell" bundle:nil] forCellWithReuseIdentifier:@"CCDouGuanCell"];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
    }
    return _collectionView;
}
- (UIButton *)bottomBtn {
    if (!_bottomBtn) {
        _bottomBtn = [[UIButton alloc]initWithTitle:nil textColor:nil backImg:@"dou_jiaoyi" font:12];
        _bottomBtn.frame = CGRectMake((App_Width - 189)*0.5, CGRectGetMaxY(self.containerView.frame)+10,  189 , 80);
        [_bottomBtn addTarget:self action:@selector(goToDoushi) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bottomBtn;
}
- (UIView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[UIView alloc]initWithFrame:CGRectMake(20, 230, App_Width - 40, App_Width - 40)];
        _emptyView.backgroundColor = WHITECOLOR;
        _emptyView.layer.cornerRadius = 10;
        _emptyView.layer.masksToBounds = YES;
        
        UILabel *lbl = [[UILabel alloc]initWithText:@"还没有金豆哦\n快去豆市买豆种吧" font:15 textColor:TEXTBLACK3];
        [_emptyView addSubview:lbl];
        lbl.frame = CGRectMake(0, 100, App_Width - 40, 50);
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn = [[UIButton alloc]initWithTitle:@"" textColor:WHITECOLOR backImg:@"dou_buy" font:15];
        [btn addTarget:self action:@selector(goToDoushi) forControlEvents:UIControlEventTouchUpInside];
        btn.frame = CGRectMake((_emptyView.width - 131)*0.5, CGRectGetMaxY(lbl.frame)+ 20, 131, 61);
        [_emptyView addSubview:btn];
    }
    return _emptyView;
}

@end
