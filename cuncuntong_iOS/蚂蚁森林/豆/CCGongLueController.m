//
//  CCGongLueController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCGongLueController.h"
#import "CCMySportController.h"
#import "CCMyReadController.h"
#import "CCMyInviteController.h"
#import "CCMyShopController.h"
#import "CCMyCreditController.h"
#import "CCShareADController.h"
@interface CCGongLueController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@end

@implementation CCGongLueController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.statusBarWhite = YES;
    self.navigationController.navigationBarHidden = YES;
    self.scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    self.scrollView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);

    [self.view addSubview:self.scrollView];
    
    UIImageView *imgView = [[UIImageView alloc]init];
    [self.scrollView addSubview:imgView];
//    if (App_Width == 375) {
        imgView.image = IMAGENAMED(@"gongLue_8s");
        imgView.frame = CGRectMake(0, 0, App_Width, 4167.0 / 750.0 * App_Width);
//    }else{
//        imgView.image = IMAGENAMED(@"gongLue_8p");
//        imgView.frame = CGRectMake(0, 0, App_Width, 5632.0 / 1080.0 * App_Width);
//    }
    self.scrollView.contentSize = CGSizeMake(imgView.width, imgView.height);
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 30, 30, 30)];
    [backBtn setBackgroundImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    [self addBtns];
    
    if (@available(iOS 11.0, *)) {
        _scrollView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
}

- (void)addBtns {
    UIButton *firstBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 500, App_Width*0.5, 60)];
    firstBtn.backgroundColor = [UIColor clearColor];
    firstBtn.tag = 1;
    [firstBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    firstBtn.alpha = 0.5;
    [self.scrollView addSubview:firstBtn];
    
    UIButton *secondBtn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width * 0.5, 500, App_Width*0.5, 60)];
    secondBtn.tag = 2;
    secondBtn.backgroundColor = [UIColor clearColor];
    secondBtn.alpha = 0.5;
    [secondBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:secondBtn];
    
    UIButton *thirdBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 570, App_Width*0.5, 60)];
    thirdBtn.tag = 3;
    thirdBtn.backgroundColor = [UIColor clearColor];
    [thirdBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    thirdBtn.alpha = 0.5;
    [self.scrollView addSubview:thirdBtn];
    
    UIButton *fourthBtn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width * 0.5, 570, App_Width*0.5, 60)];
    fourthBtn.tag = 4;
    fourthBtn.backgroundColor = [UIColor clearColor];
    [fourthBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    fourthBtn.alpha = 0.5;
    [self.scrollView addSubview:fourthBtn];
    
    UIButton *fiveBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 630, App_Width*0.5, 70)];
    fiveBtn.tag = 5;
    fiveBtn.backgroundColor = [UIColor clearColor];
    [fiveBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    fiveBtn.alpha = 0.5;
    [self.scrollView addSubview:fiveBtn];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width * 0.5 - 10, 630, 346*0.45, 139*0.45)];
    [btn addTarget:self action:@selector(lifeCircle) forControlEvents:UIControlEventTouchUpInside];
//    [btn setImage:IMAGENAMED(@"gonglue_circle") forState:UIControlStateNormal];
    [self.scrollView addSubview:btn];
}

- (void)lifeCircle {
    CCShareADController *vc = [[CCShareADController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)btnClick:(UIButton *)sender {
    NSInteger tag = sender.tag;
    if (tag == 1) { //运动
        CCMySportController *vc = [[CCMySportController alloc]init];
        vc.userInfo = self.userInfo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (tag == 2){//购物
        CCMyShopController *vc = [[CCMyShopController alloc]init];
        vc.userInfo = self.userInfo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (tag == 3){//阅读
        CCMyReadController *vc = [[CCMyReadController alloc]init];
        vc.userInfo = self.userInfo;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if(tag == 4){//邀请
        CCMyInviteController *vc =[[CCMyInviteController alloc]init];
        vc.userInfo = self.userInfo;
        [self.navigationController pushViewController:vc animated:YES];
    }else{//信用
        CCMyCreditController *vc = [[CCMyCreditController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
@end
