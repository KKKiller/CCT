//
//  CCPriceController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCPriceController.h"
#import "NSMutableAttributedString+Style.h"
#import "CCMyEnergyTagView.h"
#import "BRPickerView.h"
#import "CCMyStockController.h"
#import "WalletViewController.h"
#import "RechargeViewController.h"
#import "MYHomeController.h"
#import "CCConstantModel.h"
#import "RecordViewController.h"
#import "WithdrawViewController.h"
@interface CCPriceController ()<UIAlertViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CCMyEnergyTagView *energyTagView;
@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) CCConstantModel *constantModel;
@end

@implementation CCPriceController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.statusBarWhite = YES;
    self.navigationController.navigationBarHidden = YES;
    self.scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    self.scrollView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);

    UIImageView *imgView = [[UIImageView alloc]init];
    [self.scrollView addSubview:imgView];
    imgView.image = IMAGENAMED(@"doushi_8s");
    imgView.frame = CGRectMake(0, 0, App_Width, 2504.0 / 1242.0 * App_Width);
    self.scrollView.contentSize = CGSizeMake(imgView.width, imgView.height);
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, isIphoneX ? 40 : 20, 30, 30)];
    [backBtn setBackgroundImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    [self addTopView];
    [self addBtns];
    [self.view addSubview:self.energyTagView];
    self.energyTagView.userInfo = self.userInfo;
    self.energyTagView.hidden = !self.isOpen;
    
    [NOTICENTER addObserver:self selector:@selector(rechargeSuccess) name:@"PaySuccess" object:nil];
    [self loadPrice];
    [self loadConstant];
    
    [self loadAwardBtn];
}
- (void)loadAwardBtn {
//    CGFloat widht = 60;
//    UIButton *awardBtn  = [[UIButton alloc]initWithFrame:CGRectMake(App_Width - widht - 15, App_Height - widht - 15, widht, widht)];
    CGFloat ratio = App_Width / 1242.0 ;
    UIButton *awardBtn  = [[UIButton alloc]initWithFrame: CGRectMake(120 * ratio, 1554 * ratio, 986 * ratio, 262*ratio)];
    [self.scrollView addSubview:awardBtn];
    [awardBtn setTitle:@"领取奖品" forState:UIControlStateNormal];
    awardBtn.titleLabel.font = FFont(20);
    [awardBtn setBackgroundColor:MTRGB(0x366502)];
    awardBtn.layer.cornerRadius = 10;
    awardBtn.layer.masksToBounds = YES;
//    awardBtn.alpha = 0.8;
    [awardBtn addTarget:self action:@selector(awardBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)awardBtnClick {
    self.navigationController.navigationBarHidden = NO;
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/bean/exchange?rid=%@",USERID];
    vc.titleStr = @"领取奖品";
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)loadPrice {
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/constants") params:@{} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSString *paijia = [NSString stringWithFormat:@"%@", success[@"data"][@"paijia"]];
            NSString *text = [NSString stringWithFormat:@"￥%@/每豆",paijia];
            NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithText:text color:WHITECOLOR font:FFont(12) subStrIndex:NSMakeRange(1, 2) subStrColor:WHITECOLOR font2:FFont(30)];
            self.priceLbl.attributedText = attrStr;
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)loadConstant {
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/constants") params:@{} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            
            self.constantModel = [CCConstantModel modelWithJSON:success[@"data"]];
            
        }
    } failure:^(NSError *failure) {
    }];
}

- (void)addTopView {
    UILabel *titleLbl = [[UILabel alloc]initWithText:@"今日豆价" font:20 textColor:WHITECOLOR];
    [self.scrollView addSubview:titleLbl];
    titleLbl.frame = CGRectMake(0, 30, App_Width, 30);
    titleLbl.textAlignment = NSTextAlignmentCenter;
    
    
    UILabel *priceLbl = [[UILabel alloc]init];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithText:@"￥10/每豆" color:WHITECOLOR font:FFont(12) subStrIndex:NSMakeRange(1, 2) subStrColor:WHITECOLOR font2:FFont(30)];
    priceLbl.attributedText = attrStr;
    self.priceLbl = priceLbl;
    [self.scrollView addSubview:priceLbl];
    priceLbl.frame = CGRectMake(0, 70, App_Width, 30);
    priceLbl.textAlignment = NSTextAlignmentCenter;
}
- (void)addBtns {
    UIButton *firstBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 230, App_Width, 70)];
    firstBtn.backgroundColor = [UIColor clearColor];
    firstBtn.tag = 1;
    [firstBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    firstBtn.alpha = 0.5;
    [self.scrollView addSubview:firstBtn];
    
    UIButton *secondBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 320, App_Width, 70)];
    secondBtn.tag = 2;
    secondBtn.backgroundColor = [UIColor clearColor];
    secondBtn.alpha = 0.5;
    [secondBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:secondBtn];
//
//    UIButton *thirdBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 400, App_Width, 70)];
//    thirdBtn.tag = 3;
//    thirdBtn.backgroundColor = [UIColor clearColor];
//    [thirdBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    thirdBtn.alpha = 0.5;
//    [self.scrollView addSubview:thirdBtn];
}

- (void)btnClick:(UIButton *)sender {
    NSInteger tag = sender.tag;
    if (tag == 1) {// 出售金豆
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"为防止作弊者资金转移，出售的金豆即日起将直接提现，提现的账号为绑定的支付宝账号，未绑定支付宝的无法出售！" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSMutableArray *numArray = [NSMutableArray array];
            for (int i = 0; i< 30; i++) {
                [numArray addObject:[NSString stringWithFormat:@"%@",@(i+1)]];
            }
            [BRStringPickerView showStringPickerWithTitle:@"选择出售金豆数量" dataSource:numArray defaultSelValue:@"1" isAutoSelect:NO resultBlock:^(NSString* selectValue) {
                [self sail:selectValue];
            }];
        }];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if (tag == 2){//兑换
        NSMutableArray *numArray = [NSMutableArray array];
        for (int i = 0; i< 30; i++) {
            [numArray addObject:[NSString stringWithFormat:@"%@",@((i+1)*3)]];
        }
        [BRStringPickerView showStringPickerWithTitle:@"选择兑换金豆数量" dataSource:numArray defaultSelValue:@"3" isAutoSelect:NO resultBlock:^(NSString* selectValue) {
            [self duihuan:selectValue];
        }];
    }else if (tag == 3){//种豆
//        [self zhongdou];
    }
}
- (void)sail:(NSString *)num {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/store_sell") params:@{@"rid":USERID,@"num":num} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSString *text = [NSString stringWithFormat:@"出售金豆成功,出售金额将提现至支付宝账号：%@，24小时内到账",[UserManager sharedInstance].mobile];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:text delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"查看提现记录", nil];
            [alert show];
            self.userInfo.bean_num -= 1;
            
//            self.navigationController.navigationBarHidden = NO;
//            WalletViewController *vc = [[WalletViewController alloc]init];
//            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
//查看提现记录
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        RecordViewController *vc = [[RecordViewController alloc]init];
        vc.recordType = RecordTypeWithdraw;
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)duihuan:(NSString *)num {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/store_exchange") params:@{@"rid":USERID,@"num":num} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"兑换会员股成功");
            self.navigationController.navigationBarHidden = NO;
            CCMyStockController *vc = [[CCMyStockController alloc]init];
            vc.isProfileStock = YES;
            [self.navigationController pushViewController:vc animated:YES];
            self.userInfo.bean_num -= [num integerValue];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
-(void)zhongdou {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/create") params:@{@"rid":USERID,@"num":@"1"} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"种豆成功");
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[MYHomeController class]]) {
                    MYHomeController *vc = (MYHomeController *)controller;
                    [self.navigationController popToViewController:vc animated:YES];
                }
            }
        }else{
            NSString *msg = success[@"msg"];
            if([msg containsString:@"已经购买过"]){
                SHOW(msg);
            }else{
                self.navigationController.navigationBarHidden = NO;
                RechargeViewController *vc = [[RechargeViewController alloc]init];
                vc.money = self.constantModel ? [NSString stringWithFormat:@"%ld",self.constantModel.bean_price] :  @"10";
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
- (void)rechargeSuccess {
    [self zhongdou];
}

- (CCMyEnergyTagView *)energyTagView {
    if (!_energyTagView) {
        CGFloat width = 70;
        _energyTagView = [CCMyEnergyTagView instanceView];
        _energyTagView.frame = CGRectMake(App_Width -width, 30, width, 35);
        _energyTagView.imgView.layer.borderColor = WHITECOLOR.CGColor;
        _energyTagView.imgView.layer.borderWidth = 1;
    }
    return _energyTagView;
}
@end
