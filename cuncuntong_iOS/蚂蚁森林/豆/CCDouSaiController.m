//
//  CCDouSaiController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCDouSaiController.h"

@interface CCDouSaiController ()
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation CCDouSaiController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    self.scrollView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);

    UIImageView *imgView = [[UIImageView alloc]init];
    [self.scrollView addSubview:imgView];
    if (App_Width == 375) {
        imgView.image = IMAGENAMED(@"dousai_8");
        imgView.frame = CGRectMake(0, 0, App_Width, 2295.0 /750.0 * App_Width);
    }else{
        imgView.image = IMAGENAMED(@"dousai_8s");
        imgView.frame = CGRectMake(0, 0, App_Width, 3305.0 / 1080.0 * App_Width);
    }
    self.scrollView.contentSize = CGSizeMake(imgView.width, imgView.height);
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 20, 30, 30)];
    [backBtn setBackgroundImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
//    [self addBtns];
}

- (void)addBtns {
    UIButton *firstBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 170, App_Width, 90)];
    firstBtn.backgroundColor = [UIColor purpleColor];
    firstBtn.tag = 1;
    [firstBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    firstBtn.alpha = 0.5;
    [self.scrollView addSubview:firstBtn];
    
    UIButton *secondBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 265, App_Width, 90)];
    secondBtn.tag = 2;
    secondBtn.backgroundColor = [UIColor purpleColor];
    secondBtn.alpha = 0.5;
    [secondBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:secondBtn];
    
    UIButton *thirdBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 365, App_Width, 90)];
    thirdBtn.tag = 3;
    thirdBtn.backgroundColor = [UIColor purpleColor];
    [thirdBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    thirdBtn.alpha = 0.5;
    [self.scrollView addSubview:thirdBtn];
}

- (void)btnClick:(UIButton *)sender {
    NSInteger tag = sender.tag;
    if (tag == 1) {
        
    }else if (tag == 2){
        
    }else if (tag == 3){
        
    }
}
@end
