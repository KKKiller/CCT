//
//  CCMyDaojuHistoryController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/8/4.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYUserInfoModel.h"
@interface CCMyDaojuHistoryController : BaseViewController
@property (nonatomic, strong) MYUserInfoModel *userInfo;
@end
