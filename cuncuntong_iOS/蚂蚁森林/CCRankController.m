//
//  CCRankController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCRankController.h"
#import "MYRankModel.h"
#import "MYHomeCell.h"
#import "CCPKController.h"
static NSString *kCellId = @"homeCell";
@interface CCRankController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *header;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;
@end

@implementation CCRankController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"排行榜"];
    self.navigationController.navigationBarHidden = NO;
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [self initRefreshView];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)loadData {
    [TOOL showLoading:self.view];
    NSString *type = self.type == 0 ? @"all" : self.type == 1 ? @"province_id" : @"area_id";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:@{@"type":type,@"page":@(self.pageIndex),@"perpage":@"30"}];
    if (self.locationID) {
        [dict setValue:self.locationID forKey:@"type_val"];
    }
    if (self.isOpen) {
        [dict setValue:USERID forKey:@"rid"];
    }
                                                                                
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"bean/rank") params:dict success:^(NSDictionary *success) {
        if ([success[@"error"]integerValue] == 0) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"data"][@"data"]) {
                MYRankModel *model = [MYRankModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
        }
        [self endLoding:success[@"data"][@"data"]];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    [TOOL hideLoading:self.view];
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MYHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"homeCell"];
    MYRankModel *model = self.dataArray[indexPath.row];
    model.index = indexPath.row;
    cell.model = model;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MYRankModel *model = self.dataArray[indexPath.row];
    if ([model.rid isEqualToString:USERID]) {
        SHOW(@"不能PK自己哦～");
        return;
    }
    if (!self.isOpen) {
        SHOW(@"请先开通金豆");
        return;
    }
    CCPKController *vc = [[CCPKController alloc]init];
    vc.uid = model.rid;
    [self.navigationController pushViewController:vc animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        CGFloat y = kSystemVersion >= 11.0 ? 0 : 64;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, y, App_Width, App_Height - y) style:UITableViewStylePlain];
//        _tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 44;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"MYHomeCell" bundle:nil] forCellReuseIdentifier:kCellId];

    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

@end
