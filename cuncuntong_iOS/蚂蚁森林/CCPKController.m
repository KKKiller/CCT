//
//  CCRKController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCPKController.h"
#import "CCPKHeader.h"
#import "CCPKCell.h"
#import "MYPKModel.h"
#import "MYUserInfoModel.h"
#import "MYEnergyModel.h"
#import "CCMyEnergyTagView.h"
#import "CCConstantModel.h"
#import "RechargeViewController.h"
#import "CCChatAddFriendController.h"
#import "CCMyDaojuHistoryController.h"
static NSString *kCellId = @"CCPKCell";
@interface CCPKController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCPKHeader *headerView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) MYUserInfoModel *myUserInfo;
@property (nonatomic, strong) MYUserInfoModel *taUserInfo;
@property (nonatomic, strong) NSString *myGetEnergy;
@property (nonatomic, strong) NSString *taGetEnergy;
@property (nonatomic, strong) MYEnergyModel *energyModel;
@property (nonatomic, strong) CCMyEnergyTagView *energyTagView;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) CCConstantModel *constantModel;
@property (nonatomic, strong) UIButton *chatBtn;

@property (nonatomic, assign) BOOL hasShowAnimation;
@property (nonatomic, assign) BOOL viewDidAppear;
@property (nonatomic, assign) TreeGrowingStage currentStage;
@property (nonatomic, assign) NSInteger currentDaojuType;

@end

@implementation CCPKController

- (void)viewDidLoad {
    [super viewDidLoad];
    [NOTICENTER addObserver:self selector:@selector(rechargeSuccess) name:@"PaySuccess" object:nil];
    [self setTitle:@""];
    self.pageIndex = 1;
    self.currentStage  = -1;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.backBtn];
    [self.view addSubview:self.energyTagView];
    [self.view addSubview:self.chatBtn];
     [NOTICENTER addObserver:self selector:@selector(rechargeSuccess) name:@"PaySuccess" object:nil];
    [self initRefreshView];
    [self loadData];
    [self loadEnergy];
    
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.viewDidAppear = YES;
    [self startStageAnimation];
}
//成长动画
- (void)startStageAnimation{
    if (!self.hasShowAnimation && self.currentStage >= 0 && self.viewDidAppear) {
        self.hasShowAnimation = YES;
        [self.headerView.treeView beginBirdFlyAnimation];//小鸟动画
        if (self.energyModel.beanArray.count > 0) {
            if (self.currentStage == TreeGrowingStageKaihua) {
                self.currentStage = TreeGrowingStageYouhuayouguo;
            }else{
                self.currentStage = TreeGrowingStageHuangguo;
            }
        }
        [self.headerView.treeView beginTreeGrwoingAnimationWithStage:self.currentStage];//成长动画
        [self.headerView.treeView bubbleAnimation];//气泡动画
    }
}
- (void)refreshAnimation {
    self.hasShowAnimation = NO;
    self.currentStage = [self.constantModel getCurrentStageWithEnergy:[self.taUserInfo.energy integerValue]];
    [self startStageAnimation];
}
- (void)loadData {
    self.hasShowAnimation = NO;
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/friendinfo") params:@{@"ta_id":STR(self.uid),@"rid":USERID} success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            [self.dataArray removeAllObjects];
            self.myUserInfo = [MYUserInfoModel modelWithJSON:success[@"data"][@"userinfo"]];
            self.taUserInfo = [MYUserInfoModel modelWithJSON:success[@"data"][@"tainfo"]];
            [self loadConstant];
            //保护罩动画
            if (self.taUserInfo.isFangDaoWangEnable) {
                [self.headerView.treeView showBaohuzhaoAnimationWithFinishBlk:nil];
                self.headerView.treeView.baohuzhaoTime = self.myUserInfo.fangDaoWangShowTime;
            }
            self.energyTagView.userInfo = self.taUserInfo;
            self.headerView.myInfo = self.myUserInfo;
            self.headerView.taInfo = self.taUserInfo;
            if ([success[@"data"][@"pk"] isKindOfClass:[NSDictionary class]]) {
                self.myGetEnergy = success[@"data"][@"pk"][@"energy"];
                self.taGetEnergy = success[@"data"][@"pk"][@"energy2"];
                
                NSString *myEnergy = [self.myGetEnergy integerValue] > 9999 ? [NSString stringWithFormat:@"%.1f万g",[self.myGetEnergy integerValue] / 10000.0] : [NSString stringWithFormat:@"%@g",self.myGetEnergy];
                NSString *taEnergy = [self.taGetEnergy integerValue] > 9999 ? [NSString stringWithFormat:@"%.1f万g",[self.taGetEnergy integerValue] / 10000.0] : [NSString stringWithFormat:@"%@g",self.taGetEnergy];
                self.headerView.pkView.myEnergyLbl.text = myEnergy;
                self.headerView.pkView.heEnergyLbl.text = taEnergy;
                self.headerView.pkView.isIWin = [self.myGetEnergy integerValue] >= [self.taGetEnergy integerValue];
            }
            for (NSDictionary *dict in success[@"data"][@"logs"]) {
                MYPKModel *model = [MYPKModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
        }
        [self endLoding:nil];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}

- (void)loadEnergy {
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/check") params:@{@"rid":STR(self.uid)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.energyModel = [[MYEnergyModel alloc]initWithDict:success[@"data"]];
            self.headerView.treeView.energyModel = self.energyModel;
            self.headerView.treeView.canPickJindou = NO;
            [self.tableView reloadData];
        }
    } failure:^(NSError *failure) {
        
    }];
}
//结果常量
- (void)loadConstant {
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/constants") params:@{} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.constantModel = [CCConstantModel modelWithJSON:success[@"data"]];
            self.currentStage = [self.constantModel getCurrentStageWithEnergy:[self.taUserInfo.energy integerValue]];
            [self startStageAnimation];
        }
    } failure:^(NSError *failure) {
    }];
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [TOOL hideLoading:self.view];
    [self.tableView reloadData];
    [self.tableView.mj_footer endRefreshingWithNoMoreData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCPKCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
    MYPKModel *model = self.dataArray[indexPath.row];
    model.name = self.taUserInfo.nickname;
    cell.model = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
//聊天
- (void)chatBtnClick{
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"chat/friend_mine") params:@{@"uid":USERID,@"f_uid":STR(self.uid)} target:self success:^(NSDictionary *success) {
        if ([success[@"stat"] integerValue] == 3) { //是我的好友
            if (![self.uid isEqualToString:USERID]) {
                CCChatController *vc = [[CCChatController alloc]initWithConversationType:ConversationType_PRIVATE targetId:self.uid];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else{
            CCChatAddFriendController *vc = [[CCChatAddFriendController alloc]init];
            vc.uid = self.uid;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSError *failure) {
    }];
}

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, -20, App_Width, App_Height + 20) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 44;
        _tableView.tableHeaderView = self.headerView;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        [_tableView registerNib:[UINib nibWithNibName:@"CCPKCell" bundle:nil] forCellReuseIdentifier:kCellId];
        
    }
    return _tableView;
}
- (UIButton *)chatBtn {
    if (!_chatBtn) {
        _chatBtn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width - 60, App_Height - 60, 40, 40)];
        [_chatBtn setImage:IMAGENAMED(@"chat_mayi") forState:UIControlStateNormal];
        [_chatBtn addTarget:self action:@selector(chatBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatBtn;
}
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, isIphoneX ? 40 : 20, 30, 30)];
        [_backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [_backBtn setImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    }
    return _backBtn;
}
- (CCMyEnergyTagView *)energyTagView {
    if (!_energyTagView) {
        CGFloat width = 65;
        _energyTagView = [CCMyEnergyTagView instanceView];
        _energyTagView.frame = CGRectMake(App_Width -width, 25, width, 30);
        _energyTagView.imgView.layer.borderColor = WHITECOLOR.CGColor;
        _energyTagView.imgView.layer.borderWidth = 1;
        
    }
    return _energyTagView;
}
- (CCPKHeader *)headerView {
    if (!_headerView) {
        CGFloat height = App_Width  * 1746.0 / 1242.0 + 210 ;
        _headerView = [[CCPKHeader alloc]initWithFrame:CGRectMake(0, 0, App_Width, height)];
        WEAKSELF;
        [_headerView.treeView.sportTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"运动"];
        }];
        [_headerView.treeView.readTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"阅读"];
        }];
        [_headerView.treeView.redTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"红包"];
        }];
        [_headerView.treeView.inviteTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"邀请"];
        }];
        [_headerView.treeView.shopTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"购物"];
        }];
        [_headerView.treeView.gongxiang_tap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"生活圈充值"];
        }];
        [_headerView.treeView.creditTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"信用值"];
        }];
        [_headerView.treeView.share_tap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"分享"];
        }];
        [_headerView.treeView.tuijianTap addActionBlock:^(id  _Nonnull sender) {
            [weakSelf stealEnergy:@"推荐"];
        }];
        [_headerView.treeView.jiaoshuiBtn addTarget:self action:@selector(jiaoshui) forControlEvents:UIControlEventTouchUpInside];
        _headerView.treeView.canPickJindou = NO;
    }
    return _headerView;
}
#pragma mark - 浇水
- (void)jiaoshui {
    
    UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 165, 221)];
    view.userInteractionEnabled = YES;
    view.image = IMAGENAMED(@"jiaoshui");
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self.animateModel closeWithStraight];
        [self buyDaoju:300];
    }];
    [view addGestureRecognizer:tap];
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}
//买道具
- (void)buyDaoju:(NSInteger)type {
    [TOOL showLoading:self.view];
    self.currentDaojuType = type;
    NSString *typeStr = type == 100 ? @"防护栏" : type == 200 ? @"肥料" : @"水";
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/prop_buy") params:@{@"rid":USERID,@"num":@"1",@"type":typeStr} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [self showDaojuAnimationWithType:type];
            [self useDaoju:type];
        }else{ //余额不足
            SHOW(@"余额不足，请充值");
            self.navigationController.navigationBarHidden = NO;
            RechargeViewController *vc = [[RechargeViewController alloc]init];
            vc.money = @"1";
            [self.navigationController pushViewController:vc animated:YES];
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
- (void)showDaojuAnimationWithType:(NSInteger)tag {
    UIImageView *successImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 309, 251)];
    if (tag == 100) {
        [self.headerView.treeView showBaohuzhaoAnimationWithFinishBlk:nil];
        SHOW(@"购买成功");
    }else if (tag == 200){
        [self.headerView.treeView showShifeiAnimationWithFinishBlk:^{
            successImg.image = IMAGENAMED(@"popup_shifei");
            [self.animateModel showWithModalView:successImg inView:App_Delegate.window];
        }];
    }else if (tag == 300){
        [self.headerView.treeView showJiaoshuiAnimationWithFinishBlk:^{
            [self.headerView.treeView showTreeShakeAnimation];
            
            [self refreshAnimation];
//            successImg.image = IMAGENAMED(@"popup_jiaoshui");
//            [self.animateModel showWithModalView:successImg inView:App_Delegate.window];
        }];
    }
}
- (void)rechargeSuccess {
    [self.tableView scrollToTop];
    [self buyDaoju:self.currentDaojuType];
}

//使用道具
- (void)useDaoju:(NSInteger)type {
    NSString *typeStr = type == 100 ? @"防护栏" : type == 200 ? @"肥料" : @"水";
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/prop_use") params:@{@"rid":USERID,@"ta_id":@"0",@"type":typeStr} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSLog(@"%@",@"道具使用成功");
        }
    } failure:^(NSError *failure) {
    }];
}

- (void)stealEnergy:(NSString *)type {
    if (self.taUserInfo.isFangDaoWangEnable) {
        SHOW(@"防护罩保护中");
        return;
    }
    NSString *id = @"0";
    NSInteger index = 0;
    if ([type isEqualToString:@"邀请"] ) {
        if (self.energyModel.inviteIdArray.count > 0) {
            index = arc4random() %  self.energyModel.inviteIdArray.count;
            if (self.energyModel.inviteIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.inviteIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以偷取");
            return;
        }
    }
    if ([type isEqualToString:@"购物"] ) {
        if (self.energyModel.buyIdArray.count > 0) {
            index = arc4random() %  self.energyModel.buyIdArray.count;
            if (self.energyModel.buyIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.buyIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以偷取");
            return;
        }
    }
    if ([type isEqualToString:@"红包"] ) {
        if (self.energyModel.redIdArray.count > 0) {
            index = arc4random() %  self.energyModel.redIdArray.count;
            if (self.energyModel.redIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.redIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以偷取");
            return;
        }
    }
    if ([type isEqualToString:@"能量机"] ) {
        if (self.energyModel.nljIdArray.count > 0) {
            index = arc4random() %  self.energyModel.nljIdArray.count;
            if (self.energyModel.nljIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.nljIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"生活圈充值"] ) {
        if (self.energyModel.spIdArray.count > 0) {
            index = arc4random() %  self.energyModel.spIdArray.count;
            if (self.energyModel.spIdArray.count > index) {
                MYSubEnergyModel *model = self.energyModel.spIdArray[index];
                id = model.id;
            }
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"推荐"] ) {
        if (self.energyModel.tjArray.count > 0) {
            MYSubEnergyModel *model = self.energyModel.tjArray[0];
            id = model.id;
        }else{
            SHOW(@"暂无能量可以收取");
            return;
        }
    }
    if ([type isEqualToString:@"分享"] ) {
        if (self.energyModel.share_energy_today > 0 && self.energyModel.share_energy_today_remain > 0) {
            SHOW(@"未到收取时间");
            return;
        }
    }
    if ([type isEqualToString:@"信用值"] ) {
        if (self.energyModel.credit_energy > 0 && self.energyModel.credit_energy_remain > 0) {
            SHOW(@"未到收取时间");
            return;
        }
    }
    if ([type isEqualToString:@"运动"]) {
        if (self.energyModel.sport_energy_today > 0 && self.energyModel.sport_energy == 0) {
            SHOW(@"今日能量需明日才可偷取");
            return;
        }
    }
    if ([type isEqualToString:@"阅读"]) {
        if (self.energyModel.read_energy_today > 0 && self.energyModel.read_energy == 0) {
            SHOW(@"今日能量需明日才可偷取");
            return;
        }
    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/energy_pick") params:@{@"rid":USERID,@"type":type,@"ta_id":STR(self.uid),@"id":id} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"偷取成功");
            if ([type isEqualToString:@"邀请"]) {
                [self.energyModel.inviteIdArray removeObjectAtIndex:index];
            }
            if ([type isEqualToString:@"购物"]) {
                [self.energyModel.buyIdArray removeObjectAtIndex:index];
            }
            if ([type isEqualToString:@"生活圈充值"]) {
                [self.energyModel.spIdArray removeObjectAtIndex:index];
            }
            if ([type isEqualToString:@"能量机"]) {
                [self.energyModel.nljIdArray removeObjectAtIndex:index];
            }
            self.hasShowAnimation = NO;
            [self loadData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}

@end
