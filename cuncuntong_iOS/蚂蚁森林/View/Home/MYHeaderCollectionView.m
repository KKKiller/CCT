//
//  MYHeaderCollectionView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHeaderCollectionView.h"
#import "MYHomeHeaderCollectionViewCell.h"
@interface MYHeaderCollectionView()<UICollectionViewDelegate,UICollectionViewDataSource>
@end
@implementation MYHeaderCollectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setCollectionView];
        
    }
    return self;
}
-(void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
}
#pragma mark  UICollectionView数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MYHomeHeaderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"homeCollectionView" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

#pragma mark  collectionView代理方法,添加照片
//点击collectionView跳转到相册
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MYUserInfoModel *model = self.dataArray[indexPath.row];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"pushToPkVc" object:model];
}

- (void)setCollectionView{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(152, 184);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
    collectionView.backgroundColor = WHITECOLOR;
    collectionView.contentInset = UIEdgeInsetsMake(0, 5, 0, 0);
    self.collectionView = collectionView;
    [self.collectionView registerNib:[UINib nibWithNibName:@"MYHomeHeaderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"homeCollectionView"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self addSubview:self.collectionView];
}

@end
