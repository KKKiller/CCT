//
//  CCHomeHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHomeHeaderView.h"

@implementation MYHomeHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
    }
    return self;
}

- (void)setUI {
    self.treeView = [MYHomeTreeView instanceView];
    [self addSubview:self.treeView];
    
    self.collectionView = [[MYHeaderCollectionView alloc]initWithFrame:CGRectMake(0, self.height - 200, App_Width, 200)];
    [self addSubview:self.collectionView];
    self.collectionView.hidden = YES;
    self.treeView.visitLbl.hidden = YES;
    
    self.goBtn = [[UIButton alloc]init];
    [self addSubview:self.goBtn];
    [self.goBtn setImage:IMAGENAMED(@"go") forState:UIControlStateNormal];
    [self.goBtn setContentMode:UIViewContentModeScaleAspectFit];
    [self.goBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    
    [self layoutSubview];
}
- (void)layoutSubview {
    CGFloat treeHeight = App_Width * 1746.0 / 1242.0;
    self.treeView.treeViewHeight.constant = treeHeight;
    [self.treeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(treeHeight + 100);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.treeView.mas_bottom).offset(0);
        make.height.mas_equalTo(185);
    }];
    [self.goBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.collectionView.mas_bottom).offset(20);
        make.height.mas_equalTo(70);
    }];
}

- (void)setVisitors:(NSArray *)visitors {
    _visitors = visitors;
    self.collectionView.dataArray = visitors;
    BOOL hiddenVisitors = visitors.count == 0;
    self.collectionView.hidden = hiddenVisitors;
    self.treeView.visitLbl.hidden = hiddenVisitors;
    CGFloat treeHeight = App_Width * 1746.0 / 1242.0 + 100;
    CGFloat treeBottomHeight = visitors.count > 0 ? 64 : 30;
    [self.treeView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(treeHeight + treeBottomHeight);
    }];
    CGFloat height = visitors.count > 0 ? 185 : 0;
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.treeView.mas_bottom).offset(0);
        make.height.mas_equalTo(height);
    }];
//    CGFloat offset = hiddenVisitors ? -10 :200;
//    [self.goBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left).offset(20);
//        make.right.equalTo(self.mas_right).offset(-20);
//        make.top.equalTo(self.treeView.mas_bottom).offset(offset);
//        make.height.mas_equalTo(70);
//    }];
}
@end
