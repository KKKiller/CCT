//
//  CCShareView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/2.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCShareView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *pengyouquanBtn;
@property (weak, nonatomic) IBOutlet UIButton *weixinBtn;
@property (weak, nonatomic) IBOutlet UIButton *addressBookBtn;
+ (instancetype)instanceView;
@end
