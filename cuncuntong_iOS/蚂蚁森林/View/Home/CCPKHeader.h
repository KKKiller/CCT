//
//  CCPKHeader.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCPKView.h"
#import "MYHomeTreeView.h"
@interface CCPKHeader : UIView
@property (nonatomic, strong) MYHomeTreeView *treeView;
@property (nonatomic, strong) CCPKView *pkView;
@end
