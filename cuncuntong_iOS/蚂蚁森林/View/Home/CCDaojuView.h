//
//  CCDaojuView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCDaojuView : UIView
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UIImageView *fangDaoWang;
@property (nonatomic, strong) UIImageView *feiLiao;
@property (nonatomic, strong) UIImageView *jiaoShui;


@end
