//
//  CCHomeHeaderView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYHeaderCollectionView.h"
#import "MYHomeTreeView.h"

@interface MYHomeHeaderView : UIView

@property (nonatomic, strong) MYHomeTreeView *treeView;
@property (nonatomic, strong) MYHeaderCollectionView *collectionView;
@property (nonatomic, strong) UIButton *goBtn;
@property (nonatomic, assign) BOOL hiddenVisitors;
@property (nonatomic, strong) NSArray *visitors;
@end
