//
//  MYHomeCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHomeCell.h"

@implementation MYHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(MYRankModel *)model {
    _model = model;
    [self.avatar setImageWithURL:URL(model.avatar) placeholder:IMAGENAMED(@"head")];
    if (model.nickname.length == 11 && [model.nickname integerValue] > 0) {
        model.nickname = [model.nickname stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    self.nameLbl.text = model.nickname;
    self.energyLBl.text = [NSString stringWithFormat:@"%@个金豆",model.bean_num];
//    self.energyLBl.text  = [NSString stringWithFormat:@"%@g",model.energy];
//    self.energyLBl.hidden = YES;
    self.indexLbl.text = [NSString stringWithFormat:@"%@",@(model.index+1)];
    self.indexLbl.hidden = model.index < 3;
    self.leftImgView.hidden = model.index >= 3;
    if (model.index < 3) {
        NSString *imageStr = model.index == 0 ? @"gold" : model.index == 1 ? @"yinpai" : @"tongpai";
        self.leftImgView.image = IMAGENAMED(imageStr);        
    }
    self.inviteLbl.text = [NSString stringWithFormat:@"推荐%@人种豆",STRINGEMPTY(model.invite_num) ? @"0" : model.invite_num];
}
@end
