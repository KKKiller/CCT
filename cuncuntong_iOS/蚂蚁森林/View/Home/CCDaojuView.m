//
//  CCDaojuView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCDaojuView.h"

@implementation CCDaojuView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
    }
    return self;
}
- (void)setUI {
    self.titleLbl = [[UILabel alloc]initWithText:@"道具" font:18 textColor:RGBCOLOR(53, 75, 99)];
    [self addSubview:self.titleLbl];
    self.titleLbl.backgroundColor = WHITECOLOR;
    self.titleLbl.textAlignment = NSTextAlignmentCenter;
    
    self.closeBtn = [[UIButton alloc]init];
    [self addSubview:self.closeBtn];
    [self.closeBtn setImage:IMAGENAMED(@"close") forState:UIControlStateNormal];
    
    self.scrollView = [[UIScrollView alloc]init];
    [self addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(555, 261);
    self.scrollView.backgroundColor = BACKGRAY;
    
    self.fangDaoWang = [[UIImageView alloc]init];
    [self.scrollView addSubview:self.fangDaoWang];
    self.fangDaoWang.image = IMAGENAMED(@"fangdaowang");
    self.fangDaoWang.userInteractionEnabled = YES;
    self.fangDaoWang.tag = 100;
    
    self.feiLiao = [[UIImageView alloc]init];
    [self.scrollView addSubview:self.feiLiao];
    self.feiLiao.image = IMAGENAMED(@"huafei");
    self.feiLiao.userInteractionEnabled = YES;
    self.feiLiao.tag =200;

    
    self.jiaoShui = [[UIImageView alloc]init];
    [self.scrollView addSubview:self.jiaoShui];
    self.jiaoShui.image = IMAGENAMED(@"jiaoshui");
    self.jiaoShui.userInteractionEnabled = YES;
    self.jiaoShui.tag = 300;

}
- (void)layoutSubviews {
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self);
        make.width.height.mas_equalTo(40);
    }];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.top.mas_equalTo(self.titleLbl.mas_bottom);
    }];
    CGFloat width = 165;
    CGFloat height = 221;
    [self.fangDaoWang mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView).offset(20);
        make.top.equalTo(self.scrollView.mas_top).offset(20);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    [self.feiLiao mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView).offset(30 + width);
        make.top.equalTo(self.scrollView.mas_top).offset(20);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    [self.jiaoShui mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView).offset(40 + width * 2);
        make.top.equalTo(self.scrollView.mas_top).offset(20);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
}

@end
