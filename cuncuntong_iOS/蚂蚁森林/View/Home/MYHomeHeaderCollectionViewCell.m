//
//  MYHomeHeaderCollectionViewCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHomeHeaderCollectionViewCell.h"

@implementation MYHomeHeaderCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.layer.borderColor = MAINBLUE.CGColor;
//    self.layer.cornerRadius = 4;
//    self.layer.masksToBounds = YES;
//    self.layer.borderWidth = 1;
    
//    self.backView.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.backView.layer.shadowOpacity = 0.5f;
//    self.backView.layer.shadowRadius = 4.f;
//    self.backView.layer.shadowOffset = CGSizeMake(3,3);
}
- (void)setModel:(MYUserInfoModel *)model {
    [_headImgView setImageURL:URL(model.avatar)];
    NSString *nickName = (model.nickname.length == 11 && [model.nickname integerValue] > 0) ? [model.nickname stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] : model.nickname;
    _nameLbl.text = nickName;
    NSString *energy = [NSString stringWithFormat:@"%@g",model.energy];
    [_energyBtn setTitle:energy forState:UIControlStateNormal];
    _timeLBl.text = [NSString stringWithFormat:@"%@来过", model.visit_time];
}
@end
