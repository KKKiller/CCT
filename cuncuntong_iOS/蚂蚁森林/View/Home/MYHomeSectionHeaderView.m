//
//  MYHomeSectionHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHomeSectionHeaderView.h"

@implementation MYHomeSectionHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WHITECOLOR;
        [self setUI];
        
    }
    return self;
}
- (void)setUI {

    self.leftV = [[UIView alloc]init];
    [self addSubview:self.leftV];
    self.leftV.backgroundColor = MTRGB(0x97cd00);
    
    self.titleLbl = [[UILabel alloc]initWithText:@"金豆排行榜" font:17 textColor:MTRGB(0x344b61)];
    [self addSubview:self.titleLbl];
    
    self.moreBtn = [[UIButton alloc]initWithTitle:@"查看Top50" textColor:MTRGB(0x97cd00) backImg:nil font:13];
    [self addSubview:self.moreBtn];
    self.moreBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    
    self.lineLbl = [[UILabel alloc]initWithShallowLine];
    [self addSubview:self.lineLbl];
}
- (void)layoutSubviews {
    [self.leftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.width.mas_equalTo(5);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.mas_top).offset(27);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftV.mas_right).offset(5);
        make.top.equalTo(self.mas_top).offset(25);
    }];
    [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.titleLbl.mas_centerY);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(40);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(0.5);
        make.top.equalTo(self.mas_top);
    }];
}
@end
