//
//  CCShareView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/2.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareView.h"

@implementation CCShareView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}

@end
