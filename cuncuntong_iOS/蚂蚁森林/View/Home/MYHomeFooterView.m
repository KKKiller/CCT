//
//  MYHomeFooterView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHomeFooterView.h"
#import "CCHomeFooterCollectionViewCell.h"
@interface MYHomeFooterView()<UICollectionViewDelegate,UICollectionViewDataSource>
@end
@implementation MYHomeFooterView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
        
        
    }
    return self;
}
- (void)setUI {
    self.shareBtn = [[UIButton alloc]initWithTitle:@"分享好友" textColor:WHITECOLOR backImg:nil font:16];
    [self addSubview:self.shareBtn];
    [self.shareBtn setBackgroundColor:MTRGB(0x98cb27)];
    self.shareBtn.layer.cornerRadius = 20;
    self.shareBtn.layer.masksToBounds = YES;
    
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self);
        make.width.mas_equalTo(App_Width - 40);
        make.height.mas_equalTo(40);
    }];
    
}
//    self.titleLbl = [[UILabel alloc]initWithText:@"邀请好友收金豆" font:20 textColor:MTRGB(0x344b61)];
//    [self addSubview:self.titleLbl];
//
//    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left).offset(20);
//        make.top.equalTo(self.mas_top).offset(25);
//    }];
//
//    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.mas_equalTo(self);
//        make.top.equalTo(self.titleLbl.mas_bottom).offset(5);
//        make.height.mas_equalTo(150);
//    }];
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    //    return self.dataArray.count;
//    return 10;
//}
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CCHomeFooterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"homeFooterCollectionView" forIndexPath:indexPath];
//    return cell;
//}
//
//#pragma mark  collectionView代理方法,添加照片
////点击collectionView跳转到相册
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//
//}
//
//- (void)setCollectionView{
//    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
//    layout.itemSize = CGSizeMake(230, 70);
//    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    layout.minimumLineSpacing = 5;
//    layout.minimumInteritemSpacing = 5;
//    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
//    collectionView.backgroundColor = BACKGRAY;
//    self.collectionView = collectionView;
//    [self.collectionView registerNib:[UINib nibWithNibName:@"CCHomeFooterCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"homeFooterCollectionView"];
//    self.collectionView.delegate = self;
//    self.collectionView.dataSource = self;
//    [self addSubview:self.collectionView];
//}
//- (void)layoutSubviews {
//
//}



@end
