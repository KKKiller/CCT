//
//  MYHomeFooterView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYHomeFooterView : UIView
//@property (nonatomic, strong) UILabel *titleLbl;
//@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *shareBtn;
@end
