//
//  MYHomeSectionHeaderView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYHomeSectionHeaderView : UIView
@property (nonatomic, strong) UIView *leftV;
@property (nonatomic, strong) UILabel *lineLbl;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIButton *moreBtn;
@end
