//
//  MYHomeTreeView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "MYHomeTreeView.h"

@implementation MYHomeTreeView

+ (instancetype)instanceView {
    MYHomeTreeView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    [view addBubbleEnergyLbl];
    view.openJindouBtn.layer.borderColor = WHITECOLOR.CGColor;
    view.openJindouBtn.layer.borderWidth = 1;
    [view.jindouAnimationTap  addTarget:view action:@selector(showJindouAnimation)];
    view.stage = -1;
    
    return view;
}
- (void)addBubbleEnergyLbl {
    self.sportEnergyLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.sportBubble addSubview:self.sportEnergyLbl];
    [self.sportEnergyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self.sportBubble);
    }];
    
    self.shopEnergyLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.shopBubble addSubview:self.shopEnergyLbl];
    [self.shopEnergyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self.shopBubble);
    }];
    
    self.inviteEnergyLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.inviteBubble addSubview:self.inviteEnergyLbl];
    [self.inviteEnergyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self.inviteBubble);
    }];
    
    self.readEnergyLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.readBubble addSubview:self.readEnergyLbl];
    [self.readEnergyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self.readBubble);
    }];
    
    self.redEnergyLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.redBubble addSubview:self.redEnergyLbl];
    [self.redEnergyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.centerX.equalTo(self.redBubble);
    }];
    
    self.nljLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.nljBubble addSubview:self.nljLbl];
    [self.nljLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.centerX.equalTo(self.nljBubble);
    }];
    
    self.creditLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.creditBubble addSubview:self.creditLbl];
    [self.creditLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self.creditBubble);
    }];
    
    self.tuijianLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.tuijianBubble addSubview:self.tuijianLbl];
    [self.tuijianLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.tuijianBubble);
        make.centerY.equalTo(self.tuijianBubble.mas_centerY).offset(-5);
    }];
    
    self.shareLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    [self.share_bubble addSubview:self.shareLbl];
    self.shareLbl.textAlignment = NSTextAlignmentCenter;
    [self.shareLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.share_bubble);
        make.centerY.equalTo(self.share_bubble.mas_centerY).offset(-5);

    }];
    self.gongxiangLbl = [[UILabel alloc]initWithText:@"100g" font:13 textColor:MTRGB(0x268E28)];
    self.gongxiangLbl.textAlignment = NSTextAlignmentCenter;

    [self.gongxiang_bubble addSubview:self.gongxiangLbl];
    [self.gongxiangLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.gongxiang_bubble);
        make.centerY.equalTo(self.gongxiang_bubble.mas_centerY).offset(-5);

    }];

}
- (void)setBaohuzhaoTime:(NSString *)baohuzhaoTime {
    _baohuzhaoTime = baohuzhaoTime;
    self.baohuzhaoTimeView.hidden = NO;
    UILabel *timeLbl = [[UILabel alloc]initWithText:baohuzhaoTime font:8 textColor:WHITECOLOR];
    timeLbl.text = [NSString stringWithFormat:@"有效至:%@",baohuzhaoTime];
    [self.baohuzhaoTimeView addSubview:timeLbl];
    [timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.baohuzhaoTimeView.mas_centerX).offset(8);
        make.centerY.equalTo(self.baohuzhaoTimeView);
    }];
}
- (void)setEnergyModel:(MYEnergyModel *)energyModel {
    _energyModel = energyModel;
    self.canPickJindou = energyModel.beanArray.count > 0;
//    energyModel.sport_energy = 100;
//    energyModel.buy_energys = 100;
//    energyModel.invite_energys = 100;
//    energyModel.read_energy = 100;
//    energyModel.red_energy_today = 100;
//        energyModel.red_energy = 100;
//    energyModel.sport_energy_today = 20;
//    energyModel.credit_energy = 100;
//    energyModel.credit_energy_remain = 100;

    self.sportBubble.hidden = energyModel.sport_energy == 0 && energyModel.sport_energy_today == 0;
    self.shopBubble.hidden = energyModel.buy_energys == 0;
    self.gongxiang_bubble.hidden = energyModel.sp_energys == 0;
    self.inviteBubble.hidden = energyModel.invite_energys == 0;
    self.readBubble.hidden = energyModel.read_energy == 0 && energyModel.read_energy_today == 0;
    self.creditBubble.hidden = energyModel.credit_energy == 0;
    self.share_bubble.hidden = energyModel.share_energy_today == 0 && energyModel.share_energy == 0;
    self.tuijianBubble.hidden = energyModel.tjArray.count == 0;
    self.girlSignBubble.superview.hidden = energyModel.sign_energy_today == 0;
    
    self.sportEnergyLbl.hidden = energyModel.sport_energy == 0 && energyModel.sport_energy_today == 0;
    self.shopEnergyLbl.hidden = energyModel.buy_energys == 0;
    self.redBubble.hidden = energyModel.red_energys == 0;
    self.gongxiangLbl.hidden = energyModel.sp_energys == 0;
    self.nljBubble.hidden = energyModel.nlj_energys == 0;

    self.inviteEnergyLbl.hidden = energyModel.invite_energys == 0;
    self.redEnergyLbl.hidden = energyModel.red_energys == 0;
    self.readEnergyLbl.hidden = energyModel.read_energy == 0 && energyModel.read_energy_today == 0;
    self.creditLbl.hidden = energyModel.credit_energy == 0;
    self.shareLbl.hidden  = energyModel.share_energy_today == 0 && energyModel.share_energy == 0;
    self.tuijianLbl.hidden = energyModel.tjArray.count == 0;

    self.shopEnergyLbl.text = [NSString stringWithFormat:@"%ldg",(long)energyModel.buy_energys];
    self.gongxiangLbl.text = [NSString stringWithFormat:@"%ldg",(long)energyModel.sp_energys];
    self.redEnergyLbl.text = [NSString stringWithFormat:@"%ldg",(long)energyModel.red_energys];

    self.inviteEnergyLbl.text = [NSString stringWithFormat:@"%ldg",(long)energyModel.invite_energys];
    
    //信用
    if (energyModel.credit_energy > 0) {
        if (energyModel.credit_energy_remain == 0) {
            self.creditLbl.text = [NSString stringWithFormat:@"%ldg",(long)energyModel.credit_energy];
        }else{
            self.creditLbl.text = [NSString stringWithFormat:@"%@%ldg",[self getRemainTime:energyModel.credit_energy_remain],energyModel.credit_energy];
            self.creditLbl.font = FFont(8);

        }
    }
    
    
    //运动
    if (energyModel.sport_energy_today > 0 && energyModel.sport_energy > 0) {
        if(energyModel.sport_energy_remain > 0){
            self.sportEnergyLbl.text = [NSString stringWithFormat:@"%@\n%ldg\n今日能量\n%ldg",[self getRemainTimeWithoutRerutn:energyModel.sport_energy_remain],energyModel.sport_energy , energyModel.sport_energy_today];
        }else{
            self.sportEnergyLbl.text = [NSString stringWithFormat:@"可收能量\n%ldg\n今日能量\n%ldg",energyModel.sport_energy , energyModel.sport_energy_today];
        }
        self.sportEnergyLbl.font = FFont(8);
    }else if(energyModel.sport_energy_today > 0){
        self.sportEnergyLbl.text = [NSString stringWithFormat:@"%@%ldg",[self getRemainTime:energyModel.sport_energy_today_remain],energyModel.sport_energy_today];
        if (energyModel.sport_energy_today_remain > 0) {
            self.sportEnergyLbl.font = FFont(9);
        }
    }else{
        self.sportEnergyLbl.text = [NSString stringWithFormat:@"%@%ldg",[self getRemainTime:energyModel.sport_energy_remain],energyModel.sport_energy];
        if (energyModel.sport_energy_remain > 0) {
            self.sportEnergyLbl.font = FFont(9);
        }
    }
//阅读
    if (energyModel.read_energy_today > 0 && energyModel.read_energy > 0) {
        if(energyModel.read_energy_remain > 0){
            self.readEnergyLbl.text = [NSString stringWithFormat:@"%@\n%ldg\n今日能量\n%ldg",[self getRemainTimeWithoutRerutn:energyModel.read_energy_remain],energyModel.read_energy , energyModel.read_energy_today];
        }else{
            self.readEnergyLbl.text = [NSString stringWithFormat:@"可收能量\n%ldg\n今日能量\n%ldg",energyModel.read_energy , energyModel.read_energy_today];
        }
        self.readEnergyLbl.font = FFont(8);
    }else if(energyModel.read_energy_today > 0){
        self.readEnergyLbl.text = [NSString stringWithFormat:@"%@%ldg",[self getRemainTime:energyModel.read_energy_today_remain],energyModel.read_energy_today];
        if (energyModel.read_energy_today_remain > 0) {
            self.readEnergyLbl.font = FFont(9);
        }
    }else{
        self.readEnergyLbl.text = [NSString stringWithFormat:@"%@%ldg",[self getRemainTime:energyModel.read_energy_remain],energyModel.read_energy];
        if (energyModel.read_energy_remain > 0) {
            self.readEnergyLbl.font = FFont(9);
        }
    }

    //分享
    if (energyModel.share_energy_today > 0 && energyModel.share_energy > 0) {
        if(energyModel.share_energy_remain > 0){
            self.shareLbl.text = [NSString stringWithFormat:@"%@\n%ldg\n今日能量\n%ldg",[self getRemainTimeWithoutRerutn:energyModel.share_energy_remain],energyModel.share_energy , energyModel.share_energy_today];
        }else{
            self.shareLbl.text = [NSString stringWithFormat:@"可收能量\n%ldg\n今日能量\n%ldg",energyModel.share_energy , energyModel.share_energy_today];
        }
        self.shareLbl.font = FFont(8);
    }else if(energyModel.share_energy_today > 0){
        self.shareLbl.text = [NSString stringWithFormat:@"%@%ldg",[self getRemainTime:energyModel.share_energy_today_remain],energyModel.share_energy_today];
        if (energyModel.share_energy_today_remain > 0) {
            self.shareLbl.font = FFont(9);
        }
    }else{
        self.shareLbl.text = [NSString stringWithFormat:@"%@%ldg",[self getRemainTime:energyModel.share_energy_remain],energyModel.share_energy];
        if (energyModel.share_energy_remain > 0) {
            self.shareLbl.font = FFont(9);
        }
    }
    
    //推荐
    if(energyModel.tjArray.count > 0){
        MYSubEnergyModel *energy = energyModel.tjArray[0];
        self.tuijianLbl.text = [NSString stringWithFormat:@"%ldg",energy.energy];
    }
    
    //能量机
    if(energyModel.nljIdArray.count > 0){
        MYSubEnergyModel *energy = energyModel.nljIdArray[0];
        self.nljLbl.text = [NSString stringWithFormat:@"%ldg",energy.energy];
    }
    
    //女儿国签到
    if(_energyModel.sign_energy_today_remain > 0){
        NSString *text = [NSString stringWithFormat:@"%ldg\n%@",energyModel.sign_energy_today,[self getRemainTime:energyModel.sign_energy_today_remain]];
        self.girlSignLbl.text = [text substringToIndex:text.length - 1];
        self.girlSignLbl.font = FFont(9);
    }else{
        self.girlSignLbl.text = [NSString stringWithFormat:@"%ldg\n",energyModel.sign_energy_today];
        self.girlSignLbl.font = FFont(13);
    }
    self.sportEnergyLbl.textAlignment = NSTextAlignmentCenter;
    self.readEnergyLbl.textAlignment = NSTextAlignmentCenter;
    self.redEnergyLbl.textAlignment = NSTextAlignmentCenter;

}
- (NSString *)getRemainTime:(NSInteger)remain {
    NSString *time = @"";
    if (remain >= 3600) {
        time = [NSString stringWithFormat:@"还剩\n%ld小时\n",remain / 3600];
    }else if (remain > 0){
        time = [NSString stringWithFormat:@"还剩\n%ld分钟\n",remain/60];
    }
    return time;
}
- (NSString *)getRemainTimeWithoutRerutn:(NSInteger)remain {
    NSString *time = @"";
    if (remain >= 3600) {
        time = [NSString stringWithFormat:@"还剩%ld小时",remain / 3600];
    }else if (remain > 0){
        time = [NSString stringWithFormat:@"还剩%ld分钟",remain/60];
    }
    return time;
}
- (void)setHiddenBtns:(BOOL)hiddenBtns {
    _hiddenBtns = hiddenBtns;
    self.daojuBtn.hidden = hiddenBtns;
    self.gonglueBtn.hidden = hiddenBtns;
    self.sportBtn.hidden = hiddenBtns;
    self.doushiBtn.hidden = hiddenBtns;
    self.douguanBtn.hidden = hiddenBtns;
    self.visitLbl.hidden = hiddenBtns;
    self.visitView.hidden = hiddenBtns;
    self.jiaoshuiBtn.hidden = !hiddenBtns;
}
#pragma mark - 金豆动画
- (void)showJindouAnimation {
    if (!self.canPickJindou) {
        return;
    }
    UIImageView *imgV = [[UIImageView alloc]init];
    [self addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(325);
        make.centerX.equalTo(self.treeImgView.mas_centerX).offset(10);
        make.centerY.equalTo(self.treeImgView.mas_centerY).offset(22);

    }];
    UIImageView *beanImgV = [[UIImageView alloc]init];
    [self addSubview:beanImgV];
    beanImgV.image = IMAGENAMED(@"jindou");
    [beanImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(45);
        make.centerX.centerY.equalTo(imgV);
    }];
    [self animationWithImageView:imgV begin:17 end:25 name:@"shjd_总文件_00" repeat:NO circleBegin:0 circleEnd:0 circleName:@"" finishBlk:^{
        [UIView animateWithDuration:1.5 delay:0 options:UIViewAnimationOptionTransitionCurlDown animations:^{
            beanImgV.center = self.douguanBtn.center;
        } completion:^(BOOL finished) {
            SHOW(@"金豆已收入豆罐");
            if (self.jindouBlk) {
                self.jindouBlk();
            }
            [UIView animateWithDuration:0.3 animations:^{
                beanImgV.alpha = 0;
            }completion:^(BOOL finished) {
                [beanImgV removeFromSuperview];
            }];
        }];
    }];
}
#pragma mark - 抖动动画
- (void)showTreeShakeAnimation {
    CGFloat originY = self.treeAnimationImgView.y;
    CGFloat originH = self.treeAnimationImgView.height;
    self.treeAnimationImgView.contentMode = UIViewContentModeScaleAspectFill;
    CGFloat value = 10;
    [UIView animateWithDuration:0.2 animations:^{
        self.treeAnimationImgView.y = originY - value;
        self.treeAnimationImgView.height = originH + value;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            self.treeAnimationImgView.y = originY + value;
            self.treeAnimationImgView.height = originH - value;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                self.treeAnimationImgView.y = originY - value;
                self.treeAnimationImgView.height = originH + value;
            } completion:^(BOOL finished) {
                self.treeAnimationImgView.y = originY;
                self.treeAnimationImgView.height = originH;
                self.treeAnimationImgView.contentMode = UIViewContentModeScaleToFill;
            }];
        }];
    }];
}
#pragma mark -  阶段动画
- (void)beginBirdFlyAnimation {
    [self animationWithImageView:self.birdImgView begin:0 end:29 name:@"总文件_00" repeat:YES circleBegin:0 circleEnd:0 circleName:@"" finishBlk:nil];
}
////施肥
- (void)showShifeiAnimationWithFinishBlk:(FinishBLK)finishBlk {
    [self animationWithImageView:self.daojuImgView begin:1 end:23 name:@"sf_总文件_00" repeat:NO circleBegin:0 circleEnd:0 circleName:@"" finishBlk:finishBlk];
}
////浇水
- (void)showJiaoshuiAnimationWithFinishBlk:(FinishBLK)finishBlk {
    [self animationWithImageView:self.daojuImgView begin:1 end:22 name:@"js_总文件_00" repeat:NO circleBegin:0 circleEnd:0 circleName:@"" finishBlk:finishBlk];
}
////保护罩
- (void)showBaohuzhaoAnimationWithFinishBlk:(FinishBLK)finishBlk {
    [self animationWithImageView:self.baohuzhaoImgView begin:1 end:9 name:@"bhz_总文件_00" repeat:NO circleBegin:10 circleEnd:19 circleName:@"bhz_循环_00" finishBlk:finishBlk];
}
//树成长
- (void)beginTreeGrwoingAnimationWithStage:(TreeGrowingStage)stage {
    if (_stage == stage) {
        return;
    }
    _stage = stage;
    NSString *name,*circleName;
    int begin,end,circleBegin,circleEnd,width,height;
    switch (stage) {
        case TreeGrowingStageJindou:
            name = @"xjd_出现_00";  circleName = @"xjd_出现_00"; begin = 0; end = 4;  circleBegin = 4;  circleEnd = 4;width = 81;height = 61;
            break;
        case TreeGrowingStageFaya:
            name = @"jdm_出现_00";  circleName = @"jdm_出现_00"; begin = 0; end = 8;  circleBegin = 14;  circleEnd = 14;width = 81;height = 92;
            break;
        case TreeGrowingStageChutu:
            name = @"xsm_总文件_00";  circleName = @"xsm_总文件_00"; begin = 0; end =9;  circleBegin = 9;  circleEnd = 9;width = 89;height = 135;
            break;
        case TreeGrowingStageYoumiao:
            name = @"xs_总文件_00";  circleName = @"xs_总文件_00"; begin = 0; end = 10;  circleBegin = 11;  circleEnd = 11;width = 220;height = 280;
            break;
        case TreeGrowingStageZhigan:
            name = @"zs_总文件_00";  circleName = @"zs_总文件_00"; begin = 0; end = 19;  circleBegin = 20;  circleEnd = 20;width = 200;height = 280;
            break;
        case TreeGrowingStageMaosheng:
            name = @"ds_总文件_00";  circleName = @"ds_总文件_00"; begin = 0; end = 12;  circleBegin = 13;  circleEnd = 13;width = 325;height = 285;
            break;
        case TreeGrowingStageKaihua:
            name = @"kh_总文件_00";  circleName = @"kh_总文件_00"; begin = 4; end =16;  circleBegin = 17;  circleEnd = 17;width = 325;height = 285;
            break;
        case TreeGrowingStageQingguo:
            name = @"qg_总文件_00";  circleName = @"qg_总文件_00"; begin = 7; end =21;  circleBegin = 21;  circleEnd = 21;width = 260;height = 238;
            break;
        case TreeGrowingStageHuangguo:
            name = @"hg_总文件_00";  circleName = @"hg_总文件_00";begin = 19;  end = 32;  circleBegin = 33;  circleEnd = 33;width = 260;height = 238;
            break;
        case TreeGrowingStageYouhuayouguo:
            name = @"yhyg_总文件_00";  circleName = @"yhyg_总文件_00";begin = 5;  end = 19; circleBegin = 20;  circleEnd = 20;width = 250;height = 233;
            break;
            
            default:
            name = @"";  circleName = @""; begin = 0; end = 0;  circleBegin = 0;  circleEnd = 0;width = 0;height = 0;
            break;
    }
    [self.treeAnimationImgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    [self animationWithImageView:self.treeAnimationImgView begin:begin end:end name:name repeat:NO  circleBegin:circleBegin circleEnd:circleEnd circleName:circleName finishBlk:nil];
}

- (void)animationWithImageView:(UIImageView *)imageView begin:(int)begin end:(int)end name:(NSString *)name repeat:(BOOL)repeat circleBegin:(int)circleBegin circleEnd:(int)circleEnd circleName:(NSString *)circleName finishBlk:(FinishBLK)finishBlk{
    NSMutableArray *imgArray = [NSMutableArray array];
    NSMutableArray *circleImgArray = [NSMutableArray array];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int i = begin; i<end+1; i++) {
            NSString *nameStr = [NSString stringWithFormat:@"%@%03d",name,i];
            UIImage *image = [UIImage imageNamed:nameStr inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
            [imgArray addObject:image];
        }
        if ( circleEnd > 0) {
            for (int i = (int)circleBegin; i<circleEnd+1; i++) {
                NSString *nameStr = [NSString stringWithFormat:@"%@%03d",circleName,i];
                UIImage *image = [UIImage imageNamed:nameStr inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
                [circleImgArray addObject:image];
            }
        }
        CGFloat duration = 1 / 10.0;
        dispatch_async_on_main_queue(^{
            imageView.animationImages = imgArray;
            imageView.animationDuration = 1;
            imageView.animationDuration = duration * imgArray.count;
            imageView.animationRepeatCount = repeat ? 0 : 1;
            if(circleImgArray.count == 1){
                [imageView setImage: circleImgArray[0]];
            }
            [imageView startAnimating];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)( duration *imgArray.count * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (circleImgArray.count > 1) {
                    imageView.animationImages = circleImgArray;
                    imageView.animationRepeatCount = 0;
                    imageView.animationDuration = duration * circleImgArray.count;
                    [imageView startAnimating];
                }else{
                    if (finishBlk) {
                        finishBlk();
                    }
                }
            });
        });
    });
}
#pragma mark - 漂浮动画

- (void)bubbleAnimation {
    [self addFudongAnimation];
}

-(void) removeAllAnimation{
    [self.sportBubble.layer removeAllAnimations];
    [self.shopBubble.layer removeAllAnimations];
    [self.inviteBubble.layer removeAllAnimations];
    [self.readBubble.layer removeAllAnimations];
    [self.redBubble.layer removeAllAnimations];
    [self.nljBubble.layer removeAllAnimations];
    [self.creditBubble.layer removeAllAnimations];
     [self.tuijianBubble.layer removeAllAnimations];
    [self.gongxiang_bubble.layer removeAllAnimations];
    [self.share_bubble.layer removeAllAnimations];

}
/**
 *  浮动动画
 */
-(void)addFudongAnimation{
    [self removeAllAnimation];
    [self addAniamtionLikeGameCenterBubble:self.girlSignBubble.superview];
    [self addAniamtionLikeGameCenterBubble:self.sportBubble];
    [self addAniamtionLikeGameCenterBubble:self.shopBubble];
    [self addAniamtionLikeGameCenterBubble:self.inviteBubble];
    [self addAniamtionLikeGameCenterBubble:self.readBubble];
    [self addAniamtionLikeGameCenterBubble:self.redBubble];
    [self addAniamtionLikeGameCenterBubble:self.nljBubble];
    [self addAniamtionLikeGameCenterBubble:self.creditBubble];
    [self addAniamtionLikeGameCenterBubble:self.tuijianBubble];
    [self addAniamtionLikeGameCenterBubble:self.gongxiang_bubble];
    [self addAniamtionLikeGameCenterBubble:self.share_bubble];

    self.girlSignBubble.superview.layer.speed = 0.8;
    self.sportBubble.layer.speed=0.8;
    self.shopBubble.layer.speed=0.9;
    self.inviteBubble.layer.speed=0.7;
    self.readBubble.layer.speed=1;
    self.redBubble.layer.speed=1;
    self.nljBubble.layer.speed=1;
    self.creditBubble.layer.speed=0.8;
    self.tuijianBubble.layer.speed=0.9;
    self.gongxiang_bubble.layer.speed=0.9;
    self.share_bubble.layer.speed=0.9;

}

-(void)addAniamtionLikeGameCenterBubble:(UIView *)mView{
    
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.repeatCount = INFINITY;
    pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    pathAnimation.duration = 5.0;
    
    
    CGMutablePathRef curvedPath = CGPathCreateMutable();
//    CGRect circleContainer = CGRectInset(mView.frame, mView.bounds.size.width / 2  , mView.bounds.size.width / 2 -1);
    CGRect circleContainer = CGRectInset(CGRectMake(mView.x+mView.width*0.3, mView.y+mView.height*0.3, mView.width * 0.2, mView.width * 0.2), 0  , 0);

    CGPathAddEllipseInRect(curvedPath, NULL, circleContainer);
    
    pathAnimation.path = curvedPath;
    CGPathRelease(curvedPath);
    [mView.layer addAnimation:pathAnimation forKey:@"myCircleAnimation"];
    
    
    CAKeyframeAnimation *scaleX = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale.x"];
    scaleX.duration = 4.0;
    scaleX.values = @[@1.0, @1.1, @1.0];
    scaleX.repeatCount = INFINITY;
    scaleX.autoreverses = YES;
    
    scaleX.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [mView.layer addAnimation:scaleX forKey:@"scaleXAnimation"];
    
    
    CAKeyframeAnimation *scaleY = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale.y"];
    scaleY.duration = 4.5;
    scaleY.values = @[@1.0, @1.1, @1.0];
    scaleY.repeatCount = INFINITY;
    scaleY.autoreverses = YES;
    scaleX.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [mView.layer addAnimation:scaleY forKey:@"scaleYAnimation"];
}
@end
