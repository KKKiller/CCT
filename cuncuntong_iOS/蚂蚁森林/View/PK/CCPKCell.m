//
//  CCPKCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCPKCell.h"

@implementation CCPKCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MYPKModel *)model {
    _model = model;
    self.descLbl.text =[NSString stringWithFormat:@"%@",model.msg];
    self.timeLbl.text = model.add_time;
    if([model.msg containsString:@"水"]){
        self.imgView.image = IMAGENAMED(@"jiaoshui_to");
    }else{
        self.imgView.image = IMAGENAMED(@"shouqu");
    }
}

@end
