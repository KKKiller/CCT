//
//  CCPKView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYHomeTreeView.h"
#import "MYUserInfoModel.h"
@interface CCPKView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *myImgView;
@property (weak, nonatomic) IBOutlet UILabel *myEnergyLbl;
@property (weak, nonatomic) IBOutlet UIImageView *heImgView;
@property (weak, nonatomic) IBOutlet UILabel *heEnergyLbl;
@property (weak, nonatomic) IBOutlet UIImageView *myWinImgView;
@property (weak, nonatomic) IBOutlet UIImageView *heWinImgView;

+ (instancetype)instanceView ;
@property (nonatomic, strong) MYUserInfoModel *myInfo;
@property (nonatomic, strong) MYUserInfoModel *taInfo;
@property (nonatomic, assign) BOOL isIWin;
@end
