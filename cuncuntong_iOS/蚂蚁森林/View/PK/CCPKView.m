//
//  CCPKView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCPKView.h"

@implementation CCPKView
+ (instancetype)instanceView {
    CCPKView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.myImgView.layer.cornerRadius = 3;
    view.myImgView.layer.masksToBounds = YES;
    view.heImgView.layer.cornerRadius = 3;
    view.heImgView.layer.masksToBounds = YES;
    
    return view;
}

- (void)setMyInfo:(MYUserInfoModel *)myInfo {
    _myInfo = myInfo;
    [self.myImgView setImageWithURL:URL(myInfo.avatar) placeholder:IMAGENAMED(@"head")];
}
- (void)setTaInfo:(MYUserInfoModel *)taInfo {
    _taInfo = taInfo;
    [self.heImgView setImageWithURL:URL(taInfo.avatar) placeholder:IMAGENAMED(@"head")];

}
- (void)setIsIWin:(BOOL)isIWin {
    self.myWinImgView.hidden = !isIWin;
    self.heWinImgView.hidden = isIWin;
    
    self.myImgView.layer.borderWidth = 3;
    self.myImgView.layer.borderColor = isIWin ? MTRGB(0xfacd4b).CGColor : MTRGB(0xe6eaf5).CGColor;
    self.heImgView.layer.borderWidth = 3;
    self.heImgView.layer.borderColor = !isIWin ? MTRGB(0xfacd4b).CGColor : MTRGB(0xe6eaf5).CGColor;
}
@end
