//
//  CCPKHeader.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCPKHeader.h"
@implementation CCPKHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
        [self layoutSubview];
    }
    return self;
}
- (void)setUI {
    
    self.pkView = [CCPKView instanceView];
    [self addSubview:self.pkView];
    
    
    self.treeView = [MYHomeTreeView instanceView];
    [self addSubview:self.treeView];
    self.treeView.hiddenBtns = YES;
    self.treeView.mayiBanner.hidden = YES;
    
    
    
    
    
 
}
- (void)layoutSubview {
    
    CGFloat treeHeight = App_Width * 1746.0 / 1242.0;
    self.treeView.treeViewHeight.constant = treeHeight;
    [self.treeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.mas_equalTo(treeHeight);
    }];
    [self.pkView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.treeView.mas_bottom).offset(-24);
        make.height.mas_equalTo(210);
    }];
}
- (void)setMyInfo:(MYUserInfoModel *)myInfo {
    _myInfo = myInfo;
    self.pkView.myInfo = myInfo;
    
}
- (void)setTaInfo:(MYUserInfoModel *)taInfo {
    _taInfo = taInfo;
    self.pkView.taInfo = taInfo;
}
@end
