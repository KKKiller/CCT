//
//  MYHomeTreeView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/19.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYEnergyModel.h"
#import "MYHomeController.h"

typedef void(^FinishBLK)(void);
typedef void(^JindouBLK)(void);
@interface MYHomeTreeView : UIView
+ (instancetype)instanceView;
@property (weak, nonatomic) IBOutlet UIButton *daojuBtn;
@property (weak, nonatomic) IBOutlet UIButton *gonglueBtn;
@property (weak, nonatomic) IBOutlet UIButton *sportBtn;
@property (weak, nonatomic) IBOutlet UIButton *doushiBtn;
@property (weak, nonatomic) IBOutlet UIButton *nengliangjiBtn;


@property (weak, nonatomic) IBOutlet UIButton *douguanBtn;
@property (weak, nonatomic) IBOutlet UIButton *jiaoshuiBtn;

@property (weak, nonatomic) IBOutlet UILabel *visitLbl;
@property (weak, nonatomic) IBOutlet UIView *visitView;

@property (weak, nonatomic) IBOutlet UIImageView *treeImgView;
@property (weak, nonatomic) IBOutlet UIImageView *birdImgView;
@property (weak, nonatomic) IBOutlet UIImageView *daojuImgView;
@property (weak, nonatomic) IBOutlet UIImageView *treeAnimationImgView;

@property (weak, nonatomic) IBOutlet UIImageView *sportBubble;
@property (weak, nonatomic) IBOutlet UIImageView *shopBubble;
@property (weak, nonatomic) IBOutlet UIImageView *inviteBubble;
@property (weak, nonatomic) IBOutlet UIImageView *readBubble;
@property (weak, nonatomic) IBOutlet UIImageView *baohuzhaoTimeView;
@property (weak, nonatomic) IBOutlet UIImageView *baohuzhaoImgView;
@property (weak, nonatomic) IBOutlet UIImageView *creditBubble;
@property (weak, nonatomic) IBOutlet UIImageView *tuijianBubble;
@property (weak, nonatomic) IBOutlet UIImageView *share_bubble;
@property (weak, nonatomic) IBOutlet UIImageView *gongxiang_bubble;
@property (weak, nonatomic) IBOutlet UIImageView *redBubble;
@property (weak, nonatomic) IBOutlet UIImageView *nljBubble;

@property (weak, nonatomic) IBOutlet UIImageView *girlSignBubble;


@property (nonatomic, strong) UILabel *sportEnergyLbl;
@property (nonatomic, strong) UILabel *shopEnergyLbl;
@property (nonatomic, strong) UILabel *inviteEnergyLbl;
@property (nonatomic, strong) UILabel *readEnergyLbl;
@property (nonatomic, strong) UILabel *creditLbl;
@property (nonatomic, strong) UILabel *tuijianLbl;
@property (nonatomic, strong) UILabel *shareLbl;
@property (nonatomic, strong) UILabel *gongxiangLbl;
@property (nonatomic, strong) UILabel *nljLbl;
@property (nonatomic, strong) UILabel *redEnergyLbl;
@property (weak, nonatomic) IBOutlet UILabel *girlSignLbl;





@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bubble1Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bubble2Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bubble3Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bubble4Top;

@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *nljTap;

@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *sportTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *shopTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *inviteTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *readTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *redTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *jindouAnimationTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *creditTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *tuijianTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *gongxiang_tap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *share_tap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *signTap;


@property (nonatomic, assign) BOOL hiddenBtns;
@property (weak, nonatomic) IBOutlet UIButton *openJindouBtn;

@property (nonatomic, strong) MYEnergyModel *energyModel;
@property (nonatomic, strong) NSString *baohuzhaoTime;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *treeViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *animationViewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *animationViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *animationViewHeight;

@property (weak, nonatomic) IBOutlet UIImageView *mayiBanner;
@property (nonatomic, strong) JindouBLK jindouBlk;

@property (nonatomic, assign) TreeGrowingStage stage;
@property (nonatomic, assign) BOOL canPickJindou;
- (void)beginTreeGrwoingAnimationWithStage:(TreeGrowingStage)stage ;
- (void)showTreeShakeAnimation;
- (void)bubbleAnimation;
- (void)beginBirdFlyAnimation;
- (void)showJindouAnimation;

- (void)showShifeiAnimationWithFinishBlk:(FinishBLK)finishBlk;
- (void)showJiaoshuiAnimationWithFinishBlk:(FinishBLK)finishBlk;
- (void)showBaohuzhaoAnimationWithFinishBlk:(FinishBLK)finishBlk;
@end
