//
//  CCRankCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyRankCell.h"

@implementation CCMyRankCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(MYInviteModel *)model {
    _model = model;
    [self.avatar setImageWithURL:URL(model.avatar) placeholder:IMAGENAMED(@"head")];
    self.nameLbl.text = model.nickname;
    self.energyLbl.text  = [NSString stringWithFormat:@"%@步",STRINGEMPTY(model.steps) ? @"0" : model.steps];
    self.numLbl.text = [NSString stringWithFormat:@"%@",@(model.index)];
    self.numLbl.hidden = model.index < 3;
    self.leftImgView.hidden = model.index >= 3;
    if (model.index < 3) {
        NSString *imageStr = model.index == 0 ? @"gold" : model.index == 1 ? @"yinpai" : @"tongpai";
        self.leftImgView.image = IMAGENAMED(imageStr);
    }
    self.likeCount.text = model.praise_num;
    self.likedImgView.image = model.is_praised ? IMAGENAMED(@"dianzan_select") : IMAGENAMED(@"dianzan");
    self.likeCount.textColor = model.is_praised ? MTRGB(0xfd885e) : MTRGB(0x435e73);
}
@end
