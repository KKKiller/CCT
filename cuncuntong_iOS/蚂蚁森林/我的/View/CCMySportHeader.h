//
//  CCMySportHeader.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCProgressView.h"
@interface CCMySportHeader : UIView
+ (instancetype)instanceView ;
@property (weak, nonatomic) IBOutlet CCProgressView *borderView;
@property (weak, nonatomic) IBOutlet UIImageView *sportCircleImgView;

@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (weak, nonatomic) IBOutlet UILabel *energyLbl;
@end
