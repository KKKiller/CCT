//
//  CCMyShopCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/26.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyShopCell.h"
#import "NSMutableAttributedString+Style.h"
@implementation CCMyShopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(CCWineModel *)model {
    _model = model;
    
    self.guigeLbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.guigeLbl.layer.borderWidth = 0.5;
    self.guigeLbl.layer.cornerRadius = 8;
    self.guigeLbl.layer.masksToBounds = YES;
    
    [self.imgView setImageWithURL:URL(model.image) placeholder:IMAGENAMED(@"ph")];
    self.titleLbl.text = model.name;
    self.guigeLbl.text = [NSString stringWithFormat:@" 能量%ldg  ",[model.price intValue] * 30];//
    NSString *price = [NSString stringWithFormat:@"价格: 每件%@元",model.price];
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc]initWithText:price color:TEXTBLACK6 subStrIndex:NSMakeRange(6, price.length - 6) subStrColor:ORANGECOLOR];
    self.priceLbl.attributedText = attr1;
    
    NSString *stock = [NSString stringWithFormat:@"股份: 每瓶%@股",model.stocks];
    NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc]initWithText:stock color:TEXTBLACK6 subStrIndex:NSMakeRange(4, stock.length - 4) subStrColor:MAINBLUE];
    self.gufenLbl.attributedText = attr2;
}
@end
