//
//  CCProgressView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/7/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCProgressView : UIView
@property (nonatomic, assign) CGFloat progress;
@end
