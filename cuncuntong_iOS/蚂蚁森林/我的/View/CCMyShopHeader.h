//
//  CCMyShopHeader.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/26.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCMyShopHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
+ (instancetype)instanceView;
@end
