//
//  CCMyEnergyTagView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/26.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyEnergyTagView.h"

@implementation CCMyEnergyTagView
+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}
- (void)setUserInfo:(MYUserInfoModel *)userInfo {
    _userInfo = userInfo;
    self.titleLbl.text = userInfo.energy ? [NSString stringWithFormat:@"%@g", userInfo.energy] : @"";
    self.titleLbl.textAlignment = NSTextAlignmentCenter;
    [self.imgView setImageWithURL:URL(userInfo.avatar) placeholder:IMAGENAMED(@"head")];
}
@end
