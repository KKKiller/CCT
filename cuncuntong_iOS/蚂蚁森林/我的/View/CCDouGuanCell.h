//
//  CCDouGuanCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCDouGuanCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (nonatomic, assign) BOOL isGrowing;
@end
