//
//  CCRankCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYInviteModel.h"
@interface CCMyRankCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (weak, nonatomic) IBOutlet UILabel *energyLbl;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIImageView *likedImgView;
@property (weak, nonatomic) IBOutlet UILabel *likeCount;
//@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *likeTap;
@property (nonatomic, strong) MYInviteModel *model;
@end
