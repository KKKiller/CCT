//
//  CCMyShopHeader.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/26.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyShopHeader.h"

@implementation CCMyShopHeader

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}

@end
