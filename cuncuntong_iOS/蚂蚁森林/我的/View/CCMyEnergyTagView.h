//
//  CCMyEnergyTagView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/26.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYUserInfoModel.h"
@interface CCMyEnergyTagView : UIView
+ (instancetype)instanceView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) MYUserInfoModel *userInfo;
@end
