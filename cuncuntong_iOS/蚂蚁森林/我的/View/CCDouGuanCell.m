//
//  CCDouGuanCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/30.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCDouGuanCell.h"

@implementation CCDouGuanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setIsGrowing:(BOOL)isGrowing {
    self.titleLbl.hidden = !isGrowing;
    self.imgView.image = isGrowing ? IMAGENAMED(@"dou_grow") : IMAGENAMED(@"dou_finished");
}
@end
