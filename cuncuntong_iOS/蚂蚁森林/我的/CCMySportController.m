//
//  CCMySportController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMySportController.h"
#import "CCMyRankCell.h"
#import "CCMySportHeader.h"
#import "CCMyEnergyTagView.h"
#import "CCMyRankCell.h"
#import "MYUserInfoModel.h"
#import "UIView+Border.h"
#import "CALayer+Border.h"
#import "CCPKController.h"
#import "StepManager.h"
static NSString *kCCMyRankCell = @"CCMyRankCellId";
@interface CCMySportController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCMySportHeader *headerView;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) CCMyEnergyTagView *energyTagView;
@property (nonatomic, strong) UIImageView *redDotImgView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, strong) NSTimer *stepTimer;
@property (nonatomic, assign) NSInteger steps;
@end

@implementation CCMySportController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.backBtn];
    [self initRefreshView];
    [self.view addSubview:self.energyTagView];
    self.energyTagView.userInfo = self.userInfo;
    [self loadData];
//    [App_Delegate updateSetps:^(NSInteger steps) {
//        [self getStepNumber];
//    }];
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getStepNumber];
    [[StepManager sharedManager] startWithStep];
    self.stepTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getStepNumber) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.stepTimer forMode:NSRunLoopCommonModes];
}

- (void)getStepNumber {
    NSInteger step = [StepManager sharedManager].step;
    self.steps = App_Delegate.steps + step;
    [self setHeaderNum];
//    [App_Delegate reportSteps:self.steps];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.stepTimer invalidate];
    self.stepTimer  = nil;
}
- (void)setHeaderNum {
    NSInteger totolStep = self.steps == 0 ? App_Delegate.steps : self.steps;
    NSInteger energy = MIN( MAX(totolStep - 5000, 0) / 100, 50);
    if (energy > 0) {
        self.headerView.energyLbl.text = [NSString stringWithFormat:@"今日能量%@g",@(50  + energy)];
    }else{
        self.headerView.energyLbl.text = @"今日能量0g";
    }
    
    self.headerView.numLbl.text = [NSString stringWithFormat:@"%ld",self.steps];
    CGFloat progress = (float)self.steps / 10000.0;
        
    if(progress < 0.1 && progress > 0){
        progress = 0.1;
    }else if (progress > 1){
        progress = 1;
    }
    self.headerView.borderView.progress = progress;
    CGPoint center = self.headerView.sportCircleImgView.center;
    CGPoint point = [self calcCircleCoordinateWithCenter:center andWithAngle:progress*360 + 270 andWithRadius:84];

    self.redDotImgView.center = point;
    self.redDotImgView.hidden = progress == 0;
    [self.headerView.borderView setNeedsDisplay];
}
- (void)showNoStepAlert {
    if (App_Delegate.steps > 0) {
        return;
    }
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请打开手机里的“健康”APP，在数据来源里为全球村村通开启步数权限，这样才能正常获取到您的步数信息" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
//    [alert show];
}

- (void)loadData {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/energy_sport_rank") params:@{@"rid":USERID,@"page":@(self.pageIndex),@"perpage":@"30"} success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)] && success[@"data"]) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            self.userInfo = [MYUserInfoModel modelWithJSON:success[@"data"][@"userinfo"]];
            self.energyTagView.userInfo =self.userInfo;
//            self.num = [success[@"data"][@"num"] integerValue];
//            self.headerView.numLbl.text = [NSString stringWithFormat:@"%@",@(self.num)];
            for (NSDictionary *dict in success[@"data"][@"data"]) {
                MYInviteModel *model = [MYInviteModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
        }
        [self endLoding:nil];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    [TOOL hideLoading:self.view];
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (void)tapAtLike:(MYInviteModel *)model {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/energy_sport_praise") params:@{@"rid":model.rid,@"log_id":STR(model.id)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"点赞成功");
            model.is_praised = YES;
            self.pageIndex = 1;
            [self loadData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyRankCell *cell = [tableView dequeueReusableCellWithIdentifier:kCCMyRankCell];
    MYInviteModel *model = self.dataArray[indexPath.row];
    model.index = indexPath.row;
    cell.model = model;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
         [self tapAtLike:model];
    }];
    [cell.likedImgView addGestureRecognizer: tap];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MYInviteModel *model = self.dataArray[indexPath.row];
    if ([model.rid isEqualToString:USERID]) {
        SHOW(@"不能PK自己哦～");
        return;
    }
    CCPKController *vc = [[CCPKController alloc]init];
    vc.uid = model.rid;
    [self.navigationController pushViewController:vc animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 44;
        _tableView.tableHeaderView = self.headerView;
        _tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
        _tableView.rowHeight = UITableViewAutomaticDimension;
        //        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellId];
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyRankCell" bundle:nil] forCellReuseIdentifier:kCCMyRankCell];
        
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CCMySportHeader *)headerView {
    if (!_headerView) {
        _headerView = [CCMySportHeader instanceView];
        _headerView.frame = CGRectMake(0, 0, App_Width, 535);
    }
    return _headerView;
}
- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, isIphoneX ? 40 : 20, 30, 30)];
        [_backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [_backBtn setImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    }
    return _backBtn;
}
- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 27, App_Width, 20)];
        _titleLbl.text = @"我的运动";
        _titleLbl.font = FFont(20);
        _titleLbl.textColor = TEXTBLACK3;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _titleLbl;
}
- (CCMyEnergyTagView *)energyTagView {
    if (!_energyTagView) {
        CGFloat width = 70;
        _energyTagView = [CCMyEnergyTagView instanceView];
        _energyTagView.frame = CGRectMake(App_Width -width, 30, width, 35);
        _energyTagView.imgView.layer.borderColor = WHITECOLOR.CGColor;
        _energyTagView.imgView.layer.borderWidth = 1;
    }
    return _energyTagView;
}
- (UIImageView *)redDotImgView {
    if (!_redDotImgView) {
        _redDotImgView = [[UIImageView alloc]initWithImage:IMAGENAMED(@"yundong_quan")];
        _redDotImgView.size = CGSizeMake(22, 22);
        [self.headerView addSubview:_redDotImgView];
        _redDotImgView.hidden = YES;
    }
    return _redDotImgView;
}
- (CGPoint)calcCircleCoordinateWithCenter:(CGPoint)center  andWithAngle:(CGFloat) angle andWithRadius:(CGFloat)radius{
    CGFloat x2 = radius*cosf(angle*M_PI/180);
    CGFloat y2 = radius*sinf(angle*M_PI/180);
    return CGPointMake(center.x+x2, center.y+y2);
}
@end
