//
//  CCMyShopController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyShopController.h"
#import "CCMyShopHeader.h"
#import "CCMyEnergyTagView.h"
#import "CCWineModel.h"
#import "CCMyShopCell.h"
#import "CCBuyDetailController.h"
static NSString *kCellId = @"CCMyShopCell";
@interface CCMyShopController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCMyShopHeader *header;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) CCMyEnergyTagView *energyTagView;

@end

@implementation CCMyShopController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.backBtn];
    [self.view addSubview:self.energyTagView];
    [self initRefreshView];
    [self loadData];
    self.energyTagView.userInfo = self.userInfo;
    
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Fallback on earlier versions
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/wine_list") params:@{@"page":@(self.pageIndex),@"rid":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"code"]  isEqual: @(1)]) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"msg"]) {
                CCWineModel *model = [CCWineModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
            [self endLoding:success[@"msg"]];
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
    //今日购买数
//    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/buy_rank") params:@{@"rid":USERID} success:^(NSDictionary *success) {
//        if ([success[@"error"] integerValue] == 0) {
//            if ([success containsObjectForKey:@"data"]) {
//                self.header.moneyLbl.text = [NSString stringWithFormat:@"%@", success[@"data"][@"num"]];
//            }else{
//                self.header.moneyLbl.text = @"0";
//            }
//        }
//    } failure:^(NSError *failure) {
//
//    }];
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/check") params:@{@"rid":USERID} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSDictionary *data = success[@"data"] ;
            if ( [data.allKeys containsObject:@"buy_energy"]) {
                NSArray *array = [data valueForKey:@"buy_energy"];
                NSInteger total = 0;
                for (NSDictionary *dict in array) {
                    total += [dict[@"energy"] integerValue];
                }
                self.header.moneyLbl.text = [NSString stringWithFormat:@"%@",@(total)];
            }else{
                self.header.moneyLbl.text = @"0";
            }
        }
    } failure:^(NSError *failure) {
        
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyShopCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
    cell.model = self.dataArray[indexPath.row];
    cell.buyBtn.tag = indexPath.row;
    cell.buyBtn.userInteractionEnabled = NO;
    [cell.buyBtn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCWineModel *model = self.dataArray[indexPath.row];
        CCBuyDetailController *vc = [[CCBuyDetailController alloc]init];
        vc.wineModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}
- (void)buyBtnClick:(UIButton *)sender {
    NSInteger tag = sender.tag;
    CCWineModel *model = self.dataArray[tag];
    

}
-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 80;
        _tableView.tableHeaderView = self.header;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
        _tableView.rowHeight = UITableViewAutomaticDimension;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyShopCell" bundle:nil] forCellReuseIdentifier:kCellId];
        
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CCMyShopHeader *)header {
    if (!_header) {
        _header = [CCMyShopHeader instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 300);
    }
    return _header;
}
- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, isIphoneX ? 40 : 20, 30, 30)];
        [_backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [_backBtn setImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    }
    return _backBtn;
}
- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 27, App_Width, 20)];
        _titleLbl.text = @"我的购物";
        _titleLbl.font = FFont(20);
        _titleLbl.textColor = TEXTBLACK3;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _titleLbl;
}
- (CCMyEnergyTagView *)energyTagView {
    if (!_energyTagView) {
        CGFloat width = 65;
        _energyTagView = [CCMyEnergyTagView instanceView];
        _energyTagView.frame = CGRectMake(App_Width -width, 30, width, 35);
        _energyTagView.imgView.layer.borderColor = WHITECOLOR.CGColor;
        _energyTagView.imgView.layer.borderWidth = 1;
    }
    return _energyTagView;
}
@end
