//
//  CCMySportController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "MYUserInfoModel.h"
@interface CCMySportController : BaseViewController
@property (nonatomic, strong) MYUserInfoModel *userInfo;
@end
