//
//  CCMyInviteController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyInviteController.h"
#import "CCRankCell.h"
#import "CCmyInviteHeader.h"
#import "CCMyEnergyTagView.h"
#import "MYInviteModel.h"
#import "MYUserInfoModel.h"
#import "CCMyRankCell.h"
#import "MYInviteModel.h"
#import "CCPKController.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "CCShareView.h"
static NSString *kCellId = @"CCMyRankCell";
@interface CCMyInviteController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCMyInviteHeader *header;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) CCMyEnergyTagView *energyTagView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, strong) NSString *invite_num;
@property (nonatomic, strong) CCShareView *shareView;

@end

@implementation CCMyInviteController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.backBtn];
    [self.view addSubview:self.shareView];
    [self initRefreshView];
    [self.view addSubview:self.energyTagView];
    self.energyTagView.userInfo = self.userInfo;
    [self loadData];

}
- (void)loadData {
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/invite_rank") params:@{@"rid":USERID,@"page":[NSString stringWithFormat:@"%@",@(self.pageIndex)],@"perpage":@"30"} success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)] && success[@"data"]) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            self.userInfo = [MYUserInfoModel modelWithJSON:success[@"data"][@"userinfo"]];
            self.energyTagView.userInfo =self.userInfo;
            self.num = [success[@"data"][@"num"] integerValue];
            self.invite_num = [NSString stringWithFormat:@"%@", success[@"data"][@"invite_num"] ];
            self.header.numLbl.text = self.invite_num;
            for (NSDictionary *dict in success[@"data"][@"data"]) {
                MYInviteModel *model = [MYInviteModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
        }
        [self endLoding:nil];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [TOOL hideLoading:self.view];
    [self.tableView.mj_header endRefreshing];
    if (array.count < 30) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
    [self.tableView reloadData];
}
- (void)tapAtLike:(MYInviteModel *)model {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/invite_praise") params:@{@"rid":model.rid,@"log_id":STR(model.id)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"点赞成功");
            model.is_praised = YES;
            self.pageIndex = 1;
            [self loadData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyRankCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
    MYInviteModel *model = self.dataArray[indexPath.row];
    model.index = indexPath.row;
    cell.model = model;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self tapAtLike:model];
    }];
    [cell.likedImgView addGestureRecognizer:tap];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MYInviteModel *model = self.dataArray[indexPath.row];
    if ([model.rid isEqualToString:USERID]) {
        SHOW(@"不能PK自己哦～");
        return;
    }
    CCPKController *vc = [[CCPKController alloc]init];
    vc.uid = model.rid;
    [self.navigationController pushViewController:vc animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}

#pragma mark - 分享
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [self sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    // 设置短信内容
    NSString *url = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    NSString *shareText = [@"想长寿，种金豆，金豆收一斗，能活九十九，走路能赚钱，种豆真好玩！注册地址：" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, url];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
    
}


- (CCShareView *)shareView {
    if (!_shareView) {
        _shareView = [CCShareView instanceView];
        _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareView;
}
//分享
- (void)share {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height;
    }];
}
- (void)weixinBtnClick {
    [self shareCancelBtnClick];
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@""]]];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: @"邀你种金豆！"  descr:@"想长寿，种金豆，金豆收一斗，能活九十九，走路能赚钱，种豆真好玩！" thumImage:IMAGENAMED(@"weixinIcon")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"%@village/public/center/qrcodereg?iid=%@",App_Delegate.shareBaseUrl,USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:UMSocialPlatformType_WechatSession messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
    }];
}

#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 44;
        _tableView.tableHeaderView = self.header;
        _tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
        _tableView.rowHeight = UITableViewAutomaticDimension;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyRankCell" bundle:nil] forCellReuseIdentifier:kCellId];

    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CCMyInviteHeader *)header {
    if (!_header) {
        _header = [CCMyInviteHeader instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 390);
        [_header.shareBtn addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    }
    return _header;
}
- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 20, 30, 30)];
        [_backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [_backBtn setImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    }
    return _backBtn;
}
- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 27, App_Width, 20)];
        _titleLbl.text = @"我的邀请";
        _titleLbl.font = FFont(20);
        _titleLbl.textColor = TEXTBLACK3;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _titleLbl;
}
- (CCMyEnergyTagView *)energyTagView {
    if (!_energyTagView) {
        CGFloat width = 70;
        _energyTagView = [CCMyEnergyTagView instanceView];
        _energyTagView.frame = CGRectMake(App_Width -width, 30, width, 35);
        _energyTagView.imgView.layer.borderColor = WHITECOLOR.CGColor;
        _energyTagView.imgView.layer.borderWidth = 1;
    }
    return _energyTagView;
}
@end
