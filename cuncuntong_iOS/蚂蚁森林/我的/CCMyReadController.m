//
//  CCMyReadController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/6/25.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyReadController.h"
#import "CCMyReadCell.h"
#import "CCMyReaderHeader.h"
#import "CCMyEnergyTagView.h"
#import "CCHomeModel.h"
#import "CCHomePicCell.h"
#import "CCHome3PicCell.h"
#import "DetailViewController.h"
static NSString *kCCHomePicCell = @"CCHomePicCell";
static NSString *kCCHome3PicCell = @"CCHome3PicCell";
static NSString *kCellId = @"CCMyReadCell";
@interface CCMyReadController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCMyReaderHeader *header;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) CCMyEnergyTagView *energyTagView;

@end

@implementation CCMyReadController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.backBtn];
    [self initRefreshView];
    [self.view addSubview:self.energyTagView];
    [self loadData];
    self.energyTagView.userInfo = self.userInfo;

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self loadReadCount];
}
- (void)loadData {
    [TOOL showLoading:self.view];
    NSDictionary *paramDic = @{ @"page": [NSString stringWithFormat:@"%ld", self.pageIndex]};
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"collect/collect") params:paramDic target: nil success:^(NSDictionary *success) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [TOOL saveDataWithData:success Name:@"homeList"];
        });
        if (self.pageIndex == 1) {
            [self.dataArray removeAllObjects];
        }
        [self setData:success];
        
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        [TOOL hideLoading: self.view];
    } failure:^(NSError *failure) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];

        [TOOL hideLoading: self.view];
    }];
    
    
}
-  (void)loadReadCount {
    //今日阅读数
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"bean/read_rank") params:@{@"rid":USERID} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            if ([success containsObjectForKey:@"data"]) {
                self.header.numLbl.text = [NSString stringWithFormat:@"%@", success[@"data"][@"read_num"]];
            }else{
                self.header.numLbl.text = @"0";
            }
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)setData:(NSDictionary *)success {
    if ([success[@"code"] integerValue] == 1) {
        for (NSDictionary *dic in success[@"msg"]) {
            CCHomeModel *model = [CCHomeModel modelWithDictionary:dic];
            if (model.type != 3) {
                [self.dataArray addObject:model];
            }
        }
        [_tableView reloadData];
    }
}



//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCHomeModel *model = self.dataArray[indexPath.row];
    if (model.type == 2 && model.coverArr.count >= 3){
        CCHome3PicCell *cell = [tableView dequeueReusableCellWithIdentifier:kCCHome3PicCell];
        cell.model = model;
        return cell;
    }else{
        CCHomePicCell *cell = [tableView dequeueReusableCellWithIdentifier:kCCHomePicCell];
        cell.model = model;
        return cell;
    }
    return [UITableViewCell new];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CCHomeModel *model = self.dataArray[indexPath.row];
    DetailViewController *vc = [[DetailViewController alloc] init];
    vc.articleId = model.id;
    [self.navigationController pushViewController:vc animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCHomeModel *model = self.dataArray[indexPath.row];
    if (model.type == 2 && model.coverArr.count >= 3){
        CGFloat height = [model.title sizeForFont:FFont(17) size:CGSizeMake(App_Width - 30, CGFLOAT_MAX) mode:NSLineBreakByCharWrapping].height;
        return height > 30 ? 185 : 170;
    }else if (model.type == 1){ //视频
        CGFloat height = [model.title sizeForFont:FFont(17) size:CGSizeMake(App_Width - 30, CGFLOAT_MAX) mode:NSLineBreakByCharWrapping].height;
        CGFloat videoH = (App_Width - 30) * 9 / 16.0;
        return height > 30 ? 105 + videoH : 90 + videoH;
    }else{
        return 115;
    }
    return CGFLOAT_MIN;
}
- (void)readBtnClick {
    [self.tableView setContentOffset:CGPointMake(0, self.header.height - 75) animated:YES];
}
-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 120;
        _tableView.rowHeight = 120;
        _tableView.tableHeaderView = self.header;
        _tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
        _tableView.rowHeight = UITableViewAutomaticDimension;
        [_tableView registerNib:[UINib nibWithNibName:@"CCHomePicCell" bundle:nil] forCellReuseIdentifier:kCCHomePicCell];
        [_tableView registerNib:[UINib nibWithNibName:@"CCHome3PicCell" bundle:nil] forCellReuseIdentifier:kCCHome3PicCell];
        
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CCMyReaderHeader *)header {
    if (!_header) {
        _header = [CCMyReaderHeader instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 360);
        [_header.readBtn addTarget:self action:@selector(readBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _header;
}
- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 20, 30, 30)];
        [_backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [_backBtn setImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    }
    return _backBtn;
}
- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 27, App_Width, 20)];
        _titleLbl.text = @"我的阅读";
        _titleLbl.font = FFont(20);
        _titleLbl.textColor = TEXTBLACK3;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _titleLbl;
}
- (CCMyEnergyTagView *)energyTagView {
    if (!_energyTagView) {
        CGFloat width = 70;
        _energyTagView = [CCMyEnergyTagView instanceView];
        _energyTagView.frame = CGRectMake(App_Width -width, 30, width, 35);
        _energyTagView.imgView.layer.borderColor = WHITECOLOR.CGColor;
        _energyTagView.imgView.layer.borderWidth = 1;
    }
    return _energyTagView;
}
@end
