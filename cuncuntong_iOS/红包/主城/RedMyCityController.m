//
//  RedMyCityController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedMyCityController.h"
#import "RedMyCityCell.h"
#import "RedMyCityView.h"
#import "RedCityInfoModel.h"
#import "RedCitySailListController.h"
#import "RedCityHomeController.h"
static NSString *cellID = @"RedMyCityCell";

@interface RedMyCityController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) RedMyCityView *header;
@property (nonatomic, assign) NSInteger type;
@end

@implementation RedMyCityController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title  = @"我的主城";
    [self setRightBtn:@"主城交易大厅"];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    [self initRefreshView];
    
//    [self loadData];
//    [self loadCityData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);
    [self refreshData];
}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
    [self loadCityData];
}
- (void)rightBtnClick {
    RedCitySailListController *vc = [[RedCitySailListController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getOneCastellanMsg") params:@{@"r_id":USERID} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            self.header.todayMoney.text = [NSString stringWithFormat:@"%@", success[@"data"][@"red_today"]];
            self.header.todaySailLbl.text =  [NSString stringWithFormat:@"%@", success[@"data"][@"castellan_today_sum"]];
            self.header.totalMoneyLbl.text = [NSString stringWithFormat:@"%@", success[@"data"][@"red_today_sum"]];
            self.header.totalSailLbl.text =  [NSString stringWithFormat:@"%@", success[@"data"][@"castellan_today"]];
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)loadCityData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getThisCastellanType") params:@{@"r_id":USERID,@"offset":@(self.dataArray.count),@"type":@(self.type)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            for (NSDictionary *dict in success[@"data"]) {
                RedCityInfoModel *model = [RedCityInfoModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
            [self endLoding:success[@"data"]];
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
//    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RedMyCityCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.dataArray.count > indexPath.row) {
        cell.type = self.type;
        cell.model = self.dataArray[indexPath.row];
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        RedCityInfoModel *model = self.dataArray[indexPath.row];
        RedCityHomeController *vc = [[RedCityHomeController alloc]init];
        vc.cityId = model.city;
        vc.cityName = model.shi_name;
        vc.provinceName = model.sheng_name;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)headerTypeChange:(UIButton *)sender {
    self.header.sailCityBtn.selected = NO;
    self.header.myCityBtn.selected = NO;
    sender.selected = YES;
    self.type = sender.tag;
    self.header.type = self.type;
    [self refreshData];
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadCityData];
    }];
}
#pragma mark - 懒加载
- (RedMyCityView *)header {
    if (!_header) {
        _header = [RedMyCityView instanceView];
        _header.frame = CGRectMake(0, 64, App_Width, 320);
        [_header.myCityBtn addTarget:self action:@selector(headerTypeChange:) forControlEvents:UIControlEventTouchUpInside];
        [_header.sailCityBtn addTarget:self action:@selector(headerTypeChange:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _header;
}
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 80;
        _tableView.tableHeaderView = self.header;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"RedMyCityCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:self.tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
