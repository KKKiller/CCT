//
//  RedCityPayControlle.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedCityPayController.h"
#import "RedCityPayView.h"
#import "RechargeViewController.h"
@interface RedCityPayController ()
@property (nonatomic, strong) RedCityPayView *payView;
@property (nonatomic, strong) UIButton *payBtn;
@property (nonatomic, strong) RedCityInfoModel *cityInfoModel;

@end

@implementation RedCityPayController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xf2f2f2);
    self.title = @"支付";
    [self.view addSubview:self.payView];
    [self.view addSubview:self.payBtn];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = RED;
}
- (void)loadData {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getCastellanInfo") params:@{@"r_id":USERID,@"city_id":STR(self.cityId)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.cityInfoModel = [RedCityInfoModel modelWithJSON:success[@"data"]];
            self.payView.moneyLbl.text = self.cityInfoModel.buy_sell_money;
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)payBtnClick {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/buyCastellan") params:@{@"r_id":USERID,@"pro":STR(self.provinceId),@"city":STR(self.cityId)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            if (self.paySuccessBlock) {
                self.paySuccessBlock(self.cityId,self.provinceId,self.locationName);
            }
            SHOW(@"交易成功");
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            if ([success[@"msg"] containsString:@"钱少"]) {
//                SHOW(@"请充够所需金额后，再点击支付购买城主！");
                RechargeViewController *vc = [[RechargeViewController alloc]init];
                vc.money = [NSString stringWithFormat:@"%.2f", [self.cityInfoModel.buy_sell_money floatValue] - [[UserManager sharedInstance].money floatValue]];
                WEAKSELF
                vc.block = ^{
//                    if ([self.cityInfoModel.buy_sell_money floatValue] <= [[UserManager sharedInstance].money floatValue]) {
                        [weakSelf payBtnClick];
//                    }else{
//                        SHOW(@"余额不足，请充值");
//                    }
                };
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                SHOW(success[@"msg"]);
            }
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (RedCityPayView *)payView {
    if (!_payView) {
        _payView = [RedCityPayView instanceView];
        _payView.frame = CGRectMake(0, 64, App_Width, 220);
        _payView.cityLbl.text = self.locationName;
    }
    return _payView;
}
- (UIButton *)payBtn {
    if (!_payBtn) {
        _payBtn = [[UIButton alloc]initWithTitle:@"支付" textColor:WHITECOLOR backImg:nil font:14];
        _payBtn.backgroundColor = RED;

        [_payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _payBtn.layer.cornerRadius = 15;
        _payBtn.layer.masksToBounds = YES;
        
        _payBtn.frame = CGRectMake((App_Width - 200)*0.5, CGRectGetMaxY(self.payView.frame) + 20, 200, 30);
    }
    return _payBtn;
}


@end
