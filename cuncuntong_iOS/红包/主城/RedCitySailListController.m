//
//  RedCityListController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedCitySailListController.h"
#import "RedCityCell.h"
#import "RedSailListModel.h"
#import "BRAddressPickerView.h"
#import "BRAddressModel.h"
#import "RedCityPayController.h"
static NSString *cellID = @"RedCityCell";

@interface RedCitySailListController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *becomeCityer;

@property (nonatomic, assign) NSInteger offset;
@end

@implementation RedCitySailListController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    [self initRefreshView];
    self.title = @"主城交易大厅";
    [self setRightBtn:@"主城排行榜"];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 80, 0);
    [self.view addSubview:self.becomeCityer];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = RED;
    [self refreshData];
}
//主城排行榜
- (void)rightBtnClick {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.navColor = RED;
    vc.titleStr = @"主城排行榜";
    vc.topOffset = -48;
    vc.hiddenShare = YES;
    vc.urlStr = @"http://www.cct369.com/village/public/mfkit/red/webCastellanRankingList";
    vc.actionBlk = ^{
        [self.navigationController popViewControllerAnimated:YES];
        [self becomeCityerBtnClick];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/castellanInfo") params:@{@"offset":[NSString stringWithFormat:@"%ld",self.dataArray.count]} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            for (NSDictionary *dict in success[@"data"]) {
                RedSailListModel *model = [RedSailListModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
            [self endLoding:success[@"data"]];
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
//    if(array.count < 30){
//        [self.tableView.mj_footer endRefreshingWithNoMoreData];
//    }else{
        [self.tableView.mj_footer endRefreshing];
//    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RedCityCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.dataArray.count > indexPath.row) {
        cell.model = self.dataArray[indexPath.row];        
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

//成为城主
- (void)becomeCityerBtnClick{
    //选择城市
    [BRAddressPickerView showAddressPickerWithDefaultSelected:nil isAutoSelect:NO resultBlock:^(NSArray *selectAddressArr) {
        BRProvinceModel *province = selectAddressArr[0];
        BRProvinceModel *city = selectAddressArr[1];
        RedCityPayController *vc = [[RedCityPayController alloc]init];
        vc.cityId = city.id;
        vc.provinceId = province.id;
        vc.locationName = [NSString stringWithFormat:@"%@%@",province.name,city.name];
        WEAKSELF
        vc.paySuccessBlock = ^(NSString *cityId,NSString *provinceId,NSString *locationName) {
            [weakSelf refreshData];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }];
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
        CGFloat yy = system >= 11.0 ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 190;
        _tableView.allowsSelection = NO;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
        [_tableView registerNib:[UINib nibWithNibName:@"RedCityCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:self.tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
- (UIButton *)becomeCityer {
    if (!_becomeCityer) {
        _becomeCityer = [[UIButton alloc]initWithTitle:@"我要成为城主" textColor:WHITECOLOR backImg:nil font:12];
        [_becomeCityer setImage:IMAGENAMED(@"city_lord") forState:UIControlStateNormal];
        CGFloat width = 200;
        _becomeCityer.frame = CGRectMake((App_Width - width)*0.5, App_Height - 40, width, 30);
        _becomeCityer.backgroundColor = RED;
        [_becomeCityer setImageEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 25)];
        [_becomeCityer setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [_becomeCityer addTarget:self action:@selector(becomeCityerBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _becomeCityer.layer.cornerRadius = 15;
        _becomeCityer.layer.masksToBounds = YES;
    }
    return _becomeCityer;
}
@end
