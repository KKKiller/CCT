//
//  RedMyCityView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedMyCityView.h"

@implementation RedMyCityView

+ (instancetype)instanceView {
    RedMyCityView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}

@end
