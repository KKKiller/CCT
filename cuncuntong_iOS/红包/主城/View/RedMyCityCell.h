//
//  RedMyCityCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedCityInfoModel.h"
@interface RedMyCityCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
//@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *sailPriceLBl;
@property (weak, nonatomic) IBOutlet UILabel *buyPriceLbl;

@property (weak, nonatomic) IBOutlet UILabel *leftTagLbl;
@property (weak, nonatomic) IBOutlet UILabel *rightTagLbl;

@property (nonatomic, strong) RedCityInfoModel *model;
@property (nonatomic, assign) NSInteger type;
@end
