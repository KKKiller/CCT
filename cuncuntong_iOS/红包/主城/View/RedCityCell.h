//
//  RedCityCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedSailListModel.h"
@interface RedCityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *sailerImgView;
@property (weak, nonatomic) IBOutlet UILabel *sailerNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLbl;
@property (weak, nonatomic) IBOutlet UIImageView *buyerImgView;
@property (weak, nonatomic) IBOutlet UILabel *buyerNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *nowPriceLbl;

@property (weak, nonatomic) IBOutlet UILabel *earnMoneyLBl;
@property (nonatomic, strong) RedSailListModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *sailerNobody;
@property (weak, nonatomic) IBOutlet UIImageView *buyerNobody;

@end
