//
//  RedCityHomeView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedCityHomeView.h"

@implementation RedCityHomeView

+ (instancetype)instanceView {
    RedCityHomeView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}
- (void)setModel:(RedCityInfoModel *)model {
    _model = model;
    NSString *url = [NSString stringWithFormat:@"http://cm4.wanshitong.net/village/public/images/%@",model.user_portrait];
    [self.headImgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"head")];
    self.nameLbl.text = model.user_realname ?: @"城主虚位以待";
    self.moneyLbl.text = model.buy_sell_money;
    [self.recommandBtn setImageWithURL:URL(model.img) forState:UIControlStateNormal placeholder:IMAGENAMED(@"bulletin_board")];
    self.recommandBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
}
@end
