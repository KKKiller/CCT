//
//  RedCityPayView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedCityPayView.h"

@implementation RedCityPayView

+ (instancetype)instanceView {
    RedCityPayView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}

@end
