//
//  RedCityCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedCityCell.h"

@implementation RedCityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(RedSailListModel *)model {
    _model = model;
    self.addressLbl.text = [NSString stringWithFormat:@"%@%@",STR(model.sheng_name), STR(model.v_name)];
    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/images/%@",model.del_user_portrait];
    [self.sailerImgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"cct")];
    if ([model.del_c_buy_money floatValue] == 0 || STRINGEMPTY(model.del_user_realname)) {
        model.del_c_buy_money = @"300.00";
        model.del_user_realname = @"全球村村通";
    }
    if (STRINGEMPTY(model.c1_r_id)) {
        model.c1_user_realname = @"城主虚位以待";
        model.c1_buy_money = @"300.00";

//        self.buyerNameLbl.text = @"买家虚位以待";
    }
    self.sailerNameLbl.text = model.del_user_realname;
    self.oldPriceLbl.text = model.del_c_buy_money;
    
    NSString *url2 = [NSString stringWithFormat:@"http://www.cct369.com/village/public/images/%@",model.c1_user_portrait];
    [self.buyerImgView setImageWithURL:URL(url2) placeholder:IMAGENAMED(@"head")];
    self.buyerNameLbl.text = model.c1_user_realname;
    self.nowPriceLbl.text = model.c1_buy_money;
    
    self.earnMoneyLBl.text = [NSString stringWithFormat:@"赚了%.2f元",[model.c1_buy_money floatValue] - [model.del_c_buy_money floatValue]];
    self.timeLbl.text = [TOOL convertTextTime:model.c1_create_time];
    
//    if (STRINGEMPTY(model.del_c_r_id)) {
//        self.sailerNameLbl.text = @"全球村村通";
//        self.oldPriceLbl.text = @"300.00";
//    }
    
}
@end
