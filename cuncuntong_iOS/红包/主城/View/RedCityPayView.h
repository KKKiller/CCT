//
//  RedCityPayView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedCityInfoModel.h"
@interface RedCityPayView : UIView
@property (weak, nonatomic) IBOutlet UILabel *cityLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIButton *wxBtn;
@property (weak, nonatomic) IBOutlet UIButton *aliBtn;
+ (instancetype)instanceView;

@end
