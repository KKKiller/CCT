//
//  RedMyCityCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedMyCityCell.h"

@implementation RedMyCityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftTagLbl.layer.borderColor = MTRGB(0x46AE47).CGColor;
    self.leftTagLbl.layer.borderWidth = 0.5;
    self.rightTagLbl.layer.borderColor = MTRGB(0x5266F4).CGColor;
    self.rightTagLbl.layer.borderWidth = 0.5;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(RedCityInfoModel *)model {
    _model = model;
//    if (model.img) {
//        [self.headImgView setImageWithURL:URL(model.img) placeholder:IMAGENAMED(@"head")];
//    }else{
//        [self.headImgView setImageWithURL:URL([UserManager sharedInstance].portrait) placeholder:IMAGENAMED(@"head")];
//    }
//    self.nameLbl.text = [UserManager sharedInstance].realname;
    self.nameLbl.text = ([model.shi_name isEqualToString:@"市辖区"] || [model.shi_name isEqualToString:@"县"]) ? model.sheng_name : model.shi_name;
    NSString *sailMoney = [NSString stringWithFormat:@"%.0f",[model.mai_chu floatValue]];
    self.buyPriceLbl.text = [NSString stringWithFormat:@"¥%@",sailMoney];
    self.sailPriceLBl.text = [NSString stringWithFormat:@"¥%.0f", [model.buy_money floatValue]];
    self.timeLbl.text = [TOOL convertTextTime:model.create_time];
    if (self.type == 0) {
        self.leftTagLbl.text = @"挂牌价";
        self.rightTagLbl.text = @"购入价";
    }else{
        self.leftTagLbl.text = @"出售价";
        self.rightTagLbl.text = @"购入价";
    }
}
@end
