//
//  RedMyCityView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedMyCityView : UIView
@property (weak, nonatomic) IBOutlet UILabel *todayMoney;
+ (instancetype)instanceView;
@property (weak, nonatomic) IBOutlet UILabel *todaySailLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalSailLbl;
@property (weak, nonatomic) IBOutlet UIButton *myCityBtn;
@property (weak, nonatomic) IBOutlet UIButton *sailCityBtn;
@property (nonatomic, assign) NSInteger type;
@end
