//
//  RedCityHomeView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedCityInfoModel.h"
@interface RedCityHomeView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIButton *sailRecordBtn;
@property (weak, nonatomic) IBOutlet UIButton *recommandBtn;
@property (weak, nonatomic) IBOutlet UIButton *otherCityBtn;
@property (weak, nonatomic) IBOutlet UIButton *becomeCityerBtn;
+ (instancetype)instanceView;

@property (nonatomic, strong) RedCityInfoModel *model;

@end
