//
//  RedCityHomeController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedCityHomeController.h"
#import "RedCityHomeView.h"
#import "RedCityInfoModel.h"
#import "RedCitySailListController.h"
#import "BRAddressPickerView.h"
#import "BRAddressModel.h"
#import "RedCityPayController.h"
#import "PBViewController.h"
@interface RedCityHomeController ()<UIImagePickerControllerDelegate,PBViewControllerDataSource,PBViewControllerDelegate>
@property (nonatomic, strong) RedCityHomeView *homeView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) RedCityInfoModel *cityInfoModel;




@end

@implementation RedCityHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.homeView];
    [self setRightBtn:@"主城交易大厅"];
    if (self.cityId) {
        self.title = [NSString stringWithFormat:@"%@%@",STR(self.provinceName),STR(self.cityName)];
    }else{
        self.cityId = App_Delegate.cityId;
        if ([[Def valueForKey:@"province"] containsString:@"市"]) {
            self.title = [NSString stringWithFormat:@"%@",STR([Def valueForKey:@"cityName"])];
        }else{
            self.title = [NSString stringWithFormat:@"%@%@",STR([Def valueForKey:@"province"]),STR([Def valueForKey:@"cityName"])];
        }
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);
    [self loadData];

}
- (void)loadData {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getCastellanInfo") params:@{@"r_id":USERID,@"city_id":STR(self.cityId)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.cityInfoModel = [RedCityInfoModel modelWithJSON:success[@"data"]];
            self.homeView.model = self.cityInfoModel;
            if ([self.cityInfoModel.v_name isEqualToString:@"市辖区"] || [self.cityInfoModel.v_name isEqualToString:@"县"]) {
                self.title = [NSString stringWithFormat:@"%@",STR(self.cityInfoModel.pro_name)];
            }else{
                self.title = [NSString stringWithFormat:@"%@%@",STR(self.cityInfoModel.pro_name),STR(self.cityInfoModel.v_name)];
            }
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)updateRecommandImage:(UIImage *)image {
    NSData *data = UIImagePNGRepresentation(image);
    data = [TOOL imageCompress:data];
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"freight/addimg") params:@{@"r_id":USERID,@"name":@"file"} target:self imgaeData:data success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSString *url = success[@"msg"];
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"mfkit/red/notice") params:@{@"img":url,@"c_id":STR(self.cityInfoModel.c_id)} success:^(NSDictionary *success) {
                if ([success[@"error"] integerValue] == 0) {
                    SHOW(@"设置成功");
                    [self.homeView.recommandBtn setImageWithURL:URL(url) forState:UIControlStateNormal placeholder:IMAGENAMED(@"bulletin_board")];
                }else{
                    SHOW(success[@"msg"]);
                }
                [TOOL hideLoading:self.view];
            } failure:^(NSError *failure) {
                [TOOL hideLoading:self.view];
            }];
        }else{
            [TOOL hideLoading:self.view];
            SHOW(@"设置失败");
        }
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
//主城交易大厅
- (void)rightBtnClick {
    RedCitySailListController *vc = [[RedCitySailListController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)recommandBtnClick:(UIButton *)sender {
    if ([self.cityInfoModel.r_id isEqualToString:USERID]) {
        [self choosePic];
    }else{
        if(STRINGEMPTY(self.cityInfoModel.img)){
            SHOW(@"您不是城主，不能设置公告");
        }else{
            [self clickImg];
        }
    }
}
//交易记录
- (void)sailRecordBtnClick:(UIButton *)sender {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.titleStr = self.title;
    vc.navColor = RED;
    vc.hiddenShare = YES;
    vc.topOffset = -48;
    vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/mfkit/red/wepCastellanTradingRecord?city_id=%@",STR(self.cityId) ];
    [self.navigationController pushViewController:vc animated:YES];
}
//其他主城
- (void)otherCityBtnClick:(UIButton *)sender {
    //选择城市
    [BRAddressPickerView showAddressPickerWithDefaultSelected:nil isAutoSelect:NO resultBlock:^(NSArray *selectAddressArr) {
        BRProvinceModel *province = selectAddressArr[0];
        BRProvinceModel *city = selectAddressArr[1];
        self.provinceId = province.id;
        self.cityId = city.id;
        self.title = [NSString stringWithFormat:@"%@%@",province.name,city.name];
        [self loadData];
    }];
}
//我要成为城主
- (void)becomeCityerBtnClick:(UIButton *)sender {
    //选择城市
//    [BRAddressPickerView showAddressPickerWithDefaultSelected:nil isAutoSelect:NO resultBlock:^(NSArray *selectAddressArr) {
//        BRProvinceModel *province = selectAddressArr[0];
//        BRProvinceModel *city = selectAddressArr[1];
        RedCityPayController *vc = [[RedCityPayController alloc]init];
        vc.cityId = self.cityId;
        vc.provinceId = self.provinceId;
        vc.locationName = self.title;
        WEAKSELF
        vc.paySuccessBlock = ^(NSString *cityId,NSString *provinceId,NSString *locationName) {
            weakSelf.provinceId = provinceId;
            weakSelf.cityId = cityId;
            weakSelf.title = locationName;
            [weakSelf loadData];
        };
        [self.navigationController pushViewController:vc animated:YES];
//    }];
}
- (RedCityHomeView *)homeView {
    if (!_homeView) {
        _homeView = [RedCityHomeView instanceView];
        _homeView.frame = CGRectMake(0, 0, App_Width, 610);
        [_homeView.recommandBtn addTarget:self action:@selector(recommandBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_homeView.sailRecordBtn addTarget:self action:@selector(sailRecordBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_homeView.otherCityBtn addTarget:self action:@selector(otherCityBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_homeView.becomeCityerBtn addTarget:self action:@selector(becomeCityerBtnClick:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _homeView;
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        _scrollView.contentSize = CGSizeMake(0, 610);
    }
    return _scrollView;
}


#pragma mark - 照片
- (void)choosePic{
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    //    imagePicker.editing = YES;
    imagePicker.delegate = self;
    //    imagePicker.allowsEditing = YES;
    //创建sheet提示框，提示选择相机还是相册
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"请选择操作" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    //相机选项
    
    UIAlertAction * camera = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //选择相机时，设置UIImagePickerController对象相关属性
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
        imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        //跳转到UIImagePickerController控制器弹出相机
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    
    //相册选项
    UIAlertAction * photo = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //选择相册时，设置UIImagePickerController对象相关属性
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //跳转到UIImagePickerController控制器弹出相册
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    //查看大图
    UIAlertAction * viewer = [UIAlertAction actionWithTitle:@"查看大图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self clickImg];
    }];
    //取消按钮
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    //添加各个按钮事件
    [alert addAction:camera];
    [alert addAction:photo];
    if (!STRINGEMPTY(self.cityInfoModel.img)) {
        [alert addAction:viewer];
    }
    [alert addAction:cancel];
    //弹出sheet提示框
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    img = [TOOL fixOrientation:img];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self updateRecommandImage:img];
}

#pragma mark - 图片放大
- (void)clickImg{
    PBViewController *pbViewController = [PBViewController new];
    pbViewController.pb_dataSource = self;
    pbViewController.pb_delegate = self;
    pbViewController.pb_startPage = 0;
    [self presentViewController:pbViewController animated:YES completion:nil];
}
- (NSInteger)numberOfPagesInViewController:(PBViewController *)viewController {
    return 1;
}
- (void)viewController:(PBViewController *)viewController presentImageView:(YYAnimatedImageView *)imageView forPageAtIndex:(NSInteger)index progressHandler:(void (^)(NSInteger, NSInteger))progressHandler {
    [imageView setImageURL:URL(self.cityInfoModel.img)];
}
- (void)viewController:(PBViewController *)viewController didSingleTapedPageAtIndex:(NSInteger)index presentedImage:(UIImage *)presentedImage {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
