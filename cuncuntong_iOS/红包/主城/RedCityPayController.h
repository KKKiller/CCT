//
//  RedCityPayControlle.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "RedCityInfoModel.h"
typedef void (^RedPaySuccessBlock)(NSString *cityId,NSString *provinceId,NSString *locationName);
@interface RedCityPayController : BaseViewController
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *provinceId;
@property (nonatomic, strong) NSString *locationName;
@property (nonatomic, strong) RedPaySuccessBlock paySuccessBlock;

@end
