//
//  RedCityHomeController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/16.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface RedCityHomeController : BaseViewController
@property (nonatomic, strong) NSString *provinceId;
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *provinceName;
@property (nonatomic, strong) NSString *cityName;

@end
