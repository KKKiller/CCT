//
//  RedSailListModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/30.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedSailListModel : NSObject
@property (nonatomic, strong) NSString *c1_buy_money;
@property (nonatomic, strong) NSString *c1_c_id;
@property (nonatomic, strong) NSString *c1_create_time;
@property (nonatomic, strong) NSString *c1_delete_time;
@property (nonatomic, strong) NSString *c1_r_id;
@property (nonatomic, strong) NSString *c1_user_portrait;
@property (nonatomic, strong) NSString *c1_user_realname;
@property (nonatomic, strong) NSString *del_c_buy_money;
@property (nonatomic, strong) NSString *del_c_c_id;
@property (nonatomic, strong) NSString *del_c_create_time;
@property (nonatomic, strong) NSString *del_c_r_id;
@property (nonatomic, strong) NSString *del_user_portrait;
@property (nonatomic, strong) NSString *del_user_realname;
@property (nonatomic, strong) NSString *v_id;
@property (nonatomic, strong) NSString *v_name;
@property (nonatomic, strong) NSString *v_pid;
@property (nonatomic, strong) NSString *sheng_name;

@end

NS_ASSUME_NONNULL_END
