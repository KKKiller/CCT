//
//  RedCityInfoModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/26.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedCityInfoModel : NSObject
@property (nonatomic, strong) NSString *buy_money;
@property (nonatomic, strong) NSString *buy_sell_money;
@property (nonatomic, strong) NSString *mai_chu;
@property (nonatomic, strong) NSString *c_id;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, strong) NSString *delete_time;
@property (nonatomic, strong) NSString *img;
@property (nonatomic, strong) NSString *pro;
@property (nonatomic, strong) NSString *r_id;
@property (nonatomic, strong) NSString *up_id;
@property (nonatomic, strong) NSString *user_portrait;
@property (nonatomic, strong) NSString *user_realname;
@property (nonatomic, strong) NSString *v_id;
@property (nonatomic, strong) NSString *v_name;
@property (nonatomic, strong) NSString *pro_name;
@property (nonatomic, strong) NSString *sheng_name;
@property (nonatomic, strong) NSString *shi_name;

@end

NS_ASSUME_NONNULL_END
