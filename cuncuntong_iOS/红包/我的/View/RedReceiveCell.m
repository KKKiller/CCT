//
//  RedReceiveCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedReceiveCell.h"

@implementation RedReceiveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(RedModel *)model {
    _model = model;
//    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/images/%@",model.portrait];
    if (model.imgArray.count > 0) {
        [self.imgView setImageWithURL:URL(model.imgArray[0]) placeholder:IMAGENAMED(@"default_picture")];
    }
    self.titleLbl.text = model.title;
    self.moneyLbl.text = [NSString stringWithFormat:@"%@元", model.practical_money];
}

@end
