//
//  RedPostCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedModel.h"
@interface RedPostCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UILabel *countLbl;

@property (nonatomic, strong) RedModel *redModel;

@end
