//
//  RedPostCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedPostCell.h"

@implementation RedPostCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setRedModel:(RedModel *)redModel {
    _redModel = redModel;
    if (redModel.imgArray.count > 0) {
        [self.imgView setImageWithURL:URL(redModel.imgArray[0]) placeholder:IMAGENAMED(@"default_picture")];        
    }
    self.titleLbl.text = redModel.title;
    self.descLbl.text = redModel.red_href;
    self.moneyLbl.text = [NSString stringWithFormat:@"%@元", redModel.red_total_money];
    self.countLbl.text = [NSString stringWithFormat:@"%ld个",redModel.red_count];
}
@end
