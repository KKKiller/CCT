//
//  RedReceiveModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/23.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedReceiveModel : NSObject
@property (nonatomic, strong) NSString *annex;
@property (nonatomic, strong) NSString *create;
@property (nonatomic, strong) NSString *freight_list_id;
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *parent_r_id;
@property (nonatomic, strong) NSString *portrait;
@property (nonatomic, strong) NSString *rid;
@property (nonatomic, strong) NSString *money;

@end

NS_ASSUME_NONNULL_END
