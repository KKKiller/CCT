//
//  RedMyPostController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedMyPostController.h"
#import "RedPostCell.h"
#import "RedModel.h"
#import "RedDetailController.h"
static NSString *cellID = @"RedMyPostCell";

@interface RedMyPostController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation RedMyPostController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title  = @"发出的红包";
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    [self initRefreshView];
    
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);

}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/redPacketList") params:@{@"r_id":USERID,@"offset":@(self.dataArray.count)} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            for (NSDictionary *dict in success[@"data"]) {
                RedModel *model = [RedModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
            [self endLoding:success[@"data"]];
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 30){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RedPostCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.dataArray.count > indexPath.row) {
        RedModel *model = self.dataArray[indexPath.row];
        cell.redModel = model;        
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RedModel *model = self.dataArray[indexPath.row];
    RedDetailController *vc = [[RedDetailController alloc]init];
    vc.redId = model.red_id;
    [self.navigationController pushViewController:vc
                                         animated:YES];
}

#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        //        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
        //        CGFloat yy = system >= 11.0 ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 80;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"RedPostCell" bundle:nil] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:self.tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
@end
