//
//  RedProfileController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/2.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedProfileController.h"
#import "RedMyPostController.h"
#import "RedMyReceiveController.h"
#import "RedMyCityController.h"
static NSString *cellID = @"RedProfileCell";

@interface RedProfileController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *imgArray;




@end

@implementation RedProfileController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    _titleArray = @[@"我发出的红包",@"我收到的红包",@"我的邀请奖励",@"我的主城"];
    _imgArray = @[@"handout_red_packets",@"collect_red_packets",@"invitation_award",@"my_city"];
    [self.view addSubview:self.tableView];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.tableView.frame = self.view.bounds;
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);
    [App_Delegate.redTabbarVc setRightTitle:@""];

}

- (void)dealloc{
    NSLog(@"销毁了");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  self.titleArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = self.titleArray[indexPath.section];
    cell.imageView.image = [UIImage imageNamed:self.imgArray[indexPath.section]];
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 10)];
    view.backgroundColor = TSGRAY;
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        RedMyPostController *vc = [[RedMyPostController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 1){
        RedMyReceiveController *vc = [[RedMyReceiveController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 2){
        CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
        vc.titleStr = @"我的邀请奖励";
        vc.navColor = RED;
        vc.topOffset = -48;
        vc.hiddenShare = YES;
        vc.urlStr = [NSString stringWithFormat:@"%@village/public/mfkit/red/IInviteRewards?rid=%@",App_Delegate.shareBaseUrl,USERID];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 3){
        RedMyCityController *vc = [[RedMyCityController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
        CGFloat yy = system >= 11.0 ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, yy, App_Width, App_Height - 49 - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 50;
        _tableView.tableHeaderView = self.headerView;
        _tableView.backgroundColor = MTRGB(0xF7F7F7);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 70)];
        _imgView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 50, 50)];
        [_headerView addSubview:_imgView];
        NSString *url = [UserManager sharedInstance].portrait;
        [_imgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"head")];
        _nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame)+ 10, 25, 140, 20)];
        _nameLbl.text = [UserManager sharedInstance].realname;
        [_headerView addSubview:_nameLbl];
        _headerView.backgroundColor = WHITECOLOR;
    }
    return _headerView;
}

@end
