//
//  RedHomeController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/2.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedHomeController.h"
#import "CCPositionModel.h"
#import "BRAddressPickerView.h"
#import "BRAddressModel.h"
#import "RedDetailController.h"
#import "RedMapInfoModel.h"
#import "RedAnnotationView.h"
#import "RedDetailController.h"
#import "CCShareView.h"
#import "RedCityHomeController.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>

@interface RedHomeController ()<MAMapViewDelegate,CNContactPickerDelegate,MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) CCPositionModel *positionModel;
@property (nonatomic, strong) NSString *selectedLat;
@property (nonatomic, strong) NSString *selectedLng;
@property (nonatomic, strong) UIView *userView;
@property (nonatomic, strong) UIView *cityView;
@property (nonatomic, strong) CCShareView *shareView;

@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) UIButton *shareBtn;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIButton *myLocationBtn;
@property (nonatomic, strong) UIView *roundView;


@end

@implementation RedHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    [self setMapView];
    [self.view addSubview:self.shareBtn];
    [self.view addSubview:self.myLocationBtn];
    [self.view addSubview:self.userView];
    [self.view addSubview:self.cityView];
    [self.view addSubview:self.shareView];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);
    [self refreshData];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}



- (void)refreshData{
    [self.mapView removeAnnotations:self.dataArray];
    [self.dataArray removeAllObjects];
    [self loadAllRed];
}

- (void)loadAllRed {
    WEAKSELF
    //省市区红包
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getRedPacket") params:@{@"lat":STR(App_Delegate.lat),@"lng":STR(App_Delegate.lng),@"r_id":USERID,@"page":@"1",@"pro":STR(App_Delegate.provinceId),@"city":STR(App_Delegate.cityId),@"area":STR(App_Delegate.districtId)} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            for (NSDictionary *dict in success[@"data"]) {
                RedMapInfoModel *model = [RedMapInfoModel modelWithJSON:dict];
                if (App_Delegate.lat && App_Delegate.lng) {
                    float radom1 = (rand() % 100 - 50) * 0.0001;
                    float radom2 = (rand() % 100 - 50) * 0.0001;
                    model.lat = [NSString stringWithFormat:@"%f",[App_Delegate.lat floatValue] + radom1];
                    model.lng = [NSString stringWithFormat:@"%f",[App_Delegate.lng floatValue] + radom2];
                }
                [weakSelf.dataArray addObject:model];
            }
            [weakSelf.mapView addAnnotations:weakSelf.dataArray];
            [weakSelf.mapView showAnnotations:weakSelf.dataArray animated:YES];
            [weakSelf loadAroundRed];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)loadAroundRed {
    WEAKSELF
    //一公里红包
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getRedPacket") params:@{@"lat":STR(App_Delegate.lat),@"lng":STR(App_Delegate.lng),@"r_id":USERID,@"page":@"0",@"pro":STR(App_Delegate.provinceId),@"city":STR(App_Delegate.cityId),@"area":STR(App_Delegate.districtId)} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            for (NSDictionary *dict in success[@"data"]) {
                RedMapInfoModel *model = [RedMapInfoModel modelWithJSON:dict];
                [weakSelf.dataArray addObject:model];
            }
            [weakSelf.mapView addAnnotations:weakSelf.dataArray];
            [weakSelf.mapView showAnnotations:weakSelf.dataArray animated:YES];
            
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}

#pragma mark - 地图
- (void)setMapView {
    WEAKSELF
    if (!_mapView) {
        _mapView = [[MAMapView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 49 - 20 )];
        [self.view insertSubview:_mapView atIndex:0];
        _mapView.showsUserLocation = YES;
        _mapView.customizeUserLocationAccuracyCircleRepresentation = YES;
        _mapView.delegate = self;
        _mapView.zoomEnabled = YES;
        _mapView.zoomLevel = 10;
        _mapView.showsCompass = NO;
        _mapView.scrollEnabled = YES;
        _mapView.showsScale = NO;
        CGFloat  width = _mapView.width - 20;
        self.roundView = [[UIView alloc]initWithFrame:CGRectMake(10, (_mapView.height - width)*0.5 - 20, width, width)];
        [_mapView addSubview:self.roundView];
        self.roundView.userInteractionEnabled = NO;
        self.roundView.backgroundColor = MTRGB(0x4190e4);
        self.roundView.alpha = 0.1;
        self.roundView.layer.cornerRadius = width * 0.5;
        self.roundView.layer.masksToBounds = YES;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf myLocationBtnClick];
        });
    }
}
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation {
    if ([annotation isKindOfClass:[RedMapInfoModel class]] ) {
        RedMapInfoModel *model = (RedMapInfoModel *)annotation;
        RedAnnotationView *annotationView = (RedAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"anno"];
        if (annotationView == nil) {
            annotationView = [[RedAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"anno"];
        }
        CGFloat ratio = 28.0/18.0;
        annotationView.frame = CGRectMake(0, 0, 23, 23*ratio);
        annotationView.image = IMAGENAMED(@"red_envelopes");
        annotationView.model = model;
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view {
    if ([view isKindOfClass:[RedAnnotationView class]]) {
        RedAnnotationView *anno = (RedAnnotationView*)view;
        RedMapInfoModel *model = anno.model;
        RedDetailController *vc = [[RedDetailController alloc]init];
        vc.redId = model.red_id;
        [self.navigationController pushViewController:vc animated:YES];
        [mapView deselectAnnotation:anno.model animated:NO];
    }
}
- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id<MAOverlay>)overlay {
    return nil;
}

- (void)myLocationBtnClick {
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(self.mapView.userLocation.location.coordinate.latitude, self.mapView.userLocation.location.coordinate.longitude);
    [self.mapView setCenterCoordinate:location animated:YES];
}

- (UIView *)userView {
    if (!_userView) {
        _userView = [[UIView alloc]initWithFrame:CGRectMake(10, 74, 90, 30)];
        _userView.backgroundColor = WHITECOLOR;
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [imgView setImageWithURL:URL([UserManager sharedInstance].portrait) placeholder:IMAGENAMED(@"head")];
        [_userView addSubview:imgView];
        imgView.layer.cornerRadius = 15;
        imgView.layer.masksToBounds = YES;
        UILabel *nameLbl = [[UILabel alloc]initWithText:[UserManager sharedInstance].realname font:14 textColor:TEXTBLACK3];
        [_userView addSubview:nameLbl];
        nameLbl.frame = CGRectMake(25, 0, 65, 30);
        nameLbl.textAlignment = NSTextAlignmentCenter;
        _userView.layer.cornerRadius = 15;
        _userView.layer.shadowColor = [UIColor redColor].CGColor;
        _userView.layer.shadowOffset = CGSizeMake(2, 2);
        _userView.layer.shadowOpacity = 0.5;
        _userView.layer.shadowRadius = 3;
    }
    return _userView;
}
- (UIView *)cityView {
    WEAKSELF
    if (!_cityView) {
        _cityView = [[UIView alloc]initWithFrame:CGRectMake(10, 114, 90, 30)];
        _cityView.backgroundColor = WHITECOLOR;
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        imgView.image = IMAGENAMED(@"location") ;
        [_cityView addSubview:imgView];
        UILabel *nameLbl = [[UILabel alloc]initWithText:App_Delegate.cityName ?: [Def valueForKey:@"cityName"] font:14 textColor:TEXTBLACK3];
        [_cityView addSubview:nameLbl];
        nameLbl.frame = CGRectMake(25, 0, 65, 30);
        nameLbl.textAlignment = NSTextAlignmentCenter;
        _cityView.layer.cornerRadius = 15;
        _cityView.layer.shadowColor = [UIColor redColor].CGColor;
        _cityView.layer.shadowOffset = CGSizeMake(2, 2);
        _cityView.layer.shadowOpacity = 0.5;
        _cityView.layer.shadowRadius = 5;
        _cityView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            RedCityHomeController *vc = [[RedCityHomeController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
        [_cityView addGestureRecognizer:tap];
    }
    return _cityView;
}

- (UIButton *)myLocationBtn {
    if (!_myLocationBtn) {
        _myLocationBtn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width - 10 - 30, CGRectGetMinY(self.shareBtn.frame), 30, 30)];
        [_myLocationBtn addTarget:self action:@selector(myLocationBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_myLocationBtn setImage:IMAGENAMED(@"myLocation") forState:UIControlStateNormal];
    }
    return _myLocationBtn;
}
- (UIButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [[UIButton alloc]initWithFrame:CGRectMake((App_Width - 150)*0.5, App_Height - 49 - 60, 150, 34)];
        [_shareBtn setTitle:@"分享增收益" forState:UIControlStateNormal];
        [_shareBtn setImage:IMAGENAMED(@"share") forState:UIControlStateNormal];
        _shareBtn.titleLabel.font = FFont(14);
        [_shareBtn setTitleColor:TEXTBLACK3 forState:UIControlStateNormal];
        _shareBtn.layer.cornerRadius = 17;
        _shareBtn.layer.shadowColor = [UIColor blackColor].CGColor;
        _shareBtn.layer.shadowOffset = CGSizeMake(1, 1);
        _shareBtn.layer.shadowOpacity = 0.5;
        _shareBtn.layer.shadowRadius = 2;
        [_shareBtn addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _shareBtn.backgroundColor = WHITECOLOR;
        [_shareBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
        
    }
    return _shareBtn;
}
- (void)shareBtnClick {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.navColor = RED;
    vc.topOffset = -48;
    vc.hiddenShare = YES;
    vc.titleStr = @"分享增收益";
    vc.urlStr = [NSString stringWithFormat:@"%@village/public/mfkit/red/wepSharePage?rid=%@",App_Delegate.shareBaseUrl,USERID];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)dealloc{
//    SHOW(@"销毁了");
    NSLog(@"销毁了");
}

@end
