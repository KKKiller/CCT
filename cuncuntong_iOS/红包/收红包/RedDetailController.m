//
//  RedRecieveController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/22.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedDetailController.h"
#import "RedInfoHeaderView.h"
#import "RedModel.h"
#import "RedGetterModel.h"
#import "RedAnswerView.h"
#import "CCShareImgCell.h"
#import "UIImage+ImgSize.h"
#import "RedDetailFooter.h"
static NSString *cellID = @"CCShareCommentCell";

@interface RedDetailController ()<RedInfoFooterViewDelegate,PBViewControllerDataSource,PBViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
//@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) RedDetailFooter *footer;

@property (nonatomic, strong) RedInfoHeaderView *header;
@property (nonatomic, strong) RedModel *redModel;
@property (nonatomic, strong) NSMutableArray *redGetterList;
@property (nonatomic, strong) RedAnswerView *answerView;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) NSString *answer;


@end

@implementation RedDetailController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MTRGB(0xF7F7F7);
    self.redGetterList = [NSMutableArray array];
    [self.view addSubview:self.tableView];
//    [self.scrollView addSubview:self.header];
//    self.scrollView.contentSize = CGSizeMake(App_Width, self.header.height);
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0x50BB96);
    self.navigationController.navigationBarHidden = YES;
    if (self.answerView) {
        [self.animateModel showWithModalView:self.answerView inView:App_Delegate.window];
    }
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)refreshData { //点击tabbar刷新
    [self.dataArray removeAllObjects];
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    //红包信息
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getPacketOne") params:@{@"red_id":STR(self.redId),@"r_id":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            self.redModel = [RedModel modelWithJSON:success[@"data"]];
            self.header.model = self.redModel;
            self.footer.model = self.redModel;
            self.header.height = self.header.textHeight + 370 ;
            [self.tableView reloadData];
            for (NSString *url in self.redModel.imgArray) {
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    [self getImageHeightWithUrl:BASEURL_WITHOBJC(url)];
                });
            }
//            self.scrollView.contentSize = CGSizeMake(App_Width, self.header.height);
            if (self.redModel.is_open) {//已经打开
                [self getRedGeterList];
                if ([self.redModel.is_open.add_red_money floatValue] == 0) {
                    SHOW(@"问题回答错误，无法再次抢红包");
                }
            }else{
                if ([self.redModel.red_type isEqualToString:@"1"]) {//答题红包
                    [self showAnswerView];
                }else{//普通红包
                    [self getRed];
                }
            }
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
    
}
- (void)getRed {
    //领红包
    NSString *pro = App_Delegate.provinceId;
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/openRedPacket") params:@{@"red_id":STR(self.redId),@"r_id":USERID,@"lat":STR(App_Delegate.lat),@"lng":STR(App_Delegate.lng),@"pro":STR(App_Delegate.provinceId),@"city":STR(App_Delegate.cityId),@"area":STR(App_Delegate.districtId),@"red_key_ok":STR(self.answer)} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)]) {
            self.header.moneyLbl.text = [NSString stringWithFormat:@"%@",success[@"data"][@"money"]];
            self.header.addLbl.text = [NSString stringWithFormat:@"+%@克",success[@"data"][@"energy"]];
            [self getRedGeterList];
            [self loadData];
        }else if([success[@"msg"] containsString:@"错误"]){
            SHOW(@"答案错误");
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)getRedGeterList {
    //领红包的人
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/getPacketUser") params:@{@"red_id":STR(self.redId),@"r_id":USERID,@"offset":@"0"} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"]  isEqual: @(0)] ) {
            [self.redGetterList removeAllObjects];
            for (NSDictionary *dict in success[@"data"]) {
                RedGetterModel *model = [RedGetterModel modelWithJSON:dict];
                if (self.redGetterList.count < 8) {
                    [self.redGetterList addObject:model];                    
                }
            }
            self.header.avatars = self.redGetterList;
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.redModel.imgArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCShareImgCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.redModel.imgArray.count > indexPath.row) {
        cell.url = self.redModel.imgArray[indexPath.row];
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PBViewController *pbViewController = [PBViewController new];
    pbViewController.pb_dataSource = self;
    pbViewController.pb_delegate = self;
    pbViewController.pb_startPage = indexPath.row;
    [self presentViewController:pbViewController animated:YES completion:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *url = self.redModel.imgArray[indexPath.row];
    url = BASEURL_WITHOBJC(url);
    CGFloat height = [self getImageHeightWithUrl:url];
    return height > 0 ? height : 0;
}


#pragma mark - 按钮点击
- (void)headerClickCommentAtIndex:(NSInteger)index {
    //1好评 2中评 3差评
    NSInteger type = index == 1 ? 5 : index == 2 ? 3 : 1;
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/discuss") params:@{@"red_id":STR(self.redModel.red_id),@"type":@(type)} success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"评论成功");
            [self loadData];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
    
}


//领取红包的人
- (void)showRecieverBtnClick {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.navColor = RED;
    vc.titleStr =  @"红包领取列表";
    vc.topOffset = -48;
    vc.hiddenShare = YES;
    vc.urlStr =  [NSString stringWithFormat:@"%@village/public/mfkit/red/wepGetRedCount?red_id=%@",App_Delegate.shareBaseUrl,STR(self.redId)];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 答题
- (void)showAnswerView {
    CGFloat height = [self.redModel.red_href sizeForFont:FFont(14) size:CGSizeMake(App_Width - 30, CGFLOAT_MAX) mode:0].height;
    
    self.answerView = [RedAnswerView instanceView];
    self.answerView.frame = CGRectMake(0, 0, 260, 340 + height);
    self.answerView.model = self.redModel;
    self.answerView.layer.cornerRadius = 6;
    self.answerView.layer.masksToBounds= YES;
    
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = NO;
    self.animateModel.closeByTapBackground = NO;
    [self.animateModel showWithModalView:self.answerView inView:App_Delegate.window];
    [self.answerView.sureBtn addTarget:self action:@selector(answerChoosed) forControlEvents:UIControlEventTouchUpInside];
    [self.answerView.backBtn addTarget:self action:@selector(answerBackBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.answerView.hrefTap addActionBlock:^(id  _Nonnull sender) {
        [self.animateModel closeWithStraight];
        CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
        vc.urlStr = self.redModel.red_href ?: @"http://www.baidu.com";
        vc.navColor = RED;
        vc.titleStr = @"链接地址";
        vc.hiddenShare = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];

    
}
- (void)answerChoosed {
    if (!self.answerView.ABtn.selected && !self.answerView.BBtn.selected && !self.answerView.CBtn.selected) {
        SHOW(@"请选择答案");
        return;
    }
    [self.animateModel closeWithStraight];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"该问题只有一次答题机会哦！" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"再想想" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.animateModel showWithModalView:self.answerView inView:App_Delegate.window];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确认提交" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        if (self.answerView.selectedIndex == self.answerView.rightAnswerIndex) {
            self.answer = self.answerView.answer;
            self.answerView = nil;
            [self getRed];
//        }else{
//            UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:@"提示" message:@"很遗憾，回答错误" preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"返回主页面" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [self.navigationController popViewControllerAnimated:YES];
//            }];
//            [alert2 addAction:action3];
//            [self presentViewController:alert2 animated:YES completion:nil];
//        }
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)answerBackBtnClick {
    [self.animateModel closeWithStraight];
    [self.navigationController popViewControllerAnimated:YES];
}
- (CGFloat)getImageHeightWithUrl:(NSString *)url {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:url]) {
        //这里拿到每个图片的尺寸，然后计算出每个cell的高度
        CGSize imageSize = [UIImage getImageSizeWithURL:url];
        CGFloat imgH = imageSize.height *( App_Width / imageSize.width) + 5;
        //将最终的自适应的高度 本地化处理
        [[NSUserDefaults standardUserDefaults] setObject:@(imgH) forKey:url];
    }
    NSNumber *height = [[NSUserDefaults standardUserDefaults] objectForKey:url];
    return [height floatValue];
}
#pragma mark - 图片放大
- (void)headerClickImgAtIndex:(NSInteger)index {
    PBViewController *pbViewController = [PBViewController new];
    pbViewController.pb_dataSource = self;
    pbViewController.pb_delegate = self;
    pbViewController.pb_startPage = index;
    [self presentViewController:pbViewController animated:YES completion:nil];
}
- (NSInteger)numberOfPagesInViewController:(PBViewController *)viewController {
    return self.redModel.imgArray.count;
    return 1;
}
- (void)viewController:(PBViewController *)viewController presentImageView:(YYAnimatedImageView *)imageView forPageAtIndex:(NSInteger)index progressHandler:(void (^)(NSInteger, NSInteger))progressHandler {
    [imageView setImageURL:URL(self.redModel.imgArray[index])];
}
- (void)viewController:(PBViewController *)viewController didSingleTapedPageAtIndex:(NSInteger)index presentedImage:(UIImage *)presentedImage {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 50;
        _tableView.tableHeaderView = self.header;
        _tableView.tableFooterView = self.footer;
        _tableView.backgroundColor = WHITECOLOR;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCShareImgCell" bundle:nil]  forCellReuseIdentifier:cellID];
    }
    return _tableView;
}
- (RedInfoHeaderView *)header {
    if (!_header) {
        _header = [RedInfoHeaderView instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 425);
        WEAKSELF
        [_header.htmlTextTap addActionBlock:^(id  _Nonnull sender) {
            CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
            vc.navColor = RED;
            vc.titleStr = @"链接地址";
            vc.urlStr = weakSelf.redModel.red_href;
            vc.hiddenShare = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
        [_header.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [_header.showRecieverBtn addTarget:self action:@selector(showRecieverBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _header;
}
- (RedDetailFooter *)footer {
    if (!_footer) {
        _footer = [RedDetailFooter instanceView];
        _footer.frame = CGRectMake(0, 0, App_Width, 100);
        _footer.delegate = self;
    }
    return _footer;
}
//- (UIScrollView *)scrollView {
//    if (!_scrollView) {
//        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height)];
//    }
//    return _scrollView;
//}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

//- (CCEmptyView *)emptyView {
//    if (_emptyView == nil) {
//        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height - 64 - 49)];
//        [_scrollView addSubview:_emptyView];
//    }
//    return _emptyView;
//}

@end

