//
//  RedDetailFooter.h
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol RedInfoFooterViewDelegate <NSObject>

- (void)headerClickCommentAtIndex:(NSInteger)index;

@end
@interface RedDetailFooter : UIView
@property (weak, nonatomic) IBOutlet UIButton *goodBtn;
@property (weak, nonatomic) IBOutlet UIButton *normalBtn;
@property (weak, nonatomic) IBOutlet UIButton *badBtn;
+ (instancetype)instanceView;
@property (nonatomic, weak) id<RedInfoFooterViewDelegate> delegate;
@property (nonatomic, strong) RedModel *model;

@end

NS_ASSUME_NONNULL_END
