//
//  RedAnswerView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/31.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedAnswerView.h"

@implementation RedAnswerView

+ (instancetype)instanceView {
    RedAnswerView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.ABtn.layer.borderColor = TEXTBLACK6.CGColor;
    view.ABtn.layer.borderWidth = 0.5;
    view.BBtn.layer.borderColor = TEXTBLACK6.CGColor;
    view.BBtn.layer.borderWidth = 0.5;
    view.CBtn.layer.borderColor = TEXTBLACK6.CGColor;
    view.CBtn.layer.borderWidth = 0.5;
    [view.ABtn addTarget:view action:@selector(answerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.BBtn addTarget:view action:@selector(answerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.CBtn addTarget:view action:@selector(answerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    return view;
}
- (void)answerBtnClick:(UIButton *)sender {
    self.ABtn.selected = NO;
    self.BBtn.selected = NO;
    self.CBtn.selected = NO;
    self.ABtn.layer.borderColor = TEXTBLACK6.CGColor;
    self.BBtn.layer.borderColor = TEXTBLACK6.CGColor;
    self.CBtn.layer.borderColor = TEXTBLACK6.CGColor;
    sender.selected = YES;
    sender.layer.borderColor = RED.CGColor;
    self.selectedIndex = sender.tag;
    if(sender.titleLabel.text.length > 2){
        self.answer = [sender.titleLabel.text substringFromIndex:2];
    }else{
        self.answer = @"";
    }
}
- (void)setModel:(RedModel *)model {
    _model = model;
    self.questionLbl.text = model.title;
    self.hrefLbl.text = model.red_href;
    NSArray *ans = [model.red_key componentsSeparatedByString:@"&"];
    for (int i = 0; i<ans.count; i++) {
        if ([ans[i] isEqualToString:model.red_key_ok]) {
            self.rightAnswerIndex = i;
        }
    }
    if (ans.count == 3) {
        [self.ABtn setTitle:[NSString stringWithFormat:@"A.%@",ans[0]] forState:UIControlStateNormal];
        [self.BBtn setTitle:[NSString stringWithFormat:@"B.%@",ans[1]] forState:UIControlStateNormal];
        [self.CBtn setTitle:[NSString stringWithFormat:@"C.%@",ans[2]] forState:UIControlStateNormal];
        self.answer = ans[0];
        
        if ([ans[0] length] > 12) {
            self.ABtn.titleLabel.font = FFont(10);
        }
        if ([ans[1] length] > 12) {
            self.BBtn.titleLabel.font = FFont(10);
        }
        if ([ans[2] length] > 12) {
            self.CBtn.titleLabel.font = FFont(10);
        }
    }
}
@end
