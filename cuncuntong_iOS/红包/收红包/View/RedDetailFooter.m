//
//  RedDetailFooter.m
//  CunCunTong
//
//  Created by 周吾昆 on 2019/1/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "RedDetailFooter.h"

@implementation RedDetailFooter

+ (instancetype)instanceView {
    RedDetailFooter *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    [view.goodBtn addTarget:view action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.normalBtn addTarget:view action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.badBtn addTarget:view action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    view.goodBtn.backgroundColor = MTRGB(0xE0E0E0);
    view.normalBtn.backgroundColor = MTRGB(0xE0E0E0);
    view.badBtn.backgroundColor = MTRGB(0xE0E0E0);
    
    return view;
}
- (void)commentBtnClick:(UIButton *)sender {
    self.goodBtn.selected = NO;
    self.normalBtn.selected = NO;
    self.badBtn.selected = NO;
    self.goodBtn.backgroundColor = MTRGB(0xE0E0E0);
    self.normalBtn.backgroundColor = MTRGB(0xE0E0E0);
    self.badBtn.backgroundColor = MTRGB(0xE0E0E0);
    if (!self.model.commented) {
        sender.backgroundColor = RED;
        sender.selected = YES;
    }
    if ([self.delegate respondsToSelector:@selector(headerClickCommentAtIndex:)]) {
        [self.delegate headerClickCommentAtIndex:sender.tag];
    }
}
- (void)setModel:(RedModel *)model {
    _model = model;
    //评论
    model.commented = YES;
    if (model.commented) {
        CGFloat good = [model.discuss[@"h"] floatValue];
        CGFloat normal = [model.discuss[@"z"] floatValue];
        CGFloat bad = [model.discuss[@"c"] floatValue];
        CGFloat total = good + normal + bad;
        if (total == 0) {
            [self.goodBtn setTitle:@"好评(100%)" forState:UIControlStateNormal];
            [self.normalBtn setTitle:@"中评(0%)" forState:UIControlStateNormal];
            [self.badBtn setTitle:@"差评(0%)" forState:UIControlStateNormal];
        }else{
            [self.goodBtn setTitle:[NSString stringWithFormat:@"好评(%.0f%%)",good/total*100] forState:UIControlStateNormal];
            [self.normalBtn setTitle:[NSString stringWithFormat:@"中评(%.0f%%)",normal/total*100] forState:UIControlStateNormal];
            [self.badBtn setTitle:[NSString stringWithFormat:@"差评(%.0f%%)",bad/total*100] forState:UIControlStateNormal];
        }
    }
}
@end
