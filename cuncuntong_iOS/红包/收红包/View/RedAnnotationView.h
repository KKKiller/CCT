//
//  RedAnnotationView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/23.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "RedMapInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RedAnnotationView : MAAnnotationView
@property (nonatomic, strong) RedMapInfoModel *model;

@end

NS_ASSUME_NONNULL_END
