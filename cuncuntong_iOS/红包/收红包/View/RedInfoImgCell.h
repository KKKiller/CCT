//
//  RedInfoImgCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/22.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedInfoImgCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

NS_ASSUME_NONNULL_END
