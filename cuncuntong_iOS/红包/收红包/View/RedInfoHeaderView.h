//
//  RedInfoHeaderView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/22.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedModel.h"
#import "MTPageView.h"
#import "PBViewController.h"
#import "RedGetterModel.h"

@interface RedInfoHeaderView : UIView<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UILabel *addLbl;
@property (weak, nonatomic) IBOutlet UIView *recieverContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *receiveContainerWidth;
@property (weak, nonatomic) IBOutlet UILabel *recieveCounterLbl;
@property (weak, nonatomic) IBOutlet UIButton *showRecieverBtn;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *httpLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIButton *goodBtn;
@property (weak, nonatomic) IBOutlet UIButton *middleBtn;
@property (weak, nonatomic) IBOutlet UIButton *badBtn;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *htmlTextTap;
@property (weak, nonatomic) IBOutlet UILabel *answerHref;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTop;
@property (nonatomic, assign) CGFloat textHeight;
+ (instancetype)instanceView;

@property (nonatomic, strong) MTPageView *pageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *comnentContainerTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photosToDescTop;

@property (nonatomic, strong) NSArray *avatars;
@property (nonatomic, strong) RedModel *model;

@end
