//
//  RedInfoHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/22.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedInfoHeaderView.h"
#import "RedInfoImgCell.h"

@implementation RedInfoHeaderView
+ (instancetype)instanceView {
    RedInfoHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    view.headImgView.layer.borderWidth = 2;
    view.headImgView.layer.borderColor = WHITECOLOR.CGColor;
    

    return view;
}

- (void)setAvatars:(NSArray *)avatars {
    self.recieverContainerView.hidden = NO;
    CGFloat width = avatars.count > 8 ? 160 : avatars.count * 20;
    self.receiveContainerWidth.constant = width;
    for (int i = 0; i < avatars.count; i++) {
        RedGetterModel *model = avatars[i];
        UIImageView *imgV = [[UIImageView alloc]init];
        imgV.frame = CGRectMake(i*20 , 0, 26, 26);
        [self.recieverContainerView addSubview:imgV];
        imgV.layer.cornerRadius = 13;
        imgV.layer.masksToBounds = YES;
        NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/images/%@",model.portrait];
        [imgV setImageWithURL:URL(url) placeholder:IMAGENAMED(@"head")];
    }
}
- (void)setModel:(RedModel *)model {
    _model = model;
    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/images/%@",model.portrait];
    [self.headImgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"head")];
    self.nameLbl.text = model.realname;
    if (model.is_open && model.is_open.add_red_money) {
        self.moneyLbl.text = model.is_open.add_red_money;
        self.addLbl.text = [NSString stringWithFormat:@"+%@克",model.is_open.energy];
    }
    self.descLbl.text = model.title;
    self.httpLbl.text = model.red_href;
    self.recieveCounterLbl.text = [NSString stringWithFormat:@"%ld人已领取",model.red_count - model.red_residue_count];
    if(STRINGEMPTY(model.red_href)){
        self.httpLbl.hidden = YES;
        self.answerHref.hidden = YES;
    }
    self.answerHref.text = [model.red_type isEqualToString:@"0"] ? @"推广链接:":@"答案参考:";
    self.typeLbl.text = [self getTypeStr];

    self.textHeight = [model.title sizeForFont:FFont(14) size:CGSizeMake(App_Width - 30, CGFLOAT_MAX) mode:0].height;
    if (!STRINGEMPTY(self.model.red_href)) {
        self.textHeight += [model.red_href sizeForFont:FFont(14) size:CGSizeMake(App_Width - 30, CGFLOAT_MAX) mode:0].height + 5;
    }
}
- (NSString *)getTypeStr {
    NSString *type = @"全国";
    if ([self.model.distance isEqualToString:@"1"]) {
        type = @"一公里";
    }else{
        if (![self.model.pro isEqualToString:@"0"]) {
            type = @"全省";
        }
        if (![self.model.city isEqualToString:@"0"]) {
            type = @"全市";
        }
        if (![self.model.area isEqualToString:@"0"]) {
            type = @"全区/县";
        }
    }
    return type;
}



@end
