//
//  RedAnswerView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/31.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RedAnswerView : UIView
@property (weak, nonatomic) IBOutlet UILabel *questionLbl;
@property (weak, nonatomic) IBOutlet UIButton *ABtn;
@property (weak, nonatomic) IBOutlet UIButton *BBtn;
@property (weak, nonatomic) IBOutlet UIButton *CBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UILabel *hrelTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *hrefLbl;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *hrefTap;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) NSInteger rightAnswerIndex;
@property (nonatomic, strong) NSString *answer;

+ (instancetype)instanceView;
@property (nonatomic, strong) RedModel *model;

@end

NS_ASSUME_NONNULL_END
