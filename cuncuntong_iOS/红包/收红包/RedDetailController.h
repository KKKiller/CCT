//
//  RedRecieveController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/22.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCPositionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RedDetailController : BaseViewController
@property (nonatomic, strong) NSString *redId;
@property (nonatomic, strong) CCPositionModel *positionModel;

@end

NS_ASSUME_NONNULL_END
