//
//  RedInfoModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/22.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedMapInfoModel.h"

@implementation RedMapInfoModel
- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake([self.lat doubleValue], [self.lng doubleValue]);
}
@end
