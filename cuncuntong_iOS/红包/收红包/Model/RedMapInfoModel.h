//
//  RedInfoModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/22.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedMapInfoModel : NSObject<MAAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy, nullable) NSString *title;
@property (nonatomic, copy, nullable) NSString *subtitle;


@property (nonatomic, strong) NSString *red_id;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;



@end

NS_ASSUME_NONNULL_END
