//
//  RedPostChooseView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/11.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedPostChooseView.h"

@implementation RedPostChooseView

+ (instancetype)instanceView {
    RedPostChooseView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    [view.ABtn addTarget:view action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.BBtn addTarget:view action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view.CBtn addTarget:view action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    view.ABtn.backgroundColor = MTRGB(0x71BE6F);
    view.BBtn.backgroundColor = WHITECOLOR;
    view.CBtn.backgroundColor = WHITECOLOR;
    return view;
}
- (void)btnClick:(UIButton *)sender {
    self.ABtn.selected = NO;
    self.BBtn.selected = NO;
    self.CBtn.selected = NO;
    self.ABtn.backgroundColor = WHITECOLOR;
    self.BBtn.backgroundColor = WHITECOLOR;
    self.CBtn.backgroundColor = WHITECOLOR;
    sender.selected = YES;
    sender.backgroundColor = MTRGB(0x71BE6F);
    _selectedIndex = sender.tag;
}
- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
    UIButton *btn;
    if (selectedIndex == 0) {
        btn = self.ABtn;
    }else if (selectedIndex == 1){
        btn = self.BBtn;
    }else if (selectedIndex == 2){
        btn = self.CBtn;
    }
    [self btnClick:btn];
}
@end
