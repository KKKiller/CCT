//
//  RedChooseAreaView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/23.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedChooseAreaView : UIView
@property (weak, nonatomic) IBOutlet UIButton *nearybyBtn;
@property (weak, nonatomic) IBOutlet UIButton *districtBtn;
@property (weak, nonatomic) IBOutlet UIButton *cityBtn;
@property (weak, nonatomic) IBOutlet UIButton *provinceBtn;
@property (weak, nonatomic) IBOutlet UIButton *countryBtn;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, strong) NSArray *btnsArray;
+ (instancetype)instanceView ;
@end

NS_ASSUME_NONNULL_END
