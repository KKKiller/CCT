//
//  RedPostInfoView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/11.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedPostInfoView : UIView
@property (weak, nonatomic) IBOutlet UIView *adContainerView;
@property (weak, nonatomic) IBOutlet UILabel *adTipsLbl;
@property (weak, nonatomic) IBOutlet UITextField *adField;

@property (weak, nonatomic) IBOutlet UITextField *moneyField;
@property (weak, nonatomic) IBOutlet UITextField *countField;
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;
@property (weak, nonatomic) IBOutlet UIButton *postBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moneyContainerTopConstraint;
+ (instancetype)instanceView;
@end
