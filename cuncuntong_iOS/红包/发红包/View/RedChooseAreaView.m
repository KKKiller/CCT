//
//  RedChooseAreaView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/23.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedChooseAreaView.h"

@implementation RedChooseAreaView

+ (instancetype)instanceView {
    RedChooseAreaView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.btnsArray = @[view.nearybyBtn,view.districtBtn,view.cityBtn,view.provinceBtn,view.countryBtn];
    for (UIButton *btn in view.btnsArray) {
        [btn addTarget:view action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:WHITECOLOR forState:UIControlStateSelected];
        [btn setTitleColor:TEXTBLACK3 forState:UIControlStateNormal];
        [btn setBackgroundColor:WHITECOLOR];
        btn.titleLabel.font = FFont(12);
    }
    [view.nearybyBtn setBackgroundColor:MTRGB(0xFA6B64)];
    view.selectedIndex = 4;
    return view;
}
- (void)btnClick:(UIButton *)sender {
    for (UIButton *btn in self.btnsArray) {
        btn.selected = NO;
        [btn setBackgroundColor:WHITECOLOR];
    }
    sender.selected = YES;
    [sender setBackgroundColor:MTRGB(0xFA6B64)];
    self.selectedIndex = sender.tag;
}
@end
