//
//  RedPostTopView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/11.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedPostTopView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *dispatchBtn;
@property (weak, nonatomic) IBOutlet UIButton *normalRedBtn;
@property (weak, nonatomic) IBOutlet UIButton *answerRedBtn;
@property (weak, nonatomic) IBOutlet UIView *normalRedLine;

@property (weak, nonatomic) IBOutlet UIView *answerRedLine;
+ (instancetype)instanceView;
@end
