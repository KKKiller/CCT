//
//  RedPostChooseView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/11.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedPostChooseView : UIView
@property (weak, nonatomic) IBOutlet UITextField *AField;
@property (weak, nonatomic) IBOutlet UITextField *BField;
@property (weak, nonatomic) IBOutlet UITextField *CField;
@property (weak, nonatomic) IBOutlet UIButton *ABtn;
@property (weak, nonatomic) IBOutlet UIButton *BBtn;
@property (weak, nonatomic) IBOutlet UIButton *CBtn;
@property (weak, nonatomic) IBOutlet UITextField *answerSearchField;
+ (instancetype)instanceView;
@property (nonatomic, assign) NSInteger selectedIndex;
@end
