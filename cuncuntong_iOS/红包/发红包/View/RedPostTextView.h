//
//  RedPostTextView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/11.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedPostTextView : UIView
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLbl;
@property (weak, nonatomic) IBOutlet UILabel *countLbl;
+ (instancetype)instanceView;
@end
