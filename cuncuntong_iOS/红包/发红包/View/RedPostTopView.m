//
//  RedPostTopView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/11.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedPostTopView.h"

@implementation RedPostTopView
+ (instancetype)instanceView {
    RedPostTopView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    return view;
}

@end
