//
//  RedPostInfoView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/11.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedPostInfoView.h"

@implementation RedPostInfoView

+ (instancetype)instanceView {
    RedPostInfoView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    return view;
}

@end
