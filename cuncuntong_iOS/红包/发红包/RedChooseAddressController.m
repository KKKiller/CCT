//
//  RedChooseAddressController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/23.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedChooseAddressController.h"
#import "RedChooseAreaView.h"
#import "BRAddressPickerView.h"
#import "BRAddressModel.h"
#import <MapKit/MapKit.h>

@interface RedChooseAddressController ()<CLLocationManagerDelegate,MKMapViewDelegate>
@property (nonatomic, strong) RedChooseAreaView *chooseAreaView;
@property (nonatomic, strong) CCPositionModel *positionModel;
@property (nonatomic, strong) UIButton *sureBtn;
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@property (nonatomic, strong) UIButton *myLocationBtn;


@end

@implementation RedChooseAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择位置";
    [self setRightBtn:@"选择城市"];
    [self.view addSubview:self.chooseAreaView];
    [self setupMapView];
    [self.view addSubview:self.sureBtn];
    [self.view addSubview:self.myLocationBtn];
    
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);
}
-  (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (App_Delegate.cityId && App_Delegate.cityName) {
        self.positionModel = [[CCPositionModel alloc]init];
        self.positionModel.provinceName = App_Delegate.provinceName;
        self.positionModel.provinceId = App_Delegate.provinceId;
        self.positionModel.cityName = App_Delegate.cityName;
        self.positionModel.cityId = App_Delegate.cityId;
        self.positionModel.districtName = App_Delegate.districtName;
        self.positionModel.districtId = App_Delegate.districtId;
        
        self.lat = App_Delegate.lat;
        self.lng = App_Delegate.lng;
    }else{
        [self rightBtnClick];
    }
}
//切换城市
- (void)rightBtnClick {
    [BRAddressPickerView showAddressPickerWithDefaultSelected:nil isAutoSelect:NO resultBlock:^(NSArray *selectAddressArr) {
        BRProvinceModel *province = selectAddressArr[0];
        BRProvinceModel *city = selectAddressArr[1];
        BRProvinceModel *town = selectAddressArr[2];
        
        self.positionModel = [[CCPositionModel alloc]init];
        self.positionModel.provinceName = province.name;
        self.positionModel.provinceId = province.id;
        self.positionModel.cityName = city.name;
        self.positionModel.cityId = city.id;
        self.positionModel.districtName = town.name;
        self.positionModel.districtId = town.id;
        
        NSString *cityName = self.positionModel.cityName;
        if ([cityName isEqualToString:@"市辖区"] || [cityName isEqualToString:@"县"]) {
            cityName = self.positionModel.provinceName;
        }
        [self pushToMapView:NO cityName:cityName];
    }];
}
- (void)sureBtnClick {
    if (!self.positionModel) {
        SHOW(@"请选择发布城市");
        return;
    }
    if (self.chooseAreaView.selectedIndex == 3 && [self.positionModel.districtId isEqualToString:@"-1"]) {
        SHOW(@"发布区红包需选择确定的区");
        [self rightBtnClick];
        return;
    }
    if (self.chooseAddressBlock) {
        self.chooseAddressBlock(self.positionModel, self.lat, self.lng,self.chooseAreaView.selectedIndex);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 切换城市

- (void)pushToMapView:(BOOL)isEnd cityName:(NSString *)cityName{
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]GetUrl:@"http://api.map.baidu.com/geocoder" params:@{@"key":@"Og6DKLflNeib9DqK72Qr2sqTMvyvtctI",@"output":@"json",@"address":STR(cityName)} success:^(NSDictionary *success) {
        if ([success[@"status"]isEqualToString:@"OK"] && [success[@"result"] isKindOfClass:[NSDictionary class]]) {
            NSString *lat = [NSString stringWithFormat:@"%@", success[@"result"][@"location"][@"lat"]];
            NSString *lng = [NSString stringWithFormat:@"%@", success[@"result"][@"location"][@"lng"]];
            if (!STRINGEMPTY(lat) && !STRINGEMPTY(lng)) {
                [Def setObject:lat forKey:@"center_lat"];
                [Def setObject:lng forKey:@"center_lng"];
                [self setMapCenterLat:lat lng:lng];
            }
        }else{
            [Def removeObjectForKey:@"center_lat"];
            [Def removeObjectForKey:@"center_lng"];
        }
        [TOOL hideLoading:self.view];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
//设置地图中心
- (void)setMapCenterLat:(NSString *)lat lng:(NSString *)lng {
    MKCoordinateSpan theSpan;
    theSpan.latitudeDelta= 0.01;
    theSpan.longitudeDelta= 0.01;
    MKCoordinateRegion theRegion;
    theRegion.center= CLLocationCoordinate2DMake([lat floatValue], [lng floatValue]);
    theRegion.span= theSpan;
    [self.mapView setRegion:theRegion];
    [self.mapView regionThatFits:theRegion];
}
- (void)myLocationBtnClick {
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(self.mapView.userLocation.location.coordinate.latitude, self.mapView.userLocation.location.coordinate.longitude);
    [self.mapView setCenterCoordinate:location animated:YES];
}

- (void)setupMapView {
    _mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 108, App_Width, App_Height - 108)];
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    _mapView.scrollEnabled = YES;
    
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta = region.span.latitudeDelta * 1.5;
    region.span.longitudeDelta = region.span.longitudeDelta * 1.5;
    [self.mapView setRegion:region animated:YES];
    
    
    [self.view addSubview:self.mapView];
    CGFloat width = 24;
    CGFloat height = 34;
    UIImageView *centerImgView = [[UIImageView alloc]initWithFrame:CGRectMake((App_Width - width)*0.5, (App_Height - 108 )*0.5 - 28, width, height)];
    [_mapView addSubview:centerImgView];
    centerImgView.image = IMAGENAMED(@"red_envelopes");
    
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    self.lat = [NSString stringWithFormat:@"%f", mapView.region.center.latitude];
    self.lng = [NSString stringWithFormat:@"%f", mapView.region.center.longitude];
}


#pragma mark - 懒加载
- (RedChooseAreaView *)chooseAreaView {
    if (!_chooseAreaView) {
        _chooseAreaView = [RedChooseAreaView instanceView];
        _chooseAreaView.frame = CGRectMake(0, 64, App_Width, 44);
    }
    return _chooseAreaView;
}
- (UIButton *)sureBtn {
    if (!_sureBtn) {
        _sureBtn = [[UIButton alloc]initWithFrame:CGRectMake((App_Width - 150)*0.5, App_Height - 60, 150, 30)];
        [_sureBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_sureBtn setBackgroundColor:MTRGB(0xFA6B64)];
        [_sureBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        _sureBtn.layer.cornerRadius = 15;
        _sureBtn.layer.masksToBounds = YES;
        [_sureBtn addTarget:self action:@selector(sureBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _sureBtn.titleLabel.font = FFont(14);
    }
    return _sureBtn;
}
- (UIButton *)myLocationBtn {
    if (!_myLocationBtn) {
        _myLocationBtn = [[UIButton alloc]initWithFrame:CGRectMake(App_Width - 10 - 30, App_Height - 250, 30, 30)];
        [_myLocationBtn addTarget:self action:@selector(myLocationBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_myLocationBtn setImage:IMAGENAMED(@"myLocation") forState:UIControlStateNormal];
    }
    return _myLocationBtn;
}
@end
