//
//  RedChooseAddressController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/23.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCPositionModel.h"
typedef void (^RedChooseAddressBlock)(CCPositionModel *positionModel,NSString *lat,NSString *lng,NSInteger type);
NS_ASSUME_NONNULL_BEGIN

@interface RedChooseAddressController : BaseViewController
@property (nonatomic, strong) RedChooseAddressBlock chooseAddressBlock;

@end

NS_ASSUME_NONNULL_END
