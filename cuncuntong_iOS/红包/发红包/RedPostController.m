//
//  RedPostController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/2.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedPostController.h"
#import "RedPostTopView.h"
#import "RedPostTextView.h"
#import "RedPostChooseView.h"
#import "RedPostInfoView.h"
#import "MTUploadImageCell.h"
#import "KKPhotoPickerManager.h"
#import "MTUploadImageCell.h"
#import "BRStringPickerView.h"
#import "RedChooseAddressController.h"
#import "RechargeViewController.h"
static NSString *collectionViewCellId = @"collectionViewCellId";
static NSString *redTextSaveName = @"redDetail";
@interface RedPostController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) RedPostTopView *topView;
@property (nonatomic, strong) RedPostTextView *postTextView;
@property (nonatomic, strong) RedPostChooseView *chooseView;
@property (nonatomic, strong) RedPostInfoView *infoView;
@property (nonatomic, strong) UIView *picsView;

@property (nonatomic, assign) BOOL isAnswerRed;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, strong) NSMutableArray *deleteImgIndexArray; //记录删除的图片的位置
@property (strong, nonatomic) NSIndexPath *fromIndexPath;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@property (nonatomic, strong) CCPositionModel *positionModel;
@property (nonatomic, assign) NSInteger redDistanceType;
@property (nonatomic, strong) NSMutableArray *imgUrlArray;



@end

@implementation RedPostController

- (void)viewDidLoad {
    [super viewDidLoad];
    App_Delegate.maxPicNum = 9;
    App_Delegate.selectedPicNum = 0;
    self.imgUrlArray = [NSMutableArray array];
//    self.scrollView.contentInsetAdjustmentBehavior =  UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.topView];
    [self.scrollView addSubview:self.postTextView];
    [self.scrollView addSubview:self.chooseView];
    [self.scrollView addSubview:self.picsView];
    [self.scrollView addSubview:self.infoView];
    [self setCollectionView];
    [self updateFrame];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateFrame];
}
- (void)updateFrame {
    self.chooseView.hidden = !self.isAnswerRed;
    self.picsView.hidden = self.isAnswerRed;
    self.infoView.moneyContainerTopConstraint.constant = self.isAnswerRed ? 10 : 78;
    self.topView.frame = CGRectMake(0, 0, App_Width, 110);
    self.postTextView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), App_Width, 120);
    if (self.isAnswerRed) {
        self.chooseView.frame = CGRectMake(0, CGRectGetMaxY(self.postTextView.frame)+10, App_Width, 250);
    }else{
        self.picsView.frame = CGRectMake(0, CGRectGetMaxY(self.postTextView.frame), App_Width, 90);
    }
    CGFloat y = self.isAnswerRed ? CGRectGetMaxY(self.chooseView.frame)+10 : CGRectGetMaxY(self.picsView.frame) + 10;
    self.infoView.frame = CGRectMake(0, y, App_Width, 315);
    CGFloat height = self.isAnswerRed ? 750 : 650;
    self.scrollView.contentSize = CGSizeMake(App_Width, height);

}
- (void)post {
    if (![self enptyCheck])return;
    if (self.isAnswerRed || self.imageArray.count == 0) {
        [self postRed];
    }else{
        [self uploadImg];
    }
}
- (void)uploadImg {
    if (self.imageArray.count == 0) {
        //上传完成
        [self postRed];
        return;
    }
    [TOOL showLoading:self.view];
    NSData *data =  UIImagePNGRepresentation(self.imageArray[0]);
    data = [TOOL imageCompress:data];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"freight/addimg") params:@{@"r_id":USERID,@"name":@"file"} target:self imgaeData:data success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [self.imageArray removeObjectAtIndex:0];
            NSString *url = success[@"msg"];//[NSString stringWithFormat:@"http://www.cct369.com/village/public%@", success[@"msg"]];
            [self.imgUrlArray addObject:url];
            [self uploadImg];
        }else{
            [self.imgUrlArray removeAllObjects];
            SHOW(success[@"msg"]);
            [TOOL hideLoading:self.view];
        }
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
    }];
}
- (void)postRed {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary: @{
                            @"r_id":USERID,
                            @"red_type":self.isAnswerRed ? @"1":@"0",
                            @"lat":STR(self.lat ?: App_Delegate.lat),
                            @"lng":STR(self.lng ?: App_Delegate.lng),
                            @"red_total_money":self.infoView.moneyField.text,
                            @"red_count":self.infoView.countField.text,
                            @"title":self.postTextView.textView.text,
                            }];
    NSString *href = [self.chooseView.answerSearchField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [param setValue:href forKey:@"red_href"];
    if (self.isAnswerRed) {
        NSString *ques = [NSString stringWithFormat:@"%@&%@&%@",self.chooseView.AField.text,self.chooseView.BField.text,self.chooseView.CField.text];
        [param setValue:ques forKey:@"red_key"];
        NSString *naswer = self.chooseView.ABtn.selected ? self.chooseView.AField.text : self.chooseView.BBtn.selected ? self.chooseView.BField.text : self.chooseView.CField.text;
        [param setValue:naswer forKey:@"red_key_ok"];
        
    }else{
        if (self.imgUrlArray.count > 0) {
           NSString *imgs =  [self.imgUrlArray componentsJoinedByString:@","];
            [param setValue:imgs forKey:@"img"];
        }
    }
    if (self.redDistanceType > 3) {//距离红包
        [param setValue:@"1" forKey:@"distance"];
    }
    if (self.redDistanceType > 2) {//区红包
        [param setValue:STR(self.positionModel.districtId) forKey:@"area"];
    }
    if (self.redDistanceType > 1) {//城市红包
        [param setValue:STR(self.positionModel.cityId) forKey:@"city"];
    }
    if (self.redDistanceType > 0) {//省红包
        [param setValue:STR(self.positionModel.provinceId) forKey:@"pro"];
    }
    //全国红包不传位置
    
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"mfkit/red/addRedPacket") params:param target:self success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"发布成功");
            [self saveRedDetail];
            [self dismissViewControllerAnimated:YES completion:nil];
        }else if([success[@"error"] integerValue] == 2){
            //余额不足
            RechargeViewController *vc = [[RechargeViewController alloc]init];
            vc.money = [NSString stringWithFormat:@"%.2f", [self.infoView.moneyField.text floatValue] - [[UserManager sharedInstance].money floatValue]];
            vc.block = ^{
                [self.navigationController popViewControllerAnimated:YES];
                [self postRed];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
    
}
- (BOOL)enptyCheck {
    if (self.postTextView.textView.text.length == 0) {
        SHOW(@"请输入红包内容");
        return NO;
    }
    if (self.infoView.moneyField.text.length == 0 || [self.infoView.moneyField.text floatValue] <=0) {
        SHOW(@"请输入正确的红包金额");
        return NO;
    }
    if (self.infoView.countField.text.length == 0 || [self.infoView.countField.text integerValue] <=0) {
        SHOW(@"请输入正确的红包个数");
        return NO;
    }
    if (self.isAnswerRed) {
        if (self.chooseView.AField.text.length == 0 || self.chooseView.BField.text.length == 0 || self.chooseView.CField.text.length == 0) {
            SHOW(@"请将答案选择项填写完整");
            return NO;
        }
    }
    if (self.isAnswerRed) {
        if ([self.infoView.moneyField.text floatValue] / [self.infoView.countField.text floatValue] < 0.3) {
            SHOW(@"答题红包单个金额不能低于0.3元");
            return NO;
        }
    }else{
        if ([self.infoView.moneyField.text floatValue] / [self.infoView.countField.text floatValue] < 0.02){
            SHOW(@"答题红包单个金额不能低于0.02元");
            return NO;
        }
    }
    return YES;
}
- (void)saveRedDetail {
    NSString *redText = self.postTextView.textView.text;
    NSString *href  = self.isAnswerRed ? self.chooseView.answerSearchField.text : self.infoView.adField.text;
    NSString *urls = [self.imgUrlArray componentsJoinedByString:@","];
    NSString *answer = @"";
    if (self.isAnswerRed) {
        answer = [NSString stringWithFormat:@"%@,%@,%@,%@",self.chooseView.AField.text,self.chooseView.BField.text,self.chooseView.CField.text,@(self.chooseView.selectedIndex)];
    }
    NSString *saveText = [@[redText,href,urls,answer] componentsJoinedByString:@"&"];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSArray *array = [TOOL getCachaArrayWithName:redTextSaveName];
        BOOL exist = NO;
        for (NSString *text in array) {
            if ([text containsString:[NSString stringWithFormat:@"%@&%@",redText,href]]) {
                exist = YES;
                break;
            }
        }
        if (!exist) {
            NSMutableArray *arrayM = [NSMutableArray arrayWithArray:array];
            [arrayM insertObject:saveText atIndex:0];
            if (arrayM.count > 30) {
                [arrayM removeLastObject];
            }
            [TOOL saveArrayWithData:arrayM Name:redTextSaveName];
        }
    });
}
//红包类型
- (void)typeChange:(UIButton *)sender {
    self.isAnswerRed = sender == self.topView.answerRedBtn;
    self.topView.normalRedLine.hidden = self.isAnswerRed;
    self.topView.answerRedLine.hidden = !self.isAnswerRed;
    self.infoView.adTipsLbl.hidden = self.isAnswerRed;
    [self updateFrame];
}
- (void)cancelBtnClick {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//调用历史内容
- (void)dispatchBtnClick{
    [self.view endEditing:YES];
    NSArray *textArray = [TOOL getCachaArrayWithName:redTextSaveName];
    NSMutableArray *showArray = [NSMutableArray array];
    for (NSString *text in textArray) {
        NSArray *array = [text componentsSeparatedByString:@"&"];
        if (array.count > 0) {
            NSString *subText = array[0];
            [showArray addObject:subText];
        }
    }
    if (textArray.count > 0) {
        [BRStringPickerView showStringPickerWithTitle:@"选择历史发布内容" dataSource:showArray defaultSelValue:showArray[0] isAutoSelect:NO resultBlock:^(id selectValue) {
            self.postTextView.textView.text = (NSString *)selectValue;
            self.postTextView.placeHolderLbl.hidden = YES;
            NSInteger index  =0;
            for (int i =0; i<showArray.count; i++) {
                NSString *text  = showArray[i];
                if ([text isEqual:selectValue]) {
                    index = i;
                    break;
                }
            }
            NSString *fullText = textArray[index];
            NSArray *array = [fullText componentsSeparatedByString:@"&"];
            if (array.count > 1) {
                if (self.isAnswerRed) {
                    self.chooseView.answerSearchField.text = array[1];
                }else{
                    self.infoView.adField.text = array[1];
                }
            }
            if (array.count > 2) {
                NSString *urls = array[2];
                NSArray *imgArray = [urls componentsSeparatedByString:@","];
                [self showDispatchImgs:imgArray];
                NSLog(@"%@",imgArray);
            }
            if (self.isAnswerRed && array.count > 3) {
                NSString *text = array[3];
                NSArray *array = [text componentsSeparatedByString:@","];
                if (array.count == 4) {
                    self.chooseView.AField.text = array[0];
                    self.chooseView.BField.text = array[1];
                    self.chooseView.CField.text = array[2];
                    self.chooseView.selectedIndex = [array[3] integerValue];
                }
            }
        }];
    }else{
        SHOW(@"无历史发布内容");
    }
}
- (void)showDispatchImgs:(NSArray *)imgArray {
    [TOOL showLoading:self.view];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (NSString *url in imgArray) {
            if (!STRINGEMPTY(url)) {
                NSString *imageUrl = BASEURL_WITHOBJC(url);
                UIImage *image = [UIImage imageWithData:[NSData  dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]];
                [self.imageArray addObject:image];                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
            [TOOL hideLoading:self.view];
        });
        
    });
}
- (void)chooseAddress {
    RedChooseAddressController *vc = [[RedChooseAddressController alloc]init];
    WEAKSELF
    vc.chooseAddressBlock = ^(CCPositionModel *positionModel, NSString *lat, NSString *lng,NSInteger selectedIndex) {
        weakSelf.positionModel = positionModel;
        weakSelf.lat = lat;
        weakSelf.lng = lng;
        weakSelf.redDistanceType = selectedIndex;
        NSArray *text = @[@"全国",@"全省",@"全市",@"全区/县",@"一公里"];
        [weakSelf.infoView.addressBtn setTitle:text[selectedIndex] forState:UIControlStateNormal];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)textViewDidChange:(UITextView *)textView {
    self.postTextView.placeHolderLbl.hidden = textView.text.length != 0;
}
#pragma mark - 选择图片模块
- (void)setCollectionView{
    self.imageArray = [NSMutableArray array];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(80, 80);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 10;
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, App_Width-30, 80) collectionViewLayout:layout];
    self.collectionView = collectionView;
    [self.picsView addSubview:collectionView];
    
    [self.collectionView registerClass:[MTUploadImageCell class] forCellWithReuseIdentifier:collectionViewCellId];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    //长按换位置
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(couldExchange:)];
    [self.collectionView addGestureRecognizer:longPress];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.imageArray.count <= 9) {
        return self.imageArray.count + 1;
    }
    return self.imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MTUploadImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellId forIndexPath:indexPath];
    //添加子控件,设置布局与控件图片
    [self addAndSetSubViews:cell indexPath:indexPath];
    return cell;
}

- (void)addAndSetSubViews:(MTUploadImageCell *)cell indexPath:(NSIndexPath *)indexPath{
    //清空子控件,解决重用问题
    NSArray *subviews = [[NSArray alloc] initWithArray:cell.contentView.subviews];
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
    UIImageView *imageView = [[UIImageView alloc]init];
    [cell.contentView addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    cell.tag = 11; //根据tag值设定是否可点击,11可点击,初始全部可点击
    cell.imageView = imageView;
    cell.backgroundColor = [UIColor whiteColor];
    if(indexPath.row == 0 ){
        imageView.image = [UIImage imageNamed:@"add_pic"];
    }else{
        imageView.image = nil;
    }
    
    UIButton *cancleBtn = [[UIButton alloc]init];
    cell.cancleBtn = cancleBtn;
    [cell.contentView addSubview: cancleBtn];
    [cancleBtn setImage:[UIImage imageNamed:@"removePic"] forState:UIControlStateNormal];
    cancleBtn.hidden = YES;//初始将删除按钮隐藏
    cell.cancleBtn.tag = indexPath.row;
    [cell.cancleBtn addTarget:self action:@selector(cancleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.imageView.frame = CGRectMake(0, 0, 80, 80);
    cell.cancleBtn.frame = CGRectMake(0, 0, 20, 20);
    if (indexPath.row > 0) {
        if (self.imageArray.count > indexPath.row-1) {
            if ([self.imageArray[indexPath.row - 1] isKindOfClass:[UIImage class]]) {
                cell.imageView.image = nil;
                cell.imageView.image = self.imageArray[indexPath.row - 1];
                cell.cancleBtn.hidden = NO;
                cell.tag = 10; //初始设置tag为11,当为10时,表示已经添加图片
            }
        }
    }
}
//点击collectionView跳转到相册
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView cellForItemAtIndexPath:indexPath].tag == 11) {
        [self choosePic];
    }
}
- (void)choosePic {
    [self.view endEditing:YES];
    [[KKPhotoPickerManager shareInstace] showActionSheetInView:self.view fromController:self completionBlock:^(NSMutableArray *imageArray) {
        for (int i = 0; i<imageArray.count; i++) {
            if (self.imageArray.count < 5) {
                UIImage *image = imageArray[i];
                [self.imageArray addObject:image]; //上传图片保存到数组
            }
        }
        App_Delegate.maxPicNum = 5 - self.imageArray.count;
        App_Delegate.selectedPicNum = self.imageArray.count;
        [self.collectionView reloadData];
    }];
}
//移动顺序
- (void)couldExchange:(UILongPressGestureRecognizer *)longPressGesture{
    switch (longPressGesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:[longPressGesture locationInView:self.collectionView]];
            if (indexPath.item == 0) {
                self.fromIndexPath = nil;
            }else{
                [self moveAnimation:indexPath];
                self.fromIndexPath = indexPath;
            }
        }
            break;
        case UIGestureRecognizerStateChanged:{
            NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:[longPressGesture locationInView:self.collectionView]];
            if(indexPath.item == 0){
                return;
            }
            if (indexPath && indexPath.row!=self.fromIndexPath.row && self.fromIndexPath) {
                [self.imageArray exchangeObjectAtIndex:indexPath.row -1 withObjectAtIndex:self.fromIndexPath.row -1];
                [self.collectionView moveItemAtIndexPath:self.fromIndexPath toIndexPath:indexPath];
                self.fromIndexPath = indexPath;
            }
        }
            break;
        case UIGestureRecognizerStateEnded:{
            NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:[longPressGesture locationInView:self.collectionView]];
            if (self.fromIndexPath) {
                [self stopAnimation:self.fromIndexPath];
            }
            if (indexPath.item == 0) {
                
                return;
            }
            if (indexPath && indexPath.row!=self.fromIndexPath.row && self.fromIndexPath) {
                [self.imageArray exchangeObjectAtIndex:indexPath.row - 1 withObjectAtIndex:self.fromIndexPath.row - 1];
                [self.collectionView moveItemAtIndexPath:self.fromIndexPath toIndexPath:indexPath];
            }
            [self.collectionView reloadData];
        }
            break;
        default:
            break;
    }
}
//长按动画
- (void)moveAnimation:(NSIndexPath *)indexPath {
    MTUploadImageCell *cell = (MTUploadImageCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:0.1 animations:^{
        cell.imageView.frame = CGRectMake(5, 5, 70, 70);
        cell.cancleBtn.frame = CGRectMake(0, 0, 20, 20);
    }];
}
//取消移动动画
- (void)stopAnimation:(NSIndexPath *)indexPath {
    MTUploadImageCell *cell = (MTUploadImageCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:0.1 animations:^{
        cell.imageView.frame = CGRectMake(0, 0, 80, 80);
        cell.cancleBtn.frame = CGRectMake(-3, -3, 20, 20);
    }];
}
//删除图片
- (void)cancleBtnClick:(UIButton *)sender{
    if (sender.tag -1 < self.imageArray.count) {
        [self.imageArray removeObjectAtIndex:sender.tag - 1];
        sender.hidden = YES;
        App_Delegate.maxPicNum = 5 - self.imageArray.count;
        App_Delegate.selectedPicNum = self.imageArray.count;
        [self.collectionView reloadData];
        
    }
}


#pragma mark - 懒加载
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView  alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height)];
        _scrollView.backgroundColor = MTRGB(0xf2f2f2);
        
    }
    return _scrollView;
}
- (RedPostTopView *)topView {
    if (!_topView) {
        _topView = [RedPostTopView instanceView];
        [_topView.normalRedBtn addTarget:self action:@selector(typeChange:) forControlEvents:UIControlEventTouchUpInside];
        [_topView.answerRedBtn addTarget:self action:@selector(typeChange:) forControlEvents:UIControlEventTouchUpInside];
        [_topView.cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_topView.dispatchBtn addTarget:self action:@selector(dispatchBtnClick) forControlEvents:UIControlEventTouchUpInside];


    }
    return _topView;
}
- (RedPostTextView *)postTextView {
    if (!_postTextView) {
        _postTextView = [RedPostTextView instanceView];
        _postTextView.textView.delegate = self;
    }
    return _postTextView;
}
- (RedPostChooseView *)chooseView {
    if (!_chooseView) {
        _chooseView = [RedPostChooseView instanceView];
        
    }
    return _chooseView;
}
- (RedPostInfoView *)infoView {
    if (!_infoView) {
        _infoView = [RedPostInfoView instanceView];
        [_infoView.addressBtn addTarget:self action:@selector(chooseAddress) forControlEvents:UIControlEventTouchUpInside];
        [_infoView.postBtn addTarget:self action:@selector(post) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _infoView;
}
- (UIView *)picsView {
    if (!_picsView) {
        _picsView = [[UIView alloc]init];
        _picsView.backgroundColor = WHITECOLOR;
        
    }
    return _picsView;
}

- (void)dealloc{
    NSLog(@"销毁了");
}
@end
