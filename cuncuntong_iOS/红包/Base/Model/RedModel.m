//
//  RedModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import "RedModel.h"
@implementation RedOpenModel
@end
@implementation RedModel
- (NSArray *)imgArray {
    if (self.img && self.img.length > 0) {
        return [self.img componentsSeparatedByString:@","];
    }
    return nil;
}
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"rednew_get_red" : @"new_get_red"};
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"is_open" : [RedOpenModel class]};
}
@end
