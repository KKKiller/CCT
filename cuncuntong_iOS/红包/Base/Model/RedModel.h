//
//  RedModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/15.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RedOpenModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *rid;
@property (nonatomic, strong) NSString *red_id;
@property (nonatomic, strong) NSString *energy;
@property (nonatomic, strong) NSString *add_time;
@property (nonatomic, strong) NSString *is_picked;
@property (nonatomic, strong) NSString *is_stolen;
@property (nonatomic, strong) NSString *ownerid;
@property (nonatomic, strong) NSString *next_r_id;
@property (nonatomic, strong) NSString *add_red_money;//抢到的钱
@property (nonatomic, strong) NSString *value;//回答问题 错误的值

@end

@interface RedModel : NSObject
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *energy;
@property (nonatomic, strong) RedOpenModel *is_open;
@property (nonatomic, strong) NSString *img;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@property (nonatomic, strong) NSString *location_encode;
@property (nonatomic, strong) NSString *practical_money;
@property (nonatomic, strong) NSString *pro;
@property (nonatomic, strong) NSString *r_id;
@property (nonatomic, assign) NSInteger red_count;
@property (nonatomic, strong) NSString *red_href;
@property (nonatomic, strong) NSString *red_id;
@property (nonatomic, strong) NSString *red_key;
@property (nonatomic, strong) NSString *red_key_ok;
@property (nonatomic, strong) NSString *red_random;
@property (nonatomic, assign) NSInteger red_residue_count;
@property (nonatomic, strong) NSString *red_total_money;
@property (nonatomic, strong) NSString *red_type;
@property (nonatomic, strong) NSString *residue_energy;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray *imgArray;
@property (nonatomic, strong) NSArray *rednew_get_red;
@property (nonatomic, strong) NSString *portrait;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *this_discuss;// !=null表示评论过
@property (nonatomic, strong) NSDictionary *discuss;
@property (nonatomic, assign) BOOL commented;
@end
