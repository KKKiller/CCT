//
//  CCTransportTabbarController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCRedTabbarController.h"
#import "NavigationViewController.h"
#import "RedHomeController.h"
#import "RedProfileController.h"
#import "RedTabbar.h"
#import "CCShareHomeController.h"
#import "MYHomeController.h"
#import "RedPostController.h"
@interface CCRedTabbarController ()<UITabBarControllerDelegate>
@property (nonatomic, strong) UIButton *leftButton;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) RedTabbar *redTabbar;

@end

@implementation CCRedTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTabbar];
    self.delegate = self;
    self.navigationItem.title = @"红包";
    [self initSelf];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    App_Delegate.redTabbarVc = self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    self.delegate = nil;
    App_Delegate.redTabbarVc = nil;
}

- (void)initSelf {
    NSArray *vcArr;
    NSArray *imageArr;
    NSArray *selImageArr;
    NSArray *titleArr;
    
        vcArr = @[@"RedHomeController",
                           @"CCShareHomeController",
                            @"RedPostController",
                           @"MYHomeController",
                           @"RedProfileController"];
        
        imageArr = @[@"btn_collect_red_packets",
                              @"btn_life_circle",
                     @"",
                              @"btn_golden_beans",
                              @"btn_my_wallet"];
        
    selImageArr =    @[@"btn_collect_red_packets",
                                       @"btn_life_circle",
                                       @"",
                                       @"btn_golden_beans",
                                       @"btn_my_wallet"];

        
        titleArr = @[@"收红包",
                     @"生活圈",
                     @"",
                     @"种金豆",
                     @"我的"];
    
    
    NSMutableArray *tabArrs = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < vcArr.count; i++) {
        UIViewController *vc ;
//        if ( !App_Delegate.defaultHome ||  [App_Delegate.defaultHome isEqualToString:@"default"]) {
            vc = [[NSClassFromString(vcArr[i]) alloc] init];
//        }else{
//            UIViewController *vvcc = [[NSClassFromString(vcArr[i]) alloc] init];
//            vc = [[NavigationViewController alloc] initWithRootViewController:vvcc];
//
//        }
        
//        UIViewController *vc = [[NSClassFromString(vcArr[i]) alloc] init];
        vc.tabBarItem.image = [[UIImage imageNamed:imageArr[i]]
                               imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selImageArr[i]]
                                       imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.title = titleArr[i];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x8E8E93)} forState:UIControlStateNormal];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName :MTRGB(0xea736a) } forState:UIControlStateSelected];


        
        [tabArrs addObject:vc];
    }
    
    // 解决push/pop黑影
    self.view.backgroundColor = MTRGB(0xf7f7f7);
    self.tabBar.tintColor = [UIColor greenColor];
    self.viewControllers = tabArrs;
    
}
- (void)setTabbar {
    self.redTabbar = [[RedTabbar alloc] init];
    [self.redTabbar.centerBtn addTarget:self action:@selector(postRed) forControlEvents:UIControlEventTouchUpInside];
    //选中时的颜色
    self.redTabbar.tintColor = [UIColor colorWithRed:27.0/255.0 green:118.0/255.0 blue:208/255.0 alpha:1];
    //    self.tabBar.barTintColor = [UIColor whiteColor];
    //透明设置为NO，显示白色，view的高度到tabbar顶部截止，YES的话到底部
    self.redTabbar.translucent = NO;
    self.redTabbar.delegate = self;
    //利用KVC 将自己的tabbar赋给系统tabBar
    [self setValue:self.redTabbar forKeyPath:@"tabBar"];
    
}
- (void)postRed {
    RedPostController *vc = [[RedPostController alloc]init];
    NavigationViewController *nav = [[NavigationViewController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
- (void)setSelectIndex:(NSInteger)index {
    self.selectedIndex = index;
}
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    WEAKSELF
    if ([viewController  isKindOfClass:[CCShareHomeController class]]) {
        CCShareTabbarController *shareVc = [[CCShareTabbarController alloc]init];
        [weakSelf.navigationController pushViewController:shareVc animated:YES];
        return NO;
    }else if ([viewController isKindOfClass:[MYHomeController class]]){
        MYHomeController *vc = [[MYHomeController alloc]init];
        [weakSelf.navigationController pushViewController:vc animated:YES];
        return NO;
    }else if ([viewController isKindOfClass:[RedPostController class]]){
        [weakSelf postRed];
        return NO;
    }
    return YES;
}
- (void)setRightTitle:(NSString *)rightTitle {
    _rightTitle = rightTitle;
    [self.rightBtn setTitle:rightTitle forState:UIControlStateNormal];
}

-(void)backMove {
    if ([App_Delegate.window.rootViewController isKindOfClass:[self class]]) {
        App_Delegate.defaultHome = @"default";
        [App_Delegate setRootViewController];
    }else{
        UINavigationController *nav = self.selectedViewController;
        if ([self.selectedViewController isKindOfClass:[UINavigationController class]] &&  nav.viewControllers.count > 1) {
            [nav popViewControllerAnimated:YES];
        }else{
            [App_Delegate.tabbarVc gotoHome];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_leftButton addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        self.navigationItem.leftBarButtonItem = left;
    }
    return _leftButton;
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,10, 60, 20)];
        _rightBtn.titleLabel.font = FFont(13);
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:_rightBtn];
        self.navigationItem.rightBarButtonItem = right;
        _rightBtn.titleLabel.contentMode = UIViewContentModeRight;
    }
    return _rightBtn;
}
@end
