//
//  RedTabbar.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/12/2.
//  Copyright © 2018 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedTabbar : UITabBar
@property (nonatomic, strong) UIButton *centerBtn; //中间按钮
@end
