//
//  CCTransportTabbarController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/11/1.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCRedTabbarController : UITabBarController
@property (nonatomic, strong) NSString *rightTitle;
@property (nonatomic, strong) UIButton *rightBtn;



- (void)setSelectIndex:(NSInteger)index;

@end
