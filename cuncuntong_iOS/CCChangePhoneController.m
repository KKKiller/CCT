//
//  CCChangePhoneController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/9/2.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCChangePhoneController.h"
#import "CCTTextField.h"
#import "CCTButton.h"
#import "ResetFinishController.h"
#import "ResetViewController.h"
#import "CCBindAliController.h"
@interface CCChangePhoneController ()<NSURLSessionDelegate> {
    
    UIButton *_verButton;
    
    CCTTextField *_userNameTF;
    
}
@property (nonatomic, strong) NSMutableData *changePhoneData;

@end

@implementation CCChangePhoneController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = _navTitle;
    
    [self navConfig];
    [self uiConfig];
    
   
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.view.backgroundColor = WHITECOLOR;
}
- (void)navConfig {
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame = CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE);
    [backButton setImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backItem;
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)uiConfig {
    
    _userNameTF = [[CCTTextField alloc] init];
    [self.view addSubview:_userNameTF];
    [_userNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(64 + 23 * KEY_RATE);
        make.left.equalTo(@(15 * KEY_RATE));
        make.right.equalTo(@(-15 * KEY_RATE));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    if (self.ali) {
        _userNameTF.placeholder = @"请输入登陆密码";
    }else{
        _userNameTF.placeholder = @"请输入原登陆密码";
    }
    
    UIImageView *userImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 18.5 * KEY_RATE)
                                                   image:@"img_mima"];
    _userNameTF.leftView = userImgv;
    
    
    for (NSInteger i = 0; i < 2; i++) {
        UIView *seperatorView = [[UIView alloc] init];
        [self.view addSubview:seperatorView];
        [seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_userNameTF.mas_bottom).with.offset(i * (56 * KEY_RATE + 1));
            make.left.equalTo(_userNameTF.mas_left);
            make.right.equalTo(_userNameTF.mas_right);
            make.height.equalTo(@1);
        }];
        seperatorView.backgroundColor = RGB(238, 238, 238);
        
    }
    
   
    CCTButton *nextStepButton = [[CCTButton alloc] init];
    [self.view addSubview:nextStepButton];
    
    [nextStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_userNameTF);
        make.height.mas_equalTo(44);
        make.top.equalTo(_userNameTF.mas_bottom).with.offset(36 * KEY_RATE);
    }];
    [nextStepButton setTitle:@"下一步" forState:UIControlStateNormal];
    nextStepButton.titleLabel.font = FFont(14);
    
    [nextStepButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Target Action

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)nextStep {
    
    // 判断手机号码
    if (_userNameTF.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入正确的密码"];
        return;
    }
//    if(self.ali){+
    
    
//    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/reset_mobile_1") params:@{@"password": _userNameTF.text, @"rid": USERID} success:^(NSDictionary *success) {
//        NSLog(@"%@",success);
//
//    } failure:^(NSError *   failure) {
//
//    }];
    
    
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"accout/reset_mobile_1") params:@{@"passwd": _userNameTF.text,@"rid":USERID} target:self success:^(NSDictionary *success) {
            if([success[@"error"] integerValue] == 0){
                
                NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage]cookiesForURL:[NSURL URLWithString:BASEURL_WITHOBJC(@"account/reset_mobile_1")]];
                for (NSHTTPCookie *tempCookie in cookies){
                    NSLog(@"getCookie:%@",tempCookie);
                }
                NSDictionary *Request = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
                NSUserDefaults *userCookies = [NSUserDefaults standardUserDefaults];
                [userCookies setObject:[Request objectForKey:@"Cookie"] forKey:@"mUserDefaultsCookie"];

                
                if (self.ali) {
                    CCBindAliController *vc = [[CCBindAliController alloc]init];
                    vc.password = _userNameTF.text;
                    [self.navigationController pushViewController:vc animated:YES];
                }else{
                    ResetViewController *vc = [[ResetViewController alloc] init];
                    vc.navTitle = @"更换手机号";
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }else{
                NSString *msg = success[@"msg"];
                SHOW(msg);
            }
            
        } failure:^(NSError *failure) {
        }];
//    }
//else{
//        [self verifyPassword];
//    }

    
    
}

@end
