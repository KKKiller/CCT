//
//  CCBindAliController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/9/2.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCBindAliController.h"
#import "CCTTextField.h"
#import "CCTButton.h"
#import "ResetFinishController.h"
@interface CCBindAliController (){
    
//    UIButton *_verButton;
    
    CCTTextField *_userNameTF;
    
    CCTTextField *_verCodeTF;
}

@end

@implementation CCBindAliController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"绑定支付宝";
    
    
    [self navConfig];
    [self uiConfig];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = WHITECOLOR;
}
- (void)navConfig {
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame = CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE);
    [backButton setImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backItem;
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)uiConfig {
    
    _userNameTF = [[CCTTextField alloc] init];
    [self.view addSubview:_userNameTF];
    [_userNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(64 + 23 * KEY_RATE);
        make.left.equalTo(@(15 * KEY_RATE));
        make.right.equalTo(@(-15 * KEY_RATE));
        make.height.equalTo(@(56 * KEY_RATE));
    }];
    
    _userNameTF.placeholder = @"请输入支付宝姓名";
    UIImageView *userImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 15 * KEY_RATE, 18.5 * KEY_RATE)
                                                   image:@"img_denglu"];
    _userNameTF.leftView = userImgv;
    
    if ([Def valueForKey:@"aliName"]) {
        _userNameTF.text = [Def valueForKey:@"aliName"];
        _userNameTF.enabled = NO;
    }
    
    for (NSInteger i = 0; i < 2; i++) {
        UIView *seperatorView = [[UIView alloc] init];
        [self.view addSubview:seperatorView];
        [seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_userNameTF.mas_bottom).with.offset(i * (56 * KEY_RATE + 1));
            make.left.equalTo(_userNameTF.mas_left);
            make.right.equalTo(_userNameTF.mas_right);
            make.height.equalTo(@1);
        }];
        seperatorView.backgroundColor = RGB(238, 238, 238);
        
    }
    
    _verCodeTF = [[CCTTextField alloc] init];
    [self.view addSubview:_verCodeTF];
    _verCodeTF.secureTextEntry = YES;
    _verCodeTF.clearButtonMode = UITextFieldViewModeNever;
    [_verCodeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15 * KEY_RATE));
        make.right.equalTo(@(-15 * KEY_RATE));
        make.height.equalTo(@(56 * KEY_RATE));
        make.top.equalTo(_userNameTF.mas_bottom).with.offset(1);
    }];
    _verCodeTF.placeholder = @"请输入支付宝账号";
    if ([UserManager sharedInstance].mobile) {
        _verCodeTF.enabled = NO;
        _verCodeTF.text = [UserManager sharedInstance].mobile;
    }
    UIImageView *passImgv = [MyUtil createImageViewFrame:CGRectMake(0, 0, 17 * KEY_RATE, 14 * KEY_RATE) image:@"img_duanxin"];
    _verCodeTF.leftView = passImgv;
    
//
    CCTButton *nextStepButton = [[CCTButton alloc] init];
    [self.view addSubview:nextStepButton];
    
    [nextStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_verCodeTF);
        make.height.mas_equalTo(44);
        make.top.equalTo(_verCodeTF.mas_bottom).with.offset(36 * KEY_RATE);
    }];
    [nextStepButton setTitle:@"提交" forState:UIControlStateNormal];
    nextStepButton.titleLabel.font = FFont(14);
    
    [nextStepButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Target Action

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)nextStep {
    
    // 判断手机号码
    if (_userNameTF.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入姓名"];
        return;
    }
    // 判断验证码
    if (_verCodeTF.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入支付宝账号"];
        return;
    }
   
        //验证验证码是否正确  todo
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/rebind_aplipay") params:@{@"passwd":self.password,@"rid":USERID,@"realname":[Def valueForKey:@"realname"],@"alipay_name":_userNameTF.text,@"alipay_account":_verCodeTF.text} target:self success:^(NSDictionary *success) {
            NSLog(@"%@",success);
            if ([success[@"error"] integerValue] == 0) {
                SHOW(@"绑定成功");
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                NSString *msg = success[@"msg"];
                SHOW(msg);
            }
            
        } failure:^(NSError *failure) {
        }];

    
}


@end
