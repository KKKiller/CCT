//
//  CCShiXiVillageListView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCHomeAdminModel.h"
@interface CCShiXiVillageListView : UIView
@property (nonatomic, strong) NSArray *dataArray;
@end


@interface CCShiXIVillageCell : UITableViewCell
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel *progressLbl;
@property (nonatomic, strong) UIImageView *timeImgView;
@property (nonatomic, strong) UILabel *remainDayLbl;
@property (nonatomic, strong) UILabel *lineLbl;
@property (nonatomic, strong) CCHomeAdminModel *adminModel;
@end
