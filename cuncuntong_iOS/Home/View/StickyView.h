//
//  StickyView.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/15.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCVillageModel;
@class CCHomeVillageModel;
@protocol  StickyDelegate <NSObject>
@optional
//- (void)removeTopView:(UIButton *)sender;
- (void)replyManagerBtnClick:(BOOL)isVillage;
- (void)clickAtHomeBtn:(NSInteger)index;
- (void)clickAtHomeBanner;
- (void)showApplyList;
- (void)clickAtMZ;
- (void)pushToAgentWithId:(NSString *)idStr; //村代理 县代理
@end

@interface StickyView : UIView

//- (void)reload:(NSDictionary *)dic;

@property (nonatomic, weak) id<StickyDelegate> delegate;
@property (nonatomic, strong) CCHomeVillageModel *villageModel;
@property (nonatomic, strong) NSArray *shixiVillageArray;
@property (nonatomic, strong) UIImageView *bannerView;

@end

@interface VillageCellView : UIView
@property (nonatomic, strong) UILabel *lineLbl;
@property (nonatomic, strong) UIImageView *locationImgV;
@property (nonatomic, strong) UILabel *addressLbl;
@property (nonatomic, strong) UIImageView *locationImgV2;
@property (nonatomic, strong) UILabel *addressLbl2;
@property (nonatomic, strong) UILabel *applyManagerLbl;
@property (nonatomic, strong) UIButton *showDetailBtn; //查看
@property (nonatomic, strong) UIImageView *applyManagerImgView;

@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel *progressLbl;
@property (nonatomic, strong) UIImageView *timeImgView;
@property (nonatomic, strong) UILabel *remainDayLbl;
@property (nonatomic, weak) id<StickyDelegate> delegate;
@end

@interface  CountryCellView : UIView
@property (nonatomic, strong) UILabel *applyManagerLbl;
@property (nonatomic, strong) UIImageView *applyManagerImgView;
//@property (nonatomic, strong) UILabel *finishedCountLbl;
//@property (nonatomic, strong) UILabel *totlaCountLbl;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel *progressLbl;
@property (nonatomic, strong) UIImageView *timeImgView;
@property (nonatomic, strong) UILabel *remainDayLbl;
@property (nonatomic, strong) UILabel *lineLbl;
@end
