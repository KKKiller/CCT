//
//  CCHome3PicCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/4/15.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCHome3PicCell.h"

@implementation CCHome3PicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(CCHomeModel *)model {
    _model = model;
    _title.text = model.title;
    _source.text = [NSString stringWithFormat:@"%@ %@评论", model.original,@(model.comment ?: 0)];
    if (self.model.coverArr.count >= 1) {
        [_imgVIew1 setImageWithURL:[NSURL URLWithString:model.coverArr[0]] placeholder:IMAGENAMED(@"SubPlaceholder")];
    }
    if (self.model.coverArr.count >= 2) {
        [_imgView2 setImageWithURL:[NSURL URLWithString:model.coverArr[1]] placeholder:IMAGENAMED(@"SubPlaceholder")];
    }
    if (self.model.coverArr.count >= 3) {
        [_imgView3 setImageWithURL:[NSURL URLWithString:model.coverArr[2]] placeholder:IMAGENAMED(@"SubPlaceholder")];
    }
}
@end
