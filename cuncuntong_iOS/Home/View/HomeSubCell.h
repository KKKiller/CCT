//
//  HomeSubCell.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/29.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeSubModel.h"

@interface HomeSubCell : UITableViewCell

@property (nonatomic, strong) HomeSubModel *model;
@property (nonatomic, strong) YYLabel *titleLabel;

@end
