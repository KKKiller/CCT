//
//  CCHomePicCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/4/15.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCHomePicCell.h"

@implementation CCHomePicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(CCHomeModel *)model {
    _model = model;
    _title.text = model.title;
    _source.text = [NSString stringWithFormat:@"%@ %@评论", model.original,@(model.comment ?: 0)];    [_imgView setImageWithURL:[NSURL URLWithString:model.cover] placeholder:IMAGENAMED(@"SubPlaceholder")];
}

@end
