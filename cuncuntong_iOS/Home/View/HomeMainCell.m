//
//  HomeMainCell.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/29.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "HomeMainCell.h"

@interface HomeMainCell()

@end

@implementation HomeMainCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        _portraitImgv = [[UIImageView alloc] init];
//        _portraitImgv.layer.cornerRadius = 17.5 * KEY_RATE;
//        
//        _titleLabel      = [[UILabel alloc] init];
//        _titleLabel.font = [UIFont systemFontOfSize:16*KEY_RATE];
//
//        UILabel *authenLabel           = [[UILabel alloc] init];
//        authenLabel.text               = @"认证";
//        authenLabel.font               = [UIFont systemFontOfSize:11];
//        authenLabel.layer.borderWidth  = 0.5;
//        authenLabel.layer.borderColor  = RGB(255, 46, 80).CGColor;
//        authenLabel.textAlignment      = NSTextAlignmentCenter;
//        authenLabel.textColor          = RGB(255, 46, 80);
//        authenLabel.layer.cornerRadius = 5;
//        
//        _timeLabel               = [[UILabel alloc] init];
//        _timeLabel.textAlignment = NSTextAlignmentRight;
//        _timeLabel.font          = [UIFont systemFontOfSize:11*KEY_RATE];
//        _timeLabel.textColor     = RGB(136, 136, 136);
        
        _newsImage = [[UIImageView alloc] init];
        _newsImage.contentMode = UIViewContentModeScaleAspectFill;
        _newsImage.layer.masksToBounds = YES;
        
        _coverLabel = [[UILabel alloc] init];
        _coverLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _coverLabel.font = FontSize(16);
        _coverLabel.textColor = [UIColor whiteColor];
        
//        [self addSubview:_portraitImgv];
//        [self addSubview:_titleLabel];
//        [self addSubview:authenLabel];
//        [self addSubview:_timeLabel];
        [self addSubview:_newsImage];
        [_newsImage addSubview:_coverLabel];
        
        
//        [_portraitImgv mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(15 * KEY_RATE);
//            make.size.mas_equalTo(35 * KEY_RATE);
//            make.top.mas_equalTo(9.5 * KEY_RATE);
//        }];
//        
//        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_portraitImgv.mas_right).with.offset(10*KEY_RATE);
//            make.centerY.equalTo(_portraitImgv.mas_centerY);
//        }];
//
//        
//        [authenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(_titleLabel);
//            make.left.equalTo(_titleLabel.mas_right).with.offset(8*KEY_RATE);
//            make.width.equalTo(@(32 * KEY_RATE));
//            make.height.equalTo(@(18 * KEY_RATE));
//        }];
//        
//        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(@(-16*KEY_RATE));
//            make.centerY.equalTo(_titleLabel);
//            make.width.equalTo(@(100 * KEY_RATE));
//        }];

        [_newsImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.equalTo(self);
//            make.height.equalTo(@(200 * KEY_RATE));
        }];
        
        [_coverLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(_newsImage);
            make.height.mas_equalTo(34 * KEY_RATE);
        }];
        
    }
    return self;
}

//- (void)setBaseModel:(HomeBaseModel *)baseModel {
//    _baseModel       = baseModel;
//    _titleLabel.text = baseModel.village;
//    _timeLabel.text  = baseModel.datetime;
//    [_portraitImgv setImageWithURL:[NSURL URLWithString:baseModel.image] placeholder:[UIImage imageNamed:@"partraitFail"]];
//    
//}

- (void)setMainModel:(HomeMainModel *)mainModel {
    _mainModel = mainModel;
    [_newsImage setImageWithURL:[NSURL URLWithString:mainModel.cover] placeholder:[UIImage imageNamed:@"MainPlaceholder.jpg"]];
    _coverLabel.text = [NSString stringWithFormat:@"    %@",mainModel.title];
}

@end
