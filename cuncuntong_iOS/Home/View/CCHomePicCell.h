//
//  CCHomePicCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/4/15.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCHomeModel.h"
@interface CCHomePicCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *source;
@property (nonatomic, strong) CCHomeModel *model;
@end
