//
//  ListView.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/18.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  ListDelegate <NSObject>

- (void)itemClick:(UIButton *)sender;

@end
@interface ListView : UIView

@property (nonatomic, weak) id<ListDelegate> delegate;
@property (nonatomic, assign) BOOL isWine;
- (void)initColStatus:(BOOL)isCol;
- (void)changeColStatus:(BOOL)isCol;

@end
