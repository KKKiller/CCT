//
//  CCShiXiVillageListView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShiXiVillageListView.h"
@interface CCShiXiVillageListView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@end
@implementation CCShiXiVillageListView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataArray = [NSArray array];
        self.tableView = [[UITableView alloc]initWithFrame:self.bounds style:UITableViewStylePlain];
        [self.tableView registerClass:[CCShiXIVillageCell class] forCellReuseIdentifier:@"shixicell"];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.rowHeight = 40;
        [self addSubview:self.tableView];
        self.backgroundColor = BACKGRAY;
    }
    return self;
}
- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCShiXIVillageCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"shixicell"];
    if (self.dataArray.count > indexPath.row) {
        cell.adminModel = self.dataArray[indexPath.row];
    }
    return cell;
}
@end





@implementation CCShiXIVillageCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUI];
        [self layoutSubView];
    }
    return self;
}
- (void)setUI {
    [self addSubview:self.nameLbl];
    [self addSubview:self.slider];
    [self addSubview:self.progressLbl];
    [self addSubview:self.timeImgView];
    [self addSubview:self.remainDayLbl];
    [self addSubview:self.lineLbl];
}
- (void)setAdminModel:(CCHomeAdminModel *)adminModel {
    _adminModel = adminModel;
    if (adminModel.rate > 1) {
        adminModel.rate = adminModel.rate / 100.0;
    }
    self.nameLbl.text = adminModel.name;
    self.slider.value = adminModel.rate;
    self.progressLbl.text = [NSString stringWithFormat:@"%.01f%%",adminModel.rate * 100];
    self.remainDayLbl.text = [NSString stringWithFormat:@"%.01f天",adminModel.remainTime / (60 * 60 * 24.0)];
}


- (void)layoutSubView {
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [self.timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(10);
        make.right.equalTo(self.mas_right).offset(-15);
        make.top.equalTo(self.mas_top).offset(10);
    }];
    [self.remainDayLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeImgView.mas_bottom).offset(1);
        make.centerX.equalTo(self.timeImgView.mas_centerX);
    }];
    [self.slider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(12);
        make.height.mas_equalTo(5);
        make.width.mas_equalTo(30);
        make.right.equalTo(self.timeImgView.mas_left).offset(-10);
    }];
    [self.progressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.slider.mas_left).offset(2);
        make.top.equalTo(self.slider.mas_bottom).offset(3);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
}
- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:TEXTBLACK6];
    }
    return _nameLbl;
}
- (UIImageView *)timeImgView {
    if (!_timeImgView) {
        _timeImgView = [[UIImageView alloc]init];
        _timeImgView.image = IMAGENAMED(@"shixi_time");
    }
    return _timeImgView;
}
- (UILabel *)remainDayLbl {
    if (!_remainDayLbl) {
        _remainDayLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:MTRGB(0xF5A623)];
    }
    return _remainDayLbl;
}

- (UISlider *)slider {
    if (!_slider) {
        _slider = [[UISlider alloc]init];
        _slider.minimumTrackTintColor = MTRGB(0xFF656D);
        _slider.maximumTrackTintColor = MTRGB(0xeadada);
        CGSize s=CGSizeMake(1, 1);
        UIGraphicsBeginImageContextWithOptions(s, 0, [UIScreen mainScreen].scale);
        UIRectFill(CGRectMake(0, 0, 1, 1));
        UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [_slider setThumbImage:img forState:UIControlStateNormal];
    }
    return _slider;
}
- (UILabel *)progressLbl {
    if (!_progressLbl) {
        _progressLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:MTRGB(0xFF656D)];
    }
    return _progressLbl;
}
- (UILabel *)lineLbl {
    if (!_lineLbl) {
        _lineLbl = [[UILabel alloc]initWithShallowLine];
    }
    return _lineLbl;
}
@end
