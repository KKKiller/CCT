//
//  HomeSubCell.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/29.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "HomeSubCell.h"

@interface HomeSubCell()

@property (nonatomic, strong) UIImageView *imgv;

@end

@implementation HomeSubCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _imgv = [[UIImageView alloc] init];
        _imgv.contentMode = UIViewContentModeScaleAspectFill;
        _imgv.layer.masksToBounds = YES;
        [self addSubview:_imgv];
        [_imgv mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.left.top.equalTo(@(15 * KEY_RATE));
            make.width.equalTo(@(80 * KEY_RATE));
            make.height.equalTo(@(60 * KEY_RATE));
        }];
        
        _titleLabel = [[YYLabel alloc] init];
        [self addSubview:_titleLabel];
        
        
        _titleLabel.numberOfLines = 2;
        _titleLabel.textColor = RGB(34, 34, 34);
        _titleLabel.font = [UIFont systemFontOfSize:18 * KEY_RATE];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_imgv.mas_right).with.offset(15 * KEY_RATE);
            make.right.equalTo(@(-20 * KEY_RATE));
            make.centerY.equalTo(_imgv.mas_centerY);
            make.height.mas_equalTo(78 * KEY_RATE);
        }];
        
        
    }
    return self;
}

-(NSAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpace; // 调整行间距
    NSRange range = NSMakeRange(0, [string length]);
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributedString;
}

- (void)setModel:(HomeSubModel *)model {
    _model = model;
    _titleLabel.attributedText = [self getAttributedStringWithString:model.title lineSpace:5];
    [_imgv setImageWithURL:[NSURL URLWithString:model.mini] placeholder:[UIImage imageNamed:@"SubPlaceholder.jpg"]];

}

@end
