//
//  StickyView.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/15.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "StickyView.h"
#import "CCVillageModel.h"
#import "CCHomeVillageModel.h"
#import "MTConstants.h"
#import "NSMutableAttributedString+Style.h"
@interface StickyView()<StickyDelegate>

@property (nonatomic, strong) UIView *btnsContainerV;

@property (nonatomic, strong) VillageCellView *village1;
@property (nonatomic, strong) VillageCellView *village2;

@property (nonatomic, strong) VillageCellView *counrtyView;

@end

@implementation StickyView

- (instancetype)init {
    if (self = [super init]) {
        [self addSubview:self.btnsContainerV];
        [self creatBtns];
        [self addSubview:self.counrtyView];
        [self addSubview:self.bannerView];
        [self addSubview:self.village1];
        [self addSubview:self.village2];
        [self.village2.showDetailBtn addTarget:self action:@selector(showDetailBtn) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    [self layoutSubview];
    [self tapAtManager];
    self.backgroundColor = BACKGRAY;
    return self;
}


- (void)setVillageModel:(CCHomeVillageModel *)villageModel {
    _villageModel = villageModel;
    
    
    //县代理
    self.counrtyView.addressLbl.text = villageModel.country.val;
    
    NSString *tel  = nil;
    if (villageModel.cadminTel.length > 4) {
        tel = [villageModel.cadminTel stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    NSString *coutryText = ([TOOL stringEmpty:villageModel.cadmin] || !tel) ? @"县代理空缺，抢报中" :  [NSString stringWithFormat:@"县代理电话:%@",tel];
    if (![coutryText containsString:@"县代理电话"]) {
        self.counrtyView.applyManagerLbl.text = coutryText;
    }else{
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithText:coutryText color:MAINBLUE subStrIndex:NSMakeRange(0, 6) subStrColor:TEXTBLACK6];
        self.counrtyView.applyManagerLbl.attributedText = attrStr;
    }
    self.counrtyView.applyManagerLbl.textAlignment = NSTextAlignmentLeft;
    [self.counrtyView.applyManagerLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.counrtyView.mas_centerY);
        make.left.equalTo(self.counrtyView.mas_centerX).offset(10);
        make.width.mas_equalTo(180);
    }];
    
    //乡镇社长
    self.village1.addressLbl.text = villageModel.town.val;
    if(![TOOL stringEmpty:villageModel.tadmin]){
       NSString *townText    = [NSString stringWithFormat:@"社长:%@",villageModel.tadmin];
        if (![townText containsString:@"社长"]) {
            self.village1.applyManagerLbl.text = townText;
        }else{
            NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithText:townText color:MAINBLUE subStrIndex:NSMakeRange(0, 3) subStrColor:TEXTBLACK6];
            self.village1.applyManagerLbl.attributedText = attrStr;
        }
    }else { //没有乡社长就隐藏社长名,村长左移
        self.village1.applyManagerLbl.hidden = YES;
        [self.village2 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.top.equalTo(self.counrtyView.mas_bottom).offset(0);//30
            make.height.mas_equalTo(30);
            make.left.equalTo(self.village1.mas_right).offset(-40);
        }];
        [self.village2.applyManagerLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.village2.mas_centerY);
            make.right.equalTo(self.village2.mas_right).offset(-15);
            make.width.mas_equalTo(150);
        }];
    }
    
    
    //村长
    CCVillageModel *village = [villageModel.village firstObject];
    self.village2.addressLbl.text = village.val;
    NSString *villageText  = [TOOL stringEmpty:villageModel.vadmin] ? @"选拔中" :  [NSString stringWithFormat:@"庄主:%@",villageModel.vadmin];
    if (![villageText containsString:@"庄主"]) {
        self.village2.applyManagerLbl.text = villageText;
    }else{
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithText:villageText color:MAINBLUE subStrIndex:NSMakeRange(0, 3) subStrColor:TEXTBLACK6];
        self.village2.applyManagerLbl.attributedText = attrStr;
    }
    
    //    if([TOOL stringEmpty:villageModel.tadmin]){//没有社长,只显示村长
//        self.village2.lineLbl.hidden = NO;
//        [self.village2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(self);
//            make.top.equalTo(self.counrtyView.mas_bottom).offset(0);//30
//            make.height.mas_equalTo(30);
//        }];
//        [self.village2.applyManagerLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(self.village2.mas_centerY);
//            make.right.equalTo(self.village2.mas_right).offset(-15);
//            make.width.mas_equalTo(150);
//        }];
//    }
//    if (!villageModel.vadmin || [villageModel.vadmin isEqualToString:@""]) {
//        [self.village1 mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(self);
//            make.top.equalTo(self.counrtyView.mas_bottom).offset(0);
//            make.height.mas_equalTo(30);
//        }];
//        [self.village2.applyManagerLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(self.village2.mas_centerY);
//            make.right.equalTo(self.village2.mas_right).offset(-15);
//            make.width.mas_equalTo(150);
//        }];
//        self.village2.hidden = YES;
//    }
//    if (villageModel.village.count > 0) {
//        self.village2.addressLbl.text = [villageModel.village[0] val];
//    }
//    self.village2.locationImgV2.hidden = villageModel.village.count < 2;
//    self.village2.addressLbl2.hidden = villageModel.village.count < 2;
//    if (villageModel.village.count > 1) {
//        self.village2.addressLbl2.text = [villageModel.village[1] val];
//    }
//    self.village2.applyManagerLbl.text = [TOOL stringEmpty:villageModel.vadmin] ? @"银饭碗疯抢中" :  [NSString stringWithFormat:@"村长:%@",villageModel.vadmin];
//
//    if (villageModel.rate > 1) {
//        villageModel.rate = villageModel.rate / 100.0;
//    }
//    BOOL hiddenProgress = villageModel.rate == 0;
//    if (!hiddenProgress) {
//        self.village2.slider.value = villageModel.rate;
//        self.village2.progressLbl.text = [NSString stringWithFormat:@"%.01f%%",villageModel.rate * 100];
//        self.village2.remainDayLbl.text = [NSString stringWithFormat:@"%.01f天",villageModel.remainTime / (60 * 60 * 24.0)];
//        [self.village2.applyManagerLbl mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self.mas_right).offset(-70);
//        }];
//        self.village2.applyManagerImgView.image = IMAGENAMED(@"shixi_home_manager");
//    }
//    self.village2.progressLbl.hidden = hiddenProgress;
//    self.village2.slider.hidden = hiddenProgress;
//    self.village2.remainDayLbl.hidden = hiddenProgress;
//    self.village2.timeImgView.hidden = hiddenProgress;

    
    
    
    
}


- (void)homeBtnClick:(UIButton *)sender {
    [self.delegate clickAtHomeBtn:sender.tag - 100];
}
- (void)showDetailBtn {
    [self.delegate showApplyList];
}
- (void)tapAtManager {
    //社长
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        if (![TOOL stringEmpty:self.villageModel.tadminTel] && self.villageModel.tadminTel.length == 11) {
            [self makePhoneCall:self.villageModel.tadminTel name:self.villageModel.tadmin];
        }else{
//            if ([self.delegate respondsToSelector:@selector(replyManagerBtnClick:)]) {
//                [self.delegate replyManagerBtnClick:YES];
//            }
            if ([self.delegate respondsToSelector:@selector(pushToAgentWithId:)]) {
                [self.delegate pushToAgentWithId:nil];
            }
        }
    }];
    [self.village1.applyManagerLbl addGestureRecognizer:tap];
    
    //村长
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        if (![TOOL stringEmpty:self.villageModel.vadminTel] && self.villageModel.vadminTel.length == 11) {
            [self makePhoneCall:self.villageModel.vadminTel name:self.villageModel.vadmin];
        }else{
//            if ([self.delegate respondsToSelector:@selector(replyManagerBtnClick:)]) {
//                [self.delegate replyManagerBtnClick:NO];
//            }
            if ([self.delegate respondsToSelector:@selector(pushToAgentWithId:)]) {
                [self.delegate pushToAgentWithId:nil];
            }
        }
    }];
    [self.village2.applyManagerLbl addGestureRecognizer:tap1];
    
    //县代理
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        if (![TOOL stringEmpty:self.villageModel.cadminTel] && self.villageModel.cadminTel.length == 11) {
            [self makePhoneCall:self.villageModel.cadminTel name:self.villageModel.cadmin];
        }else{
            if ([self.delegate respondsToSelector:@selector(pushToAgentWithId:)]) {
                [self.delegate pushToAgentWithId:nil];
            }
//            if ([self.delegate respondsToSelector:@selector(replyManagerBtnClick:)]) {
//                [self.delegate replyManagerBtnClick:NO];
//            }
        }
    }];
    [self.counrtyView.applyManagerLbl addGestureRecognizer:tap2];
}
- (void)makePhoneCall:(NSString *)phone name:(NSString *)name{
    NSString *title =  [NSString stringWithFormat:@"呼叫%@",name];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:phone preferredStyle:1];
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [App_Delegate.tabbarVc presentViewController:alertController animated:YES completion:nil];
}

- (void)layoutSubview {

    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.btnsContainerV.mas_bottom);
        make.height.mas_equalTo(80);
    }];
    CGFloat height = 30;
    [self.counrtyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.equalTo(self);
        make.height.mas_equalTo(height);
        make.top.equalTo(self.bannerView.mas_bottom).offset(2);
    }];
    [self.village1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self.counrtyView.mas_bottom).offset(0);
        make.height.mas_equalTo(height);
        make.right.mas_equalTo(self.mas_centerX);
    }];
    [self.village2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.top.equalTo(self.village1.mas_top).offset(0);//30
        make.height.mas_equalTo(height);
        make.left.equalTo(self.mas_centerX);
    }];
    [self.counrtyView.applyManagerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.counrtyView.mas_centerY);
        make.right.equalTo(self.counrtyView.mas_right).offset(-25);
        make.width.mas_equalTo(120);
    }];
    [self.village1.applyManagerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.village1.mas_centerY);
        make.right.equalTo(self.village1.mas_right).offset(-25);
        make.width.mas_equalTo(80);
    }];
    
    [self.village2.applyManagerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.village2.mas_centerY);
        make.right.equalTo(self.village2.mas_right).offset(-25);
        make.width.mas_equalTo(80);
    }];
    
}
- (void)creatBtns {
    CGFloat width = 40;
    CGFloat margin = (App_Width - 30 - width * 6) / 5.0;
    NSArray *titleArray = @[@"女儿国",@"乐淘淘",@"种金豆",@"酒老大",@"货运通",@"红包"];
    for (int i = 0; i<titleArray.count; i++) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(15 +i*(width + margin), 5, width, width)];
        btn.tag = 100+i;
        NSString *imgName = [NSString stringWithFormat:@"homeBtn_%@",@(i)];
        [btn setImage: IMAGENAMED(imgName) forState:UIControlStateNormal];
        [self.btnsContainerV addSubview:btn];
        [btn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLbl = [[UILabel alloc]initWithText:titleArray[i] font:13 textColor:TEXTBLACK3];
        titleLbl.textAlignment = NSTextAlignmentCenter;
        [self.btnsContainerV addSubview:titleLbl];
        [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(btn.mas_bottom).offset(5);
            make.centerX.equalTo(btn.mas_centerX);
        }];
        
    }
}
#pragma mark - 懒加载
- (UIView *)btnsContainerV {
    if (!_btnsContainerV) {
        _btnsContainerV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 75)];
        _btnsContainerV.backgroundColor = WHITECOLOR;
    }
    return _btnsContainerV;
}
- (VillageCellView *)village1 {
    if (_village1 == nil) {
        _village1 = [[VillageCellView alloc]init];
        _village1.delegate = self;
    }
    return _village1;
}
- (VillageCellView *)village2 {
    if (_village2 == nil) {
        _village2 = [[VillageCellView alloc]init];
        _village2.delegate = self;
        

    }
    return _village2;
}
- (VillageCellView *)counrtyView {
    if (!_counrtyView) {
        _counrtyView = [[VillageCellView alloc]init];
//        _counrtyView.backgroundColor = [UIColor redColor];
        _counrtyView.delegate = self;
    }
    return _counrtyView;
}
- (UIImageView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[UIImageView alloc]init];
        _bannerView.contentMode = UIViewContentModeScaleToFill;
        _bannerView.image = IMAGENAMED(@"jindoujiBanner");
        _bannerView.userInteractionEnabled = YES;
    }
    return _bannerView;
}

@end



@interface VillageCellView ()

@end
@implementation VillageCellView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatViews];
        [self layoutSubView];
    }
    self.backgroundColor = WHITECOLOR;
    return self;
}

- (void)creatViews {
    [self addSubview:self.applyManagerLbl];
    [self addSubview:self.applyManagerImgView];
    [self addSubview:self.locationImgV];
    [self addSubview:self.addressLbl];
    [self addSubview:self.lineLbl];
    [self addSubview:self.addressLbl2];
    [self addSubview:self.locationImgV2];
    [self addSubview:self.slider];
    [self addSubview:self.progressLbl];
    [self addSubview:self.timeImgView];
    [self addSubview:self.remainDayLbl];
    [self addSubview:self.showDetailBtn];
}

- (void)layoutSubView {
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
//    [self.locationImgV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left).offset(15);
//        make.top.equalTo(self.mas_top).offset(5);
//        make.width.mas_equalTo(16);
//        make.height.mas_equalTo(16 * (50.0/42.0));
//    }];
    [self.addressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(10);
    }];
    [self.locationImgV2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.addressLbl.mas_right).offset(10);
        make.top.equalTo(self.mas_top).offset(5);
        make.width.mas_equalTo(16);
         make.height.mas_equalTo(16 * (50.0/42.0));
    }];
    [self.addressLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.locationImgV2.mas_right).offset(3);
    }];
//    [self.applyManagerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.left.equalTo(self.applyManagerImgView.mas_right).offset(5);
//        make.centerY.equalTo(self.mas_centerY);
//        make.right.equalTo(self.mas_right).offset(-5);
//        make.width.mas_equalTo(80);
//    }];
    [self.showDetailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(self.applyManagerLbl);
        make.width.mas_equalTo(25);
    }];
//    [self.applyManagerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.applyManagerLbl.mas_centerY);
//        make.width.mas_equalTo(13);
//        make.height.mas_equalTo(13 * (44.0/37.0));
//        make.right.equalTo(self.applyManagerLbl.mas_left).offset(-5);
//    }];
    [self.timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top).offset(6);
    }];
    [self.remainDayLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeImgView.mas_bottom).offset(1);
        make.centerX.equalTo(self.timeImgView.mas_centerX);
    }];
    [self.slider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(7);
        make.height.mas_equalTo(5);
        make.width.mas_equalTo(30);
        make.right.equalTo(self.timeImgView.mas_left).offset(-10);
    }];
    [self.progressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.slider.mas_left).offset(2);
        make.top.equalTo(self.slider.mas_bottom).offset(3);
    }];
}

#pragma mark - 懒加载
- (UILabel *)lineLbl {
    if (_lineLbl == nil) {
        _lineLbl = [[UILabel alloc]initWithShallowLine];
    }
    return _lineLbl;
}
- (UIImageView *)locationImgV {
    if (_locationImgV == nil) {
        _locationImgV = [[UIImageView alloc]init];
        _locationImgV.image = IMAGENAMED(@"home_location");
    }
    return _locationImgV;
}
- (UILabel *)addressLbl {
    if (_addressLbl == nil) {
        _addressLbl = [[UILabel alloc]initWithText:@"" font:11 textColor:TEXTBLACK6];
    }
    return _addressLbl;
}
- (UIImageView *)locationImgV2 {
    if (_locationImgV2 == nil) {
        _locationImgV2 = [[UIImageView alloc]init];
        _locationImgV2.image = IMAGENAMED(@"home_location");
        _locationImgV2.hidden = YES;
    }
    return _locationImgV2;
}
- (UILabel *)addressLbl2 {
    if (_addressLbl2 == nil) {
        _addressLbl2 = [[UILabel alloc]initWithText:@"" font:11 textColor:TEXTBLACK6];
        _addressLbl2.hidden = YES;
    }
    return _addressLbl2;
}
- (UILabel *)applyManagerLbl {
    if (_applyManagerLbl == nil) {
        _applyManagerLbl = [[UILabel alloc]initWithText:@"银饭碗疯抢中" font:10 textColor:TEXTBLACK6];
        _applyManagerLbl.userInteractionEnabled = YES;
        _applyManagerLbl.numberOfLines = 0;
        _applyManagerLbl.textColor = MAINBLUE;
        _applyManagerLbl.textAlignment =  NSTextAlignmentRight;
    }
    return _applyManagerLbl;
}
- (UIButton *)showDetailBtn {
    if (!_showDetailBtn) {
        _showDetailBtn = [[UIButton alloc]initWithTitle:@"查看" textColor:MAINCOLOR backImg:nil font:11];
        _showDetailBtn.hidden = YES;;
    }
    return _showDetailBtn;
}
- (UIImageView *)applyManagerImgView {
    if (_applyManagerImgView == nil) {
        _applyManagerImgView = [[UIImageView alloc]init];
        _applyManagerImgView.image = IMAGENAMED(@"home_manager");
        _applyManagerImgView.contentMode = UIViewContentModeScaleToFill;
    }
    return _applyManagerImgView;
}
- (UIImageView *)timeImgView {
    if (!_timeImgView) {
        _timeImgView = [[UIImageView alloc]init];
        _timeImgView.image = IMAGENAMED(@"shixi_time");
        _timeImgView.hidden = YES;
    }
    return _timeImgView;
}
- (UILabel *)remainDayLbl {
    if (!_remainDayLbl) {
        _remainDayLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:MTRGB(0xF5A623)];
        _remainDayLbl.hidden= YES;
    }
    return _remainDayLbl;
}
- (UISlider *)slider {
    if (!_slider) {
        _slider = [[UISlider alloc]init];
        _slider.minimumTrackTintColor = MTRGB(0xFF656D);
        _slider.maximumTrackTintColor = MTRGB(0xeadada);
        CGSize s=CGSizeMake(1, 1);
        UIGraphicsBeginImageContextWithOptions(s, 0, [UIScreen mainScreen].scale);
        UIRectFill(CGRectMake(0, 0, 1, 1));
        UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [_slider setThumbImage:img forState:UIControlStateNormal];
        _slider.hidden = YES;
    }
    return _slider;
}
- (UILabel *)progressLbl {
    if (!_progressLbl) {
        _progressLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:MTRGB(0xFF656D)];
        _progressLbl.hidden = YES;
    }
    return _progressLbl;
}

@end

@implementation CountryCellView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatViews];
        [self layoutSubView];
    }
    self.backgroundColor = WHITECOLOR;
    return self;
}

- (void)creatViews {
    [self addSubview:self.applyManagerLbl];
    [self addSubview:self.applyManagerImgView];
//    [self addSubview:self.finishedCountLbl];
//    [self addSubview:self.totlaCountLbl];
    [self addSubview:self.slider];
    [self addSubview:self.progressLbl];
    [self addSubview:self.timeImgView];
    [self addSubview:self.remainDayLbl];
    [self addSubview:self.lineLbl];
}

- (void)layoutSubView {
    
    [self.applyManagerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.applyManagerImgView.mas_right).offset(5);
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-25);
    }];
    [self.applyManagerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.applyManagerLbl.mas_centerY);
        make.width.mas_equalTo(13);
        make.height.mas_equalTo(13 * (44.0/37.0));
        make.right.equalTo(self.applyManagerLbl.mas_left).offset(-5);
    }];
    [self.timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top).offset(6);
    }];
    [self.remainDayLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeImgView.mas_bottom).offset(1);
        make.centerX.equalTo(self.timeImgView.mas_centerX);
    }];
//    [self.totlaCountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.remainDayLbl.mas_left).offset(-2);
//        make.centerY.equalTo(self.mas_centerY);
//    }];
//    [self.finishedCountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.totlaCountLbl.mas_left).offset(-2);
//        make.centerY.equalTo(self.mas_centerY);
//    }];
    [self.slider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(7);
        make.height.mas_equalTo(5);
        make.width.mas_equalTo(30);
        make.right.equalTo(self.timeImgView.mas_left).offset(-10);
    }];
    [self.progressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.slider.mas_left).offset(2);
        make.top.equalTo(self.slider.mas_bottom).offset(3);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
}


- (UILabel *)applyManagerLbl {
    if (_applyManagerLbl == nil) {
        _applyManagerLbl = [[UILabel alloc]initWithText:@"乡盟主" font:11 textColor:TEXTBLACK6];
        _applyManagerLbl.userInteractionEnabled = YES;
        _applyManagerLbl.numberOfLines = 1;
    }
    return _applyManagerLbl;
}
- (UIImageView *)applyManagerImgView {
    if (_applyManagerImgView == nil) {
        _applyManagerImgView = [[UIImageView alloc]init];
        _applyManagerImgView.image = IMAGENAMED(@"mz_shixi");
        _applyManagerImgView.contentMode = UIViewContentModeScaleToFill;
    }
    return _applyManagerImgView;
}
- (UIImageView *)timeImgView {
    if (!_timeImgView) {
        _timeImgView = [[UIImageView alloc]init];
        _timeImgView.image = IMAGENAMED(@"shixi_time");
        _timeImgView.hidden = YES;
    }
    return _timeImgView;
}
- (UILabel *)remainDayLbl {
    if (!_remainDayLbl) {
        _remainDayLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:MTRGB(0xF5A623)];
        _remainDayLbl.hidden= YES;
    }
    return _remainDayLbl;
}
//- (UILabel *)finishedCountLbl {
//    if (!_finishedCountLbl) {
//        _finishedCountLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:MTRGB(0xFF656D)];
//    }
//    return _finishedCountLbl;
//}
//- (UILabel *)totlaCountLbl {
//    if (!_totlaCountLbl) {
//        _totlaCountLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:TEXTBLACK9];
//    }
//    return _totlaCountLbl;
//}
- (UISlider *)slider {
    if (!_slider) {
        _slider = [[UISlider alloc]init];
        _slider.minimumTrackTintColor = MTRGB(0xFF656D);
        _slider.maximumTrackTintColor = MTRGB(0xeadada);
        CGSize s=CGSizeMake(1, 1);
        UIGraphicsBeginImageContextWithOptions(s, 0, [UIScreen mainScreen].scale);
        UIRectFill(CGRectMake(0, 0, 1, 1));
        UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [_slider setThumbImage:img forState:UIControlStateNormal];
        _slider.hidden = YES;
    }
    return _slider;
}
- (UILabel *)progressLbl {
    if (!_progressLbl) {
        _progressLbl = [[UILabel alloc]initWithText:@"" font:8 textColor:MTRGB(0xFF656D)];
        _progressLbl.hidden = YES;
    }
    return _progressLbl;
}
- (UILabel *)lineLbl {
    if (!_lineLbl) {
        _lineLbl = [[UILabel alloc]initWithShallowLine];
    }
    return _lineLbl;
}
@end
