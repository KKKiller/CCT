//
//  HomeMainCell.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/29.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeBaseModel.h"
#import "HomeMainModel.h"

@interface HomeMainCell : UITableViewCell

//@property (nonatomic, strong) HomeBaseModel *baseModel;
@property (nonatomic, strong) HomeMainModel *mainModel;
//@property (nonatomic, strong) UIImageView *portraitImgv;
//@property (nonatomic, strong) UILabel *titleLabel;
//@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *newsImage;
@property (nonatomic, strong) UILabel *coverLabel;

@end
