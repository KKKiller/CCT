//
//  CCScanController.m
//  TestDemo
//
//  Created by 周吾昆 on 2017/9/9.
//  Copyright © 2017年 周吾昆. All rights reserved.
//

#import "CCScanController.h"
#import "CCWinePayController.h"
#import "CCWineModel.h"
@interface CCScanController ()

@end

@implementation CCScanController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"扫描二维码";
    self.cameraInvokeMsg = @"相机启动中";
}

#pragma mark -实现类继承该方法，作出对应处理

- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    if (!array ||  array.count < 1)
    {
        SHOW(@"二维码识别失败");
        return;
    }
    
    LBXScanResult *scanResult = array[0];
    NSString *strResult = scanResult.strScanned;
    self.scanImage = scanResult.imgScanned;
    
    if (!strResult) {
        
        SHOW(@"二维码识别失败");
        return;
    }
    
    NSLog(@"%@",strResult);
    NSString *pid = [TOOL param:@"pid" htmlStr:strResult];
    if (!pid) {
        SHOW(@"二维码识别失败");
    }else{
        [self getData:pid];
    }
}
- (void)getData:(NSString *)productId {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/wine_detail") params:@{@"id":STR(productId)} target:self success:^(NSDictionary *success) {
        if ([success[@"code"]  isEqual: @(1)]) {
            CCWineModel *model = [CCWineModel modelWithJSON:success[@"msg"]];
            CCWinePayController *vc = [[CCWinePayController alloc]init];
            vc.price = model.final_price;
            vc.wineId = model.id;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            SHOW(@"二维码有误");
        }
    } failure:^(NSError *failure) {
        
    }];
}
@end
