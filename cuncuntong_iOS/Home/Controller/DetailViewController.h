//
//  DetailViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/29.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^articleBlock)(BOOL isCol);

@interface DetailViewController : BaseViewController

@property (nonatomic, copy) NSString *village;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *aid;
@property (nonatomic, copy) NSString *iscol;
@property (nonatomic, copy) articleBlock block;
@property (nonatomic, copy) NSString *mini;
@property (nonatomic, strong) NSString *share_title;

@property (nonatomic, strong) NSString *articleId;
@end
