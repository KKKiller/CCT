//
//  DetailViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/8/29.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "DetailViewController.h"
#import <WebKit/WebKit.h>
#import "ListView.h"
#import "ComplaintsOneController.h"
#import "RewardController.h"

@interface DetailViewController ()<ListDelegate, WKNavigationDelegate> {
    WKWebView *_webView;
    UIProgressView *_progressView;
    BOOL _isCollect;
    WKWebViewConfiguration *_configuration;
}
@property (nonatomic, strong) ListView *listView;

/**
 分享 链接
 */
@property(nonatomic,strong)NSString *share_url;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    _isCollect = [_iscol boolValue];
    
    
    
    [self naviConfig];
    self.navigationController.navigationBarHidden = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect frame = [UIScreen mainScreen].bounds;
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, KEY_WIDTH, 0)];
    _progressView.progressTintColor = [UIColor greenColor];
    _progressView.transform = CGAffineTransformMakeScale(1.0f,2.0f);
    [self.view addSubview:_progressView];
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(-10, 0, App_Width + 20, App_Height)];
    _webView.allowsBackForwardNavigationGestures = YES;
    _webView.navigationDelegate = self;
    _webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, -40);
//    NSString *url = _url;
//    url = [url stringByAppendingString:USERID];
    self.aid = self.articleId;
    
//    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [self.view insertSubview:_webView belowSubview:_progressView];

    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
    [self loadArticleData];
}

//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
////    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"icon_back_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
////    self.navigationItem.leftBarButtonItem = backItem;
//}



- (void)loadArticleData {
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"collect/article") params:@{@"rid":USERID,@"id":STR(self.articleId)} success:^(NSDictionary *success) {
        NSLog(@"%@",success);
        if([success[@"code"] integerValue] == 1){
            self.url = success[@"msg"][@"url"];
            self.share_title = success[@"msg"][@"title"];
            self.mini = success[@"msg"][@"mini"];
            self.village = success[@"msg"][@"village"];
            
            NSString *content = success[@"msg"][@"content"];
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
            self.navigationItem.title = self.share_title;

            [self loadShareUrl];

        }
    } failure:^(NSError *failure) {
        NSLog(@"%@",failure);
    }];
}



- (void)loadShareUrl {
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"misc/shareurl") params:nil success:^(NSDictionary *success) {
        NSLog(@"%@",success);
        NSString *baseUrl = success[@"data"][@"urls"][@"domain"];
        NSString *pid = [TOOL param:@"p" htmlStr:self.url];
        self.share_url =  [NSString stringWithFormat:@"%@wxman/url.php?id=%@&rid=%@",baseUrl,pid,USERID];
        NSLog(@"%@",self.share_url);
    } failure:^(NSError *failure) {
        NSLog(@"%@",failure);
    }];
  
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    NSString *url = [navigationAction.request.URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if ([url isEqualToString:@"http://139.196.179.91/village/public/ds"] || [url isEqualToString:@"http://139.196.179.91/village/public/ts"]) {
        if ([url isEqualToString:@"http://139.196.179.91/village/public/ds"]) {
            RewardController *vc = [[RewardController alloc] init];
            vc.aid = _aid;
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            ComplaintsOneController *vc = [[ComplaintsOneController alloc] init];
            vc.aid = _aid;
            [self.navigationController pushViewController:vc animated:YES];
        }
        decisionHandler(WKNavigationActionPolicyCancel);
    }else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

- (void)naviConfig {
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"icon_back_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = backItem;
    
    UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE)];
    [detailButton setImage:[UIImage imageNamed:@"icon_xialacaidan_white"] forState:UIControlStateNormal];
    
    UIBarButtonItem * detailButtonItem = [[UIBarButtonItem alloc] initWithCustomView:detailButton];
    self.navigationItem.rightBarButtonItem = detailButtonItem;
    detailButton.titleLabel.font = FontSize(12);
    
    [detailButton addTarget:self action:@selector(listMenu) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Target-Action

- (void)back {
    
    if(self.block){
        _block(_isCollect);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)listMenu {
    self.listView.hidden = !self.listView.hidden;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        _progressView.hidden = _webView.estimatedProgress == 1.0;
        [_progressView setProgress:(float)_webView.estimatedProgress animated:YES];
    }
}

- (ListView *)listView {
    
    if (!_listView) {
        _listView = [[ListView alloc] init];
        [_listView initColStatus:_isCollect];
        [self.view addSubview:_listView];
        [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - WCFNavigationHeight);
        }];
        _listView.hidden = YES;
        _listView.delegate = self;
        [_listView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                         initWithActionBlock:^(id  _Nonnull sender) {
            _listView.hidden = YES;
        }]];

    }
    return _listView;
}

- (void)dealloc {
    
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

#pragma mark - ListDelegate
- (void)itemClick:(UIButton *)sender {
    switch (sender.tag - 3000) {
        case 0: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_WechatSession];
        }
            break;
        case 1: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine];
        }
            break;
        case 2: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_QQ];
        }
            break;
        case 3: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_Qzone];
        }
            break;
        case 4: {
            [self collection];
        }
            break;
        case 5: {
            [self complain];
        }
            break;
        default:
            break;
    }
}

#pragma mark - 下拉菜单方法


- (void)collection {
    [self.listView changeColStatus:_isCollect];
    if (_isCollect) {
        
        //col_type :0 文章 1:众筹
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/dela")
                                        params:@{@"id": USERID, @"aid": _aid,@"col_type":@"0"}
                                        target:self
                                       success:^(NSDictionary *success) {
                                           [MBProgressHUD showMessage:@"取消收藏"];
                                       } failure:^(NSError *failure) {
                                           
                                       }];
    }else {
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"article/collect")
                                        params:@{@"id": USERID, @"aid": _aid,@"col_type":@"0"}
                                        target:self
                                       success:^(NSDictionary *success) {
                                           [MBProgressHUD showMessage:@"收藏成功"];
                                           
                                       } failure:^(NSError *failure) {
                                           
                                       }];

    }
    _isCollect = !_isCollect;
}

- (void)complain {
    self.listView.hidden = YES;
    ComplaintsOneController *vc = [[ComplaintsOneController alloc] init];
    vc.aid = _aid;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    self.listView.hidden = YES;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_mini]]];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:self.share_title ?: @"全球村村通"  descr:@"——来自全球村村通APP" thumImage:image];
    //设置网页地址
    shareObject.webpageUrl = self.share_url;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            [self shareAward];
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}
- (void)shareAward {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"bean/article_trans") params:@{@"rid":USERID,@"aid":STR(self.aid)} success:^(NSDictionary *success) {
    } failure:^(NSError *failure) {
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setters
-(void)setShare_url:(NSString *)share_url{
    _share_url = share_url;
}





@end


