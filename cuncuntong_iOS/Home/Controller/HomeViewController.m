//
//  HomeViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/23/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "HomeViewController.h"
#import "NewsViewController.h"
#import "DetailViewController.h"
#import "HomeMainCell.h"
#import "HomeSubCell.h"
#import "StickyView.h"
#import "HomeBaseModel.h"
#import "HomeMainModel.h"
#import "RedViewController.h"
#import "CCApplyManagerController.h"
#import "CCVillageModel.h"
#import "CCHomeVillageModel.h"
#import "SetHomeViewController.h"
#import "CCShareTabbarController.h"
#import "CCShareCommentController.h"
#import "CCShareIssueController.h"
#import "CCBuyTabbarController.h"
#import "CrowdfundingViewController.h"
#import "CCOpenRedView.h"
#import "CCPostRedController.h"
#import "CCHomeAdminModel.h"
#import "CCRedLIstController.h"
#import "CCScanController.h"
#import "CCShiXiVillageListView.h"
#import "CCWineTabbarController.h"
#import "CCWinePayController.h"
#import "CrowdfundingInfoViewController.h"
#import "CCHomeModel.h"
#import "CCHomePicCell.h"
#import "CCHome3PicCell.h"
#import "CCHomeVideoCell.h"
#import "ZFPlayer.h"
#import "MYHomeController.h"
#import "CCEnergyController.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "ChatGroupSettingController.h"
#import "CCGongLueController.h"
#import "CCPriceController.h"
#import "CCShareHomeController.h"
#import "CCTransportTabbarController.h"
#import "TSVerifyHomeController.h"
#import "SetHomeViewController.h"
#import "AddVillageController.h"
#import "CCRedTabbarController.h"
#import "RedDetailController.h"
#import "ChatGroupSettingController.h"
#import "THChatController.h"
#import "ChatGroupMemberSettingController.h"
#import "CCChatAddMsgController.h"
#import "CCGirlCountryHomeController.h"
#import "LTTTabBarController.h"

static NSString *kCCHomePicCell = @"CCHomePicCell";
static NSString *kCCHome3PicCell = @"CCHome3PicCell";
static NSString *kCCHomeVideoCell = @"CCHomeVideoCell";

@interface HomeViewController () <UITableViewDelegate, UITableViewDataSource, StickyDelegate,UIAlertViewDelegate,ZFPlayerDelegate,UISearchBarDelegate,UIScrollViewDelegate>{
    
    UITableView *_tableView;
    NSInteger _page;
    StickyView *_stickyView;
    BOOL _isCol;
    UIButton *_messageItem;
    UIButton *_scanItem;

}

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIButton *redButton;
@property (nonatomic, strong) NSDictionary *stickyDic;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) NSArray *shixiVillArray;
@property (nonatomic, strong) ZFPlayerView *playerView;
@property (nonatomic, assign) NSIndexPath *playingIndexPath;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIButton *searchBtn;
@property (nonatomic, strong) UIButton *baiduBtn;
@property (nonatomic, strong) NSString *topTitleId;
@property (nonatomic, assign) BOOL setVillageed;
@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UIView *searchView;
@end
typedef void(^selectHomeBlock)(void);
@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self loadCacheData];
    [self loadData];
    [NOTICENTER addObserver:self selector:@selector(loadData) name:UIApplicationWillEnterForegroundNotification object:nil];
    
//    CCLiveController *vc = [[CCLiveController alloc]init];
//    ChatGroupSettingController *vc = [[ChatGroupSettingController alloc]init];
//    vc.groupId = @"e9942125-da14-4918-b507-a5312a97e751";
//    [self.navigationController pushViewController:vc animated:YES];

    
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBarHidden = YES;
    [App_Delegate.tabbarVc updateBadgeValueForTabBarItem];

}
- (void)viewDidAppear:(BOOL)animated {
    if (![[UserManager sharedInstance].actpoint isEqualToString:@"0"] || ![[UserManager sharedInstance].syspoint isEqualToString:@"0"]) {
        [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang1"] forState:UIControlStateNormal];
    }else {
        [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang0"] forState:UIControlStateNormal];
    }

}

#pragma mark - 懒加载
- (UIView *)navView{
    if (!_navView){
       _navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, WCFNavigationHeight)];
        _navView.backgroundColor = MTRGB(0x5191FF);
        [self.view addSubview:_navView];
    }
    return _navView;
}

- (UIView *)searchView{
    if (!_searchView){
        _searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, App_Width - 100, 32)];
        _searchView.backgroundColor = [WHITECOLOR colorWithAlphaComponent:0.1];
        _searchView.layer.cornerRadius = 16;
        _searchView.layer.masksToBounds = YES;
        _searchView.centerY = _scanItem.centerY;
        _searchView.centerX = self.navView.centerX;
    }
    return _searchView;
}

- (UISearchBar *)searchBar{
    if(!_searchBar){
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(-10, 0, App_Width - CGRectGetMinX(self.searchBtn.frame) + 10, 32)];
        _searchBar.placeholder = @"搜你所思 得你所想";
        _searchBar.delegate = self;
        //光标颜色
        _searchBar.tintColor = MAINBLUE;
        UIImage* searchBarBg = [self GetImageWithColor:CLEARCOLOR andHeight:32.0f];
        //设置背景图片
        [_searchBar setBackgroundImage:searchBarBg];
        //设置背景色
        [_searchBar setBackgroundColor:CLEARCOLOR];
        //设置文本框背景
        [_searchBar setSearchFieldBackgroundImage:searchBarBg forState:UIControlStateNormal];
        //拿到searchBar的输入框
        UITextField *searchTextField = [_searchBar valueForKey:@"_searchField"];
        //字体大小
        searchTextField.font = [UIFont systemFontOfSize:13];
        [searchTextField setValue:[[UIColor whiteColor] colorWithAlphaComponent:0.5] forKeyPath:@"_placeholderLabel.textColor"];
        UIImageView *leftV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        leftV.image = IMAGENAMED(@"icon_search_white");
        searchTextField.leftView = leftV;
    }
    return _searchBar;
}

- (void)setUI {

    _messageItem = [[UIButton alloc] initWithFrame:CGRectMake(App_Width - 44, StatusBarHeight + 3, 32, 32)];
//    _messageItem.layer.cornerRadius = 16;
//    _messageItem.layer.masksToBounds = YES;
//    _messageItem.backgroundColor = [WHITECOLOR colorWithAlphaComponent:0.3];
    [_messageItem addTarget:self action:@selector(messageDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    _scanItem = [[UIButton alloc] initWithFrame:CGRectMake(12, 0, 32, 32)];
 
    [_scanItem addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
    [_scanItem setImage:IMAGENAMED(@"icon_scan") forState:UIControlStateNormal];

    _scanItem.centerY = _messageItem.centerY;

    [self.navView addSubview:_messageItem];
    [self.navView addSubview:_scanItem];
    [self.navView addSubview:self.searchView];
    [self addSearchBtns];
    [self.searchView addSubview:self.searchBar];
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_top).offset(WCFNavigationHeight);
        make.bottom.equalTo(self.view.mas_bottom).offset(-WCFTabBarHeight);
    }];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor colorWithHexString:@"EEEEEE"];
    
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    _tableView.tableHeaderView = _stickyView;
    
    _stickyView = [[StickyView alloc] init];
    _stickyView.delegate = self;
    _stickyView.frame = CGRectMake(0, 0, App_Width, 310 *KEY_RATE);
    //banner点击
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
//        if (self.topTitleId) {
//            DetailViewController *vc = [[DetailViewController alloc] init];
//            vc.articleId = self.topTitleId;
//            [self.navigationController pushViewController:vc animated:YES];
//        }
        CCEnergyController *vc = [[CCEnergyController alloc]init];
        //    vc.userInfo = self.userInfoModel;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [_stickyView.bannerView addGestureRecognizer:tap];
    
    [_tableView registerNib:[UINib nibWithNibName:@"CCHomePicCell" bundle:nil] forCellReuseIdentifier:kCCHomePicCell];
    [_tableView registerNib:[UINib nibWithNibName:@"CCHome3PicCell" bundle:nil] forCellReuseIdentifier:kCCHome3PicCell];
    [_tableView registerNib:[UINib nibWithNibName:@"CCHomeVideoCell" bundle:nil] forCellReuseIdentifier:kCCHomeVideoCell];
    _tableView.estimatedRowHeight = 0;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    
//    [self setSearchBar];

}


/**
 *  生成图片
 *
 *  @param color  图片颜色
 *  @param height 图片高度
 *
 *  @return 生成的图片
 */
- (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height
{
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}
- (void)addSearchBtns {
    
    self.baiduBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.searchView.width - 40, 0, 40, 32)];
    [self.searchView addSubview:self.baiduBtn];
    [self.baiduBtn addTarget:self action:@selector(searchBaidu) forControlEvents:UIControlEventTouchUpInside];
    [self.baiduBtn setTitle:@"百度" forState:UIControlStateNormal];
    [self.baiduBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    self.baiduBtn.titleLabel.font = FFont(13);
    self.baiduBtn.backgroundColor = CLEARCOLOR;
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.baiduBtn.frame) - 1, 10, 1, 12)];
    lineView.backgroundColor = WHITECOLOR;
    [self.searchView addSubview:lineView];
    
    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMinX(lineView.frame) - 45, 0, 40, 32)];
    [self.searchView addSubview:self.searchBtn];
    [self.searchBtn addTarget:self action:@selector(searchLifeCircle) forControlEvents:UIControlEventTouchUpInside];
    [self.searchBtn setTitle:@"生活圈" forState:UIControlStateNormal];
    [self.searchBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = FFont(13);
    self.searchBtn.backgroundColor  = CLEARCOLOR;
}
- (void)searchLifeCircle {
    if (self.searchBar.text.length == 0) {
        SHOW(@"请输入搜索内容");
        return;
    }
    [self.view endEditing:YES];
    CCShareHomeController *vc = [[CCShareHomeController alloc]init];
    vc.keyword = self.searchBar.text;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)searchBaidu {
    if (self.searchBar.text.length == 0) {
        SHOW(@"请输入搜索内容");
        return;
    }
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    NSString *url = [NSString stringWithFormat:@"https://www.baidu.com/s?wd=%@",self.searchBar.text];
    vc.urlStr = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    vc.titleStr = @"百度搜索";
    [self.view endEditing:YES];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"SearchButton");
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSString *inputStr = searchText;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.searchBar endEditing:YES];
}
#pragma mark - 数据
- (void)loadCacheData {
    [self setData:[TOOL getCachaDataWithName:@"homeStickyData"]];
    [self setData:[TOOL getCachaDataWithName:@"homeList"]];
}
- (void)loadData {
    _page = 1;
    [self loadVillageData];
    NSDictionary *paramDic = @{ @"page": [NSString stringWithFormat:@"%ld", _page]};
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"collect/collect") params:paramDic target: nil success:^(NSDictionary *success) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [TOOL saveDataWithData:success Name:@"homeList"];
        });
        [self.dataArray removeAllObjects];
        [self setData:success];
        
        [_tableView.mj_header endRefreshing];
    } failure:^(NSError *failure) {
        [_tableView.mj_header endRefreshing];
    }];
    
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"article/stick") params:@{@"id":USERID} success:^(NSDictionary *success) {
        if ([success[@"code"]integerValue] == 0) {
            self.topTitleId = success[@"msg"][@"id"];
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (void)setData:(NSDictionary *)success {
    if (success[@"data"][@"sticky"]) { //顶部
        self.stickyDic = success[@"data"][@"sticky"];
    }
    if ([success[@"code"] integerValue] == 1) {
        for (NSDictionary *dic in success[@"msg"]) {
            CCHomeModel *model = [CCHomeModel modelWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [_tableView reloadData];
    }
}

- (void)loadMore {
    _page ++;
    NSDictionary *paramDic = @{@"page": [NSString stringWithFormat:@"%@", @(_page)]};
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"collect/collect") params:paramDic target: nil success:^(NSDictionary *success) {
        for (NSDictionary *dic in success[@"msg"]) {
            CCHomeModel *model = [CCHomeModel modelWithJSON:dic];
            [self.dataArray addObject:model];
        }
        
        [_tableView reloadData];
        [_tableView.mj_footer endRefreshing];
    } failure:^(NSError *failure) {
        _page--;
        [_tableView.mj_footer endRefreshing];
    }];
}
//我关注的村
- (void)loadVillageData{
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/village") params:@{@"id":USERID} target:nil success:^(NSDictionary *success) {
        if (!self.stickyDic || ![self.stickyDic[@"id"] isEqualToString:@"-1"]) {
            //缓存
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [TOOL saveDataWithData:success Name:@"homeStickyData"];
            });
            CCHomeVillageModel *model = [CCHomeVillageModel modelWithJSON:success[@"data"]];
            App_Delegate.villageModel = model;
            _stickyView.villageModel = model;
            CGFloat rowH = 30;
//            CGFloat towmH = [TOOL stringEmpty:model.town.val] ? 0 : rowH;
            CGFloat height = 130 + rowH  * 2 + rowH ;
            _stickyView.height = height *KEY_RATE;
            _tableView.tableHeaderView = _stickyView;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if ([UserManager sharedInstance].userid && [UserManager sharedInstance].locs.count == 0) {
                    [self setHomeAddress:nil];
                }
            });
        }
    } failure:^(NSError *failure) {
    }];
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCHomeModel *model = self.dataArray[indexPath.row];
    if (model.type == 1){ //视频
        CCHomeVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:kCCHomeVideoCell];
        cell.play.tag = indexPath.row;
        [cell.play addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        cell.model = model;
        return cell;
    }else if (model.type == 2 && model.coverArr.count >= 3){
        CCHome3PicCell *cell = [tableView dequeueReusableCellWithIdentifier:kCCHome3PicCell];
        cell.model = model;
        return cell;
    }else{
        CCHomePicCell *cell = [tableView dequeueReusableCellWithIdentifier:kCCHomePicCell];
        cell.model = model;
        return cell;
    }
    return [UITableViewCell new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCHomeModel *model = self.dataArray[indexPath.row];
    if (model.type == 2 && model.coverArr.count >= 3){
        CGFloat height = [model.title sizeForFont:FFont(17) size:CGSizeMake(App_Width - 30, CGFLOAT_MAX) mode:NSLineBreakByCharWrapping].height;
        return height > 30 ? 185 : 170;
    }else if (model.type == 1){ //视频
        CGFloat height = [model.title sizeForFont:FFont(17) size:CGSizeMake(App_Width - 30, CGFLOAT_MAX) mode:NSLineBreakByCharWrapping].height;
        CGFloat videoH = (App_Width - 30) * 9 / 16.0;
        return height > 30 ? 105 + videoH : 90 + videoH;
    }else{
        return 115;
    }
    return CGFLOAT_MIN;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CCHomeModel *model = self.dataArray[indexPath.row];
    DetailViewController *vc = [[DetailViewController alloc] init];
    vc.articleId = model.id;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.playingIndexPath isEqual:indexPath]) {
        CCHomeVideoCell *cell = (CCHomeVideoCell *)[_tableView cellForRowAtIndexPath:indexPath];
        cell.play.hidden = NO;
        [self.playerView pause];
        [self.playerView removeFromSuperview];
        self.playerView = nil;
    }
}
//播放视频
- (void)playVideo:(UIButton *)sender {
    NSInteger index = sender.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    CCHomeVideoCell *cell = (CCHomeVideoCell *)[_tableView cellForRowAtIndexPath:indexPath];
    cell.imgView.userInteractionEnabled = YES;
    self.playingIndexPath = indexPath;
    CCHomeModel  *model = self.dataArray[index];
//    model.content = @"http://static.smartisanos.cn/common/video/proud-farmer.mp4";
    if (![model.content containsString:@".mp4"]) {//网页播放
        DetailViewController *vc = [[DetailViewController alloc] init];
        vc.articleId = model.id;
        [self.navigationController pushViewController:vc animated:YES];
    }else{ //播放器播放
        
        ZFPlayerModel *playerModel = [[ZFPlayerModel alloc]init];
        playerModel.title = model.title;
        playerModel.videoURL = [NSURL URLWithString:model.content];
        playerModel.placeholderImageURLString = model.cover;
        playerModel.fatherView = cell.imgView;
        
        self.playerView = [[ZFPlayerView alloc]init];
        self.playerView.hasPreviewView = YES;
        self.playerView.disablePanGesture = YES;
        self.playerView.delegate = self;
        [self.playerView playerControlView:nil playerModel:playerModel];
        [self.playerView autoPlayTheVideo];
        cell.play.hidden = YES;
    }
}

#pragma mark - Lazy Load

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (UIButton *)redButton {
    if (!_redButton) {
        _redButton = [[UIButton alloc] init];
    }
    return _redButton;
}
//村代理 县代理
#pragma mark - delegate StickyDelegate  Banner
//实习村长列表

- (void)showApplyList {
    CCShiXiVillageListView *view = [[CCShiXiVillageListView alloc]initWithFrame:CGRectMake(0, 0, App_Width - 50, App_Width - 50)];
    view.dataArray = self.shixiVillArray;
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}
- (void)replyManagerBtnClick:(BOOL)isVillage {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.titleStr = @"申请管理员";
    vc.urlStr =  [NSString stringWithFormat:@"http://www.cct369.com/village/public/misc/zadmin_reg?rid=%@",USERID];
    vc.hiddenNav = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)pushToAgentWithId:(NSString *)idStr {
    [App_Delegate.tabbarVc gotoIndex:1];
}
- (void)clickAtHomeBanner {
    DetailViewController *vc = [[DetailViewController alloc] init];
    vc.url     = self.stickyDic[@"url"];
    vc.village = self.stickyDic[@"village"];
    vc.aid     = self.stickyDic[@"id"];
    vc.iscol   = self.stickyDic[@"iscol"];
    vc.mini    = self.stickyDic[@"mini"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)clickAtHomeBtn:(NSInteger)index {
    WEAKSELF
    if(index == 0){//生活圈
#pragma mark - PTTEST
        CCGirlCountryHomeController *vc = [[CCGirlCountryHomeController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
//        if ([UserManager sharedInstance].locs.count == 0){
//            [self setHomeAddress:^{
//                CCShareTabbarController *vc = [[CCShareTabbarController alloc]init];
//                vc.isPushed = YES;
//                [weakSelf.navigationController pushViewController:vc animated:YES];
//            }];
//        }else{
//            CCShareTabbarController *vc = [[CCShareTabbarController alloc]init];
//            vc.isPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
//        }
    }else if(index == 1) {
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"account/login") params:@{@"mobile":PHONE, @"passwd":PASSWORD,@"tel":STR(@"+86")} success:^(NSDictionary *success) {
                
                [self.navigationController pushViewController:[LTTTabBarController new] animated:YES];
                
            } failure:^(NSError *   failure) {
                
                NSLog(@"%@",failure.localizedDescription);
            }];
    }else if (index == 2) {//种金豆
        if ([UserManager sharedInstance].locs.count == 0){
            [self setHomeAddress:^{
                MYHomeController *vc = [[MYHomeController alloc]init];
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }];
        }else{
            MYHomeController *vc = [[MYHomeController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if(index == 3 ){ //酒老大
        CCWineTabbarController *vc = [[CCWineTabbarController alloc]init];
        [vc setNavTitle:index == 0 ? @"3+N" : @"阿罗拉"];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (index == 4){ //货运通
        [self transportReviewState];
    }else if(index == 5){// 红包
        if ([UserManager sharedInstance].userid && App_Delegate.lat && App_Delegate.cityId ) {
            CCRedTabbarController *vc = [[CCRedTabbarController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            SHOW(@"数据与定位获取中，请稍后");
        }
    }else{//教育
        SHOW(@"玩命开发中,敬请期待");
    }

}
- (void)transportReviewState {
    TSRoleReviewState state = [UserManager sharedInstance].tsReviewState;
    if ([UserManager sharedInstance].tsRole == TSRoleNormal && [UserManager sharedInstance].userid) { //去认证
        TSVerifyHomeController *vc = [[TSVerifyHomeController alloc]init];
        vc.verifyBlock = ^{
            [App_Delegate getUserDataFinishBlock:nil];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }else if(state == TSRoleReviewStateing){
        [App_Delegate getUserDataFinishBlock:^(NSDictionary *dict) {
            if ([UserManager sharedInstance].tsReviewState == TSRoleReviewStateing) {
                SHOW(@"认证审核中，请耐心等待");
            }else{
                [self transportReviewState];
            }
        }];
    }else if(state == TSRoleReviewStatePass){//认证通过
        CCTransportTabbarController *vc = [[CCTransportTabbarController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if(state == TSRoleReviewStateRefuse){
        SHOW(@"认证被拒绝，详情请联系客服");
    }else{
        SHOW(@"数据加载中，请稍后");
    }
}
- (void)setHomeAddress:(selectHomeBlock)selectBlock {
    if (self.setVillageed) {
        return;
    }
    self.setVillageed = YES;
    AddVillageController *vc = [[AddVillageController alloc]init];
    vc.selectLocBlock = ^(NSString *location, NSString *locationId) {
        NSDictionary *dic = @{@"id": [UserManager sharedInstance].secret,
                              @"portrait": [UserManager sharedInstance].path,
                              @"realname": [UserManager sharedInstance].realname,
                              @"location": locationId};
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/update") params:dic target:self success:^(NSDictionary *success) {
            [MBProgressHUD showMessage:@"保存成功"];
            [App_Delegate getUserDataFinishBlock:nil];
            [self loadData];
            if (selectBlock) {
                selectBlock();
            }
        } failure:^(NSError *failure) {
        }];
    };
    [self presentViewController:vc animated:YES completion:nil];
}


#pragma mark - Target-Action
// Push to News Controller
- (void)messageDetail:(UIButton *)rightItem {
    NewsViewController *vc = [[NewsViewController alloc]init];
    vc.callback = ^(){
        if (![[UserManager sharedInstance].actpoint isEqualToString:@"0"] || ![[UserManager sharedInstance].syspoint isEqualToString:@"0"]) {
            [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang1"] forState:UIControlStateNormal];
        }else {
            [_messageItem setImage:[UIImage imageNamed:@"btn_lingdang0"] forState:UIControlStateNormal];
        }
        
    };
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)scan {
    if(ISLOGIN){
        CCScanController *vc = [[CCScanController alloc]init];
        vc.libraryType = SLT_Native;
        vc.scanCodeType = SCT_QRCode;
        vc.isNeedScanImage  = YES;
        vc.isOpenInterestRect = YES;
        vc.style = [self DIY];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        SHOW(@"请登录后使用");
    }
}
//红包
- (void)hasRed {
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"rlis") params:@{@"id": USERID} target:nil success:^(NSDictionary *success) {
        
        if ([success[@"data"][@"redid"] intValue] == 0) {
        
        }else {
        
            RedViewController *vc = [[RedViewController alloc] init];

            vc.imgvStr = success[@"data"][@"image"];
            vc.redid = success[@"data"][@"redid"];
            vc.redText = success[@"data"][@"text"];
        
        vc.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        
            [self presentViewController:vc animated:YES completion:^{
                
            }];
            
        }
    } failure:^(NSError *failure) {
        
     }];
}

- (void)dealloc {
    [NOTICENTER removeObserver:self];
}
//扫码
- (LBXScanViewStyle*)DIY
{
    //设置扫码区域参数
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //扫码框中心位置与View中心位置上移偏移像素(一般扫码框在视图中心位置上方一点)
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型设置为在框的上面,可自行修改查看效果
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_On;
    
    //扫码框周围4个角绘制线段宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角水平长度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角垂直高度
    style.photoframeAngleH = 24;
    
    
    //动画类型：网格形式，模仿支付宝
    style.anmiationStyle = LBXScanViewAnimationStyle_NetGrid;
    
    //动画图片:网格图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_part_net"];;
    
    //扫码框周围4个角的颜色
    style.colorAngle =MAINBLUE;
    
    //是否显示扫码框
    style.isNeedShowRetangle = YES;
    //扫码框颜色
    style.colorRetangleLine = [UIColor colorWithRed:247/255. green:202./255. blue:15./255. alpha:1.0];
    
    //非扫码框区域颜色(扫码框周围颜色，一般颜色略暗)
    //必须通过[UIColor colorWithRed: green: blue: alpha:]来创建，内部需要解析成RGBA
    style.notRecoginitonArea = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    return style;
}
@end
