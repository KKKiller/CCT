//
//  ComplaintsThreeController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/19.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "ComplaintsThreeController.h"
#import "CCTButton.h"

@interface ComplaintsThreeController ()

@end

@implementation ComplaintsThreeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addReturnBtn:@"img_cha"];
    [self addTitle:@"投诉"];
    [self uiConfig];
}

- (void)uiConfig {
    UIImageView *imgv = [[UIImageView alloc] init];
    [self.view addSubview:imgv];
    imgv.image = [UIImage imageNamed:@"tscg"];
    [imgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@99);
        make.centerX.equalTo(self.view);
        make.size.mas_equalTo(90 * KEY_RATE);
    }];
    UILabel *successLabel = [[UILabel alloc] init];
    [self.view addSubview:successLabel];
    successLabel.text = @"投诉成功";
    [successLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(imgv.mas_bottom).with.offset(20 * KEY_RATE);
        make.height.mas_equalTo(28 * KEY_RATE);
    }];
    successLabel.textAlignment = NSTextAlignmentCenter;
    
    UILabel *contentLabel = [[UILabel alloc] init];
    [self.view addSubview:contentLabel];
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(15 * KEY_RATE);
        make.right.equalTo(self.view).with.offset(-15 * KEY_RATE);
        make.top.equalTo(successLabel.mas_bottom).with.offset(20 * KEY_RATE);
        make.height.mas_equalTo(62 * KEY_RATE);
    }];
    contentLabel.textAlignment = NSTextAlignmentCenter;
    contentLabel.attributedText = [self getAttributedStringWithString:@"感谢您的参与，全球村村通坚决反对色情、暴力、欺诈等违规信息，我们会认真处理你的投诉，维护绿色，健康的网络环境。" lineSpace:5];
    contentLabel.numberOfLines = 3;
    contentLabel.font = FontSize(14);
    contentLabel.textColor = [UIColor colorWithHexString:@"#888888"];
    
    CCTButton *btn = [[CCTButton alloc] init];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentLabel.mas_bottom).with.offset(40 * KEY_RATE);
        make.left.mas_equalTo(15 * KEY_RATE);
        make.right.mas_equalTo(-15 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpace; // 调整行间距
    NSRange range = NSMakeRange(0, [string length]);
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributedString;
}

- (void)backMove {
    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
}

- (void)btnClick {
    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
}
@end
