//
//  CCApplyManagerController.m
//  CunCunTong
//
//  Created by 我是MT on 2017/5/31.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCApplyManagerController.h"
#import "CCTTextField.h"
#import <WebKit/WebKit.h>

@interface CCApplyManagerController ()<WKNavigationDelegate>
@property (nonatomic, strong) WKWebView *webview;
//@property (nonatomic, strong) UILabel *tipsLbl;
//@property (nonatomic, strong) UILabel *typeLbl;
//@property (nonatomic, strong) UILabel *phoneLbl;
//@property (nonatomic, strong) UILabel *passwordLbl;
//@property (nonatomic, strong) UILabel *addressLbl;
//
//@property (nonatomic, strong) UIButton *managerBtn1;
//@property (nonatomic, strong) UIButton *managerBtn2;
//@property (nonatomic, strong) UILabel *phoneTextLbl;
//@property (nonatomic, strong) CCTTextField *passwordTF;
//@property (nonatomic, strong) UILabel *addressTextLbl;
//
//@property (nonatomic, strong) UIButton *applyBtn;
//@property (nonatomic, strong) UILabel *bottomTipsLbl;
@end

@implementation CCApplyManagerController

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    [self setNav];
//    [self setUI];
//    [self layoutSubview];
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    //    self.navigationController.navigationBarHidden = YES;
    [self setRequest];
}
- (void)setNav {
    [self addReturnBtn:@"tui_"];
    [self addTitle:self.titleStr];
}
- (void)setRequest {
    NSURL *url = [NSURL URLWithString:self.urlStr];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    [self.webview loadRequest:request];
}

- (WKWebView *)webview {
    if (_webview == nil) {
        _webview = [[WKWebView alloc]initWithFrame:CGRectMake(0, 24, App_Width, App_Height-24)];
        _webview.navigationDelegate = self;
        [self.view addSubview:_webview];
    }
    return _webview;
}
//- (void)setNav {
//    [self addReturnBtn:@"tui_"];
//    [self addTitle:@"申请管理员"];
//}
//- (void)setUI {
//    self.tipsLbl = [[UILabel alloc]initWithText:@"申请乡/镇级管理员请确保您钱包余额至少有1600元,申请村级管理员无限制\n充值方法:进入我的->我的钱包->充值" font:12 textColor:[UIColor orangeColor]];
//    [self.view addSubview:self.tipsLbl];
//    
//    self.typeLbl = [[UILabel alloc]initWithText:@"申请类别:" font:12 textColor:MTRGB(0x666666)];
//    [self.view addSubview:self.typeLbl];
//    
//    self.phoneLbl = [[UILabel alloc]initWithText:@"申请账号:" font:12 textColor:MTRGB(0x666666)];
//    [self.view addSubview:self.phoneLbl];
//    
//    self.passwordLbl = [[UILabel alloc]initWithText:@"登录密码:" font:12 textColor:MTRGB(0x666666)];
//    [self.view addSubview:self.passwordLbl];
//    
//    self.addressLbl = [[UILabel alloc]initWithText:@"申请地址:" font:12 textColor:MTRGB(0x666666)];
//    [self.view addSubview:self.addressLbl];
//    
//    self.managerBtn1 = [[UIButton alloc]init];
//    [self.view addSubview:self.managerBtn1];
//    [self.managerBtn1 setTitle:@"经营性管理员" forState:UIControlStateNormal];
//    [self.managerBtn1 setTitleColor:TEXTLIGHTBLACK6 forState:UIControlStateNormal];
//    [self.managerBtn1 setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
//    self.managerBtn1.selected = YES;
//    self.managerBtn1.layer.borderColor = [UIColor orangeColor].CGColor;
//    self.managerBtn1.layer.borderWidth = 0.5;
//    self.managerBtn1.layer.cornerRadius = 3;
//    self.managerBtn1.layer.masksToBounds = YES;
//    self.managerBtn1.titleLabel.font = FFont(12);
//    [self.managerBtn1 addTarget:self action:@selector(managerTypeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    
//    self.managerBtn2 = [[UIButton alloc]init];
//    [self.view addSubview:self.managerBtn2];
//    [self.managerBtn2 setTitle:@"党政管理员" forState:UIControlStateNormal];
//    [self.managerBtn2 setTitleColor:TEXTLIGHTBLACK6 forState:UIControlStateNormal];
//    [self.managerBtn2 setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
//    self.managerBtn2.titleLabel.font = FFont(12);
//    self.managerBtn2.layer.borderColor = TEXTLIGHTBLACK6.CGColor;
//    self.managerBtn2.layer.borderWidth = 0.5;
//    self.managerBtn2.layer.cornerRadius = 3;
//    self.managerBtn2.layer.masksToBounds = YES;
//    [self.managerBtn2 addTarget:self action:@selector(managerTypeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    
//    self.phoneTextLbl = [[UILabel alloc]initWithText:@"18678897939" font:12 textColor:TEXTLIGHTBLACK6];
//    [self.view addSubview:self.phoneTextLbl];
//    
//    self.passwordTF = [[CCTTextField alloc]init];
//    [self.view addSubview:self.passwordTF];
//    self.passwordTF.font = FFont(12);
//    self.passwordTF.placeholder = @"输入登录密码";
//    self.passwordTF.borderStyle = UITextBorderStyleNone;
//    
//    self.addressTextLbl = [[UILabel alloc]initWithText:@"选择所在地 >" font:12 textColor:TEXTLIGHTBLACK6];
//    [self.view addSubview:self.addressTextLbl];
//    self.addressTextLbl.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
//        SHOW(@"选择");
//    }];
//    [self.addressTextLbl addGestureRecognizer:tap];
//    
//    self.applyBtn = [[UIButton alloc]initWithTitle:@"申请" textColor:WHITECOLOR backImg:nil font:14];
//    [self.view addSubview:self.applyBtn];
//    self.applyBtn.layer.cornerRadius = 3;
//    self.applyBtn.layer.masksToBounds = YES;
//    self.applyBtn.backgroundColor = MAINBLUE;
//    
//    self.bottomTipsLbl = [[UILabel alloc]initWithText:@"1. 只有党政机关可以申请党政管理员,其他只能申请经营性管理员。\n2. 申请管理员请填写与APP会员相同的电话和密码!" font:12 textColor:TEXTGRAY9];
//    [self.view addSubview:self.bottomTipsLbl];
//}
//
//- (void)layoutSubview {
//    CGFloat margin = 15;
//    [self.tipsLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view.mas_left).offset(15);
//        make.right.equalTo(self.view.mas_right).offset(-15);
//        make.top.equalTo(self.view.mas_top).offset(64 + 15);
//        make.height.mas_equalTo(60);
//    }];
//    [self.typeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.tipsLbl.mas_left);
//        make.top.equalTo(self.tipsLbl.mas_bottom).offset(margin);
//    }];
//    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.tipsLbl.mas_left);
//        make.top.equalTo(self.typeLbl.mas_bottom).offset(margin);
//    }];
//    [self.passwordLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.tipsLbl.mas_left);
//        make.top.equalTo(self.phoneLbl.mas_bottom).offset(margin);
//    }];
//    [self.addressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.tipsLbl.mas_left);
//        make.top.equalTo(self.passwordLbl.mas_bottom).offset(margin);
//    }];
//    [self.managerBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.typeLbl.mas_right).offset(margin);
//        make.centerY.equalTo(self.typeLbl.mas_centerY);
//        make.height.mas_equalTo(25);
//        make.width.mas_equalTo(80);
//    }];
//    [self.managerBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.managerBtn1.mas_right).offset(margin);
//        make.centerY.equalTo(self.typeLbl.mas_centerY);
//        make.height.mas_equalTo(25);
//        make.width.mas_equalTo(70);
//    }];
//    [self.phoneTextLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.phoneLbl.mas_right).offset(margin);
//        make.centerY.equalTo(self.phoneLbl.mas_centerY);
//    }];
//    [self.passwordTF mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.left.equalTo(self.passwordLbl.mas_right).offset(margin);
//        make.left.equalTo(self.view.mas_left).offset(72);
//        make.centerY.equalTo(self.passwordLbl.mas_centerY);
//        make.right.equalTo(self.view.mas_right).offset(-margin);
//        make.height.mas_equalTo(30);
//    }];
//    [self.addressTextLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.addressLbl.mas_right).offset(margin);
//        make.right.equalTo(self.tipsLbl.mas_right);
//        make.centerY.equalTo(self.addressLbl.mas_centerY);
//    }];
//    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self.tipsLbl);
//        make.top.equalTo(self.addressLbl.mas_bottom).offset(40);
//        make.height.mas_equalTo(40);
//    }];
//    [self.bottomTipsLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self.tipsLbl);
//        make.bottom.equalTo(self.view.mas_bottom).offset(-margin);
//    }];
//}
//
//- (void)managerTypeBtnClick:(UIButton *)sender {
//    sender.layer.borderColor = [UIColor orangeColor].CGColor;
//    sender.selected = YES;
//    if (sender == self.managerBtn1) {
//        self.managerBtn2.layer.borderColor = TEXTLIGHTBLACK6.CGColor;
//        self.managerBtn2.selected = NO;
//    }else{
//        self.managerBtn1.layer.borderColor = TEXTLIGHTBLACK6.CGColor;
//        self.managerBtn1.selected = NO;
//    }
//}
@end
