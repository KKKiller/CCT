//
//  ComplaintsOneController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/19.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "ComplaintsOneController.h"
#import "CCTButton.h"
#import "ComplaintsTwoController.h"

@interface ComplaintsOneController ()<UITableViewDelegate, UITableViewDataSource> {
    UITableView *_tableView;
    NSInteger _complaintsType;
    BOOL _isSelect;
}

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) NSMutableArray *selectArray;

@end

@implementation ComplaintsOneController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addReturnBtn:@"tui_"];
    [self addTitle:@"投诉"];

    
    CCTButton *btn = [[CCTButton alloc] init];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15 * KEY_RATE);
        make.right.mas_equalTo(-15 * KEY_RATE);
        make.bottom.mas_equalTo(-32 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    [btn setTitle:@"下一步" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    
    _tableView                  = [[UITableView alloc] init];
    _tableView.delegate         = self;
    _tableView.dataSource       = self;
    _tableView.tableFooterView  = [UIView new];
    _tableView.backgroundColor  = [UIColor colorWithHexString:@"#EEEEEE"];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(btn.mas_top);
        make.height.mas_equalTo(KEY_HEIGHT - 64 - 82 * KEY_RATE);
    }];
    
    _isSelect = NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellid = @"cellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    cell.textLabel.text = self.dataSource[indexPath.row];
    if ([self.selectArray[indexPath.row] boolValue]) {
        cell.textLabel.textColor = [UIColor colorWithHexString:@"#387EFF"];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else {
        cell.textLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] initWithArray:@[@"欺诈",
                                                              @"色情",
                                                              @"政治谣言",
                                                              @"常识性谣言",
                                                              @"诱导分享",
                                                              @"恶意营销",
                                                              @"隐私信息收集",
                                                              @"抄袭文章",
                                                              @"其他侵权类(冒名、诽谤)",
                                                              @"违规声明原创"]];
    }
    return _dataSource;
}

- (NSMutableArray *)selectArray {
    if (!_selectArray) {
        _selectArray = [[NSMutableArray alloc] initWithArray:@[@NO, @NO, @NO, @NO, @NO, @NO, @NO, @NO, @NO, @NO]];
    }
    return _selectArray;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 40)];
    titleLabel.text = @"    请选择投诉原因";
    titleLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    return titleLabel;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    for (NSInteger i = 0; i < self.selectArray.count; i++) {
        self.selectArray[i] = @NO;
    }
    self.selectArray[indexPath.row] = @YES;
    _complaintsType = indexPath.row + 1;
    _isSelect = YES;
    [tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (void)btnClick {
    if (!_isSelect) {
        [MBProgressHUD showMessage:@"必需选择一个类型"];
        return;
    }
    ComplaintsTwoController *vc = [[ComplaintsTwoController alloc] init];
    vc.complaintsType = _complaintsType;
    vc.aid = _aid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
