//
//  ComplaintsOneController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/19.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface ComplaintsOneController : BaseViewController

@property (nonatomic, copy) NSString *aid;

@end
