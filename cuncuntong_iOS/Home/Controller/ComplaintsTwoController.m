//
//  ComplaintsTwoController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/19.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "ComplaintsTwoController.h"
#import "CCTButton.h"
#import "ComplaintsThreeController.h"

@interface ComplaintsTwoController ()<UITextViewDelegate> {
    UILabel *_currentPageLabel;
    NSInteger _currentPage;
    UITextView *_textView;
}

@end

@implementation ComplaintsTwoController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addReturnBtn:@"img_cha"];
    [self addTitle:@"投诉"];
    [self uiConfig];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    
}

- (void)uiConfig {
    _currentPage = 0;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, KEY_WIDTH, 40)];
    titleLabel.text = @"    投诉概述";
    titleLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    [self.view addSubview:titleLabel];
    
    _textView = [[UITextView alloc] init];
    _textView.delegate = self;
    _textView.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:_textView];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(titleLabel.mas_bottom);
        make.height.mas_equalTo(130 * KEY_RATE);
    }];
    
    CCTButton *btn = [[CCTButton alloc] init];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15 * KEY_RATE);
        make.right.mas_equalTo(-15 * KEY_RATE);
        make.top.equalTo(_textView.mas_bottom).with.offset(35 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    [btn setTitle:@"提交" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    
    _currentPageLabel = [[UILabel alloc] init];
    [self.view addSubview:_currentPageLabel];
    [_currentPageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).with.offset(-15);
        make.top.equalTo(self.view).with.offset(104 + 100 * KEY_RATE);
        make.width.mas_equalTo(50 * KEY_RATE);
        make.height.mas_equalTo(15 * KEY_RATE);
    }];
    _currentPageLabel.textAlignment = NSTextAlignmentRight;
    _currentPageLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    _currentPageLabel.font = [UIFont systemFontOfSize:14];
    _currentPageLabel.text = [NSString stringWithFormat:@"%ld/10",_currentPage];
    _currentPageLabel.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    // font 15 -> height 18
    if (textView.contentSize.height > 180) {
        return NO;
    }
    _currentPageLabel.text = [NSString stringWithFormat:@"%ld/10",(NSInteger)textView.contentSize.height / 18];
    return YES;
}

- (void)backMove {
    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
}

- (void)btnClick {
    if (_textView.text.length == 0) {
        [MBProgressHUD showMessage:@"必需输入概述"];
        return;
    }
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"article/complaint")
                                    params:@{@"id": USERID,
                                             @"aid": _aid,
                                             @"type": [NSString stringWithFormat:@"%ld", _complaintsType],
                                             @"content": _textView.text}
                                    target:self success:^(NSDictionary *success) {
                                        [MBProgressHUD showMessage:@"投诉成功"];
                                        ComplaintsThreeController *vc = [[ComplaintsThreeController alloc] init];
                                        [self.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *failure) {
    
    }];

}

@end
