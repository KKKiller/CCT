//
//  RewardController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/21.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "RewardController.h"
#import "CCTButton.h"
#import "RechargeViewController.h"

@interface RewardController () {
    NSMutableAttributedString *_mStr;
}

@property (nonatomic, strong) UITextField *moneyTF;
@property (nonatomic, strong) CCTButton *rewardButton;
@property (nonatomic, strong) CCTButton *rechargeButton;
@property (nonatomic, strong) YYLabel *moneyLabel;

@end

@implementation RewardController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self uiConfig];
}

- (void)uiConfig {
    
    [self addTitle:@"打赏"];
    [self addReturnBtn:@"tui_"];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    
    NSString *money = [NSString stringWithFormat:@"本次最多可打赏%@元",[UserManager sharedInstance].money];
    
    _mStr = [[NSMutableAttributedString alloc] initWithString:money];
    [_mStr addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"AAAAAA"]} range:NSMakeRange(0, 7)];
    [_mStr addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"AAAAAA"]} range:NSMakeRange(_mStr.length - 1, 1)];
    [_mStr addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"FF2E50"]} range:NSMakeRange(7, _mStr.length - 8)];
    
    
    [self.rewardButton setTitle:@"确认打赏" forState:UIControlStateNormal];
    [self.rewardButton  addTarget:self action:@selector(sureReward) forControlEvents:UIControlEventTouchUpInside];
    
    [self.rechargeButton setTitle:@"充值" forState:UIControlStateNormal];
    [self.rechargeButton setBackgroundColor:[UIColor whiteColor]];
    [self.rechargeButton setTitleColor:[UIColor colorWithHexString:@"444"] forState:UIControlStateNormal];
    [self.rechargeButton addTarget:self action:@selector(recharge) forControlEvents:UIControlEventTouchUpInside];
    
    self.moneyLabel.attributedText = _mStr;
    
    
    [self.view addSubview:self.moneyTF];
    [self.view addSubview:self.rewardButton];
    [self.view addSubview:self.moneyLabel];
    [self.view addSubview:self.rechargeButton];
    
    [self.moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view).with.offset(10 * KEY_RATE + 64);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    
    [self.rewardButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(17 * KEY_RATE);
        make.right.mas_equalTo(-17 * KEY_RATE);
        make.top.equalTo(self.moneyTF.mas_bottom).with.offset(36 * KEY_RATE);
        make.height.mas_equalTo(50 * KEY_RATE);
    }];
    
    [self.rechargeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.equalTo(self.rewardButton);
        make.top.equalTo(self.rewardButton.mas_bottom).with.offset(10 * KEY_RATE);
    }];
    
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18 * KEY_RATE);
        make.top.equalTo(self.rechargeButton.mas_bottom).with.offset(10 * KEY_RATE);
        make.right.mas_equalTo(-17 * KEY_RATE);
        make.height.mas_equalTo(13 * KEY_RATE);
    }];

}

- (UITextField *)moneyTF {
    if (!_moneyTF) {
        UILabel *left            = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120 * KEY_RATE, 50 * KEY_RATE)];
        left.text                = @"打赏金额";
        left.font                = FontSize(16);
        left.textAlignment       = NSTextAlignmentCenter;
        UIView *right            = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20 * KEY_RATE, 50 * KEY_RATE)];
        _moneyTF                 = [[UITextField alloc] init];
        _moneyTF.backgroundColor = [UIColor whiteColor];
        _moneyTF.placeholder     = @"请输入打赏的金额";
        _moneyTF.font            = FontSize(16);
        _moneyTF.leftView        = left;
        _moneyTF.leftViewMode    = UITextFieldViewModeAlways;
        _moneyTF.textAlignment   = NSTextAlignmentRight;
        _moneyTF.rightView       = right;
        _moneyTF.rightViewMode   = UITextFieldViewModeAlways;
    }
    return _moneyTF;
}

- (CCTButton *)rewardButton {
    if (!_rewardButton) {
        _rewardButton = [[CCTButton alloc] init];
    }
    return _rewardButton;
}

- (CCTButton *)rechargeButton {
    if (!_rechargeButton) {
        _rechargeButton = [[CCTButton alloc] init];
    }
    return _rechargeButton;
}

- (YYLabel *)moneyLabel {
    if (!_moneyLabel) {
        _moneyLabel      = [[YYLabel alloc] init];
        _moneyLabel.font = FontSize(11);
    }
    return _moneyLabel;
}

- (void)sureReward {
    
    if (self.moneyTF.text.length == 0) {
        [MBProgressHUD showMessage:@"必须输入打赏金额"];
        return;
    }
    
    if ([self.moneyTF.text floatValue] > [[UserManager sharedInstance].money floatValue]) {
        [MBProgressHUD showMessage:[NSString stringWithFormat:@"最多可以打赏%@元",[UserManager sharedInstance].money]];
        return;
    }
    
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"article")
                                    params:@{@"id": USERID,
                                             @"aid": _aid,
                                             @"money": self.moneyTF.text}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       [MBProgressHUD showMessage:@"打赏成功"];
    } failure:^(NSError *failure) {
        
    }];
}

- (void)recharge {
    RechargeViewController *vc = [[RechargeViewController alloc] init];
    vc.block = ^() {
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": [UserManager sharedInstance].secret} target:self success:^(NSDictionary *success) {

            [_mStr replaceCharactersInRange:NSMakeRange(7, _mStr.length - 8) withString:success[@"data"][@"money"]];
            self.moneyLabel.attributedText = _mStr;
        } failure:^(NSError *failure) {
        }];
    };
    [self.navigationController pushViewController:vc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
