//
//  RedViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/23.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedViewController : UIViewController

@property (nonatomic, copy) NSString *imgvStr;
@property (nonatomic, copy) NSString *redid;
@property (nonatomic, copy) NSString *redText;

@end
