//
//  HomeViewController.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 8/23/16.
//  Copyright © 2016 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeViewController : BaseViewController
- (void)showShare;
@end
