//
//  RedViewController.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/23.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "RedViewController.h"

@interface RedViewController ()

@property (nonatomic, strong) UIImageView *redImageView;
@property (nonatomic, strong) UIImageView *portraitImgv;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *openLabel;
@property (nonatomic, strong) UIButton *quitButton;
@property (nonatomic, strong) UIImageView *subImageView;
@property (nonatomic, strong) UIImageView *subPortraitImgv;
@property (nonatomic, strong) UILabel *moneyLabel;
@property (nonatomic, strong) UILabel *saveLabel;
@property (nonatomic, assign) BOOL isGesOpen;

@end

@implementation RedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self uiConfig];
}

- (void)uiConfig {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        [self open];
    }];
    
    _isGesOpen = NO;
    [self.view addSubview:self.redImageView];
    self.redImageView.image = [UIImage imageNamed:@"hongbao1"];
    [self.redImageView addGestureRecognizer:tap];
    [self.redImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(200 * KEY_RATE);
        make.centerX.equalTo(self.view);
        make.height.mas_equalTo(240 * KEY_RATE);
        make.width.mas_equalTo(225 * KEY_RATE);
    }];
    
    [self.redImageView addSubview:self.portraitImgv];
    self.portraitImgv.layer.cornerRadius = 19 * KEY_RATE;
    [self.portraitImgv setImageWithURL:[NSURL URLWithString:self.imgvStr] placeholder:[UIImage imageNamed:@"partraitFail"]];
    [self.portraitImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(43 * KEY_RATE);
        make.centerX.equalTo(self.redImageView);
        make.size.mas_equalTo(38 * KEY_RATE);
    }];
    
    [self.redImageView addSubview:self.titleLabel];
    self.titleLabel.text = _redText;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor colorWithHexString:@"#FFCD1F"];
    self.titleLabel.font = FontSize(20);
    self.titleLabel.numberOfLines = 0;
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(120 * KEY_RATE);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
//        make.centerX.width.equalTo(self.redImageView);
//        make.height.mas_equalTo(35 * KEY_RATE);
    }];
    
    [self.redImageView addSubview:self.openLabel];
    self.openLabel.text = @"待拆开";
    self.openLabel.textAlignment = NSTextAlignmentCenter;
    self.openLabel.textColor = [UIColor colorWithHexString:@"#A90715"];
    self.openLabel.font = FontSize(14);
    [self.openLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(196 * KEY_RATE);
        make.centerX.width.equalTo(self.redImageView);
        make.height.mas_equalTo(16 * KEY_RATE);
    }];
    
    [self.redImageView addSubview:self.quitButton];
    [self.quitButton addTarget:self action:@selector(quit) forControlEvents:UIControlEventTouchUpInside];
    [self.quitButton setBackgroundImage:[UIImage imageNamed:@"hb_guanbi"] forState:UIControlStateNormal];
    [self.quitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(9 * KEY_RATE);
        make.right.mas_equalTo(-9 * KEY_RATE);
        make.size.mas_equalTo(13 * KEY_RATE);
    }];
    
}

- (void)open {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"qiang")
                                    params:@{@"id": USERID,
                                             @"redid": self.redid} target:self success:^(NSDictionary *success) {
                                                 [self.redImageView removeFromSuperview];
                                                 self.redImageView = nil;
                                                 [self getRed:success[@"data"]];
                                                 _isGesOpen = YES;
    } failure:^(NSError *failure) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (UIImageView *)redImageView {
    if (!_redImageView) {
        _redImageView = [[UIImageView alloc] init];
        _redImageView.userInteractionEnabled = YES;
    }
    return _redImageView;
}

- (UIImageView *)portraitImgv {
    if (!_portraitImgv) {
        _portraitImgv = [[UIImageView alloc] init];
    }
    return _portraitImgv;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
    }
    return _titleLabel;
}

- (UILabel *)openLabel {
    if (!_openLabel) {
        _openLabel = [[UILabel alloc] init];
    }
    return _openLabel;
}

- (UIButton *)quitButton {
    if (!_quitButton) {
        _quitButton = [[UIButton alloc] init];
    }
    return _quitButton;
}

- (UIImageView *)subImageView {
    if (!_subImageView) {
        _subImageView = [[UIImageView alloc] init];
    }
    return _subImageView;
}

- (UIImageView *)subPortraitImgv {
    if (!_subPortraitImgv) {
        _subPortraitImgv = [[UIImageView alloc] init];
    }
    return _subPortraitImgv;
}

- (UILabel *)moneyLabel {
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc] init];
    }
    return _moneyLabel;
}

- (UILabel *)saveLabel {
    if (!_saveLabel) {
        _saveLabel = [[UILabel alloc] init];
    }
    return _saveLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)quit {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"rej")
                                    params:@{@"id": USERID,
                                             @"redid": self.redid}
                                    target:self
                                   success:^(NSDictionary *success) {
                                       [self dismissViewControllerAnimated:YES completion:nil];
    } failure:^(NSError *failure) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)getRed:(NSDictionary *)dic {
    
    [self.view addSubview:self.subImageView];
    self.subImageView.image = [UIImage imageNamed:@"hongbao0"];
    [self.subImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(200 * KEY_RATE);
        make.centerX.equalTo(self.view);
        make.height.mas_equalTo(269 * KEY_RATE);
        make.width.mas_equalTo(225 * KEY_RATE);
    }];
    
    [self.subImageView addSubview:self.subPortraitImgv];
    self.subPortraitImgv.layer.cornerRadius = 19 * KEY_RATE;
    [self.subPortraitImgv setImageWithURL:[NSURL URLWithString:dic[@"image"]] placeholder:[UIImage imageNamed:@"partraitFail"]];
    [self.subPortraitImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(43 * KEY_RATE);
        make.centerX.equalTo(self.subImageView);
        make.size.mas_equalTo(38 * KEY_RATE);
    }];
    
    if ([dic[@"state"] isEqualToString:@"1"]) {
        [self.subImageView addSubview:self.moneyLabel];
        self.moneyLabel.textAlignment = NSTextAlignmentCenter;
        NSMutableAttributedString *maStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@元",dic[@"money"]]];
        [maStr addAttributes:@{NSFontAttributeName: FontSize(30)} range:NSMakeRange(0, maStr.length-1)];
        [maStr addAttributes:@{NSFontAttributeName: FontSize(14)} range:NSMakeRange(maStr.length-1, 1)];
        self.moneyLabel.textColor = [UIColor colorWithHexString:@"#FFCD1F"];
        self.moneyLabel.attributedText = maStr;
        [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(145 * KEY_RATE);
            make.centerX.equalTo(self.subImageView);
            make.top.mas_equalTo(134 * KEY_RATE);
            make.height.mas_equalTo(35 * KEY_RATE);
        }];
        
        [self.subImageView addSubview:self.saveLabel];
        self.saveLabel.textAlignment = NSTextAlignmentCenter;
        self.saveLabel.font = FontSize(12);
        self.saveLabel.textColor = [UIColor colorWithHexString:@"#A90715"];
        self.saveLabel.text = @"已存入余额";
        [self.saveLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.subImageView);
            make.top.equalTo(self.moneyLabel.mas_bottom).with.offset(10);
            make.height.mas_equalTo(13 * KEY_RATE);
        }];
        return;
    }
    [self.subImageView addSubview:self.moneyLabel];
    self.moneyLabel.textAlignment = NSTextAlignmentCenter;
    self.moneyLabel.textColor = [UIColor colorWithHexString:@"#FFCD1F"];
    self.moneyLabel.text = @"来晚一步，红包被领完了";
    self.moneyLabel.font = FontSize(21);
    self.moneyLabel.numberOfLines = 2;
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(145 * KEY_RATE);
        make.centerX.equalTo(self.subImageView);
        make.top.mas_equalTo(134 * KEY_RATE);
        make.height.mas_equalTo(60 * KEY_RATE);
    }];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!_isGesOpen) {
        return;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
