//
//  CCHomeAdminModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCHomeAdminModel : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger remainTime;
@property (nonatomic, assign) CGFloat rate;
@end
