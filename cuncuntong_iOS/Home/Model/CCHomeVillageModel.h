//
//  CCHomeVillageModel.h
//  CunCunTong
//
//  Created by 我是MT on 2017/6/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCVillageModel.h"
@interface CCHomeVillageModel : NSObject
@property (nonatomic, strong) NSArray<CCVillageModel *> *village; //村
//@property (nonatomic, strong) CCVillageModel *village;//村
@property (nonatomic, strong) NSString *vadmin; //村管理员
@property (nonatomic, strong) NSString *vadminTel;//村管理员电话

@property (nonatomic, strong) CCVillageModel *town;//乡镇
@property (nonatomic, strong) NSString *tadmin;//乡镇管理员
@property (nonatomic, strong) NSString *tadminTel;//乡镇管理员电话


@property (nonatomic, strong) CCVillageModel *country;//乡镇
@property (nonatomic, strong) NSString *cadmin;
@property (nonatomic, strong) NSString *cadminTel;
//村长实习相关
//@property (nonatomic, assign) NSInteger remainTime; //剩余时间(单位:秒)
//@property (nonatomic, strong) NSString *memberCount;//已发展会员数
//@property (nonatomic, assign) CGFloat rate; //完成百分比
//@property (nonatomic, assign) BOOL isZhengShi;


//盟主
//@property (nonatomic, strong) NSString *mzName;
//@property (nonatomic, assign) NSInteger status;
//@property (nonatomic, strong) NSString *mzTel; //1实习 2正式
//@property (nonatomic, assign) NSInteger mzRemainTime; //剩余时间(单位:秒)
//@property (nonatomic, assign) CGFloat mzRate; //完成百分比

@end
