//
//  CCHomeModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/4/15.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCHomeModel : NSObject
@property (nonatomic, assign) NSInteger reader;
@property (nonatomic, strong) NSString *create;
@property (nonatomic, assign) NSInteger type;  // 1 一张图  2 三张图   3视频
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *original;
@property (nonatomic, strong) NSString *mini;
@property (nonatomic, strong) NSString *cover;
@property (nonatomic, strong) NSArray *coverArr;
@property (nonatomic, assign) NSInteger zan;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, assign) NSInteger comment;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *content;
@end
