//
//  HomeSubModel.m
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/18.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import "HomeSubModel.h"

@implementation HomeSubModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"articleId" : @"id"};
}

@end
