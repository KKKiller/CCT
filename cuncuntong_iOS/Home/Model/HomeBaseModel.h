//
//  HomeBaseModel.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/18.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeBaseModel : NSObject

@property (nonatomic, copy) NSString *datetime;
@property (nonatomic, copy) NSString *village;
@property (nonatomic, copy) NSString *image;

@end
