//
//  CCHomeVillageModel.m
//  CunCunTong
//
//  Created by 我是MT on 2017/6/5.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCHomeVillageModel.h"

@implementation CCHomeVillageModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    // value should be Class or Class name.
    return @{@"village" : [CCVillageModel class]};
}
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"remainTime" : @[@"admininfo.remainTime"],
             @"memberCount" : @[@"admininfo.memberCount"],
             @"rate" : @[@"admininfo.rate"],
             
             @"mzName":@[@"unionAdmin.name"],
             @"mzTel":@[@"unionAdmin.tel"],
             @"mzRate":@[@"unionAdmin.rate"],
             @"mzRemainTime":@[@"unionAdmin.remainTime"],
             @"status":@[@"unionAdmin.status"]
             };
}
@end
