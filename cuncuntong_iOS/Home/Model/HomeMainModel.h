//
//  HomeMainModel.h
//  cuncuntong_iOS
//
//  Created by 尚文忠 on 16/9/18.
//  Copyright © 2016年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeMainModel : NSObject

@property (nonatomic, copy) NSString *village;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSString *articleId;
@property (nonatomic, copy) NSString *iscol;
@property (nonatomic, copy) NSString *mini;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *url;

@end
