//
//  CCWineHomeModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineModel.h"

@implementation CCWineModel
- (NSString *)price {
    return [NSString stringWithFormat:@"%.2f", [self.final_price floatValue]*[self.rule integerValue]];
}
@end
