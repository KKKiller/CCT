//
//  CCWineOrderController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCWineOrderedController.h"
#import "CCWineOrderingController.h"
@interface CCWineOrderController : BaseViewController
@property (strong, nonatomic) CCWineOrderingController *orderingVc;

@property (strong, nonatomic) CCWineOrderedController *orderedVc;

- (void)gotoOrdering;
- (void)gotoOrdered;
- (void)refreshData;
@end
