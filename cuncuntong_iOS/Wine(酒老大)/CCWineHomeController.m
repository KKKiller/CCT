//
//  CCWineHomeController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineHomeController.h"
#import "CCWineHomeCell.h"
#import "ZYBannerView.h"
#import "CCBuyDetailController.h"
#import "CCFacePayController.h"
#import "CCBuyPayController.h"
static NSString *cellID = @"CCWineHomeCell";
static CGFloat bannerHeight = 150;
@interface CCWineHomeController ()<UITableViewDataSource,UITableViewDelegate,ZYBannerViewDataSource,ZYBannerViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZYBannerView *banner;
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, assign) NSInteger pageIndex;

@end
@implementation CCWineHomeController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initRefreshView];
    self.pageIndex = 1;
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.banner startTimer];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.banner stopTimer];
}
- (void)refreshData { //点击tabbar刷新
    self.pageIndex = 1;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/wine_list") params:@{@"page":@(self.pageIndex),@"rid":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"code"]  isEqual: @(1)]) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            if ([success[@"msg"] isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dict in success[@"msg"]) {
                    CCWineModel *model = [CCWineModel modelWithJSON:dict];
                    [self.dataArray addObject:model];
                }
                [self endLoding:success[@"msg"]];
            }
        }else{
            SHOW(success[@"msg"]);
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCWineHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.buyBtn.tag = indexPath.row;
    cell.faceBuyBtn.tag = indexPath.row;
    [cell.buyBtn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.faceBuyBtn addTarget:self action:@selector(faceBuyBtnClick:) forControlEvents:UIControlEventTouchUpInside];

    if (self.dataArray.count > indexPath.row) {
        CCWineModel *model = self.dataArray[indexPath.row];
        cell.model = model;
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCWineModel *model = self.dataArray[indexPath.row];
        CCBuyDetailController *vc = [[CCBuyDetailController alloc]init];
        vc.wineModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)buyBtnClick:(UIButton *)sender {
    if (self.dataArray.count > sender.tag) {
        CCWineModel *model = self.dataArray[sender.tag];
        CCBuyPayController *vc = [[CCBuyPayController alloc]init];
        vc.wineModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)faceBuyBtnClick:(UIButton *)sender {
    if (self.dataArray.count > sender.tag) {
        CCWineModel *model = self.dataArray[sender.tag];
        CCFacePayController *vc = [[CCFacePayController alloc]init];
        vc.wineModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - banner
- (NSInteger)numberOfItemsInBanner:(ZYBannerView *)banner {
    return  [self.tabBarController.title isEqualToString:@"3+N"] ? 2 : 1;
}
- (UIView *)banner:(ZYBannerView *)banner viewForItemAtIndex:(NSInteger)index {
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, App_Width, bannerHeight) ];
    imgV.contentMode = UIViewContentModeScaleToFill;
    imgV.layer.masksToBounds = YES;
    NSString *title = self.tabBarController.title;
    if ([title isEqualToString:@"3+N"]) {
        imgV.image = index == 0 ? IMAGENAMED(@"banner_jiu0") : IMAGENAMED(@"banner_jiu1");
    }else{
        imgV.image =  IMAGENAMED(@"banner_aluola") ;

    }
    return imgV;
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat system = [[UIDevice currentDevice].systemVersion floatValue];
        CGFloat yy = system >= 11.0 ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, yy, App_Width, App_Height - 49)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 160 *KEY_RATE;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CCWineHomeCell class] forCellReuseIdentifier:cellID];
        _tableView.tableHeaderView = self.banner;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (ZYBannerView *)banner {
    if (!_banner) {
        _banner = [[ZYBannerView alloc]initWithFrame:CGRectMake(0, 0, App_Width, bannerHeight) ];
        _banner.dataSource = self;
        _banner.autoScroll = YES;
        _banner.shouldLoop = YES;
    }
    return _banner;
}
- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:CGRectMake(0, bannerHeight, App_Width, App_Height - 64 - bannerHeight)];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
