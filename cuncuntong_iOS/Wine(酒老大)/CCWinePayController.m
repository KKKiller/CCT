//
//  CCWinePayController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWinePayController.h"
#import "CCWineSimplePayView.h"
#import "RechargeViewController.h"
#import "CCPayController.h"
#import "MYHomeController.h"
@interface CCWinePayController ()<UITextFieldDelegate,UIAlertViewDelegate>
@property (nonatomic, strong) CCWineSimplePayView *payView;
@property (nonatomic, strong) NSString *money;
@end

@implementation CCWinePayController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"支付"];
    self.payView = [CCWineSimplePayView instanceView];
    self.payView.frame = CGRectMake(0, 64, App_Width, 300);
    [self.view addSubview:self.payView];
    self.view.backgroundColor = WHITECOLOR;
    [self.payView.addBtn addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.payView.minusBtn addTarget:self action:@selector(minusBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.payView.numField.delegate = self;
    self.payView.moneyLbl.text = [NSString stringWithFormat:@"金额:  %.2f元", [self.price floatValue]];
    [self.payView.payBtn addTarget:self action:@selector(pay) forControlEvents:UIControlEventTouchUpInside];
    self.money = self.price;
}
- (void)pay {
    if (self.payView.idNumField.text.length == 0) {
        SHOW(@"请输入配货编号");
        return;
    }
    [self.view endEditing:YES];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/pay") params:@{@"uid":USERID,@"wineid":STR(self.wineId),@"count":self.payView.numField.text,@"vnumber":self.payView.idNumField.text} target:self success:^(NSDictionary *success) {
        if ([success[@"code"] integerValue] == 1) {
            BaseViewController *jindou;
            for (BaseViewController *vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[MYHomeController class]]) {
                    jindou = vc;
                    break;
                }
            }
            if (jindou) {
                SHOW(@"购买成功");
                [self.navigationController popToViewController:jindou animated:YES];
                return ;
            }
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"购买成功" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            
            [alert show];
        }else if([success[@"msg"] isEqualToString:@"余额不足，请充值"]){
            SHOW(success[@"msg"]);
            CCPayController *vc =[[ CCPayController alloc]init];
            vc.money = self.money;
            vc.paySuccessBlock = ^{
                [self pay];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self backMove];
}

- (void)addBtnClick {
    NSInteger currentNum = [self.payView.numField.text integerValue];
    self.payView.numField.text = [NSString stringWithFormat:@"%ld",currentNum+1];
    self.payView.moneyLbl.text = [NSString stringWithFormat:@"金额:  %.2f元",(currentNum + 1) * [self.price floatValue]];
    self.money = [NSString stringWithFormat:@"%.2f",(currentNum + 1) * [self.price floatValue]];
}
- (void)minusBtnClick {
    NSInteger currentNum = [self.payView.numField.text integerValue];
    if (currentNum >= 2) {
        self.payView.numField.text = [NSString stringWithFormat:@"%ld",currentNum-1];
        self.payView.moneyLbl.text = [NSString stringWithFormat:@"金额:  %.2f元",(currentNum - 1) * [self.price floatValue]];
        self.money = [NSString stringWithFormat:@"%.2f",(currentNum - 1) * [self.price floatValue]];

    }else{
        SHOW(@"不能再少了");
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    if ([textField.text integerValue] > 0) {
        self.payView.moneyLbl.text = [NSString stringWithFormat:@"%.2f",[self.price floatValue] * [textField.text integerValue]];
    }
}
- (void)backMove {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
