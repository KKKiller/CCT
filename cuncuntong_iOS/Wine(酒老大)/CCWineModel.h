//
//  CCWineHomeModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCWineModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *rule; //规则
@property (nonatomic, strong) NSString *final_price; //价格
@property (nonatomic, strong) NSString *stocks; //股份
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *energy;
@property (nonatomic, strong) NSString *price; //每箱价格
@property (nonatomic, strong) NSString *unit;

@end
