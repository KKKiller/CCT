//
//  CCWineHomeCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCWineModel.h"
@interface CCWineHomeCell : UITableViewCell
@property (nonatomic, strong) UIButton *buyBtn;
@property (nonatomic, strong) UIButton *faceBuyBtn;

@property (nonatomic, strong) CCWineModel *model;
@end
