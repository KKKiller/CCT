//
//  CCExpressDetailController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCExpressDetailController : BaseViewController
@property (nonatomic, strong) NSString *expressName; //快递公司
@property (nonatomic, strong) NSString *expressNum; //快递编号
@end
