//
//  CCFacePayView.m
//  CunCunTong
//
//  Created by TAL on 2019/5/1.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCFacePayView.h"

@implementation CCFacePayView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}

@end
