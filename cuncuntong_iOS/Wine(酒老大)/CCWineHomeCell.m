//
//  CCWineHomeCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineHomeCell.h"
#import "NSMutableAttributedString+Style.h"
@interface CCWineHomeCell()
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *typeLbl;
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) UILabel *stockLbl;
@property (nonatomic, strong) UIView *sepV;
@end
@implementation CCWineHomeCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUI];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}



- (void)setModel:(CCWineModel *)model {
    _model = model;
//    model.rule = @"12*43";
//    model.price = @"22";
//    model.stock = @"33";
//    model.name = @"酒老大";
//    if (model.imgurl.count > 0) {
//    }
    [self.imgView setImageWithURL:URL(model.image) placeholder:IMAGENAMED(@"ph")];
    self.titleLbl.text = model.name;
    self.typeLbl.text = [NSString stringWithFormat:@"规格: 每件%@%@",model.rule,model.unit];
    NSString *price = [NSString stringWithFormat:@"价格: 每件%@元",model.price];
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc]initWithText:price color:TEXTBLACK6 subStrIndex:NSMakeRange(6, price.length - 6) subStrColor:ORANGECOLOR];
    self.priceLbl.attributedText = attr1;

    NSString *stock = [NSString stringWithFormat:@"股份: 每%@%@股",model.unit,model.stocks];
    NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc]initWithText:stock color:TEXTBLACK6 subStrIndex:NSMakeRange(4, stock.length - 4) subStrColor:MAINBLUE];
    self.stockLbl.attributedText = attr2;
}


- (void)setUI {
    self.imgView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.imgView];
    self.imgView.layer.cornerRadius = 4;
    self.imgView.layer.masksToBounds = YES;
    self.imgView.contentMode = UIViewContentModeScaleAspectFill;
    
    
    self.titleLbl = [[UILabel alloc]initWithText:@"酒老大" font:15 textColor:TEXTBLACK3];
    [self.contentView addSubview:self.titleLbl];
    self.titleLbl.font = [UIFont boldSystemFontOfSize:15];
    self.titleLbl.numberOfLines = 1;
    
    self.typeLbl = [[UILabel alloc]initWithText:@"规格:" font:12 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.typeLbl];
    self.typeLbl.numberOfLines = 1;
    
    self.priceLbl = [[UILabel alloc]initWithText:@"价格:" font:12 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.priceLbl];
    self.priceLbl.numberOfLines = 1;
    
    self.stockLbl = [[UILabel alloc]initWithText:@"股份:" font:12 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.stockLbl];
    self.stockLbl.numberOfLines = 1;
    
    
    self.buyBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.buyBtn];
    self.buyBtn.layer.cornerRadius = 4;
    self.buyBtn.layer.masksToBounds = YES;
    [self.buyBtn setBackgroundColor:MTRGB(0xCE1111)];
    [self.buyBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    [self.buyBtn setTitle:@"订 购" forState:UIControlStateNormal];
    self.buyBtn.titleLabel.font = FFont(16);
    
    
    self.faceBuyBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.faceBuyBtn];
    self.faceBuyBtn.layer.cornerRadius = 4;
    self.faceBuyBtn.layer.masksToBounds = YES;
    [self.faceBuyBtn setBackgroundColor:MTRGB(0xCE1111)];
    [self.faceBuyBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    [self.faceBuyBtn setTitle:@"当面购" forState:UIControlStateNormal];
    self.faceBuyBtn.titleLabel.font = FFont(16);
    
    
    self.sepV = [[UIView alloc]init];
    [self.contentView addSubview:self.sepV];
    self.sepV.backgroundColor = MTRGB(0xfafafa);
}
- (void)layoutSubviews {
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView).offset(15);
        make.width.mas_equalTo(170 * KEY_RATE);
        make.height.mas_equalTo(170 * (416.0/ 500.0) * KEY_RATE);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgView.mas_top).offset(3);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        make.left.equalTo(self.imgView.mas_right).offset(10);
    }];
    [self.typeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLbl.mas_bottom).offset(5);
        make.left.equalTo(self.titleLbl.mas_left).offset(0);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        
    }];
    
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.typeLbl.mas_bottom).offset(5);
        make.left.equalTo(self.typeLbl.mas_left).offset(0);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        
    }];
    [self.stockLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.priceLbl.mas_bottom).offset(5);
        make.left.equalTo(self.priceLbl.mas_left).offset(0);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        
    }];
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.typeLbl.mas_left);
        make.bottom.equalTo(self.imgView.mas_bottom).offset(-10);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(25);
    }];
    [self.faceBuyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.buyBtn.mas_right).offset(5);
        make.bottom.equalTo(self.imgView.mas_bottom).offset(-10);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(25);
    }];
    [self.sepV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
}

@end
