//
//  CCWineOrderingController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineOrderingController.h"
#import "CCWineOrderModel.h"
#import "CCWineOrderCell.h"
#import "CCExpressDetailController.h"
#import "CCWineOrderController.h"
static NSString *CellId = @"orderCell";

@interface CCWineOrderingController ()<UITableViewDataSource,UITableViewDelegate,CCWineOrderCellDelegate>
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CCEmptyView *emptyView;

@end

@implementation CCWineOrderingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    [self initRefreshView];
    [self.view addSubview:self.tableView];
    [self loadData];
}
- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/order") params:@{@"uid":USERID,@"stat":@"0"} target:nil success:^(NSDictionary *success) {
        if ([success[@"code"] integerValue] == 1) {
            [self.dataArray removeAllObjects];
            for (NSDictionary *dict in success[@"msg"]) {
                CCWineOrderModel *model = [CCWineOrderModel modelWithJSON:dict];
                [self.dataArray addObject: model];
            }
            [self endLoding:success[@"msg"]];
        }else{
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO : YES;
    [self.tableView.mj_footer endRefreshingWithNoMoreData];
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCWineOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    cell.delegate = self;
    if (self.dataArray.count > indexPath.row) {
        CCWineOrderModel *model = self.dataArray[indexPath.row];
        cell.model = model;
    }
    return cell;
}
//快递
- (void)clickAtExpressWithModel:(CCWineOrderModel *)model {
    CCExpressDetailController *vc = [[CCExpressDetailController alloc]init];
        vc.expressNum = model.courier_number;
        vc.expressName = model.courier_company;
    [self.navigationController pushViewController:vc animated:YES];
}
//收货
- (void)clickAtRecievtWithModel:(CCWineOrderModel *)model {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/sign") params:@{@"orderid":STR(model.orderid),@"uid":USERID} target:self success:^(NSDictionary *success) {
        if ([success[@"code"] integerValue] == 1) {
            SHOW(@"收货成功");
            CCWineOrderController *parentVc = (CCWineOrderController *)self.parentViewController;
            [parentVc refreshData];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {        
    }
}

#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height -108 - 49)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 160;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, -44, 0);
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CCWineOrderCell class] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
@end
