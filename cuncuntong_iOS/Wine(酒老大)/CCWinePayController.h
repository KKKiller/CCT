//
//  CCWinePayController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCWinePayController : BaseViewController
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *wineId;
@end
