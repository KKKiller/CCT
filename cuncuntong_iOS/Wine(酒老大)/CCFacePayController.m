//
//  CCFacePayController.m
//  CunCunTong
//
//  Created by TAL on 2019/5/1.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCFacePayController.h"
#import "CCFacePayView.h"
#import "BRPickerView.h"
@interface CCFacePayController ()<UITextFieldDelegate>
@property (nonatomic, strong) CCFacePayView *payView;
@property(nonatomic,assign) NSInteger count;
@end

@implementation CCFacePayController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"当面购";

    self.view.backgroundColor = [UIColor whiteColor];
    self.payView = [CCFacePayView instanceView];
    [self.view addSubview:self.payView];
    self.payView.frame = CGRectMake(0, 64, App_Width, 400);
    self.count = 1;
    self.payView.moneyLbl.text = [NSString stringWithFormat:@"金额：%@元", self.wineModel.final_price];
    self.payView.codefield.delegate = self;
    [self.payView.codefield addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.payView.editBtn addTarget:self action:@selector(editNum) forControlEvents:UIControlEventTouchUpInside];
    [self.payView.payBtn addTarget:self
                            action:@selector(payBtnClick)  forControlEvents:UIControlEventTouchUpInside];
}
- (void)editNum {
    [BRStringPickerView showStringPickerWithTitle:@"选择数量" dataSource:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30"] defaultSelValue:@1 isAutoSelect:NO resultBlock:^(id selectValue) {
        self.count = [selectValue integerValue];
        self.payView.numLbl.text = [NSString stringWithFormat:@"数量：%@",selectValue];
        NSString *money = [NSString stringWithFormat:@"%.2f", [self.wineModel.final_price floatValue] * [selectValue floatValue]];
        self.payView.moneyLbl.text = [NSString stringWithFormat:@"金额： %@元",money];
    }];
}
- (void)payBtnClick {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/pay") params:@{@"uid":USERID,@"vnumber":self.payView.codefield.text,@"count":@(self.count),@"wineid":self.wineModel.id} success:^(NSDictionary *success) {
        NSString *msg = success[@"msg"];
         SHOW(msg);
         [self.navigationController popViewControllerAnimated:YES];
        [App_Delegate.wineTabbarVc gotoOrder];
    } failure:^(NSError *failure) {
    }];
}

- (void)textFieldDidChange:(UITextField *)textField {
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"wine/get_admin_by_id") params:@{@"id":textField.text} success:^(NSDictionary *success) {
        if ([success[@"code"] integerValue] == 0 ) {
            NSString *name = [NSString stringWithFormat:@"%@%@",success[@"msg"][@"areaName"],success[@"msg"][@"name"]];
            self.payView.agentInfo.text = name;
        }
    } failure:^(NSError *failure) {
        
    }];
}
@end
