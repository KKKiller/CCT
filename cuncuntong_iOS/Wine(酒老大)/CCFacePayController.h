//
//  CCFacePayController.h
//  CunCunTong
//
//  Created by TAL on 2019/5/1.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCWineModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CCFacePayController : BaseViewController
@property (nonatomic, strong) CCWineModel *wineModel;

@end

NS_ASSUME_NONNULL_END
