//
//  CCWineOrderModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCWineOrderModel : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *orderid;
@property (nonatomic, strong) NSString *wid;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *wineid;
@property (nonatomic, strong) NSString *vid;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *stat; //0未发货  1已发货 2已完成
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *per_price;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *count;
@property (nonatomic, assign) NSTimeInterval addtime;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *final_price;
@property (nonatomic, assign) NSInteger rule;
@property (nonatomic, strong) NSString *totalPrice; //每箱总价

@property (nonatomic, strong) NSString *courier_company;
@property (nonatomic, strong) NSString *courier_number;
@end
