//
//  CCWineSimplePayView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/16.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCTTextField.h"
@interface CCWineSimplePayView : UIView
@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@property (weak, nonatomic) IBOutlet UITextField *idNumField;
@property (weak, nonatomic) IBOutlet CCTTextField *numField;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

+ (instancetype)instanceView;

@end
