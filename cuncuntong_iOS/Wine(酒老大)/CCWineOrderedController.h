//
//  CCWineOrderedController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCWineOrderedController : BaseViewController
@property (strong, nonatomic) UITableView *tableView;
- (void)refreshData;
@end
