//
//  CCWineOrderedController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineOrderedController.h"
#import "CCWineOrderModel.h"
#import "CCWineOrderCell.h"
#import "CCBuyPayController.h"
static NSString *CellId = @"orderCell";
@interface CCWineOrderedController ()<UITableViewDataSource,UITableViewDelegate,CCWineOrderCellDelegate>
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CCEmptyView *emptyView;

@end

@implementation CCWineOrderedController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    [self initRefreshView];
    [self.view addSubview:self.tableView];
    [self loadData];
}
- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"wine/order") params:@{@"uid":USERID,@"stat":@"1"} target:nil success:^(NSDictionary *success) {
        if ([success[@"code"] integerValue] == 1) {
            [self.dataArray removeAllObjects];
            for (NSDictionary *dict in success[@"msg"]) {
                CCWineOrderModel *model = [CCWineOrderModel modelWithJSON:dict];
                [self.dataArray addObject: model];
            }
            [self endLoding:success[@"msg"]];
        }else{
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO : YES;
    [self.tableView.mj_footer endRefreshingWithNoMoreData];
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCWineOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    cell.delegate = self;
    if (self.dataArray.count > indexPath.row) {
        CCWineOrderModel *model = self.dataArray[indexPath.row];
    cell.model = model; 
    }
    return cell;
}

#pragma mark - 代理方法
//再次购买
- (void)clickAtStateWithModel:(CCWineOrderModel *)model{
    CCBuyPayController *vc = [[CCBuyPayController alloc]init];
    CCWineModel *wineModel = [CCWineModel new];
    wineModel.id = model.wineid;
    wineModel.final_price = model.final_price;
    wineModel.image = model.image;
    wineModel.name = model.title;
    wineModel.rule = [NSString stringWithFormat:@"%@", @(model.rule)];
    vc.wineModel = wineModel;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)clickAtDeleteWithModel:(CCWineOrderModel *)model {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"wine/delete_order") params:@{@"orderid":STR(model.orderid)} target:self success:^(NSDictionary *success) {
        if([success[@"code"] isEqual:@(1)]){
            SHOW(@"删除成功");
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
//        CCWineOrderModel *model = self.dataArray[indexPath.row];
    
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (self.dataArray.count > indexPath.row) {
            CCWineOrderModel *model = self.dataArray[indexPath.row];
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"wine/delete_order") params:@{@"orderid":STR(model.orderid)} target:self success:^(NSDictionary *success) {
                if([success[@"code"] isEqual:@(1)]){
                    SHOW(@"删除成功");
                    [self.dataArray removeObject:model];
                    [self.tableView reloadData];
                }else{
                    SHOW(success[@"msg"]);
                }
            } failure:^(NSError *failure) {
            }];
        }
    }
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height -108 - 49)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 160;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, -44, 0);
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
         [_tableView registerClass:[CCWineOrderCell class] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
