//
//  CCWineOrderCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCWineOrderModel.h"

@protocol CCWineOrderCellDelegate <NSObject>
@optional
- (void)clickAtRecievtWithModel:(CCWineOrderModel *)model;
- (void)clickAtStateWithModel:(CCWineOrderModel *)model;
- (void)clickAtExpressWithModel:(CCWineOrderModel *)model;
- (void)clickAtDeleteWithModel:(CCWineOrderModel *)model;


@end
@interface CCWineOrderCell : UITableViewCell
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) UILabel *countLbl;
@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UIButton *stateBtn;
@property (nonatomic, strong) UIButton *receiveBtn;
@property (nonatomic, strong) UIButton *searchBtn;
@property (nonatomic, strong) UIButton *deleteBtn;

@property (nonatomic, strong) UILabel *sepLbl;


@property (nonatomic, strong) CCWineOrderModel *model;
@property (nonatomic, weak) id<CCWineOrderCellDelegate> delegate;
@end
