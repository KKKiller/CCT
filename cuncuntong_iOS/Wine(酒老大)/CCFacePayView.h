//
//  CCFacePayView.h
//  CunCunTong
//
//  Created by TAL on 2019/5/1.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCFacePayView : UIView
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UITextField *codefield;
@property (weak, nonatomic) IBOutlet UILabel *agentInfo;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
+ (instancetype)instanceView;

@end

NS_ASSUME_NONNULL_END
