//
//  CCWineTabbarController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCWineTabbarController : UITabBarController
- (void)gotoOrder;
- (void)setNavTitle:(NSString *)title;
@end
