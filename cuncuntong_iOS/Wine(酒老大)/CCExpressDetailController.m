//
//  CCExpressDetailController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCExpressDetailController.h"
#import "ExpressInfo.h"
#import "MF_Base64Additions.h"
#define eBusinessID @"1261753"
#define appKey @"d715f778-086b-4a25-95be-59dff451abab"
#define reqURL @"http://api.kdniao.cc/Ebusiness/EbusinessOrderHandle.aspx"
#define systemBlue [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]
@interface CCExpressDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ExpressInfo *express;
@property (nonatomic, strong) NSDictionary *expressNameAndCodeTrace;
@end

@implementation CCExpressDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"快递查询"];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    if ([self.expressNum isEqualToString:@""]) {
        SHOW(@"快递单号不正确");
    }
    
    self.expressNameAndCodeTrace = [NSDictionary dictionaryWithObjectsAndKeys:@"安信达快递",@"AXD",@"澳邮专线",@"AYCA",@"北青小红帽",@"BQXHM",@"百福东方",@"BFDF",@"百世快运",@"BTWL",@"百世快递",@"HTKY",@"CNPEX中邮快递",@"CNPEX",@"CCES快递",@"CCES",@"COE东方快递",@"COE",@"城际快递",@"CJKD",@"城市100",@"CITY100",@"长沙创一",@"CSCY",@"德邦",@"DBL",@"D速物流",@"DSWL",@"大田物流",@"DTWL",@"EMS",@"EMS",@"FEDEX联邦(国内件）",@"FEDEX",@"FEDEX联邦(国际件）",@"FEDEX_GJ",@"泛捷快递",@"PANEX",@"飞康达",@"FKD",@"广东邮政",@"GDEMS",@"共速达",@"GSD",@"国通快递",@"GTO",@"高铁速递",@"GTSD",@"鸿桥供应链",@"HOTSCM",@"海派通物流公司",@"HPTEX",@"汇丰物流",@"HFWL",@"汇强快递",@"ZHQKD",@"恒路物流",@"HLWL",@"华强物流",@"hq568",@"华夏龙物流",@"HXLWL",@"好来运快递",@"HYLSD",@"京广速递",@"JGSD",@"九曳供应链",@"JIUYE",@"佳吉快运",@"JJKY",@"嘉里物流",@"JLDT",@"捷特快递",@"JTKD",@"急先达",@"JXD",@"晋越快递",@"JYKD",@"加运美",@"JYM",@"佳怡物流",@"JYWL",@"快捷速递",@"FAST",@"快客快递",@"QUICK",@"跨越物流",@"KYWL",@"龙邦快递",@"LB",@"联昊通速递",@"LHT",@"民航快递",@"MHKD",@"明亮物流",@"MLWL",@"能达速递",@"NEDA",@"平安达腾飞快递",@"PADTF",@"PCA Express",@"PCA",@"全晨快递",@"QCKD",@"全峰快递",@"QFKD",@"全一快递",@"UAPEX",@"全日通快递",@"QRT",@"如风达",@"RFD",@"瑞丰速递",@"RFEX",@"速必达物流",@"SUBIDA",@"赛澳递",@"SAD",@"圣安物流",@"SAWL",@"盛邦物流",@"SBWL",@"上大物流",@"SDWL",@"顺丰快递",@"SF",@"盛丰物流",@"SFWL",@"盛辉物流",@"SHWL",@"速通物流",@"ST",@"申通快递",@"STO",@"速腾快递",@"STWL",@"速尔快递",@"SURE",@"天地华宇",@"HOAU",@"天天快递",@"HHTT",@"唐山申通",@"TSSTO",@"UEQ Express",@"UEQ",@"万家物流",@"WJWL",@"万象物流",@"WXWL",@"新邦物流",@"XBWL",@"信丰快递",@"XFEX",@"希优特",@"XYT",@"新杰物流",@"XJ",@"优速快递",@"UC",@"亚马逊物流",@"AMAZON",@"源安达快递",@"YADEX",@"远成物流",@"YCWL",@"韵达快递",@"YD",@"义达国际物流",@"YDH",@"越丰物流",@"YFEX",@"原飞航物流",@"YFHEX",@"亚风快递",@"YFSD",@"运通快递",@"YTKD",@"圆通速递",@"YTO",@"亿翔快递",@"YXKD",@"邮政平邮/小包",@"YZPY",@"增益快递",@"ZENY",@"宅急送",@"ZJS",@"众通快递",@"ZTE",@"中铁快运",@"ZTKY",@"中通速递",@"ZTO",@"中铁物流",@"ZTWL",@"中邮物流",@"ZYWL", nil];
    [self loadData];
}

- (void)loadData {
    //1.编写上传参数
//    self.expressName = @"YD";
//    self.expressNum = @"3901099043551";
    NSString* requestData = [NSString stringWithFormat:@"{\"OrderCode\":\"\",\"ShipperCode\":\"%@\",\"LogisticCode\":\"%@\"}",
                             self.expressName,self.expressNum];
    NSMutableDictionary* params = [[NSMutableDictionary alloc]init];
    NSString* dataSignTmp = [[NSString alloc]initWithFormat:@"%@%@",requestData,appKey];
    NSString* dataSign = [[MD5UTils getmd5WithString:dataSignTmp] base64String];
    [params setObject:[requestData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"RequestData"];
    [params setObject:eBusinessID forKey:@"EBusinessID"];
    [params setObject:@"1002" forKey:@"RequestType"];
    [params setObject:[dataSign stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"DataSign"];
    [params setObject:@"2" forKey:@"DataType"];
    //2.上传参数并获得返回值
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:reqURL parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"请求成功：%@",responseObject);
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        //3. 获得网络数据赋值给ExpressInfo对象
        NSMutableArray* expressTraces = [[NSMutableArray alloc]init];
        for (NSDictionary* traces in [json objectForKey:@"Traces"]) {
            [expressTraces insertObject:traces atIndex:0];
        }
        NSString* shipperCode = [json objectForKey:@"ShipperCode"];
        NSString* logisticCode = [json objectForKey:@"LogisticCode"];
        
        self.express = [[ExpressInfo alloc]initWitfShipperCode:shipperCode andlogisticCode:logisticCode andexpressForUser:@"" andexpressTraces:expressTraces];
        [self.tableView reloadData];
        if([[json objectForKey:@"State"] isEqualToString:@"0"]){
            NSString *msg = [json objectForKey:@"Reason"];
            SHOW(msg);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"请求失败：%@",error.description);
    }];
}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return [self.express.expressTraces count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell;
    //1. 返回快递名称 单号数据
    if (indexPath.section == 0) {
        static NSString* cellIndentifier = @"cell";
        cell = [self.tableView dequeueReusableCellWithIdentifier:cellIndentifier];
//        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIndentifier];
            cell.textLabel.text = [self.expressNameAndCodeTrace objectForKey:self.express.shipperCode];
            NSLog(@"%@++++%@",[self.expressNameAndCodeTrace objectForKey:self.express.shipperCode],self.express.shipperCode);
            cell.detailTextLabel.text = self.express.logisticCode;
            cell.imageView.image = [UIImage imageNamed:@"express-history"];
//        }
    }else if (indexPath.section == 1){
        
        static NSString* cellIndentifier = @"UITabelViewCellTracesSystem";
        cell = [self.tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIndentifier];
        }
        NSDictionary* traces = self.express.expressTraces[indexPath.row];
        NSString* tracesInfoString = [traces objectForKey:@"AcceptStation"];//得到物流轨迹
        NSString* timeInfoString = [traces objectForKey:@"AcceptTime"];//获得物流时间
        cell.textLabel.text = timeInfoString;
        cell.detailTextLabel.text = tracesInfoString;
        cell.imageView.image = [UIImage imageNamed:@"express-history"];
        //调整UILabel自动换行
        cell.detailTextLabel.numberOfLines = 0;
        
        cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica"  size:11];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        cell.textLabel.textColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
        
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height-65)];
    }
    return _tableView;
}
@end
