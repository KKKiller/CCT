//
//  CCWineOrderModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/10.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineOrderModel.h"

@implementation CCWineOrderModel
- (NSString *)totalPrice {
    return [NSString stringWithFormat:@"%.2f", [self.final_price floatValue] * 1];
}
@end
