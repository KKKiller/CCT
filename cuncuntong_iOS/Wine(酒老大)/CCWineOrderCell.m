//
//  CCWineOrderCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineOrderCell.h"
static CGFloat width = 80;
@implementation CCWineOrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUI];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}


- (void)setModel:(CCWineOrderModel *)model {
    _model = model;

    self.titleLbl.text = model.title;
    [self.imgView setImageWithURL:URL(model.image) placeholder:IMAGENAMED(@"ph")];
    self.priceLbl.text = [NSString stringWithFormat:@"单价%.2f元",[model.totalPrice floatValue]];
    self.countLbl.text = [NSString stringWithFormat:@"订购数:%@件  总金额:%.2f元",model.count,[model.totalPrice floatValue]*[model.count integerValue]];
    self.timeLbl.text = [TOOL convertIntavalTime:model.addtime];
    
    self.deleteBtn.hidden = ![model.stat isEqualToString:@"2"];
    self.receiveBtn.hidden = ![model.stat isEqualToString:@"1"];
    self.searchBtn.hidden = ![model.stat isEqualToString:@"1"];
    
    if ([model.stat isEqualToString:@"0"]) { //代发货
        [self.stateBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        [self.stateBtn setTitle:@"待发货" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundColor:MTRGB(0x0760de)];
        self.stateBtn.layer.borderWidth = 0;
    }else if ([model.stat isEqualToString:@"1"]){//已发货
        [self.stateBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        [self.stateBtn setTitle:@"已发货" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundColor:MTRGB(0xce1211)];
        self.stateBtn.layer.borderWidth = 0;
    }else if ([model.stat isEqualToString:@"2"]){  //已完成
        [self.stateBtn setTitleColor:MTRGB(0xce1211) forState:UIControlStateNormal];
        [self.stateBtn setTitle:@"再次购买" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundColor:WHITECOLOR];
        self.stateBtn.layer.borderColor = MTRGB(0xce1211).CGColor;
        self.stateBtn.layer.borderWidth = 0.5;
    }
    CGFloat rightMargin = [model.stat isEqualToString:@"1"] ? (width * 2 + 15*3) : 15;
    [self.stateBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(30);
        make.right.equalTo(self.mas_right).offset(-rightMargin);
        make.top.equalTo(self.searchBtn.mas_top);
    }];
}

- (void)receiveBtnClick {
    if ([self.delegate respondsToSelector:@selector(clickAtRecievtWithModel:)]) {
        [self.delegate clickAtRecievtWithModel:self.model];
    }
}
- (void)searchBtnClick {
    if ([self.delegate respondsToSelector:@selector(clickAtExpressWithModel:)]) {
        [self.delegate clickAtExpressWithModel:self.model];
    }
}
- (void)stateBtnClick {
    if ([self.delegate respondsToSelector:@selector(clickAtStateWithModel:)]) {
        [self.delegate clickAtStateWithModel:self.model];
    }
}
- (void)deleteBtnClick {
    if ([self.delegate respondsToSelector:@selector(clickAtDeleteWithModel:)]) {
        [self.delegate clickAtDeleteWithModel:self.model];
    }
}
- (void)setUI{
    self.imgView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.imgView];
    self.imgView.layer.cornerRadius = 4;
    self.imgView.layer.masksToBounds = YES;
    
    self.titleLbl = [[UILabel alloc]initWithText:@"" font:15 textColor:BLACKCOLOR];
    [self.contentView addSubview:self.titleLbl];
    self.titleLbl.numberOfLines = 1;
    
    self.priceLbl = [[UILabel alloc]initWithText:@"" font:13 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.priceLbl];
    self.priceLbl.numberOfLines = 1;
    
    self.countLbl = [[UILabel alloc]initWithText:@"" font:13 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.countLbl];
    self.countLbl.numberOfLines = 1;
    
    self.timeLbl = [[UILabel alloc]initWithText:@"" font:13 textColor:TEXTBLACK9];
    [self.contentView addSubview:self.timeLbl];
    self.timeLbl.numberOfLines = 1;
    
    self.stateBtn = [[UIButton alloc]initWithTitle:@"已发货" textColor:WHITECOLOR backImg:nil font:15];
    [self.contentView addSubview:self.stateBtn];
    [self.stateBtn setBackgroundColor:MTRGB(0xce1211)];
    self.stateBtn.layer.cornerRadius = 3;
    self.stateBtn.layer.masksToBounds = YES;
    [self.stateBtn addTarget:self action:@selector(stateBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.receiveBtn = [[UIButton alloc]initWithTitle:@"签收" textColor:MTRGB(0xce1211) backImg:nil font:15];
    [self.receiveBtn setBackgroundColor:WHITECOLOR];
    [self.contentView addSubview:self.receiveBtn];
    self.receiveBtn.layer.cornerRadius = 3;
    self.receiveBtn.layer.masksToBounds = YES;
    self.receiveBtn.layer.borderWidth = 0.5;
    self.receiveBtn.layer.borderColor = MTRGB(0xce1211).CGColor;
    [self.receiveBtn addTarget:self action:@selector(receiveBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.searchBtn = [[UIButton alloc]initWithTitle:@"快递查询" textColor:MTRGB(0x666666) backImg:nil font:15];
    [self.searchBtn setBackgroundColor:WHITECOLOR];
    [self.contentView addSubview:self.searchBtn];
    self.searchBtn.layer.cornerRadius = 3;
    self.searchBtn.layer.masksToBounds = YES;
    self.searchBtn.layer.borderWidth = 0.5;
    self.searchBtn.layer.borderColor = MTRGB(0x666666).CGColor;
    [self.searchBtn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.sepLbl = [[UILabel alloc]initWithShallowLine];
    [self.contentView addSubview:self.sepLbl];
    
    self.deleteBtn = [[UIButton alloc]initWithTitle:nil textColor:nil backImg:@"wineDelete" font:15];
    [self.contentView addSubview:self.deleteBtn];
    self.deleteBtn.hidden = YES;
    [self.deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat margin = 5;
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView).offset(15);
        make.width.height.mas_equalTo(80);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imgView.mas_right).offset(15);
        make.top.equalTo(self.imgView.mas_top);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(margin);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl);
        make.top.equalTo(self.priceLbl.mas_bottom).offset(margin);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl);
        make.top.equalTo(self.countLbl.mas_bottom).offset(margin);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    CGFloat height = 30;
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.top.equalTo(self.imgView.mas_bottom).offset(15);
    }];
    [self.receiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
        make.right.equalTo(self.searchBtn.mas_left).offset(-15);
        make.top.equalTo(self.searchBtn.mas_top);

    }];
    [self.stateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
        make.right.equalTo(self.contentView.mas_right).offset(-(width * 2 + margin * 3));
        make.top.equalTo(self.searchBtn.mas_top);
    }];
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(20);
        make.centerY.equalTo(self.stateBtn.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(30);
    }];
    [self.sepLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
}
@end
