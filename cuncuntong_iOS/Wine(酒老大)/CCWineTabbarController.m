//
//  CCWineTabbarController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineTabbarController.h"
#import "NavigationViewController.h"
#import "CCWineOrderController.h"
@interface CCWineTabbarController ()<UITabBarDelegate,UITabBarControllerDelegate>
@property (nonatomic, strong) UIButton *leftButton;

@end

@implementation CCWineTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.barTintColor = MTRGB(0xf0f0f0);
    [self initSelf];
    self.title = @"3+N";
    self.delegate = self;
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    App_Delegate.wineTabbarVc = self;
//    
//    for (int i = 0; i<3; i++) {
//        
//        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"banner_jiu"]];
//        imageView.frame = CGRectMake(App_Width/3 * i, 0, App_Width/3, 49);
//        [self.tabBar addSubview:imageView];
//        imageView.hidden = i != 0;
//        imageView.tag = 100 + i;
//    }
    
}
//- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
//    NSInteger index = tabBarController.selectedIndex;
//    for (UIImageView *v in self.tabBar.subviews) {
//        if (v.tag >= 100) {
//            v.hidden = v.tag != 100+index;
//        }
//    }
//}
- (void)setNavTitle:(NSString *)title {
    self.title = title;
}
- (void)initSelf {
    
    NSArray *vcArr = @[@"CCWineHomeController",
                       @"CCWineOrderController",
                       @"CCMyStockController"];
    
    NSArray *imageArr = @[@"home01",
                          @"dingdan01",
                          @"gufen01"];
    
    NSArray *selImageArr = @[@"home",
                             @"dingdan",
                             @"gufen"];
    NSArray *titleArr = @[@"首页",
                          @"我的订单",
                          @"我的股份"];
    
    NSMutableArray *tabArrs = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < vcArr.count; i++) {
        
        UIViewController *vc = [[NSClassFromString(vcArr[i]) alloc] init];
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        vc.tabBarItem.image = [[UIImage imageNamed:imageArr[i]]
                               imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selImageArr[i]]
                                       imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.title = titleArr[i];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x6c6c6c)} forState:UIControlStateNormal];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0xcc161d)} forState:UIControlStateSelected];
        
        
        [tabArrs addObject:vc];
    }
    
    // 解决push/pop黑影
    self.view.backgroundColor = [UIColor whiteColor];
    //    self.tabBar.tintColor = [UIColor greenColor];
    self.viewControllers = tabArrs;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGSize size = CGSizeMake(App_Width / 3.0, self.tabBar.bounds.size.height);
    self.tabBar.selectionIndicatorImage = [self drawTabbarBackWithSize:size];
    self.tabBar.selectionIndicatorImage = IMAGENAMED(@"tabbarRed");
}
- (UIImage *)drawTabbarBackWithSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    CGContextRef content = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(content, 206/255.0, 17/255.0, 17/255.0, 1);
    CGContextFillRect(content, CGRectMake(0, 0, size.width, size.height));
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
- (void)gotoOrder {
    self.selectedIndex = 1;
    CCWineOrderController *vc =  self.viewControllers[1];
    [vc refreshData];
}
-(void)backMove {
    [App_Delegate.tabbarVc gotoHome];
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_leftButton addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        self.navigationItem.leftBarButtonItem = left;
    }
    return _leftButton;
}
@end
