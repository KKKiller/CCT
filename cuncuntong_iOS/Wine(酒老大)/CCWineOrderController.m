//
//  CCWineOrderController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineOrderController.h"
#import "MTPageView.h"
static CGFloat pageViewHeight = 44;

@interface CCWineOrderController ()<UIScrollViewDelegate,MTPageViewDelegate>
@property (strong, nonatomic) MTPageView *pageView;
@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation CCWineOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
     [self addTitle:@"订单"];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)gotoOrdering {
    [self pageViewDidClickAtIndex:0];
    [self.orderingVc  refreshData];
    self.pageView.offset = 0;
}
- (void)gotoOrdered {
    [self pageViewDidClickAtIndex:1];
    [self.orderedVc  refreshData];
    [self performSelector:@selector(setOffset) withObject:nil afterDelay:0.3];
}
- (void)refreshData {
    [self.orderingVc refreshData];
    [self.orderedVc refreshData];
}
- (void)setOffset {
    self.pageView.offset = App_Width;
}
#pragma mark - 代理方法
#pragma mark - 代理
- (void)pageViewDidClickAtIndex:(NSUInteger)index {
    [self.scrollView setContentOffset:CGPointMake(App_Width * index, 0) animated:YES];
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.pageView.offset = scrollView.contentOffset.x;
    self.pageView.colorArray = [TOOL colorWithR1:56 g1:126 b1:255 r2:34 g2:34 b2:34 currentOffset:(float)scrollView.contentOffset.x totlalLength:(float)App_Width];
}
#pragma mark - 私有方法
- (void)setUI {
    [self addTitle:@"我的订单"];
    [self.view addSubview:self.pageView];
    [self.view addSubview:self.scrollView];
}

#pragma mark - 懒加载
- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        CGFloat height = App_Height -108 - 49;
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 108, App_Width,height )];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(App_Width * 2, 0);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled= YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        
        self.orderedVc = [[CCWineOrderedController alloc]init];
        [self addChildViewController:self.orderedVc];
        self.orderedVc.view.frame = CGRectMake(App_Width, 0, App_Width, height);
        [_scrollView addSubview:self.orderedVc.view];
        self.orderedVc.tableView.frame = CGRectMake(0, 0, App_Width, height);
        
        self.orderingVc = [[CCWineOrderingController alloc]init];
        [self addChildViewController:self.orderingVc];
        self.orderingVc.view.frame = CGRectMake(0 , 0, App_Width, height);
        [_scrollView addSubview:self.orderingVc.view];
        self.orderingVc.tableView.frame = CGRectMake(0, 0, App_Width, height);
        
        
    }
    return _scrollView;
}
- (MTPageView *)pageView {
    if (_pageView == nil) {
        _pageView = [[MTPageView alloc]initWithFrame:CGRectMake(0, 64, App_Width, pageViewHeight)];
        _pageView.titleArray = @[@"进行中",@"已完成"];
        _pageView.delegate = self;
        [self.view addSubview:_pageView];
    }
    return _pageView;
}

@end
