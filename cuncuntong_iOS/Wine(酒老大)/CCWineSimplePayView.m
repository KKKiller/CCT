//
//  CCWineSimplePayView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/9/16.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCWineSimplePayView.h"

@implementation CCWineSimplePayView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}
@end
