//
//  CCCreditController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/24.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCCreditEnhanceController.h"

@interface CCCreditEnhanceController ()
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *originalCreditLbl;
@property (weak, nonatomic) IBOutlet UILabel *currentCreditLbl;

@property (weak, nonatomic) IBOutlet UIButton *returnHomeBtn;

@end

@implementation CCCreditEnhanceController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.returnHomeBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
}

@end
