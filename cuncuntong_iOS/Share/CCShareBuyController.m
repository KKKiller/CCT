//
//  CCShareBuyController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareBuyController.h"
#import "CCTTextField.h"
#import "CCSelectCityController.h"
#import "CCShareCommentController.h"
#import "RechargeViewController.h"
#import "CCInputKeywordView.h"
#import "CCPayController.h"
@interface CCShareBuyController ()<UITextViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *addressLbl;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *addressTap;


@property (weak, nonatomic) IBOutlet UITextView *addressDetailTextView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolder;
@property (weak, nonatomic) IBOutlet CCTTextField *nameField;
@property (weak, nonatomic) IBOutlet CCTTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet CCTTextField *numField;
@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (nonatomic, strong) CCInputKeywordView *keywordView;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, assign) CGFloat balance;
@property (nonatomic, strong) NSString *addressId;

@end

@implementation CCShareBuyController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

- (void)setUI {
    [IQKeyboardManager sharedManager].enable = YES;
     [self setTitle:@"订购"];
    self.view.backgroundColor = WHITECOLOR;
    [self.submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.addBtn addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.minusBtn addTarget:self action:@selector(minusBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.addressTap addTarget:self action:@selector(chooseAddress)];
    self.addressDetailTextView.delegate = self;
    self.numField.delegate = self;
    self.priceLbl.text = [NSString stringWithFormat:@"%.2f",self.price];
    NSLog(@"%@",[self.addressLbl class]);
}
//提交 支付
- (void)submitBtnClick {
    if ([self.addressLbl.text isEqualToString:@"选择地址"] || self.addressDetailTextView.text.length == 0) {
        SHOW(@"请选择地址");
        return;
    }
    if (self.nameField.text.length == 0) {
        SHOW(@"请输入姓名");
        return;
    }
    if (self.numField.text.length == 0) {
        SHOW(@"请输入数量");
        return;
    }
    if (self.phoneTextField.text.length == 0) {
        SHOW(@"请输入电话");
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": STR([UserManager sharedInstance].secret)} target:self success:^(NSDictionary *success) {
        self.balance = [success[@"data"][@"money"] floatValue];
        
        [self inputPassword];
    } failure:^(NSError *failure) {
    }];
}
- (void)inputPassword {
    [self.view endEditing:YES];
    CCInputKeywordView *view = [CCInputKeywordView instanceView];
    self.keywordView = view;
    [view.closeBtn addTarget:self action:@selector(closeRed) forControlEvents:UIControlEventTouchUpInside];
    [view.payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [view.chargeBtn addTarget:self action:@selector(chargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, 240, 270);
    view.keywordField.returnKeyType = UIReturnKeyDone;
//    view.keywordField.delegate = self;
    
    view.redMoney = [self.priceLbl.text floatValue];
    view.balance = self.balance;
    view.nameLbl.text = @"支付金额";
    
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:self.view];
}
- (void)closeRed {
    [self.animateModel closeWithStraight];
}
//充值
- (void)chargeBtnClick {
    [self closeRed];
    
    RechargeViewController *vc = [[RechargeViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
//发红包
- (void)payBtnClick {
    if (self.keywordView.keywordField.text.length == 0) {
        SHOW(@"请输入登录密码");
        return;
    }
    if (self.keywordView.keywordField.text.length == 0) {
        SHOW(@"请输入登录密码");
        return;
    }
    [self buy];
    
}

- (void)buy {
    NSDictionary *dict = @{@"order_user":USERID,
                           @"order_cid":STR(self.shareId),
                           @"passwd":self.keywordView.keywordField.text,
                           @"order_address":[NSString stringWithFormat:@"%@%@",self.addressLbl.text,self.addressDetailTextView.text],
                           @"order_name":self.nameField.text,
                           @"order_phone":self.phoneTextField.text,
                           @"order_count":self.numField.text,
                           @"order_total":self.priceLbl.text
                           };

    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/balance_pay") params:dict target:self  success:^(NSDictionary *success) {
        if([success[@"data"][@"stat"] integerValue] == 1 ){
            for (BaseViewController *vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[CCShareTabbarController class]]) {
                    [self.navigationController popToViewController:vc animated:NO];
                    [App_Delegate.shareTabbarVc gotoOrdering];
                }
            }
            SHOW(success[@"data"][@"info"]);
        }else if([success[@"data"][@"info"] isEqualToString:@"余额不足，请充值"]){
//            RechargeViewController *vc = [[RechargeViewController alloc]init];
//            [self.navigationController pushViewController:vc animated:YES];
            CCPayController *vc =[[ CCPayController alloc]init];
            vc.money = self.priceLbl.text;
            vc.paySuccessBlock = ^{
                [self buy];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            SHOW(success[@"data"][@"info"]);
        }
        [self closeRed];
    } failure:^(NSError *failure) {
    }];
}
- (void)chooseAddress {
    CCSelectCityController *vc = [[CCSelectCityController alloc]init];
    vc.selectLocBlock = ^(NSString *location,NSString *longAddress, NSString *locationId) {
        self.addressLbl.text = longAddress;
        self.addressId = locationId;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)addBtnClick {
    NSInteger currentNum = [self.numField.text integerValue];
    self.numField.text = [NSString stringWithFormat:@"%ld",currentNum+1];
    self.priceLbl.text = [NSString stringWithFormat:@"%.2f",(currentNum + 1) * self.price];
}
- (void)minusBtnClick {
    NSInteger currentNum = [self.numField.text integerValue];
    if (currentNum >= 2) {
        self.numField.text = [NSString stringWithFormat:@"%ld",currentNum-1];
        self.priceLbl.text = [NSString stringWithFormat:@"%.2f",(currentNum - 1) * self.price];
    }else{
        SHOW(@"不能再少了");
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    self.placeHolder.hidden = textView.text.length != 0;
}
- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    if ([textField.text integerValue] > 0) {
        self.priceLbl.text = [NSString stringWithFormat:@"%.2f",self.price * [textField.text integerValue]];
    }
}
@end
