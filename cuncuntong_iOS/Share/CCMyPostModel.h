//
//  CCMyPostModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCMyPostModel : NSObject
@property (nonatomic, strong) NSString *today_total;
@property (nonatomic, strong) NSString *today_total_money;
@property (nonatomic, strong) NSString *total;
@property (nonatomic, strong) NSString *total_money;

@property (nonatomic, strong) NSString *generalize_money;//推广余额

@end
