//
//  CCSelectCityController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^selectLocationBlock)(NSString *location, NSString *longAddress, NSString *locationId);
@interface CCSelectCityController : BaseViewController

@property (nonatomic, copy) selectLocationBlock selectLocBlock;
@property (nonatomic, assign, getter=isSelectProvince) BOOL selectProvince;
@end
