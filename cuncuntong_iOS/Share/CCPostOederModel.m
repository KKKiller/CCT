//
//  CCPostOederModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/18.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCPostOederModel.h"

@implementation CCPostOederModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"pinfo" : [CCShareDetailModel class]};
}
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"name" : @[@"uinfo.realname"],
             @"avatar" : @[@"uinfo.portrait"]
             };
}
@end
