//
//  CCMyPostShareCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCShareDetailModel.h"
@protocol CCMyPostShareCellDelegate<NSObject>
- (void)deleteBtnClick:(CCShareDetailModel *)model;
- (void)modifyBtnClick:(CCShareDetailModel *)model;
- (void)topBtnClick:(CCShareDetailModel *)model;
@end

@interface CCMyPostShareCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet UIButton *modifyBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *topBtn;
@property (weak, nonatomic) IBOutlet UIImageView *topImgView;
@property (nonatomic, strong) CCShareDetailModel *model;
@property (nonatomic, assign) BOOL noMoney;

@property (nonatomic, weak) id delegate;
@end
