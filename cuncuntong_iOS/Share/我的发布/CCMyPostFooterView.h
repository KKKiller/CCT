//
//  CCMyPostFooterView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMyPostModel.h"
@interface CCMyPostFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UIButton *chargeBtn;
@property (weak, nonatomic) IBOutlet UILabel *showTimesLbl;
@property (weak, nonatomic) IBOutlet UILabel *clickTimesLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesLbl;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UIView *containerView;
+ (instancetype)instanceView ;
@property (nonatomic, strong) CCMyPostModel *model;
@end
