//
//  CCMyPostShareCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyPostShareCell.h"

@implementation CCMyPostShareCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.modifyBtn.layer.cornerRadius = 6;
    self.modifyBtn.layer.masksToBounds = YES;
    self.modifyBtn.layer.borderWidth = 0.5;
    self.modifyBtn.layer.borderColor = MAINBLUE.CGColor;
    
    self.deleteBtn.layer.cornerRadius = 6;
    self.deleteBtn.layer.masksToBounds = YES;
    self.deleteBtn.layer.borderWidth = 0.5;
    self.deleteBtn.layer.borderColor = [UIColor redColor].CGColor;
    
    [self.modifyBtn addTarget:self action:@selector(modifyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topBtn addTarget:self action:@selector(topBtnClick) forControlEvents:UIControlEventTouchUpInside];

}
- (void)modifyBtnClick {
    if ([self.delegate respondsToSelector:@selector(modifyBtnClick:)]) {
        [self.delegate modifyBtnClick:self.model];
    }
}
- (void)deleteBtnClick {
    if ([self.delegate respondsToSelector:@selector(deleteBtnClick:)]) {
        [self.delegate deleteBtnClick:self.model];
    }
}
- (void)topBtnClick {
    if ([self.delegate respondsToSelector:@selector(topBtnClick:)]) {
        [self.delegate topBtnClick:self.model];
    }
}
- (void)setModel:(CCShareDetailModel *)model {
    _model = model;
    NSString *url = model.sp_imgs.count > 0 ? model.sp_imgs[0] : @"";
    [self.imgView  setImageWithURL:URL(url) placeholder:IMAGENAMED(@"SubPlaceholder")];
    self.titleLbl.text = model.sp_title;
    self.detailLbl.text = model.sp_content;
    self.priceLbl.text = [NSString stringWithFormat:@"定价：%@元", model.sp_pricing];
    NSString *text = model.share_state == 1 ? @"上线" : @"下线";
    [self.topBtn setTitle:text forState:UIControlStateNormal];
    if(model.share_state == 1){
        [self.topBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        self.topImgView.image = IMAGENAMED(@"share_top");
    }else{
        self.topImgView.image = IMAGENAMED(@"offline");
        [self.topBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
