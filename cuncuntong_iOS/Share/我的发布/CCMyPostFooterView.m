//
//  CCMyPostFooterView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyPostFooterView.h"

@implementation CCMyPostFooterView

+ (instancetype)instanceView {
    CCMyPostFooterView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.containerView.layer.cornerRadius = 4;
    view.containerView.layer.masksToBounds = YES;
    view.containerView.layer.borderWidth = 0.5;
    view.containerView.layer.borderColor = BACKGRAY.CGColor;
    
    view.detailBtn.layer.cornerRadius = 4;
    view.detailBtn.layer.masksToBounds = YES;
    view.detailBtn.layer.borderWidth = 0.5;
    view.detailBtn.layer.borderColor = MAINBLUE.CGColor;
    return view;
}
- (void)setModel:(CCMyPostModel *)model {
    _model = model;
    self.totalTimesLbl.text = [NSString stringWithFormat:@"总点击次数：%@",model.total];
    self.clickTimesLbl.text = [NSString stringWithFormat:@"今日点击次数：%@次",model.today_total];
    self.balanceLbl.text = model.generalize_money;
    
    CGFloat count = [model.generalize_money floatValue] / 0.03;
    self.showTimesLbl.text = [NSString stringWithFormat:@"可展示次数：%.0f次",count];

}
@end
