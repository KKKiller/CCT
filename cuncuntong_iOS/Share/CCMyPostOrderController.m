//
//  CCMyPostOrderController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/18.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyPostOrderController.h"
#import "CCMyOrderCell.h"
#import "CCShareDetailController.h"
#import "CCPostOederModel.h"
#import "RecordViewController.h"
#import "BRPickerView.h"
static NSString *CellId = @"orderCell";
@interface CCMyPostOrderController ()<UITableViewDataSource,UITableViewDelegate,CCMyOrderCellDelegate,UITextFieldDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CCEmptyView *emptyView;
@property (nonatomic, strong) UIView *deliveryView;
@property (nonatomic, strong) NSString *choosedCompnay;
@property (nonatomic, strong) NSString *deliveryNum;
@property (nonatomic, strong) NSArray *companyArray;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) NSString *deliveryOrderNum;
@property (nonatomic, strong) UIButton *chooseBtn;

@end

@implementation CCMyPostOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self initRefreshView];
    [self.tableView.mj_header beginRefreshing];
}
- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
}
- (void)setUI {
    [self addTitle: self.title ?: @"订购列表"];
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [NOTICENTER addObserver:self selector:@selector(refreshData) name:@"myShareChange" object:nil];
    
    if (!self.title) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
        rightBtn.titleLabel.font = FFont(14);
        [rightBtn addTarget:self action:@selector(rightBtn) forControlEvents:UIControlEventTouchUpInside];
        [rightBtn setTitle:@"收支记录" forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];

    }
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/byspidgetorders") params:@{@"sp_id":STR(self.sp_id),@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"data"][@"info"]) {
                [self.dataArray addObject:[CCPostOederModel modelWithJSON:dict]];
            }
            [self endLoding:success[@"data"][@"info"]];
        }else{
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
    //快递公司
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/courier_companys") params:nil target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            NSArray *array = success[@"data"][@"info"];
            NSMutableArray *nameArray = [NSMutableArray array];
            NSMutableArray *codeArray = [NSMutableArray array];
            for (NSDictionary *dict in array) {
                [nameArray addObject:dict[@"name"]];
                [codeArray addObject:dict[@"code"]];
            }
            self.companyArray = @[nameArray,codeArray];
        }else{
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO : YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    if (self.dataArray.count > indexPath.row) {
        cell.delegate = self;
        CCPostOederModel *model = self.dataArray[indexPath.row];
        cell.orderNum = model.order_num;
        cell.postOrderModel = model;
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCShareDetailController *vc = [[CCShareDetailController alloc]init];
        CCPostOederModel *model = self.dataArray[indexPath.row];
        vc.shareId = model.pinfo.sp_id;
        vc.isSelf = [model.pinfo.sp_uid isEqualToString:USERID];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
//确认发货
- (void)CCMyOrderCellPayBtnClickWithOrderId:(NSString *)orderNum {
    self.deliveryOrderNum = orderNum;
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:self.deliveryView inView:self.view];
}
- (void)rightBtn {
    RecordViewController *vc = [[RecordViewController alloc]init];
    vc.recordType = RecordShare;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 115;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyOrderCell" bundle:nil] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}
#pragma mark - 发货
- (UIView *)deliveryView {
    if (!_deliveryView) {
        _deliveryView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 200)];
        _deliveryView.backgroundColor = WHITECOLOR;
        _deliveryView.layer.cornerRadius = 4;
        _deliveryView.layer.masksToBounds = YES;
        
        UILabel *nameLbl = [[UILabel alloc]initWithText:@"发货" font:17 textColor:TEXTBLACK3];
        [_deliveryView addSubview:nameLbl];
        nameLbl.textAlignment = NSTextAlignmentCenter;
        nameLbl.frame = CGRectMake(0, 20, 250, 20);
        
        UIButton *chooseBtn = [[UIButton alloc]initWithTitle:@"点击选择快递公司" textColor:TEXTBLACK6 backImg:nil font:14];
        [_deliveryView addSubview:chooseBtn];
        chooseBtn.frame = CGRectMake(0, 45, 250, 40);
        [chooseBtn addTarget:self action:@selector(chooseCompany) forControlEvents:UIControlEventTouchUpInside];
        
        UITextField *field = [[UITextField alloc]initWithFrame:CGRectMake(15, 95, 220, 40)];
        field.placeholder  = @"请输入快递单号";
        [_deliveryView addSubview:field];
        field.delegate = self;
        field.borderStyle = UITextBorderStyleRoundedRect;
        
        UIButton *sendBtn = [[UIButton alloc]initWithTitle:@"发货" textColor:WHITECOLOR backImg:nil font:14];
        [sendBtn setBackgroundColor:[UIColor orangeColor]];
        sendBtn.layer.cornerRadius = 4;
        sendBtn.layer.masksToBounds = YES;
        [_deliveryView addSubview:sendBtn];
        self.chooseBtn = chooseBtn;
        [sendBtn addTarget:self action:@selector(sendBtnClick) forControlEvents:UIControlEventTouchUpInside];
        sendBtn.frame = CGRectMake(15, 150, 220, 40);
    }
    return _deliveryView;
}
- (void)chooseCompany{
    [BRStringPickerView showStringPickerWithTitle:@"选择快递公司" dataSource:self.companyArray[0] defaultSelValue:self.companyArray[0][0] isAutoSelect:YES resultBlock:^(id selectValue) {
        NSInteger index = [self.companyArray[0] indexOfObject:selectValue];
        self.choosedCompnay = [self.companyArray[1] objectAtIndex:index];
        [self.chooseBtn setTitle:self.companyArray[0][index] forState:UIControlStateNormal];
    }];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    self.deliveryNum = textField.text;
    return YES;
}
//发货
- (void)sendBtnClick {
    if ([TOOL stringEmpty:self.deliveryNum]) {
        SHOW(@"请输入快递单号");
        return;
    }
    if (!self.choosedCompnay) {
        SHOW(@"请选择快递公司");
        return;
    }
    for (int i = 0; i<self.dataArray.count; i++) {
        CCPostOederModel *model = self.dataArray[i];
        if ([model.order_num isEqualToString:self.deliveryOrderNum]) {
            if ([model.order_stat isEqualToString:@"1"]) {
                [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/comfirm_pay") params:@{@"order_num":self.deliveryOrderNum,@"order_stat":@"3",@"courier_company":self.choosedCompnay,@"courier_number":self.deliveryNum} target:self success:^(NSDictionary *success) {
                    if ([success[@"data"][@"stat"] integerValue]  == 1) {
                        SHOW(@"发货成功");
                        CCMyOrderCell *cell = (CCMyOrderCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                        cell.payBtnText = @"已发货";
                        [self.animateModel closeWithStraight];
                        [self refreshData];
                    }else{
                        SHOW(success[@"data"][@"info"]);
                    }
                } failure:^(NSError *failure) {
                    
                }];
                break;
            }
        }
    }
}
@end
