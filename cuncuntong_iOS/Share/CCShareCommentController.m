//
//  CCShareCommentController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareCommentController.h"
#import "CCShareDetailController.h"
@interface CCShareCommentController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *goodLbl;
@property (weak, nonatomic) IBOutlet UILabel *normalLbl;
@property (weak, nonatomic) IBOutlet UILabel *badLbl;
@property (weak, nonatomic) IBOutlet UIButton *goodBtn;
@property (weak, nonatomic) IBOutlet UIButton *normalBtn;
@property (weak, nonatomic) IBOutlet UIButton *badBtn;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLbl;

@end

@implementation CCShareCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@"评论"];
    self.textView.delegate = self;
    [self.goodBtn addTarget:self action:@selector(goodBtnClick) forControlEvents:UIControlEventTouchUpInside];
     [self.normalBtn addTarget:self action:@selector(normalBtnClick) forControlEvents:UIControlEventTouchUpInside];
     [self.badBtn addTarget:self action:@selector(badBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)normalBtnClick {
    self.normalBtn.selected = YES;
    self.goodBtn.selected = NO;
    self.badBtn.selected = NO;
}
- (void)goodBtnClick {
    self.normalBtn.selected = NO;
    self.goodBtn.selected = YES;
    self.badBtn.selected = NO;
}
- (void)badBtnClick {
    self.normalBtn.selected = NO;
    self.goodBtn.selected = NO;
    self.badBtn.selected = YES;
}
- (void)textViewDidChange:(UITextView *)textView {
    self.placeHolderLbl.hidden = textView.text.length != 0;
}
- (void)submitBtnClick {
    if (self.textView.text.length == 0) {
        SHOW(@"请输入评论");
        return;
    }
    NSString *comment = self.goodBtn.selected ? @"1" : self.normalBtn.selected ? @"2" : @"3";
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/publish_pj") params:@{@"order_num":STR(self.orderNum),@"pj":comment,@"order_pl":self.textView.text} target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            SHOW(@"评论成功");
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
    }];
}
@end
