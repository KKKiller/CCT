//
//  CCShareDetailModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/4.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCShareDetailModel : NSObject
@property (nonatomic, strong) NSString *sp_id;
@property (nonatomic, strong) NSString *sp_uid;
@property (nonatomic, strong) NSString *sp_fbd;
@property (nonatomic, strong) NSString *sp_title;
@property (nonatomic, strong) NSString *sp_sign;
@property (nonatomic, strong) NSString *sp_content;
@property (nonatomic, strong) NSString *sp_pricing;
@property (nonatomic, strong) NSString *sp_phone;
@property (nonatomic, strong) NSString *sp_stat;
@property (nonatomic, strong) NSString *sp_credit;
@property (nonatomic, strong) NSString *sp_pic;
@property (nonatomic, strong) NSString *sp_saleCount;
@property (nonatomic, assign) NSTimeInterval sp_addtime;
@property (nonatomic, assign) NSTimeInterval refresh_time;
@property (nonatomic, strong) NSArray<NSString*> *sp_imgs;

@property (nonatomic, assign) BOOL is_collection;

@property (nonatomic, strong) NSString *sp_area;
@property (nonatomic, strong) NSString *sp_city;
@property (nonatomic, strong) NSString *sp_jie;
@property (nonatomic, strong) NSString *sp_province;
@property (nonatomic, assign) BOOL sp_tj;
@property (nonatomic, strong) NSString *sp_zhen;

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *mobile;

@property (nonatomic, assign) NSInteger del_stat;
@property (nonatomic, assign) CGFloat distance;

@property (nonatomic, assign) NSInteger share_state;
@property (nonatomic, strong) NSString *x_axis;
@property (nonatomic, strong) NSString *y_axis;
@property (nonatomic, strong) NSString *location_info;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *frequency;
//@property (nonatomic, assign) BOOL top;//置顶


@end
