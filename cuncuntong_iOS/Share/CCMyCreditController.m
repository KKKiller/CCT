//
//  CCMyCreditController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/24.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyCreditController.h"
#import "CCCreditEnhanceController.h"
#import "CCCreditPayController.h"
#import "CCApplyManagerController.h"
#import "CrowdfundingViewController.h"
#import "CCIdVerifyController.h"
#import "CCCreditEnhanceController.h"
#import "CCCreditRankController.h"
#import "CCRankModel.h"
#import "UICountingLabel.h"
#import "CCMyStockController.h"
@interface CCMyCreditController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *moreBtn;
@property (weak, nonatomic) IBOutlet UICountingLabel *myCreditLbl;
@property (weak, nonatomic) IBOutlet UILabel *navTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *indicatorImgView;

@property (weak, nonatomic) IBOutlet UIButton *idVerifyBtn;
@property (weak, nonatomic) IBOutlet UIButton *investmentBtn;
//@property (weak, nonatomic) IBOutlet UIButton *applyShezhang;
//@property (weak, nonatomic) IBOutlet UIButton *applyManagerBtn;
@property (weak, nonatomic) IBOutlet UIButton *payDepositBtn;

@property (weak, nonatomic) IBOutlet UIButton *showRankListBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *margin2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *margin1;

@property (nonatomic, strong) NSMutableArray *rankListArray;
@end

@implementation CCMyCreditController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self setTitle:@"我的信用"];
    self.rankListArray = [NSMutableArray array];
    self.timeLbl.text = [self getCurrentTimes];
    [self loadRankListData];
    self.myCreditLbl.format = @"%d";
    [self indicatorAnimationWithCredit:[UserManager sharedInstance].credit];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSInteger num = [[UserManager sharedInstance].credit integerValue];
    [self.myCreditLbl countFrom:0 to:num withDuration:3];
}
- (void)setUI {
    [self.idVerifyBtn addTarget:self action:@selector(idVerifyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.payDepositBtn addTarget:self action:@selector(payDepositBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
//    [self.applyManagerBtn addTarget:self action:@selector(replyManagerBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.applyShezhang addTarget:self action:@selector(applyShezhangClick) forControlEvents:UIControlEventTouchUpInside];
    [self.investmentBtn addTarget:self action:@selector(investmentBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.showRankListBtn addTarget:self action:@selector(rankList) forControlEvents:UIControlEventTouchUpInside];
    CGFloat margin = (App_Width - 40 * 5 - 30) / 4;
    self.margin1.constant = margin;
    self.margin2.constant = margin;
    self.indicatorImgView.layer.anchorPoint = CGPointMake(0.8, 0.5);
    self.scrollView.delegate = self;
}

- (void)loadRankListData {
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/creditranking") params:nil target:self success:^(NSDictionary *success) {
        for (NSDictionary *dict in success[@"data"]) {
            CCRankModel *model = [CCRankModel modelWithJSON:dict];
            [self.rankListArray addObject:model];
        }
        [self configRank];
    } failure:^(NSError *failure) {
    }];
}

//身份认证
- (void)idVerifyBtnClick {
    //已认证
    if ([UserManager sharedInstance].is_smrz) {
        CCCreditEnhanceController *vc = [[CCCreditEnhanceController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{//未认证
        CCIdVerifyController *vc = [[CCIdVerifyController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}
//缴纳押金
- (void)payDepositBtnClick {
    CCCreditPayController *vc = [[CCCreditPayController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
//申请管理
- (void)replyManagerBtnClick {
    CCApplyManagerController *vc = [[CCApplyManagerController alloc]init];
    self.navigationController.navigationBarHidden = NO;
    vc.titleStr = @"申请管理员";
    vc.urlStr =  [NSString stringWithFormat:@"http://www.cct369.com/village/public/misc/zadmin_reg?rid=%@",USERID];
    [self.navigationController pushViewController:vc animated:YES];
}
//申请社长
- (void)applyShezhangClick {
    CCApplyManagerController *vc = [[CCApplyManagerController alloc]init];
    self.navigationController.navigationBarHidden = NO;
    vc.titleStr = @"申请管理员";
    vc.urlStr =  [NSString stringWithFormat:@"http://www.cct369.com/village/public/misc/zadmin_reg?rid=%@",USERID];
    [self.navigationController pushViewController:vc animated:YES];
}
//投资股票
- (void)investmentBtnClick {
    self.navigationController.navigationBarHidden = NO;
//    CrowdfundingViewController *vc = [[CrowdfundingViewController alloc]init];
    CCMyStockController *vc = [[CCMyStockController alloc]init];
    vc.isProfileStock = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)rankList {
    CCCreditRankController *vc = [[CCCreditRankController alloc]init];
    vc.rankListArray = self.rankListArray;
    [self.navigationController pushViewController:vc animated:YES];
}
//设置排行榜数据
- (void)configRank {
    for (int i = 1; i<=6; i++) {
        if (self.rankListArray.count > i-1) {
            CCRankModel *model = self.rankListArray[i-1];
            for (UIView *v in self.scrollView.subviews) {
                if (v.tag == 100 + i) {
                    UIImageView *img = (UIImageView *)v;
                    [img setImageWithURL:URL(model.portrait) placeholder:IMAGENAMED(@"head")];
                }
                if (v.tag == 200 + i) {
                    UILabel *lbl = (UILabel *)v;
                    if (model.realname.length == 11) {
                        model.realname = [model.realname stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
                    }
                    lbl.text = model.realname;
                }
                if (v.tag == 300 + i) {
                    UILabel *lbl = (UILabel *)v;
                    lbl.text = [NSString stringWithFormat:@" %@ ",model.credit];
                }
            }
        }
    }
}
- (NSString*)getCurrentTimes{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];// HH:mm:ss
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return [NSString stringWithFormat:@"评估时间: %@",currentTimeString];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.navTitleLbl.hidden = scrollView.contentOffset.y > 200;
}
- (void)indicatorAnimationWithCredit:(NSString *)creditStr {
    CGFloat credit = [creditStr floatValue];
    NSInteger num = 0;
    if (credit == 0) {
        num = 0;
    }else if (credit < 100) {
        num = 1;
    }else if(credit < 1000){
        num = credit / 100;
    }else if (credit < 5000){
        num = 10;
    }else if (credit < 10000){
        num = 11;
    }else if (credit < 50000){
        num = 12;
    }else if (credit < 100000){
        num = 13;
    }else if (credit < 500000){
        num = 14;
    }else if (credit < 1000000){
        num = 15;
    }else if (credit < 5000000){
        num = 16;
    }else if (credit < 10000000){
        num = 17;
    }else{
        num = 18;
    }
    
    
    CGAffineTransform transform;
    transform = CGAffineTransformRotate(self.indicatorImgView.transform,M_PI/20.0 * num);
    [UIView beginAnimations:@"rotate" context:nil ];
    [UIView setAnimationDuration:3];
    [UIView setAnimationDelegate:self];
    [self.indicatorImgView setTransform:transform];
    [UIView commitAnimations];
}
@end
