//
//  CCShareHomeController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareHomeController.h"
#import "CCShareHomeCell.h"
#import "CCShareHomeHeaderView.h"
#import "MYHomeController.h"
#import "CCWineTabbarController.h"
#import "CCChatTabbarController.h"
#import "CCHobbyModel.h"
#import "CCShareDetailController.h"
static NSString *CellId = @"CCShareHomeCell";
@interface CCShareHomeController ()<UITableViewDataSource,UITableViewDelegate,CCShareHomeHeaderViewDelegate,UISearchBarDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic, strong) CCShareHomeHeaderView *headerView;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) NSMutableArray *hobbyArray;
@property (nonatomic, strong) CCSubHobbyModel *selectedSubHobbyModel;
@property (nonatomic, strong) NSString *empty;
@end

@implementation CCShareHomeController

-(void)dealloc{
    NSLog(@"销毁了");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitle:@""];
    self.empty = @"0";
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [self initRefreshView];
    [self loadHobby];
}
- (void)refreshData {
    self.pageIndex = 1;
    self.empty = @"";
    [self.dataArray removeAllObjects];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);

}
#pragma mark - 获取数据
- (void)loadData {
    NSMutableDictionary *param;
    param = [[NSMutableDictionary alloc]initWithDictionary:@{@"userid":USERID,@"x_axis":App_Delegate.lat,@"y_axis":App_Delegate.lng,@"page":@(self.pageIndex),@"size":@"20",@"empty":self.empty}];
    
    if (!STRINGEMPTY(self.keyword) && [self gettypeStr].length == 0) {
        [param setValue:self.keyword forKey:@"kw"];
    }
    if ([self gettypeStr].length > 0){
        [param setValue:[self gettypeStr] forKey:@"type1"];
    }
    if (self.selectedSubHobbyModel) {
        [param setValue:self.selectedSubHobbyModel.tid forKey:@"type2"];
    }
    [TOOL showLoading:self.tableView];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharepublish_list") params:param target:nil success:^(NSDictionary *success) {
        if (success[@"data"] && [success[@"data"] isKindOfClass:[NSArray class]]) {
            
            for (NSDictionary *dict in success[@"data"]) {
                CCShareDetailModel *model = [CCShareDetailModel modelWithJSON:dict];
                [self.dataArray addObject:model];
            }
            self.empty = [NSString stringWithFormat:@"%@", success[@"empty"]];
            [self endLoding:self.dataArray];
        }else{
            [self endLoding:nil];
        }
        [TOOL hideLoading:self.tableView];
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.tableView];
    }];
}

- (void)loadHobby{
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharelist") params:@{@"userid":USERID,@"new":@"2"} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [TOOL saveDataWithData:success Name:@"hobbys"];
            [self.hobbyArray removeAllObjects];
            for (NSDictionary *dict in success[@"data"]) {
                CCHobbyModel *model = [CCHobbyModel modelWithJSON:dict];
                [self.hobbyArray addObject:model];
            }
            self.headerView.hobbyArray = self.hobbyArray;
            App_Delegate.shareTabbarVc.hobbyArray = self.hobbyArray;
            [self.tableView reloadData];
            [self loadData];
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCShareHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    if (self.dataArray.count > indexPath.row) {
        CCShareDetailModel *model = self.dataArray[indexPath.row];
        cell.model = model;
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CCShareDetailModel *model = self.dataArray[indexPath.row];
    CCShareDetailController *vc = [[CCShareDetailController alloc]init];
    vc.shareId = model.sp_id;
    vc.fromHomeList = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchWithText:searchBar.text];
}
- (void)searchWithText:(NSString *)text {
    self.keyword = text;
    [self clearHobby];
    [self refreshData];
    [self.view endEditing:YES];
}
- (void)baiduWithText:(NSString *)text {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    NSString *url = [NSString stringWithFormat:@"https://www.baidu.com/s?wd=%@",text];
    vc.urlStr = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    vc.titleStr = @"百度搜索";
    [self.view endEditing:YES];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)clickTopBtnAtIndex:(NSInteger)index {
    UIViewController *vc;
    switch (index) {
        case 0://种金豆
            vc = [[MYHomeController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 1://酒老大
            vc = [[CCWineTabbarController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 2://货运宝
//            SHOW(@"玩命开发中...")
            vc = [[CCTransportTabbarController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 3://聊聊
            vc = [[CCChatTabbarController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 4://3+N
            vc = [[CCWineTabbarController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 5://3+N
            SHOW(@"玩命开发中...");
            break;
        default:
            break;
    }
}
//点击一级目录
- (void)clickHobbyAtIndex:(NSInteger)index {
    self.selectedSubHobbyModel = nil;
    self.headerView.height = 138 + 20;
    NSInteger hobbyCount = 0;
    for (CCHobbyModel *model in self.hobbyArray) {
        if (model.user_state == 1) {
            hobbyCount++;
        }
    }
    for (int i = 0; i<self.hobbyArray.count; i++) {
        CCHobbyModel *model = self.hobbyArray[i];
        if (hobbyCount > 0 && i != index) {
            model.user_state = NO;
            if (index == i) {
                model.user_state = YES;
            }
        }else{
            if (index == i) {
                model.user_state = !model.user_state;
            }
        }
    }
    self.headerView.hobbyArray = self.hobbyArray;
    [self refreshData];
}
//点击二级目录
- (void)clickSubHobbyAtIndex:(NSInteger)index hobbyIndex:(NSInteger)hobbyIndex{
    if (index == -1) {
        self.selectedSubHobbyModel = nil;
    }else{
        CCHobbyModel *model = self.hobbyArray[hobbyIndex];
        CCSubHobbyModel *subModel = model.find[index];
        self.selectedSubHobbyModel = subModel;
    }
    [self refreshData];
}
- (NSString *)gettypeStr{
    NSString *type = @"";
    for (CCHobbyModel *model in self.hobbyArray) {
        if (model.user_state == 1) {
            if ([type isEqualToString:@""]) {
                type = [type stringByAppendingString:model.tid];
            }else{
               type = [type stringByAppendingString:[NSString stringWithFormat:@",%@",model.tid]];
            }
        }
    }
    return type;
}
- (void)clearHobby {
    for (CCHobbyModel *model in self.hobbyArray) {
        model.user_state = NO;
    }
    self.headerView.hobbyArray = self.hobbyArray;
}
-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat y = kSystemVersion >= 11.0 || self.keyword ||![App_Delegate.defaultHome isEqualToString:@"default"] ? 64 : 0;
        CGFloat tab = self.keyword ? 0 : 49;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, y, App_Width, App_Height - tab - y)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 130;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableHeaderView = self.headerView;
        [_tableView registerNib:[UINib nibWithNibName:@"CCShareHomeCell" bundle:nil] forCellReuseIdentifier:CellId];
#ifdef __IPHONE_11_0
        if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
#endif
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CCShareHomeHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[CCShareHomeHeaderView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 138)];
        _headerView.delegate = self;
        _headerView.searchBar.delegate = self;
        if (self.keyword) {
            _headerView.searchBar.text = self.keyword;
        }
    }
    return _headerView;
}
- (NSMutableArray *)hobbyArray {
    if (!_hobbyArray) {
        _hobbyArray = [[NSMutableArray alloc]init];
    }
    return _hobbyArray;
}

@end
