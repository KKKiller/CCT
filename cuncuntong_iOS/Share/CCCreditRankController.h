//
//  CCCreditRankController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCCreditRankController : BaseViewController
@property (nonatomic, strong) NSMutableArray *rankListArray;
@end
