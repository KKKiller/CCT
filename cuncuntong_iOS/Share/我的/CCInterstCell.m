//
//  CCInterstCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCInterstCell.h"

@implementation CCInterstCell
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}
- (void)setUI {
    self.btn = [[UIButton alloc]initWithFrame:CGRectMake((self.width - 60)*0.5, 5, 60, 25)];
    [self.contentView addSubview:self.btn];
    self.btn.layer.cornerRadius = 12.5;
    self.btn.layer.masksToBounds = YES;
    self.btn.layer.borderWidth = 0.5;
    self.btn.layer.borderColor = MAINBLUE.CGColor;
    [self.btn setTitleColor:WHITECOLOR forState:UIControlStateSelected];
    [self.btn setTitleColor:MAINBLUE forState:UIControlStateNormal];
    
//    [self.btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.btn.titleLabel.font = FFont(13);
    self.btn.userInteractionEnabled = NO;
}
- (void)setModel:(CCHobbyModel *)model {
    _model = model;
    [self.btn setTitle:model.tname forState:UIControlStateNormal];
    self.btn.selected = model.user_state;
    if (model.user_state) {
        self.btn.backgroundColor = MAINBLUE;
    }else{
        self.btn.backgroundColor = WHITECOLOR;
    }
}
//- (void)btnClick:(UIButton *)sender {
//    sender.selected = !sender.selected;
//}
@end
