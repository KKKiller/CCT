//
//  CCMyFooter.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyFooter.h"
#import "CCInterstCell.h"
#import "CCHobbyModel.h"
@interface CCMyFooter()<UICollectionViewDelegate,UICollectionViewDataSource>

@end
@implementation CCMyFooter

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame: frame]) {
        self.backgroundColor = WHITECOLOR;
//        NSArray *array = @[@"服装",@"美食",@"招聘",@"房产",@"交友",@"车辆",@"宾馆",@"其他"];
        _dataArray = [NSMutableArray array];
//        for (NSString *str in array) {
//            CCHobbyModel *Model = [[CCHobbyModel alloc]init];
//            Model.tname = str;
//            [_dataArray addObject:Model];
//        }
        [self setUI];
        [self layoutSubView];
        
    }
    return self;
}

- (void)addHobby:(CCHobbyModel *)model {
    NSInteger index = 0;
    for (CCHobbyModel *model in self.dataArray) {
        if (model.user_state == 1) {
            index++;
            if (index == 3) {
                [self removeBobby:model];
            }
        }
    }
    model.user_state = YES;
    [self.collectionView reloadData];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/addclasshobby") params:@{@"userid":USERID,@"type_id":STR(model.tid)} target:nil success:^(NSDictionary *success) {
    } failure:^(NSError *failure) {
        
    }];
}
- (void)removeBobby:(CCHobbyModel *)model {
    model.user_state = NO;
    [self.collectionView reloadData];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/deleteclasshobby") params:@{@"userid":USERID,@"type_id":model.tid} target:nil success:^(NSDictionary *success) {
    } failure:^(NSError *failure) {
        
    }];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
}
- (void)setUI {
    self.interestLbl = [[UILabel alloc]init];
    self.interestLbl.text = @"兴趣设置（最多设置三类）";
    [self addSubview:self.interestLbl];
    self.interestLbl.font = FFont(15);
    
    self.lineLbl = [[UILabel alloc]init];
    self.lineLbl.backgroundColor = BACKGRAY;
    [self addSubview:self.lineLbl];
    
    [self addSubview:self.collectionView];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CCInterstCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CCInterstCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CCInterstCell *cell = (CCInterstCell *)[collectionView cellForItemAtIndexPath:indexPath];
    CCHobbyModel *model = self.dataArray[indexPath.row];
    if (model.user_state) {
        [self removeBobby:model];
    }else{
        [self addHobby:model];
    }
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CGFloat width =  (App_Width ) / 4.0;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(width, 35);
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame: CGRectMake(0, 45, self.width, self.height - 45) collectionViewLayout:layout];
        collectionView.backgroundColor = WHITECOLOR;
        _collectionView = collectionView;
//        _collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        [_collectionView registerClass:[CCInterstCell class] forCellWithReuseIdentifier:@"CCInterstCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}
- (void)layoutSubView {
    [self.interestLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.top.equalTo(self.mas_top).offset(10);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(1);
        make.top.equalTo(self.mas_top).offset(39);
    }];
    
}
@end
