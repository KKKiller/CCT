//
//  CCMyCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyCell.h"

@implementation CCMyCell

- (void)awakeFromNib {
    [super awakeFromNib];

    
    self.deleteBtn.layer.cornerRadius = 6;
    self.deleteBtn.layer.masksToBounds = YES;
    self.deleteBtn.layer.borderColor = [UIColor redColor].CGColor;
    self.deleteBtn.layer.borderWidth = 0.5;
    
    [self.deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)deleteBtnClick {
    if ([self.delegate respondsToSelector:@selector(deleteBtnClick:)]) {
        [self.delegate deleteBtnClick:self.model];
    }
}
- (void)setModel:(CCAddressModel *)model {
    _model = model;
    self.addressLbl.text = [NSString stringWithFormat:@"详细地址：%@",STR(model.location)];
    self.phoneLbl.text = [NSString stringWithFormat:@"联系电话：%@",STR(model.phone)];
    self.addressName.text = [NSString stringWithFormat:@"地址名称：%@",STR(model.name)];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
