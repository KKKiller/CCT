//
//  CCMyFooter.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCMyFooter : UIView
@property (nonatomic, strong) UILabel *interestLbl;
@property (nonatomic, strong) UILabel *lineLbl;
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *dataArray;
@end
