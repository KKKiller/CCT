//
//  CCMyHeader.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyHeader.h"

@implementation CCMyHeader

+ (instancetype)instanceView {
    CCMyHeader *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.saveBtn.layer.cornerRadius = 4;
    view.saveBtn.layer.masksToBounds = YES;
    return view;
}
- (void)setModel:(CCAddressModel *)model {
    _model = model;
    self.addressField.text = model.location;
    self.addressNameField.text = model.name;
}
@end
