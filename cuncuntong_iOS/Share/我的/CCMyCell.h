//
//  CCMyCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCAddressModel.h"
@protocol CCMyCellDelegate<NSObject>
- (void)deleteBtnClick:(CCAddressModel *)model;
@end

@interface CCMyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressName;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (nonatomic, strong) CCAddressModel *model;

@property (nonatomic, weak) id delegate;
@end
