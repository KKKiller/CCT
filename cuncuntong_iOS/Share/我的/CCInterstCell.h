//
//  CCInterstCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCHobbyModel.h"
@interface CCInterstCell : UICollectionViewCell
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) CCHobbyModel *model;
@end
