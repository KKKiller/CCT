//
//  CCShareMyController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCAddressModel.h"
typedef void(^selectAddressBlock)(CCAddressModel *model);
@interface CCShareMyController : BaseViewController
@property (nonatomic, assign) BOOL hideFooter;
@property (nonatomic, strong) selectAddressBlock selectAddressBlock;

@property (nonatomic,strong)NSString *addressType;//0：女儿国调用的地址   1：生活圈
@end
