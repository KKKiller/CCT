//
//  CCCreditRankController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCCreditRankController.h"
#import "CCEmptyView.h"
#import "CCCreditRankCell.h"
#import "CCRankModel.h"
static NSString *cellID = @"cellId";
@interface CCCreditRankController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, assign) NSInteger pageIndex;

@end
@implementation CCCreditRankController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addTitle:@"排行榜"];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.rankListArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCCreditRankCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.rankLbl.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
    if (self.rankListArray.count > indexPath.row) {
        CCRankModel *model =self.rankListArray[indexPath.row];
        cell.model = model;
    }
    return cell;
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.rankListArray.count > indexPath.row) {
        
    }
}


#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height-64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 40;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CCCreditRankCell class] forCellReuseIdentifier:cellID];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
//- (NSMutableArray *)dataArray {
//    if (_dataArray == nil) {
//        _dataArray = [[NSMutableArray alloc]init];
//    }
//    return _dataArray;
//}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
