//
//  CCShareHomeHeaderTypeCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCHobbyModel.h"
@interface CCShareHomeHeaderTypeCell : UICollectionViewCell
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) CCHobbyModel *model;
@property (nonatomic, strong) CCSubHobbyModel *subModel;
@end
