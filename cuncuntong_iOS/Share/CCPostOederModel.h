//
//  CCPostOederModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/18.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCShareDetailModel.h"
@interface CCPostOederModel : NSObject
@property (nonatomic, strong) NSString *orderid;
@property (nonatomic, strong) NSString *order_num;
@property (nonatomic, strong) NSString *order_cid;
@property (nonatomic, strong) NSString *order_user;
@property (nonatomic, strong) NSString *order_phone;
@property (nonatomic, strong) NSString *order_address;
@property (nonatomic, strong) NSString *order_name;
@property (nonatomic, strong) NSString *order_stat;
@property (nonatomic, strong) NSString *order_count;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) CCShareDetailModel *pinfo;

@end
//": 27,
//"order_num": "2017071712462477574",
//"transaction_id": "",
//"": 27,
//"": 1,
//"": "北京市县发阿里了",
//"order_name": "哈咯了",
//"order_phone": "1253425844",
//"order_count": 3,
//"order_total": 3,
//"order_pl": "不咋滴",
//"order_pltime": "1500266874",
//"order_plzj": null,
//"order_good_pj": null,
//"order_middle_pj": 1,
//"order_bad_pj": null,
//"order_stat": 2,
//"pay_type": 2,
//"order_addtime": "1500266784",
//"complete_time": "",
//
//-"uinfo": {
//    "id": 27,
//    "mobile": "18039290408",
//    "passwd": "E10ADC3949BA59ABBE56E057F20F883E",
//    "realname": "的卡",
//    "portrait": "http://www.cct369.com/village/public/images/b101dea2578f7770c680879f97530a66.jpeg",
//    "location": 380518,
//    "point": 430,
//    "money": 30322,
//    "QQ": "",
//    "WB": "",
//    "WX": "",
//    "create": 1475506727,
//    "secret": "0F6DDF4B2F831D868DA7230E01F47E20",
//    "redpoint": 0,
//    "syspoint": 0,
//    "actpoint": 0,
//    "inviter_id": 0,
//    "inviter_ids": "",
//    "invite_num": 1210,
//    "openid": "",
//    "isbind": 0,
//    "address": "",
//    "stock": 2,
//    "stock_jiu": 3,
//    "stock_member": 0,
//    "curr_month": 20170620,
//    "tx_num": 14,
//    "mobilereg": 0,
//    "sign_money": 0,
//    "credit": 526,
//    "is_smrz": 0,
//    "yajin": 10,
//    "yajin_time": "1500264846"
//}
//},
