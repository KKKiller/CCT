//
//  CCShareDetailController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareDetailController.h"
#import "CCShareDetailHeaderView.h"
#import "CCShareDetailBottomView.h"
#import "CCShareCommentCell.h"
#import "YYPhotoGroupView.h"
#import "PBViewController.h"
#import "CCPostShareController.h"
#import "CCShareBuyController.h"
#import "ListView.h"
#import "CCShareCommentController.h"
#import "CCShareDetailModel.h"
#import "IQKeyboardManager.h"
#import "CCShareLocationController.h"
#import "CCShareDetailFooterView.h"
#import "CCShareImgCell.h"
#import "UIImage+ImgSize.h"
static NSString *cellID = @"CCShareCommentCell";
static CGFloat normalHeaderheight = 500;

@interface CCShareDetailController ()<UITableViewDataSource,UITableViewDelegate,CCShareDetailHeaderDelegate,CCShareDetailBottomViewDelegate,PBViewControllerDataSource,PBViewControllerDelegate,ListDelegate>
@property (nonatomic, strong) CCShareDetailHeaderView *header;
//@property (nonatomic, strong) CCShareDetailBottomView *bottomView;
@property (nonatomic, strong) ListView *listView;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CCShareDetailFooterView *footerView;
@property (nonatomic, strong) NSMutableArray *dataArray; //网络获取数据
//@property (nonatomic, strong) CCEmptyView *emptyView;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) NSString *share_url;
@property (nonatomic, copy) NSString *mini;
@property (nonatomic, assign) BOOL isCollect;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSMutableArray<UIImage *> *imageDataArray;
@property (nonatomic, strong) CCShareDetailModel *shareModel;
@end


@implementation CCShareDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    self.tableView.tableHeaderView = self.header;
    [self loadData];
    [self loadCommentPercent];
    [NOTICENTER addObserver:self selector:@selector(loadData) name:@"ShareDetailChange" object:nil];
}

- (void)setUI {
    [self addTitle:@"共享详情"];
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
//    [self.view addSubview:self.bottomView];
    
    UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30 * KEY_RATE, 30 * KEY_RATE)];
    [detailButton setImage:[UIImage imageNamed:@"xialacaidan"] forState:UIControlStateNormal];
    UIBarButtonItem * detailButtonItem = [[UIBarButtonItem alloc] initWithCustomView:detailButton];
    
    App_Delegate.shareTabbarVc.navigationItem.rightBarButtonItem = detailButtonItem;
    detailButton.titleLabel.font = FontSize(12);
    [detailButton addTarget:self action:@selector(listMenu) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden= NO;
}
- (void)refreshData { //点击tabbar刷新
    self.pageIndex = 1;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 获取数据
- (void)loadData {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{@"sp_id":STR(self.shareId),@"uid":USERID,@"x_axis":App_Delegate.lat,@"y_axis":App_Delegate.lng}];
    if (self.fromHomeList && ![self.shareModel.userId isEqualToString:USERID]) {
        [param setValue:@"sharepublish_list" forKey:@"source"];
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/detailshare") params:param target:self success:^(NSDictionary *success) {
            self.shareModel = [CCShareDetailModel modelWithJSON:success[@"data"][@"info"]];
            self.header.model  = self.shareModel;
            self.footerView.visitCount = self.shareModel.frequency;
            self.imageArray = self.shareModel.sp_imgs;
            [self.dataArray addObjectsFromArray:self.shareModel.sp_imgs];
            if (self.dataArray.count > 0) {
                [self.dataArray removeObjectAtIndex:0];
            }
            self.isCollect = self.shareModel.is_collection;
            [self setImgsDataArray];
            self.share_url = [NSString stringWithFormat:@"%@village/public/share/sharedetail?sp_id=%@",App_Delegate.shareBaseUrl,self.shareModel.sp_id];
            [self.tableView reloadData];
            for (NSString *url in self.dataArray) {
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    [self getImageHeightWithUrl:url];
                });
            }
//        [self loadCommentData];

    } failure:^(NSError *failure) {
        
    }];
}

- (CGFloat)getImageHeightWithUrl:(NSString *)url {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:url]) {
        //这里拿到每个图片的尺寸，然后计算出每个cell的高度
        CGSize imageSize = [UIImage getImageSizeWithURL:url];
        CGFloat imgH = imageSize.height *( App_Width / imageSize.width) + 5;
        //将最终的自适应的高度 本地化处理
        [[NSUserDefaults standardUserDefaults] setObject:@(imgH) forKey:url];
    }
    NSNumber *height = [[NSUserDefaults standardUserDefaults] objectForKey:url];
    return [height floatValue];
}
- (void)loadCommentData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/comment_list") params:@{@"order_cid":STR(self.shareId),@"page":@(self.pageIndex)} target:self success:^(NSDictionary *success) {
        if([success[@"data"][@"stat"] integerValue] == 1){
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"data"][@"info"]) {
                CCCommentModel *model = [CCCommentModel modelWithJSON:dict];
                [self.dataArray addObject: model];
            }
            self.header.existComment = self.dataArray.count > 0;
            if (self.dataArray.count > 0) {
                self.header.height = self.header.height - 40;
                self.tableView.tableHeaderView = self.header;
            }
        }
        [self endLoding:success[@"data"][@"info"]];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
-  (void)loadCommentPercent {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/getcommentsum") params:@{@"goods_id":STR(self.shareId)} target:self success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSString *good = [NSString stringWithFormat:@"%@", success[@"data"][@"good"]];
            NSString *normal = [NSString stringWithFormat:@"%@", success[@"data"][@"good_in"]];
            NSString *bad = [NSString stringWithFormat:@"%@", success[@"data"][@"difference"]];
            [self.footerView configCommentPercentWithGood:good normal:normal bad:bad];
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)addCommentWithType:(NSString *)type {
   //1good 2normal 3bad
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharegrade") params:@{@"userid":USERID,@"shareid":STR(self.shareModel.sp_id),@"type":type} target:self success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"评价成功");
            [self loadCommentPercent];
        }else{
            SHOW(success[@"msg"]);
        }
    } failure:^(NSError *failure) {
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
//    self.emptyView.hidden = self.dataArray.count == 0 ? NO :YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
        self.tableView.contentInset = UIEdgeInsetsZero;
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCShareImgCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (self.dataArray.count > indexPath.row) {
        cell.url = self.dataArray[indexPath.row];
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PBViewController *pbViewController = [PBViewController new];
    pbViewController.pb_dataSource = self;
    pbViewController.pb_delegate = self;
    pbViewController.pb_startPage = indexPath.row;
    [self presentViewController:pbViewController animated:YES completion:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *url = self.dataArray[indexPath.row];
    return [self getImageHeightWithUrl:url];
}
#pragma mark - header-Delegate
//定位
- (void)shareDetailHeaderClickAtLocationBtn {
    CCShareLocationController *vc = [[CCShareLocationController alloc]init];
    vc.shareModel = self.shareModel;
    [self.navigationController pushViewController:vc animated:YES];
}
//查看更多
- (void)updateHeaderHeight:(CGFloat)height {
    self.header.height = height + normalHeaderheight;
    if (self.shareModel.sp_imgs.count == 0) {
        self.header.height = self.header.height -  200;
    }
    self.tableView.tableHeaderView = self.header;
}
//聊天 在线咨询
- (void)shareDetailHeaderClickAtOnlineChatBtn {
    [IQKeyboardManager sharedManager].enable = NO;
        RCConversationViewController *chat = [[RCConversationViewController alloc] initWithConversationType:1 targetId:self.shareModel.sp_uid];
        chat.title = @"在线咨询";
        [self.navigationController pushViewController:chat animated:YES];
}
//刷新
- (void)CCShareDetailBottomViewClickbuyAndRefreshBtn {
    if (self.isSelf) {
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/refresh_share") params:@{@"sp_id":STR(self.shareId)} target:self success:^(NSDictionary *success) {
            SHOW(@"刷新成功");
        } failure:^(NSError *failure) {
        }];
    }else{ //订购
        CCShareBuyController *vc = [[CCShareBuyController alloc]init];
        vc.price = [self.shareModel.sp_pricing floatValue];
        vc.shareId = self.shareId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
//修改
- (void)CCShareDetailBottomViewClickAtModifyBtn {
    CCPostShareController *vc = [[CCPostShareController alloc]init];
    vc.shareModel = self.shareModel;
//    vc.imageDataArray = self.imageDataArray;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void )setImgsDataArray {
    self.imageDataArray = [NSMutableArray array];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int i = 0; i<self.imageArray.count; i++) {
            NSData *dateImg = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.imageArray[i]]];
            UIImage *img = [UIImage imageWithData:dateImg];
            if (img) {
                [self.imageDataArray addObject:img];
            }
        }
    });
}
#pragma mark - 图片放大
- (void)shareDetailHeaderdidClickImageAtIndex:(NSInteger)index{
    PBViewController *pbViewController = [PBViewController new];
    pbViewController.pb_dataSource = self;
    pbViewController.pb_delegate = self;
    pbViewController.pb_startPage = index;
    [self presentViewController:pbViewController animated:YES completion:nil];
}
- (NSInteger)numberOfPagesInViewController:(PBViewController *)viewController {
    return self.dataArray.count;
}
- (void)viewController:(PBViewController *)viewController presentImageView:(YYAnimatedImageView *)imageView forPageAtIndex:(NSInteger)index progressHandler:(void (^)(NSInteger, NSInteger))progressHandler {
    [imageView setImageURL:URL(self.dataArray[index])];
}
- (void)viewController:(PBViewController *)viewController didSingleTapedPageAtIndex:(NSInteger)index presentedImage:(UIImage *)presentedImage {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 分享
- (void)listMenu {
    self.listView.hidden = !self.listView.hidden;
}
- (void)itemClick:(UIButton *)sender {
    switch (sender.tag - 3000) {
            case 0: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatSession];
            }
            break;
            case 1: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine];
            }
            break;
            case 2: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_QQ];
            }
            break;
            case 3: {
                [self shareWebPageToPlatformType:UMSocialPlatformType_Qzone];
            }
            break;
        case 4: {
            [self collection];
        }
            break;
        case 5: {
            [self complain];
        }
            break;
        default:
            break;
    }
}


- (void)collection {
    [[NSNotificationCenter defaultCenter]  postNotificationName:@"shareCollectChange" object:nil];
    [self.listView changeColStatus:_isCollect];
    if (_isCollect) {
        if (self.fromCollectList) {
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/cancel_collection") params:@{@"col_id":STR(self.collectId)} target:self success:^(NSDictionary *success) {
                if ([success[@"data"][@"stat"] integerValue] == 1) {
                    SHOW(@"取消成功");
                }else{
                    SHOW(success[@"data"][@"info"]);
                }
            } failure:^(NSError *failure) {
            }];
        }else{
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/otcancel_collection") params:@{@"col_sid":STR(self.shareId),@"col_uid":USERID} target:self success:^(NSDictionary *success) {
                if ([success[@"data"][@"stat"] integerValue] == 1) {
                    SHOW(@"取消成功");
                }else{
                    SHOW(success[@"data"][@"info"]);
                }
            } failure:^(NSError *failure) {
            }];
        }
    }else {
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/ck_collection") params:@{@"col_uid":USERID,@"col_sid":STR(self.shareId)} target:self success:^(NSDictionary *success) {
            if ([success[@"data"][@"stat"] integerValue] == 1) {
                SHOW(@"收藏成功");
            }else{
                SHOW(success[@"data"][@"info"]);
            }
        } failure:^(NSError *failure) {
        }];
        
    }
    _isCollect = !_isCollect;
}

- (void)complain {
    self.listView.hidden = YES;
    SHOW(@"举报成功");
}


- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    self.listView.hidden = YES;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UIImage *image = self.imageArray ? [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imageArray[0]]]] : IMAGENAMED(@"cct");
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: self.shareModel.sp_title  descr:self.shareModel.sp_content ?  self.shareModel.sp_content: @"——来自全球村村通APP" thumImage:image];
    //设置网页地址
    shareObject.webpageUrl = self.share_url;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/share_energy") params:@{@"userid":USERID} success:^(NSDictionary *success) {
                
            } failure:^(NSError *failure) {
                
            }];
        }
    }];
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height-64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 50;
        _tableView.tableFooterView = self.footerView;
        _tableView.tableHeaderView = self.header;
        _tableView.backgroundColor = WHITECOLOR;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCShareImgCell" bundle:nil]  forCellReuseIdentifier:cellID];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

//- (CCEmptyView *)emptyView {
//    if (_emptyView == nil) {
//        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
//        [_tableView addSubview:_emptyView];
//    }
//    return _emptyView;
//}
- (CCShareDetailHeaderView *)header {
    if (!_header) {
        _header = [CCShareDetailHeaderView instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, normalHeaderheight);
        _header.delegate = self;
        
        
    }
    return _header;
}
//- (CCShareDetailBottomView *)bottomView {
//    if (!_bottomView) {
//        _bottomView = [CCShareDetailBottomView instanceView];
//        _bottomView.frame = CGRectMake(0, App_Height - 60, App_Width, 60);
//        _bottomView.isSelf = self.isSelf;
//        _bottomView.delegate = self;
//    }
//    return _bottomView;
//}
- (CCShareDetailFooterView *)footerView {
    if (!_footerView) {
        _footerView = [CCShareDetailFooterView instanceView];
        _footerView.goodBtn.tag = 100;
        _footerView.normalBtn.tag = 200;
        _footerView.badBtn.tag = 300;

        [_footerView.goodBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_footerView.normalBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_footerView.badBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _footerView;
}
- (void)commentBtnClick:(UIButton *)sender {
    NSString *type = sender.tag == 100 ? @"h" : sender.tag == 200 ? @"z" : @"c";
    [self addCommentWithType:type];
}
- (ListView *)listView {
    
    if (!_listView) {
        _listView = [[ListView alloc] init];
        [_listView initColStatus:_isCollect];
        [self.view addSubview:_listView];
        [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _listView.hidden = YES;
        _listView.delegate = self;
        [_listView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                         initWithActionBlock:^(id  _Nonnull sender) {
                                             _listView.hidden = YES;
                                         }]];
        
    }
    return _listView;
}
@end
