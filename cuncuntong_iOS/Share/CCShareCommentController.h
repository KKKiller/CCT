//
//  CCShareCommentController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

/**
发布评论
 */
@interface CCShareCommentController : BaseViewController
@property (nonatomic, strong) NSString *orderNum;
@end
