//
//  CCMyShareHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/29.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareHeaderView.h"
#import "CCShareListController.h"
#import "CCCategoryModel.h"
#import "ZYBannerView.h"
static NSString *CellId = @"orderCell";
static NSString * const reuseIdentifierHeader = @"HeaderCell";
static NSString * const reuseIdentifierFooter = @"FooterCell";

const CGFloat shareTopImgHeight = 150;
const CGFloat shareTopBtnsHeight = 95;
const CGFloat shareHeaderHeight = 30;
const CGFloat shareFooterHeight = 20;
const CGFloat shareSectionHeight = 150;

@interface CCShareHeaderView() <UICollectionViewDelegate,UICollectionViewDataSource,ZYBannerViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) NSMutableArray *foldStateArray;
@end

@implementation CCShareHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = WHITECOLOR;
        self.foldStateArray = [NSMutableArray arrayWithArray: @[@YES,@YES,@YES]];
        [self addSubview:self.topImgView];
        [self addTopBtns];
        [self addSubview:self.collectionView];
        self.frame = CGRectMake(0, 0, App_Width, shareTopImgHeight + shareTopBtnsHeight + self.collectionView.height);
    }
    return self;
}
- (void)setCategoryArray:(NSMutableArray *)categoryArray {
    _categoryArray = categoryArray;
    [self.collectionView reloadData];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.categoryArray.count - 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.categoryArray) {
        if ([self.foldStateArray[section] isEqual:@YES] && [self.categoryArray[section + 1] count] >= 8) {
            return 8;
        }else{
            return [self.categoryArray[section + 1] count];
        }
    }
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CCMyShareCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellId forIndexPath:indexPath];
    cell.model = self.categoryArray[indexPath.section + 1][indexPath.row];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.categoryArray[indexPath.section + 1] count] > indexPath.row) {
        CCCategoryModel *model = self.categoryArray[indexPath.section + 1][indexPath.row];
        [self.delegate clickAtCategory:model.typeid title:model.tname];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{

    UICollectionReusableView *supplementaryView;

    if ([kind isEqualToString:UICollectionElementKindSectionHeader]){
        CCCollectionViewHeader *view = (CCCollectionViewHeader *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:reuseIdentifierHeader forIndexPath:indexPath];
        view.titleLbl.text = indexPath.section == 0 ? @"服务" : indexPath.section == 1 ? @"家政" : @"商务";
        supplementaryView = view;

    }
    else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        CCCollectionViewFooter *view = (CCCollectionViewFooter *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:reuseIdentifierFooter forIndexPath:indexPath];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) { //查看更多
            self.foldStateArray[indexPath.section] = @NO;
            self.collectionView.height = [self getSectionheight:0] + [self getSectionheight:1] + [self getSectionheight:2];
            self.height = shareTopImgHeight + shareTopBtnsHeight + self.collectionView.height;
            view.moreImgView.hidden = YES;
            [self.collectionView reloadData];
            [self.delegate myShareHeaderUpdateHeight];

        }];
        [view.moreImgView addGestureRecognizer:tap];
        view.moreImgView.hidden = ![self.foldStateArray[indexPath.section] isEqual:@YES];
        supplementaryView = view;
    }

    return supplementaryView;
}
// 设置Header的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(App_Width, shareHeaderHeight);
}

// 设置Footer的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(App_Width, shareFooterHeight);
}
#pragma mark - banner
- (NSInteger)numberOfItemsInBanner:(ZYBannerView *)banner {
    return 2;
}
- (UIView *)banner:(ZYBannerView *)banner viewForItemAtIndex:(NSInteger)index {
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, App_Width, shareTopImgHeight) ];
    imgV.image = index == 0 ? IMAGENAMED(@"share_banner1") : IMAGENAMED(@"share_banner2");
    return imgV;
}
#pragma mark - 私有方法
//分类
- (void)topBtnsClick:(UIButton *)sender {
    NSInteger tag = sender.tag - 1000;
    NSString *tile = tag == 25 ? @"地方特产"  : tag == 26 ? @"征婚交友" : tag == 27 ? @"农家乐" : @"二手物品";
    [self.delegate clickAtCategory:[NSString stringWithFormat:@"%zd",tag] title:tile];
}
- (CGFloat)getSectionheight:(NSInteger)index {
     NSInteger count = [self.categoryArray[index + 1] count];
    NSInteger rows = count % 4 == 0 ? count / 4 : count / 4 + 1;
    CGFloat secH = rows*80.0 - 10.0;
    CGFloat contentHeight =  [self.foldStateArray[index]  isEqual: @YES]  ? shareSectionHeight : secH;
    return contentHeight + shareHeaderHeight + shareFooterHeight;
}

#pragma mark - 懒加载
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, shareTopImgHeight + 95, App_Width, (shareHeaderHeight + shareFooterHeight + shareSectionHeight)*3) collectionViewLayout:self.flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
        _collectionView.backgroundColor = WHITECOLOR;
        [_collectionView registerClass:[CCCollectionViewHeader class]  forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:reuseIdentifierHeader];
        [_collectionView registerClass:[CCCollectionViewFooter class]forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:reuseIdentifierFooter];
        [_collectionView registerClass:[CCMyShareCollectionCell class] forCellWithReuseIdentifier:CellId];
    }
    return _collectionView;
}
- (UICollectionViewFlowLayout *) flowLayout{
    CGFloat width = 40;
    CGFloat margin = (App_Width - 20 - width* 4)/5;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.headerReferenceSize = CGSizeMake(300.0f, shareHeaderHeight);  //设置head大小
    flowLayout.footerReferenceSize = CGSizeMake(300.0f, shareFooterHeight);
    flowLayout.itemSize = CGSizeMake(width, 70);
    flowLayout.minimumLineSpacing = 10;
    flowLayout.minimumInteritemSpacing = margin;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, margin+10, 0, margin+10);
    return flowLayout;
}

- (ZYBannerView *)topImgView {
    if (!_topImgView) {
        _topImgView = [[ZYBannerView alloc]initWithFrame:CGRectMake(0, 0, App_Width, shareTopImgHeight) ];
        _topImgView.dataSource = self;
        _topImgView.autoScroll = YES;
        _topImgView.shouldLoop = YES;
    }
    return _topImgView;
}
- (void)addTopBtns {
    CGFloat margin = (App_Width - 40*4) / 5;
    NSArray *imgArray = @[@"difangtechan",@"zhenghun",@"nongjiale",@"ershou"];
    NSArray *titleArray = @[@"地方特产",@"征婚交友",@"农家乐",@"二手物品"];
    for (int i = 0; i<4; i++) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(margin + (40 + margin)*i, shareTopImgHeight + 15, 40, 40)];
        [btn setImage:IMAGENAMED(imgArray[i]) forState:UIControlStateNormal];
        btn.tag = 1000 + i + 25;
        [btn addTarget:self action:@selector(topBtnsClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        UILabel *titleLbl = [[UILabel alloc]initWithText:titleArray[i] font:14 textColor:TEXTBLACK3];
        [self addSubview:titleLbl];
        [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(btn.mas_centerX);
            make.top.equalTo(btn.mas_bottom).offset(10);
        }];
    }
    UILabel *line = [[UILabel alloc]initWithShallowLine];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(0.5);
        make.top.equalTo(self.topImgView.mas_bottom).offset(94.5);
    }];
}
@end









@implementation CCCollectionViewHeader

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.titleLbl = [[UILabel alloc]initWithText:@"类别" font:14 textColor:TEXTBLACK3];
        self.titleLbl.frame = CGRectMake(15, 0, App_Width, shareHeaderHeight);
        [self addSubview:self.titleLbl];
    }
    return self;
}

@end


@implementation CCCollectionViewFooter

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UILabel *lineLbl = [[UILabel alloc]initWithShallowLine];
        [self addSubview:lineLbl];
        lineLbl.frame = CGRectMake(0, 0, App_Width, 0.5);
        
        self.moreImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 3, App_Width, 17)];
        self.moreImgView.image = IMAGENAMED(@"more");
        [self addSubview:self.moreImgView];
        self.moreImgView.userInteractionEnabled = YES;
        self.moreImgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}

@end

@implementation CCMyShareCollectionCell
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.width)];
        [self.contentView addSubview:self.imgView];
        
        self.titleLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:TEXTBLACK3];
        [self.contentView addSubview:self.titleLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.top.equalTo(self.imgView.mas_bottom).offset(5);
        }];
        
        
    }
    return self;
}
- (void)setModel:(CCCategoryModel *)model {
    _model = model;
    [self.imgView setImageURL:URL(model.imgurl)];
    self.titleLbl.text = model.tname;
}
@end
