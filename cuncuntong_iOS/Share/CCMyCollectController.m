//
//  CCMyCollectController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyCollectController.h"
#import "CCMyOrderCell.h"
#import "CCShareDetailController.h"
#import "CCShareCollectModel.h"
#import "CCMyCollectCell.h"
static NSString *myCollectCell = @"myCollectCell";

@interface CCMyCollectController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CCEmptyView *emptyView;

@end

@implementation CCMyCollectController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self initRefreshView];
    [self.tableView.mj_header beginRefreshing];
}

- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
}
- (void)setUI {
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [NOTICENTER addObserver:self selector:@selector(refreshData) name:@"shareCollectChange" object:nil];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/getmycol") params:@{@"col_uid":USERID,@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
        if([success[@"data"][@"stat"] integerValue] == 1){
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"data"][@"info"]) {
                [self.dataArray addObject:[CCShareCollectModel modelWithJSON:dict]];
            }
            [self endLoding:success[@"data"][@"info"]];
        }else{
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}

//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO : YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyCollectCell *cell = [tableView dequeueReusableCellWithIdentifier:myCollectCell];
    if (self.dataArray.count > indexPath.row) {
        CCShareCollectModel *model = self.dataArray[indexPath.row];
        cell.model = model;
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCShareCollectModel *model = self.dataArray[indexPath.row];
        CCShareDetailController *vc = [[CCShareDetailController alloc]init];
        vc.collectId = model.col_id;
        vc.shareId = model.shareinfo.sp_id;
        vc.fromCollectList = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"取消收藏";
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (self.dataArray.count > indexPath.row) {
            CCShareCollectModel *model = self.dataArray[indexPath.row];
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/cancel_collection") params:@{@"col_id":STR(model.col_id)} target:self success:^(NSDictionary *success) {
                SHOW(@"取消成功");
                [self.dataArray removeObject:model];
                [self.tableView reloadData];
            } failure:^(NSError *failure) {
            }];
        }
    }
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat y = ![App_Delegate.defaultHome isEqualToString:@"default"] ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, y, App_Width, App_Height - 49)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 90;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyCollectCell" bundle:nil] forCellReuseIdentifier:myCollectCell];
#ifdef __IPHONE_11_0
        if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
#endif
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
