//
//  CCShareMapInfoView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareMapInfoView.h"

@implementation CCShareMapInfoView
+ (instancetype)instanceView {
    CCShareMapInfoView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    return view;
}
- (void)setModel:(CCShareDetailModel *)model {
    _model = model;
    self.addressLbl.text = [NSString stringWithFormat:@"详细地址：%@", model.location_info];
    self.nameLbl.text = model.sp_title;
    self.addressNameLbl.text = [NSString stringWithFormat:@"地址：%@", model.name];
    if (model.distance == 0) {
        self.distanceLbl.text = @"";
    }else{
        self.distanceLbl.text = [NSString stringWithFormat:@"距离：%.0fkm", model.distance / 1000];
    }
}
@end
