//
//  CCShareHomeController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCShareHomeController : BaseViewController
@property (nonatomic, strong) NSString *keyword;
@end
