//
//  CCShareMyController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareMyController.h"
#import "CCMyHeader.h"
#import "CCMyFooter.h"
#import "CCMyCell.h"
#import "CCAddressModel.h"
#import "CCHobbyModel.h"


@interface CCShareMyController ()<UITableViewDataSource,UITableViewDelegate,CCMyCellDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *hobbyArray;
@property (nonatomic, strong) CCMyHeader *header;
@property (nonatomic, strong) CCMyFooter *footer;

@end

@implementation CCShareMyController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
    [self loadData];
    [self loadHobby];
    [NOTICENTER addObserver:self selector:@selector(chooseAddressFinished:) name:@"ChooseLocationNotification" object:nil];

}
- (void)refreshData {
    [self loadData];
}
- (void)setUI {
    [self addTitle:@""];
    [self.view addSubview:self.tableView];
}
#pragma mark - 获取数据
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"userid":USERID,@"offset":@"0",@"limit":@"30"}];
    params[@"type"] = self.addressType;
    
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/getaddress") params:params target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSArray *array = success[@"data"];
            [self.dataArray removeAllObjects];
            for (NSDictionary *dic  in array) {
                CCAddressModel *model = [CCAddressModel modelWithJSON:dic];
                [self.dataArray addObject:model];
            }
        }else{
            
        }
        [self.tableView reloadData];
    } failure:^(NSError *failure) {
    }];
}
//兴趣
- (void)loadHobby{
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharelist") params:@{@"userid":USERID,@"new":@"2"} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [self.hobbyArray removeAllObjects];
            for (NSDictionary *dict in success[@"data"]) {
                CCHobbyModel *model = [CCHobbyModel modelWithJSON:dict];
                [self.hobbyArray addObject:model];
            }
            self.footer.dataArray = self.hobbyArray;
            [self.tableView reloadData];
        }
    } failure:^(NSError *failure) {
        
    }];
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    cell.delegate = self;
    cell.model = self.dataArray[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CCAddressModel *model = self.dataArray[indexPath.row];
    if (self.selectAddressBlock) {
        self.selectAddressBlock(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
}
//保存
- (void)saveBtnClick {
    if (!self.header.model.lat || !self.header.model.lng) {
        SHOW(@"请通过“定位当前位置”设置详细地址");
        return;
    }
    if (self.header.phoneField.text.length == 0) {
        SHOW(@"请输入电话号码");
        return;
    }
    if (self.header.addressField.text.length == 0) {
        SHOW(@"请通过“定位当前位置”设置详细地址");
        return;
    }
    if (self.header.addressNameField.text.length == 0) {
        SHOW(@"请输入地址名称");
        return;
    }
   
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"userid":USERID,@"phone":self.header.phoneField.text,@"location":self.header.addressField.text,@"location_info":self.header.addressField.text,@"name":self.header.addressNameField.text,@"lat":self.header.model.lat,@"lng":self.header.model.lng}];
//    if (self.addressType) {
//        params[@"type"] = self.addressType;
//    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/address") params:params target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"添加成功");
            [self refreshData];
            self.header.phoneField.text = @"";
            self.header.addressNameField.text = @"";
            self.header.addressField.text = @"";
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}
//删除
- (void)deleteBtnClick:(CCAddressModel *)model {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/deleteaddress") params:@{@"userid":USERID,@"address_id":model.id} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"删除成功");
            [self refreshData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (void)chooseAddress {
    ShowMapViewController *vc = [[UIStoryboard storyboardWithName:@"ShowMapViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"ShowMapViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}
//选择地址
- (void)chooseAddressFinished:(NSNotification *)noti {
    NSDictionary *dict = noti.userInfo;
    NSLog(@"选择地址--%@",dict);
    CCAddressModel *model = [[CCAddressModel alloc]init];
    model.name = dict[@"name"];
    model.location = dict[@"address"];
    model.lat = dict[@"lat"];
    model.lng = dict[@"lng"];
    self.header.model = model;

}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat y = kSystemVersion >= 11.0 || self.hideFooter || ![App_Delegate.defaultHome isEqualToString:@"default"] ? 64 : 0;
        CGFloat height = self.hideFooter ? y: y + 49;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, y, App_Width, App_Height - height)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.header;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (!self.hideFooter) {
            _tableView.tableFooterView = self.footer;
        }
        _tableView.rowHeight = 120;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CCMyHeader *)header {
    if (!_header) {
        _header = [CCMyHeader instanceView];
        _header.frame = CGRectMake(0, 0, App_Width, 255);
        [_header.saveBtn addTarget:self action:@selector(saveBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_header.locateBtn addTarget:self action:@selector(chooseAddress) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _header;
}
- (CCMyFooter *)footer {
    if (!_footer) {
        _footer = [[CCMyFooter alloc]initWithFrame:CGRectMake(0, 0, App_Width, 150)];
    }
    return _footer;
}

- (NSMutableArray *)hobbyArray {
    if (!_hobbyArray) {
        _hobbyArray = [[NSMutableArray alloc]init];
    }
    return _hobbyArray;
}


@end
