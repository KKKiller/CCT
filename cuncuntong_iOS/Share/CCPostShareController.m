//
//  CCPostShareController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCPostShareController.h"
#import "MTUploadImageCell.h"
#import "KKPhotoPickerManager.h"
#import "CCSelectCityController.h"
#import "CCCategoryModel.h"
#import "CCShareDetailModel.h"
#import "IQKeyboardManager.h"
#import "CCHobbyModel.h"
#import "BRPickerView.h"
#import "CCAddressModel.h"
#import "CCShareMyController.h"
#import "CCGirlAddressEditVc.h"
static NSString *collectionViewCellId = @"collectionViewCellId";
static CGFloat imageSize = 78;
@interface CCPostShareController () <UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (weak, nonatomic) IBOutlet UILabel *descPlaceHolderLbl;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UIView *chooseImgContainerV;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UIButton *chooseCategoryBtn;
@property (weak, nonatomic) IBOutlet UILabel *categoryLbl;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *hidePhoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *postBtn;
@property (weak, nonatomic) IBOutlet UIButton *chooseAddressBtn;
@property (weak, nonatomic) IBOutlet UIButton *addAddressBtn;

@property (weak, nonatomic) IBOutlet UITextField *inviterField;


@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *addressTap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottomMargin;
@property (nonatomic, strong) UICollectionView *collectionView; //添加图片,每个cell内有一个imageView
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, strong) NSMutableArray *deleteImgIndexArray; //记录删除的图片的位置
@property (strong, nonatomic) NSIndexPath *fromIndexPath;

@property(nonatomic, strong) UIPickerView *pickerView;
@property(nonatomic, strong) UITextField *textField; //作为响应着弹出pickerView
@property(nonatomic, strong) UIView *maskView;//蒙版
@property(nonatomic, strong) UILabel *accessoryViewName;
@property (nonatomic, strong) CCSubHobbyModel *selectedSubHobby;
@property (nonatomic, strong) CCHobbyModel *selectedHobby;
@property (nonatomic, assign) NSInteger selectedSection;
@property (nonatomic, assign) NSInteger selectdRow;

@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (nonatomic, strong) NSString *categoryText; //分类
@property (nonatomic, strong) NSString *addressId; //地址
@property (nonatomic, assign) NSInteger loadIndex;
//@property (nonatomic, strong)  LocationModel *locationModel;
@property (nonatomic, strong) CCAddressModel *addressModel;
@end

@implementation CCPostShareController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self setCollectionView];
    App_Delegate.maxPicNum = 5;
    App_Delegate.selectedPicNum = 0;
    self.deleteImgIndexArray = [NSMutableArray array];
    self.addressId = [UserManager sharedInstance].location;
    self.chooseAddressBtn.layer.borderColor = MAINBLUE.CGColor;
    self.chooseAddressBtn.layer.borderWidth = 0.5;
    [self.addAddressBtn addTarget:self action:@selector(chooseAddress) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseAddressBtn addTarget:self action:@selector(chooseAddress) forControlEvents:UIControlEventTouchUpInside];
    if (!STRINGEMPTY([UserManager sharedInstance].inviter_id)) {
        self.inviterField.text = [UserManager sharedInstance].inviter_id;
    }
    [self getHobbyData];
    [self setOldShareDetail];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.scrollView scrollToTop];
    [IQKeyboardManager sharedManager].enable = YES;
}
- (void)setUI {
    if (self.shareModel) {
        self.scrollBottomMargin.constant = 0;
        self.scrollView.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
    }
    if([[UIDevice currentDevice].systemVersion floatValue] >= 11.0 || ![App_Delegate.defaultHome isEqualToString:@"default"]){
        self.scrollView.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
    }
    [self addTitle:@"发布共享"];
    self.navigationController.navigationBarHidden = NO;
    self.descTextView.delegate = self;
    [self.hidePhoneBtn addTarget:self action:@selector(hidePhoneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.postBtn addTarget:self action:@selector(postBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseCategoryBtn addTarget:self action:@selector(chooseCategoryBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)getHobbyData {
//    if ([TOOL getCachaDataWithName:@"hobbys"]) {
//        [self handleCategoryData:[TOOL getCachaDataWithName:@"hobbys"]];
//    }else{
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharelist") params:@{@"userid":USERID,@"new":@"2"} target:nil success:^(NSDictionary *success) {
            if ([success[@"error"] integerValue] == 0) {
                [TOOL saveDataWithData:success Name:@"hobbys"];
                [self handleCategoryData:[TOOL getCachaDataWithName:@"hobbys"]];
            }
        } failure:^(NSError *failure) {
        }];
//    }
}
- (void)handleCategoryData:(NSDictionary *)dict {
    if (dict) {
        @synchronized (self) {
            [self.categoryArray removeAllObjects];
            for (NSDictionary *dic in dict[@"data"]) {
                CCHobbyModel *model = [CCHobbyModel modelWithJSON:dic];
                [self.categoryArray addObject:model];
            }
        }
    }
}
- (void)setOldShareDetail {
    if (self.shareModel) {
        self.titleField.text = self.shareModel.sp_title;
        self.descTextView.text = self.shareModel.sp_content;
        self.descPlaceHolderLbl.hidden = YES;
        self.priceTextField.text = self.shareModel.sp_pricing;
        self.phoneTextField.text = self.shareModel.sp_phone;
        for (CCHobbyModel *model in App_Delegate.shareTabbarVc.hobbyArray) {
            if ([model.tid isEqualToString:self.shareModel.sp_sign]) {
                self.categoryLbl.text = [NSString stringWithFormat:@"类别: %@",model.tname];
                self.categoryText = model.tname;
                break;
            }
        }
        self.locationLbl.text = self.shareModel.location_info;
        self.addressModel = [[CCAddressModel alloc]init];
        self.addressModel.location = self.shareModel.location_info;
        self.addressModel.lat = self.shareModel.x_axis;
        self.addressModel.lng = self.shareModel.y_axis;
        
        NSMutableArray *oldImageArray = [NSMutableArray array];
        if (self.shareModel.sp_imgs.count > 0) {
            for (int i = 0;  i < self.shareModel.sp_imgs.count; i++) {
                NSString *url = self.shareModel.sp_imgs[i];
                UIImage *image = [UIImage imageWithData:[NSData
                                                         dataWithContentsOfURL:[NSURL URLWithString:url]]];
                if (image) {
                    [oldImageArray addObject:image];
                }
            }
        }
        [self.imageArray addObjectsFromArray:oldImageArray];
        [self.collectionView reloadData];
        
        [self deleteOldImages];//删除旧图片
    }
}
- (void)deleteOldImages{
    for (NSString *url in self.shareModel.sp_imgs) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/del_share_img") params:@{@"userid":USERID,@"imgurl":url,@"sp_id":self.shareModel.sp_id} target:nil success:^(NSDictionary *success) {
                if ([success[@"error"] integerValue] == 0) {
                
                }else{
                    SHOW(@"删除失败");
                }
            } failure:^(NSError *failure) {
            }];
        });
    }
}






#pragma mark - 点击事件
//发布
- (void)postBtnClick {
    if ([self inputTextCheck]) return;
    NSString *location_infor = [self.addressModel.location_info isKindOfClass:[NSNull class]] || self.addressModel.location_info == nil ? @"" : self.addressModel.location_info;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{
//                            @"sp_uid":USERID,
                            @"sp_fbd":STR(self.addressId),//发布地
                            @"sp_title":self.titleField.text,
                            @"sp_sign":STR(self.selectedHobby.tid), //分类标签
                            @"sp_sign2":STR(self.selectedSubHobby.tid),
                            @"sp_content":self.descTextView.text,
                            @"sp_pricing":self.priceTextField.text,
                            @"sp_phone":self.phoneTextField.text,
                            @"sp_hidden":self.hidePhoneBtn.selected ? @"1" : @"0",
                            @"x_axis":STR(self.addressModel.lat),
                            @"y_axis":STR(self.addressModel.lng),
                            @"location_info":STR(location_infor),
                            @"name":STR(self.addressModel.realname)
                            }];
    if (self.shareModel) {
        [param setValue:STR(self.shareModel.sp_id) forKey:@"sp_id"];
    }else{
        [param setValue:USERID forKey:@"sp_uid"];
    }
    if (self.inviterField.text) {
        [param setValue:self.inviterField.text forKey:@"parent_id"];
    }
//    [param setValue:@"http://www.cct369.com/village/public/shareimgs/20181021/6799347f64b5eae01304623f16ac772b.png" forKey:@"sp_ct1"];
    [TOOL showLoading:self.scrollView];
    NSString *url = self.shareModel ? @"share/updateshare" : @"share/sharepublish";
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(url) params:param target:self success:^(NSDictionary *success) {
        if([success[@"data"][@"stat"] integerValue] == 1){
            NSString *shareId = self.shareModel ? self.shareModel.sp_id : [NSString stringWithFormat:@"%@",success[@"data"][@"id"]];
            if (self.imageArray.count > 0) {
                for (int i = 0; i<self.imageArray.count; i++) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(i * 0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self postImage:self.imageArray[i] shareId:shareId];
                    });
                }
            }else{
                [self finishLoad];
                //                App_Delegate.shareTabbarVc.selectedIndex = 1;
                //                 [NOTICENTER postNotificationName:@"myShareChange" object:nil];
            }
        }else{
            [TOOL hideLoading:self.scrollView];
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.scrollView];
    }];
    //图片
    
}
- (void)postImage:(UIImage *)image shareId:(NSString *)shareId {
    image = [TOOL fixOrientation:image];
    NSData *data = UIImagePNGRepresentation(image);
    data = [TOOL imageCompress:data];
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/upload_pic") params:@{@"sp_id":STR(shareId),@"sp_ct":@""} target:self imgaeData:data success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            [self finishLoad];
        }else{
            [TOOL hideLoading:self.scrollView];
            SHOW(success[@"data"][@"info"]);
            self.loadIndex = 0;
            [NOTICENTER postNotificationName:@"myShareChange" object:nil];
        }
    } failure:^(NSError *failure) {
        self.loadIndex = 0;
        [TOOL hideLoading:self.scrollView];
    }];
}
- (void)finishLoad {
    self.loadIndex += 1;
    if (self.imageArray.count == 0 || self.loadIndex == self.imageArray.count) {
        self.loadIndex = 0;
        if(self.shareModel){
            [NOTICENTER postNotificationName:@"ShareDetailChange" object:nil];
            SHOW(@"修改成功");
            [self.navigationController popViewControllerAnimated:YES];
        }else{
             SHOW(@"发布成功");
            App_Delegate.shareTabbarVc.selectedIndex = 1;
            [self postSuccessCleadData];
        }
        [NOTICENTER postNotificationName:@"myShareChange" object:nil];
        [TOOL hideLoading:self.scrollView];
    }
}
- (void)postSuccessCleadData {
    self.titleField.text = @"";
    self.descTextView.text = @"";
    self.priceTextField.text = @"";
    self.categoryText = @"";
    self.categoryLbl.text = @"";
    [self.imageArray removeAllObjects];
    [self.collectionView reloadData];
    
}
- (void)hidePhoneBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}
//选择分类
- (void)chooseCategoryBtnClick {
    [self.view addSubview:self.maskView];
    [self.pickerView reloadAllComponents];
    [self.textField becomeFirstResponder];
    
//    NSMutableArray *array = [NSMutableArray array];
//    if (self.hobbyArray.count > 0) {
//        for (CCHobbyModel *model in self.hobbyArray) {
//            [array addObject:model.tname];
//        }
//    }
////    else{
////    }
//            array = [[NSMutableArray alloc]initWithArray: @[@[@"服装",@"美食",@"招聘",@"房产",@"交友",@"车辆",@"宾馆",@"其他"],@[@"服装",@"美食",@"招聘",@"房产",@"交友",@"车辆",@"宾馆",@"其他"]]];
//    [BRStringPickerView  showStringPickerWithTitle:@"选择分类" dataSource:array defaultSelValue:array[0] isAutoSelect:YES resultBlock:^(id selectValue) {
//        self.categoryLbl.text = [NSString stringWithFormat:@"分类：%@", selectValue];
//        self.categoryText = selectValue;
//    }];

}
- (void)textViewDidChange:(UITextView *)textView {
    self.descPlaceHolderLbl.hidden = textView.text.length != 0;
}
- (void)chooseAddress {
//    CCShareMyController *vc = [[CCShareMyController alloc]init];
//    vc.addressType = @"1";
//    vc.selectAddressBlock = ^(CCAddressModel *model) {
//        self.addressModel = model;
//        self.locationLbl.text = [NSString stringWithFormat:@"地址：%@", model.location];
//        self.phoneTextField.text = model.phone;
//    };
//    vc.hidesBottomBarWhenPushed = YES;
//    vc.hideFooter = YES;
//    vc.title = @"选择地址";
//    [self.navigationController pushViewController:vc animated:YES];
    
    __block typeof(self)this = self;
    CCGirlAddressEditVc *vc = [[CCGirlAddressEditVc alloc]init];
    vc.selectAddressBlock = ^(CCAddressModel *model) {
        this.addressModel = model;
        NSString *location_infor = [this.addressModel.location_info isKindOfClass:[NSNull class]] || this.addressModel.location_info == nil ? @"" : self.addressModel.location_info;
        self.locationLbl.text = [NSString stringWithFormat:@"地址：%@",location_infor];
        self.phoneTextField.text = model.phone;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (BOOL)inputTextCheck{
    if(self.titleField.text.length == 0) {
        SHOW(@"请输入标题");
        return YES;
    }
    if(self.titleField.text.length > 18 ) {
        SHOW(@"输入标题不能超过18个字");
        return YES;
    }
    if(self.descTextView.text.length == 0) {
        SHOW(@"请输入共享内容");
        return YES;
    }
    if(self.descTextView.text.length > 300) {
        SHOW(@"共享内容不能超过300个字");
        return YES;
    }
    if(self.categoryText.length == 0) {
        SHOW(@"请选择分类");
        return YES;
    }
    if(self.phoneTextField.text.length == 0) {
        SHOW(@"请输入电话");
        return YES;
    }
    if (!self.addressModel) {
        SHOW(@"请选择地址");
        return YES;
    }
    return NO;
}






#pragma mark - 选择图片模块
- (void)setCollectionView{
    self.imageArray = [NSMutableArray array];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(imageSize, imageSize);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 10;
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 35, App_Width-30, imageSize + 20) collectionViewLayout:layout];
    self.collectionView = collectionView;
    [self.chooseImgContainerV addSubview:collectionView];
    
    [self.collectionView registerClass:[MTUploadImageCell class] forCellWithReuseIdentifier:collectionViewCellId];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    //长按换位置
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(couldExchange:)];
    [self.collectionView addGestureRecognizer:longPress];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.imageArray.count <= 5) {
        return self.imageArray.count + 1;
    }
    return self.imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MTUploadImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellId forIndexPath:indexPath];
    //添加子控件,设置布局与控件图片
    [self addAndSetSubViews:cell indexPath:indexPath];
    return cell;
}

- (void)addAndSetSubViews:(MTUploadImageCell *)cell indexPath:(NSIndexPath *)indexPath{
    //清空子控件,解决重用问题
    NSArray *subviews = [[NSArray alloc] initWithArray:cell.contentView.subviews];
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
    UIImageView *imageView = [[UIImageView alloc]init];
    [cell.contentView addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    cell.tag = 11; //根据tag值设定是否可点击,11可点击,初始全部可点击
    cell.imageView = imageView;
    cell.backgroundColor = [UIColor whiteColor];
    if(indexPath.row == 0 ){
        imageView.image = [UIImage imageNamed:@"add_pic"];
    }else{
        imageView.image = nil;
    }
    
    UIButton *cancleBtn = [[UIButton alloc]init];
    cell.cancleBtn = cancleBtn;
    [cell.contentView addSubview: cancleBtn];
    [cancleBtn setImage:[UIImage imageNamed:@"removePic"] forState:UIControlStateNormal];
    cancleBtn.hidden = YES;//初始将删除按钮隐藏
    cell.cancleBtn.tag = indexPath.row;
    [cell.cancleBtn addTarget:self action:@selector(cancleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.imageView.frame = CGRectMake(0, 0, imageSize, imageSize);
    cell.cancleBtn.frame = CGRectMake(-3, -3, 20, 20);
    if (indexPath.row > 0) {
        if (self.imageArray.count > indexPath.row-1) {
            if ([self.imageArray[indexPath.row - 1] isKindOfClass:[UIImage class]]) {
                cell.imageView.image = nil;
                cell.imageView.image = self.imageArray[indexPath.row - 1];
                cell.cancleBtn.hidden = NO;
                cell.tag = 10; //初始设置tag为11,当为10时,表示已经添加图片
            }
        }
    }
}
//点击collectionView跳转到相册
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView cellForItemAtIndexPath:indexPath].tag == 11) {
        [self choosePic];
    }
}
- (void)choosePic {
    [self.view endEditing:YES];
    [[KKPhotoPickerManager shareInstace] showActionSheetInView:self.view fromController:self completionBlock:^(NSMutableArray *imageArray) {
        for (int i = 0; i<imageArray.count; i++) {
            if (self.imageArray.count < 5) {
                UIImage *image = imageArray[i];
                [self.imageArray addObject:image]; //上传图片保存到数组
            }
        }
        App_Delegate.maxPicNum = 5 - self.imageArray.count;
        App_Delegate.selectedPicNum = self.imageArray.count;
        [self.collectionView reloadData];
    }];
}
//移动顺序
- (void)couldExchange:(UILongPressGestureRecognizer *)longPressGesture{
    switch (longPressGesture.state) {
            case UIGestureRecognizerStateBegan:
        {
            NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:[longPressGesture locationInView:self.collectionView]];
            if (indexPath.item == 0) {
                self.fromIndexPath = nil;
            }else{
                [self moveAnimation:indexPath];
                self.fromIndexPath = indexPath;
            }
        }
            break;
            case UIGestureRecognizerStateChanged:{
                NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:[longPressGesture locationInView:self.collectionView]];
                if(indexPath.item == 0){
                    return;
                }
                if (indexPath && indexPath.row!=self.fromIndexPath.row && self.fromIndexPath) {
                    [self.imageArray exchangeObjectAtIndex:indexPath.row -1 withObjectAtIndex:self.fromIndexPath.row -1];
                    [self.collectionView moveItemAtIndexPath:self.fromIndexPath toIndexPath:indexPath];
                    self.fromIndexPath = indexPath;
                }
            }
            break;
            case UIGestureRecognizerStateEnded:{
                NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:[longPressGesture locationInView:self.collectionView]];
                if (self.fromIndexPath) {
                    [self stopAnimation:self.fromIndexPath];
                }
                if (indexPath.item == 0) {
                    
                    return;
                }
                if (indexPath && indexPath.row!=self.fromIndexPath.row && self.fromIndexPath) {
                    [self.imageArray exchangeObjectAtIndex:indexPath.row - 1 withObjectAtIndex:self.fromIndexPath.row - 1];
                    [self.collectionView moveItemAtIndexPath:self.fromIndexPath toIndexPath:indexPath];
                }
                [self.collectionView reloadData];
            }
            break;
        default:
            break;
    }
}
//长按动画
- (void)moveAnimation:(NSIndexPath *)indexPath {
    MTUploadImageCell *cell = (MTUploadImageCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:0.1 animations:^{
        cell.imageView.frame = CGRectMake(5, 5, 70, 70);
        cell.cancleBtn.frame = CGRectMake(0, 0, 20, 20);
    }];
}
//取消移动动画
- (void)stopAnimation:(NSIndexPath *)indexPath {
    MTUploadImageCell *cell = (MTUploadImageCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:0.1 animations:^{
        cell.imageView.frame = CGRectMake(0, 0, 80, 80);
        cell.cancleBtn.frame = CGRectMake(-3, -3, 20, 20);
    }];
}
//删除图片
- (void)cancleBtnClick:(UIButton *)sender{
    if (sender.tag -1 < self.imageArray.count) {
        [self.imageArray removeObjectAtIndex:sender.tag - 1];
        sender.hidden = YES;
        App_Delegate.maxPicNum = 5 - self.imageArray.count;
        App_Delegate.selectedPicNum = self.imageArray.count;
        [self.collectionView reloadData];
        
    }
}


#pragma mark - 选择分类
#pragma mark - 选择分类

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return self.categoryArray.count;
    }else{
        CCHobbyModel *model = self.categoryArray[self.selectedSection];
        return [model.find count];
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        CCHobbyModel *model = self.categoryArray[row];
        return model.tname;
        
    }else{
        CCHobbyModel *model = self.categoryArray[self.selectedSection];
        CCSubHobbyModel *subModel = model.find[row];
        
        return subModel.tname;
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        [pickerView selectRow:0 inComponent:1 animated:YES];
        self.selectedSection = row;
    }else{
        self.selectdRow = row;
    }
    self.selectedHobby = self.categoryArray[self.selectedSection];
    self.selectedSubHobby = self.selectedHobby.find[self.selectdRow];
    
    [pickerView reloadComponent:1];
    self.categoryLbl.text = [NSString stringWithFormat:@"类别: %@",self.selectedSubHobby.tname];
}
- (UITextField *)textField {
    if (_textField == nil) {
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(50, 0, 1, 1)];
        _textField.inputView = self.pickerView;
        [self.view addSubview:_textField];
        UIView *accessoryView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 44)];
        UIImageView *backView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, App_Width, 44)];
        backView.image = [UIImage imageNamed:@"App_headImage"];
        [accessoryView addSubview:backView];
        self.accessoryViewName = [[UILabel alloc]initWithText:@"" font:18 textColor:WHITECOLOR];
        self.accessoryViewName.font = FFont(18);
        self.accessoryViewName.alpha = 0.8;
        [accessoryView addSubview:self.accessoryViewName];
        [self.accessoryViewName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(accessoryView.mas_centerX);
            make.centerY.equalTo(accessoryView.mas_centerY);
        }];
        UIButton *sureBtn = [[UIButton alloc]init];
        sureBtn.alpha = 0.8;
        [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
        sureBtn.titleLabel.font = FFont(14);
        [sureBtn addTarget:self action:@selector(sureBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [accessoryView addSubview:sureBtn];
        [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(60);
            make.centerY.equalTo(accessoryView.mas_centerY);
            make.right.equalTo(accessoryView.mas_right).offset(-10);
        }];
        accessoryView.backgroundColor = MAINCOLOR;
        _textField.inputAccessoryView = accessoryView;
    }
    return _textField;
}
- (UIView *)maskView {
    if (_maskView == nil) {
        _maskView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, App_Width , App_Height - 44)];
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0.3;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sureBtnClick)];
        [_maskView addGestureRecognizer:tap];
    }
    return _maskView;
}
- (void)sureBtnClick{
//    CCCategoryModel *model = self.categoryArray[self.selectedSection][self.selectdRow];
    self.categoryText = self.selectedSubHobby.tname;
    self.categoryLbl.text =  [NSString stringWithFormat:@"类别: %@",self.categoryText];
    
    [self.textField resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        self.maskView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.maskView removeFromSuperview];
        self.maskView = nil;
    }];
}
- (UIPickerView *)pickerView {
    if (_pickerView == nil) {
        _pickerView = [[UIPickerView alloc]init];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        _pickerView.backgroundColor = [UIColor whiteColor];
        _pickerView.showsSelectionIndicator =  YES;
    }
    return _pickerView;
}
- (NSMutableArray *)categoryArray {
    if (!_categoryArray) {
        _categoryArray = [NSMutableArray array];
    }
    return _categoryArray;
}
@end
