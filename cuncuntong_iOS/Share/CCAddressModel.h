//
//  CCAddressModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/13.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCAddressModel : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *location_info;
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@property (nonatomic, strong) NSString *village_id;

@end
