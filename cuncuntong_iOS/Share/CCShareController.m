//
//  CCMyShareController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareController.h"
#import "CCMyOrderCell.h"
#import "CCShareHeaderView.h"
#import "CCShareDetailController.h"
#import "CCSelectCityController.h"
#import "CCCategoryModel.h"
#import "CCShareListController.h"
static NSString *CellId = @"orderCell";
static NSString * const reuseIdentifierHeader = @"MGHeaderCell";
static NSString * const reuseIdentifierFooter = @"MGFooterCell";
@interface CCShareController ()<UITableViewDataSource,UITableViewDelegate,CCMyShareHeaderDelegate,UISearchBarDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) CCShareHeaderView *headerView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *searchDataArray;
@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (assign, nonatomic) NSInteger pageIndex;


@property (nonatomic, strong) UILabel *addressLbl;
@property (nonatomic, strong) UISearchBar *seachBar;
@property (nonatomic, strong) UIView *searchMaskView;
@property (nonatomic, strong)  LocationModel *locationModel;

@end

@implementation CCShareController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    self.locationModel =  [[UserManager sharedInstance].locs count] >=4 ? [UserManager sharedInstance].locs[3] : [[LocationModel alloc]init];
    self.searchDataArray = [NSMutableArray array];
    [self setUI];
    [self initRefreshView];
    [self loadCategoryData];
    [self loadData];

}
- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
    [self loadCategoryData];
}
- (void)setUI {
    self.view.backgroundColor = BACKGRAY;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.addressLbl];
    [self.view addSubview:self.seachBar];
    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(55, 17+64, 10, 10)];
    arrow.image = IMAGENAMED(@"arrow_down");
    [self.view addSubview:arrow];
    
    UIButton *changeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [changeBtn addTarget:App_Delegate.shareTabbarVc action:@selector(changeRootView) forControlEvents:UIControlEventTouchUpInside];
    [changeBtn setTitle:@"切换首页" forState:UIControlStateNormal];
    changeBtn.titleLabel.font = FFont(14);
    [changeBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:changeBtn];
    self.navigationItem.rightBarButtonItem = right;
    
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/tj") params:@{@"page":@(self.pageIndex)} target:self success:^(NSDictionary *success) {
        if(self.pageIndex == 1){
            [self.dataArray removeAllObjects];
        }
        for (NSDictionary *dict in success[@"data"]) {
            CCShareDetailModel *model = [CCShareDetailModel modelWithJSON:dict];
            [self.dataArray addObject:model];
        }
        [self endLoding:success[@"data"]];
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
- (void)loadCategoryData {
    if ([TOOL getCachaDataWithName:@"shareCagetorys"]) {
        [self handleCategoryData:[TOOL getCachaDataWithName:@"shareCagetorys"]];
    }
    [[MyNetWorking sharedInstance]GetUrl:BASEURL_WITHOBJC(@"share/sharelist") params:nil success:^(NSDictionary *success) {
        [self handleCategoryData:success];
        [TOOL saveDataWithData:success Name:@"shareCagetorys"];
    } failure:^(NSError *failure) {
        
    }];
}
- (void)handleCategoryData:(NSDictionary *)dict {
    if (dict) {
        @synchronized (self) {
            [self.categoryArray removeAllObjects];
            for (int i = 0; i<[[dict[@"data"] allValues] count] -1; i++) {
                NSString *key = [NSString stringWithFormat:@"type%@",@(i)];
                NSArray *array = dict[@"data"][key];
//            for (NSArray *array in [dict[@"data"] allValues]) {
//                if ([array isKindOfClass:[NSArray class]]) {
                    NSMutableArray *arrayM = [NSMutableArray array];
                    for (NSDictionary *dict in array) {
                        CCCategoryModel *model = [CCCategoryModel modelWithJSON:dict];
                        [arrayM addObject:model];
                    }
                    
                    [self.categoryArray addObject:arrayM];
                }
        }
        self.headerView.categoryArray = self.categoryArray;
    }
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchDataArray.count > 0 ? self.searchDataArray.count : self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    cell.payBtnHidden = YES;
    if (self.searchDataArray.count > 0) {
        if (self.searchDataArray.count > indexPath.row) {
            cell.detailModel = self.searchDataArray[indexPath.row];
        }
    }else{
        if (self.dataArray.count > indexPath.row) {
            cell.detailModel = self.dataArray[indexPath.row];
        }
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchDataArray.count > 0) {
        if (self.searchDataArray.count > indexPath.row) {
            CCShareDetailController *vc = [[CCShareDetailController alloc]init];
            CCShareDetailModel *model = self.searchDataArray[indexPath.row];
            vc.shareId = model.sp_id;
            vc.isSelf = [model.sp_uid isEqualToString:USERID];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
        if (self.dataArray.count > indexPath.row) {
            CCShareDetailController *vc = [[CCShareDetailController alloc]init];
            CCShareDetailModel *model = self.dataArray[indexPath.row];
            vc.shareId = model.sp_id;
            vc.isSelf = [model.sp_uid isEqualToString:USERID];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    self.tableView.tableHeaderView = self.headerView;
    [self.searchDataArray removeAllObjects];
    [self.tableView reloadData];
}
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    self.searchMaskView.hidden = NO;
    return YES;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    self.searchMaskView.hidden = YES;
    return YES;
}
//搜索
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (searchBar.text.length == 0) {
        SHOW(@"请输入搜索内容");
        return;
    }
    [searchBar resignFirstResponder];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharepublish_list") params:@{@"cityid":STR(self.locationModel.id),@"kw":searchBar.text,@"page":@"1",@"size":@"100"} target:self success:^(NSDictionary *success) {
        [self.searchDataArray removeAllObjects];
        for (NSDictionary *dict in success[@"data"]) {
            [self.searchDataArray addObject:[CCShareDetailModel modelWithJSON:dict]];
        }
        if (self.searchDataArray.count > 0) {
            [self endLoding:success[@"data"]];
            self.tableView.tableHeaderView = nil;
            [self.tableView scrollToTop];
        }else{
            [self performSelector:@selector(showMessage) withObject:nil afterDelay:0.5];
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)showMessage {
    SHOW(@"暂无数据");
}
- (void)myShareHeaderUpdateHeight {
    self.tableView.tableHeaderView = self.headerView;
}
- (void)clickAtCategory:(NSString *)typeId title:(NSString *)title{
    CCShareListController *vc = [[CCShareListController alloc]init];
    vc.typeId = typeId;
    vc.areaId = self.locationModel.id;
    vc.title = title;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.headerView.topImgView startTimer];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.headerView.topImgView stopTimer];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, App_Width, App_Height -49-44)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 90;
        _tableView.tableHeaderView = self.headerView;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, -44, 0);
        if([[UIDevice currentDevice].systemVersion floatValue] >= 11.0){
            _tableView.frame  = CGRectMake(0, 44 + 64, App_Width, App_Height - 64 - 44);
             _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyOrderCell" bundle:nil] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (CCShareHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[CCShareHeaderView alloc]init];
        _headerView.delegate = self;
    }
    return _headerView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (NSMutableArray  *)categoryArray {
    if (!_categoryArray) {
        _categoryArray = [[NSMutableArray alloc]init];
    }
    return _categoryArray;
}


- (UILabel *)addressLbl {
    if (!_addressLbl) {
        _addressLbl = [[UILabel alloc]initWithText:self.locationModel.name font:12 textColor:TEXTBLACK6];
        _addressLbl.frame = CGRectMake(0, 64, 61, 44);
        _addressLbl.backgroundColor = MTARGB(0xeeeeee,0.96);
        _addressLbl.userInteractionEnabled = YES;
        _addressLbl.textAlignment = NSTextAlignmentCenter;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            //选择地点 地址
            CCSelectCityController *vc = [[CCSelectCityController alloc]init];
            vc.selectProvince = YES;
            vc.selectLocBlock = ^(NSString *location, NSString *longAddress, NSString *locationId) {
                _addressLbl.text = location;
                self.locationModel.id = locationId;
                [self refreshData];
            };
            [self.navigationController pushViewController:vc animated:YES];

        }];
        [_addressLbl addGestureRecognizer:tap];
    }
    return _addressLbl;
}
- (UISearchBar *)seachBar {
    if (!_seachBar) {
        _seachBar = [[UISearchBar alloc]initWithFrame:CGRectMake(60, 64, App_Width - 60, 44)];
        _seachBar.delegate = self;
        _seachBar.placeholder = @"搜索共享";
        _seachBar.barTintColor = BACKGRAY;
        _seachBar.translucent = NO;
        _seachBar.showsCancelButton = YES;
        _seachBar.layer.borderWidth = 1;
        _seachBar.layer.borderColor = BACKGRAY.CGColor;
    }
    return _seachBar;
}
- (UIView *)searchMaskView {
    if (!_searchMaskView) {
        _searchMaskView = [[UIView alloc]initWithFrame:CGRectMake(0, 108, App_Width, App_Height - 64-44-49)];
        _searchMaskView.backgroundColor = BLACKCOLOR;
        _searchMaskView.alpha = 0.1;
        _searchMaskView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            [self.seachBar endEditing:YES];
        }];
        [_searchMaskView addGestureRecognizer:tap];
        [self.view addSubview:_searchMaskView];
    }
    return _searchMaskView;
}
@end
