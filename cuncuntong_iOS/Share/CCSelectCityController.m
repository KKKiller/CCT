//
//  CCSelectCityController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCSelectCityController.h"
#import <FMDB.h>
#import "PinYin4Objc.h"
#import "AddTitleView.h"
#import "SectionHeader.h"
//#import "FollowVillageView.h"
#import "FollowVillageModel.h"
#import "CCTTextField.h"
#import "TabBarController.h"

@interface CCSelectCityController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate> {
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    FMDatabase *_dataBase;
    NSString *_currentTitle;
    AddTitleView *_titleView;
    BOOL _isSearch;
}
// 保存所有人名称的字典，人名按钮首字符放在不同键值对当中
@property (nonatomic, strong) NSMutableDictionary * dictNames;
@property (nonatomic, strong) NSMutableDictionary * dicNameId;

// 索引标题数组
@property (nonatomic, strong) NSArray *arrayIndexTitles;
@property (nonatomic, strong) NSMutableSet *identifySet;


@property (nonatomic, strong) UIView *coverView;

@property (nonatomic, strong) UITableView *secondTableView;

@property (nonatomic, strong) NSMutableArray *secondaryTitleArray;

@property(nonatomic,strong) NSIndexPath *secondSelectIndexPath;

@property (nonatomic, strong) SectionHeader *sectionHeader;

@property (nonatomic, strong) NSMutableArray *lastResultArray;

@property (nonatomic, strong) NSMutableArray *searchArray;

@property (nonatomic, strong) NSString *addressStr;

@property (nonatomic, strong) NSString *province;

@end

static NSString *const mainCellIdentifier = @"mainCellIdentifier";

static NSString *const subCellIdentifier = @"subCellIdentifier";

@implementation CCSelectCityController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self uiConfig];
    self.addressStr = @"";
    [self getAreaByPid:@"0"];
    _isSearch = NO;
    
}
/**
 x 获取地区列表
 @param pid pid=0即获取省列表，可根据返回列表的ID获取市列表，以此类推
 */
-(void)getAreaByPid:(NSString *)pid{
    if (![pid isKindOfClass:[NSString class]]) {
        pid = [NSString stringWithFormat:@"%@",pid];
    }
    NSDictionary *parDic = @{@"pid" : pid};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MyNetWorking sharedInstance] GetUrl:BASEURL_WITHOBJC(@"main/area") params:parDic success:^(NSDictionary *success) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"success = %@",success);
        if ([[success objectForKey:@"data"]isKindOfClass:[NSDictionary class]]) {
            NSDictionary *data = [success objectForKey:@"data"];
            if ([[data objectForKey:@"village"]isKindOfClass:[NSArray class]]) {
                NSArray *village = [data objectForKey:@"village"];
                if ([pid integerValue] == 0 ) {
                    [self setDataByAreaArray:village];
                }else{
                    if (village.count == 0) {
                        if(self.selectLocBlock){
                            self.addressStr = [self.province stringByAppendingString:self.addressStr];
                            self.selectLocBlock(_currentTitle, self.addressStr, pid);
                        }
                        [self.navigationController popViewControllerAnimated:YES];
                    }else{
                        self.secondaryTitleArray = [NSMutableArray arrayWithArray:village];
                        [_secondTableView reloadData];
                    }
                }
            }
        }
    } failure:^(NSError *failure) {
        NSLog(@"failure = %@",failure);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

/**
 x 设置数据
 
 @param array 传入的数组
 */
-(void)setDataByAreaArray:(NSArray *)array{
    
    self.dictNames = [NSMutableDictionary dictionary];
    
    HanyuPinyinOutputFormat *outputFormat=[[HanyuPinyinOutputFormat alloc] init];
    [outputFormat setToneType:ToneTypeWithoutTone];
    [outputFormat setVCharType:VCharTypeWithUUnicode];
    [outputFormat setCaseType:CaseTypeUppercase];
    for (NSDictionary *na in array) {
        //        NSLog(@"%@",[na objectForKey:@"name"]);
        NSString *name = [NSString stringWithFormat:@"%@",[na objectForKey:@"name"]];
        
        NSString *pinyin = [[PinyinHelper getFirstHanyuPinyinStringWithChar:[name characterAtIndex:0] withHanyuPinyinOutputFormat:outputFormat] substringWithRange:NSMakeRange(0, 1)];
        
        NSMutableArray *array = [self.dictNames objectForKey:pinyin];
        
        if (array == nil) { // 键值对还不存在
            // 创建数组，保存人名
            NSMutableArray *array = [NSMutableArray arrayWithObject:name];
            // 往字典中添加一个键值对
            [self.dictNames setObject:array forKey:pinyin];
            
        } else
        {  // 键值对已经存在
            [array addObject:name];
        }
        
        NSArray *keyArrays = [self.dictNames allKeys];
        //        NSLog(@"count = %li",keyArrays.count);
        self.arrayIndexTitles = [keyArrays sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        //        NSLog(@"arrayIndexTitles = %@",self.arrayIndexTitles);
        
        self.dicNameId[name] = [na objectForKey:@"id"];
    }
    
    [_tableView reloadData];
    
}



// Open database and search.
//- (void)dbConfig {
//    NSString *dbPath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"sqlite"];
//    _dataBase = [FMDatabase databaseWithPath:dbPath];
//    
//    if ([_dataBase open]) {
//        BOOL result = [_dataBase executeUpdate:@"create table if not exists village (id integer PRIMARY KEY AUTOINCREMENT NOT NULL,identify integer NOT NULL, name text, 'order' text, parent text, children text, pid integer);"];
//        if (result) {
//            [self select];
//        }else{
//            
//        }
//    }
//    [_dataBase close];
//}

- (void)select{
    //创建查询结果对象
    
    self.dictNames = [NSMutableDictionary dictionary];
    
    FMResultSet *resultSet = [_dataBase executeQuery:@"select * from village where pid = 0 order by identify asc"];
    
    //遍历结果
    while ([resultSet next]) {
        NSString *name = [resultSet stringForColumn:@"name"];
        
        HanyuPinyinOutputFormat *outputFormat=[[HanyuPinyinOutputFormat alloc] init];
        [outputFormat setToneType:ToneTypeWithoutTone];
        [outputFormat setVCharType:VCharTypeWithUUnicode];
        [outputFormat setCaseType:CaseTypeUppercase];
        
        NSString *pinyin = [[PinyinHelper getFirstHanyuPinyinStringWithChar:[name characterAtIndex:0] withHanyuPinyinOutputFormat:outputFormat] substringWithRange:NSMakeRange(0, 1)];
        
        NSMutableArray *array = [self.dictNames objectForKey:pinyin];
        
        if (array == nil) { // 键值对还不存在
            // 创建数组，保存人名
            NSMutableArray *array = [NSMutableArray arrayWithObject:name];
            // 往字典中添加一个键值对
            [self.dictNames setObject:array forKey:pinyin];
            
        } else
        {  // 键值对已经存在
            [array addObject:name];
        }
        NSArray *keyArrays = [self.dictNames allKeys];
        
        self.arrayIndexTitles = [keyArrays sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        self.dicNameId[name] = @[[resultSet stringForColumn:@"children"], [resultSet stringForColumn:@"identify"]];
        
        //        NSLog(@"dicNameId = %@",self.dicNameId);
        
    }
    [resultSet close];
    
    //    [self getAreaByPid:@"0"];
    [_tableView reloadData];
    
}

- (void)uiConfig {
    [self addTitle:@"选择地址"];
    
    UIButton *rightBtn = [[UIButton alloc]initWithTitle:@"取消" textColor:WHITECOLOR backImg:nil font:15];
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.frame = CGRectMake(0, 0, 40, 30);
    self.navigationItem.rightBarButtonItem = btn;
    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    _tableView.sectionIndexColor = [UIColor lightGrayColor];
}

- (void)rightBtnClick {
    [self remove];
    self.addressStr = @"";
}
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _tableView) {
        if (_isSearch) {
            return 1;
        }
        return  self.arrayIndexTitles.count;// 15
    }
    return 1;// 1
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableView) {
        if (_isSearch) {
            return self.searchArray.count;
        }
        NSString *key = [self.arrayIndexTitles objectAtIndex:section];    // 取到section分组显示的索引名称
        return [[self.dictNames objectForKey:key] count];    // 返回该分组的人的个数
    }
    return self.secondaryTitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView == _tableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:mainCellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:mainCellIdentifier];
        }
        if (_isSearch) {
            cell.textLabel.text = self.searchArray[indexPath.row];
        }else {
            NSString *firstChar = [self.arrayIndexTitles objectAtIndex:indexPath.section];
            
            // 取到首字母对应的人名数组
            NSArray *arrayNames = [self.dictNames objectForKey:firstChar];
            
            // 取到人名，设置在cell上显示
            cell.textLabel.text = [arrayNames objectAtIndex:indexPath.row];
        }
        return cell;
        
        
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:subCellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:subCellIdentifier];
        }
        //        cell.textLabel.text = self.secondaryTitleArray[indexPath.row][1];
        
        NSDictionary *dictionary = [self.secondaryTitleArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [dictionary objectForKey:@"name"];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:15 * KEY_RATE];
        return cell;
    }
    
    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == _tableView) {
        return self.arrayIndexTitles;
    }
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == _tableView) {
        if (_isSearch) {
            return @"";
        }
        return  [self.arrayIndexTitles objectAtIndex:section];
    }
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45 * KEY_RATE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == _tableView) {
        return  35 * KEY_RATE;
    }
    return 45 * KEY_RATE;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView != _secondTableView) {
        return nil;
    }
    [self.sectionHeader titleLabelConfig:_currentTitle];
    return self.sectionHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == _tableView) {
        [self.secondaryTitleArray removeAllObjects];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(remove)];
        tap.delegate = self;
        [self.coverView addGestureRecognizer:tap];
        
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        _currentTitle = cell.textLabel.text;
        self.province = cell.textLabel.text;
        //        NSLog(@"_currentTitle = %@",[self.dicNameId objectForKey:_currentTitle]);
        [self getAreaByPid:[self.dicNameId objectForKey:_currentTitle]];
    } else {
        self.secondSelectIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        _currentTitle = cell.textLabel.text;
        NSDictionary *dic = [self.secondaryTitleArray objectAtIndex:indexPath.row];
        NSString *locationId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
        if (self.isSelectProvince) {
            if (self.selectLocBlock) {
                if([_currentTitle isEqualToString:@"市辖区"]){
                    _currentTitle = @"北京市";
                }
                self.selectLocBlock(_currentTitle, _currentTitle, locationId);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            self.addressStr = [self.addressStr stringByAppendingString:_currentTitle];
            [self getAreaByPid:locationId];
        }
    }
}

#pragma mark - Lazy Load

- (UIView *)coverView {
    if (!_coverView) {
        _coverView = [[UIView alloc] init];
        [self.view addSubview:_coverView];
        [_coverView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _coverView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.secondTableView.hidden = NO;
    }
    return _coverView;
}

- (UITableView *)secondTableView {
    if (!_secondTableView) {
        _secondTableView = [[UITableView alloc] init];
        [self.coverView addSubview:_secondTableView];
        [_secondTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(17 * KEY_RATE);
            make.left.mas_equalTo(15 * KEY_RATE);
            make.right.mas_equalTo(-15 * KEY_RATE);
            make.height.mas_equalTo(540 * KEY_RATE);
        }];
        _secondTableView.delegate = self;
        _secondTableView.dataSource = self;
    }
    return _secondTableView;
}

- (NSMutableDictionary *)dicNameId {
    if (!_dicNameId) {
        _dicNameId = [[NSMutableDictionary alloc] init];
    }
    return _dicNameId;
}

- (NSMutableArray *)secondaryTitleArray {
    if (!_secondaryTitleArray) {
        _secondaryTitleArray = [[NSMutableArray alloc] init];
    }
    return _secondaryTitleArray;
}

- (SectionHeader *)sectionHeader {
    if (!_sectionHeader) {
        _sectionHeader = [[SectionHeader alloc]
                          initWithFrame:CGRectMake(0, 0, KEY_WIDTH, 45 * KEY_RATE)];
    }
    return _sectionHeader;
}


- (void)remove {
    [_coverView removeFromSuperview];
    _coverView = nil;
    _secondTableView = nil;
}

//- (NSMutableArray *)followVillageArray {
//    
//    if (!_followVillageArray) {
//        _followVillageArray = [[NSMutableArray alloc] init];
//    }
//    return _followVillageArray;
//}

- (NSMutableArray *)searchArray {
    if (!_searchArray) {
        _searchArray = [[NSMutableArray alloc] init];
    }
    return _searchArray;
}

-(NSIndexPath *)secondSelectIndexPath{
    if (!_secondSelectIndexPath) {
        _secondSelectIndexPath = [NSIndexPath indexPathForRow:-1 inSection:0];
    }
    return _secondSelectIndexPath;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.secondTableView]) {
        return NO;
    }
    return YES;
}

#pragma mark 模糊查询
- (void)textFieldDidChange:(UITextField *)textField {
    _isSearch = textField.text.length == 0?  NO: YES;
    self.searchArray = [self searchData:textField.text];
    [_tableView reloadData];
}

- (NSMutableArray *)searchData:(NSString *)str {
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 0; i < self.dicNameId.allKeys.count; i++) {
        if ([self.dicNameId.allKeys[i] rangeOfString:str].location != NSNotFound) {
            [array addObject:self.dicNameId.allKeys[i]];
        }
    }
    return array;
}

@end
