//
//  CCHobbyModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/13.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCSubHobbyModel : NSObject
@property (nonatomic, strong) NSString *tid;
@property (nonatomic, strong) NSString *tname;
@end

@interface CCHobbyModel : NSObject
@property (nonatomic, strong) NSString *tid;
@property (nonatomic, strong) NSString *tname;
@property (nonatomic, assign) BOOL user_state;
@property (nonatomic, strong) NSArray<CCSubHobbyModel *> *find;
@end
