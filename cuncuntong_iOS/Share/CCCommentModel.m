//
//  CCCommentModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCCommentModel.h"

@implementation CCCommentModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"name" : @[@"userinfo.realname"],
             @"avatar" : @[@"userinfo.portrait"]
             };
}
@end
