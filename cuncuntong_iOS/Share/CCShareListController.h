//
//  CCShareListController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCShareListController : BaseViewController
@property (nonatomic, strong) NSString *typeId;
@property (nonatomic, strong) NSString *areaId;
@property (nonatomic, strong) NSString *title;
@end
