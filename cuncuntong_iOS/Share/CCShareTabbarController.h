//
//  CCShareTabbarController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCShareTabbarController : UITabBarController
- (void)gotoOrdering;
- (void)gotoOrdered;
@property (nonatomic, assign) BOOL isPushed;
- (void)changeRootView;
@property (nonatomic, strong) NSArray *hobbyArray;
@end
