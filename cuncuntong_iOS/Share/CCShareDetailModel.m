//
//  CCShareDetailModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/4.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareDetailModel.h"

@implementation CCShareDetailModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"userId" : @[@"userinfo.id"],
             @"avatar" : @[@"userinfo.portrait"],
             @"mobile" : @[@"userinfo.mobile"]
             };
}
//- (NSString *)avatar {
//    return BASEURL_WITHOBJC(_avatar);
//}
@end
