//
//  CCMyOrderingController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/26.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyOrderingController.h"
#import "CCMyOrderCell.h"
#import "CCShareDetailController.h"
#import "CCShareIssueController.h"
#import "CCShareCommentController.h"
#import "CCOrderModel.h"
#import "CCExpressDetailController.h"
static NSString *CellId = @"orderCell";

@interface CCMyOrderingController ()<UITableViewDataSource,UITableViewDelegate,CCMyOrderCellDelegate>
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CCEmptyView *emptyView;

@end

@implementation CCMyOrderingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageIndex = 1;
    [self initRefreshView];
    [self.view addSubview:self.tableView];
    [self loadData];
}
- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/my_ordergoing") params:@{@"order_user":USERID,@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"data"][@"info"]) {
                CCOrderModel *model = [CCOrderModel modelWithJSON:dict];
                [self.dataArray addObject: model];
            }
            [self endLoding:success[@"data"][@"info"]];
        }else{
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO : YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    cell.delegate = self;
    if (self.dataArray.count > indexPath.row) {
        if (self.dataArray.count > indexPath.row) {
            CCOrderModel *model = self.dataArray[indexPath.row];
            cell.order_stat = model.order_stat;
            cell.order_count = model.order_count;
            cell.orderNum = model.order_num;
            cell.detailModel = model.pinfo;
        }
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCShareDetailController *vc = [[CCShareDetailController alloc]init];
        CCOrderModel *model = self.dataArray[indexPath.row];
        vc.shareId = model.pinfo.sp_id;
        vc.isSelf = self;
        [self.navigationController pushViewController:vc animated:YES];

    }
}
//立即支付
- (void)CCMyOrderCellPayBtnClickWithOrderId:(NSString *)orderNum {
    NSString *orderStat = nil;
    for (CCOrderModel *model in self.dataArray) {
        if ([model.order_num isEqualToString:orderNum]) {
            orderStat = model.order_stat;
            model.courier_number = @"223444";
            model.courier_company = @"SF";
            [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/comfirm_pay") params:@{@"order_num":STR(orderNum),@"order_stat":orderStat,@"courier_company":model.courier_company,@"courier_number":model.courier_number} target:self success:^(NSDictionary *success) {
                if ([success[@"data"][@"stat"] integerValue] == 1) {
                    //支付成功后,调接口将订单状态改为已完成 stat==2
                    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/comfirm_pay") params:@{@"order_num":STR(orderNum),@"order_stat":@"2",@"courier_company":model.courier_company,@"courier_number":model.courier_number} target:self success:^(NSDictionary *success) {
                        if ([success[@"data"][@"stat"] integerValue] == 1) {
                            SHOW(@"支付成功");
                            [App_Delegate.shareTabbarVc gotoOrdered];
                            CCShareCommentController *vc = [[CCShareCommentController alloc]init];
                            vc.orderNum = orderNum;
                            [self.navigationController pushViewController:vc animated:YES];
                            [self refreshData];
                        }else{
                            SHOW(success[@"data"][@"info"]);
                        }
                    } failure:^(NSError *failure) {
                        
                    }];
                }else{
                    SHOW(success[@"data"][@"info"]);
                }
            } failure:^(NSError *failure) {
                
            }];
        }
    }
}
//争议投诉
- (void)CCMyOrderCellComplainBtnClickWithOrderId:(NSString *)orderNum {
    for (CCOrderModel *model in self.dataArray) {
        if ([model.order_num isEqualToString:orderNum]) {
            CCShareIssueController *vc = [[CCShareIssueController alloc]init];
            vc.uid = model.pinfo.sp_uid;
            vc.orderNum = model.order_num;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
    }
}
//退款
- (void)applyDrawbackWithOrderId:(NSString *)orderNum{
    for (CCOrderModel *model in self.dataArray) {
        if ([model.order_num isEqualToString:orderNum]) {
            if ([model.order_stat isEqualToString:@"2"]) {//申请退款
                [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/drawback") params:@{@"order_num":STR(orderNum)} target:self success:^(NSDictionary *success) {
                    if ([success[@"data"][@"stat"] integerValue] == 1) {
                        SHOW(@"申请成功,等待管理员审核");
                    }else{
                        SHOW(success[@"data"][@"info"]);
                    }
                } failure:^(NSError *failure) {
                }];
            }else if ([model.order_stat isEqualToString:@"3"]){//快递查询
                CCExpressDetailController *vc = [[CCExpressDetailController alloc]init];
                vc.expressNum = model.courier_number;
                vc.expressName = model.courier_company;
                [self.navigationController pushViewController:vc animated:YES];
            }
            break;
        }
    }
    
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, App_Height -108 - 49)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 110;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, -44, 0);
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
         [_tableView registerNib:[UINib nibWithNibName:@"CCMyOrderCell" bundle:nil] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}

@end
