//
//  CCMyShareHeaderView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/29.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCCategoryModel;
#import "ZYBannerView.h"
extern const CGFloat shareTopImgHeight;
extern const CGFloat shareTopBtnsHeight;
extern const CGFloat shareHeaderHeight;
extern const CGFloat shareFooterHeight;
extern const CGFloat shareSectionheight;

@protocol CCMyShareHeaderDelegate
- (void)myShareHeaderUpdateHeight;
- (void)clickAtCategory:(NSString *)typeId title:(NSString *)title;
@end

@interface CCShareHeaderView : UIView
@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (nonatomic, weak) id<CCMyShareHeaderDelegate> delegate;
@property (nonatomic, strong) ZYBannerView *topImgView;

@end
@interface CCCollectionViewHeader : UICollectionReusableView
@property (nonatomic, strong) UILabel *titleLbl;
@end
@interface CCCollectionViewFooter : UICollectionReusableView
@property (nonatomic, strong) UIImageView *moreImgView;
@end
@interface CCMyShareCollectionCell : UICollectionViewCell
@property (nonatomic, strong) CCCategoryModel *model;
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *titleLbl;
@end
