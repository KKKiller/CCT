//
//  CCOrderModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCShareDetailModel.h"
@interface CCOrderModel : NSObject
@property (nonatomic, strong) NSString *complete_time;
@property (nonatomic, strong) NSString *order_address;
@property (nonatomic, strong) NSString *order_addtime;
@property (nonatomic, strong) NSString *order_bad_pj;
@property (nonatomic, strong) NSString *order_cid;
@property (nonatomic, strong) NSString *order_count;
@property (nonatomic, strong) NSString *order_good_pj;
@property (nonatomic, strong) NSString *order_middle_pj;
@property (nonatomic, strong) NSString *order_name;
@property (nonatomic, strong) NSString *order_num;
@property (nonatomic, strong) NSString *order_phone;
@property (nonatomic, strong) NSString *order_pl;
@property (nonatomic, strong) NSString *order_pltime;
@property (nonatomic, strong) NSString *order_plzj;
@property (nonatomic, strong) NSString *order_stat; //1代发货  2已完成  3已发货  0未付款
@property (nonatomic, strong) NSString *order_total;
@property (nonatomic, strong) NSString *order_user;
@property (nonatomic, strong) NSString *orderid;
@property (nonatomic, strong) NSString *pay_type;
@property (nonatomic, strong) NSString *sp_credit;
@property (nonatomic, strong) NSString *courier_company;
@property (nonatomic, strong) NSString *courier_number;
@property (nonatomic, strong) CCShareDetailModel *pinfo;
@end
