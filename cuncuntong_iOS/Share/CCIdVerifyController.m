//
//  CCIdVerifyController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/24.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCIdVerifyController.h"
#import "CCCreditEnhanceController.h"
@interface CCIdVerifyController ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *idField;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UIImageView *img3;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *imgTap1;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *imgTap2;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *imgTap3;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn1;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn2;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn3;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (nonatomic, strong) UIImageView *currentImgView;
@property (nonatomic, strong) UIButton *currentDeleteBtn;
@property (nonatomic, strong) NSMutableArray *imgArray;
@end

@implementation CCIdVerifyController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imgArray = [NSMutableArray array];
    [self.submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.imgTap1 addTarget:self action:@selector(choosePic:)];
    [self.imgTap2 addTarget:self action:@selector(choosePic:)];
    [self.imgTap3 addTarget:self action:@selector(choosePic:)];
    [self.deleteBtn1 addTarget:self action:@selector(deleteBtn1Click:) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteBtn2 addTarget:self action:@selector(deleteBtn2Click:) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteBtn3 addTarget:self action:@selector(deleteBtn3Click:) forControlEvents:UIControlEventTouchUpInside];
    [self.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
}

- (void)submitBtnClick {
    if (self.nameField.text.length == 0) {
        SHOW(@"请输入名字");
        return;
    }
    if (self.idField.text.length == 0) {
        SHOW(@"请输入身份证号");
        return;
    }
    if (self.imgArray.count < 3) {
        SHOW(@"请完整上传图片");
        return;
    }
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"share/smrz") params:@{@"s_uid":USERID,@"s_truename":self.nameField.text,@"s_idcard":self.idField.text} target:self success:^(NSDictionary *success) {
        if([success[@"data"][@"stat"] integerValue] == 1){
            NSString *sid = [NSString stringWithFormat:@"%zd",[success[@"data"][@"sid"] integerValue]];
            [self uploadPic:sid index:0];
            [TOOL showLoading:App_Delegate.window];
        }else{
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
- (void)uploadPic:(NSString *)sid index:(NSInteger)index{
    UIImage *image = self.imgArray[index];
    index++;
    NSData *data = UIImagePNGRepresentation(image);
    NSString *zj = [NSString stringWithFormat:@"s_zj%@",@(index)];
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/smupload_pic") params:@{@"sid":sid,@"s_zj":zj} target:self imgaeData:data success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"]integerValue] == 1) {
            if (index == 3) {
                SHOW(@"提交成功，等待审核认证");
                [self.navigationController popViewControllerAnimated:YES];
                [TOOL hideLoading:App_Delegate.window];
            }else{
                [self uploadPic:sid index:index];
            }
        }else{
            NSString *msg =success[@"data"][@"info"];
            SHOW(msg);
            [TOOL hideLoading:App_Delegate.window];
        }
    } failure:^(NSError *failure) {
        SHOW(@"图片上传失败");
        
        [TOOL hideLoading:App_Delegate.window];
    }];
}
- (void)choosePic:(UITapGestureRecognizer *)recog {
    if (self.imgTap1 == recog) {
        self.currentImgView = self.img1;
        self.currentDeleteBtn = self.deleteBtn1;
    }else if (self.imgTap2 == recog){
        self.currentImgView = self.img2;
        self.currentDeleteBtn = self.deleteBtn2;
    }else{
        self.currentImgView = self.img3;
        self.currentDeleteBtn = self.deleteBtn3;
    }
      UIActionSheet  *sheet = [[UIActionSheet alloc]
                 initWithTitle:@"选择"
                 delegate:self
                 cancelButtonTitle:@"取消"
                 destructiveButtonTitle:nil
                 otherButtonTitles:@"拍照",@"从相册选择", nil];
    [sheet showInView:self.view];
}
- (void)deleteBtn1Click:(UIButton *)sender {
    sender.hidden = YES;
    for (UIImage *image in self.imgArray) {
        if ([image isEqual:self.img1.image]) {
            [self.imgArray removeObject:image];
            break;
        }
    }
    self.img1.image = IMAGENAMED(@"add_pic");
}
- (void)deleteBtn2Click:(UIButton *)sender {
    sender.hidden = YES;
    for (UIImage *image in self.imgArray) {
        if ([image isEqual:self.img2.image]) {
            [self.imgArray removeObject:image];
            break;
        }
    }
    self.img2.image = IMAGENAMED(@"add_pic");
}
- (void)deleteBtn3Click:(UIButton *)sender {
    sender.hidden = YES;
    for (UIImage *image in self.imgArray) {
        if ([image isEqual:self.img3.image]) {
            [self.imgArray removeObject:image];
            break;
        }
    }
    self.img3.image = IMAGENAMED(@"add_pic");
}
//代理方法
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
        NSUInteger sourceType = 0;
        if(buttonIndex == 2){
            return;
        }else if(buttonIndex == 1){
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }else{
             sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = sourceType;
        [self presentViewController:imagePickerController animated:YES completion:^{}];
}

//返回的图片数据
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.currentImgView.image = image;
    self.currentDeleteBtn.hidden = NO;
    //压缩图片
//    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    [self.imgArray addObject:image];
}
//关闭拍照窗口
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [TOOL hideLoading:App_Delegate.window];
}
@end
