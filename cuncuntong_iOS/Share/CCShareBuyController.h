//
//  CCShareBuyController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCShareBuyController : BaseViewController
@property (nonatomic, assign) CGFloat price;
@property (nonatomic, strong) NSString *shareId;
@end
