//
//  CCCreditRankCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCCreditRankCell.h"
@interface CCCreditRankCell ()
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *creditLbl;
@property (nonatomic, strong) UILabel *lineLbl;
@end
@implementation CCCreditRankCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUI];
    }
    return self;
}
- (void)setModel:(CCRankModel *)model {
    _model = model;
    if (model.realname.length == 11) {
        model.realname = [model.realname stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    self.titleLbl.text = [NSString stringWithFormat:@" %@ ",model.realname];
    self.creditLbl.text = [NSString stringWithFormat:@"信用分: %@",model.credit];
    [self.imgView setImageWithURL:URL(model.portrait) placeholder:IMAGENAMED(@"head")];
}
- (void)setUI {
    self.imgView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.imgView];
    self.imgView.layer.cornerRadius = 15;
    self.imgView.layer.masksToBounds = YES;
    
    self.titleLbl = [[UILabel alloc]initWithText:@"安若宸" font:14 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.titleLbl];
    
    self.creditLbl = [[UILabel alloc]initWithText:@"信用分 346" font:14 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.creditLbl];
    
    self.rankLbl = [[UILabel alloc]initWithText:@"" font:14 textColor:TEXTBLACK6];
    [self.contentView addSubview:self.rankLbl];
    
    self.lineLbl = [[UILabel alloc]initWithShallowLine];
    [self.contentView addSubview:self.lineLbl];
    
}

- (void)layoutSubviews {
    [self.rankLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.width.height.mas_equalTo(30);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imgView.mas_right).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.mas_equalTo(100);
    }];
    [self.creditLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_right).offset(25);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
}
@end
