//
//  CCHobbyModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/13.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCHobbyModel.h"
@implementation CCSubHobbyModel

@end
@implementation CCHobbyModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    // value should be Class or Class name.
    return @{@"find" : [CCSubHobbyModel class]};
}
@end
