//
//  CCShareHomeCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCShareDetailModel.h"
@interface CCShareHomeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (weak, nonatomic) IBOutlet UIView *addressLineView;
@property (weak, nonatomic) IBOutlet UIImageView *locationImgView;
@property (nonatomic, strong) CCShareDetailModel *model;
@end
