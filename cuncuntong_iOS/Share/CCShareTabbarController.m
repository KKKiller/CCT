//
//  CCShareTabbarController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareTabbarController.h"
#import "NavigationViewController.h"
#import "CCMyOrderController.h"
#import "CCChangeHomeView.h"
@interface CCShareTabbarController ()
@property (nonatomic, strong) UIButton *leftButton;
@property (strong, nonatomic) PathDynamicModal *animateModel;

@end

@implementation CCShareTabbarController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    //之前的色值0xFA6B64
    self.navigationController.navigationBar.barTintColor = MTRGB(0x5191FF);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.navigationItem.title = @"生活圈";
    [self initSelf];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"tui_"] forState:UIControlStateNormal];
    App_Delegate.shareTabbarVc = self;
    
    UIButton *changeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [changeBtn addTarget:self action:@selector(changeRootView) forControlEvents:UIControlEventTouchUpInside];
    [changeBtn setTitle:@"切换首页" forState:UIControlStateNormal];
    changeBtn.titleLabel.font = FFont(14);
    [changeBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:changeBtn];
//    self.navigationItem.rightBarButtonItem = right;
    self.navigationController.navigationBar.barTintColor = MTRGB(0xFA6B64);

}
- (void)initSelf {
    
    NSArray *vcArr = @[@"CCShareHomeController",
                       @"CCShareMyPostController",
                       @"CCPostShareController",
                       @"CCMyCollectController",
                       @"CCShareMyController"];
    
    NSArray *imageArr = @[@"shareTab_share",
                          @"shareTab_myShare",
                          @"shareTab_post",
                          @"shareTab_order",
                          @"shareTab_collect"];
    
    NSArray *selImageArr = @[@"shareTab_share_select",
                             @"shareTab_myShare_select",
                             @"shareTab_post_select",
                             @"shareTab_order_select",
                             @"shareTab_collect_select"];
    NSArray *titleArr = @[@"首页",
                          @"我的发布",
                          @"发布",
                          @"我的收藏",
                          @"我的"];
    
    NSMutableArray *tabArrs = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < vcArr.count; i++) {
        UIViewController *vc ;
        if (self.isPushed || [App_Delegate.defaultHome isEqualToString:@"default"]) {
            vc = [[NSClassFromString(vcArr[i]) alloc] init];
        }else{
            UIViewController *vvcc = [[NSClassFromString(vcArr[i]) alloc] init];
            vc = [[NavigationViewController alloc] initWithRootViewController:vvcc];
        }
        
//        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        vc.tabBarItem.image = [[UIImage imageNamed:imageArr[i]]
                                imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selImageArr[i]]
                                        imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.title = titleArr[i];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x6b6b6b)} forState:UIControlStateNormal];
        [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MTRGB(0x0b499b)} forState:UIControlStateSelected];
        
        [tabArrs addObject:vc];
    }
    
    // 解决push/pop黑影
    self.view.backgroundColor = [UIColor whiteColor];
    self.tabBar.tintColor = [UIColor greenColor];
    self.viewControllers = tabArrs;
    
}

- (void)gotoOrdering {
    self.selectedIndex = 3;
    CCMyOrderController *vc =  self.viewControllers[3];
    [vc gotoOrdering];
}
- (void)gotoOrdered {
    self.selectedIndex = 3;
    CCMyOrderController *vc =  self.viewControllers[3];
    [vc gotoOrdered];
}
-(void)backMove {
    if ([App_Delegate.window.rootViewController isEqual:self]) {
        App_Delegate.defaultHome = @"default";
        [App_Delegate setRootViewController];
    }else{
        UINavigationController *nav = self.selectedViewController;
        if (nav.viewControllers.count > 1) {
            [nav popViewControllerAnimated:YES];
        }else{
            [App_Delegate.tabbarVc gotoHome];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_leftButton addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        self.navigationItem.leftBarButtonItem = left;
    }
    return _leftButton;
}

//切换首页
- (void)changeRootView {
    CCChangeHomeView *view = [CCChangeHomeView instanceView];
    view.frame = CGRectMake(0, 0, 260, 155);
    view.title = @"生活圈";
    
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:App_Delegate.window];
}

@end
