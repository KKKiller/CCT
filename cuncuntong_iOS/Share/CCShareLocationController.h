//
//  CCShareLocationController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/9/18.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCShareDetailModel.h"
@interface CCShareLocationController : BaseViewController
@property (nonatomic, strong) CCShareDetailModel *shareModel;
@end
