//
//  AnnotationModel.h
//  NaviDemo
//
//  Created by 周吾昆 on 2018/9/9.
//  Copyright © 2018年 WZH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AnnotationModel : NSObject<MKAnnotation>

@property (nonatomic, copy) NSString *iconName;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *subtitle;


@end
