//
//  CCShareMyPostController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareMyPostController.h"
#import "CCMyPostShareCell.h"
#import "CCMyPostFooterView.h"
#import "CCShareDetailModel.h"
#import "CCMyPostModel.h"
#import "RechargeViewController.h"
#import "CCShareDetailController.h"
#import "CCPostShareController.h"
#import "CCHTMLWebviewController.h"
static NSString *CellId = @"myPostCell";
@interface CCShareMyPostController ()<UITableViewDataSource,UITableViewDelegate,CCMyPostShareCellDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger pageIndex;

@property (nonatomic, strong) CCMyPostFooterView *footer;
@property (nonatomic, strong) CCMyPostModel *myPostModel;
@property (nonatomic, assign) BOOL noMoney;

@end

@implementation CCShareMyPostController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    self.pageIndex = 1;
    [self initRefreshView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshData];
}
- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
    [self loadCount];
}
- (void)setUI {
    [self addTitle:@""];
    [self.view addSubview:self.tableView];
}
#pragma mark - 获取数据
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/myshare") params:@{@"sp_uid":USERID,@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            if (self.pageIndex == 1) {
                [self.dataArray removeAllObjects];
            }
            for (NSDictionary *dict in success[@"data"][@"info"]) {
                [self.dataArray addObject:[CCShareDetailModel modelWithJSON:dict]];
            }
            [self endLoding:success[@"data"][@"info"]];
        }else{
            [self endLoding:nil];
        }
    } failure:^(NSError *failure) {
        [self endLoding:nil];
    }];
    
}
- (void)loadCount {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/getsharegoodsinfo") params:@{@"userid":USERID,@"offset":@(self.pageIndex),@"limit":@"20"} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            self.myPostModel = [CCMyPostModel modelWithJSON:success[@"data"]];
            self.footer.model = self.myPostModel;
            self.noMoney = [self.myPostModel.generalize_money integerValue] == 0 ;
            [self.tableView reloadData];
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    if(array.count < 20){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyPostShareCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    CCShareDetailModel *model = self.dataArray[indexPath.row];
    cell.delegate = self;
    cell.noMoney = self.noMoney;
    cell.model = model;
    return cell;
}

//修改
- (void)modifyBtnClick:(CCShareDetailModel *)model {
    CCPostShareController *vc = [[CCPostShareController alloc]init];
    vc.shareModel = model;
//    vc.imageDataArray = self.imageDataArray;
    [self.navigationController pushViewController:vc animated:YES];
}
//删除
- (void)deleteBtnClick:(CCShareDetailModel *)model {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/share_del") params:@{@"sp_id":model.sp_id} target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            [self refreshData];
            SHOW(@"删除成功");
        }else{
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
    }];
}
//置顶
- (void)topBtnClick:(CCShareDetailModel *)model {
    BOOL top  = model.share_state != 1;
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharegoods") params:@{@"sp_uid":USERID,@"type":@(top),@"goods_id":STR(model.sp_id)} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            [self refreshData];
            SHOW(@"操作成功");
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
    }];
}

- (void)detailBtnClick {
    CCHTMLWebviewController *vc = [[CCHTMLWebviewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
//充值
- (void)chargeBtnClick {
    RechargeViewController *vc = [[RechargeViewController alloc]init];
    vc.placeHolder = @"充10送10，充50送80，充100送100";
    WEAKSELF
    vc.block = ^{
        [weakSelf refreshData];
    };
    vc.shareTopCharge = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCShareDetailController *vc = [[CCShareDetailController alloc]init];
        CCShareDetailModel *model = self.dataArray[indexPath.row];
        vc.shareId = model.sp_id;
        vc.isSelf = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf refreshData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat y = kSystemVersion >= 11.0 || ![App_Delegate.defaultHome isEqualToString:@"default"] ? 64 : 0;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, y, App_Width, App_Height - 49 - y)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = self.footer;
        _tableView.rowHeight = 140;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyPostShareCell" bundle:nil] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCMyPostFooterView *)footer {
    if (!_footer) {
        _footer = [CCMyPostFooterView instanceView];
        _footer.frame =  CGRectMake(0, 0, App_Width, 280);
        [_footer.detailBtn addTarget:self action:@selector(detailBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_footer.chargeBtn addTarget:self action:@selector(chargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _footer;
}


@end
