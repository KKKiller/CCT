//
//  CCShareListController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareListController.h"
#import "CCMyOrderCell.h"
#import "CCShareDetailController.h"
#import "CCShareDetailModel.h"
#import "CCMyPostOrderController.h"
static NSString *CellId = @"orderCell";
@interface CCShareListController ()<UITableViewDataSource,UITableViewDelegate,CCMyOrderCellDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CCEmptyView *emptyView;

@end

@implementation CCShareListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self initRefreshView];
    [self.tableView.mj_header beginRefreshing];
}
- (void)refreshData {
    self.pageIndex = 1;
    [self loadData];
}
- (void)setUI {
    [self addTitle: self.title ?: @"共享"];
    self.pageIndex = 1;
    [self.view addSubview:self.tableView];
    [NOTICENTER addObserver:self selector:@selector(refreshData) name:@"myShareChange" object:nil];
}
#pragma mark - 获取数据
- (void)loadData {
    if (self.typeId) {
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/sharepublish_list") params:@{@"typeid":STR(self.typeId),@"areaid":STR(self.areaId),@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
                if (self.pageIndex == 1) {
                    [self.dataArray removeAllObjects];
                }
                for (NSDictionary *dict in success[@"data"]) {
                    [self.dataArray addObject:[CCShareDetailModel modelWithJSON:dict]];
                }
            [self endLoding:success[@"data"]];
        } failure:^(NSError *failure) {
            [self endLoding:nil];
        }];
    }else{
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/myshare") params:@{@"sp_uid":USERID,@"page":@(self.pageIndex)} target:nil success:^(NSDictionary *success) {
            if ([success[@"data"][@"stat"] integerValue] == 1) {
                if (self.pageIndex == 1) {
                    [self.dataArray removeAllObjects];
                }
                for (NSDictionary *dict in success[@"data"][@"info"]) {
                    [self.dataArray addObject:[CCShareDetailModel modelWithJSON:dict]];
                }
                [self endLoding:success[@"data"][@"info"]];
            }else{
                [self endLoding:nil];
            }
        } failure:^(NSError *failure) {
            [self endLoding:nil];
        }];
        
    }
}
//停止加载
- (void)endLoding:(NSArray *)array {
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    self.emptyView.hidden = self.dataArray.count == 0 ? NO : YES;
    if(array.count < 10){
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    if(self.typeId){
        cell.payBtnHidden = YES;
    }else{
        cell.myPostList = YES;
    }
    cell.delegate = self;
    if (self.dataArray.count > indexPath.row) {
        CCShareDetailModel *model = self.dataArray[indexPath.row];
        cell.detailModel = model;
    }
    return cell;
}

#pragma mark - 代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        CCShareDetailController *vc = [[CCShareDetailController alloc]init];
        CCShareDetailModel *model = self.dataArray[indexPath.row];
        vc.shareId = model.sp_id;
        vc.isSelf = [model.sp_uid isEqualToString:USERID];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)CCMyOrderCellPayBtnClickWithOrderId:(NSString *)orderNum {
    CCMyPostOrderController *vc = [[CCMyPostOrderController alloc]init];
    vc.sp_id = orderNum;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)CCMyOrderCellComplainBtnClickWithOrderId:(NSString *)orderNum {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/share_del") params:@{@"sp_id":STR(orderNum)} target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            for (CCShareDetailModel *model in self.dataArray) {
                if ([model.sp_id isEqualToString:orderNum]) {
                    [self.dataArray removeObject:model];
                    [self.tableView reloadData];
                    SHOW(@"删除成功");
                    break;
                }
            }
        }else{
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
        
    }];
}
#pragma mark - 私有方法

-(void)initRefreshView {
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex++;
        [weakSelf loadData];
    }];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        CGFloat height = self.typeId ? App_Height : App_Height - 49;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, App_Width, height)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        if(self.typeId){
            _tableView.rowHeight = 90;
            _tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            if([[UIDevice currentDevice].systemVersion floatValue] >= 11.0){
                _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            }
        }else{
            _tableView.rowHeight = 110;
        }
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyOrderCell" bundle:nil] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:_tableView.bounds];
        [_tableView addSubview:_emptyView];
    }
    return _emptyView;
}


@end
