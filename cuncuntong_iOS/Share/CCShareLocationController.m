//
//  CCShareLocationController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/9/18.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareLocationController.h"
#import "MapTool.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "AnnotationModel.h"
#import "CCShareMapInfoView.h"
#define SCREEN_WIDTH self.view.frame.size.width
#define SCREEN_HEIGHT self.view.frame.size.height
static CGFloat mapInfoHeight = 200;
@interface CCShareLocationController ()<CLLocationManagerDelegate,MKMapViewDelegate>
@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) CLGeocoder *myGeocoder;
@property (nonatomic ,assign) CLLocationCoordinate2D ToCoords;
@property (nonatomic ,assign) CLLocationCoordinate2D FromCoords;
@property (nonatomic, strong) MKAnnotationView *annotation;
@property (nonatomic, strong) CCShareMapInfoView *mapInfoView;

@end

@implementation CCShareLocationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ToCoords = CLLocationCoordinate2DMake([self.shareModel.x_axis doubleValue], [self.shareModel.y_axis doubleValue]);
    [self setupMapView];
    
    [self addTitle:@"位置"];
    self.mapInfoView = [CCShareMapInfoView instanceView];
    self.mapInfoView.frame = CGRectMake(0,App_Height - mapInfoHeight , App_Width, mapInfoHeight);
    [self.view addSubview:self.mapInfoView];
    [self.mapInfoView.mapBtn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mapInfoView.phoneBtn addTarget:self action:@selector(phoneBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.mapInfoView.model = self.shareModel;
}
- (void)phoneBtnClick {
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",self.shareModel.sp_phone];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
    [self.view addSubview:callWebview];
    
}
- (void)btnClick{
    self.coordinate = self.ToCoords;
    [[MapTool sharedMapTool] navigationActionWithCoordinate:self.coordinate WithENDName:self.shareModel.location_info tager:self];
}
- (void)setupMapView {
    _manager = [[CLLocationManager alloc]init];
    _manager.delegate = self;
    [_manager requestAlwaysAuthorization];
    [_manager startUpdatingLocation];
    
    _mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, App_Height - mapInfoHeight - 64)];
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
//    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    
    [self.view addSubview:self.mapView];
    _mapView.delegate = self;
    
    
    _manager.desiredAccuracy = kCLLocationAccuracyBest;
    if (self.ToCoords.latitude) {
        // 自定义的大头针
        AnnotationModel *anno = [[AnnotationModel alloc]init];
        anno.coordinate = self.ToCoords;
        anno.iconName = @"arrow_location";
        [_mapView addAnnotation:anno];
//        [self.mapView setCenterCoordinate:self.ToCoords];
        
        MKCoordinateSpan theSpan;
        theSpan.latitudeDelta= 0.01;
        theSpan.longitudeDelta= 0.01;
        MKCoordinateRegion theRegion;
        theRegion.center= self.ToCoords;
        theRegion.span= theSpan;
        if (self.ToCoords.latitude < 60) {
            [self.mapView setRegion:theRegion];
            [self.mapView regionThatFits:theRegion];
        }
    }else{
        _myGeocoder = [[CLGeocoder alloc]init];
        
        [self.myGeocoder geocodeAddressString:self.shareModel.location_info
                            completionHandler:^(NSArray *placemarks,NSError *error){
                                if(nil == error && [placemarks count] > 0){
                                    CLPlacemark *pm = [placemarks objectAtIndex:0];
                                    
                                    CLLocationCoordinate2D coords=
                                    CLLocationCoordinate2DMake(pm.location.coordinate.latitude,            pm.location.coordinate.longitude);
                                    self.ToCoords = coords;
                                    float zoomLevel = 0.01;
                                    MKCoordinateRegion region = MKCoordinateRegionMake(coords, MKCoordinateSpanMake(zoomLevel, zoomLevel));
                                    [_mapView setRegion:region animated:YES];
                                    
                                    
                                    // 自定义的大头针
                                    AnnotationModel *anno = [[AnnotationModel alloc]init];
                                    anno.coordinate = self.ToCoords;
                                    anno.iconName = @"arrow_location";
                                    [_mapView addAnnotation:anno];
                                    [self.mapView setCenterCoordinate:self.ToCoords];

                                }else if([placemarks count] == 0 && error == nil){
                                    NSLog(@"找不到给定地址的经纬度");
                                }
                            }];
    }
    
    
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:   (id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    return nil;
    _annotation = [self.mapView dequeueReusableAnnotationViewWithIdentifier:@"ID"];
    
    if (_annotation == nil) {
        _annotation = [[MKAnnotationView alloc]initWithAnnotation:annotation   reuseIdentifier:@"ID"];
    }
    _annotation.canShowCallout = NO;
    // 根据模型对象，决定本大头针显示的图片样式。
    _annotation.image = [UIImage imageNamed:@"arrow_location"];
    return _annotation;
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"--%f",manager.location.coordinate.latitude);
    NSLog(@"--%f",manager.location.coordinate.longitude);
    self.FromCoords = manager.location.coordinate;
    [self.manager stopUpdatingLocation];
    
}
@end
