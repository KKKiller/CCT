//
//  CCCategoryModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCCategoryModel : NSObject
@property (nonatomic, strong) NSString *tid;
@property (nonatomic, strong) NSString *typeid;
@property (nonatomic, strong) NSString *tname;
@property (nonatomic, strong) NSString *imgurl;
@property (nonatomic, assign) NSInteger torder;
@property (nonatomic, strong) NSString *taddtime;

@end
