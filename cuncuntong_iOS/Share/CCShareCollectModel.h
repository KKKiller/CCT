//
//  CCShareCollectModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/8.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCShareDetailModel.h"
@interface CCShareCollectModel : NSObject
@property (nonatomic, assign) NSTimeInterval col_addtime;
@property (nonatomic, strong) NSString *col_id;
@property (nonatomic, strong) NSString *col_sid;
@property (nonatomic, strong) NSString *col_uid;
@property (nonatomic, strong) NSString *sp_credit;
@property (nonatomic, strong) CCShareDetailModel *shareinfo;
@end
