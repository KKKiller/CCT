//
//  CCShareHomeCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareHomeCell.h"

@implementation CCShareHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(CCShareDetailModel *)model {
    _model = model;
    NSString *url = model.sp_imgs.count > 0 ? model.sp_imgs[0] : @"";
    [self.imgView  setImageWithURL:URL(url) placeholder:IMAGENAMED(@"SubPlaceholder")];
    self.titleLbl.text = model.sp_title;
    self.detailLbl.text = model.sp_content;
    self.addressLbl.text = STR(model.location_info);
    self.locationImgView.hidden = STRINGEMPTY(model.location_info);
    self.addressLineView.hidden = model.distance == 0;
    self.distanceLbl.hidden = NO;
    if (model.distance == 0 || model.distance > 4000 * 1000) {
        self.distanceLbl.text = @"";
    }else{
        if(model.distance < 1000){
            self.distanceLbl.text = [NSString stringWithFormat:@"%.0fm",model.distance];
        }else{
            self.distanceLbl.text = [NSString stringWithFormat:@"%.0fkm",model.distance / 1000];
        }
    }
    self.priceLbl.text = [NSString stringWithFormat:@"%@元", model.sp_pricing];
}
@end
