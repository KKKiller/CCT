//
//  CCPostShareController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
@class CCShareDetailModel;
@interface CCPostShareController : BaseViewController
//@property (nonatomic, assign) BOOL isModify;
@property (nonatomic, strong) CCShareDetailModel *shareModel;
//@property (nonatomic, strong) NSMutableArray<UIImage *> *imageDataArray;
@end
