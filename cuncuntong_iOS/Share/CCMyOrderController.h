//
//  MTWFController.h
//  Mentor
//
//  Created by 我是MT on 16/7/28.
//  Copyright © 2016年 馒头科技. All rights reserved.
//

#import "BaseViewController.h"
#import "CCMyOrderedController.h"
#import "CCMyOrderingController.h"


@interface CCMyOrderController : BaseViewController
@property (strong, nonatomic) CCMyOrderedController *orderedVc;
@property (strong, nonatomic) CCMyOrderingController *orderingVc;

- (void)gotoOrdering;
- (void)gotoOrdered;
@end
