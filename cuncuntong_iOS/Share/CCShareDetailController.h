//
//  CCShareDetailController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

/**
 分享详情
 */
@interface CCShareDetailController : BaseViewController
@property (nonatomic, assign) BOOL isSelf;
@property (nonatomic, strong) NSString *shareId;
@property (nonatomic, strong) NSString *collectId;
@property (nonatomic, assign) BOOL fromCollectList;
@property (nonatomic, assign) BOOL fromHomeList;

@end
