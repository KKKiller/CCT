//
//  CCCommentModel.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/9.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCCommentModel : NSObject
@property (nonatomic, strong) NSString *order_user;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, assign) NSTimeInterval order_pltime;
@property (nonatomic, strong) NSString *order_pl;

@end
