//
//  CCShareHomeHeaderTypeCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareHomeHeaderTypeCell.h"

@implementation CCShareHomeHeaderTypeCell
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}
- (void)setUI {
    self.btn = [[UIButton alloc]initWithFrame:CGRectMake((self.width - 60)*0.5, 5, 40, 25)];
    [self.contentView addSubview:self.btn];
//    self.btn.layer.cornerRadius = 12.5;
//    self.btn.layer.masksToBounds = YES;
//    self.btn.layer.borderWidth = 0.5;
//    self.btn.layer.borderColor = MAINBLUE.CGColor;
    [self.btn setTitleColor:MAINBLUE forState:UIControlStateSelected];
    [self.btn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
//    [self.btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.btn.titleLabel.font = FFont(12);
    self.btn.userInteractionEnabled = NO;
}
- (void)setModel:(CCHobbyModel *)model {
    _model = model;
    self.btn.selected = model.user_state;
    [self.btn setTitle:model.tname forState:UIControlStateNormal];
}

- (void)setSubModel:(CCSubHobbyModel *)subModel {
    _subModel = subModel;
    [self.btn setTitle:subModel.tname forState:UIControlStateNormal];
}
@end
