//
//  CCShareDetailFooterView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/21.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareDetailFooterView.h"

@implementation CCShareDetailFooterView

+ (instancetype)instanceView {
    CCShareDetailFooterView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
//    view.layer.cornerRadius = 5;
//    view.layer.masksToBounds = YES;
//    view.layer.borderColor = BACKGRAY.CGColor;
//    view.layer.borderWidth = 0.5;
    return view;
}
- (void)setVisitCount:(NSString *)visitCount {
    _visitCount = visitCount;
    self.visitLbl.text = [NSString stringWithFormat:@"浏览次数：%@次",visitCount];
}
- (void)configCommentPercentWithGood:(NSString *)good normal:(NSString *)normal bad:(NSString *)bad {
    CGFloat goodCount = [good integerValue];
    CGFloat normalCount  = [normal integerValue];
    CGFloat badCount = [bad integerValue];
    CGFloat total = goodCount + normalCount + badCount;
    if (total == 0) {
        self.goodLbl.text = @"100%";
    }else{
        self.goodLbl.text = [NSString stringWithFormat:@"%.1f%%",goodCount == 0 ? 0 : goodCount / total * 100];
    }
    self.normalLbl.text = [NSString stringWithFormat:@"%.1f%%", normalCount == 0 ? 0 : normalCount / total * 100];
    self.badLbl.text = [NSString stringWithFormat:@"%.1f%%",badCount == 0 ? 0 : badCount / total * 100];
}
@end
