//
//  CCShareDetailBottomView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareDetailBottomView.h"
@interface CCShareDetailBottomView()
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *modifyBtn;
@property (weak, nonatomic) IBOutlet UIButton *buyAndRefreshBtn;

@end
@implementation CCShareDetailBottomView
- (void)awakeFromNib {
    [super awakeFromNib];
    [self.modifyBtn addTarget:self action:@selector(modifyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.buyAndRefreshBtn addTarget:self action:@selector(buyAndRefreshBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
}
- (void)setPrice:(NSString *)price {
    _price = price;
    self.priceLbl.text = [NSString stringWithFormat:@"报价: ¥ %@元",price];
}
- (void)setIsSelf:(BOOL)isSelf {
    _isSelf = isSelf;
    self.priceLbl.hidden = isSelf;
    self.modifyBtn.hidden = !isSelf;
   
    if(isSelf){
        self.buyAndRefreshBtn.backgroundColor = MTRGB(0x03f3c9);
        [self.buyAndRefreshBtn setTitle:@"刷新" forState:UIControlStateNormal];
    }else{
        [self.buyAndRefreshBtn setTitle:@"订购" forState:UIControlStateNormal];
       self.buyAndRefreshBtn.backgroundColor = MTRGB(0xffcf0f);
    }
}
- (void)modifyBtnClick {
    if ([self.delegate respondsToSelector:@selector(CCShareDetailBottomViewClickAtModifyBtn)]) {
        [self.delegate CCShareDetailBottomViewClickAtModifyBtn];
    }
}
- (void)buyAndRefreshBtnClick {
    if ([self.delegate respondsToSelector:@selector(CCShareDetailBottomViewClickbuyAndRefreshBtn)]) {
        [self.delegate CCShareDetailBottomViewClickbuyAndRefreshBtn];
    }
}
@end
