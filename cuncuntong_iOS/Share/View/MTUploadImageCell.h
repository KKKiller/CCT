//
//  MTUploadImageCell.h
//  Mentor
//
//  Created by 我是MT on 16/4/19.
//  Copyright © 2016年 馒头科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTUploadImageCell : UICollectionViewCell

@property(nonatomic, strong) UIButton *imageViewBtn;
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UIButton *cancleBtn;
@property(nonatomic, strong) UIImage *image;
@end
