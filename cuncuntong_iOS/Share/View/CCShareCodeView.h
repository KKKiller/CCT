//
//  CCShareCodeView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/20.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCShareCodeView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
+ (instancetype)instanceView;
@end
