//
//  CCMyOrderCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/26.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCMyOrderCell.h"
@interface CCMyOrderCell()
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *creditLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property (weak, nonatomic) IBOutlet UIButton *complainBtn;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;
@property (weak, nonatomic) IBOutlet UILabel *numDotLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numDotWidth;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLbl;
@property (weak, nonatomic) IBOutlet UIButton *applyDrawback;

@end
@implementation CCMyOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.complainBtn addTarget:self action:@selector(complainBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.applyDrawback addTarget:self action:@selector(drawback) forControlEvents:UIControlEventTouchUpInside];
    self.applyDrawback.hidden =YES;

}
- (void)setPayBtnText:(NSString *)payBtnText {
    _payBtnText = payBtnText;
    [self.payBtn setTitle:payBtnText forState:UIControlStateNormal];
}
- (void)payBtnClick {
    if ([self.delegate respondsToSelector:@selector(CCMyOrderCellPayBtnClickWithOrderId:)]) {
        if (self.myPostList) {
            [self.delegate CCMyOrderCellPayBtnClickWithOrderId:self.detailModel.sp_id];
        }else{ //订单列表 确认支付
            if ([self.order_stat isEqualToString:@"3"]) { //已发货
                [self.delegate CCMyOrderCellPayBtnClickWithOrderId:self.orderNum];
            }
        }
    }
}
- (void)complainBtnClick {
    if ([self.delegate respondsToSelector:@selector(CCMyOrderCellComplainBtnClickWithOrderId:)]) {
        if (self.myPostList) {
             [self.delegate CCMyOrderCellComplainBtnClickWithOrderId:self.detailModel.sp_id];
        }else{
            [self.delegate CCMyOrderCellComplainBtnClickWithOrderId:self.orderNum];
        }
    }
}
- (void)drawback {
    if ([self.delegate respondsToSelector:@selector(applyDrawbackWithOrderId:)]) {
        [self.delegate applyDrawbackWithOrderId:self.orderNum];
    }
}
- (void)setFinished:(BOOL)finished {
    
    [self.payBtn setTitle:@"已支付" forState:UIControlStateNormal];
    self.payBtn.backgroundColor = [UIColor lightGrayColor];
    self.complainBtn.hidden = YES;
}
- (void)setPayBtnHidden:(BOOL)payBtnHidden {
    _payBtnHidden = payBtnHidden;
    self.payBtn.hidden = payBtnHidden;
    self.complainBtn.hidden = YES;
}

//发布列表
- (void)setMyPostList:(BOOL)myPostList {
    _myPostList = myPostList;
    [self.complainBtn setTitle:@"删除共享" forState:UIControlStateNormal];
    [self.complainBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"已售列表" forState:UIControlStateNormal];
}
//收藏列表
- (void)setCollectModel:(CCShareCollectModel *)collectModel{
    _collectModel = collectModel;
    self.titleLbl.text = collectModel.shareinfo.sp_title;
    if (collectModel.shareinfo.sp_imgs.count > 0) {
        [self.headImgView setImageWithURL:URL(collectModel.shareinfo.sp_imgs[0]) placeholder:IMAGENAMED(@"head")];
    }
    self.descLbl.text = collectModel.shareinfo.sp_content;
    self.priceLbl.text = [NSString stringWithFormat:@"定价: ¥%@元", collectModel.shareinfo.sp_pricing];
    self.creditLbl.text = [NSString stringWithFormat:@"信用值: %@",collectModel.sp_credit];
}
//共享列表,进行中 已结束
- (void)setDetailModel:(CCShareDetailModel *)detailModel {
    _detailModel  = detailModel;
    self.titleLbl.text = detailModel.sp_title;
    if (detailModel.sp_imgs.count > 0) {
        [self.headImgView setImageWithURL:URL(detailModel.sp_imgs[0]) placeholder:IMAGENAMED(@"head")];
    }
    self.descLbl.text = detailModel.sp_content;
    self.priceLbl.text = [NSString stringWithFormat:@"定价: ¥%@元", detailModel.sp_pricing];
    if (detailModel.sp_credit) {
        self.creditLbl.text = [NSString stringWithFormat:@"信用值: %@",detailModel.sp_credit];
    }else if (self.order_count){
        NSInteger totalMoney = [self.order_count integerValue] * [detailModel.sp_pricing integerValue];
       self.creditLbl.text = [NSString stringWithFormat:@"订购数:%@\n  总金额:¥%@元",self.order_count,@(totalMoney)];
    }
    if (self.myPostList && [detailModel.sp_saleCount integerValue] > 0) {
//        [self.payBtn setTitle:[NSString stringWithFormat:@"已售列表 %@",detailModel.sp_saleCount] forState:UIControlStateNormal];
        self.numDotLbl.text = detailModel.sp_saleCount;
        self.numDotLbl.hidden = !([detailModel.sp_saleCount integerValue] > 0);
        self.numDotWidth.constant = detailModel.sp_saleCount.length > 1 ? 12 * detailModel.sp_saleCount.length + 6 : 16;
    }
    if (self.order_stat) {
        self.stateBtn.hidden = NO;
         NSString *state = [self.order_stat isEqualToString:@"1"] ? @"待发货" : [self.order_stat isEqualToString:@"3"] ? @"已发货" : @"已完成";
        [self.stateBtn setTitle:state forState:UIControlStateNormal];
        self.applyDrawback.hidden = [self.order_stat isEqualToString:@"1"];
        NSString *title = [self.order_stat isEqualToString:@"2"] ? @"申请退款" : @"快递查询";
        [self.applyDrawback setTitle:title forState:UIControlStateNormal];
    }
    //已发货,显示确认支付按钮,其他情况隐藏
    if(!self.myPostList){
        self.payBtn.hidden = ![self.order_stat isEqualToString:@"3"];
    }
    
}
//购买订单列表
- (void)setPostOrderModel:(CCPostOederModel *)postOrderModel {
    _postOrderModel = postOrderModel;
    self.titleLbl.text = postOrderModel.pinfo.sp_title;
    [self.headImgView setImageWithURL:URL(postOrderModel.avatar) placeholder:IMAGENAMED(@"head")];
    self.descLbl.text = [NSString stringWithFormat:@"收货地址: %@",postOrderModel.order_address];
    self.creditLbl.text = [NSString stringWithFormat:@"姓名: %@",postOrderModel.order_name];
    self.priceLbl.text = [NSString stringWithFormat:@"%@",postOrderModel.order_phone];
    self.creditLbl.textColor = TEXTBLACK6;
    self.priceLbl.textColor = TEXTBLACK6;
    self.descLbl.numberOfLines = 0;
    self.orderNumLbl.hidden = NO;
    NSInteger totalMoney = [postOrderModel.order_count integerValue] * [postOrderModel.pinfo.sp_pricing integerValue];
    self.orderNumLbl.text = [NSString stringWithFormat:@"订购数:%@\n  总金额:¥%@元",postOrderModel.order_count,@(totalMoney)];
    self.complainBtn.hidden = YES;
    NSString *state = [postOrderModel.order_stat isEqualToString:@"1"] ? @"待发货" : [postOrderModel.order_stat isEqualToString:@"3"] ? @"已发货" : @"已完成";
    [self.payBtn setTitle:state forState:UIControlStateNormal];
}
- (void)setCredit:(NSString *)credit {
    self.creditLbl.text = [NSString stringWithFormat:@"信用值: %@",credit];
}
@end
