//
//  CCShareCategoryCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/27.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareCategoryCell.h"
@interface CCShareCategoryCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end
@implementation CCShareCategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
