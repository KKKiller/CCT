//
//  CCShareDetailHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareDetailHeaderView.h"
#import "CCShareDetailImageCollectionViewCell.h"
#import "CCShareIssueController.h"
#import "CCShareDetailModel.h"
@interface CCShareDetailHeaderView()<UICollectionViewDelegate,UICollectionViewDataSource>
//@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UIImageView *topImgView;
@property (weak, nonatomic) IBOutlet UIView *addressContainerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTopMargin;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *onlineBtn;
@property (weak, nonatomic) IBOutlet UILabel *sharePriceLbl;

@property (weak, nonatomic) IBOutlet UIView *picsContainerView;
@property (weak, nonatomic) IBOutlet UILabel *goodLbl;
@property (weak, nonatomic) IBOutlet UILabel *normalLbl;
@property (weak, nonatomic) IBOutlet UILabel *badLbl;

@property (strong, nonatomic) UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *noCommentLbl;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailLineTopMargin;
@property (weak, nonatomic) IBOutlet UILabel *noPicLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (weak, nonatomic) IBOutlet UIButton *locateBtn;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moreBtnTopMargin;

@property (nonatomic, strong) NSMutableArray *imgArray;
//@property (weak, nonatomic) IBOutlet UIButton *locationBtn;

@end
@implementation CCShareDetailHeaderView

+ (instancetype)instanceView {
    return [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self creatViews];
//    [self.showDetailBtn addTarget:self action:@selector(showDetail:) forControlEvents:UIControlEventTouchUpInside];
    [self.phoneBtn addTarget:self action:@selector(phoneBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.onlineBtn addTarget:self action:@selector(onlineBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.locateBtn addTarget:self action:@selector(locationBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.addressContainerView.layer.cornerRadius = 6;
    self.addressContainerView.layer.masksToBounds = YES;
    self.addressContainerView.layer.borderColor = BACKGRAY.CGColor;
    self.addressContainerView.layer.borderWidth = 1;
}

- (void)setModel:(CCShareDetailModel *)model {
    _model = model;
    self.titleLbl.text = model.sp_title;
    self.detailLbl.text = model.sp_content;
    self.sharePriceLbl.text = [NSString stringWithFormat:@"报价:¥%@元",model.sp_pricing];
    self.topImgView.hidden = model.sp_imgs.count == 0;
    if (model.sp_imgs.count > 0) {
        [self.topImgView setImageWithURL:URL(model.sp_imgs[0]) options:0];
    }else{
        self.titleTopMargin.constant = 10;
    }
//    [self.headImgView setImageURL:URL(model.avatar)];
    if (model.sp_imgs) {
        self.imgArray = model.sp_imgs.mutableCopy;
        [self.collectionView reloadData];
    }
    self.addressLbl.text = model.location_info;
    if (model.distance == 0) {
        self.distanceLbl.text = @"";
    }else{
        self.distanceLbl.text = [NSString stringWithFormat:@"距离：%.0fkm", model.distance / 1000];        
    }
    [self.picsContainerView bringSubviewToFront:self.noPicLbl];
    self.noPicLbl.hidden = self.imgArray.count != 0;
    CGFloat textHeight = [model.sp_content sizeForFont:FFont(15) size:CGSizeMake(App_Width  - 30, CGFLOAT_MAX) mode:0].height;
    if ([self.delegate respondsToSelector:@selector(updateHeaderHeight:)]) {
        [self.delegate updateHeaderHeight:textHeight];
    }

    self.sharePriceLbl.hidden = [model.sp_sign isEqualToString:@"26"];
}

- (void)configCommentPercentWithGood:(NSString *)good normal:(NSString *)normal bad:(NSString *)bad {
    CGFloat goodCount = [good integerValue];
    CGFloat normalCount  = [normal integerValue];
    CGFloat badCount = [bad integerValue];
    CGFloat total = goodCount + normalCount + badCount;
    if (total == 0) {
        self.goodLbl.text = @"100%";
    }else{
        self.goodLbl.text = [NSString stringWithFormat:@"%.1f%%",goodCount == 0 ? 0 : goodCount / total * 100];
    }
    self.normalLbl.text = [NSString stringWithFormat:@"%.1f%%", normalCount == 0 ? 0 : normalCount / total * 100];
    self.badLbl.text = [NSString stringWithFormat:@"%.1f%%",badCount == 0 ? 0 : badCount / total * 100];
}

//定位
- (void)locationBtnClick {
    if([self.delegate respondsToSelector:@selector(shareDetailHeaderClickAtLocationBtn)]){
        [self.delegate shareDetailHeaderClickAtLocationBtn];
    }
}
//电话咨询
- (void)phoneBtnClick {
    if (self.model.sp_phone) {
        NSString *phone = self.model.sp_phone;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:phone message:nil preferredStyle:1];
        //18039290408
        UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                    {
                                        NSString *allString = [NSString stringWithFormat:@"tel:%@",phone];
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                    }];
        //取消栏
        UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
        }];
        [alertController addAction:chooseOne];
        [alertController addAction:cancelOne];
        [App_Delegate.tabbarVc presentViewController:alertController animated:YES completion:nil];
    }
}
//在线咨询
- (void)onlineBtnClick {
    if([self.delegate respondsToSelector:@selector(shareDetailHeaderClickAtOnlineChatBtn)]){
        [self.delegate shareDetailHeaderClickAtOnlineChatBtn];
    }
}
- (void)setExistComment:(BOOL)existComment {
    _existComment = existComment;
//    self.noCommentLbl.hidden = existComment;
}

- (void)creatViews {
    
    //collectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(100, 100);
    layout.minimumLineSpacing = 20;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 5, App_Width,100 ) collectionViewLayout:layout];
    [self.picsContainerView addSubview:self.collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[CCShareDetailImageCollectionViewCell class] forCellWithReuseIdentifier:@"CCShareDetailImageCollectionViewCell"];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
//    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.backgroundColor = WHITECOLOR;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.imgArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CCShareDetailImageCollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"CCShareDetailImageCollectionViewCell" forIndexPath:indexPath];
    if (self.imgArray.count > indexPath.row) {
        cell.urlStr = self.imgArray[indexPath.row];
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.imgArray.count > indexPath.row) {
        if ([self.delegate respondsToSelector:@selector(shareDetailHeaderdidClickImageAtIndex:)]) {
            [self.delegate shareDetailHeaderdidClickImageAtIndex:indexPath.row];
        }
    }
}

@end
