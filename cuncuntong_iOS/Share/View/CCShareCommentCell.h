//
//  CCShareCommentCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCCommentModel.h"
@interface CCShareCommentCell : UITableViewCell
@property (nonatomic, strong) CCCommentModel *model;
@end
