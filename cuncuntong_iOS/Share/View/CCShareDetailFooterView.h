//
//  CCShareDetailFooterView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/21.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCShareDetailFooterView : UIView
@property (weak, nonatomic) IBOutlet UIButton *goodBtn;
@property (weak, nonatomic) IBOutlet UIButton *normalBtn;
@property (weak, nonatomic) IBOutlet UIButton *badBtn;
@property (weak, nonatomic) IBOutlet UILabel *goodLbl;
@property (weak, nonatomic) IBOutlet UILabel *normalLbl;
@property (weak, nonatomic) IBOutlet UILabel *badLbl;
@property (weak, nonatomic) IBOutlet UILabel *visitLbl;

+ (instancetype)instanceView;
- (void)configCommentPercentWithGood:(NSString *)good normal:(NSString *)normal bad:(NSString *)bad;
@property (nonatomic, strong) NSString *visitCount;

@end
