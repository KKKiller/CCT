//
//  CCShareCodeView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/20.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareCodeView.h"

@implementation CCShareCodeView

+ (instancetype)instanceView {
    CCShareCodeView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    
    return view;
}

@end
