//
//  MTMyClassCollectionViewCell.h
//  Mentor
//
//  Created by 我是MT on 17/2/21.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YYControl;
@interface CCShareDetailImageCollectionViewCell : UICollectionViewCell
@property(strong,nonatomic)YYControl *backImgView;
@property (nonatomic, strong) NSString *urlStr;
@end
