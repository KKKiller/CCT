//
//  CCMyCollectCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCMyCollectCell.h"

@implementation CCMyCollectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(CCShareCollectModel *)model {
    _model = model;
    self.titleLbl.text = model.shareinfo.sp_title;
    if (model.shareinfo.sp_imgs.count > 0) {
        [self.imgView setImageWithURL:URL(model.shareinfo.sp_imgs[0]) placeholder:IMAGENAMED(@"head")];
    }
    self.descLbl.text = model.shareinfo.sp_content;
    self.priceLbl.text = [NSString stringWithFormat:@"定价: ¥%@元", model.shareinfo.sp_pricing];
    if (model.shareinfo.distance == 0) {
        self.distanceLbl.text = @"";
    }else{
        if(model.shareinfo.distance > 100){
            self.distanceLbl.text = @">100km";
        }else{
            self.distanceLbl.text = [NSString stringWithFormat:@"%.0fkm",model.shareinfo.distance];
        }
    }
}

@end
