//
//  CCChangeHomeView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCChangeHomeView.h"

@implementation CCChangeHomeView

+ (instancetype)instanceView {
    CCChangeHomeView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.layer.cornerRadius = 4;
    view.layer.masksToBounds = YES;
    
    [view.defaultHome addTarget:view action:@selector(defaultHomeClick) forControlEvents:UIControlEventTouchUpInside];
    [view.lifeCircleHome addTarget:view action:@selector(lifeCircleHomeClick) forControlEvents:UIControlEventTouchUpInside];
    [view.setBtn addTarget:view action:@selector(setBtnClick) forControlEvents:UIControlEventTouchUpInside];
    view.defaultHome.selected = YES;
    
    view.setBtn.layer.cornerRadius = 4;
    view.setBtn.layer.masksToBounds = YES;
    
    return view;
}
- (void)setTitle:(NSString *)title {
    _title = title;
    [self.lifeCircleHome setTitle:[NSString stringWithFormat:@"设置%@为首页",title] forState:UIControlStateNormal];
}
- (void)defaultHomeClick{
    self.defaultHome.selected = YES;
    self.lifeCircleHome.selected = NO;
}
- (void)lifeCircleHomeClick {
    self.lifeCircleHome.selected = YES;
    self.defaultHome.selected = NO;
    
}
- (void)setBtnClick {
    NSString *home = self.lifeCircleHome.selected ? self.title : @"default";
    [Def setValue:home forKey:@"defaultHome"];
    App_Delegate.defaultHome = home;
    [App_Delegate setRootViewController];
}
@end
