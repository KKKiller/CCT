//
//  MTMyClassCollectionViewCell.m
//  Mentor
//
//  Created by 我是MT on 17/2/21.
//  Copyright © 2017年 馒头科技. All rights reserved.
//

#import "CCShareDetailImageCollectionViewCell.h"
#import "YYControl.h"
@interface CCShareDetailImageCollectionViewCell()
@end
@implementation CCShareDetailImageCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatViews];
        self.contentView.backgroundColor = WHITECOLOR;
        self.contentView.layer.masksToBounds = YES;
    }
    return self;
}
//
//- (void)setModel:(MTRecommendCourseModel *)model {
//    _model = model;
//    [self.backImgView setImageWithURL:URL(model.goods_cover) placeholder:IMAGENAMED(@"placeHolder_homeCard")];
//}
- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr;
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.contentView addSubview:imgV];
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    [imgV setImageWithURL:URL(urlStr) placeholder:IMAGENAMED(@"pic") options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        self.backImgView.image = image;
    }];
}
- (void)creatViews {
    self.backImgView = [[YYControl alloc]init];
    self.backImgView.layer.masksToBounds = YES;
    self.backImgView.image = IMAGENAMED(@"pic");
    self.backImgView.clipsToBounds = YES;
    self.backImgView.exclusiveTouch = YES;
    self.backImgView.userInteractionEnabled = NO;
    [self.contentView addSubview:self.backImgView];
}

- (void)layoutSubviews {
    [self.backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.top.equalTo(self.contentView.mas_top);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}
@end
