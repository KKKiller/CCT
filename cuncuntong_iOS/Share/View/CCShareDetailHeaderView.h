//
//  CCShareDetailHeaderView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YYControl;
@class CCShareDetailModel;
@protocol CCShareDetailHeaderDelegate <NSObject>

- (void)updateHeaderHeight:(CGFloat)height;
- (void)shareDetailHeaderdidClickImageAtIndex:(NSInteger)index;
- (void)shareDetailHeaderClickAtOnlineChatBtn;
- (void)shareDetailHeaderClickAtLocationBtn;

@end

@interface CCShareDetailHeaderView : UIView
+ (instancetype)instanceView;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *goodTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *normalTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *badTap;
@property (nonatomic, assign, getter=isExistComment) BOOL existComment;
@property (nonatomic, weak) id<CCShareDetailHeaderDelegate> delegate;
@property (nonatomic, strong) CCShareDetailModel *model;
- (void)configCommentPercentWithGood:(NSString *)good normal:(NSString *)normal bad:(NSString *)bad;
@end
