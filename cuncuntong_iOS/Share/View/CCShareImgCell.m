//
//  CCShareImgCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/21.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareImgCell.h"

@implementation CCShareImgCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setUrl:(NSString *)url {
    _url = url;
    [self.imgView setImageWithURL:URL(url) placeholder:IMAGENAMED(@"MainPlaceholder")];
}

@end
