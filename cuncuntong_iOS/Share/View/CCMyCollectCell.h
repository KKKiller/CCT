//
//  CCMyCollectCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/9.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCShareCollectModel.h"
@interface CCMyCollectCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (nonatomic, strong) CCShareCollectModel *model;
@end
