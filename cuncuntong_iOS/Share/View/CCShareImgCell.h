//
//  CCShareImgCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/21.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCShareImgCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) NSString *url;
@end
