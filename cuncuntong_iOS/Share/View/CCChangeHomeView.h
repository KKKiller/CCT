//
//  CCChangeHomeView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/8.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCChangeHomeView : UIView
@property (weak, nonatomic) IBOutlet UIButton *defaultHome;
@property (weak, nonatomic) IBOutlet UIButton *lifeCircleHome;
@property (weak, nonatomic) IBOutlet UIButton *setBtn;
+ (instancetype)instanceView ;
@property (nonatomic, strong) NSString *title;

@end
