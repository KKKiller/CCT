//
//  CCShareCommentCell.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareCommentCell.h"

@interface CCShareCommentCell()
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentLbl;

@end
@implementation CCShareCommentCell
- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
- (void)setModel:(CCCommentModel *)model {
    _model = model;
    [self.headImgView setImageWithURL:URL(model.avatar) placeholder:IMAGENAMED(@"head")];
    self.titleLbl.text = model.name;
    self.timeLbl.text = [TOOL convertIntavalTime:model.order_pltime];
    self.commentLbl.text = model.order_pl;
}

@end
