//
//  CCMyOrderCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/26.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCShareCollectModel.h"
#import "CCShareDetailModel.h"
#import "CCOrderModel.h"
#import "CCPostOederModel.h"
#import "CCWineOrderModel.h"
@protocol CCMyOrderCellDelegate <NSObject>
@optional
- (void)CCMyOrderCellPayBtnClickWithOrderId:(NSString *)orderNum;
- (void)CCMyOrderCellComplainBtnClickWithOrderId:(NSString *)orderNum;
- (void)CCMyOrderCellStateBtnClickWithOrderId:(NSString *)orderNum;
- (void)applyDrawbackWithOrderId:(NSString *)orderNum;
@end
@interface CCMyOrderCell : UITableViewCell
@property (nonatomic, strong) NSString *payBtnText;
@property (nonatomic, assign) BOOL payBtnHidden;
@property (nonatomic, assign) BOOL myPostList; //我的共享列表
@property (nonatomic, assign) BOOL finished;//我的已完成订单

@property (nonatomic, strong) CCShareCollectModel *collectModel;
@property (nonatomic, strong) CCShareDetailModel *detailModel;
@property (nonatomic, strong) CCPostOederModel *postOrderModel;
//@property (nonatomic, strong) CCWineOrderModel *wineModel;
@property (nonatomic, strong) NSString *orderNum;
@property (nonatomic, strong) NSString *credit;
@property (nonatomic, strong) NSString *order_count;
@property (nonatomic, strong) NSString *order_stat;// 0 未付款 1 待发货 3 已发货 2 交易完成 
@property (nonatomic, weak) id<CCMyOrderCellDelegate> delegate;
@end
