//
//  CCShareDetailBottomView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CCShareDetailBottomViewDelegate <NSObject>

- (void)CCShareDetailBottomViewClickAtModifyBtn;
- (void)CCShareDetailBottomViewClickbuyAndRefreshBtn;

@end
@interface CCShareDetailBottomView : UIView
@property (nonatomic, assign) BOOL isSelf;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, weak) id<CCShareDetailBottomViewDelegate> delegate;
+ (instancetype)instanceView;
@end
