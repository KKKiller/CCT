//
//  CCShareIssueController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCShareIssueController : BaseViewController

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *orderNum;
@end
