//
//  CCShareHomeHeaderView.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareHomeHeaderView.h"
#import "CCShareHomeHeaderTypeCell.h"
#import "CCHobbyModel.h"
@interface CCShareHomeHeaderView () <UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@end
@implementation CCShareHomeHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUI];
        [self creatBtns];
        [self layoutSubView];
        self.backgroundColor = WHITECOLOR;
        _hobbyArray = [NSMutableArray array];
//        NSArray *array = @[@"服装",@"美食",@"招聘",@"房产",@"交友",@"车辆",@"宾馆",@"其他"];
//        for (NSString *str in array) {
//            CCHobbyModel *Model = [[CCHobbyModel alloc]init];
//            Model.tname = str;
//            [_hobbyArray addObject:Model];
//        }
    }
    return self;
}
- (void)setHobbyArray:(NSMutableArray *)hobbyArray {
    _hobbyArray = hobbyArray;
    [self.collectionView reloadData];
}
- (void)setUI {
    self.btnsContainerV = [[UIView alloc]init];
    [self addSubview:self.btnsContainerV];
    
    [self setSearchBar];
    [self addSubview:self.collectionView];
    [self addSubview:self.subCollectionView];

    self.bottomLineView = [[UIView alloc]init];
    [self addSubview:self.bottomLineView];
    self.bottomLineView.backgroundColor = BACKGRAY;
}
- (void)creatBtns {
    CGFloat width = 30;
    CGFloat margin = (App_Width - 30 - width * 6) / 5.0;
    NSArray *titleArray = @[@"种金豆",@"酒老大",@"货运通",@"聊聊",@"3+N",@"红包"];
    for (int i = 0; i<titleArray.count; i++) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(15 +i*(width + margin), 10, width, width)];
        btn.tag = 100+i;
        NSString *imgName = [NSString stringWithFormat:@"top_%@",@(i)];
        [btn setImage: IMAGENAMED(imgName) forState:UIControlStateNormal];
        [self.btnsContainerV addSubview:btn];
        [btn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLbl = [[UILabel alloc]initWithText:titleArray[i] font:12 textColor:TEXTBLACK3];
        titleLbl.textAlignment = NSTextAlignmentCenter;
        [self.btnsContainerV addSubview:titleLbl];
        [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(btn.mas_bottom).offset(0);
            make.centerX.equalTo(btn.mas_centerX);
        }];
        
    }
}
- (void)homeBtnClick:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(clickTopBtnAtIndex:)] ){
        [self.delegate clickTopBtnAtIndex:sender.tag - 100];
    }
}


- (void)setSearchBar
{
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(5, 64 + 5,App_Width - 5 - 110, 30)];
    searchBar.layer.borderColor = BACKGRAY.CGColor;
    searchBar.layer.borderWidth  = 1;
    searchBar.placeholder = @"搜你所思 得你所想";
    searchBar.delegate = self;
    //光标颜色
    searchBar.tintColor = MAINBLUE;
    //拿到searchBar的输入框
    UITextField *searchTextField = [searchBar valueForKey:@"_searchField"];
    //字体大小
    searchTextField.font = [UIFont systemFontOfSize:13];
    
    [self addSubview:searchBar];
    self.searchBar = searchBar;
    
    UIView *firstSubView = self.searchBar.subviews.firstObject;
    UIView *backgroundImageView = [firstSubView.subviews firstObject];
    [backgroundImageView removeFromSuperview];
    
    
    [self addSearchBtns];
}
- (void)addSearchBtns {
    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.searchBar.width + self.searchBar.x, self.searchBar.y, 50, self.searchBar.height)];
    [self addSubview:self.searchBtn];
    [self.searchBtn addTarget:self action:@selector(searchLifeCircle) forControlEvents:UIControlEventTouchUpInside];
    [self.searchBtn setTitle:@"生活圈" forState:UIControlStateNormal];
    [self.searchBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = FFont(13);
    self.searchBtn.backgroundColor  = MAINBLUE;
    
    
    self.baiduBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.searchBtn.frame), self.searchBar.y, self.searchBtn.width, self.searchBar.height)];
    [self addSubview:self.baiduBtn];
    [self.baiduBtn addTarget:self action:@selector(searchBaidu) forControlEvents:UIControlEventTouchUpInside];
    [self.baiduBtn setTitle:@"百度" forState:UIControlStateNormal];
    [self.baiduBtn setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    self.baiduBtn.titleLabel.font = FFont(13);
    self.baiduBtn.backgroundColor = MTRGB(0xfd7940);
}
- (void)searchLifeCircle {
    if (self.searchBar.text) {
        if ([self.delegate respondsToSelector:@selector(searchWithText:)]) {
            [self.delegate searchWithText:self.searchBar.text];
        }
    }else{
        SHOW(@"请输入搜索内容");
    }
}
- (void)searchBaidu {
    if (self.searchBar.text) {
        if ([self.delegate respondsToSelector:@selector(baiduWithText:)]) {
            [self.delegate baiduWithText:self.searchBar.text];
        }
    }else{
        SHOW(@"请输入搜索内容");
    }
}


- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CGFloat width =  35;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(width, 25);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame: CGRectMake(0, 45, self.width, 25) collectionViewLayout:layout];
        collectionView.backgroundColor = WHITECOLOR;
        _collectionView = collectionView;
        _collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0,10);
        [_collectionView registerClass:[CCShareHomeHeaderTypeCell class] forCellWithReuseIdentifier:@"CCShareHomeHeaderTypeCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}
- (UICollectionView *)subCollectionView {
    if (!_subCollectionView) {
        CGFloat width =  35;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(width, 25);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame: CGRectMake(0, CGRectGetMaxY(self.collectionView.frame), self.width, 25) collectionViewLayout:layout];
        collectionView.backgroundColor = WHITECOLOR;
        _subCollectionView = collectionView;
        _subCollectionView.contentInset = UIEdgeInsetsMake(0, 15, 0,15);
        [_subCollectionView registerClass:[CCShareHomeHeaderTypeCell class] forCellWithReuseIdentifier:@"CCShareHomeHeaderTypeCell"];
        _subCollectionView.delegate = self;
        _subCollectionView.dataSource = self;
    }
    return _subCollectionView;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.collectionView) {
        return  self.hobbyArray.count;
    }else{
        if (self.hobbyArray.count > self.selectedIndex) {
            CCHobbyModel *model = self.hobbyArray[self.selectedIndex];
            return model.find.count;
        }
    }
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CCShareHomeHeaderTypeCell *cell;
    if (collectionView == self.collectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CCShareHomeHeaderTypeCell" forIndexPath:indexPath];
        CCHobbyModel *model = self.hobbyArray[indexPath.row];
        cell.model = model;
    }else{
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CCShareHomeHeaderTypeCell" forIndexPath:indexPath];
        CCHobbyModel *model = self.hobbyArray[self.selectedIndex];
        CCSubHobbyModel *subModel = model.find[indexPath.row];
        cell.subModel = subModel;
        [cell.btn setTitleColor:TEXTBLACK6 forState:UIControlStateNormal];
        if (self.selectedSubIndex == indexPath.row) {
            [cell.btn setTitleColor:MAINBLUE forState:UIControlStateNormal];
        }
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.collectionView == collectionView) {
        if ([self.delegate respondsToSelector:@selector(clickHobbyAtIndex:)]) {
            [self.delegate clickHobbyAtIndex:indexPath.row];
        }
        self.selectedIndex = indexPath.row;
        self.selectedSubIndex = -1;
        [self.subCollectionView reloadData];
    }else{
        if (self.selectedSubIndex == indexPath.row) {
            self.selectedSubIndex = -1;
        }else{
            self.selectedSubIndex = indexPath.row;
        }
        [self.subCollectionView reloadData];
        if ([self.delegate respondsToSelector:@selector(clickSubHobbyAtIndex:hobbyIndex:)]) {
            [self.delegate clickSubHobbyAtIndex:self.selectedSubIndex hobbyIndex:self.selectedIndex];
        }
    }
}
- (void)layoutSubView {
    [self.btnsContainerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(60);
    }];

    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.searchBar.mas_bottom);
        make.height.mas_equalTo(25);
    }];
    [self.subCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.collectionView.mas_bottom);
        make.height.mas_equalTo(25);
    }];
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(5);
    }];
}
@end
