//
//  CCMyOrderedController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/26.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCMyOrderedController : BaseViewController
@property (strong, nonatomic) UITableView *tableView;
- (void)refreshData;
@end
