//
//  CCHTMLWebviewController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/20.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCHTMLWebviewController.h"

@interface CCHTMLWebviewController ()
@property (nonatomic, strong) UIWebView *webView;
@end

@implementation CCHTMLWebviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"推广详情";
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, App_Width, App_Height - 64)];
    [self.view addSubview:self.webView];
    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/share/deduction?rid=%@",USERID];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}



@end
