//
//  CCShareController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/24.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCCreditPayController.h"
#import "CCPostShareController.h"
#import "CCCreditRankController.h"
#import "CCMyOrderController.h"
#import "UserManager.h"
#import "WithdrawViewController.h"
#import "WXApi.h"
#import "WXApiObject.h"
#import "CCYajinWithdrwController.h"
@interface CCCreditPayController ()<WithdrawViewControllerDelegate,WithdrawYajinControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *moneyField;
@property (weak, nonatomic) IBOutlet UILabel *myMoneyLbl; //我的押金


@property (weak, nonatomic) IBOutlet UIButton *takeoutDeposit;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;
@property (weak, nonatomic) IBOutlet UIButton *chargeBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *improveImgV;

@end

@implementation CCCreditPayController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    self.myMoneyLbl.text = [NSString stringWithFormat:@"我的押金:¥ %.2f元",[UserManager sharedInstance].yajin];
    [NOTICENTER addObserver:self selector:@selector(PaySuccess) name:@"PaySuccess" object:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setUI {
    self.view.backgroundColor = WHITECOLOR;
    [self.wechatBtn addTarget:self action:@selector(wechatBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.alipayBtn addTarget:self action:@selector(alipayBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.takeoutDeposit addTarget:self action:@selector(takeoutDepositClick) forControlEvents:UIControlEventTouchUpInside];
    [self.chargeBtn addTarget:self action:@selector(chargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
}
//微信
- (void)wechatBtnClick {
    self.wechatBtn.selected = YES;
    self.alipayBtn.selected = NO;
}
//支付宝
- (void)alipayBtnClick {
    self.wechatBtn.selected = NO;
    self.alipayBtn.selected = YES;
}

//押金提现
- (void)takeoutDepositClick {
//     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationController.navigationBarHidden = NO;
    CCYajinWithdrwController *vc = [[CCYajinWithdrwController alloc]init];
    vc.maxMoney = [UserManager sharedInstance].yajin;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}
//刷新
- (void)WithdrawViewControllerSuccessOnMoney:(CGFloat)money {
    money = -money;
    [self updateYajin:money];
    
    
}
//充值
- (void)chargeBtnClick {
    if ([self.moneyField.text floatValue] <= 0) {
        SHOW(@"输入金额有误");
        return ;
    }
    if (self.wechatBtn.selected) { //微信充值
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/wxoeder_yj") params:@{@"userid":USERID,@"yajin":self.moneyField.text} target:self  success:^(NSDictionary *success) {
            [WXApi registerApp:success[@"data"][@"appid"]];

            PayReq *request = [[PayReq alloc] init];
            request.partnerId = success[@"data"][@"partnerid"];
            request.prepayId  = success[@"data"][@"prepayid"];
            request.package   = @"Sign=WXPay";
            request.nonceStr  = success[@"data"][@"noncestr"];
            request.timeStamp = (UInt32)[success[@"data"][@"timestamp"] integerValue];
            request.sign      = success[@"data"][@"sign"];
            
            [WXApi sendReq:request];
        } failure:^(NSError *failure) {
            
        }];
    }else{ //支付宝充值
        [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/alipay_yj") params:@{@"userid":USERID,@"yajin":self.moneyField.text} target:self success:^(NSDictionary *success) {
            [[AlipaySDK defaultService] payOrder:success[@"data"][@"paystring"] fromScheme:@"cuncuntong" callback:^(NSDictionary *resultDic) {
                [MBProgressHUD showMessage:@"充值成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }];
            [self updateYajin:[self.moneyField.text floatValue]];
        } failure:^(NSError *failure) {
            
        }];
    }
}
- (void)PaySuccess {
    [self updateYajin:[self.moneyField.text floatValue]];
}
- (void)updateYajin:(CGFloat)money {
    [UserManager sharedInstance].yajin = [UserManager sharedInstance].yajin + money;
    if ([UserManager sharedInstance].yajin  < 0) {
        [UserManager sharedInstance].yajin  = 0;
    }
    self.myMoneyLbl.text = [NSString stringWithFormat:@"我的押金:¥ %.2f元",[UserManager sharedInstance].yajin];
    self.improveImgV.image = IMAGENAMED(@"hongyuan");
}


@end
