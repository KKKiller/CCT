//
//  CCOrderModel.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/14.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCOrderModel.h"

@implementation CCOrderModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"pinfo" : [CCShareDetailModel class]};
}
@end
