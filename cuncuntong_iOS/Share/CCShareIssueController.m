//
//  CCShareIssueController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2017/7/1.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CCShareIssueController.h"
#import <RongIMKit/RongIMKit.h>
#import "IQKeyboardManager.h"

@interface CCShareIssueController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *oblineBtn;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UIImageView *issueerImgView;
@property (weak, nonatomic) IBOutlet UIImageView *issuedImgView;
@property (weak, nonatomic) IBOutlet UILabel *issuerPhoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *issuedPhoneLbl;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLbl;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (nonatomic, strong) NSString *phone;

@end

@implementation CCShareIssueController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITECOLOR;
    [self addTitle:@"争议诉讼"];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden= YES;
    self.textView.delegate = self;
    [self.submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.oblineBtn addTarget:self action:@selector(onlineBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.phoneBtn addTarget:self action:@selector(phoneBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.issueerImgView setImageURL:URL([UserManager sharedInstance].portrait)];
    self.issuerPhoneLbl.text = [NSString stringWithFormat:@"联系电话: %@",[UserManager sharedInstance].mobile];
    [self loadData];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)loadData {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/tsinfo") params:@{@"ts_uid":STR(self.uid)} target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue] == 1) {
            NSString *portrait = success[@"data"][@"info"][@"portrait"];
            self.phone = success[@"data"][@"info"][@"mobile"];
            
            //被投诉
            [self.issuedImgView setImageWithURL:URL(portrait) placeholder:IMAGENAMED(@"head")];
            self.issuedPhoneLbl.text = [NSString stringWithFormat:@"联系电话: %@",self.phone];
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (void)submitBtnClick{
    if (self.textView.text.length == 0) {
        SHOW(@"请输入投诉内容");
        return;
    }
    NSDictionary *dict = @{@"ts_uid":USERID,@"ts_buid":STR(self.uid),@"ts_people":[UserManager sharedInstance].mobile,@"ts_bpeople":STR(self.phone),@"ts_content":self.textView.text,@"order_num":STR(self.orderNum)};
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/tszy") params:dict target:self success:^(NSDictionary *success) {
        if ([success[@"data"][@"stat"] integerValue ] == 1) {
            SHOW(@"提交成功");
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            SHOW(success[@"data"][@"info"]);
        }
    } failure:^(NSError *failure) {
    }];
}
- (void)onlineBtnClick{
    [IQKeyboardManager sharedManager].enable = NO;
        RCConversationViewController *chat = [[RCConversationViewController alloc] initWithConversationType:1 targetId:@"608524"];
        chat.title = @"客服";
        [self.navigationController pushViewController:chat animated:YES];
}
- (void)phoneBtnClick {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"010-82886232" message:nil preferredStyle:1];
    //18039290408
    UIAlertAction *chooseOne = [UIAlertAction actionWithTitle:@"呼叫" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action)
                                {
                                    NSString *allString = [NSString stringWithFormat:@"tel:010-82886232"];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
                                }];
    //取消栏
    UIAlertAction *cancelOne = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:chooseOne];
    [alertController addAction:cancelOne];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (void)textViewDidChange:(UITextView *)textView {
    self.placeHolderLbl.hidden = textView.text.length != 0;
}
@end
