//
//  CCShareHomeHeaderView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/11.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CCShareHomeHeaderViewDelegate <NSObject>
- (void)clickTopBtnAtIndex:(NSInteger)index;
- (void)searchWithText:(NSString *)text;
- (void)baiduWithText:(NSString *)text;
- (void)clickHobbyAtIndex:(NSInteger)index;
- (void)clickSubHobbyAtIndex:(NSInteger)index hobbyIndex:(NSInteger)hobbyIndex;

@end
@interface CCShareHomeHeaderView : UIView
@property (nonatomic, strong) UIView *btnsContainerV;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionView *subCollectionView;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) NSInteger selectedSubIndex;

@property (nonatomic, strong) UIButton *searchBtn;
@property (nonatomic, strong) UIButton *baiduBtn;
@property (nonatomic, strong) UIView *bottomLineView;
@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) NSMutableArray *hobbyArray;

@end
