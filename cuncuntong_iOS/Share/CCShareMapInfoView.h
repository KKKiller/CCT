//
//  CCShareMapInfoView.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/14.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCShareDetailModel.h"
@interface CCShareMapInfoView : UIView
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
+ (instancetype)instanceView ;
@property (nonatomic, strong) CCShareDetailModel *model;
@end
