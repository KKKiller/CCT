//
//  CCCreditRankCell.h
//  CunCunTong
//
//  Created by 周吾昆 on 2017/6/25.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCRankModel.h"
@interface CCCreditRankCell : UITableViewCell
@property (nonatomic, strong) UILabel *rankLbl;
@property (nonatomic, strong) CCRankModel *model;
@end
