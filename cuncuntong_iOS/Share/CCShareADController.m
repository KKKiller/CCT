//
//  CCShareADController.m
//  CunCunTong
//
//  Created by 周吾昆 on 2018/10/20.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "CCShareADController.h"
#import "CCBaseWebviewController.h"
#import "CCShareCodeView.h"
@interface CCShareADController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (strong, nonatomic) PathDynamicModal *animateModel;
@property (nonatomic, strong) UIImage *image;

@end

@implementation CCShareADController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.statusBarWhite = YES;
    self.navigationController.navigationBarHidden = YES;
    self.scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    self.scrollView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    
    [self.view addSubview:self.scrollView];
    
    UIImageView *imgView = [[UIImageView alloc]init];
    [self.scrollView addSubview:imgView];
    imgView.image = IMAGENAMED(@"share_ad");
    imgView.frame = CGRectMake(0, 0, App_Width, 3878.0 / 750.0 * App_Width);
    self.scrollView.contentSize = CGSizeMake(imgView.width, imgView.height);
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 20, 30, 30)];
    [backBtn setBackgroundImage:IMAGENAMED(@"back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    [self addBtns];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;

}
- (void)addBtns {
    CGFloat height = 3878.0 / 750.0 * App_Width;
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(0, height - 90, App_Width, 90)];
    [self.scrollView addSubview:btn1];
    btn1.backgroundColor = [UIColor clearColor];
    [btn1 addTarget:self action:@selector(btn1Click) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btn2 = [[UIButton alloc]initWithFrame:CGRectMake(0, height - 180, App_Width * 0.5, 90)];
    [self.scrollView addSubview:btn2];
    btn2.backgroundColor = [UIColor clearColor];
    [btn2 addTarget:self action:@selector(btn2Click) forControlEvents:UIControlEventTouchUpInside];

    
    UIButton *btn3 = [[UIButton alloc]initWithFrame:CGRectMake(App_Width * 0.5, height - 180, App_Width * 0.5, 90)];
    [self.scrollView addSubview:btn3];
    btn3.backgroundColor = [UIColor clearColor];
    [btn3 addTarget:self action:@selector(btn3Click) forControlEvents:UIControlEventTouchUpInside];

}

//下载专属二维码
- (void)btn1Click {
    CCShareCodeView *view = [CCShareCodeView instanceView];
    view.frame  = CGRectMake(0, 0, App_Width - 60, App_Width - 20 );
    [view.saveBtn addTarget:self action:@selector(saveImg) forControlEvents:UIControlEventTouchUpInside];
    NSString *url = [NSString stringWithFormat:@"http://cm4.wanshitong.net/village/public/center/qrcode2?rid=%@&just_qrcode",USERID];
    view.userInteractionEnabled = YES;
    [view.imgView setImageWithURL:URL(url) placeholder:nil options:0 completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        self.image = image;
    }];
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:App_Delegate.window];
    
    
}
//下载宣传页文件
- (void)btn2Click {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    vc.urlStr =  [NSString stringWithFormat:@"http://www.cct369.com/village/public/article?p=8EF1716B20524C8FD1ED080EED160672&u=%@",USERID];
    [self.navigationController pushViewController:vc animated:YES];
}
//推广区域查询
- (void)btn3Click {
    CCBaseWebviewController *vc = [[CCBaseWebviewController alloc]init];
    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/article?p=8AAE7683E818ED1F898C9ABDEE45C641&u=%@",USERID];
    vc.urlStr = url;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)saveImg{
    UIImageWriteToSavedPhotosAlbum(self.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
}
-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *msg = nil ;
    if(error){
        msg = @"保存图片失败" ;
    }else{
        msg = @"保存图片成功" ;
    }
    SHOW(msg);
}

@end
