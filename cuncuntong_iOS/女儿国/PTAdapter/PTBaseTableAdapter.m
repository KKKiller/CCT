//
//  PTBaseTableAdapter.m
//  MvvmTest
//
//  Created by 彭涛 on 2019/4/9.
//  CopyriPTt © 2019 K12. All riPTts reserved.
//

#import "PTBaseTableAdapter.h"
@interface PTBaseTableAdapter ()<UITableViewDataSource,UITableViewDelegate>
//列表View
@property (nonatomic,strong)UITableView *tableView;
@end
@implementation PTBaseTableAdapter

- (instancetype)initWithTableView:(UITableView *)tableView andDataSource:(NSArray *)data refreshStyle:(PTBaseRefreshStyle)refreshStyle cellRegNames:(NSArray *)nameList refreshModel:(PTBaseRefreshModel)refreshModel{
    if (self = [super init]) {
        _tableView = tableView;
        _dataList = data;
        self.refreshStyle = refreshStyle;
        self.refreshModel = refreshModel;
        [self initTableUI:nameList];
    }
    return self;
}

/**
 初始化列表UI
 */
- (void)initTableUI:(NSArray *)nameList{
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (nameList && nameList.count > 0) {
        for (NSString *name in nameList) {
            UINib *nib = [UINib nibWithNibName:name bundle:nil];
            [_tableView registerNib:nib forCellReuseIdentifier:name];
        }
        _tableView.estimatedRowHeight=100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    
}
- (void)setRefreshModel:(PTBaseRefreshModel)refreshModel{
    _refreshModel = refreshModel;
    [self refreshTableRrefreshUI];
   
}
- (void)setRefreshStyle:(PTBaseRefreshStyle)refreshStyle{
    _refreshStyle = refreshStyle;
     [self refreshTableRrefreshUI];
}

/**
 根据刷新模式设置刷新UI
 */
- (void)refreshTableRrefreshUI{
    [self endTableRefresh];
    self.tableView.mj_header = nil;
    self.tableView.mj_footer = nil;
    switch (self.refreshModel) {
        case PTBaseRefreshModelNone:
            break;
        case PTBaseRefreshModelTop:{
            [self setTableRerfeshTopUI];
        }
            break;
        case PTBaseRefreshModelBoth:{
            [self setTableRerfeshTopUI];
            [self setTableRerfeshBtmUI];
        }
            break;
        default:
            break;
    }
}

/**
 设置顶部刷新UI
 */
- (void)setTableRerfeshTopUI{
    switch (self.refreshStyle) {
        case PTBaseRefreshStyleDefault:{
            self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshFromTop)];
        }
            break;
        default:
            break;
    }
}

/**
 设置底部刷新UI
 */
- (void)setTableRerfeshBtmUI{
    switch (self.refreshStyle) {
        case PTBaseRefreshStyleDefault:{
            self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshFromBtm)];
        }
            break;
        default:
            break;
    }
}

#pragma mark-刷新回调事件
- (void)refreshFromTop{
    if (self.topRefreshBd) {
        self.topRefreshBd(self);
    }
}
- (void)refreshFromBtm{
    if (self.topRefreshBd) {
        self.btmRefreshBd(self);
    }
}
#pragma mark-显示下拉刷新UI
- (void)beginTopRefreh{
    if (![self.tableView.mj_header isRefreshing]) {
        [self.tableView.mj_header beginRefreshing];
    }
}
#pragma mark-结束列表刷新

- (void)endTableRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
#pragma mark-滚动回调事件
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(adapter:scrollViewDidScroll:)]) {
        [self.delegate adapter:self scrollViewDidScroll:scrollView];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(adapter:scrollViewDidEndDecelerating:)]) {
        [self.delegate adapter:self scrollViewDidEndDecelerating:scrollView];
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(adapter:scrollViewDidEndScrollingAnimation:)]) {
        [self.delegate adapter:self scrollViewDidEndScrollingAnimation:scrollView];
    }
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (self.delegate && [self.delegate respondsToSelector:@selector(adapter:scrollViewDidEndDragging:willDecelerate:)]) {
        [self.delegate adapter:self scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}
#pragma marl-列表datasource实现
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataSource) {
        id model = self.dataList[indexPath.row];
        return [self.dataSource cellWithTable:tableView andIndexPath:indexPath andData:model];
    }else{
        NSString *name = [NSString stringWithFormat:@"%@ -> cellForRowAtIndexPath 方法 报错了",NSStringFromClass(self.class)];
        @throw [NSException exceptionWithName:name reason:@"亲爱的，你没有为我设置dataSource" userInfo:nil];
    }
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate && [self.delegate respondsToSelector:@selector(adapter:tableView:didSelectRowAtIndexPath:data:)]) {
        [self.delegate adapter:self tableView:tableView didSelectRowAtIndexPath:indexPath data:self.dataList[indexPath.row]];
    }
}
#pragma mark-数据设置事件
- (void)setDataList:(NSArray *)dataList{
    _dataList = dataList;
    [self.tableView reloadData];
    [self endTableRefresh];
}
@end
