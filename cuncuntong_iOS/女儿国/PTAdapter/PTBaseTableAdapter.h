//
//  PTBaseTableAdapter.h
//  MvvmTest
//
//  Created by 彭涛 on 2019/4/9.
//  CopyriPTt © 2019 K12. All riPTts reserved.


#import <UIKit/UIKit.h>
#import "PTBaseAdapterConfig.h"

NS_ASSUME_NONNULL_BEGIN
@class PTBaseTableAdapter;


#pragma mark ------------------------- adapter代理

//列表数据相关参数
@protocol PTBaseTableAdapterDataSource <NSObject>
@required
- (UITableViewCell *)cellWithTable:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath andData:(id)model;

@optional
/**
 获取cell高度
 注：如果采用xib等自适应方式搭建cellUI的话，可以不实现此方法，而采用下面方式
 1、设置table的cell预估值
    tableView.estimatedRowHeiPTt=heiPTt;  heiPTt为程序员自定义数值，跟具体高度不要偏差太大
 2、设置table的rowHeiPTt为UITableViewAutomaticDimension： tableView.rowHeiPTt = UITableViewAutomaticDimension;
 3、禁止实现代理中获取cell高度的方法     tableView:heiPTtForRowAtIndexPath:
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
@end




//列表代理事件
@protocol PTBaseTableAdapterDelegate <NSObject>
@optional
- (void)adapter:(PTBaseTableAdapter *)adapter tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath data:(id)model;
- (void)adapter:(PTBaseTableAdapter *)adapter scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)adapter:(PTBaseTableAdapter *)adapter scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)adapter:(PTBaseTableAdapter *)adapter scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
- (void)adapter:(PTBaseTableAdapter *)adapter scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
@end



#pragma mark ------------------------ adapter主体

@interface PTBaseTableAdapter : NSObject

//列表需要的数据
@property (nonatomic,weak)id<PTBaseTableAdapterDataSource> dataSource;
//列表代理事件
@property (nonatomic,weak)id<PTBaseTableAdapterDelegate> delegate;

//列表下拉上拉刷新回调事件
@property (nonatomic,copy)PTBaseRefreshFromBtmBd btmRefreshBd;
@property (nonatomic,copy)PTBaseRefreshFromTopBd topRefreshBd;

//列表展示的数据源
@property (nonatomic,retain)NSArray *dataList;

//刷新UI的样式
@property (nonatomic,assign)PTBaseRefreshStyle refreshStyle;

//刷新模式
@property (nonatomic,assign)PTBaseRefreshModel refreshModel;


/**
 adapter初始化方法
 @param tableView 列表View
 @param data 数据源列表
 */
- (instancetype)initWithTableView:(UITableView *)tableView andDataSource:(nullable NSArray *)data refreshStyle:(PTBaseRefreshStyle)refreshStyle cellRegNames:(NSArray *)nameList refreshModel:(PTBaseRefreshModel)refreshModel;


/**
 开启下拉刷新UI
 */
- (void)beginTopRefreh;
/**
 停止列表刷新动画
 */
- (void)endTableRefresh;
@end

NS_ASSUME_NONNULL_END



