//
//  PTBaseAdapterConfig.h
//  liuliu
//
//  Created by dccb-pengt on 2019/8/16.
//  Copyright © 2019 Wenxuan Sun. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PTBaseRefreshStyle){
    PTBaseRefreshStyleDefault = 0
} ;
/*
 刷新模式
 */
typedef NS_ENUM(NSInteger, PTBaseRefreshModel){
    PTBaseRefreshModelNone = 0,//上下都不刷新
    PTBaseRefreshModelTop,//只上不刷新
    PTBaseRefreshModelBoth//上下都刷新
} ;

//列表从顶部刷新的回调block
typedef void(^PTBaseRefreshFromTopBd) (id adapter);
//列表从底部刷新的回调block
typedef void(^PTBaseRefreshFromBtmBd) (id adapter);
