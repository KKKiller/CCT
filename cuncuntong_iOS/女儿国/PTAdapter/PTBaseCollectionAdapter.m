//
//  PTBaseCollectionAdapter.m
//  liuliu
//
//  Created by dccb-pengt on 2019/8/16.
//  Copyright © 2019 Wenxuan Sun. All rights reserved.
//

#import "PTBaseCollectionAdapter.h"
@interface PTBaseCollectionAdapter()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,retain)UICollectionView *collectionView;
@end
@implementation PTBaseCollectionAdapter
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView andDataSource:(nullable NSArray *)data refreshStyle:(PTBaseRefreshStyle)refreshStyle cellRegNames:(NSArray *)nameList refreshModel:(PTBaseRefreshModel)refreshModel{
    if (self = [super init]) {
        self.collectionView = collectionView;
        self.collectionView.dataSource = self;
        self.collectionView.delegate = self;
    
        self.dataList = data;
        self.refreshModel = refreshModel;
        self.refreshStyle = refreshStyle;
    
        
        //注册cell
        for (NSString *cellName in nameList) {
            [self.collectionView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellWithReuseIdentifier:cellName];
        }
        
//        //注册sectionHeader
//        for (NSString *headerName in headerNameList) {
//            [self.collectionView registerNib:[UINib nibWithNibName:headerName bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerName];
//        }
//
//        //注册sectionFooter
//        for (NSString *footerName in footNameList) {
//            [self.collectionView registerNib:[UINib nibWithNibName:footerName bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerName];
//        }
    }
    return self;
}
#pragma mark-设置刷新模式
- (void)setRefreshModel:(PTBaseRefreshModel)refreshModel{
    _refreshModel = refreshModel;
    [self refreshTableRrefreshUI];
    
}
- (void)setRefreshStyle:(PTBaseRefreshStyle)refreshStyle{
    _refreshStyle = refreshStyle;
    [self refreshTableRrefreshUI];
}

/**
 根据刷新模式设置刷新UI
 */
- (void)refreshTableRrefreshUI{
    [self endTableRefresh];
    self.collectionView.mj_header = nil;
    self.collectionView.mj_footer = nil;
    switch (self.refreshModel) {
        case PTBaseRefreshModelNone:
            break;
        case PTBaseRefreshModelTop:{
            [self setTableRerfeshTopUI];
        }
            break;
        case PTBaseRefreshModelBoth:{
            [self setTableRerfeshTopUI];
            [self setTableRerfeshBtmUI];
        }
            break;
        default:
            break;
    }
}

/**
 设置顶部刷新UI
 */
- (void)setTableRerfeshTopUI{
    switch (self.refreshStyle) {
        case PTBaseRefreshStyleDefault:{
            self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshFromTop)];
        }
            break;
        default:
            break;
    }
}

/**
 设置底部刷新UI
 */
- (void)setTableRerfeshBtmUI{
    switch (self.refreshStyle) {
        case PTBaseRefreshStyleDefault:{
            self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshFromBtm)];
        }
            break;
        default:
            break;
    }
}
- (void)endTableRefresh{
    [self.collectionView.mj_header endRefreshing];
    [self.collectionView.mj_footer endRefreshing];
}
#pragma mark-刷新回调事件
- (void)refreshFromTop{
    if (self.topRefreshBd) {
        self.topRefreshBd(self);
    }
}
- (void)refreshFromBtm{
    if (self.topRefreshBd) {
        self.btmRefreshBd(self);
    }
}
#pragma mark - collection datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataSource) {
       return [self.dataSource getCollectionCell:self collectionView:collectionView indexPath:indexPath model:self.dataList[indexPath.row]];
    }else{
        @throw [NSException exceptionWithName:@"方法缺失" reason:@"方法缺失dataSource 缺失方法：getCollectionCell:indexPath: model:" userInfo:nil];
    }
    return nil;
}
/**
 * 获取item大小
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataSource) {
        return [self.dataSource getCollectionCellSize:self layout:collectionViewLayout indexPath:indexPath model:self.dataList[indexPath.row]];
    }
    else{
        @throw [NSException exceptionWithName:@"方法缺失" reason:@"方法缺失dataSource 缺失方法：getCollectionCell:indexPath: model:" userInfo:nil];
    }
    return CGSizeZero;
}
#pragma mark -------- colletion delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate && [self.delegate respondsToSelector:@selector(collectionAdapter:didSelectItemAtIndexPath:model:)]) {
        [self.delegate collectionAdapter:self didSelectItemAtIndexPath:indexPath model:self.dataList[indexPath.row]];
    }
}
#pragma mark-数据设置事件
- (void)setDataList:(NSArray *)dataList{
    _dataList = dataList;
    [self.collectionView reloadData];
}
@end
