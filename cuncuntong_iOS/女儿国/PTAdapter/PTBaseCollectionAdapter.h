//
//  PTBaseCollectionAdapter.h
//  liuliu
//
//  Created by dccb-pengt on 2019/8/16.
//  Copyright © 2019 Wenxuan Sun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTBaseAdapterConfig.h"
NS_ASSUME_NONNULL_BEGIN

#pragma mark ----------------------- adapter代理
@class PTBaseCollectionAdapter;
@protocol PTBaseCollectionAdapterDatasource <NSObject>
- (__kindof UICollectionViewCell *)getCollectionCell:(PTBaseCollectionAdapter *)adapter collectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath model:(id)model;
- (CGSize)getCollectionCellSize:(PTBaseCollectionAdapter *)adapter layout:(UICollectionViewLayout *)collectionViewLayout indexPath:(NSIndexPath *)indexPath model:(id)model;
@end

@protocol PTBaseCollectionAdapterDelegate <NSObject>
- (void)collectionAdapter:(PTBaseCollectionAdapter *)adapter didSelectItemAtIndexPath:(NSIndexPath *)indexPath model:(id)model;
@end









#pragma mark ----------------------- adapter主体
@interface PTBaseCollectionAdapter : NSObject

//列表需要的数据
@property (nonatomic,weak)id<PTBaseCollectionAdapterDatasource> dataSource;
//列表代理事件
@property (nonatomic,weak)id<PTBaseCollectionAdapterDelegate> delegate;

//列表下拉上拉刷新回调事件
@property (nonatomic,copy)PTBaseRefreshFromBtmBd btmRefreshBd;
@property (nonatomic,copy)PTBaseRefreshFromTopBd topRefreshBd;

//列表展示的数据源
@property (nonatomic,retain)NSArray *dataList;

//刷新UI的样式
@property (nonatomic,assign)PTBaseRefreshStyle refreshStyle;

//刷新模式
@property (nonatomic,assign)PTBaseRefreshModel refreshModel;

/**
  adapter 初始化方法
 @param collectionView 列表View
 @param data 数据源列表
 */
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView andDataSource:(nullable NSArray *)data refreshStyle:(PTBaseRefreshStyle)refreshStyle cellRegNames:(NSArray *)nameList refreshModel:(PTBaseRefreshModel)refreshModel;
@end

NS_ASSUME_NONNULL_END
