//
//  CCGirlApplyDeliveryModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCGirlApplyDeliveryModel : CCBaseModel
@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *image;

@property (nonatomic,assign)int increase;
@property (nonatomic,assign)int price;
@property (nonatomic,assign)int freight;
@end

NS_ASSUME_NONNULL_END
