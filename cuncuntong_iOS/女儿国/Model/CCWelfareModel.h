//
//  CCWelfareModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCWelfareModel : CCBaseModel
@property (nonatomic,retain)NSString *name;//商品名称
@property (nonatomic,assign)int order_id;//订单ID
@property (nonatomic,assign)int order_number;//订单号 null说明未该福利品未发货
@property (nonatomic,assign)int product_id;//商品ID
@property (nonatomic,retain)NSString *receive_welfare_time;
@property (nonatomic,assign)int num;//数量
//一下在详情页才会有
@property (nonatomic,retain)NSString *create_time;//商品创建时间
@property (nonatomic,retain)NSString *image;
@property (nonatomic,retain)NSString *postage_price;
@property (nonatomic,assign)int product_num;
@property (nonatomic,retain)NSString *reader_address;
@property (nonatomic,retain)NSString *reader_name;
@property (nonatomic,retain)NSString *reader_phone;
@property (nonatomic,retain)NSString *total_price;
@end

NS_ASSUME_NONNULL_END
