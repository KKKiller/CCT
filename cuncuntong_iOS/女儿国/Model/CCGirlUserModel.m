//
//  CCGirlUserModel.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/18.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGirlUserModel.h"
static CCGirlUserModel *girlUserModel;
@implementation CCGirlUserModel
+ (instancetype)share{
    if (girlUserModel == nil) {
        girlUserModel = [[self alloc] init];
    }
    return girlUserModel;
}
+ (void)setShareData:(CCGirlUserModel *)userModel{
    girlUserModel = userModel;
    girlUserModel.currentType = 1;
}
+(NSString *)getIdentityName:(int)identity gender:(int)gender{
    switch (identity) {
        case 1:{
            return gender == 1 ? @"游客" : @"千金";
        }
        case 2:{
            return gender == 1 ? @"都督" : @"格格";
        }
        case 3:{
            return gender == 1 ? @"侯爷" : @"郡主";
        }
        case 4:{
            return gender == 1 ? @"王爷" : @"女王";
        }
        default:
            break;
    }
    return gender == 1 ? @"王爷" : @"女王";
}
+ (UIColor *)getIdentityNameColor:(int)identity{
    switch (identity) {
        case 1:{
            return [UIColor colorWithRGB:0x333333];
        }
            
            break;
        case 2:{
            return [UIColor colorWithRGB:0x333333];
        }
            
            break;
        case 3:{
            return [UIColor colorWithRGB:0xFF6600];
        }
            
            break;
        case 4:{
            return [UIColor colorWithRGB:0xFF0099];
        }
            
            break;
        default:
            break;
    }
    return [UIColor colorWithRGB:0x333333];
}
- (int)currentLevel{
    return self.currentType == 0 ? self.identity_wsj : self.identity_mm;
}
@end
