//
//  CCKuCunModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCKuCunModel : CCBaseModel
@property (nonatomic,assign)int count;
@property (nonatomic,assign)int id_;
@property (nonatomic,retain)NSString *name;
@end

NS_ASSUME_NONNULL_END
