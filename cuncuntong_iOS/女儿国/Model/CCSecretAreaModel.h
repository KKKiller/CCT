//
//  CCSecretAreaModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCSecretAreaModel : CCBaseModel
@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *iconName;
@property (nonatomic,assign)int itemId;
@property (nonatomic,retain)id data;
- (instancetype)initWithName:(NSString *)name iconImgName:(NSString *)imgUrl itemId:(int)itemId data:(id _Nullable)data;
@end

NS_ASSUME_NONNULL_END
