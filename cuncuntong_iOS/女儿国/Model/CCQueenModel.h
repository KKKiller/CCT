//
//  CCQueenModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCQueenModel : CCBaseModel
@property (nonatomic,retain)NSString *reader_name;
@property (nonatomic,retain)NSString *reader_photo;

@property (nonatomic,assign)int sum_inventory;//领取数量
@property (nonatomic,assign)int identity;//等级
@end



@interface CCQueenListModel : CCBaseModel
@property (nonatomic,assign)int queen_count;//女王数量
@property (nonatomic,retain)NSArray<CCQueenModel *> *queen_info;
@end

NS_ASSUME_NONNULL_END
