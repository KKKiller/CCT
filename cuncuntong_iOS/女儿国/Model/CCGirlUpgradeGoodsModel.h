//
//  CCGirlUpgradeGoodsModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/19.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCGirlUpgradeGoodsModel : CCBaseModel
@property (nonatomic,assign)int id_;
@property (nonatomic,assign)int price;
@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *image;
@property (nonatomic,retain)NSString *path_image;
@end

NS_ASSUME_NONNULL_END
