//


#import "CCBaseModel.h"
#import <objc/runtime.h>
@interface CCBaseModel ()
@end
@implementation CCBaseModel
#pragma mark-需要开发人员配置的方法
- (NSArray *)mapKeys{
    if (_mapKeys == nil) {
        _mapKeys = @[];
    }
    return _mapKeys;
}

#pragma mark-json转模型时候碰到开发者自定义模型时候，设置模型指定的分类
/**
 属性中有其他自定义类型的对象，将对象名字做key，对象类型名称做value放入字典返回
 如属性中有 ModelOne  obj；
 则此自定义类的对象放入字典格式为{@"obj":@"ModelOne"}
 */
+ (NSDictionary *)getKvcClsMap{
    NSDictionary *dict = [self getDefaultClsMap];
    return dict;
}
/**
 自己检测出的属性中有子类型的属性
 */
+(NSDictionary *)getDefaultClsMap{
    //存储已经处理过的类名称
    NSMutableDictionary *propertyMap = [NSMutableDictionary new];
    //获取对象的类型objc_getClass("UserModel")
    unsigned int propertyCount = 0;
    objc_property_t * result = class_copyPropertyList([self class], &propertyCount);
    for (int i = 0; i < propertyCount; i++) {
        objc_property_t o_t =  result[i];
        NSString *name = [NSString stringWithFormat:@"%s", property_getName(o_t)];
        NSString *attr = [NSString stringWithFormat:@"%s", property_getAttributes(o_t)];
        NSArray *arr = [attr componentsSeparatedByString:@"\""];
        if (arr.count == 3) {
            //将该类型存储到处理过的类列表中
            Class cls = NSClassFromString(arr[1]);
            if ([cls isSubclassOfClass:[CCBaseModel class]]) {
                [propertyMap setObject:NSStringFromClass(cls) forKey:name];
            }
        }
    }
    return propertyMap;
}
#pragma mark-给模型赋值方法
/**
 将json数据转化到模型对象里面
 @param json 数据源
 */
- (void)setJsonData:(NSString *)json{
    id data = [[self class] jsonStrToOcObj:json];
    if ([data isKindOfClass:[NSDictionary class]]) {
        [self setMapData:data];
    }
}
- (void)setMapData:(NSDictionary *)dataSource{
    @try {
        NSDictionary *cKeys = [[self class] getKvcClsMap];
        if (dataSource != nil && ![dataSource isKindOfClass:[NSNull class]]) {
            for (NSString *key in dataSource.allKeys) {
                id value = dataSource[key];
                if (![value isKindOfClass:[NSNull class]]) {
                    id className = cKeys[key];
                    if ([value isKindOfClass:[NSArray class]]) {
                        NSArray *arr = [self dealSubArrayData:value withClsName:className];
                        [self pt_setValue:arr forKey:key];
                    }else if([value isKindOfClass:[NSDictionary class]]){
                        if (className) {
                            Class Cl = NSClassFromString(className);
                            id mm = [[Cl alloc] init];
                            if([mm isKindOfClass:[CCBaseModel class]]){
                                [mm performSelector:@selector(setMapData:) withObject:value];
                            }else{
                                [self setData:value toModel:mm];
                            }
                            [self pt_setValue:mm forKey:key];
                        }else{
                            [self pt_setValue:value forKey:key];
                        }
                    }else{
                        if ([key isEqualToString:@"id"]) {
                            [self pt_setValue:value forKey:@"id_"];
                        }else if ([key isEqualToString:@"description"]) {
                            [self pt_setValue:value forKey:@"description_"];
                        }else{
                            [self pt_setValue:value forKey:key];
                        }
                    }
                }
            }
        }
    }@catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
}
- (void)pt_setValue:(id)value forKey:(id)key{
    @try {
        [self setValue:value forKey:key];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
}
- (NSArray *)dealSubArrayData:(NSArray *)dataArr withClsName:(NSString *)className{
    NSMutableArray *resultArr = [NSMutableArray new];
    if (className) {
        for (id data in dataArr) {
            if ([data isKindOfClass:[NSDictionary class]]) {
                Class Cl = NSClassFromString(className);
                id mm = [[Cl alloc] init];
                if ([Cl isSubclassOfClass:[CCBaseModel class]]) {
                    [mm performSelector:@selector(setMapData:) withObject:data];
                }else{
                    [self setData:data toModel:mm];
                }
                [resultArr addObject:mm];
            }else if ([data isKindOfClass:[NSArray class]]){
                [self dealSubArrayData:data withClsName:className];
            }else{
                [resultArr addObject:data];
            }
        }
    }else{
        //没有这个字段对应的类
        [resultArr addObjectsFromArray:dataArr];
    }
    
    return resultArr;
}
- (void)setData:(NSDictionary *)dataSource toModel:(id)model{
    for (NSString *key in dataSource.allKeys) {
        @try {
            [model setValue:dataSource[key] forKey:key];
        } @catch (NSException *exception) {
            NSLog(@"%@",exception.description);
        }
    }
}
#pragma mark-根据模型获取对应数据方法
/**
 把对象转成字典
 */
- (NSDictionary *)getMapData{
    NSMutableDictionary*props=[NSMutableDictionary new];
    for (NSString *key in self.mapKeys){
        id value = [self valueForKey:key];
        if(value == nil || [value isKindOfClass:[NSNull class]]){
            continue;
        }
        if ([value isKindOfClass:[NSArray class]]) {
            NSArray *arr = (NSArray *)value;
            arr = [self dealArray:arr];
            [props setObject:arr forKey:key];
        }else if ([value isKindOfClass:[NSDictionary class]]){
            NSDictionary *ddd = (NSDictionary *)value;
            ddd = [self dealDict:ddd];
            [props setObject:ddd forKey:key];
        }else if([value isKindOfClass:[CCBaseModel class]]){
            [props setObject:[(CCBaseModel *)value getMapData] forKey:key];
        }else{
            [props setObject:value forKey:key];
        }
    }
    return props;
}

/**
 在模型转转oc对象（数组、字典）时候，属性为数组时候，处理数组的方法

 @param arr 数组对象
 @return 处理过的数组对象
 */
- (NSArray *)dealArray:(NSArray *)arr{
    if (arr == nil || [arr isKindOfClass:[NSArray class]] == false) {
        return nil;
    }
    NSMutableArray *resultArr = [NSMutableArray  new];
    for (id obj in arr) {
        if ([obj isKindOfClass:[NSArray class]]) {
            NSArray *arr = (NSArray *)obj;
            arr = [self dealArray:arr];
            [resultArr addObject:arr];
        }else if ([obj isKindOfClass:[NSDictionary class]]){
            NSDictionary *ddd = (NSDictionary *)obj;
            ddd = [self dealDict:ddd];
            [resultArr addObject:ddd];
        }else if([obj isKindOfClass:[CCBaseModel class]]){
            [resultArr addObject:[(CCBaseModel *)obj getMapData]];
        }else{
            [resultArr addObject:obj];
        }
    }
    return resultArr;
}

/**
 在模型转oc对象（数组、字典）时候，属性为字典时候，处理数组的方法
 @param dict 字典对象
 @return 处理过的字典对象
 */
- (NSDictionary *)dealDict:(NSDictionary *)dict{
    if (dict == nil || [dict isKindOfClass:[NSDictionary class]] == false) {
        return nil;
    }
    NSMutableDictionary*props=[NSMutableDictionary new];
    for (NSString *key in dict){
        id value = [dict objectForKey:key];
        if ([value isKindOfClass:[NSArray class]]) {
            NSArray *arr = (NSArray *)value;
            arr = [self dealArray:arr];
            [props setObject:arr forKey:key];
        }else if ([value isKindOfClass:[NSDictionary class]]){
            NSDictionary *ddd = (NSDictionary *)value;
            ddd = [self dealDict:ddd];
            [props setObject:ddd forKey:key];
        }else if([value isKindOfClass:[CCBaseModel class]]){
            [props setObject:[(CCBaseModel *)value getMapData] forKey:key];
        }else{
            [props setObject:value forKey:key];
        }
    }
    return props;
}
/**
 模型转化为json数据
 */
#pragma mark-模型转json
- (NSString *)getJsonData{
    NSDictionary *dict = [self getMapData];
    NSString *jsonStr = [CCBaseModel objToJsonStr:dict];
    return jsonStr;
}
/**
 oc对象（nsarray/nsdictionary）转成json字符串
 */
+ (NSString *)objToJsonStr:(id)obj{
    if (obj == nil) {
        return nil;
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:obj options:0 error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return jsonStr;
}
#pragma mark-把数组转成数据模型数组
/**
 将数据源传进来，返回模型
 
 @param data 数据源（json反序列化后的数据）
 @return 数据模型
 */
+ (instancetype)getModel:(id)data{
    CCBaseModel *model = [[self class] new];
    if ([data isKindOfClass:[NSString class]]) {
        [model setJsonData:data];
    }else if([data isKindOfClass:[NSDictionary class]]){
        [model setMapData:data];
    }
    
    return model;
}
/**
 将数据源传进来，返回做成模型的数据
 
 @param dataList 数据源（json反序列化后的数据）
 @return 数据模型数组
 */
+ (NSArray *)getModelList:(NSArray *)dataList{
    if (!dataList || ![dataList isKindOfClass:[NSArray class]]) {
        return nil;
    }
    NSMutableArray *resultArr = [NSMutableArray new];
    for (id obj in dataList) {
        CCBaseModel *model = [[self class] new];
        [model setMapData:obj];
        [resultArr addObject:model];
    }
    return resultArr;
}


+ (id)jsonStrToOcObj:(NSString *)json{
    if (json == nil || [json isKindOfClass:[NSNull class]]) {
        return nil;
    }
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    id obj = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return obj;
}
@end
