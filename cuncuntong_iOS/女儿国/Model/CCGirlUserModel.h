//
//  CCGirlUserModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/18.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCGirlUserModel : CCBaseModel
+ (instancetype)share;
+ (void)setShareData:(CCGirlUserModel*)userModel;
@property (nonatomic,assign)int identity_mm;
@property (nonatomic,assign)int identity_wsj;
@property (nonatomic,assign)int reader_gender;// 1男生， 2女生
@property (nonatomic,retain)NSString *reader_name;
@property (nonatomic,retain)NSString *reader_photo;


@property (nonatomic,assign)int currentLevel;//当前等级
@property (nonatomic,assign)int currentType;//当前类型 1:卫生巾，2:面膜
+(NSString *)getIdentityName:(int)identity gender:(int)gender;
+(UIColor *)getIdentityNameColor:(int)identity;
@end

NS_ASSUME_NONNULL_END
