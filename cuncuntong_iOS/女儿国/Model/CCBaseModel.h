//

//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface CCBaseModel : NSObject{
    @protected
    //对象转字典或者转json时候，都需要转的属性名字数组
    NSArray<NSString *> *_mapKeys;
}
#pragma mark-需要开发人员配置的方法
/**
 对象转字典或者转json时候，都需要转的属性名字数组
 注：默认为空数组，开发人员需根据具体需求复写get方法或设置通过set方法设置
 */
@property (nonatomic,retain)NSArray<NSString *> *mapKeys;
/**
 在json转模型时候，如果模型中有属性为自定义类型的，通过复写此方法设置属性所属类型
 属性中有开发人员自定义类型的对象，将对象名字做key，对象类型名称做value放入字典返回
 如属性中有 ModelOne *obj；
 则此自定义类的对象放入字典格式为@{@"obj":@"ModelOne"}
 
 注：
 如果属性为数组类型时，该属性名称对应的类名称为数组元素要转化的类名称。
 默认包含了属性的类为CCBaseModel子类的属性
 当属性有数组时候，请自行设定数组元素的类名，即 @"数组属性名称"：@"元素类名"
 
 请开发人员在创建新类时，根据需求重写该方法；
 */
+ (NSDictionary *)getKvcClsMap;


#pragma mark-给模型赋值方法
/**
 将json数据转化到模型对象里面
 @param json 数据源
 */
- (void)setJsonData:(NSString *)json;
/**
 将NSDictionary对象存储到模型对象里面
 @param map 数据源
 */
- (void)setMapData:(NSDictionary *)map;


#pragma mark-根据模型获取对应数据方法

/**
 把对象转成字典并返回
 注：请提前设置mapKeys（需要转json的属性名称数组）
 */
- (NSDictionary *)getMapData;


/**
 把对象转成json数据并返回
 注：请提前设置mapKeys（需要转json的属性名称数组）
 */
- (NSString *)getJsonData;

#pragma mark-根据数据获取模型方法
/**
 将数据源传进来，返回模型
 
 @param data 数据源（json反序列化后的数据）
 @return 数据模型
 */
+ (instancetype)getModel:(id)data;
/**
 将数据源传进来，返回模型的数组
 
 @param dataList 数据源（json反序列化后的数据）
 @return 数据模型数组
 */
+ (NSArray *)getModelList:(NSArray *)dataList;


#pragma mark-方便模型使用的一些工具方法

/**
 json数据转换成oc里面的NSArray 或者 NSDictory

 @param jsonStr json字符串
 @return oc对象
 */
+ (id)jsonStrToOcObj:(NSString *)jsonStr;
/**
 oc对象（nsarray/nsdictionary）转成json字符串
 */
+ (NSString *)objToJsonStr:(id)obj;
@end

NS_ASSUME_NONNULL_END
