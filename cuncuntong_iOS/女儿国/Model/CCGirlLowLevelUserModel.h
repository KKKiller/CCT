//
//  CCGirlLowLevelUserModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/19.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCGirlLowLevelUserModel : CCBaseModel

@property (nonatomic,retain)NSString *reader_name;
@property (nonatomic,retain)NSString *reader_photo;

@property (nonatomic,assign)int identity;//等级
@property (nonatomic,assign)int reader_gender;//性别 1：男  2：女
@end

NS_ASSUME_NONNULL_END
