//
//  CCSecretAreaModel.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCSecretAreaModel.h"

@implementation CCSecretAreaModel
- (instancetype)initWithName:(NSString *)name iconImgName:(NSString *)imgUrl itemId:(int)itemId data:(id _Nullable)data{
    if(self = [super init]){
        self.name = name;
        self.iconName = imgUrl;
        self.itemId = itemId;
        self.data = data;
    }
    return self;
}
@end
