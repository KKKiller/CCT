//
//  CCWelfareCell.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCWelfareModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CCWelfareCell : UITableViewCell
@property (nonatomic,retain)CCWelfareModel *cellModel;
@end

NS_ASSUME_NONNULL_END
