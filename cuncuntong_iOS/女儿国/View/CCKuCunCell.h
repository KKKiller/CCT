//
//  CCKuCunCell.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCKuCunModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CCKuCunCell : UITableViewCell
@property (nonatomic,retain)CCKuCunModel *cellModel;
@end

NS_ASSUME_NONNULL_END
