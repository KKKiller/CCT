//
//  CCQueenListView.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCQueenModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol CCQueenListViewDelegate <NSObject>

/**
 获取列表数据
 
 @param page 获取数据的页面
 @param resultBd 返回成功时的数据
 */
- (void)getQueenUsers:(int)page callBack:(void(^)(BOOL isOk,int page,CCQueenListModel *listModel))resultBd;
- (void)queenUsersCellClick:(NSIndexPath *)index model:(CCQueenModel *)model;
@end
@interface CCQueenListView : UIView
@property (nonatomic,assign,readonly)int page;
@property (nonatomic,weak)id<CCQueenListViewDelegate> delegate;//获取数据的代理者

/**
 通知view，它加载到页面了，让页面处理事件
 */
- (void)viewApper;
@end

NS_ASSUME_NONNULL_END
