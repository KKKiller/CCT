//
//  CCQueenListView.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCQueenListView.h"
#import "CCGirlUserModel.h"
#import "CCToastView.h"
@interface CCQueenCell : UITableViewCell
@property (nonatomic,strong)UIImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *subTitleLabel;
@property (nonatomic,strong)UILabel *rightLabel;
@property (nonatomic,strong)UIView *btmLine;
@end

@interface CCQueenListView()<UITableViewDelegate,UITableViewDataSource>{
    int _page;
}
@property (nonatomic,strong)NSMutableArray<CCQueenModel *> *bodyModelList;//列表数据
@property (nonatomic,strong)UITableView *bodyListView;//列表view
@property (nonatomic,strong)UILabel *lingCountLabel;

@property (nonatomic,assign)int currentType;//当前类型
@end

@implementation CCQueenListView
- (instancetype)init{
    if (self = [super init]) {
        [self createUI];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)viewApper{
    [self loadData:_page];
//    if ((_page == 0 && self.bodyModelList.count == 0)|| (self.currentType == [CCGirlUserModel share].currentType)) {
//        [self loadData:_page];
//    }
}
- (void)loadData:(int)page{
    __block typeof(self) this = self;
    [self.delegate getQueenUsers:page callBack:^(BOOL isOk, int page, CCQueenListModel * _Nonnull listModel) {
        //        [this.bodyListView.mj_header endRefreshing];
        [this.bodyListView.mj_footer endRefreshing];
        if (isOk) {
            if (page == 0) {
                [this.bodyModelList removeAllObjects];
            }
            this -> _page = page;
            [this.bodyModelList addObjectsFromArray:listModel.queen_info];
            [this.bodyListView reloadData];
            self.lingCountLabel.text = [NSString stringWithFormat:@"%d个",listModel.queen_count];
        }
    }];
}
- (void)createUI{
    UIView *topView = [[UIView alloc] init];
    [self addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.mas_equalTo(20);
    }];
    UILabel *label = [[UILabel alloc] init];
    [topView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(topView);
        make.right.equalTo(topView).offset(-10);
    }];
    label.textColor = [UIColor colorWithRGB:0xFF99FF];
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"0个";
    self.lingCountLabel = label;
    
    label = [[UILabel alloc] init];
    [topView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(topView);
        make.right.equalTo(self.lingCountLabel.mas_left).offset(-17);
    }];
    label.textColor = [UIColor colorWithRGB:0xFF99FF];
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"女王总数";
    
    
    
    
    UITableView *listView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    listView.showsVerticalScrollIndicator = false;
    listView.separatorStyle = UITableViewCellSeparatorStyleNone;
    listView.dataSource = self;
    listView.delegate = self;
    [listView registerClass:[CCQueenCell class] forCellReuseIdentifier:@"CCQueenCell"];
    
    __weak typeof(self) this = self;
    //    listView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
    //        if (this.delegate) {
    //            [this loadData:0];
    //        }
    //    }];
//    listView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        if (this.delegate) {
//            [this loadData:this.page + 1];
//        }
//    }];
    
    
    [self addSubview:listView];
    [listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.top.equalTo(topView.mas_bottom);
    }];
    
    self.bodyListView = listView;
    

}
#pragma mark - 列表代理事件
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bodyModelList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCQueenCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CCQueenCell"];
    CCQueenModel *model = self.bodyModelList[indexPath.row];
    
    [cell.iconImgView setImageWithURL:[NSURL URLWithString:model.reader_photo] placeholder:[UIImage imageNamed:@"groupHead"]];
    cell.titleLabel.text = [CCGirlUserModel getIdentityName:-1 gender:2];
    cell.titleLabel.textColor = [CCGirlUserModel getIdentityNameColor:-1];
    cell.subTitleLabel.text = model.reader_name;
    cell.rightLabel.text = [NSString stringWithFormat:@"领取  %d盒",model.sum_inventory];
    cell.btmLine.hidden = indexPath.row + 1 == self.bodyModelList.count;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate) {
        [self.delegate queenUsersCellClick:indexPath model:self.bodyModelList[indexPath.row]];
    }
}
#pragma mark-懒加载
- (NSMutableArray<CCQueenModel *> *)bodyModelList{
    if (_bodyModelList == nil) {
        _bodyModelList = [NSMutableArray new];
    }
    return _bodyModelList;
}
@end





@implementation CCQueenCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)createUI{
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.layer.cornerRadius = 13;
    imgView.clipsToBounds = YES;
    [self.contentView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.width.height.mas_equalTo(26);
        make.centerY.equalTo(self.contentView);
    }];
    self.iconImgView = imgView;
    
    //第一个label
    UILabel *label = [[UILabel alloc] init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_right).offset(11);
        make.centerY.equalTo(imgView.mas_centerY);
    }];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    self.titleLabel = label;
    
    //第二个label
    label = [[UILabel alloc] init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).offset(15);
        make.centerY.equalTo(imgView.mas_centerY);
    }];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    self.subTitleLabel = label;
    
    
    
    label = [[UILabel alloc] init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.centerY.equalTo(imgView.mas_centerY);
    }];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    self.rightLabel = label;
    
    
    UIView *line = [[UIView alloc] init];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_left);
        make.bottom.equalTo(self.contentView);
        make.right.equalTo(self.rightLabel.mas_right);
        make.height.mas_equalTo(1);
    }];
    line.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
    self.btmLine = line;
}

@end
