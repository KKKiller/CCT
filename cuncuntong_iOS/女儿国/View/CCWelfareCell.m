//
//  CCWelfareCell.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCWelfareCell.h"
@interface CCWelfareCell()
@property  (nonatomic,retain)UILabel *nameLabel;
@property  (nonatomic,retain)UILabel *rightLabel;
@end
@implementation CCWelfareCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}
- (void)createUI{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"卫生巾";
    [self.contentView  addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(10);
    }];
    self.nameLabel = label;
    
    label = [[UILabel alloc] init];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"7盒";
    [self.contentView  addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    self.rightLabel = label;
    
    
    UIView *line = [[UIView alloc] init];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    line.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
}
- (void)setCellModel:(CCWelfareModel *)cellModel{
    _cellModel = cellModel;
    self.nameLabel.text = cellModel.name;
    
    self.rightLabel.text = [NSString stringWithFormat:@"%d盒",cellModel.num];
}
@end
