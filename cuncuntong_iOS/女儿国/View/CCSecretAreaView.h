//
//  CCSecretAreaView.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCSecretAreaModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol CCSecretAreaDelegate <NSObject>
- (void)secretAreaCellClick:(NSIndexPath *)index model:(CCSecretAreaModel *)model;
@end

@interface CCSecretAreaView : UIView
@property (nonatomic,assign,readonly)int page;
@property (nonatomic,weak)id<CCSecretAreaDelegate> delegate;//获取数据的代理者

/**
 通知view，它加载到页面了，让页面处理事件
 */
- (void)viewApper;
@end

NS_ASSUME_NONNULL_END
