//
//  CCSelectAreaCell.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/10/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCSelectAreaCell.h"

@implementation CCSelectAreaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
