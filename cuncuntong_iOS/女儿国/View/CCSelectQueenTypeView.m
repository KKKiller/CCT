//
//  CCSelectQueenTypeView.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCSelectQueenTypeView.h"

@implementation CCSelectQueenTypeView
- (instancetype)init{
    if (self = [super init]) {
        [self creatUI];
        self.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.5];
    }
    return self;
}
- (void)creatUI{
    UIView *bg = [[UIView alloc] init];
    [self addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(11);
    }];
    bg.layer.cornerRadius = 8;
    bg.clipsToBounds = YES;
    bg.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label = [[UILabel alloc] init];
    [bg addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bg);
        make.top.equalTo(bg).offset(46);
    }];
    label.text = @"请选择面膜还是卫生巾直升女王";
    label.textColor = [UIColor colorWithRGB:0xFE76B4];
    label.font = [UIFont systemFontOfSize:15];
    
    UIView *btnBg = [[UIView alloc] init];
    [bg addSubview:btnBg];
    [btnBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bg).offset(8);
        make.top.equalTo(label).offset(28);
        make.bottom.equalTo(bg).offset(-10);
        make.height.mas_equalTo(57);
        make.centerX.equalTo(bg);
    }];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setTitle:@"面膜" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setViewBg:leftBtn];
    [btnBg addSubview:leftBtn];
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(btnBg);
    }];
    
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"卫生巾" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setViewBg:rightBtn];
    [rightBtn addTarget:self action:@selector(girlBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnBg addSubview:rightBtn];
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftBtn.mas_right).offset(11);
        make.right.equalTo(btnBg).offset(-11);
        make.top.bottom.right.equalTo(btnBg);
        make.width.equalTo(leftBtn);
    }];
}
- (void)setViewBg:(UIView *)view{
    view.backgroundColor = [UIColor colorWithRGB:0xFDD1E5];
    view.layer.cornerRadius = 5;
    view.clipsToBounds = YES;
    
    view.layer.shadowColor = view.backgroundColor.CGColor;
    view.layer.shadowOffset = CGSizeMake(0,5);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 9;
    view.layer.cornerRadius = 4;
}
#pragma mark -按钮点击事件
- (void)leftBtnClick{
    if(self.delegate){
        [self.delegate selectMM];
    }
}
- (void)girlBtnClick{
    if(self.delegate){
        [self.delegate selectWSJ];
    }
}
@end
