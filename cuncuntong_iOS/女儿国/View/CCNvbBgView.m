//
//  CCNvbBgView.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCNvbBgView.h"
@interface CCNvbBgView (){
    UIView *_nvBgView;
}

@property (nonatomic,retain)UIButton *returnBtn;//返回按钮
@property (nonatomic,retain)UILabel *nvTitleLabel;//标题
@end
@implementation CCNvbBgView

- (instancetype)initWithParentView:(UIView *)parentView delegate:(id<CCNvbBgViewDelegate>)delegate{
    if (self = [super init]) {
        self.delegate = delegate;
        //导航栏背景
        [parentView addSubview:self];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.left.equalTo(parentView);
            make.height.mas_equalTo(WCFNavigationHeight);
        }];
    }
    return self;
}
- (void)setRetunBtnImg:(NSString *)img{
    [self.returnBtn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateNormal];
    [self.returnBtn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateHighlighted];
}
- (void)setNvTitle:(NSString *)nvTitle color:(UIColor *)color size:(CGFloat)size{
    self.nvTitleLabel.textColor = color;
    self.nvTitleLabel.font = [UIFont systemFontOfSize:size];
    self.nvTitleLabel.text = nvTitle;
}
#pragma mark - 按钮点击事件
- (void)backMove{
    if (self.delegate) {
        [self.delegate backMove];
    }else{
        if (self.viewController.navigationController) {
            [self.viewController.navigationController popViewControllerAnimated:YES];
        }else{
            [self.viewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
#pragma mark - 懒加载
- (UIView *)nvBgView{
    if (!_nvBgView) {
        _nvBgView = [[UIView alloc] init];
        [self addSubview:_nvBgView];
        [_nvBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.right.left.equalTo(self);
            make.top.equalTo(self).offset(StatusBarHeight);
        }];
    }
    return _nvBgView;
}
- (UILabel *)nvTitleLabel{
    if (_nvTitleLabel == nil) {
        _nvTitleLabel = [[UILabel alloc] init];
        [self.nvBgView addSubview:_nvTitleLabel];
        [_nvTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.nvBgView);
        }];
    }
    return _nvTitleLabel;
}
- (UIButton *)returnBtn{
    if (_returnBtn == nil) {
        _returnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_returnBtn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
        [self.nvBgView addSubview:_returnBtn];
        [_returnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nvBgView).offset(0);
            make.top.bottom.equalTo(self.nvBgView);
            make.width.mas_equalTo(40);
        }];
    }
    return _returnBtn;
}
@end
