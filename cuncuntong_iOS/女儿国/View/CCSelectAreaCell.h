//
//  CCSelectAreaCell.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/10/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCSelectAreaCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
