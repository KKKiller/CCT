//
//  CCSelectSexAlert.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CCSelectSexAlertDelegate <NSObject>
- (void)selectGirlSex;
- (void)selectBoySex;
@end
@interface CCSelectSexAlert : UIButton
@property (nonatomic,weak)id<CCSelectSexAlertDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
