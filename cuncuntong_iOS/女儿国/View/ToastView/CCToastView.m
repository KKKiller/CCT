//
//  GZToastView.m
//  微兼职
//
//  Created by 彭涛 on 15/6/13.
//  Copyright (c) 2015年 guangzhi. All rights reserved.
//

#import "CCToastView.h"

#define getScreenWindow     [[[UIApplication sharedApplication] delegate] window]

@interface CCToastView()
{
    CGFloat textWidth;
}
@end
@implementation CCToastView
- (instancetype)init{
    CGRect frame = CGRectMake(0, 0, kScreenWidth/2, 0);
    if (self = [super initWithFrame:frame]) {
        self.textColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor colorWithRGBA:0x000000cc];
        self.font = [UIFont systemFontOfSize:14];
        self.textAlignment = NSTextAlignmentCenter;
        self.layer.cornerRadius = 5;
        self.numberOfLines = 0;
        self.clipsToBounds = YES;
    }
    return  self;
}
- (void)show{
    
    [getScreenWindow addSubview:self];
    self.center = CGPointMake(kScreenWidth/2, kScreenHeight / 2);
    
    [UIView animateWithDuration:0.5 delay:self.visibleTime options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    [self resignSubViews:getScreenWindow];
}
+ (CCToastView *)showText:(NSString *)text{
    //发送隐藏键盘通知
//    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_HIDE_KEYBOARD object:nil];
    
    CCToastView *this = [[CCToastView alloc] init];
    this.text = text;
    this.visibleTime = 1.5;
    [this show];
    
    return this;
}
- (void)resignSubViews:(UIView *)superView{
    for (UIView *subView in superView.subviews) {
        [subView resignFirstResponder];
        [self resignSubViews:subView];
    }
}
+ (CCToastView *)showText:(NSString *)text visibleTime:(float)time{
    CCToastView *this = [[CCToastView alloc] init];
    this.text = text;
    this.visibleTime = time;
    [this show];
    return this;
}
#pragma mark -重写setText已重新设置UI大小
- (void)setText:(NSString *)text{
    [super setText:text];
    textWidth = kScreenWidth/3*2;
    CGSize size = [self getSizeWidthText:text];
    CGRect frame = self.frame;
    frame.size.height = size.height + 20;
    frame.size.width = size.width + 10;
    if (frame.size.width < 100) {
        frame.size.width = 100;
    }
    self.frame = frame;
}

#pragma mark -获取字符串对应的UI大小
- (CGSize)getSizeWidthText:(NSString *)text{
    CGSize size = [text boundingRectWithSize:CGSizeMake(textWidth, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.font} context:nil].size;
    return size;
}
@end
