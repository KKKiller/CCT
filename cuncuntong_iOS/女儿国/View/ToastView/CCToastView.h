//
//  GZToastView.h
//  微兼职
//
//  Created by 彭涛 on 15/6/13.
//  Copyright (c) 2015年 guangzhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCToastView : UILabel
@property (nonatomic,assign) float visibleTime;
+ (CCToastView *)showText:(NSString *)text;
+ (CCToastView *)showText:(NSString *)text visibleTime:(float)time;
- (void)show;
@end
