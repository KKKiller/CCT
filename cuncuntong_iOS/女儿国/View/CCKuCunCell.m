//
//  CCKuCunCell.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCKuCunCell.h"
@interface CCKuCunCell()
@property (nonatomic,retain)UIImageView *topImg;
@property (nonatomic,retain)UILabel *bodyLabel;
@end
@implementation CCKuCunCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (void)createUI{
    UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_cell_bg_img"]];
    view.userInteractionEnabled = true;
    view.contentMode = UIViewContentModeScaleToFill;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    view.layer.cornerRadius = 6;
    
    UIImageView *topImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tab3_ling_qu_gift"]];
    [view addSubview:topImg];
    [topImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(view);
        make.centerY.equalTo(view).offset(-22);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(54);
    }];
    self.topImg = topImg;
    
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor colorWithRGB:0xF86D6D];
    label.font = [UIFont systemFontOfSize:18];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topImg.mas_bottom).offset(17);
        make.centerX.equalTo(view);
    }];
    self.bodyLabel = label;
}
- (void)setCellModel:(CCKuCunModel *)cellModel{
    _cellModel = cellModel;
    self.bodyLabel.text = [NSString stringWithFormat:@"%@  %d盒",cellModel.name,cellModel.count];
}
@end
