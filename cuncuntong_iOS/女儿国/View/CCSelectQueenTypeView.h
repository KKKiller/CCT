//
//  CCSelectQueenTypeView.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CCSelectQueenTypeViewDelegate <NSObject>
- (void)selectWSJ;
- (void)selectMM;
@end
@interface CCSelectQueenTypeView : UIButton
@property (nonatomic,weak)id<CCSelectQueenTypeViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
