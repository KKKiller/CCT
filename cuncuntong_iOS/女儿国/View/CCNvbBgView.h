//
//  CCNvbBgView.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CCNvbBgViewDelegate <NSObject>

/**
 返回按钮点击事件
 */
- (void)backMove;
@end
@interface CCNvbBgView : UIView
@property (nonatomic,retain,readonly)UIView *nvBgView;//导航栏背景

@property (nonatomic,weak)id<CCNvbBgViewDelegate> delegate;

/**
 初始化方法

 @param parentView 父控件
 @return
 */
- (instancetype)initWithParentView:(UIView *)parentView delegate:(id<CCNvbBgViewDelegate>)delegate;

//设置标题
- (void)setNvTitle:(NSString * _Nonnull)nvTitle color:(UIColor *)color size:(CGFloat)size;
//设置返回按钮
- (void)setRetunBtnImg:(NSString *)img;
@end

NS_ASSUME_NONNULL_END
