//
//  CCGirlLowLevelListView.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGirlLowLevelListView.h"
#import "CCGirlUserModel.h"
@interface CCGirlLowLevelCell : UITableViewCell
@property (nonatomic,strong)UIImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *rightLabel;
@property (nonatomic,strong)UIView *btmLine;
@end


@interface CCGirlLowLevelListView()<UITableViewDelegate,UITableViewDataSource>{
    int _page;
}
@property (nonatomic,strong)NSMutableArray<CCGirlLowLevelUserModel *> *bodyModelList;//列表数据
@property (nonatomic,strong)UITableView *bodyListView;//列表view
@property (nonatomic,assign)int currentType;//当前是哪种类型 0卫生巾  1面膜
@end

@implementation CCGirlLowLevelListView
- (instancetype)init{
    if (self = [super init]) {
        [self createUI];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)viewApper{
    [self loadData:_page];
//    if ((_page == 0 && self.bodyModelList.count == 0) || (self.currentType == [CCGirlUserModel share].currentType)) {
//        [self loadData:_page];
//    }
}
- (void)loadData:(int)page{
    [self.delegate getLowLevelUsers:page callBack:^(BOOL isOk, int page, NSArray<CCGirlLowLevelUserModel *> * _Nonnull models) {
//        [this.bodyListView.mj_header endRefreshing];
        [self.bodyListView.mj_footer endRefreshing];
        if (isOk) {
            _page = page;
            if (page == 0) {
                [self.bodyModelList removeAllObjects];
            }
            [self.bodyModelList addObjectsFromArray:models];
            [self.bodyListView reloadData];
        }else{
            
        }
    }];
}
- (void)createUI{
    UITableView *listView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    listView.showsVerticalScrollIndicator = false;
    listView.separatorStyle = UITableViewCellSeparatorStyleNone;
    listView.dataSource = self;
    listView.delegate = self;
    [listView registerClass:[CCGirlLowLevelCell class] forCellReuseIdentifier:@"CCGirlLowLevelCell"];
    
//    __weak typeof(self) this = self;
//    listView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        if (this.delegate) {
//            [this loadData:0];
//        }
//    }];
//    listView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        if (this.delegate) {
//            [this loadData:this.page + 1];
//        }
//    }];
    
    
    [self addSubview:listView];
    [listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(self);
    }];
    
    self.bodyListView = listView;
}
#pragma mark - 列表代理事件
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bodyModelList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCGirlLowLevelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CCGirlLowLevelCell"];
    CCGirlLowLevelUserModel *model = self.bodyModelList[indexPath.row];
    
    [cell.iconImgView setImageURL:[NSURL URLWithString:model.reader_photo]];
    
    cell.titleLabel.text = model.reader_name;
    cell.rightLabel.text = [CCGirlUserModel getIdentityName:model.identity gender:model.reader_gender];
    cell.btmLine.hidden = indexPath.row + 1 == self.bodyModelList.count;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate) {
        [self.delegate lowLevelUsersCellClick:indexPath model:self.bodyModelList[indexPath.row]];
    }
}
#pragma mark-懒加载
- (NSMutableArray<CCGirlLowLevelUserModel *> *)bodyModelList{
    if (_bodyModelList == nil) {
        _bodyModelList = [NSMutableArray new];
    }
    return _bodyModelList;
}
@end



@implementation CCGirlLowLevelCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)createUI{
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.layer.cornerRadius = 13;
    imgView.clipsToBounds = YES;
    [self.contentView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.width.height.mas_equalTo(26);
        make.centerY.equalTo(self.contentView);
    }];
    self.iconImgView = imgView;
    
    UILabel *label = [[UILabel alloc] init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_right).offset(11);
        make.centerY.equalTo(imgView.mas_centerY);
    }];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    self.titleLabel = label;
    
    label = [[UILabel alloc] init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-45);
        make.centerY.equalTo(imgView.mas_centerY);
    }];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    self.rightLabel = label;
    
    
    UIView *line = [[UIView alloc] init];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_left);
        make.bottom.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-10);
        make.height.mas_equalTo(1);
    }];
    line.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
    self.btmLine = line;
}

@end
