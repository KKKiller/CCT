//
//  CCSelectSexAlert.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCSelectSexAlert.h"

@implementation CCSelectSexAlert
- (instancetype)init{
    if (self = [super init]) {
        [self creatUI];
        self.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.5];
    }
    return self;
}
- (void)creatUI{
    UIView *bg = [[UIView alloc] init];
    [self addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(-70);
        make.left.equalTo(self).offset(11);
    }];
    bg.layer.cornerRadius = 8;
    bg.clipsToBounds = YES;
    bg.backgroundColor = [UIColor whiteColor];
    
    
    UIImageView *renImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_select_ren_img"]];
    [self addSubview:renImgView];
    [renImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bg).offset(10);
        make.bottom.equalTo(bg.mas_top);
    }];
    
    UILabel *label = [[UILabel alloc] init];
    [bg addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bg);
        make.top.equalTo(bg).offset(46);
    }];
    label.text = @"请选择你的性别，一旦选择无法更改";
    label.textColor = [UIColor colorWithRGB:0xFE76B4];
    label.font = [UIFont systemFontOfSize:15];
    
    UIView *btnBg = [[UIView alloc] init];
    [bg addSubview:btnBg];
    [btnBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bg).offset(8);
        make.top.equalTo(label).offset(28);
        make.bottom.equalTo(bg).offset(-10);
        make.height.mas_equalTo(57);
    }];
    
    UIButton *boyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [boyBtn setImage:[UIImage imageNamed:@"girl_sex_boy_btn"] forState:UIControlStateNormal];
    [boyBtn setImage:[UIImage imageNamed:@"girl_sex_boy_btn"] forState:UIControlStateHighlighted];
    [boyBtn addTarget:self action:@selector(bodyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnBg addSubview:boyBtn];
    [boyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(btnBg);
    }];
    
    
    UIButton *girlBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [girlBtn setImage:[UIImage imageNamed:@"girl_sex_girl_btn"] forState:UIControlStateNormal];
    [girlBtn setImage:[UIImage imageNamed:@"girl_sex_girl_btn"] forState:UIControlStateHighlighted];
    [girlBtn addTarget:self action:@selector(girlBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnBg addSubview:girlBtn];
    [girlBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(boyBtn.mas_right);
        make.top.bottom.right.equalTo(btnBg);
    }];
    
}
#pragma mark -按钮点击事件
- (void)bodyBtnClick{
    if(self.delegate){
        [self.delegate selectBoySex];
    }
}
- (void)girlBtnClick{
    if(self.delegate){
        [self.delegate selectGirlSex];
    }
}
@end
