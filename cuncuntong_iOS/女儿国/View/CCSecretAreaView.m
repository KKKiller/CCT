//
//  CCSecretAreaView.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCSecretAreaView.h"
@interface CCSecretAreaCell : UITableViewCell
@property (nonatomic,strong)UIImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *btmLine;
@end

@interface CCSecretAreaView () <UITableViewDelegate,UITableViewDataSource>{
    int _page;
}
@property (nonatomic,strong)NSMutableArray<CCSecretAreaModel *> *bodyModelList;//列表数据
@property (nonatomic,strong)UITableView *bodyListView;//列表view
@end

@implementation CCSecretAreaView
- (instancetype)init{
    if (self = [super init]) {
        [self createUI];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)viewApper{
}

- (void)createUI{
    UITableView *listView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    listView.showsVerticalScrollIndicator = false;
    listView.separatorStyle = UITableViewCellSeparatorStyleNone;
    listView.dataSource = self;
    listView.delegate = self;
    [listView registerClass:[CCSecretAreaCell class] forCellReuseIdentifier:@"CCSecretAreaCell"];
    
    [self addSubview:listView];
    [listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(self);
    }];
    
    self.bodyListView = listView;
}
#pragma mark - 列表代理事件
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bodyModelList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCSecretAreaModel *model = self.bodyModelList[indexPath.row];
    CCSecretAreaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CCSecretAreaCell"];
    [cell.iconImgView setImage:[UIImage imageNamed:model.iconName]];
    cell.titleLabel.text = model.name;
    cell.btmLine.hidden = indexPath.row + 1 == self.bodyModelList.count;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate) {
        [self.delegate secretAreaCellClick:indexPath model:self.bodyModelList[indexPath.row]];
    }
}
#pragma mark-懒加载
- (NSMutableArray<CCSecretAreaModel *> *)bodyModelList{
    if (_bodyModelList == nil) {
        _bodyModelList = [NSMutableArray new];
        
        [_bodyModelList addObject:[[CCSecretAreaModel alloc] initWithName:@"选才任贤" iconImgName:@"tab3_xuan_cai" itemId:0 data:nil]];
         [_bodyModelList addObject:[[CCSecretAreaModel alloc] initWithName:@"加官进禄" iconImgName:@"tab3_jia_guan" itemId:0 data:nil]];
         [_bodyModelList addObject:[[CCSecretAreaModel alloc] initWithName:@"领取福利" iconImgName:@"tab3_ling_qu_gift" itemId:0 data:nil]];
         [_bodyModelList addObject:[[CCSecretAreaModel alloc] initWithName:@"我的库存" iconImgName:@"tab3_ku_cun" itemId:0 data:nil]];
        [_bodyModelList addObject:[[CCSecretAreaModel alloc] initWithName:@"月光宝盒" iconImgName:@"yueguangbaohe" itemId:0 data:nil]];

    }
    return _bodyModelList;
}

@end



@implementation CCSecretAreaCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)createUI{
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.layer.cornerRadius = 13;
    imgView.clipsToBounds = YES;
    [self.contentView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.width.height.mas_equalTo(26);
        make.centerY.equalTo(self.contentView);
    }];
    self.iconImgView = imgView;
    
    //第一个label
    UILabel *label = [[UILabel alloc] init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_right).offset(11);
        make.centerY.equalTo(imgView.mas_centerY);
    }];
    label.textColor = [UIColor colorWithRGB:0x333333];
    label.font = [UIFont systemFontOfSize:15];
    self.titleLabel = label;
    
    
    UIView *line = [[UIView alloc] init];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_left);
        make.bottom.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-10);
        make.height.mas_equalTo(1);
    }];
    line.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
    self.btmLine = line;
}

@end
