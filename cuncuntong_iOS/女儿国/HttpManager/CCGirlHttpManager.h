//
//  CCGirlHttpManager.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/13.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCNetBaseModel.h"
#import "CCGirlLowLevelUserModel.h"
#import "CCGirlUserModel.h"
#import "CCQueenModel.h"
#import "CCKuCunModel.h"
#import "CCWelfareModel.h"
#import "CCGirlUpgradeGoodsModel.h"
#import "CCGirlApplyDeliveryModel.h"
#import "CCAddressModel.h"


#pragma mark - 网络请求回调block
typedef void (^CCGirlNetSucceedBD)(NSURLSessionDataTask *operation,CCNetBaseModel *data);//正常获取数据成功的block
typedef void (^CCGirlNetFailedBD)(NSURLSessionDataTask *operation,NSError *error);//正常获取数据失败的block

NS_ASSUME_NONNULL_BEGIN

@interface CCGirlHttpManager : NSObject
+ (instancetype)share;//单例

/**
 获取用户的女儿国信息
 */
- (void)getGirlUserInfor:(NSString *)userId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCGirlUserModel * _Nullable userModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
 获取用户的女儿国信息
 */
- (void)setGirlUserSex:(NSString *)userId gender:(NSString *)gender succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCGirlUserModel * _Nullable userModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

- (void)girlUserSign:(NSString *)userId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;
/**
 我的部下
 
 @param uid 用户id
 */
- (void)getUserLowLeveList:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCGirlLowLevelUserModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
 女王榜单
 
 @param uid 用户id
 */
- (void)getQueenList:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCQueenListModel * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
 库存
 
 @param uid 用户id
 */
- (void)getGirlKuCun:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCKuCunModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
领取过得福利
 
 @param uid 用户id
 */
- (void)getGiftHistoryList:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCWelfareModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
 发货记录
 
 @param uid 用户id
 */
- (void)alreadyDelivery:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void (^)(CCNetBaseModel * _Nonnull, NSArray<CCWelfareModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD)failBd;
/**
 女王升级购买产品列表
 
 @param uid 用户id
 */
- (void)getGirlUpgradeGoodsList:(NSString *)uid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCGirlUpgradeGoodsModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
  领取福利产品列表
 
 @param uid 用户id
 */
- (void)getGirlWelfareGoodsList:(NSString *)uid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCGirlUpgradeGoodsModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;


/**
 领取奖励
 
 @param uid 用户id
 */
- (void)getGift:(NSString *)uid product_id:(int)productId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,id _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
 领取的详情
 
 @param uid 用户id
 */
- (void)getOrderDetail:(NSString *)uid product_id:(int)orderId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCWelfareModel *_Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;


/**
 被领取的产品的详情信息
 @param uid 用户id
 */
- (void)getKuCunGoodsDetail:(NSString *)uid product_id:(int)orderId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCGirlApplyDeliveryModel *_Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;


/**
 申请发货

 @param uid 用户id
 @param quantity 数量
 @param product_id 产品id
 @param address 地址
 @param phone 手机号
 @param name 收件人姓名
 */
- (void)applyToDelivery:(NSString *)uid quantity:(NSString *)quantity product_id:(int)product_id address:(NSString *)address phone:(NSString *)phone name:(NSString *)name succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

/**
 获取地址列表

 @param uid 用户id
 @param type 0:不含经纬度   1：含经纬度
 */
- (void)getAddressList:(NSString *)uid type:(int)type succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCAddressModel *> *_Nullable model))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;
/**
 直升女王
 @param product_flag 体系标识（1卫生巾体系2面膜体系）
 @param password 登录密码
 */
- (void)becomeQueen:(NSString *)uid product_flag:(NSString *)product_flag password:(NSString *)password succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;
/**
 是否有发货记录
 */
- (void)isDeliveryRecord:(NSString *)uid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;
/**
 选择地址的l地区列表
 */
- (void)getSelectAreaList:(NSString *)pid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray *_Nullable areaList))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;

@end

NS_ASSUME_NONNULL_END

