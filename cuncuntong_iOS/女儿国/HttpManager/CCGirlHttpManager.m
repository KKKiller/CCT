//
//  CCGirlHttpManager.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/13.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGirlHttpManager.h"
@interface CCGirlHttpManager ()
@property (nonatomic,strong)AFHTTPSessionManager *manager;//网络请求工具
@property (nonatomic,retain)NSMutableArray *taskArr;//当前的任务数组

@end
@implementation CCGirlHttpManager
+ (instancetype)share{
    static id tool = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool = [[self alloc] init];
    });
    return tool;
}
- (AFHTTPSessionManager *)manager{
    if (_manager == nil) {
        _manager= [AFHTTPSessionManager manager];
        // 设置请求格式
        //       _manager.requestSerializer = [AFPropertyListRequestSerializer serializer];
        //        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //设置超时 10s
        _manager.requestSerializer.timeoutInterval = 10;
        
        // 设置返回格式
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",nil];
        
        
        
        
        AFSecurityPolicy *policy = [AFSecurityPolicy defaultPolicy];
        policy.allowInvalidCertificates = YES;
        [policy setValidatesDomainName:NO];
        _manager.securityPolicy = policy;
    }
    return _manager;
}
- (NSMutableArray *)taskArr{
    if (_taskArr == nil) {
        _taskArr = [NSMutableArray new];
    }
    return _taskArr;
}
#pragma mark - 网络请求 参数处理方法
- (NSDictionary *)dealPostParams:(NSDictionary *)params{
    return params;
//    if (params == nil) {
//        return @"";
//    }
//    NSString *json = [CCBaseModel objToJsonStr:params];
//    return json;
}
- (id)dealResponseData:(id)response{
    if (response && [response isKindOfClass:[NSDictionary class]]) {
        CCNetBaseModel *result = [CCNetBaseModel getModel:response];
        result.originData = response;
        return result;
    }
    return nil;
}


#pragma mark - 基本的网络数据下载方法
- (void)postJsonDataWithUrlStr:(NSString *)urlStr header:(NSDictionary *)header postDict:(id)dict succeedBD:(CCGirlNetSucceedBD)succeedBD failedBD:(CCGirlNetFailedBD)failedBD {
    for (NSString *key in header.allKeys) {
        [self.manager.requestSerializer setValue:header[key] forHTTPHeaderField:key];
    }
    
    urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"PT>> %@  >>  %@",urlStr,dict);
    __block CCGirlHttpManager *this = self;
    NSURLSessionDataTask *operation = [self.manager POST:urlStr parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [this.taskArr removeObject:task];
        NSLog(@"PT>> %@    >>>>>   \n%@",urlStr,responseObject);
        if (succeedBD) {
            id data = [this dealResponseData:responseObject];
            succeedBD(task, data);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [this.taskArr removeObject:task];
        if (failedBD) {
            failedBD(task,error);
        }
        
    } ];
    [self.taskArr addObject:operation];
}
//get方法获取数据
- (void)getJsonDataWithUrlStr:(NSString *)urlStr header:(NSDictionary *)header params:(NSDictionary *)dict succeedBD:(CCGirlNetSucceedBD)succeedBD failedBD:(CCGirlNetFailedBD)failedBD{
    for (NSString *key in header.allKeys) {
        [self.manager.requestSerializer setValue:header[key] forHTTPHeaderField:key];
    }
    
    urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"PT>> %@  >>  %@",urlStr,dict);
    __block CCGirlHttpManager *this = self;
    NSURLSessionDataTask *operation = [self.manager GET:urlStr parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [this.taskArr removeObject:task];
        NSLog(@"PT>> %@    >>>>>   \n%@",urlStr,responseObject);
        if (succeedBD) {
            id data = [this dealResponseData:responseObject];
            succeedBD(task, data);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [this.taskArr removeObject:task];
        if (failedBD) {
            failedBD(task,error);
        }
        
    }];
    [self.taskArr addObject:operation];
}
#pragma mark - 统一的网络请求方法
- (void)postDataToNet:(NSString *)url postParams:(NSDictionary *)params succeedBD:(CCGirlNetSucceedBD)succeedBD failedBD:(CCGirlNetFailedBD)failedBD{
    
    NSDictionary *dict = [self dealPostParams:params];
    
    url = BASEURL_WITHOBJC(url);
    
    [self postJsonDataWithUrlStr:url header:nil  postDict:dict succeedBD:succeedBD failedBD:failedBD];
}
- (void)getNetData:(NSString *)url postParams:(NSDictionary *)params succeedBD:(CCGirlNetSucceedBD)succeedBD failedBD:(CCGirlNetFailedBD)failedBD{
    
    NSDictionary *dict = [self dealPostParams:params];
    
    url = BASEURL_WITHOBJC(url);
    [self getJsonDataWithUrlStr:url header:nil params:dict succeedBD:succeedBD failedBD:failedBD];
}
#pragma mark ------------------- 以下是具体业务请求方法 ------------------
- (void)getGirlUserInfor:(NSString *)userId succeedBd:(void (^)(CCNetBaseModel * _Nonnull, CCGirlUserModel * _Nullable))succeedBd failBd:(CCGirlNetFailedBD)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = userId;
    [self postDataToNet:@"kof/getActivitiesReaderInfo" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                CCGirlUserModel *model = [CCGirlUserModel getModel:data.data];
                succeedBd(data,model);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)setGirlUserSex:(NSString *)userId gender:(NSString *)gender succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCGirlUserModel * _Nullable userModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = userId;
    params[@"gender"] = gender;
    [self postDataToNet:@"kof/participateInActivities" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                CCGirlUserModel *model = [CCGirlUserModel getModel:data.data];
                succeedBd(data,model);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)girlUserSign:(NSString *)userId succeedBd:(void (^)(CCNetBaseModel * _Nonnull))succeedBd failBd:(CCGirlNetFailedBD)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = userId;
    [self postDataToNet:@"kof/signIn" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            succeedBd(data);
        }
        
    } failedBD:failBd];
}
- (void)getUserLowLeveList:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void (^)(CCNetBaseModel * _Nonnull, NSArray<CCGirlLowLevelUserModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_flag"] = flag;
    [self postDataToNet:@"kof/myUnder" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                NSArray<CCGirlLowLevelUserModel *> *modelList = [CCGirlLowLevelUserModel getModelList:data.data];
                succeedBd(data,modelList);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)getQueenList:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCQueenListModel * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd;{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_flag"] = flag;
    [self postDataToNet:@"kof/getQueenInfo" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                CCQueenListModel *listModel = [CCQueenListModel getModel:data.data];
                succeedBd(data,listModel);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)getGirlKuCun:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCKuCunModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_flag"] = flag;
    [self postDataToNet:@"kof/getMyInventory" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                NSArray<CCKuCunModel *> *modelList = [CCKuCunModel getModelList:data.data];
                succeedBd(data,modelList);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)getGiftHistoryList:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void (^)(CCNetBaseModel * _Nonnull, NSArray<CCWelfareModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_flag"] = flag;
    [self postDataToNet:@"kof/getMyWelfareLog" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                NSArray<CCWelfareModel *> *modelList = [CCWelfareModel getModelList:data.data];
                succeedBd(data,modelList);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)alreadyDelivery:(NSString *)uid product_flag:(NSString *)flag succeedBd:(void (^)(CCNetBaseModel * _Nonnull, NSArray<CCWelfareModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_flag"] = flag;
    [self postDataToNet:@"kof/getMyWelfareLog" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                NSArray<CCWelfareModel *> *modelList = [CCWelfareModel getModelList:data.data];
                succeedBd(data,modelList);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}

- (void)getGirlUpgradeGoodsList:(NSString *)uid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCGirlUpgradeGoodsModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    [self getNetData:@"kof/activityProductList" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                NSArray<CCGirlUpgradeGoodsModel *> *modelList = [CCGirlUpgradeGoodsModel getModelList:data.data];
                succeedBd(data,modelList);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)getGirlWelfareGoodsList:(NSString *)uid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCGirlUpgradeGoodsModel *> * _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    [self getNetData:@"kof/welfareProductList" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                NSArray<CCGirlUpgradeGoodsModel *> *modelList = [CCGirlUpgradeGoodsModel getModelList:data.data];
                succeedBd(data,modelList);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}

- (void)getGift:(NSString *)uid product_id:(int)productId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,id _Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_id"] = [NSString stringWithFormat:@"%d",productId];
    [self getNetData:@"kof/getWelfare" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                succeedBd(data,data.data);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)getOrderDetail:(NSString *)uid product_id:(int)orderId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCWelfareModel *_Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"order_id"] = [NSString stringWithFormat:@"%d",orderId];
    [self postDataToNet:@"kof/deliverGoodsDetails" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                CCWelfareModel *model = [CCWelfareModel getModel:data.data];
                succeedBd(data,model);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)getKuCunGoodsDetail:(NSString *)uid product_id:(int)orderId succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,CCGirlApplyDeliveryModel *_Nullable))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_id"] = [NSString stringWithFormat:@"%d",orderId];
    [self postDataToNet:@"kof/applyForDeliverGoods" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                CCGirlApplyDeliveryModel *model = [CCGirlApplyDeliveryModel getModel:data.data];
                succeedBd(data,model);
            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)applyToDelivery:(NSString *)uid quantity:(NSString *)quantity product_id:(int)product_id address:(NSString *)address phone:(NSString *)phone name:(NSString *)name succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
 //
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"uid"] = uid;
    params[@"product_id"] = [NSString stringWithFormat:@"%d",product_id];
    params[@"quantity"] = quantity;
    params[@"address"] = address;
    params[@"phone"] = phone;
    params[@"name"] = name;
    [self postDataToNet:@"kof/delivery" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                succeedBd(data);
            }else{
                succeedBd(data);
            }
        }
        
    } failedBD:failBd];
}
- (void)getAddressList:(NSString *)uid type:(int)type succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray<CCAddressModel *> *_Nullable model))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    //
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"userid"] = uid;
    params[@"offset"] = @"0";
    params[@"limit"] = @"30";
    if (type >= 0) {
        params[@"type"] = [NSString stringWithFormat:@"%d",type];
    }
    [self postDataToNet:@"share/getaddress" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            if (data.error == 0) {
                NSArray *array = data.data;
                NSMutableArray<CCAddressModel *> *resultList = [NSMutableArray new];
                for (NSDictionary *dic  in array) {
                    CCAddressModel *model = [CCAddressModel modelWithJSON:dic];
                    [resultList addObject:model];
                }
                succeedBd(data,resultList);

            }else{
                succeedBd(data,nil);
            }
        }
        
    } failedBD:failBd];
}
- (void)becomeQueen:(NSString *)uid product_flag:(NSString *)product_flag password:(NSString *)password succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"userid"] = uid;
    params[@"product_flag"] = product_flag;
    params[@"password"] = password;
    [self postDataToNet:@"kof/upgradeQueen" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            succeedBd(data);
        }
        
    } failedBD:failBd];
}
/**
 是否有发货记录
 @param product_flag 体系标识（1卫生巾体系2面膜体系）
 @param password 登录密码
 */
- (void)isDeliveryRecord:(NSString *)uid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"userid"] = uid;
    [self postDataToNet:@"kof/isDeliveryRecord" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            succeedBd(data);
        }
        
    } failedBD:failBd];
}
/**
 选择地址的l地区列表
 */
- (void)getSelectAreaList:(NSString *)pid succeedBd:(void(^_Nullable)(CCNetBaseModel *baseModel,NSArray *_Nullable areaList))succeedBd failBd:(CCGirlNetFailedBD _Nullable)failBd{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"pid"] = pid;
    [self postDataToNet:@"main/area" postParams:params succeedBD:^(NSURLSessionDataTask *operation, CCNetBaseModel *data) {
        if (succeedBd) {
            succeedBd(data,data.data[@"village"]);
        }
        
    } failedBD:failBd];
}


@end

