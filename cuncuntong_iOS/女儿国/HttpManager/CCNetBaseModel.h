//
//  CCNetBaseModel.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/13.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCNetBaseModel : CCBaseModel
@property (nonatomic,assign)int error;//返回状态, 0:返回正确
@property (nonatomic,retain)NSString *msg;//返回提示文字
@property (nonatomic,retain)id data;//返回结果数据

@property (nonatomic,assign)int status;//返回状态, 9000:返回正确


@property (nonatomic,retain)id originData;//网络请求的原始数据


@end

NS_ASSUME_NONNULL_END
