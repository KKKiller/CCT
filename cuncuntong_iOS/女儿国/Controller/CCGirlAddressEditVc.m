//
//  CCAddressEditVc.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/10/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGirlAddressEditVc.h"
#import "CCMyFooter.h"
#import "CCMyCell.h"
#import "CCAddressModel.h"
#import "CCHobbyModel.h"
#import "CCSelectAreaVc.h"
@interface CCGirlAddressEditVc ()<UITableViewDataSource,UITableViewDelegate,CCMyCellDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;

@property (nonatomic,strong)NSString *village_id;

@property (nonatomic,retain)UITextField *nameView;
@property (nonatomic,retain)UITextField *phoneView;
@property (nonatomic,retain)UITextField *addressView;
@property (nonatomic,retain)UIButton *addressBtn;
@property (nonatomic,retain)UITextField *saveBtn;
@end

@implementation CCGirlAddressEditVc
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
    [self loadData];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
}

- (void)refreshData {
    [self loadData];
}

- (void)setUI {
    self.title = @"地址";
    CCNvbBgView *header = [[CCNvbBgView alloc] initWithParentView:self.view delegate:self];
    header.backgroundColor = [UIColor colorWithRGB:0xFDD1E5];
    [header setRetunBtnImg:@"return_bg_yellow_icon"];
    [header setNvTitle:@"地址" color:[UIColor colorWithRGB:0xF86D6D] size:18];
    
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.top.equalTo(header.mas_bottom);
    }];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenSize.width, 219)];
    self.tableView.tableHeaderView = bgView;

    UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"CCGirlAddressHeaderView" owner:nil options:nil] lastObject];
    [bgView addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(bgView);
    }];
    self.nameView = [headerView viewWithTag:1111];
    self.phoneView = [headerView viewWithTag:1112];
    self.addressView = [headerView viewWithTag:1113];
    self.addressBtn = [headerView viewWithTag:1114];
    self.saveBtn = [headerView viewWithTag:1115];
    
    [self.addressBtn addTarget:self action:@selector(selectAddress) forControlEvents:UIControlEventTouchUpInside];
    [self.saveBtn addTarget:self action:@selector(saveBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - 获取数据
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"userid":USERID,@"offset":@"0",@"limit":@"30",@"type":@"0"}];
    
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/getaddress") params:params target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSArray *array = success[@"data"];
            [self.dataArray removeAllObjects];
            for (NSDictionary *dic  in array) {
                CCAddressModel *model = [CCAddressModel modelWithJSON:dic];
                [self.dataArray addObject:model];
            }
        }else{
            
        }
        [self.tableView reloadData];
    } failure:^(NSError *failure) {
    }];
}

#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCAddressModel *model = self.dataArray[indexPath.row];

    CCMyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    cell.delegate = self;
    cell.model = model;
    cell.addressName.text = [NSString stringWithFormat:@"姓  名：%@",model.realname];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CCAddressModel *model = self.dataArray[indexPath.row];
    if (self.selectAddressBlock) {
        self.selectAddressBlock(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)selectAddress{
    
    CCSelectAreaVc *vc = [CCSelectAreaVc new];
    __block typeof(self) this = self;
    vc.resultBd = ^(NSString * _Nonnull village_id, NSString * _Nonnull areaName) {
        this.village_id = village_id;
        this.addressView.text = areaName;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
//保存
- (void)saveBtnClick {
    if (!self.nameView.text || self.nameView.text.length == 0) {
        SHOW(@"请输入姓名");
        return;
    }
    if (self.phoneView.text.length == 0) {
        SHOW(@"请输入电话号码");
        return;
    }
    if (self.addressView.text.length == 0) {
        SHOW(@"请选择地址");
        return;
    }


    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"realname":self.nameView.text,@"userid":USERID,@"phone":self.phoneView.text,@"location_info":self.addressView.text,@"village_id":self.village_id}];
    //    if (self.addressType) {
    //        params[@"type"] = self.addressType;
    //    }
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/address") params:params target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"添加成功");
            [self refreshData];
            self.nameView.text = nil;
            self.phoneView.text = @"";
            self.addressView.text = @"";
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}
//删除
- (void)deleteBtnClick:(CCAddressModel *)model {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/deleteaddress") params:@{@"userid":USERID,@"address_id":model.id} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"删除成功");
            [self refreshData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}

#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                
        _tableView.rowHeight = 120;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCMyCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

@end
