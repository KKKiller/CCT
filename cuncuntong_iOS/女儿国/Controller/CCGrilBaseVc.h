//
//  CCGrilBaseVc.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CCNvbBgView.h"
#import "CCGirlHttpManager.h"
#import "CCToastView.h"
NS_ASSUME_NONNULL_BEGIN

@interface CCGrilBaseVc : BaseViewController<CCNvbBgViewDelegate>
@end

NS_ASSUME_NONNULL_END
