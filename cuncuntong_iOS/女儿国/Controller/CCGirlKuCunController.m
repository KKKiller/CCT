//
//  CCKuCunController.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGirlKuCunController.h"
#import "CCKuCunCell.h"
#import "CCApplyToDeliveryController.h"
#import "CCApplyDeliveryHistoryVc.h"
@interface CCGirlKuCunController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,retain)UITableView *bodyListView;//列表
@property (nonatomic,retain)UIButton *rightHistoryBtn;//发货记录按钮

@property (nonatomic,retain)NSMutableArray<CCKuCunModel *> *bodyModels;
@end

@implementation CCGirlKuCunController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createBodyUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadNetData:0];
    [self getIsDeliveryRecord];
}
#pragma mark - 初始化UI视图
- (void)createBodyUI{
    self.view.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
    CCNvbBgView *header = [[CCNvbBgView alloc] initWithParentView:self.view delegate:self];
    header.backgroundColor = self.view.backgroundColor;
    [header setRetunBtnImg:@"return_bg_yellow_icon"];
    [header setNvTitle:@"我的库存" color:[UIColor colorWithRGB:0xF86D6D] size:18];
    
    UIButton *historyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [historyBtn setTitle:@"发货记录" forState:UIControlStateNormal];
    [historyBtn setTitleColor:[UIColor colorWithRGB:0x333333] forState:UIControlStateNormal];
    historyBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [historyBtn addTarget:self action:@selector(historyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [header.nvBgView addSubview:historyBtn];
    [historyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(header.nvBgView);
        make.right.equalTo(header.nvBgView).offset(-10);
        make.height.mas_equalTo(30);
    }];
    historyBtn.hidden = YES;
    self.rightHistoryBtn = historyBtn;
    
    //主体列表
    UITableView *listView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    listView.showsVerticalScrollIndicator = false;
    listView.separatorStyle = UITableViewCellSeparatorStyleNone;
    listView.backgroundColor = [UIColor clearColor];
    listView.dataSource = self;
    listView.delegate = self;
    [listView registerClass:[CCKuCunCell class] forCellReuseIdentifier:NSStringFromClass([CCKuCunCell class])];
    
//    __weak typeof(self) this = self;
//    listView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//
//    }];
//    listView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//
//    }];
    
    
    [self.view addSubview:listView];
    [listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(header.mas_bottom);
    }];
    
    self.bodyListView = listView;
}
#pragma mark - 加载列表数据
- (void)loadNetData:(NSInteger) page{
    NSString *flag = [NSString stringWithFormat:@"%d",[[CCGirlUserModel share] currentType]];
    [[CCGirlHttpManager share] getGirlKuCun:USERID product_flag:flag succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray<CCKuCunModel *> * _Nullable modeList) {
        if(page == 0){
            [self.bodyModels removeAllObjects];
        }
        if (baseModel.error == 0) {
            [self.bodyModels addObjectsFromArray:modeList];
        }else{
            [CCToastView showText:baseModel.msg];
        }
        [self.bodyListView reloadData];

    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误!"];
    }];
}
//是否有发货记录接口
- (void)getIsDeliveryRecord{
    [[CCGirlHttpManager share] isDeliveryRecord:USERID succeedBd:^(CCNetBaseModel * _Nonnull baseModel) {
        if (baseModel.error == 0) {
            self.rightHistoryBtn.hidden = [baseModel.data intValue] == 0;
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        
    }];
}
#pragma mark 历史按钮点击事件
- (void)historyBtnClick{
    CCApplyDeliveryHistoryVc *vc = [CCApplyDeliveryHistoryVc new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 列表代理事件
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bodyModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCKuCunCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CCKuCunCell class])];
    CCKuCunModel *model = self.bodyModels[indexPath.row];
    [cell setCellModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CCKuCunModel *model = self.bodyModels[indexPath.row];
    CCApplyToDeliveryController *vc = [[CCApplyToDeliveryController alloc] init];
    vc.productData = model;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -懒加载
- (NSMutableArray<CCKuCunModel *> *)bodyModels{
    if (_bodyModels == nil) {
        _bodyModels = [NSMutableArray new];
    }
    return _bodyModels;
}
@end
