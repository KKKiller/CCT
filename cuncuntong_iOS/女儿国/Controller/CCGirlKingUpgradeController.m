//
//  CCGirlKingUpgradeController.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGirlKingUpgradeController.h"
#import "CCBuyDetailController.h"
#import "CCSelectQueenTypeView.h"
#import "CCInputKeywordView.h"
#import "RechargeViewController.h"
#define MID_GOODS_BUY_BTN_BASE_TAG 2091 // 中间购买按钮tag的基数
@interface CCGirlKingUpgradeController ()<CCSelectQueenTypeViewDelegate>
@property (nonatomic,retain)UIView *midBgView;//中间产品列表的背景view
@property (nonatomic,retain)NSArray<CCGirlUpgradeGoodsModel *> *bodyModelList;//产品列表数据

@property (nonatomic, strong) CCInputKeywordView *keywordView;//密码输入控件
@property (strong, nonatomic) PathDynamicModal *animateModel;//

@property (nonatomic,retain)CCSelectQueenTypeView *queenTypeView;//用户选择女王分类的控件
@property (nonatomic,retain)NSString *queenType;//女王类型
@property (nonatomic,assign)float balance;//我的钱数
@end

@implementation CCGirlKingUpgradeController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    [self createUI];
    
    //返回按钮
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:btn];
    [btn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(10);
        make.top.equalTo(self.view).offset(StatusBarHeight + 5);
        make.width.height.mas_equalTo(35);
    }];
    
    
    [self loadNetGoodsList];
    
    [self getMyMoney];
}
#pragma mark - 初始化视图
- (void)createUI{
    self.view.backgroundColor = [UIColor colorWithRGB:0xFFF2FA];

    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = false;
    scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    //去除scrollView极其子类的内边距
    if (@available(iOS 11.0, *)) {
        scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    
    UIView * contentView = [[UIView alloc] init];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(scrollView);
        make.width.equalTo(scrollView);
        make.height.greaterThanOrEqualTo(@0.f);//此处保证容器View高度的动态变化 大于等于0.f的高度
    }];
    
    UIImageView *topImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_nvwang_top_bg"]];
    [contentView addSubview:topImg];
    [topImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(contentView);
        make.height.mas_equalTo(702 * kScreenWidth / 375);
        make.width.equalTo(contentView);
    }];
    

    
    /*
     中间产品列表的背景view
     */
    UIView *midBgView = [[UIView alloc] init];
    [contentView addSubview:midBgView];
    [midBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topImg.mas_bottom).offset(6);
        make.left.equalTo(contentView).offset(10);
        make.right.equalTo(contentView).offset(-10);
        make.height.mas_equalTo(0);
    }];
//    midBgView.backgroundColor = [UIColor whiteColor];
//    midBgView.layer.cornerRadius = 6;
//    midBgView.layer.shadowColor = [UIColor colorWithRGB:0xFF94B1].CGColor;
//    midBgView.layer.shadowOffset = CGSizeMake(0,4);
//    midBgView.layer.shadowOpacity = 1;
//    midBgView.layer.shadowRadius = 4;
    self.midBgView = midBgView;
    
    
    
   
    
    
    //创建底部视图
    UIImage *signImg = [UIImage imageNamed:@"girl_up_nvwang_btn"];
    signImg =  [signImg resizableImageWithCapInsets:UIEdgeInsetsMake(0, 100, 0, 100) resizingMode:UIImageResizingModeStretch];
    UIButton *signBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [signBtn addTarget:self action:@selector(btmBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [signBtn setTitle:@"直升女王" forState:UIControlStateNormal];
    [signBtn setBackgroundImage:signImg forState:UIControlStateNormal];
    [contentView addSubview:signBtn];
    [signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView).offset(24);
        make.right.equalTo(contentView).offset(-24);
        make.top.equalTo(midBgView.mas_bottom).offset(34);
        make.height.mas_equalTo(53);
    }];
    
    
    
    //底部图片
    UIImage *img = [UIImage imageNamed:@"girl_get_btm_bg"];
    img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(0, 100, 0, 100) resizingMode:UIImageResizingModeStretch];
    UIImageView *btmImgView = [[UIImageView alloc] initWithImage:img];
    [contentView addSubview:btmImgView];
    [btmImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(signBtn.mas_bottom).offset(4);
        make.left.equalTo(contentView).offset(0);
        make.right.equalTo(contentView).offset(0);
        make.height.mas_equalTo(107 * kScreenWidth / 375);
        make.bottom.equalTo(contentView);
    }];
}
#pragma mark - 网络请求列表数据
- (void)loadNetGoodsList{
    [[CCGirlHttpManager share] getGirlUpgradeGoodsList:USERID succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray<CCGirlUpgradeGoodsModel *> * goodsList) {
        if (baseModel.error == 0) {
            self.bodyModelList = goodsList;
            [self resetBodyGoodsListUI];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络请求失败！"];
    }];
}
- (void)resetBodyGoodsListUI{
    [self.midBgView removeAllSubviews];
    [self.midBgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0);
    }];
    
    for (int i = 0; i < self.bodyModelList.count; i++) {
        [self createMidItemView:i model:self.bodyModelList[i] isShowBtmLine:(i < self.bodyModelList.count - 1)];
    }
    if (self.bodyModelList.count > 0) {
        [self.midBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([[self.midBgView.subviews lastObject] bottom] + 34);
        }];
    }
   
}

#pragma mark - 创建中间的item项
- (void)createMidItemView:(int)num model:(CCGirlUpgradeGoodsModel *)model isShowBtmLine:(BOOL)isShowLine{
    CGFloat leftRightGap = 25,midGap = 15;
    CGFloat topGap = 34;
    CGFloat bgBgHeight = 70;
    CGFloat itemWidth = (kScreenWidth - leftRightGap * 2 - midGap) / 2;
    CGFloat itemHeight = itemWidth * 105 / 155 + bgBgHeight;
    CGFloat originX = leftRightGap + (itemWidth + midGap) * (num % 2);
    CGFloat originY = topGap + (topGap + itemHeight) * (num / 2);
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(originX, originY, itemWidth, itemHeight)];
    bgView.backgroundColor = [UIColor colorWithRGB:0xFFE1E1];
    [self.midBgView addSubview:bgView];
    
    //按钮背景
    UIView *btnBg = [[UIView alloc] init];
    [bgView addSubview:btnBg];
    [btnBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(bgView);
        make.height.mas_equalTo(bgBgHeight);
    }];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"立即购买" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setBackgroundImage:[UIImage imageNamed:@"girl_buy_btn_bg"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"girl_buy_btn_bg"] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnBg addSubview:btn];
    btn.tag = MID_GOODS_BUY_BTN_BASE_TAG + num;
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(btnBg);
    }];
    
    UIImageView *goodsIconView = [[UIImageView alloc] init];
    [goodsIconView setImageURL:[NSURL URLWithString:model.path_image]];
    [bgView addSubview:goodsIconView];
    [goodsIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(bgView);
        make.bottom.equalTo(btnBg.mas_top);
    }];
}

#pragma mark - 按钮点击事件
/**
 点击购买商品
 
 @param btn
 */
- (void)buyBtnClick:(UIButton *)btn{
    NSInteger index = btn.tag - MID_GOODS_BUY_BTN_BASE_TAG;
    CCGirlUpgradeGoodsModel *model = self.bodyModelList[index];
    
    CCWineModel *wineModel = [CCWineModel new];
    wineModel.id = [NSString stringWithFormat:@"%d",model.id_];
    wineModel.image = model.path_image;
    CCBuyDetailController *vc = [[CCBuyDetailController alloc]init];
    vc.wineModel = wineModel;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 弹出女王类型选择框
- (void)btmBtnClick{
    CCSelectQueenTypeView *alert = [[CCSelectQueenTypeView alloc] init];
    //    [alert addTarget:self action:@selector(sexSelectBgClick) forControlEvents:UIControlEventTouchUpInside];
    alert.delegate = self;
    UIWindow *win = [[[UIApplication sharedApplication] delegate] window];
    [win addSubview:alert];
    [alert mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(win);
    }];
    self.queenTypeView = alert;
}
- (void)selectMM{
    self.queenTypeView.hidden = YES;
    self.queenType = @"2";
    [self inputPassword];
}
- (void)selectWSJ{
    self.queenTypeView.hidden = YES;
    self.queenType = @"1";
    [self inputPassword];
}
- (void)goToQueen:(NSString *)product_flag{
    [[CCGirlHttpManager share] becomeQueen:USERID product_flag:product_flag password:self.keywordView.keywordField.text succeedBd:^(CCNetBaseModel * _Nonnull baseModel) {
        if (baseModel.error == 0) {
            [self closeRed];
        }else{
            
        }
        [CCToastView showText:baseModel.msg];
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}
#pragma mark - 底部键盘
- (void)getMyMoney{
    [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/money") params:@{@"id": STR([UserManager sharedInstance].secret)} target:self success:^(NSDictionary *success) {
        self.balance = [success[@"data"][@"money"] floatValue];
    } failure:^(NSError *failure) {
    }];
}
- (void)inputPassword {
    [self.view endEditing:YES];
    CCInputKeywordView *view = [CCInputKeywordView instanceView];
    self.keywordView = view;
    [view.closeBtn addTarget:self action:@selector(closeRed) forControlEvents:UIControlEventTouchUpInside];
    [view.payBtn addTarget:self action:@selector(payBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [view.chargeBtn addTarget:self action:@selector(chargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    view.layer.cornerRadius = 6;
    view.layer.masksToBounds  = YES;
    view.frame = CGRectMake(0, 0, 240, 270);
    view.keywordField.returnKeyType = UIReturnKeyDone;
    //    view.keywordField.delegate = self;
    
    view.redMoney = 1800;
    view.balance = self.balance;
    view.nameLbl.text = @"支付金额";
    
    self.animateModel = [[PathDynamicModal alloc]init];
    self.animateModel.closeBySwipeBackground = YES;
    self.animateModel.closeByTapBackground = YES;
    [self.animateModel showWithModalView:view inView:self.view];
}
- (void)closeRed {
    [self.animateModel closeWithStraight];
}
//充值
- (void)chargeBtnClick {
    [self closeRed];
    
    RechargeViewController *vc = [[RechargeViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
//发红包
- (void)payBtnClick {
    if (self.keywordView.keywordField.text.length == 0) {
        SHOW(@"请输入登录密码");
        return;
    }
    [self goToQueen:self.queenType];
    
}
@end
