//
//  CCGirlCountryHomeController.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/6.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGirlCountryHomeController.h"
#import "CCGirlLowLevelListView.h"
#import "CCQueenListView.h"
#import "CCSecretAreaView.h"
#import "CCSelectSexAlert.h"
#import "CCGirlWelfareHistory.h"
#import "CCGirlKuCunController.h"
#import "CCApplyToDeliveryController.h"
#import "CCApplyDetailController.h"
#import "CCGetGiftListController.h"
#import "CCGirlKingUpgradeController.h"
#import "CCGirlHttpManager.h"
#import "CCShareView.h"
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "AccountViewController.h"
#define GirlShareUrl @"http://wanshitong.com/village/public/center/qrcodereg"
#define TabBaseBtnTag 1000
@interface CCGirlCountryHomeController ()<CCGirlLowLevelListViewDelegate,CCQueenListViewDelegate,CCSecretAreaDelegate,CCSelectSexAlertDelegate,CNContactPickerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic,strong)UIImageView *userBgImgView;//用户头像背景图片（标示身份的图片）
@property (nonatomic,strong)UIImageView *userIconImgView;//用户头像
@property (nonatomic,strong)UILabel *userNameLabel;//用户头像

@property (nonatomic,strong)UIButton *resetBtn;//切换面膜按钮
@property (nonatomic,strong)UIButton *signBtn;//签到按钮

@property (nonatomic,strong)UIImageView *midIndicatorLine;//中间底部的滑块

@property (nonatomic,retain)UIView *bodyContainer;//底部切换的视图容器
@property (nonatomic,strong)CCGirlLowLevelListView *lowLevelView;//我的部下主体视图
@property (nonatomic,strong)CCQueenListView *queenListView;//女王榜主体视图
@property (nonatomic,strong)CCSecretAreaView *secretAreaView;//私密领地主体视图

@property (nonatomic,strong)CCSelectSexAlert *sexSelectView;//性别选择弹框


@property (nonatomic,strong)UIButton *signOkView;//签到成功弹框
@property (nonatomic,strong)UILabel *signGetCountLabel;//签到获得数量Label


@property (nonatomic,retain)CCShareView *shareView;//分享控件

@end

@implementation CCGirlCountryHomeController
#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    //初始化页面
    UIView *headerView = [self createHeaderView];
    UIView *midTabView = [self createMidTabView:headerView];
    [self createBtmContainer:midTabView];
    
    //默认选中第一个
    [self midTabBtnClick:[self.view viewWithTag:TabBaseBtnTag]];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //获取用户数据
    [self getGirlUserData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self shareCancelBtnClick];
}

#pragma mark - UI初始化
/**
 创建顶部视图
 */
- (UIView *)createHeaderView{
    UIView *headerBgView = [[UIView alloc] init];
    headerBgView.backgroundColor = [UIColor colorWithRGB:0xFF99C7];
    [self.view addSubview:headerBgView];
    [headerBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(200);
    }];
    
    //背景图片
    UIImageView *bgImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_home_bg"]];
    [headerBgView addSubview:bgImgView];
    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(headerBgView);
    }];
    
    /**
     中间用户头像跟昵称部分
     */
    UIView *userBgView = [[UIView alloc] init];
    [headerBgView addSubview:userBgView];
    [userBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(headerBgView);
        make.bottom.equalTo(headerBgView).offset(-20);
    }];
    
    //头像背景图
    UIImageView *userbgImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_level_0"]];
    [userBgView addSubview:userbgImg];
    [userbgImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(userBgView);
        make.height.mas_equalTo(96);
//        make.width.mas_equalTo(124);
    }];
    self.userBgImgView = userbgImg;
    
    //头像图片
    self.userIconImgView = [[UIImageView alloc] init];
    self.userIconImgView.layer.cornerRadius = 31;
    self.userIconImgView.clipsToBounds = YES;
    self.userIconImgView.userInteractionEnabled = YES;
    [userBgView addSubview:self.userIconImgView];
    [self.userIconImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(userBgView);
        make.bottom.equalTo(userbgImg.mas_bottom).offset(-6);
        make.width.height.mas_equalTo(62);
    }];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showActionSheet)];
    [self.userIconImgView addGestureRecognizer:tap];
    
    //用户昵称
    self.userNameLabel = [[UILabel alloc] init];
    [userBgView addSubview:self.userNameLabel];
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(userbgImg.mas_bottom).offset(10);
        make.left.right.equalTo(userBgView);
        make.bottom.equalTo(userBgView);
    }];
    _userNameLabel.textColor = [UIColor colorWithRGB:0xF86D6D];
    _userNameLabel.font = [UIFont systemFontOfSize:15];
    _userNameLabel.textAlignment = NSTextAlignmentCenter;
    _userNameLabel.text = @"用户昵称";
    
    
    //切换面膜
    self.resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.resetBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.resetBtn setTitleColor:[UIColor colorWithRGB:0xF86D6D] forState:UIControlStateNormal];
    [self.resetBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    [self.resetBtn setTitle:@" 切换面膜" forState:UIControlStateNormal];
    [self.resetBtn setTitle:@" 切换卫生巾" forState:UIControlStateSelected];
    [self.resetBtn setImage:[UIImage imageNamed:@"girl_change_infor"] forState:UIControlStateNormal];
    [self.resetBtn setImage:[UIImage imageNamed:@"girl_change_infor"] forState:UIControlStateHighlighted];
    [self.resetBtn addTarget:self action:@selector(resetBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerBgView addSubview:self.resetBtn];
    [self.resetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headerBgView).offset(-11);
        make.top.equalTo(headerBgView).offset(84);
    }];
    
    //点击签到
    self.signBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.signBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.signBtn setTitleColor:[UIColor colorWithRGB:0xF86D6D] forState:UIControlStateNormal];
    [self.signBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    [self.signBtn setTitle:@" 点击签到" forState:UIControlStateNormal];
    [self.signBtn setImage:[UIImage imageNamed:@"girl_sign_icon"] forState:UIControlStateNormal];
    [self.signBtn setImage:[UIImage imageNamed:@"girl_sign_icon"] forState:UIControlStateHighlighted];
    [self.signBtn addTarget:self action:@selector(signBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerBgView addSubview:self.signBtn];
    [self.signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headerBgView).offset(-11);
        make.top.equalTo(self.resetBtn).offset(22);
    }];
    
    
    //添加返回按钮
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [headerBgView addSubview:btn];
    [btn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerBgView).offset(10);
        make.top.equalTo(headerBgView).offset(StatusBarHeight + 5);
        make.width.height.mas_equalTo(35);
    }];
    
    UIImageView *titleImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_home_title_icon"]];
    [headerBgView addSubview:titleImgView];
    [titleImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerBgView).offset(StatusBarHeight + 5);
        make.centerX.equalTo(headerBgView);
    }];
    return headerBgView;
}

/**
 创建中间tab按钮栏的背景View
 @param headerView 顶部VIew
 @return 中间的View
 */
- (UIView *)createMidTabView:(UIView *)headerView{
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerView.mas_bottom).offset(-20);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.mas_equalTo(110);
    }];
    
    
    //背景图
    UIImageView *innerImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_tab_bg"]];;
    [bgView addSubview:innerImgView];
    [innerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(bgView).offset(0);
        make.bottom.equalTo(bgView);
    }];
    
    NSArray *tabs = [self getMidTabInfor];
    UIView *lastView = nil;
    UIButton *lastBtn = nil;
    int tag = 0;
    for (NSDictionary *dict in tabs) {
        UIView *gapView = [[UIView alloc] init];
        [bgView addSubview:gapView];
        [gapView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lastBtn == nil ? bgView : lastBtn.mas_right);
            make.top.bottom.equalTo(bgView);
            if (lastView) {
                make.width.equalTo(lastView.mas_width);
            }
        }];
        lastView = gapView;
        
        //tab 按钮
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [bgView addSubview:btn];
        btn.tag = TabBaseBtnTag + tag;
        [btn addTarget:self action:@selector(midTabBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lastView.mas_right);
            make.top.bottom.equalTo(bgView);
            make.width.mas_equalTo(100);
            if (lastBtn) {
                make.width.equalTo(lastBtn);
            }
        }];
        lastBtn = btn;
        
        
        //tab 按钮上面的图片
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:dict[@"img"]]];
        [btn addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(btn);
            make.top.equalTo(bgView).offset(15);
            make.width.height.mas_equalTo(27);
        }];
        
        //tab 按钮上面的文字
        UILabel *label = [[UILabel alloc] init];
        label.textColor = [UIColor colorWithRGB:0x333333];
        label.font = [UIFont systemFontOfSize:15];
        label.text = dict[@"title"];
        [btn addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(btn);
            make.top.equalTo(imgView.mas_bottom).offset(6);
        }];
        
        tag++;
    }
    //最后一个gap
    UIView *gapView = [[UIView alloc] init];
    [bgView addSubview:gapView];
    [gapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lastBtn.mas_right);
        make.top.bottom.equalTo(bgView);
        make.width.equalTo(lastView);
        make.right.equalTo(bgView);
    }];
    
    //底部指示线条
    UIImageView *btmLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    btmLine.backgroundColor = [UIColor colorWithRGB:0xFF99C7];
    [bgView addSubview:btmLine];
    self.midIndicatorLine = btmLine;
    [self resetTabIndicatorLine:0];

    return bgView;
}
- (NSArray *)getMidTabInfor{
    return @[
             @{
                 @"title":@"我的部下",
                 @"img":@"tab_low_level_icon"
                 },
             @{
                 @"title":@"女王榜",
                 @"img":@"tab_nv_wang_icon"
                 },
             @{
                 @"title":@"私密领地",
                 @"img":@"tab_secret_icon"
                 }
             ];
}

/**
 创建主体切换的视图容器
 
 @param tabBgView 中间视图
 */
- (void)createBtmContainer:(UIView *)tabBgView{
    UIView *container = [[UIView alloc] init];
    [self.view addSubview:container];
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(tabBgView.mas_bottom);
    }];
    self.bodyContainer = container;
}
#pragma mark - 弹出选择性别弹框
- (void)showSelectSexAlertView{
    CCSelectSexAlert *alert = [[CCSelectSexAlert alloc] init];
//    [alert addTarget:self action:@selector(sexSelectBgClick) forControlEvents:UIControlEventTouchUpInside];
    alert.delegate = self;
    UIWindow *win = [[[UIApplication sharedApplication] delegate] window];
    [win addSubview:alert];
    [alert mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(win);
    }];
    self.sexSelectView = alert;
}
- (void)selectBoySex{
    __block typeof(self) this = self;
    [[CCGirlHttpManager share] setGirlUserSex:USERID gender:@"1" succeedBd:^(CCNetBaseModel * _Nonnull baseModel, CCGirlUserModel * _Nullable userModel) {
        if (baseModel.error == 0) {
            [this.sexSelectView removeFromSuperview];
            [this.navigationController popViewControllerAnimated:YES];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}
- (void)selectGirlSex{
    __block typeof(self) this = self;
    [[CCGirlHttpManager share] setGirlUserSex:USERID gender:@"2" succeedBd:^(CCNetBaseModel * _Nonnull baseModel, CCGirlUserModel * _Nullable userModel) {
        if (baseModel.error == 0) {
            [this.sexSelectView removeFromSuperview];
            //获取用户参与信息
            [this getGirlUserData];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}
#pragma mark - 获取用户的信息
- (void)getGirlUserData{
    __block typeof(self) this = self;
    [[CCGirlHttpManager share] getGirlUserInfor:USERID succeedBd:^(CCNetBaseModel * _Nonnull baseModel, CCGirlUserModel * _Nullable userModel) {
        if (baseModel.error == 0) {
            if (userModel.reader_gender != 1 && userModel.reader_gender != 2) {
                [this showSelectSexAlertView];
            }else{
                [CCGirlUserModel setShareData:userModel];
                [this updateUIAndData];
            }
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [this.navigationController popViewControllerAnimated:YES];
    }];
}
- (void)updateUIAndData{
    //页面UI
    CCGirlUserModel *user = [CCGirlUserModel share];
    NSString *imgName = [NSString stringWithFormat:@"girl_level_%d",user.currentLevel];
    self.userBgImgView.image = [UIImage imageNamed:imgName];
    
    self.userNameLabel.text = [NSString stringWithFormat:@"%@  %@",[UserManager sharedInstance].realname,[CCGirlUserModel getIdentityName:user.currentLevel gender:user.reader_gender]];
    [self.userIconImgView setImageURL:[NSURL URLWithString: [[UserManager sharedInstance] portrait]]];
    
    //加载第一个视图
    for (int i = 0; i < 3; i++) {
        UIButton *tabBtn = [self.view viewWithTag:TabBaseBtnTag + i];
        if (tabBtn.selected) {
            //底部容器联动
            [self resetContainerView:i];
            break;
        }
    }
}
#pragma mark - 主体视图切换
- (void)resetContainerView:(NSInteger)tab{
    [self.bodyContainer removeAllSubviews];
    switch (tab) {
        case 0:{
            [self.bodyContainer addSubview:self.lowLevelView];
            [self.lowLevelView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.bottom.equalTo(self.bodyContainer);
            }];
            [self.lowLevelView viewApper];
        }
            break;
        case 1:{
            [self.bodyContainer addSubview:self.queenListView];
            [self.queenListView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.bottom.equalTo(self.bodyContainer);
            }];
            [self.queenListView viewApper];
        }
            break;
        case 2:{
            [self.bodyContainer addSubview:self.secretAreaView];
            [self.secretAreaView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.bottom.equalTo(self.bodyContainer);
            }];
            [self.secretAreaView viewApper];
        }
            break;
        default:
            break;
    }
}
#pragma mark - 我的部下代理
- (void)getLowLevelUsers:(int)page callBack:(void(^)(BOOL isOk,int page,NSArray<CCGirlLowLevelUserModel *> *models))resultBd{
    NSString *flag = [NSString stringWithFormat:@"%d",[[CCGirlUserModel share] currentType]];
    [[CCGirlHttpManager share] getUserLowLeveList:USERID product_flag:flag succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray<CCGirlLowLevelUserModel *> * userList) {
       resultBd(baseModel.error == 0,page,userList);
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];

}
- (void)lowLevelUsersCellClick:(NSIndexPath *)index model:(CCGirlLowLevelUserModel *)model{
    

}
#pragma mark - 女王榜代理
- (void)getQueenUsers:(int)page callBack:(void(^)(BOOL isOk,int page,CCQueenListModel *models))resultBd{
    NSString *flag = [NSString stringWithFormat:@"%d",[[CCGirlUserModel share] currentType]];
    [[CCGirlHttpManager share] getQueenList:USERID product_flag:flag succeedBd:^(CCNetBaseModel * _Nonnull baseModel, CCQueenListModel * userList) {
        resultBd(baseModel.error == 0,page,userList);
        if(baseModel.error != 0){
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}
- (void)queenUsersCellClick:(NSIndexPath *)index model:(CCQueenModel *)model{
    
}
#pragma mark - 私密领地代理回调
- (void)secretAreaCellClick:(NSIndexPath *)index model:(CCSecretAreaModel *)model{
    switch (index.row) {
        case 0:{//选材任贤
//            CCGirlWelfareHistory *vc = [[CCGirlWelfareHistory alloc] init];
//            [self.navigationController pushViewController:vc animated:YES];
            [self showShareView];
        }
            break;
        case 1:{//加官进禄
            CCGirlKingUpgradeController *vc = [[CCGirlKingUpgradeController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:{//领取福利
            CCGetGiftListController *vc = [[CCGetGiftListController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:{//库存
            CCGirlKuCunController *vc = [[CCGirlKuCunController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
        case 4:{//库存
            CCBaseWebviewController *vc = [[CCBaseWebviewController alloc] init];
            vc.urlStr = [NSString stringWithFormat:@"http://www.cct369.com/village/public/kof/mineInferior/%@",USERID];
            vc.titleStr = @"月光宝盒";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark - 显示分享
- (void)showShareView {
    if(self.shareView == nil){
        _shareView = [CCShareView instanceView];
        _shareView.frame =  CGRectMake(0, App_Height, App_Width, 180);
        [_shareView.weixinBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.pengyouquanBtn addTarget:self action:@selector(weixinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.addressBookBtn addTarget:self action:@selector(addressBookBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_shareView.cancelBtn addTarget:self action:@selector(shareCancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_shareView];
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height - self.shareView.height;
    }];
}
- (void)shareCancelBtnClick {
    [UIView animateWithDuration:0.5 animations:^{
        self.shareView.y = App_Height;
    }];
}
- (void)addressBookBtnClick {
    CNContactPickerViewController * contactVc = [CNContactPickerViewController new];
    contactVc.delegate = self;
    [self presentViewController:contactVc animated:YES completion:nil];
    [self shareCancelBtnClick];
}
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    CNContact *contact = contactProperty.contact;
    NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
    CNPhoneNumber *phoneValue= contactProperty.value;
    NSString *phoneNumber = phoneValue.stringValue;
    NSLog(@"%@--%@",name, phoneNumber);
    [picker dismissViewControllerAnimated:YES completion:^{
        [self sendMessageToContacts:@[phoneNumber]];
    }];
}

- (void)sendMessageToContacts:(NSArray *)array {
    // 设置短信内容
    NSString *url = GirlShareUrl;
    NSString *shareText = [[NSString stringWithFormat:@"%@,%@", [self getInviteTextTitle],[self getInviteTextDesc]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (array.count > 0) {
        NSString *phoneStr = array[0];//发短信的号码
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@&body=%@%@", phoneStr,shareText, url];
        NSURL *openUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:openUrl];
    }
}

- (NSString *)getInviteTextTitle {
    NSString *text = @"女儿国邀你做女王";
    return text;
}
- (NSString *)getInviteTextDesc {
    NSString *text = @"女王可终身免费领取面膜、卫生巾，享受至尊荣耀！";
    return text;
}
- (void)weixinBtnClick:(UIButton *)sender {
    [self shareCancelBtnClick];
    NSInteger tag = sender.tag;
    UMSocialPlatformType  platform = tag == 10 ? UMSocialPlatformType_WechatSession : UMSocialPlatformType_WechatTimeLine;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    NSString *title = [self getInviteTextTitle];
    NSString *desc = [self getInviteTextDesc];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: title  descr:desc thumImage:IMAGENAMED(@"girl_nvwang_top_bg")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:GirlShareUrl];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}
#pragma mark - 中间tab按钮点击事件
- (void)midTabBtnClick:(UIButton *)btn{
    for (int i = 0; i < 3; i++) {
        UIButton *tabBtn = [self.view viewWithTag:TabBaseBtnTag + i];
        if (tabBtn != btn) {
            tabBtn.selected = false;
        }else{
            tabBtn.selected = true;
        }
    }
    
    //tab的指示线联动
    [self resetTabIndicatorLine:btn.tag - TabBaseBtnTag];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    //底部容器联动
    [self resetContainerView:btn.tag - TabBaseBtnTag];
}
- (void)resetTabIndicatorLine:(NSInteger)postion{
    //设置滑块位置
    [self.midIndicatorLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo([self.view viewWithTag:TabBaseBtnTag + postion].mas_centerX);
        make.bottom.equalTo(self.midIndicatorLine.superview).offset(-18);
        make.width.mas_equalTo(96);
        make.height.mas_equalTo(3);
    }];
    
}
#pragma mark - 切换按钮点击
- (void)resetBtnClick:(UIButton *)btn{
    btn.selected = !btn.isSelected;
    [[CCGirlUserModel share]setCurrentType:btn.selected ? 2 : 1];
    [self updateUIAndData];
}
#pragma mark - 签到按钮点击
- (void)signBtnClick:(UIButton *)btn{
    [[CCGirlHttpManager share] girlUserSign:USERID succeedBd:^(CCNetBaseModel * _Nonnull baseModel) {
        if (baseModel.error == 0) {
            self.signOkView.hidden = false;
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}
- (void)hideSignOkView{
    self.signOkView.hidden = true;
}
#pragma mark - 修改头像
- (void)showActionSheet {
    AccountViewController *vc = [[AccountViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - 懒加载
- (CCGirlLowLevelListView *)lowLevelView{
    if (!_lowLevelView) {
        _lowLevelView = [[CCGirlLowLevelListView alloc] init];
        _lowLevelView.delegate = self;
    }
    return _lowLevelView;
}
- (CCQueenListView *)queenListView{
    if (!_queenListView) {
        _queenListView = [[CCQueenListView alloc] init];
        _queenListView.delegate = self;
    }
    return _queenListView;
}
- (CCSecretAreaView *)secretAreaView{
    if (!_secretAreaView) {
        _secretAreaView = [[CCSecretAreaView alloc] init];
        _secretAreaView.delegate = self;
    }
    return _secretAreaView;
}
- (UIButton *)signOkView{
    if(_signOkView == nil){
        UIButton *view = [[UIButton alloc] init];
        [view addTarget:self action:@selector(hideSignOkView) forControlEvents:UIControlEventTouchUpInside];
        view.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.5];
        [self.view addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.equalTo(self.view);
        }];
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_sign_count_bg"]];
        [view addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.top.equalTo(view).offset(150);
        }];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"5克";
        titleLabel.font = [UIFont systemFontOfSize:15];
        titleLabel.textColor = [UIColor colorWithRGB:0xFE76B4];
        [view addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(imgView);
        }];
        self.signGetCountLabel = titleLabel;
    
        UIImageView *textImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_sign_ok_text_img"]];
        textImg.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:textImg];
        [textImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.height.mas_equalTo(16);
            make.top.equalTo(imgView.mas_bottom).offset(25);
        }];
        
        _signOkView = view;
    }
    return _signOkView;
}
@end
