//
//  CCAddressEditVc.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/10/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGrilBaseVc.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^GirlAddressBD)(CCAddressModel *model);

@interface CCGirlAddressEditVc : CCGrilBaseVc
@property (nonatomic, strong) GirlAddressBD selectAddressBlock;
@end

NS_ASSUME_NONNULL_END
