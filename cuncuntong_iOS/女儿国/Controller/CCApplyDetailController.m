//
//  CCApplyDetailController.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCApplyDetailController.h"

@interface CCApplyDetailController ()
@property (nonatomic,retain)UILabel *userNameLabel;//用户名字
@property (nonatomic,retain)UILabel *userPhoneLabel;//用户手机号
@property (nonatomic,retain)UILabel *userAddressLabel;//地址
@property (nonatomic,retain)UIImageView *goodsIconImgView;//商品图片
@property (nonatomic,retain)UILabel *goodsNameLabel;//商品名字
@property (nonatomic,retain)UILabel *goodsCountLabel;//商品数量
@property (nonatomic,retain)UILabel *postPriceLabel;//邮费
@property (nonatomic,retain)UILabel *totalMoneyLabel;//总价
@property (nonatomic,retain)UILabel *totalCountLabel;//总数量
@property (nonatomic,retain)UILabel *orderNumLabel;//订单编号
@property (nonatomic,retain)UILabel *createTimeLabel;//创建时间
@property (nonatomic,retain)UILabel *payTimeLabel;//支付时间



@property (nonatomic,retain)CCWelfareModel *bodyModel;
@end

@implementation CCApplyDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *topView = [self createNv];
    topView = [self createReceiveUserView:topView];
    topView = [self createGoodsView:topView];
    topView = [self createOrderInforView:topView];
    [self createLogisticsView:topView];
    
    [self loadOrderInfor];
}
#pragma mark - 初始化视图

/**
 导航栏
 */
- (UIView *)createNv{
    CCNvbBgView *header = [[CCNvbBgView alloc] initWithParentView:self.view delegate:self];
    header.backgroundColor = [UIColor colorWithRGB:0xFDD1E5];
    [header setRetunBtnImg:@"return_bg_yellow_icon"];
    [header setNvTitle:@"领取详情" color:[UIColor colorWithRGB:0xF86D6D] size:18];
    return header;
}

/**
 收件人信息
 */
- (UIView *)createReceiveUserView:(UIView *)topView{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 6;
    view.clipsToBounds = YES;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(77);
    }];
    
    
    //收件人
    UILabel *recieverNameLabel = [[UILabel alloc] initWithText:@"张丽丽" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:recieverNameLabel];
    [recieverNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(11);
        make.top.equalTo(view).offset(18);
    }];
    self.userNameLabel = recieverNameLabel;
    
    //电话
    UILabel *phoneNumLabel = [[UILabel alloc] initWithText:@"18501983195" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:phoneNumLabel];
    [phoneNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(recieverNameLabel.mas_right).offset(14);
        make.top.equalTo(recieverNameLabel.mas_top);
    }];
    self.userPhoneLabel = phoneNumLabel;
    
    //地址
    UILabel *addressLabel = [[UILabel alloc] initWithText:@"北京市朝阳区龙子街富丽花园 25-15-2" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:addressLabel];
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(recieverNameLabel.mas_left);
        make.bottom.equalTo(view).offset(-18);
    }];
    self.userAddressLabel = addressLabel;

    return view;
}

/**
 产品展示
 */
- (UIView *)createGoodsView:(UIView *)topView{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 6;
    view.clipsToBounds = YES;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
    }];
    
    
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.backgroundColor = [UIColor clearColor];
    [view addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(10);
        make.top.equalTo(view).offset(16);
        make.width.mas_equalTo(139);
        make.height.mas_equalTo(83);
    }];
    self.goodsIconImgView = imgView;
    
    UILabel *goodsNameLabel = [[UILabel alloc] initWithText:@"福利卫生巾" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:goodsNameLabel];
    [goodsNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_right).offset(11);
        make.top.equalTo(imgView.mas_top).offset(0);
        make.right.equalTo(view).offset(-10);
    }];
    self.goodsNameLabel = goodsNameLabel;
    
    UILabel *goodsCountLabel = [[UILabel alloc] initWithText:@"x2" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:goodsCountLabel];
    [goodsCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-11);
        make.bottom.equalTo(imgView.mas_bottom).offset(-16);
    }];
    self.goodsCountLabel = goodsCountLabel;
    
    
    UILabel *youLabel = [[UILabel alloc] initWithText:@"邮寄费用" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:youLabel];
    [youLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_left).offset(0);
        make.top.equalTo(imgView.mas_bottom).offset(14);
    }];
    
    
    UILabel *youFeiLabel = [[UILabel alloc] initWithText:@"￥10 " font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:youFeiLabel];
    [youFeiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-11);
        make.top.equalTo(youLabel.mas_top);
    }];
    self.postPriceLabel = youFeiLabel;
    
    UIView *gapLine = [[UIView alloc] init];
    gapLine.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
    [view addSubview:gapLine];
    [gapLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_left);
        make.right.equalTo(goodsCountLabel.mas_right);
        make.top.equalTo(youLabel.mas_bottom).offset(15);
    }];
    
    
    UILabel *sumFeiLabel = [[UILabel alloc] initWithText:@"共  2 件  小计  ￥10" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:sumFeiLabel];
    [sumFeiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-11);
        make.top.equalTo(gapLine.mas_bottom).offset(15);
        make.bottom.equalTo(view).offset(-15);
    }];
    self.totalMoneyLabel = sumFeiLabel;
    
    return view;
}
/**
 订单信息
 */
- (UIView *)createOrderInforView:(UIView *)topView{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 6;
    view.clipsToBounds = YES;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
    }];
    UILabel *titleLabel = [[UILabel alloc] initWithText:@"订单信息" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(11);
        make.top.equalTo(view).offset(14);
    }];
    
    
    
    
    UILabel *titleLabel1 = [[UILabel alloc] initWithText:@"订单编号" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:titleLabel1];
    [titleLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleLabel.mas_bottom).offset(20);
    }];
    UILabel *orderNumLabel = [[UILabel alloc] initWithText:@"665545112144" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:orderNumLabel];
    [orderNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel1);
        make.right.equalTo(view).offset(-12);
    }];
    self.orderNumLabel = orderNumLabel;
    
    
    UILabel *titleLabel2 = [[UILabel alloc] initWithText:@"创建时间" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:titleLabel2];
    [titleLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleLabel1.mas_bottom).offset(15);
    }];
    UILabel *orderTimeLabel = [[UILabel alloc] initWithText:@"2019-08-19  13:45" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:orderTimeLabel];
    [orderTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel2);
        make.right.equalTo(view).offset(-12);
    }];
    self.createTimeLabel = orderTimeLabel;
    
    
    UILabel *titleLabel3 = [[UILabel alloc] initWithText:@"付款时间" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:titleLabel3];
    [titleLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleLabel2.mas_bottom).offset(15);
    }];
    UILabel *payTimeLabel = [[UILabel alloc] initWithText:@"2019-08-19  13:45" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:payTimeLabel];
    [payTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel3);
        make.right.equalTo(view).offset(-12);
        make.bottom.equalTo(view).offset(-16);
    }];
    self.payTimeLabel = payTimeLabel;
    return view;
}
/**
 查看物流
 */
- (UIView *)createLogisticsView:(UIView *)topView{
    UIButton *view = [[UIButton alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 6;
    view.clipsToBounds = YES;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(45);
    }];
    [view addTarget:self action:@selector(goKuiDiDetail) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithText:@"查看物流信息" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(11);
        make.centerY.equalTo(view);
    }];
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_right_arrow_icon"]];
    [view addSubview:arrow];
    [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-12);
        make.centerY.equalTo(view);
        make.width.mas_equalTo(9);
        make.height.mas_equalTo(16);
    }];
    return view;
}
#pragma mark - 跳转快递详情页
- (void)goKuiDiDetail{
    CCBaseWebviewController *vc = [CCBaseWebviewController new];
    [vc setHiddenShare:YES];
    vc.urlStr = [NSString stringWithFormat:@"https://m.kuaidi100.com/result.jsp?nu=%d",self.bodyModel.order_number];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 数据加载完成后更新UI
- (void)resetSubViewUI{
    self.userNameLabel.text = self.bodyModel.reader_name;
    self.userPhoneLabel.text = self.bodyModel.reader_phone;
    self.userAddressLabel.text = self.bodyModel.reader_address;
    
    [self.goodsIconImgView setImageURL:[NSURL URLWithString:self.bodyModel.image]];
    self.goodsNameLabel.text = self.bodyModel.name;
    self.goodsCountLabel.text = [NSString stringWithFormat:@"x%d",self.bodyModel.product_num];
    self.postPriceLabel.text = [NSString stringWithFormat:@"￥%@",self.bodyModel.postage_price];
    self.totalMoneyLabel.text = [NSString stringWithFormat:@"共 %d 件 小计  ￥%@",self.bodyModel.order_number,self.bodyModel.total_price];
    self.orderNumLabel.text = [NSString stringWithFormat:@"%d",self.bodyModel.order_number];
    self.createTimeLabel.text = self.bodyModel.create_time;
    self.payTimeLabel.text = self.bodyModel.receive_welfare_time;
}
#pragma mark - 网络请求
- (void)loadOrderInfor{
    [[CCGirlHttpManager share] getOrderDetail:USERID product_id:self.order_id succeedBd:^(CCNetBaseModel * _Nonnull baseModel, id _Nullable model) {
        if (baseModel.error == 0) {
            self.bodyModel = model;
            [self resetSubViewUI];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络请求失败"];
    }];
}
@end
