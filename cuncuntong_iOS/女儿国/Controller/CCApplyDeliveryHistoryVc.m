//
//  CCApplyDeliveryHistoryVc.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCApplyDeliveryHistoryVc.h"
#import "CCWelfareCell.h"
#import "CCApplyDetailController.h"
@interface CCApplyDeliveryHistoryVc ()
<UITableViewDelegate,UITableViewDataSource,CCNvbBgViewDelegate>
@property (nonatomic,retain)UITableView *bodyListView;//列表

@property (nonatomic,retain)NSMutableArray<CCWelfareModel *> *bodyModels;
@end

@implementation CCApplyDeliveryHistoryVc

- (void)viewWillAppear:(BOOL)Animation{
    [super viewWillAppear:Animation];
    self.navigationController.navigationBarHidden = true;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createBodyUI];
    
    [self loadNetData:1];
}

#pragma mark - 初始化UI视图
- (void)createBodyUI{
    CCNvbBgView *header = [[CCNvbBgView alloc] initWithParentView:self.view delegate:self];
    header.backgroundColor = [UIColor colorWithRGB:0xFDD1E5];
    [header setRetunBtnImg:@"return_bg_yellow_icon"];
    [header setNvTitle:@"发货记录" color:[UIColor colorWithRGB:0xF86D6D] size:18];
    
    
    //主体列表
    UITableView *listView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    listView.showsVerticalScrollIndicator = false;
    listView.separatorStyle = UITableViewCellSeparatorStyleNone;
    listView.dataSource = self;
    listView.delegate = self;
    [listView registerClass:[CCWelfareCell class] forCellReuseIdentifier:NSStringFromClass([CCWelfareCell class])];
    
    //    __weak typeof(self) this = self;
    //    listView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
    //
    //    }];
    //    listView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
    //
    //    }];
    
    
    [self.view addSubview:listView];
    [listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(header.mas_bottom);
    }];
    
    self.bodyListView = listView;
}
#pragma mark - 加载列表数据
- (void)loadNetData:(NSInteger) page{
    NSString *flag = [NSString stringWithFormat:@"%d",[[CCGirlUserModel share] currentType]];
    [[CCGirlHttpManager share] alreadyDelivery:USERID product_flag:flag succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray<CCWelfareModel *> * _Nullable modeList) {
        if (baseModel.error == 0) {
            if(page == 0){
                [self.bodyModels removeAllObjects];
            }
            [self.bodyModels addObjectsFromArray:modeList];
            [self.bodyListView reloadData];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误!"];
    }];
}
#pragma mark - 列表代理事件
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bodyModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CCWelfareCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CCWelfareCell class])];
    CCWelfareModel *model = self.bodyModels[indexPath.row];
    [cell setCellModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CCWelfareModel *model = self.bodyModels[indexPath.row];
    CCApplyDetailController *vc = [[CCApplyDetailController alloc] init];
    vc.order_id = model.order_id;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -懒加载
- (NSMutableArray<CCWelfareModel *> *)bodyModels{
    if (_bodyModels == nil) {
        _bodyModels = [NSMutableArray new];
    }
    return _bodyModels;
}
@end
