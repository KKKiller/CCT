//
//  CCSelectAreaVc.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/10/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCSelectAreaVc.h"
#import "CCSelectAreaCell.h"
@interface CCSelectAreaVc ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *dataArray;
@property (nonatomic,retain)NSMutableArray *selectHistory;
@property (nonatomic,assign)BOOL isLoadNetList;
@end
@implementation CCSelectAreaVc
- (void)viewDidLoad{
    [super viewDidLoad];
    [self initSubUI];
    [self loadList];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
- (void)initSubUI{
    self.title = @"地址";
    CCNvbBgView *header = [[CCNvbBgView alloc] initWithParentView:self.view delegate:self];
    header.backgroundColor = [UIColor colorWithRGB:0xFDD1E5];
    [header setRetunBtnImg:@"return_bg_yellow_icon"];
    [header setNvTitle:@"地址" color:[UIColor colorWithRGB:0xF86D6D] size:18];
    
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.top.equalTo(header.mas_bottom);
    }];
}
- (void)loadList{
    
    NSString *id_ = @"0";
    if (self.selectHistory.count > 0) {
        id_ = [NSString stringWithFormat:@"%d",[self.selectHistory.lastObject[@"id"] intValue]];
    }
    self.isLoadNetList = YES;
    [[CCGirlHttpManager share] getSelectAreaList:id_ succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray * _Nullable areaList) {
        self.isLoadNetList = NO;
        if (baseModel.status == 9000) {
            if (areaList.count == 0) {
                //说明到最后一级了
                if (self.resultBd) {
                    NSMutableString *result = [NSMutableString new];
                    for (int i = 0; i < self.selectHistory.count; i++) {
                        [result appendString:self.selectHistory[i][@"name"]];
                    }
                    self.resultBd([NSString stringWithFormat:@"%d",[self.selectHistory.lastObject[@"id"] intValue]], result);
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }else{
                self.dataArray = areaList;
                [self.tableView reloadData];
            }
        }else{
            [CCToastView showText:baseModel.msg];
        }
        
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误！"];
        self.isLoadNetList = NO;
    }];
}
- (void)backMove{
    if (self.selectHistory.count > 0) {
        [self.selectHistory removeLastObject];
        [self loadList];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark - 数据源方法
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, [self tableView:tableView heightForHeaderInSection:section])];
    view.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
    UILabel *label = [[UILabel alloc] init];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(20);
        make.right.equalTo(view).offset(-20);
        make.top.bottom.equalTo(view);
    }];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor colorWithRGB:0x666666];
    NSMutableString *result = [NSMutableString stringWithFormat:@"地址："];
    for (int i = 0; i < self.selectHistory.count; i++) {
        [result appendString:self.selectHistory[i][@"name"]];
    }
    label.text = result;
    return view;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCSelectAreaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CCSelectAreaCell"];
    NSDictionary *dict = self.dataArray[indexPath.row];
    cell.nameLabel.text = dict[@"name"];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isLoadNetList) {
        return;
    }
    NSDictionary *dict = self.dataArray[indexPath.row];
    [self.selectHistory addObject:dict];
    [self loadList];
}
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _tableView.rowHeight = 50;
        _tableView.backgroundColor = BACKGRAY;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"CCSelectAreaCell" bundle:nil] forCellReuseIdentifier:@"CCSelectAreaCell"];
    }
    return _tableView;
}
- (NSMutableArray *)selectHistory{
    if (!_selectHistory) {
        _selectHistory = [NSMutableArray new];
    }
    return _selectHistory;
}
@end
