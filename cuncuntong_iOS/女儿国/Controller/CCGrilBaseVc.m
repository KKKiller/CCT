//
//  CCGrilBaseVc.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGrilBaseVc.h"

@interface CCGrilBaseVc ()

@end

@implementation CCGrilBaseVc

- (void)viewWillAppear:(BOOL)Animation{
    [super viewWillAppear:Animation];
    self.navigationController.navigationBarHidden = true;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRGB:0xeeeeee];

    // Do any additional setup after loading the view.
}
@end
