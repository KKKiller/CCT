//
//  CCGetGiftListController.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGetGiftListController.h"
#import "CCGirlWelfareHistory.h"
#import "CCBuyDetailController.h"
#define MID_BUY_BTN_BASE_TAG 2091 // 中间购买按钮tag的基数
#define MID_GET_GIFT_BTN_BASE_TAG 3091 // 中间领取按钮tag的基数
@interface CCGetGiftListController ()
@property (nonatomic,retain)UIView *midBgView;//中间产品列表的背景view

@property (nonatomic,retain)NSArray<CCGirlUpgradeGoodsModel *> *bodyModelList;//产品列表数据

@property (nonatomic,strong)UIButton *signOkView;//签到成功弹框
@property (nonatomic,strong)UILabel *signGetCountLabel;//签到获得数量Label
@end

@implementation CCGetGiftListController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self loadNetGoodsList];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    
    //返回按钮
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:btn];
    [btn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"return_bg_yellow_icon"] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(backMove) forControlEvents:UIControlEventTouchUpInside];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(10);
        make.top.equalTo(self.view).offset(StatusBarHeight + 5);
        make.width.height.mas_equalTo(35);
    }];
    
    
}
#pragma mark - 初始化视图
- (void)createUI{
    self.view.backgroundColor = [UIColor colorWithRGB:0xFFF2FA];

    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = false;
    scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
   
    //去除scrollView极其子类的内边距
    if (@available(iOS 11.0, *)) {
        scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    
    UIView * contentView = [[UIView alloc] init];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(scrollView);
        make.width.equalTo(scrollView);
        make.height.greaterThanOrEqualTo(@0.f);//此处保证容器View高度的动态变化 大于等于0.f的高度
    }];
    
    UIImageView *topImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_get_gift_top_bg"]];
    [contentView addSubview:topImg];
    [topImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(contentView);
        make.height.mas_equalTo(248 * kScreenWidth / 375);
        make.width.mas_equalTo(kScreenWidth);
        make.width.equalTo(contentView);
    }];
    
    
    
    
    
    
    /*
     中间产品列表的背景view
     */
    UIView *midBgView = [[UIView alloc] init];
    midBgView.backgroundColor = [UIColor colorWithRGB:0xf8f8f8];
    [contentView addSubview:midBgView];
    [midBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topImg.mas_bottom).offset(6);
        make.left.equalTo(contentView).offset(10);
        make.right.equalTo(contentView).offset(-10);
    }];
//    midBgView.backgroundColor = [UIColor whiteColor];
    midBgView.layer.cornerRadius = 6;
    midBgView.layer.shadowColor = [UIColor colorWithRGB:0xFF94B1].CGColor;
    midBgView.layer.shadowOffset = CGSizeMake(0,4);
    midBgView.layer.shadowOpacity = 1;
    midBgView.layer.shadowRadius = 4;
    
    
    
    UIView *goodBgView = [[UIView alloc] init];
    goodBgView.backgroundColor = [UIColor whiteColor];
    goodBgView.clipsToBounds = YES;
    [midBgView addSubview:goodBgView];
    [goodBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(midBgView);
        make.left.equalTo(midBgView);
        make.right.equalTo(midBgView);
        make.height.mas_equalTo(0);
    }];
    self.midBgView = goodBgView;

    
    
    //底部弹框
    UIView *alertView = [[UIView alloc] init];
    alertView.backgroundColor = [UIColor clearColor];
    [midBgView addSubview:alertView];
    [alertView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(goodBgView.mas_bottom);
        make.left.equalTo(midBgView);
        make.right.equalTo(midBgView);
        make.height.mas_equalTo(86);
        make.bottom.equalTo(midBgView);
    }];
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithRGB:0xFF5181];
    label1.font = [UIFont boldSystemFontOfSize:16];
    label1.text = @"每月签到3次以上才能领取";
    [alertView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(alertView);
        make.top.equalTo(alertView).offset(20);
    }];
    UILabel *label2 = [[UILabel alloc] init];
    label2.textColor = [UIColor colorWithRGB:0xAD6565];
    label2.font = [UIFont boldSystemFontOfSize:16];
    label2.text = @"每月只能领取其中的一件";
    [alertView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(alertView);
        make.top.equalTo(label1.mas_bottom).offset(3);
    }];
    
    
    //创建底部视图
    UIImage *signImg = [UIImage imageNamed:@"girl_up_nvwang_btn"];
   signImg =  [signImg resizableImageWithCapInsets:UIEdgeInsetsMake(0, 100, 0, 100) resizingMode:UIImageResizingModeStretch];
    UIButton *signBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [signBtn addTarget:self action:@selector(signBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [signBtn setTitle:@"立即签到" forState:UIControlStateNormal];
    [signBtn setBackgroundImage:signImg forState:UIControlStateNormal];
    [contentView addSubview:signBtn];
    [signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView).offset(24);
        make.right.equalTo(contentView).offset(-24);
        make.top.equalTo(midBgView.mas_bottom).offset(24);
        make.height.mas_equalTo(53);
    }];
    
    //创建底部视图
    UIButton *historyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [historyBtn addTarget:self action:@selector(historyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [historyBtn setTitle:@"已领取的福利" forState:UIControlStateNormal];
    [historyBtn setBackgroundImage:signImg forState:UIControlStateNormal];
    [contentView addSubview:historyBtn];
    [historyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView).offset(24);
        make.right.equalTo(contentView).offset(-24);
        make.top.equalTo(signBtn.mas_bottom).offset(24);
        make.height.mas_equalTo(53);
    }];
    
    
    //底部提示
    UIImage *img = [UIImage imageNamed:@"girl_get_gift_remark_bg"];
    img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(0, 100, 0, 100) resizingMode:UIImageResizingModeStretch];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [contentView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView);
        make.right.equalTo(contentView);
        make.top.mas_equalTo(historyBtn.mas_bottom).offset(25);
        make.height.mas_equalTo(103);
    }];
    
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfSize:19];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    [imgView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(imgView);
    }];
    NSString *testString = @"面膜女王  卫生巾女王\n双料女王可以领取其中的两件哦～";
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:testString];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //行间距
    [paragraphStyle setLineSpacing:5];
    //对齐方式
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [testString length])];
    label.attributedText = attributedString;
    
    
    
    //底部图片
    img = [UIImage imageNamed:@"girl_gift_img__"];
    img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(0, 100, 0, 100) resizingMode:UIImageResizingModeStretch];
    UIImageView *btmImgView = [[UIImageView alloc] initWithImage:img];
    [contentView addSubview:btmImgView];
    [btmImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgView.mas_bottom).offset(4);
        make.left.equalTo(contentView).offset(26);
        make.right.equalTo(contentView).offset(13);
        make.bottom.equalTo(contentView);
    }];
}

#pragma mark - 网络请求列表数据
- (void)loadNetGoodsList{
    [[CCGirlHttpManager share] getGirlWelfareGoodsList:USERID succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray<CCGirlUpgradeGoodsModel *> * goodsList) {
        if (baseModel.error == 0) {
            self.bodyModelList = goodsList;
            [self resetBodyGoodsListUI];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络请求失败！"];
    }];
}
- (void)resetBodyGoodsListUI{
    [self.midBgView removeAllSubviews];
    [self.midBgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0);
    }];
    
    
    for (int i = 0; i < self.bodyModelList.count; i++) {
        [self createMidItemView:i model:self.bodyModelList[i] isShowBtmLine:(i < self.bodyModelList.count - 1)];
    }
    if (self.bodyModelList.count > 0) {
        [self.midBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([[self.midBgView.subviews lastObject] bottom]);
        }];
    }
    
}

#pragma mark - 创建中间的item项
- (void)createMidItemView:(int)num model:(CCGirlUpgradeGoodsModel *)model isShowBtmLine:(BOOL)isShowLine{
    CGFloat itemHeight = 120;
    CGFloat parentLeftRightGap = 10;
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, num * itemHeight, kScreenWidth - parentLeftRightGap * 2, itemHeight)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.midBgView addSubview:bgView];
    
    //产品图片
    UIImageView *imgView = [[UIImageView alloc] init];
//WithFrame:CGRectMake(10, 15, 148, 95)];
    [bgView addSubview:imgView];
    [imgView setImageURL:[NSURL URLWithString:model.path_image]];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.left.equalTo(bgView).offset(10);
        make.width.mas_equalTo(148);
        make.height.mas_equalTo(95);
    }];
    
    UIButton *buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bgView addSubview:buyBtn];
    [buyBtn setBackgroundImage:[UIImage imageNamed:@"girl_get_gift_buy_btn"] forState:UIControlStateNormal];
    buyBtn.tag = num + MID_BUY_BTN_BASE_TAG;
    [buyBtn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.right.equalTo(bgView).offset(-10);
    }];
    
    UIButton *getBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bgView addSubview:getBtn];
    [getBtn setBackgroundImage:[UIImage imageNamed:@"girl_get_gift_btn_bg"] forState:UIControlStateNormal];
    getBtn.tag = num + MID_GET_GIFT_BTN_BASE_TAG;
    [getBtn addTarget:self action:@selector(getGiftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [getBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.right.equalTo(buyBtn.mas_left).offset(-5);
    }];
    
    if (isShowLine) {
        UIView *btmLine = [[UIView alloc] init];
        [bgView addSubview:btmLine];
        btmLine.backgroundColor = [UIColor colorWithRGB:0xEDEDED];
        [btmLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(imgView.mas_left);
            make.right.equalTo(buyBtn.mas_right);
            make.height.mas_equalTo(0.5);
            make.bottom.equalTo(bgView);
        }];
    }
}

#pragma mark - 按钮点击事件
/**
 点击购买商品,跳转购买页面

 @param btn
 */
- (void)buyBtnClick:(UIButton *)btn{
    NSInteger index = btn.tag - MID_BUY_BTN_BASE_TAG;
    CCGirlUpgradeGoodsModel *model = self.bodyModelList[index];
    
    CCWineModel *wineModel = [CCWineModel new];
    wineModel.id = [NSString stringWithFormat:@"%d",model.id_];
    wineModel.image = model.path_image;
    
//    CCBuyHomeModel *buyModel = [CCBuyHomeModel new];
//    buyModel.title = model.name;
//    buyModel.imgurl = @[model.path_image];
//    buyModel.id = [NSString stringWithFormat:@"%d",model.id_];
    
    
    CCBuyDetailController *vc = [[CCBuyDetailController alloc]init];
    vc.wineModel = wineModel;
//    vc.buyModel = buyModel;
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 中间领取按点击事件
 */
- (void)getGiftBtnClick:(UIButton *)btn{
    CCGirlUpgradeGoodsModel *model = self.bodyModelList[btn.tag - MID_GET_GIFT_BTN_BASE_TAG];
    [[CCGirlHttpManager share] getGift:USERID product_id:model.id_ succeedBd:^(CCNetBaseModel * _Nonnull baseModel, id _Nullable bean) {
        if (baseModel.error == 0) {
            [CCToastView showText:@"领取成功"];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}

/**
 签到按钮点击事件
 */
- (void)signBtnClick{
    [[CCGirlHttpManager share] girlUserSign:USERID succeedBd:^(CCNetBaseModel * _Nonnull baseModel) {
        if (baseModel.error == 0) {
            self.signOkView.hidden = false;
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}
- (void)hideSignOkView{
    self.signOkView.hidden = true;
}
- (void)historyBtnClick{
    CCGirlWelfareHistory *vc = [CCGirlWelfareHistory new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 懒加载
- (UIButton *)signOkView{
    if(_signOkView == nil){
        UIButton *view = [[UIButton alloc] init];
        [view addTarget:self action:@selector(hideSignOkView) forControlEvents:UIControlEventTouchUpInside];
        view.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.5];
        [self.view addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.equalTo(self.view);
        }];
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_sign_count_bg"]];
        [view addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.top.equalTo(view).offset(150);
        }];
        
        UILabel *label = [[UILabel alloc] init];
        label.textColor = [UIColor colorWithRGB:0xFE76B4];
        label.font = [UIFont systemFontOfSize:15];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(imgView);
        }];
        self.signGetCountLabel = label;
        
        UIImageView *textImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"girl_sign_ok_text_img"]];
        textImg.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:textImg];
        [textImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.height.mas_equalTo(16);
            make.top.equalTo(imgView.mas_bottom).offset(25);
        }];
        
        _signOkView = view;
    }
    return _signOkView;
}
@end
