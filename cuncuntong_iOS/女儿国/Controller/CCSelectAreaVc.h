//
//  CCSelectAreaVc.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/10/7.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGrilBaseVc.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SelectAreaBd)(NSString *village_id,NSString *areaName);
@interface CCSelectAreaVc : CCGrilBaseVc
@property (nonatomic,copy)SelectAreaBd resultBd;
@end

NS_ASSUME_NONNULL_END
