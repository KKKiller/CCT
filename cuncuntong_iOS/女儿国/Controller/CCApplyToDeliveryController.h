//
//  CCApplyToDeliveryController.h
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCGrilBaseVc.h"

NS_ASSUME_NONNULL_BEGIN

@interface CCApplyToDeliveryController : CCGrilBaseVc
@property (nonatomic,retain)CCKuCunModel *productData;
@end

NS_ASSUME_NONNULL_END
