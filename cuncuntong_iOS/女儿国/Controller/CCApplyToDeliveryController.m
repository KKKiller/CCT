//
//  CCApplyToDeliveryController.m
//  CunCunTong
//
//  Created by 彭涛 on 2019/9/9.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCApplyToDeliveryController.h"
#import "CCShareMyController.h"
#import "CCGirlAddressEditVc.h"
@interface CCApplyToDeliveryController ()<UITextFieldDelegate>
@property (nonatomic,retain)UILabel *userNameLabel;//用户名字
@property (nonatomic,retain)UILabel *userPhoneLabel;//用户手机号
@property (nonatomic,retain)UILabel *userAddressLabel;//地址
@property (nonatomic,retain)UIImageView *goodsIconImgView;//商品图片
@property (nonatomic,retain)UILabel *goodsNameLabel;//商品名字
@property (nonatomic,retain)UITextField *goodsCountTextFiled;//商品数量
@property (nonatomic,retain)UILabel *postPriceLabel;//邮费
@property (nonatomic,retain)UILabel *totalMoneyLabel;//总价
@property (nonatomic,retain)UILabel *totalCountLabel;//总数量

@property (nonatomic,retain)CCGirlApplyDeliveryModel *bodyModel;
@property (nonatomic,retain)CCAddressModel *addressModel;
@end

@implementation CCApplyToDeliveryController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *topView = [self createNv];
    topView = [self createReceiveUserView:topView];
    topView = [self createGoodsView:topView];
    [self createPostBtn:topView];
    
    
    [self getGoodsDetailInfor];
    [self loadNetAddressList];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}
#pragma mark - 初始化视图

/**
 导航栏
 */
- (UIView *)createNv{
    CCNvbBgView *header = [[CCNvbBgView alloc] initWithParentView:self.view delegate:self];
    header.backgroundColor = [UIColor colorWithRGB:0xFDD1E5];
    [header setRetunBtnImg:@"return_bg_yellow_icon"];
    [header setNvTitle:@"申请发货" color:[UIColor colorWithRGB:0xF86D6D] size:18];
    return header;
}

/**
 收件人信息
 */
- (UIView *)createReceiveUserView:(UIView *)topView{
    UIButton *view = [[UIButton alloc] init];
    [view addTarget:self action:@selector(chooseAddress) forControlEvents:UIControlEventTouchUpInside];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 6;
    view.clipsToBounds = YES;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(77);
    }];
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_go"]];
    img.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:img];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.right.equalTo(view).offset(-11);
        make.width.height.mas_equalTo(20);
    }];
    
    //收件人
    UILabel *recieverNameLabel = [[UILabel alloc] initWithText:@"请选择地址" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:recieverNameLabel];
    [recieverNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(11);
        make.top.equalTo(view).offset(18);
    }];
    self.userNameLabel = recieverNameLabel;

    
    //电话
    UILabel *phoneNumLabel = [[UILabel alloc] initWithText:@"" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:phoneNumLabel];
    [phoneNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(recieverNameLabel.mas_right).offset(0);
        make.top.equalTo(recieverNameLabel.mas_top);
    }];
    self.userPhoneLabel = phoneNumLabel;

    
    //地址
    UILabel *addressLabel = [[UILabel alloc] initWithText:@"" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    addressLabel.numberOfLines = 1;
    [view addSubview:addressLabel];
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(recieverNameLabel.mas_left);
        make.right.equalTo(img.mas_left).offset(-10);
        make.top.equalTo(phoneNumLabel.mas_bottom).offset(8);
    }];
    self.userAddressLabel = addressLabel;

    
    return view;
}

/**
 产品展示
 */
- (UIView *)createGoodsView:(UIView *)topView{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 6;
    view.clipsToBounds = YES;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
    }];
    
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.backgroundColor = [UIColor clearColor];
    [view addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(10);
        make.top.equalTo(view).offset(16);
        make.width.mas_equalTo(139);
        make.height.mas_equalTo(83);
    }];
    self.goodsIconImgView = imgView;

    UILabel *goodsNameLabel = [[UILabel alloc] initWithText:@"" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:goodsNameLabel];
    [goodsNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_right).offset(11);
        make.top.equalTo(imgView.mas_top).offset(0);
        make.right.equalTo(view).offset(-10);
    }];
    self.goodsNameLabel = goodsNameLabel;

    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addBtn.layer.cornerRadius = 12;
    addBtn.clipsToBounds = YES;
    addBtn.layer.borderWidth = 1;
    addBtn.layer.borderColor = [UIColor colorWithRGB:0xcccccc].CGColor;
    [addBtn setTitle:@"+" forState:UIControlStateNormal];
    [addBtn setTitleColor:[UIColor colorWithRGB:0x333333] forState:UIControlStateNormal];
    addBtn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [addBtn addTarget:self action:@selector(goodsCountAdd) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-11);
        make.bottom.equalTo(imgView.mas_bottom).offset(-16);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(24);
    }];

    UITextField *goodsCountTextFiled = [[UITextField alloc] init];
    goodsCountTextFiled.font = [UIFont systemFontOfSize:15];
    goodsCountTextFiled.textColor = [UIColor colorWithRGB:0x333333];
    goodsCountTextFiled.layer.cornerRadius = 3;
    goodsCountTextFiled.layer.borderColor = [UIColor colorWithRGB:0xeeeeee].CGColor;
    goodsCountTextFiled.textAlignment = NSTextAlignmentCenter;
    goodsCountTextFiled.keyboardType = UIKeyboardTypeNumberPad;
    goodsCountTextFiled.delegate = self;
    goodsCountTextFiled.layer.borderWidth = 1;
    
    [view addSubview:goodsCountTextFiled];
    [goodsCountTextFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(addBtn.mas_left).offset(-3);
        make.centerY.equalTo(addBtn);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(25);
    }];
    self.goodsCountTextFiled = goodsCountTextFiled;
    
    
    UIButton *reduceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    reduceBtn.layer.cornerRadius = 12;
    reduceBtn.clipsToBounds = YES;
    reduceBtn.layer.borderColor = [UIColor colorWithRGB:0xcccccc].CGColor;
    reduceBtn.layer.borderWidth = 1;
    [reduceBtn setTitle:@"-" forState:UIControlStateNormal];
    [reduceBtn setTitleColor:[UIColor colorWithRGB:0x333333] forState:UIControlStateNormal];
    reduceBtn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [reduceBtn addTarget:self action:@selector(goodsCountReduce) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:reduceBtn];
    [reduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(goodsCountTextFiled.mas_left).offset(-3);
        make.centerY.equalTo(goodsCountTextFiled);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(24);
    }];
    

    
    UILabel *youLabel = [[UILabel alloc] initWithText:@"邮寄费用" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:youLabel];
    [youLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_left).offset(0);
        make.top.equalTo(imgView.mas_bottom).offset(14);
    }];
    
    UILabel *youFeiLabel = [[UILabel alloc] initWithText:@"￥0 " font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:youFeiLabel];
    [youFeiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-11);
        make.top.equalTo(youLabel.mas_top);
    }];
    self.postPriceLabel = youFeiLabel;

    
    UIView *gapLine = [[UIView alloc] init];
    gapLine.backgroundColor = [UIColor colorWithRGB:0xeeeeee];
    [view addSubview:gapLine];
    [gapLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_left);
        make.right.equalTo(goodsCountTextFiled.mas_right);
        make.top.equalTo(youLabel.mas_bottom).offset(15);
    }];
    
    
    UILabel *sumFeiLabel = [[UILabel alloc] initWithText:@"共  0 件  小计  ￥8" font:15 textColor:[UIColor colorWithRGB:0x333333]];
    [view addSubview:sumFeiLabel];
    [sumFeiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-11);
        make.top.equalTo(gapLine.mas_bottom).offset(15);
        make.bottom.equalTo(view).offset(-15);
    }];
    self.totalMoneyLabel = sumFeiLabel;

    return view;
}



/**
 确定按钮
 */
- (UIView *)createPostBtn:(UIView *)topView{
    UIButton *view = [[UIButton alloc] init];
    [view setBackgroundColor:[UIColor colorWithRGB:0xFDD1E5]];
    [view setTitle:@"申请发货" forState:UIControlStateNormal];
    [view setTitleColor:[UIColor colorWithRGB:0xF86D6D] forState:UIControlStateNormal];
    [view setTitleColor:[UIColor colorWithRGB:0xF86D6D] forState:UIControlStateHighlighted];
    view.layer.cornerRadius = 6;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(40);
    }];
    [view addTarget:self action:@selector(submitDataToNet) forControlEvents:UIControlEventTouchUpInside];
    
    view.layer.shadowOffset = CGSizeMake(0,5);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 9;
    view.layer.cornerRadius = 4;
    view.layer.shadowColor = [UIColor colorWithRGB:0xFDD1E5].CGColor;
    
    return view;
}
#pragma mark - 商品数量加减按钮点击事件
- (void)goodsCountReduce{
    int count = [self.goodsCountTextFiled.text intValue];
    if (count > 1) {
        self.goodsCountTextFiled.text = [NSString stringWithFormat:@"%d",count - 1];
        [self caclulateTotalMoney:count - 1];
    }
}
- (void)goodsCountAdd{
    int count = [self.goodsCountTextFiled.text intValue];
    self.goodsCountTextFiled.text = [NSString stringWithFormat:@"%d",count + 1];
    [self caclulateTotalMoney:count + 1];
}
#pragma mark - 选择地址按钮
- (void)chooseAddress{
    //realname
    CCGirlAddressEditVc *vc = [[CCGirlAddressEditVc alloc]init];
    vc.selectAddressBlock = ^(CCAddressModel *model) {
        self.addressModel = model;
        [self resetUserAddressInfor];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)loadNetAddressList{
    [[CCGirlHttpManager share] getAddressList:USERID type:0 succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray<CCAddressModel *> * _Nullable model) {
        if (baseModel.error == 0) {
            if (model.count > 0) {
                self.addressModel = model[0];
                [self resetUserAddressInfor];
            }
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络错误"];
    }];
}
- (void)resetUserAddressInfor{
    self.userNameLabel.text = self.addressModel.realname;
    self.userPhoneLabel.text = self.addressModel.phone;
    NSString *location_infor = [self.addressModel.location_info isKindOfClass:[NSNull class]] || self.addressModel.location_info == nil ? @"" : self.addressModel.location_info;
    self.userAddressLabel.text = [NSString stringWithFormat:@"%@%@",location_infor,self.addressModel.location];
}
#pragma mark - 数据加载完成后更新UI
- (void)resetSubViewUI{
    
    [self.goodsIconImgView setImageURL:[NSURL URLWithString:self.bodyModel.image]];
    self.goodsNameLabel.text = self.bodyModel.name;
    self.goodsCountTextFiled.text = [NSString stringWithFormat:@"%d",1];
    
   [self caclulateTotalMoney:1];
}
#pragma mark - 网络请求
- (void)getGoodsDetailInfor{
    [[CCGirlHttpManager share] getKuCunGoodsDetail:USERID product_id:self.productData.id_ succeedBd:^(CCNetBaseModel * _Nonnull baseModel, CCGirlApplyDeliveryModel * _Nullable model) {
        if (baseModel.error == 0) {
            self.bodyModel = model;
            [self resetSubViewUI];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
        [CCToastView showText:@"网络请求失败"];
    }];
}
- (void)submitDataToNet{
    if(self.addressModel == nil){
        [CCToastView showText:@"请选择地址！"];
        return;
    }
    if ([self.goodsCountTextFiled.text intValue] <= 0) {
        [CCToastView showText:@"请输入领取数量!"];
        return;
    }
    [[CCGirlHttpManager share] applyToDelivery:USERID quantity:self.goodsCountTextFiled.text product_id:self.productData.id_ address:self.userAddressLabel.text phone:self.addressModel.phone name:self.addressModel.realname succeedBd:^(CCNetBaseModel * _Nonnull baseModel) {
        if (baseModel.error == 0) {
            [CCToastView showText:@"申请成功！"];
            //跳转详情页
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [CCToastView showText:baseModel.msg];
        }
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
         [CCToastView showText:@"网络请求失败！"];
    }];
}
#pragma mark - textField 代理事件
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableString *sumStr = [[NSMutableString alloc] initWithString:textField.text];
    [sumStr replaceCharactersInRange:range withString:string];
    [self caclulateTotalMoney:[sumStr intValue]];
    return true;
}
- (void)caclulateTotalMoney:(int)count{
    if (count <= 0) {
        count = 1;
    }
    int money = 8 + self.bodyModel.increase * (count - 1);
    self.totalMoneyLabel.text = [NSString stringWithFormat:@"共 %d 件 小计  ￥%d",count,money];
    self.postPriceLabel.text = [NSString stringWithFormat:@"￥%d",money];
}
@end
