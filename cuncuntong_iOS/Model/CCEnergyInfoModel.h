//
//  CCEnergyInfoModel.h
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCEnergyInfoModel : NSObject
@property (nonatomic, assign) int machine_level;
@property (nonatomic, assign) int total_produce_energy_coin;
@property (nonatomic, assign) int my_energy_coin;
@property (nonatomic, assign) int total_energy_coin_ceiling;

@property (nonatomic, assign) int max_coin_today;
@property (nonatomic, assign) int now_day_total_gold_number;


@property (nonatomic, strong) NSString *produce_start;
@property (nonatomic, strong) NSString *portrait;

@property (nonatomic, assign) int actualLevel;
@property (nonatomic, assign) int coinsOneMinute;


@end

NS_ASSUME_NONNULL_END
