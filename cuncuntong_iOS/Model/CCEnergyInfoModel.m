//
//  CCEnergyInfoModel.m
//  CunCunTong
//
//  Created by TAL on 2019/11/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "CCEnergyInfoModel.h"

@implementation CCEnergyInfoModel

- (int)actualLevel {
    if (self.machine_level == 0) {
        return 0;
    }else if (self.machine_level < 10){
        return self.machine_level + 2;
    }else{
        return self.machine_level - 10;
    }
    return 0;
}
- (int)coinsOneMinute {
    if (self.actualLevel < 3) {
        return 20;
    }else{
        return (self.actualLevel - 2) * 20 + 20;
    }
    return 0;
}
@end
