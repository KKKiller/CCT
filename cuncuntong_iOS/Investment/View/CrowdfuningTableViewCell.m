//
//  CrowdfuningTableViewCell.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CrowdfuningTableViewCell.h"

#import "MyUtil.h"

@implementation CrowdfuningTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.imageView_zt];
        [self.contentView addSubview:self.label_title];
        [self.contentView addSubview:self.label_money];
        [self.contentView addSubview:self.progressView];
        
        [self.contentView addSubview:self.label_ps];
        
        [self.contentView addSubview:self.textField_num];
        [self.contentView addSubview:self.textField_totalmoney];
        [self.contentView addSubview:self.textField_time];
    }
    return self;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    self.imageView_zt.frame = CGRectMake(10, 10, 70, 70);
    self.label_title.frame = CGRectMake(CGRectGetMaxX(self.imageView_zt.frame) + 10, CGRectGetMinY(self.imageView_zt.frame)-2, CGRectGetWidth(frame) - CGRectGetMaxX(self.imageView_zt.frame) - 20, 20);
    
    self.label_money.frame = CGRectMake(CGRectGetMinX(self.label_title.frame), CGRectGetMaxY(self.label_title.frame)+2, CGRectGetWidth(self.label_title.frame), 18);
    
    self.progressView.frame = CGRectMake(CGRectGetMinX(self.label_title.frame), CGRectGetMaxY(self.label_money.frame)+8, CGRectGetWidth(self.label_money.frame) - 35, 1);
    
    self.label_ps.frame = CGRectMake(CGRectGetMaxX(self.progressView.frame), CGRectGetMaxY(self.label_money.frame), 35, 18);
    
    self.textField_num.frame = CGRectMake(CGRectGetMinX(self.label_money.frame), 65, CGRectGetWidth(self.label_title.frame)/3 + 20, 20);
    
    self.textField_totalmoney.frame = CGRectMake(CGRectGetMaxX(self.textField_num.frame) - 20, 65, CGRectGetWidth(self.label_title.frame)/3, 20);
    
    self.textField_time.frame = CGRectMake(CGRectGetMaxX(self.textField_totalmoney.frame)+10, 65, CGRectGetWidth(self.label_title.frame)/3, 20);
}


#pragma mark - setters and getters

-(void)setCfdto:(CrowdfundingSimpleModel *)cfdto{
    _cfdto = cfdto;
    
    self.label_title.text = [NSString stringWithFormat:@"%@",cfdto.title];
    
    NSString *money = [NSString stringWithFormat:@"¥ %@",cfdto.money];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@元起",money]];
    [str addAttributes:@{NSForegroundColorAttributeName : [UIColor redColor],NSFontAttributeName : [UIFont systemFontOfSize:13]} range:NSMakeRange(0, money.length)];
    self.label_money.attributedText = str;
    
    CGFloat f1 = [_cfdto.need_money floatValue];
    CGFloat f2 = [_cfdto.total_money floatValue];
    
    self.label_ps.text = [NSString stringWithFormat:@"%0.0f%@",f2/f1*100,@"%"];//@"100%";
    
    [self.progressView setProgress:f2/f1];
    
    NSString *text2 = [cfdto.title containsString:@"县"]  ?  @"县（区）" : @"人";
    NSString *text = [cfdto.title containsString:@"村"]  ?  @"村" : text2;
    self.textField_num.text = [NSString stringWithFormat:@"  %@%@",cfdto.total_num,text];
//    self.textField_totalmoney.text = [NSString stringWithFormat:@"  %@元",cfdto.total_money];
    
    NSDate *da = [NSDate dateWithTimeIntervalSince1970:[cfdto.end_time integerValue]];
    NSDate *newda = [NSDate date];
    NSTimeInterval time = [da timeIntervalSinceDate:newda];
//    NSLog(@"%@",da);
    self.textField_time.text = [NSString stringWithFormat:@"  %i天",(int)time/(60*60*24)];
    
    [self.imageView_zt setImageURL:[NSURL URLWithString:_cfdto.mini]];
}

-(UIImageView *)imageView_zt{
    if (!_imageView_zt) {
        _imageView_zt = [[UIImageView alloc]init];
//        _imageView_zt.frame = CGRectMake(15, 10, 80, 80);
        _imageView_zt.backgroundColor = [UIColor whiteColor];
        _imageView_zt.contentMode = UIViewContentModeScaleAspectFill;
        _imageView_zt.layer.masksToBounds = YES;
    }
    return _imageView_zt;
}

-(UILabel *)label_title{
    if (!_label_title) {
        _label_title = [[UILabel alloc]init];
        _label_title.font = [UIFont systemFontOfSize:14];
    }
    return _label_title;
}

-(UILabel *)label_money{
    if (!_label_money) {
        _label_money = [[UILabel alloc]init];
        _label_money.font = [UIFont systemFontOfSize:9];
        _label_money.textColor = [UIColor grayColor];
    }
    return _label_money;
}

-(UIProgressView *)progressView{
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] init];//WithFrame:CGRectMake(0, 64, KEY_WIDTH, 0)];
        _progressView.progressTintColor = [UIColor colorWithRed:84/255.0f green:187/255.0f blue:247/255.0f alpha:1.0f];
        _progressView.backgroundColor = [UIColor grayColor];
        _progressView.transform = CGAffineTransformMakeScale(1.0f,2.0f);
    }
    return _progressView;
}

-(UILabel *)label_ps{
    if (!_label_ps) {
        _label_ps = [[UILabel alloc]init];
        _label_ps.textColor = [UIColor colorWithRed:0.22f green:0.49f blue:1.00f alpha:1.00f];
        _label_ps.font = [UIFont systemFontOfSize:9];
        _label_ps.textAlignment = NSTextAlignmentCenter;
    }
    return _label_ps;
}

-(UITextField *)textField_num{
    if (!_textField_num) {
        _textField_num = [MyUtil createTextFieldFrame:CGRectZero bgColor:[UIColor clearColor] placeholder:@"" textAlignment:NSTextAlignmentLeft font:10 textColor:[UIColor grayColor]];
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 7, 13, 13)];
        img.image = [UIImage imageNamed:@"zc_filter_people"];
        _textField_num.leftView = img;
        _textField_num.leftViewMode = UITextFieldViewModeAlways;
        _textField_num.userInteractionEnabled = NO;
    }
    return _textField_num;
}

-(UITextField *)textField_totalmoney{
    if (!_textField_totalmoney) {
        _textField_totalmoney = [MyUtil createTextFieldFrame:CGRectZero bgColor:[UIColor clearColor] placeholder:@"" textAlignment:NSTextAlignmentLeft font:10 textColor:[UIColor grayColor]];
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 7, 13, 13)];
        img.image = [UIImage imageNamed:@"zc_filter_money"];
        _textField_totalmoney.leftView = img;
        _textField_totalmoney.leftViewMode = UITextFieldViewModeAlways;
        _textField_totalmoney.userInteractionEnabled = NO;
        _textField_totalmoney.hidden = YES;
    }
    return _textField_totalmoney;
}

-(UITextField *)textField_time{
    if (!_textField_time) {
        _textField_time = [MyUtil createTextFieldFrame:CGRectZero bgColor:[UIColor clearColor] placeholder:@"" textAlignment:NSTextAlignmentLeft font:10 textColor:[UIColor grayColor]];
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 7, 13, 13)];
        img.image = [UIImage imageNamed:@"zc_filter_time"];
        _textField_time.leftView = img;
        _textField_time.leftViewMode = UITextFieldViewModeAlways;
        
        _textField_time.userInteractionEnabled = NO;
    }
    return _textField_time;
}
@end
