//
//  CrowdfuningTableViewCell.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CrowdfundingSimpleModel.h"
/**
 众投 列表 cell
 */
@interface CrowdfuningTableViewCell : UITableViewCell


@property(nonatomic,strong)CrowdfundingSimpleModel *cfdto;

/**
 Description 众投 图片
 */
@property(nonatomic,strong)UIImageView *imageView_zt;

/**
 Description 众投 标题
 */
@property(nonatomic,strong)UILabel *label_title;

/**
 Description 众投 每份的价格
 */
@property(nonatomic,strong)UILabel *label_money;

/**
 Description 进度
 */
@property(nonatomic,strong)UIProgressView *progressView;

/**
 Description 百分比
 */
@property(nonatomic,strong)UILabel *label_ps;

/**
 Description 人次
 */
@property(nonatomic,strong)UITextField *textField_num;

/**
 Description 金额
 */
@property(nonatomic,strong)UITextField *textField_totalmoney;

/**
 Description 时间
 */
@property(nonatomic,strong)UITextField *textField_time;


@end
