//
//  MineCrowdfundingTableViewCell.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/4.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineCrowdfundingTableViewCell : UITableViewCell

@property(nonatomic,strong)UIImageView *imgeView;
@property(nonatomic,strong)UILabel *label_title;
@property(nonatomic,strong)UILabel *label_money;
@property(nonatomic,strong)UILabel *label_time;

@property(nonatomic,strong)NSDictionary *dictionary_logs;
@property(nonatomic,strong)NSDictionary *dictionary_zc;
@end
