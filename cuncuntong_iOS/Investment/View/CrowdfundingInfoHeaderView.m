//
//  CrowdfundingInfoHeaderView.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CrowdfundingInfoHeaderView.h"

#import "Define.h"

@implementation CrowdfundingInfoHeaderView
{
    CGFloat hight_1;
    
    BOOL isInfo;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        isInfo = NO;
        [self addSubview:self.img_cover];
        [self addSubview:self.label_title];
        [self addSubview:self.textview_des];
        [self addSubview:self.progressview];
        [self addSubview:self.label_pr];
        [self addSubview:self.label_num];
        [self addSubview:self.label_money];
        [self addSubview:self.label_time];
        [self addSubview:self.button_info];
//        [self addSubview:self.button_pay];
        
//        [self addSubview:self.webview_content];
    }
    return self;
}


/**
 Description 点击 查看详情
 */
-(void)ButtonClickToInfo{
//    NSString *ht = [self.cfInfoDto.content stringByReplacingOccurrencesOfString:@"data-src" withString:@"src"];
//    [self.webview_content loadHTMLString:ht baseURL:nil];//_cfInfoDto.content
    if ([self.delegate respondsToSelector:@selector(CrowdfundingInfoHeaderViewButtonClickToInfo)]) {
        [self.delegate CrowdfundingInfoHeaderViewButtonClickToInfo];
    }
}


/**
 购买 button
 */
-(void)ButtonClickPay{

}

/**
 Description

 @param attstring 字符串
 @param label label
 */
-(void)LabelAttributes:(NSString *)attstring andLabel:(UILabel *)label{
    NSMutableAttributedString *num1 = [[NSMutableAttributedString alloc]initWithString:attstring];
    [num1 addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:11],NSForegroundColorAttributeName : [UIColor grayColor]} range:NSMakeRange(0, 4)];
    label.attributedText = num1;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
}


#pragma mark  - KVO回调
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{

//    }
}

- (void)dealloc {
    
//    [self.webview_content.scrollView removeObserver:self forKeyPath:@"contentSize"];
}
#pragma mark -UIWebViewDelegate
-(void)webViewDidFinishLoad:(UIWebView *)webView{
//        CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
////    NSLog(@"webviewheight = %f",height);offsetHeight scrollHeight
//        self.webview_content.frame = CGRectMake(0, hight_1 - 70, [UIScreen mainScreen].bounds.size.width, height);
//       self.button_pay.frame = CGRectMake(30, CGRectGetMaxY(self.webview_content.frame)+5, KEY_WIDTH - 60, 50);
//        self.headerHeight = hight_1 + height;
}

#pragma mark - setters and getters

-(void)setCfInfoDto:(CrowdfundingInfoModel *)cfInfoDto{
    _cfInfoDto = cfInfoDto;
    
    [self.img_cover setImageURL:[NSURL URLWithString:_cfInfoDto.cover]];
    
    self.label_title.text = [NSString stringWithFormat:@"%@",_cfInfoDto.title];
    
    self.img_cover.frame = CGRectMake(0, 0, KEY_WIDTH, (200)*KEY_RATE);
    self.label_title.frame = CGRectMake(10, CGRectGetMaxY(self.img_cover.frame)+5, KEY_WIDTH - 20, 35);
    
    NSString *des = [NSString stringWithFormat:@"%@",_cfInfoDto.desc];
    CGSize size = [des boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.label_title.frame), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size;//sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:9]}];
    self.textview_des.frame = CGRectMake(10, CGRectGetMaxY(self.label_title.frame), CGRectGetWidth(self.label_title.frame), size.height+20);
    self.textview_des.text = des;
    
    CGFloat f1 = [_cfInfoDto.need_money floatValue];
    CGFloat f2 = [_cfInfoDto.total_money floatValue];
//    f1 = f1 >0 ? f1 : 0;
//    f2 = 0;
    self.progressview.frame = CGRectMake(20, CGRectGetMaxY(self.textview_des.frame)+10, CGRectGetWidth(self.label_title.frame)-20, 2);
    [self.progressview setProgress:f2/f1];
    
    self.label_pr.frame = CGRectMake(CGRectGetWidth(self.progressview.frame)*(f2/f1 <1 ? f2/f1:1.0), CGRectGetMinY(self.progressview.frame)-8, 25, 18);
    self.label_pr.text = [NSString stringWithFormat:@"%0.0f%@",f2/f1*100,@"%"];//@"100%";
    self.label_pr.layer.cornerRadius = 8;
    
    self.label_num.frame = CGRectMake(10, CGRectGetMaxX(self.progressview.frame)+5, CGRectGetWidth(self.label_title.frame)/3, 60);
//    self.label_num.backgroundColor = [UIColor blackColor];
    self.label_money.frame = CGRectMake(CGRectGetMaxX(self.label_num.frame), CGRectGetMaxX(self.progressview.frame)+5 , CGRectGetWidth(self.label_title.frame)/3, 60);
    self.label_time.frame = CGRectMake(CGRectGetMaxX(self.label_money.frame), CGRectGetMaxX(self.progressview.frame) + 5, CGRectGetWidth(self.label_title.frame)/3, 60);
    
    NSString *num = [NSString stringWithFormat:@"支持人数\n\n%@人",_cfInfoDto.total_num];
    [self LabelAttributes:num andLabel:self.label_num];
    
    NSString *money = [NSString stringWithFormat:@"已筹金额\n\n%@元",_cfInfoDto.total_money];
    [self LabelAttributes:money andLabel:self.label_money];
    
    NSDate *da = [NSDate dateWithTimeIntervalSince1970:[_cfInfoDto.end_time integerValue]];
    NSDate *newda = [NSDate date];
    NSTimeInterval time1 = [da timeIntervalSinceDate:newda];
    NSString *time = [NSString stringWithFormat:@"剩余时间\n\n%i天",(int)time1/(60*60*24)];
    [self LabelAttributes:time andLabel:self.label_time];
    
    self.button_info.frame = CGRectMake(CGRectGetWidth(self.label_title.frame)/2 - 70, CGRectGetMaxY(self.label_time.frame)+5, 160, 30);
    
//    self.button_pay.frame = CGRectMake(30, CGRectGetMaxY(self.button_info.frame)+15, KEY_WIDTH - 60, 50);
//    [self.button_pay setTitle:[NSString stringWithFormat:@"去支持(¥%@元起)",_cfInfoDto.money] forState:UIControlStateNormal];
    hight_1 = CGRectGetMaxY(self.button_info.frame) + 20;
    self.headerHeight = hight_1;
}

-(void)setHeaderHeight:(CGFloat)headerHeight{
    _headerHeight = headerHeight;
    CGRect frame = self.frame;
    frame.size.height = _headerHeight;
    self.frame = frame;
    if ([self.delegate respondsToSelector:@selector(CrowdfundingInfoHeaderView:andHeight:)]) {
        [self.delegate CrowdfundingInfoHeaderView:self andHeight:headerHeight];
    }
}

-(UIImageView *)img_cover{
    if (!_img_cover) {
        _img_cover = [[UIImageView alloc]init];
    }
    return _img_cover;
}
-(UILabel *)label_title{
    if (!_label_title) {
        _label_title = [[UILabel alloc]init];
        _label_title.font = [UIFont systemFontOfSize:16];
        [_label_title sizeToFit];
    }
    return _label_title;
}
-(UITextView *)textview_des{
    if (!_textview_des) {
        _textview_des = [[UITextView alloc]init];
//        _textview_des.backgroundColor = [UIColor blueColor];
        _textview_des.font = [UIFont systemFontOfSize:12];
        _textview_des.textColor = [UIColor grayColor];//[UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1];
        _textview_des.userInteractionEnabled = NO;
    }
    return _textview_des;
}

-(UIProgressView *)progressview{
    if (!_progressview) {
        _progressview = [[UIProgressView alloc] init];//WithFrame:CGRectMake(0, 64, KEY_WIDTH, 0)];
        _progressview.progressTintColor = [UIColor colorWithRed:84/255.0f green:187/255.0f blue:247/255.0f alpha:1.0f];//[UIColor greenColor];
        _progressview.backgroundColor = [UIColor grayColor];
        _progressview.transform = CGAffineTransformMakeScale(1.0f,2.0f);
    }
    return _progressview;
}

-(UILabel *)label_pr{
    if (!_label_pr) {
        _label_pr = [[UILabel alloc]init];
        _label_pr.backgroundColor = [UIColor colorWithRed:84/255.0f green:187/255.0f blue:247/255.0f alpha:1.0f];
        _label_pr.font = [UIFont systemFontOfSize:9];
        _label_pr.textColor = [UIColor whiteColor];
        _label_pr.layer.masksToBounds = YES;
        _label_pr.textAlignment = NSTextAlignmentCenter;
    }
    return _label_pr;
}

-(UILabel *)label_num{
    if (!_label_num) {
        _label_num = [[UILabel alloc]init];
        _label_num.font = [UIFont systemFontOfSize:15];
        _label_num.textAlignment = NSTextAlignmentCenter;
        _label_num.numberOfLines = 0;
    }
    return _label_num;
}
-(UILabel *)label_money{
    if (!_label_money) {
        _label_money = [[UILabel alloc]init];
        _label_money.font = [UIFont systemFontOfSize:15];
        _label_money.textAlignment = NSTextAlignmentCenter;
        _label_money.numberOfLines = 0;
        [_label_money sizeToFit];
    }
    return _label_money;
}
-(UILabel *)label_time{
    if (!_label_time) {
        _label_time = [[UILabel alloc]init];
        _label_time.font = [UIFont systemFontOfSize:15];
        _label_time.textAlignment = NSTextAlignmentCenter;
        _label_time.numberOfLines = 0;
    }
    return _label_time;
}

-(UIButton *)button_info{
    if (!_button_info) {
        _button_info = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_button_info setTitle:@"查看详情>>" forState:UIControlStateNormal];
        _button_info.titleLabel.textColor = [UIColor colorWithRed:84/255.0f green:187/255.0f blue:247/255.0f alpha:1.0f];
        _button_info.titleLabel.font = [UIFont systemFontOfSize:13];
        [_button_info addTarget:self action:@selector(ButtonClickToInfo) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_info;
}

//-(UIButton *)button_pay{
//    if (!_button_pay) {
//        _button_pay = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//        [_button_pay setTitle:@"去支持" forState:UIControlStateNormal];
//        [_button_pay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
////        _button_pay.titleLabel.tintColor = [UIColor whiteColor];//[UIColor colorWithRed:84/255.0f green:187/255.0f blue:247/255.0f alpha:1.0f];
//        _button_pay.backgroundColor = [UIColor colorWithRed:84/255.0f green:187/255.0f blue:247/255.0f alpha:1.0f];
//        _button_pay.layer.masksToBounds = YES;
//        _button_pay.layer.cornerRadius = 5;
////        _button_pay.titleLabel.font = [UIFont systemFontOfSize:20];
//        [_button_pay addTarget:self action:@selector(ButtonClickPay) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _button_pay;
//}

//-(UIWebView *)webview_content{
//    if (!_webview_content) {
//        _webview_content = [[UIWebView alloc]init];
//        _webview_content.frame = CGRectZero;
//        _webview_content.delegate = self;
////        _webview_content.UIDelegate = self;
////        _webview_content.navigationDelegate = self;
////        _webview_content.scrollView.bounces = NO;
////         [_webview_content.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
//    }
//    return _webview_content;
//}
@end
