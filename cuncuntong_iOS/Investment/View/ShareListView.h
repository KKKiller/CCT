//
//  ShareListView.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSInteger {
    Share_weixin = 0,
    Share_pengyouquan,
    Share_qq,
    Share_qqkj,
    Share_shoucang
} ShareListViewButtonType;

@protocol ShareListViewDelegate <NSObject>

- (void)itemClick:(UIButton *)sender;

@end

@interface ShareListView : UIView

@property (nonatomic, weak) id<ShareListViewDelegate> delegate;

/**
   初始化

 @param array 分享类型 ShareListViewButtonType 数组
 @return 
 */
-(instancetype)initWithShareTypes:(NSArray *)array;

- (void)initColStatus:(BOOL)isCol;
- (void)changeColStatus:(BOOL)isCol;
@end
