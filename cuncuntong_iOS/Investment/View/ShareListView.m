//
//  ShareListView.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "ShareListView.h"

@interface ShareListView()

@property (nonatomic, strong) UIView *list;
@property (nonatomic, strong) NSArray *iconArr;
@property (nonatomic, strong) NSArray *titleArray;

@end

@implementation ShareListView

- (instancetype)init {
    
    if (self = [super init]) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        [self addSubview:self.list];
        [self.list mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.right.equalTo(self).with.offset(-16);
            make.height.mas_equalTo(297*KEY_RATE);
            make.width.mas_equalTo(205*KEY_RATE);
        }];
    }
    return self;
}

-(instancetype)initWithShareTypes:(NSArray *)array{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        [self addSubview:self.list];
        [self.list mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.right.equalTo(self).with.offset(0);
            make.height.mas_equalTo(297*KEY_RATE*array.count/6);
            make.width.mas_equalTo(205*KEY_RATE);
        }];
        
        for (int i = 0; i < array.count; i++) {
            
            NSInteger type = [[array objectAtIndex:i] integerValue];
//            NSLog(@"type = %li ,T= %li",type,(long)Share_weixin);
            UIButton *btn = [[UIButton alloc] init];
            [self.list addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(20 * KEY_RATE);
                make.top.mas_equalTo((20 + 45 * i)*KEY_RATE);
                make.right.mas_equalTo(-30 * KEY_RATE);
                make.height.mas_equalTo(30);
            }];
            btn.highlighted = NO;
            btn.titleEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btn setImage:[UIImage imageNamed:self.iconArr[type]] forState:UIControlStateNormal];
            [btn setTitle:self.titleArray[type] forState:UIControlStateNormal];
            btn.titleLabel.font = FontSize(16);
            [btn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = 3000 + type;
        }
    }
    return self;
}

- (UIView *)list {
    if (!_list) {
        _list = [[UIView alloc] init];
        _list.backgroundColor = [UIColor whiteColor];
    }
    return _list;
}

- (NSArray *)iconArr {
    if (!_iconArr) {
        _iconArr = @[@"weixin", @"pengyouquan", @"qq", @"qqkj", @"shoucang", @"tousu"];
    }
    return _iconArr;
}

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = @[@"发送给微信好友", @"分享到朋友圈", @"发送给QQ好友", @"分享到QQ空间", @"收藏", @"投诉"];
    }
    return _titleArray;
}

#pragma mark - Target Action
- (void)btnClick:(UIButton *)sender {
    if ([_delegate respondsToSelector:@selector(itemClick:)]) {
        [_delegate itemClick:sender];
    }
}

- (void)initColStatus:(BOOL)isCol {
    UIButton *colButton = [self viewWithTag:3004];
    if (!isCol) {
        [colButton setTitle:@"收藏" forState:UIControlStateNormal];
        [colButton setImage:[UIImage imageNamed:@"shoucang"] forState:UIControlStateNormal];
        return;
    }
    [colButton setTitle:@"取消收藏" forState:UIControlStateNormal];
    [colButton setImage:[UIImage imageNamed:@"quxiaoshoucang"] forState:UIControlStateNormal];
    
}

- (void)changeColStatus:(BOOL )isCol {
    
    
    UIButton *colButton = [self viewWithTag:3004];
    if (isCol) {
        [colButton setTitle:@"收藏" forState:UIControlStateNormal];
        [colButton setImage:[UIImage imageNamed:@"shoucang"] forState:UIControlStateNormal];
        return;
    }
    [colButton setTitle:@"取消收藏" forState:UIControlStateNormal];
    [colButton setImage:[UIImage imageNamed:@"quxiaoshoucang"] forState:UIControlStateNormal];
    
}


@end
