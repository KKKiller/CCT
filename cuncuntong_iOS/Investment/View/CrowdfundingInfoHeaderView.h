//
//  CrowdfundingInfoHeaderView.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

#import "CrowdfundingInfoModel.h"

@class CrowdfundingInfoHeaderView;
@protocol CrowdfundingInfoHeaderViewDelegate <NSObject>

@optional
/**
 更新 高度
 */
-(void)CrowdfundingInfoHeaderView:(CrowdfundingInfoHeaderView *)headerView andHeight:(CGFloat)height;

/**
 点击 去查看详情
 */
-(void)CrowdfundingInfoHeaderViewButtonClickToInfo;
@end

/**
 Description 众投详情 头视图
 */
@interface CrowdfundingInfoHeaderView : UIView <WKUIDelegate,WKNavigationDelegate,UIWebViewDelegate>

/**
 Description 众投 数据
 */
@property(nonatomic,strong)CrowdfundingInfoModel *cfInfoDto;

/**
 Description 高度
 */
@property(nonatomic,assign)CGFloat headerHeight;

@property(nonatomic,strong)id<CrowdfundingInfoHeaderViewDelegate> delegate;

/**封面图片*/
@property(nonatomic,strong)UIImageView *img_cover;

/**
 Description 众筹 标题
 */
@property(nonatomic,strong)UILabel *label_title;

/**
 Description 众筹 描述
 */
@property(nonatomic,strong)UITextView *textview_des;

/**
 Description 众筹 条
 */
@property(nonatomic,strong)UIProgressView *progressview;

/**
 Description 百分比
 */
@property(nonatomic,strong)UILabel *label_pr;

/**
 Description 人数
 */
@property(nonatomic,strong)UILabel *label_num;

/**
 Description 金额
 */
@property(nonatomic,strong)UILabel *label_money;

/**
 Description 剩余时间
 */
@property(nonatomic,strong)UILabel *label_time;

/**
 Description 查看详情
 */
@property(nonatomic,strong)UIButton *button_info;

///**
// 购买 button
// */
//@property(nonatomic,strong)UIButton *button_pay;

/**
 Description 网页详情
 */
//@property(nonatomic,strong)UIWebView *webview_content;
@end
