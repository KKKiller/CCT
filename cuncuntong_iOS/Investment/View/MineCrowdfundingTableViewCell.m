//
//  MineCrowdfundingTableViewCell.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/4.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "MineCrowdfundingTableViewCell.h"

@implementation MineCrowdfundingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.imgeView];
        [self.contentView addSubview:self.label_title];
        [self.contentView addSubview:self.label_money];
        [self.contentView addSubview:self.label_time];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - setters and getters

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.imgeView.frame = CGRectMake(15, 10, 70, 70);
    
    self.label_title.frame = CGRectMake(CGRectGetMaxX(self.imgeView.frame) + 10, CGRectGetMinY(self.imgeView.frame), CGRectGetWidth(frame) - CGRectGetMaxX(self.imgeView.frame) - 20, 25);
    
    self.label_money.frame = CGRectMake(CGRectGetMinX(self.label_title.frame), CGRectGetMaxY(self.label_title.frame), CGRectGetWidth(self.label_title.frame), 25);
    self.label_time.frame = CGRectMake(CGRectGetMinX(self.label_title.frame), CGRectGetMaxY(self.label_money.frame), CGRectGetWidth(self.label_title.frame), 20);
}

-(void)setDictionary_logs:(NSDictionary *)dictionary_logs{
    _dictionary_logs = dictionary_logs;
    
    NSString *m = [NSString stringWithFormat:@"已支付%@元",[_dictionary_logs objectForKey:@"money"]];
    NSMutableAttributedString *monry = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@    %@份",m,[_dictionary_logs objectForKey:@"num"]]];
    [monry addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor],NSFontAttributeName :[UIFont systemFontOfSize:12]} range:NSMakeRange(0, m.length)];
    self.label_money.attributedText = monry;
    
    NSTimeInterval time = [[dictionary_logs objectForKey:@"dateline"] integerValue];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *st = [format stringFromDate:date];
    self.label_time.text = st;
}

-(void)setDictionary_zc:(NSDictionary *)dictionary_zc{
    _dictionary_zc = dictionary_zc;
    self.label_title.text = [NSString stringWithFormat:@"%@",[_dictionary_zc objectForKey:@"title"]];
    [self.imgeView setImageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[_dictionary_zc objectForKey:@"mini"]]]];
}

-(UIImageView *)imgeView{
    if (!_imgeView) {
        _imgeView = [[UIImageView alloc]init];
        _imgeView.backgroundColor = [UIColor whiteColor];
        _imgeView.contentMode = UIViewContentModeScaleAspectFill;
        _imgeView.layer.masksToBounds = YES;
    }
    return _imgeView;
}

-(UILabel *)label_title{
    if (!_label_title) {
        _label_title = [[UILabel alloc]init];
        _label_title.font = [UIFont systemFontOfSize:14];
    }
    return _label_title;
}

-(UILabel *)label_money{
    if (!_label_money) {
        _label_money = [[UILabel alloc]init];
        _label_money.font = [UIFont systemFontOfSize:10];
        _label_money.textColor = [UIColor grayColor];
    }
    return _label_money;
}

-(UILabel *)label_time{
    if (!_label_time) {
        _label_time = [[UILabel alloc]init];
        _label_time.font = [UIFont systemFontOfSize:10];
        _label_time.textColor = [UIColor grayColor];
    }
    return _label_time;
}
@end
