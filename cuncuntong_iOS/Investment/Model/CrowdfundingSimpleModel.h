//
//  CrowdfundingSimpleModel.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Description 众投列表 数据
 */
@interface CrowdfundingSimpleModel : NSObject

/**
 众筹 简介
 */
@property(nonatomic,copy)NSString *desc;
/**
 众筹 截止时间
 */
@property(nonatomic,copy)NSString *end_time;
/**
 众筹 ID
 */
@property(nonatomic,copy)NSString *cfid;
/**
 众筹 图片
 */
@property(nonatomic,copy)NSString *mini;
/**
 众筹 每份金额
 */
@property(nonatomic,copy)NSString *money;
/**
 众筹
 */
@property(nonatomic,copy)NSString *need_money;
/**
 众筹 标题
 */
@property(nonatomic,copy)NSString *title;
/**
 众筹 总金额
 */
@property(nonatomic,copy)NSString *total_money;
/**
 众筹 总人次
 */
@property(nonatomic,copy)NSString *total_num;
@end
