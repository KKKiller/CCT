//
//  CrowdfundingSimpleModel.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CrowdfundingSimpleModel.h"

@implementation CrowdfundingSimpleModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"cfid" : @"id"};
}
@end
