//
//  CrowdfundingInfoModel.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Description 众筹 详情
 */
@interface CrowdfundingInfoModel : NSObject
/**
 众筹 简介
 */
@property(nonatomic,copy)NSString *desc;
/**
 众筹 截止时间
 */
@property(nonatomic,copy)NSString *end_time;
/**
 众筹 ID
 */
@property(nonatomic,copy)NSString *cfid;
/**
 众筹 图片
 */
@property(nonatomic,copy)NSString *mini;
/**
 众筹 每份金额
 */
@property(nonatomic,copy)NSString *money;
/**
 众筹
 */
@property(nonatomic,copy)NSString *need_money;
/**
 众筹 标题
 */
@property(nonatomic,copy)NSString *title;
/**
 众筹 总金额
 */
@property(nonatomic,copy)NSString *total_money;
/**
 众筹 总人次
 */
@property(nonatomic,copy)NSString *total_num;

/**
 Description 评论数
 */
@property(nonatomic,copy)NSString *comment;

/**
 Description  内容
 */
@property(nonatomic,copy)NSString *content;

/**
 Description 封面图
 */
@property(nonatomic,copy)NSString *cover;

/**
 Description 创建日期
 */
@property(nonatomic,copy)NSString *create;

@property(nonatomic,copy)NSString *orderby;

/**
 Description 发布日期
 */
@property(nonatomic,copy)NSString *publish;
/**
 Description 阅读量
 */
@property(nonatomic,copy)NSString *reader;
/**
 Description 文章状态 0 ：被删除 1:审核中 2：在线
 */
@property(nonatomic,copy)NSString *status;
/**
 Description 村序号
 */
@property(nonatomic,copy)NSString *vid;
/**
 Description 发稿人 序号
 */
@property(nonatomic,copy)NSString *wid;
/**
 Description 点赞量
 */
@property(nonatomic,copy)NSString *zan;
@end
