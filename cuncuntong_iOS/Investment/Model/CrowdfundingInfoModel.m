//
//  CrowdfundingInfoModel.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/3.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CrowdfundingInfoModel.h"

@implementation CrowdfundingInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"cfid" : @"id"};
}
@end
