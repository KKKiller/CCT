//
//  ZcPayViewController.h
//  UIDesignViewProject
//
//  Created by HAORUN on 2017/4/5.
//  Copyright © 2017年 HAORUN. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CrowdfundingSimpleModel.h"

/**
 支付 页面
 */
@interface ZcPayViewController : UIViewController

@property(nonatomic,strong)CrowdfundingSimpleModel *cfdto;

@property(nonatomic,strong)UIView *view_bottom;

/**
 图片
 */
@property(nonatomic,strong)UIImageView *img_zc;

/**
 标题
 */
@property(nonatomic,strong)UILabel *label_title;

/**
 每份金额
 */
@property(nonatomic,strong)UILabel *label_money;

/**
 
 */
@property(nonatomic,strong)UILabel *label_num;

/**
 旋转的份数
 */
@property(nonatomic,strong)UITextField *textField_num;

@property(nonatomic,strong)UIButton *button_pay;


@end
