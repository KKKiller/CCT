//
//  CrowdfundingViewController.h
//  cuncuntong_iOS
//
//  Created by i棠_熊 on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

/**
 众筹模块 首页
 */
@interface CrowdfundingViewController : BaseViewController

/**
 众投 列表
 */
@property(nonatomic,strong)UITableView *tableview_list;

/**
  众投 数据
 */
@property(nonatomic,strong)NSMutableArray *array_ztdto;

/**
 头图片
 */
//@property(nonatomic,strong)UIImageView *imageview_header;

@property (nonatomic, assign) BOOL existBackBtn;
@end
