//
//  CrowdfundingInfoViewController.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CrowdfundingInfoViewController.h"
#import <WebKit/WebKit.h>

#import "ZcPayViewController.h"
#import "CrowdfundingInfoHeaderView.h"
#import "ShareListView.h"

static NSString * const ztinfoTableViewCellid = @"ztInfoTableViewListCellId";

@interface CrowdfundingInfoViewController ()<ShareListViewDelegate,WKNavigationDelegate>
{
    NSInteger Page;
}

/**
 详情内容
 */
@property(nonatomic,strong)WKWebView *webView_list;

@property(nonatomic,strong)UIProgressView *progressView;


//下拉 分享 菜单
@property(nonatomic,strong)ShareListView *shareView;
/**
 分享的链接
 */
@property(nonatomic,strong)NSString *share_url;
@property(nonatomic,strong)NSString *share_mini;

/**
  是否收藏
 */
@property(nonatomic,assign)BOOL isCollect;

//@property(nonatomic,strong)CrowdfundingInfoHeaderView *cfHeaderView;

@end

@implementation CrowdfundingInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self addReturnBtn:@"tui_"];
    
    UIBarButtonItem *rightbarbutton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"xialacaidan"] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemClick)];
    rightbarbutton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = rightbarbutton;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.view addSubview:self.webView_list];
    [self.view addSubview:self.progressView];
    
    
    
//    [self.view insertSubview:self.webView_list belowSubview:self.progressView];
    
    [self.webView_list addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 Description 下拉菜单
 */
-(void)rightBarButtonItemClick{
    self.shareView.hidden = !self.shareView.hidden;
}

-(void)ButtonClickPay{
    //购买
    ZcPayViewController *zcPayVc = [[ZcPayViewController alloc]init];
    zcPayVc.cfdto = self.cfDto;
//    [self.navigationController pushViewController:zcPayVc animated:YES];
    UINavigationController *naVc = [[UINavigationController alloc]initWithRootViewController:zcPayVc];
    naVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:naVc animated:YES completion:nil];
}

#pragma mark  - KVO回调
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        _progressView.hidden = _webView_list.estimatedProgress == 1.0;
//        [_progressView setProgress:(float)_webView_list.estimatedProgress animated:YES];
        self.progressView.progress = self.webView_list.estimatedProgress;
    }
}

- (void)dealloc {
    [self.webView_list removeObserver:self forKeyPath:@"estimatedProgress"];
}

#pragma mark - WKWebView delegate

-(void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
    [webView reload];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    NSString *url = [navigationAction.request.URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"url = %@",url);
    if ([url isEqualToString:@"http://www.cct369.com/village/public/zc/buy"]) {
        [self ButtonClickPay];
      decisionHandler(WKNavigationActionPolicyCancel);
    }else{
      decisionHandler(WKNavigationActionPolicyAllow);
    }
}

#pragma mark - setters and getters
-(void)setCfDto:(CrowdfundingSimpleModel *)cfDto{
    _cfDto = cfDto;
    
    self.share_mini = [NSString stringWithFormat:@"%@",cfDto.mini];
    self.share_url = [NSString stringWithFormat:@"http://www.cct369.com/wxman/url.php?zc_id=%@",cfDto.cfid];
    
    [self setTitle:[NSString stringWithFormat:@"%@",_cfDto.title]];
//    [self getCfInfobyId:[NSString stringWithFormat:@"%@",_cfDto.cfid]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    NSString *url = [NSString stringWithFormat:@"http://www.cct369.com/village/public/zc/hview?id=%@&u=1%@&inapp",_cfDto.cfid,USERID];
    NSLog(@"url = %@",url);
    [self.webView_list loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

-(void)setCfInfoDto:(CrowdfundingInfoModel *)cfInfoDto{
    _cfInfoDto = cfInfoDto;
}

-(void)setShare_url:(NSString *)share_url{
    _share_url = share_url;
}
-(void)setShare_mini:(NSString *)share_mini{
    _share_mini = share_mini;
}

-(WKWebView *)webView_list{
    if (!_webView_list) {
        
//        WKWebViewConfiguration * configuration = [[WKWebViewConfiguration alloc]init];//先实例化配置类 以前UIWebView的属性有的放到了这里
//        configuration.preferences = [[WKPreferences alloc]init];
        //注册供js调用的方法
//        userContentController =[[WKUserContentController alloc]init];
        //弹出登录
//        [configuration.userContentController addScriptMessageHandler:self  name:@"buy()"];
        
//        configuration.userContentController = userContentController;
//        configuration.preferences.javaScriptEnabled = YES;//打开js交互
        
        _webView_list = [[WKWebView alloc]init];//WithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-64) configuration:configuration];
        _webView_list.frame = CGRectMake(0, 64, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-64);
        _webView_list.backgroundColor = [UIColor whiteColor];
        _webView_list.navigationDelegate = self;
//        _webView_list.UIDelegate = self;
        _webView_list.allowsLinkPreview = YES;//允许预览链接
    }
    return _webView_list;
}

-(UIProgressView *)progressView{
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, KEY_WIDTH, 0)];
        _progressView.progressTintColor = [UIColor greenColor];
        _progressView.transform = CGAffineTransformMakeScale(1.0f,2.0f);
    }
    return _progressView;
}


- (ShareListView *)shareView {
    
    if (!_shareView) {
        _shareView = [[ShareListView alloc]initWithShareTypes:@[[NSNumber numberWithInteger:Share_weixin],[NSNumber numberWithInteger:Share_pengyouquan],[NSNumber numberWithInteger:Share_shoucang]]];
//        [_shareView initColStatus:_isCollect];
        [self.view addSubview:_shareView];
        [self.shareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(KEY_HEIGHT - 64);
        }];
        _shareView.hidden = YES;
        _shareView.delegate = self;
        [_shareView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                         initWithActionBlock:^(id  _Nonnull sender) {
                                             _shareView.hidden = YES;
                                         }]];
        
    }
    return _shareView;
}

#pragma mark - 分享

- (void)collection {
    [self.shareView changeColStatus:_isCollect];
    if (_isCollect) {
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"center/dela")
                                        params:@{@"id":USERID, @"aid": self.cfDto.cfid,@"col_type":@"1"}
                                        target:self
                                       success:^(NSDictionary *success) {
                                           [MBProgressHUD showMessage:@"取消收藏"];
                                       } failure:^(NSError *failure) {
                                           
                                       }];
    }else {
        [[MyNetWorking sharedInstance] PostUrl:BASEURL_WITHOBJC(@"article/collect")
                                        params:@{@"id": USERID, @"aid": self.cfDto.cfid,@"col_type":@"1"}
                                        target:self
                                       success:^(NSDictionary *success) {
                                           [MBProgressHUD showMessage:@"收藏成功"];
                                           
                                       } failure:^(NSError *failure) {
                                           
                                       }];
        
    }
    _isCollect = !_isCollect;
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    self.shareView.hidden = YES;
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.share_mini]]] ?: [UIImage imageNamed:@"cct"];
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle: @"全球村村通"  descr:@"——来自全球村村通APP" thumImage:image];
    //设置网页地址
    shareObject.webpageUrl = self.share_url;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}
#pragma mark - ShareListViewDelegate
- (void)itemClick:(UIButton *)sender {
    switch (sender.tag - 3000) {
        case 0: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_WechatSession];
        }
        break;
        case 1: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine];
        }
        break;
        case 2: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_QQ];
        }
        break;
        case 3: {
            [self shareWebPageToPlatformType:UMSocialPlatformType_Qzone];
        }
            break;
        case 4: {
            [self collection];
        }
            break;
        default:
            break;
    }
}

@end
