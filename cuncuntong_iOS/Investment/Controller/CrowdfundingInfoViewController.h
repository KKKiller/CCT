//
//  CrowdfundingInfoViewController.h
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"
#import "CrowdfundingSimpleModel.h"
#import "CrowdfundingInfoModel.h"
/**
 Description 众投 详情
 */
@interface CrowdfundingInfoViewController : BaseViewController

/**
 Description 众投 列表 数据
 */
@property(nonatomic,copy)CrowdfundingSimpleModel *cfDto;

/**
 Description 众投 详情 数据
 */
@property(nonatomic,strong)CrowdfundingInfoModel *cfInfoDto;
@end
