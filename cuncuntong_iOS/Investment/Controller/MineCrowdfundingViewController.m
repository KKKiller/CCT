//
//  MineCrowdfundingViewController.m
//  cuncuntong_iOS
//
//  Created by HAORUN on 2017/4/4.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "MineCrowdfundingViewController.h"
#import "MineCrowdfundingTableViewCell.h"
#import "CrowdfundingSimpleModel.h"

static NSString * const CellId = @"CEllID";
@interface MineCrowdfundingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger Page;
}

@property(nonatomic,strong)UITableView *tableView;
@property (nonatomic, strong) CCEmptyView *emptyView;
@property(nonatomic,strong)NSMutableArray *array_logs;
@property(nonatomic,strong)NSMutableDictionary * dictionary_zcs;

/**
 存放 数据 dto
 */
@property(nonatomic,strong)NSMutableArray *array_dto;
@end

@implementation MineCrowdfundingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addTitle:@"我的众投"];
    [self addReturnBtn:@"tui_"];
    
    [self.view addSubview:self.tableView];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.array_dto removeAllObjects];
        Page = 1;
        [self getMineCrowdfundingData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        Page += 1;
        [self getMineCrowdfundingData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 获取 我的众投
 */
-(void)getMineCrowdfundingData{
    NSDictionary *paramDic = @{@"rid":USERID};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:BASEURL_WITHOBJC(@"zc/logs") parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success = %@",responseObject);
        if ([[responseObject class]isSubclassOfClass:[NSDictionary class]]) {
            NSDictionary *dictionary = (NSDictionary *)responseObject;
            if ([[dictionary objectForKey:@"msg"]isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dic_msg = [dictionary objectForKey:@"msg"];
                if ([[dic_msg objectForKey:@"logs"] isKindOfClass:[NSArray class]]) {
                    NSArray *logs = [dic_msg objectForKey:@"logs"];
                    self.array_logs = [NSMutableArray arrayWithArray:logs];
                }
                if ([[dic_msg objectForKey:@"zcs"]isKindOfClass:[NSDictionary class]]) {
                    self.dictionary_zcs = [[NSMutableDictionary alloc]initWithDictionary:[dic_msg objectForKey:@"zcs"]];
                }
            }
        }
        self.emptyView.hidden = self.array_logs.count != 0;
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - uitableview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array_logs.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MineCrowdfundingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    if (self.array_logs.count>0) {
        NSDictionary *dictionary = [self.array_logs objectAtIndex:indexPath.row];
        cell.dictionary_logs = dictionary;
        NSString *zcid = [dictionary objectForKey:@"zc_id"];
//        NSLog(@"%@",self.dictionary_zcs);
//        NSLog(@"%@",[self.dictionary_zcs objectForKey:[NSString stringWithFormat:@"%@",zcid]]);
        cell.dictionary_zc = [self.dictionary_zcs objectForKey:[NSString stringWithFormat:@"%@",zcid]];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark  - setters and getters
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, KEY_WIDTH, KEY_HEIGHT - 64) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[MineCrowdfundingTableViewCell class] forCellReuseIdentifier:CellId];
    }
    return _tableView;
}

-(NSMutableArray *)array_dto{
    if (!_array_dto) {
        _array_dto = [NSMutableArray new];
    }
    return _array_dto;
}

-(NSMutableArray *)array_logs{
    if (!_array_logs) {
        _array_logs = [NSMutableArray new];
    }
    return _array_logs;
}
-(NSMutableDictionary *)dictionary_zcs{
    if (!_dictionary_zcs) {
        _dictionary_zcs = [NSMutableDictionary new];
    }
    return _dictionary_zcs;
}
- (CCEmptyView *)emptyView {
    if (_emptyView == nil) {
        _emptyView = [[CCEmptyView alloc]initWithFrame:self.tableView.bounds];
        [self.tableView addSubview:_emptyView];
    }
    return _emptyView;
}
@end
