//
//  CrowdfundingViewController.m
//  cuncuntong_iOS
//
//  Created by i棠_熊 on 2017/4/2.
//  Copyright © 2017年 zhushuai. All rights reserved.
//

#import "CrowdfundingViewController.h"

#import "Define.h"
#import "CrowdfundingSimpleModel.h"
#import "ZYBannerView.h"

#import "CrowdfuningTableViewCell.h"
#import "CrowdfundingInfoViewController.h"
#import "CCWineTabbarController.h"
static NSString * const ztTableViewCellid = @"ztTableViewListCellId";

@interface CrowdfundingViewController ()<UITableViewDelegate,UITableViewDataSource,ZYBannerViewDelegate,ZYBannerViewDataSource>
{
    NSInteger Page;
}
@property (nonatomic, strong) UIView *header;
@property (nonatomic, strong) ZYBannerView *banner;

@end

@implementation CrowdfundingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self loadCacheData];
    [self refreshData];
    [NOTICENTER addObserver:self selector:@selector(refreshData) name:UIApplicationWillEnterForegroundNotification object:nil];
}
- (void)refreshData {
    Page = 1;
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.banner startTimer];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.banner stopTimer];
}
- (void)setUI{
    self.view.backgroundColor = [UIColor whiteColor];
//    [self.navigationController.navigationBar setBarTintColor:RGB(54, 128, 255)];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName : [UIFont systemFontOfSize:18]}];
//    [self addTitle:@"众投"];
    self.navigationItem.title = @"3+N";
//    if (!self.existBackBtn) {
//        [self hideNavLeftButton];
//    }
    [self.view addSubview:self.tableview_list];
    WEAKSELF
    self.tableview_list.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf.array_ztdto removeAllObjects];
        Page = 1;
        [weakSelf loadData];
    }];
    self.tableview_list.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        Page += 1;
        [weakSelf loadData];
    }];
}
- (void)loadCacheData {
    [self setData:[TOOL getCachaDataWithName:@"zhongtou"]];
}

- (void)backMove{
    [App_Delegate.tabbarVc gotoHome];
}
- (void)btnClick:(UIButton *)sender {
    NSInteger tag = sender.tag - 100;
    if (tag == 0) {
        CCWineTabbarController *vc = [[CCWineTabbarController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if(tag == 1){
        CCWineTabbarController *vc = [[CCWineTabbarController alloc]init];
        [vc setNavTitle:@"阿罗拉"];
        [self.navigationController pushViewController:vc animated:YES];

    }else{
        SHOW(@"项目规划中，敬请期待！");
        
    }
}
#pragma mark - zc  url
-(void)loadData{
    NSDictionary *paramDic = @{@"page":[NSString stringWithFormat:@"%li",(long)Page]};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:BASEURL_WITHOBJC(@"zc/list") parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (Page == 1) {
            [self.array_ztdto removeAllObjects];
        }
        [self setData:responseObject];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [TOOL saveDataWithData:responseObject Name:@"zhongtou"];
        });
        [self.tableview_list.mj_header endRefreshing];
        [self.tableview_list.mj_footer endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self.tableview_list.mj_header endRefreshing];
        [self.tableview_list.mj_footer endRefreshing];
    }];
}


- (void)setData:(NSDictionary *)responseObject {
    if ([[responseObject class]isSubclassOfClass:[NSDictionary class]]) {
        NSDictionary *dictionary = (NSDictionary *)responseObject;
        NSArray *array = [dictionary objectForKey:@"msg"];
        for (NSDictionary *dic in array) {
            CrowdfundingSimpleModel * dto = [CrowdfundingSimpleModel modelWithDictionary:dic];
            [self.array_ztdto addObject:dto];
        }
        [self.tableview_list reloadData];
    }
}
#pragma mark -UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array_ztdto.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CrowdfuningTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ztTableViewCellid];
    cell.backgroundColor = [UIColor whiteColor];
    
    if (self.array_ztdto.count > 0) {
        cell.cfdto = [self.array_ztdto objectAtIndex:indexPath.row];
    }
    return cell;
}
#pragma mark -UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (!ISLOGIN) {
        SHOW(@"请登录后查看");
        return;
    }
    CrowdfundingInfoViewController *vc = [[CrowdfundingInfoViewController alloc]init];
    vc.cfDto = [self.array_ztdto objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)dealloc {
    [NOTICENTER removeObserver:self];
}
#pragma mark - banner
- (NSInteger)numberOfItemsInBanner:(ZYBannerView *)banner {
    return 2;
}
- (UIView *)banner:(ZYBannerView *)banner viewForItemAtIndex:(NSInteger)index {
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, App_Width, (150)*KEY_RATE) ];
    imgV.contentMode = UIViewContentModeScaleToFill;
    imgV.layer.masksToBounds = YES;
    imgV.image = index == 0 ? IMAGENAMED(@"header1") : IMAGENAMED(@"header2");
    return imgV;
}
- (ZYBannerView *)banner {
    if (!_banner) {
        _banner = [[ZYBannerView alloc]initWithFrame:CGRectMake(0, 0, App_Width, (150)*KEY_RATE) ];
        _banner.dataSource = self;
        _banner.autoScroll = YES;
        _banner.shouldLoop = YES;
    }
    return _banner;
}
#pragma mark - setters and getters
-(NSMutableArray *)array_ztdto{
    if (!_array_ztdto) {
        _array_ztdto = [[NSMutableArray alloc]init];
    }
    return _array_ztdto;
}

-(UITableView *)tableview_list{
    if (!_tableview_list) {
        _tableview_list = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-64) style:UITableViewStyleGrouped];
        _tableview_list.backgroundColor = [UIColor clearColor];
        _tableview_list.tableHeaderView = self.header;
        _tableview_list.delegate = self;
        _tableview_list.dataSource = self;
        [_tableview_list registerClass:[CrowdfuningTableViewCell class] forCellReuseIdentifier:ztTableViewCellid];
    }
    return _tableview_list;
}

//-(UIImageView *)imageview_header{
//    if (!_imageview_header) {
//        _imageview_header = [[UIImageView alloc]init];
//        _imageview_header.image = [UIImage imageNamed:@"cf_header"];
//        _imageview_header.frame = CGRectMake(0, 0, KEY_WIDTH, (150)*KEY_RATE);
//        _imageview_header.backgroundColor = [UIColor grayColor];
//    }
//    return _imageview_header;
//}
- (UIView *)header {
    if (!_header) {
        CGFloat width = 50;
        CGFloat margin = (App_Width - width * 3 ) / 4;
        _header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KEY_WIDTH, (150 + width + 60)*KEY_RATE)];
        [_header addSubview:self.banner];
        for (int i =0; i<3; i++) {
            
            UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(margin + (margin +  width)*i,   (150+15)*KEY_RATE, width, width)];
            NSString *str = [NSString stringWithFormat:@"crowdfunding_%@",@(i)];
            [btn setImage:IMAGENAMED(str) forState:UIControlStateNormal];
            btn.layer.cornerRadius = width * 0.5;
            btn.layer.masksToBounds = YES;
            btn.tag = 100 + i;
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [_header addSubview:btn];
            NSString *text = i == 0 ? @"全民酒业" : i== 1 ? @"阿罗拉": @"生态庄园";
            UILabel *title = [[UILabel alloc]initWithText:text font:14 textColor:TEXTBLACK3];
            title.textAlignment = NSTextAlignmentCenter;
            [_header addSubview:title];
            [title mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(btn.mas_bottom).offset(5);
                make.centerX.equalTo(btn.mas_centerX);
                make.width.mas_equalTo(60);
                make.height.mas_equalTo(17);
            }];
        }
    }
    return _header;
}
@end
