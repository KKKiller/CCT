//
//  ZcPayViewController.m
//  UIDesignViewProject
//
//  Created by HAORUN on 2017/4/5.
//  Copyright © 2017年 HAORUN. All rights reserved.
//

#import "ZcPayViewController.h"
#import "RechargeViewController.h"
#import "CCPayController.h"
#import "Define.h"

@interface ZcPayViewController ()<UITextFieldDelegate,UIAlertViewDelegate>

@end

@implementation ZcPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.view.backgroundColor = [UIColor whiteColor];
//    self.view.alpha = 0.5;
    UIColor *color = [UIColor blackColor];
    self.view.backgroundColor = [color colorWithAlphaComponent:0.5];
    
    self.view.frame = [UIScreen mainScreen].bounds;
    
    UIView *v = [[UIView alloc]initWithFrame:self.view.frame];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(TapView)];
    [v addGestureRecognizer:tap];
    [self.view addSubview:v];

    [self.view addSubview:self.view_bottom];
    
    [self.view_bottom addSubview:self.button_pay];
    [self.view_bottom addSubview:self.img_zc];
    [self.view_bottom addSubview:self.label_title];
    [self.view_bottom addSubview:self.label_money];
    [self.view_bottom addSubview:self.label_num];
    [self.view_bottom addSubview:self.textField_num];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;

}
-(void)TapView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 购买 按钮
 */
-(void)buttonClickPay{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSDictionary *pardic = @{@"rid":USERID,@"id":[NSString stringWithFormat:@"%@",self.cfdto.cfid],@"num":[NSString stringWithFormat:@"%@",self.textField_num.text]};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:BASEURL_WITHOBJC(@"zc/buy") parameters:pardic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[responseObject class]isSubclassOfClass:[NSDictionary class]]) {
            NSDictionary *dictionary = (NSDictionary *)responseObject;
            if ([[dictionary objectForKey:@"code"] boolValue]) { //支付出错 
                NSString *msg = [dictionary objectForKey:@"msg"];
                SHOW(msg);
                if([msg isEqualToString:@"余额不足，请充值"]){
                    RechargeViewController *vc = [[RechargeViewController alloc]init];
                    [self.navigationController pushViewController:vc animated:YES];
//                    CCPayController *vc =[[ CCPayController alloc]init];
//                    vc.money = self.price;
//                    vc.paySuccessBlock = ^{
//                        [self pay];
//                    };
//                    [self.navigationController pushViewController:vc animated:YES];
                }
            }else{
                [MBProgressHUD showMessage:@"支付成功！"];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:@"支付失败！"];
    }];
}

-(void)buttonClick:(UIButton *)sender{
    NSInteger nu = [self.textField_num.text integerValue];
    if (sender.tag == 101) {
        if (nu > 0) {
            nu --;
        }
    }else if(sender.tag == 102){
        nu ++;
    }
    
    self.textField_num.text = [NSString stringWithFormat:@"%li",nu];
}
-(UIButton *)buttonByTitle:(NSString *)title andTag:(NSInteger)tag{
    UIButton *bt = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    bt.frame = CGRectMake(0, 0, 50, CGRectGetHeight(self.textField_num.frame));
    bt.backgroundColor = [UIColor whiteColor];
    [bt setTitle:title forState:UIControlStateNormal];
    [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    bt.titleLabel.font = [UIFont systemFontOfSize:20];
    [bt addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    bt.tag = tag;
    bt.layer.masksToBounds = YES;
    bt.layer.borderWidth = 1;
    bt.layer.borderColor = [UIColor grayColor].CGColor;
    return bt;
}
#pragma mark - uialertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        self.navigationController.navigationBar.hidden = NO;
        RechargeViewController *vc = [[RechargeViewController alloc] init];
//        UINavigationController *nacVc = [[UINavigationController alloc]initWithRootViewController:vc];
//        [self presentViewController:nacVc animated:YES completion:nil];
         [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}
#pragma mark - setters and getters

-(void)setCfdto:(CrowdfundingSimpleModel *)cfdto{
    _cfdto = cfdto;
    [self.img_zc setImageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_cfdto.mini]]];
    
    self.label_title.text = [NSString stringWithFormat:@"%@",_cfdto.title];
    self.label_money.text = [NSString stringWithFormat:@"¥ %@",_cfdto.money];
}

-(UIView *)view_bottom{
    if (!_view_bottom) {
        _view_bottom = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 200, CGRectGetWidth(self.view.frame), 200)];
        _view_bottom.backgroundColor = [UIColor whiteColor];
    }
    return _view_bottom;
}

-(UILabel *)label_title{
    if (!_label_title) {
        _label_title = [[UILabel alloc]init];
        _label_title.font = [UIFont systemFontOfSize:14];
        _label_title.numberOfLines = 0;
        _label_title.frame = CGRectMake(CGRectGetMaxX(self.img_zc.frame)+10, CGRectGetMinY(self.img_zc.frame)+5, CGRectGetWidth(self.view.frame) - CGRectGetMaxX(self.img_zc.frame) - 20, 35);
    }
    return _label_title;
}

-(UILabel *)label_money{
    if (!_label_money) {
        _label_money = [[UILabel alloc]init];
        _label_money.font = [UIFont systemFontOfSize:14];
        _label_money.numberOfLines = 0;
        _label_money.frame = CGRectMake(CGRectGetMaxX(self.img_zc.frame)+10, CGRectGetMaxY(self.img_zc.frame) - 30, CGRectGetWidth(self.view.frame) - CGRectGetMaxX(self.img_zc.frame) - 20, 25);
        _label_money.textColor = [UIColor redColor];
    }
    return _label_money;
}

-(UILabel *)label_num{
    if (!_label_num) {
        _label_num = [[UILabel alloc]init];
        _label_num.font = [UIFont systemFontOfSize:14];
        _label_num.numberOfLines = 0;
        _label_num.frame = CGRectMake(CGRectGetMinX(self.img_zc.frame), CGRectGetMaxY(self.img_zc.frame)+5, 80, 25);
        _label_num.text = @"支持份数";
        _label_num.textColor = [UIColor grayColor];
    }
    return _label_num;
}

-(UIImageView *)img_zc{
    if (!_img_zc) {
        _img_zc = [[UIImageView alloc]initWithFrame:CGRectMake(30, 20, 80, 80)];
    }
    return _img_zc;
}
-(UIButton *)button_pay{
    if (!_button_pay) {
        _button_pay = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _button_pay.frame = CGRectMake(0, 150, CGRectGetWidth(self.view.frame), 50);
        _button_pay.backgroundColor = [UIColor colorWithRed:84/255.0f green:187/255.0f blue:247/255.0f alpha:1.0f];//[UIColor blueColor];
        [_button_pay setTitle:@"支付" forState:UIControlStateNormal];
        [_button_pay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button_pay addTarget:self action:@selector(buttonClickPay) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_pay;
}

-(UITextField *)textField_num{
    if (!_textField_num) {
        _textField_num = [[UITextField alloc]init];
//        _textField_num.userInteractionEnabled = NO;
        _textField_num.delegate = self;
        _textField_num.frame = CGRectMake(CGRectGetMaxX(self.img_zc.frame) + 10, CGRectGetMaxY(self.img_zc.frame) + 5, CGRectGetWidth(self.view.frame) - CGRectGetMaxX(self.img_zc.frame) - 30, 25);
//        _textField_num.layer.borderColor = [UIColor blackColor].CGColor;
//        _textField_num.layer.borderWidth = 1;
        _textField_num.textAlignment = NSTextAlignmentCenter;
        _textField_num.text = @"0";
        _textField_num.leftView = [self buttonByTitle:@"-" andTag:101];
        _textField_num.leftViewMode = UITextFieldViewModeAlways;
        _textField_num.rightView = [self buttonByTitle:@"+" andTag:102];
        _textField_num.rightViewMode = UITextFieldViewModeAlways;
    }
    return _textField_num;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
