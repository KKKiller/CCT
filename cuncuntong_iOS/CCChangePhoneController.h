//
//  CCChangePhoneController.h
//  CunCunTong
//
//  Created by 周吾昆 on 2018/9/2.
//  Copyright © 2018年 zhushuai. All rights reserved.
//

#import "BaseViewController.h"

@interface CCChangePhoneController : BaseViewController
@property (nonatomic, copy) NSString *navTitle;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, assign) BOOL ali;//绑定支付宝流程

@end
