//
//  LTTMyRelease.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTMyRelease : NSObject

@property (nonatomic,copy)NSString *current_of_lottery;
@property (nonatomic,copy)NSString *dp_id;
@property (nonatomic,copy)NSString *energy_for_lucky_draw;
@property (nonatomic,copy)NSString *number_of_lottery;
@property (nonatomic,copy)NSString *prize_title;
@property (nonatomic,copy)NSString *prize_cover_picture;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *qi;
@property (nonatomic,copy)NSString *state;
@property (nonatomic,copy)NSString *is_audit;

@end


@interface LTTMyReleaseItem : NSObject
//prize_title 开奖活动标题，
//prize_cover_picture 开奖活动封面图，
//number_of_lottery 开奖活动总需要人数，
//current_of_lottery 当前参与人数，
//energy_for_lucky_draw 开奖所需能量，
//lottery_numbers 开奖号码，
//reader_name 中奖人姓名，
// 中奖人头像，

@property (nonatomic,copy)NSString *portrait;
@property (nonatomic,copy)NSString *prize_cover_picture;
/// 当前人次
@property (nonatomic,copy)NSString *current_of_lottery;
/// （抽奖所需能量 1-25克，2-50克，3-75克，4-100克）
@property (nonatomic,copy)NSString *energy_for_lucky_draw;
/// 期ID （该抽奖产品是第几期
@property (nonatomic,copy)NSString *jackpot_id;
/// 开奖号码
@property (nonatomic,copy)NSString *lottery_numbers;
/// 开奖时间
@property (nonatomic,copy)NSString *lottery_time;
/// 总参与人次
@property (nonatomic,copy)NSString *number_of_lottery;
/// 抽奖产品标题名称
@property (nonatomic,copy)NSString *prize_title;
/// state 4代表已完成（发货）
@property (nonatomic,copy)NSString *status;
/// 物流公司名称
@property (nonatomic,copy)NSString *courier_comp_name;
/// 物流单号
@property (nonatomic,copy)NSString *courier_sn;
/// 中奖用户名
@property (nonatomic,copy)NSString *reader_name;
/// 电话
@property (nonatomic,copy)NSString *mobile;
/// 地址
@property (nonatomic,copy)NSString *address;
/// 用户ID
@property (nonatomic,copy)NSString *reader_id;

@property (nonatomic,copy)NSString *reader_address;

@property (nonatomic,copy)NSString *is_delivery;

@property (nonatomic,copy)NSString *is_confirm;

@property (nonatomic,copy)NSString * consignee_address;
@property (nonatomic,copy)NSString * consignee_mobile;
@property (nonatomic,copy)NSString * consignee_name;



- (NSArray<NSString *>*)numbers;

- (BOOL)isPrize;

- (BOOL)isUser;

- (BOOL)isExpressdDelivery;

@end


NS_ASSUME_NONNULL_END
