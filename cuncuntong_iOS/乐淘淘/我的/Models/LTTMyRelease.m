//
//  LTTMyRelease.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMyRelease.h"
#import "NSString+SubString.h"
@implementation LTTMyRelease

+ (NSDictionary *)modelCustomPropertyMapper{
    
    return @{@"ID":@[@"id"]};
}

@end

@interface LTTMyReleaseItem (){
    
    NSArray *items;
}

@end

@implementation LTTMyReleaseItem


- (BOOL)isPrize{
    if ([self.lottery_numbers isEqual:[NSNull class]]) { return NO; }
    return self.lottery_numbers.length > 0;
}
- (BOOL)isUser{
    
    if ([self.reader_id isEqual:[NSNull class]]) { return NO; }
    
    return self.reader_id.length > 0;
}

- (void)setLottery_numbers:(NSString *)lottery_numbers{
    
    _lottery_numbers = [NSString stringWithFormat:@"%6ld",(long)[lottery_numbers integerValue]];
    
    
    items = _lottery_numbers.substrings;
        
}

- (NSArray<NSString *>*)numbers{
    
    return items;
}



@end
