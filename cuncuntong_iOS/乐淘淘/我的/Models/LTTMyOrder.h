//
//  LTTMyOrder.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTMyOrder : NSObject

/// 参与人次
@property(nonatomic,copy)NSString * current_of_lottery;
/// 是否中奖
@property(nonatomic,copy)NSString * is_winning;
/// 开榜号码
@property(nonatomic,copy)NSString * lottery_numbers;
/// 下单号码
@property(nonatomic,copy)NSString * number;
/// 开奖封面图
@property(nonatomic,copy)NSString * prize_cover_picture;
/// 奖品名称标题
@property(nonatomic,copy)NSString * prize_title;

@property (nonatomic,copy)NSString *now_qi;

@end

NS_ASSUME_NONNULL_END
