//
//  LTTMyPrize.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTMyPrize : NSObject

/// 当前人次
@property (nonatomic,copy)NSString * current_of_lottery ;
/// （抽奖所需能量 1-25克，2-50克，3-75克，4-100克）
@property (nonatomic,copy)NSString * energy_for_lucky_draw;
/// 期ID （该抽奖产品是第几期）
@property (nonatomic,copy)NSString * ID;
/// 开奖号码
@property (nonatomic,copy)NSString * lottery_numbers;
///  中奖人选择的号码
@property (nonatomic,copy)NSString * number;
/// 总参与人次
@property (nonatomic,copy)NSString * number_of_lottery;
/// 开奖封面图
@property (nonatomic,copy)NSString * prize_cover_picture;
/// 抽奖产品标题名称
@property (nonatomic,copy)NSString * prize_title;
/// 中奖人联系地址
@property (nonatomic,copy)NSString * reader_address;
/// 中奖人 di
@property (nonatomic,copy)NSString * reader_id;
/// 中奖人电话
@property (nonatomic,copy)NSString * reader_mobile;
/// 中奖用户名
@property (nonatomic,copy)NSString * reader_name;
/// 状态  1/进行中  2/倒计时中 3/开奖中  4/已完成
@property (nonatomic,copy)NSString * state;

@property (nonatomic,readonly)NSString *energy;

@property (nonatomic,readonly)NSArray <NSString *>*numbers;

@property (nonatomic,copy)NSString *now_id;

/// 物流公司名字
@property (nonatomic,copy)NSString *courier_comp_name;
/// 物流单号
@property (nonatomic,copy)NSString *courier_sn;
/// 是否发货
@property (nonatomic,copy)NSString *is_delivery;

@property (nonatomic,copy)NSString *fb_reader_mobile;

@property (nonatomic,copy)NSString * fb_reader_name;

@property (nonatomic,copy)NSString *is_confirm;

@property (nonatomic,copy)NSString *q_id;

@property (nonatomic,copy)NSString *consignee_address;
@property (nonatomic,copy)NSString *consignee_mobile;
@property (nonatomic,copy)NSString *consignee_name;
@property (nonatomic,copy)NSString *now_qi;

@end

NS_ASSUME_NONNULL_END
