//
//  LTTMyPrize.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMyPrize.h"
#import "NSString+SubString.h"
@interface LTTMyPrize (){
    
    NSArray *items;
    
}


@end

@implementation LTTMyPrize


+ (NSDictionary *)modelCustomPropertyMapper{
    
    return @{@"ID":@[@"id"]};
}
- (NSString *)energy{
    
    switch ([self.energy_for_lucky_draw intValue]) {
        case 1:
            return @"25";
            break;
        case 2:
            return @"50";
        case 3:
            return @"75";
        case 4:
            return @"100";
        default:
            return @"未知";
            break;
    }
}

- (void)setLottery_numbers:(NSString *)lottery_numbers{
    
    _lottery_numbers = [NSString stringWithFormat:@"%6ld",(long)[lottery_numbers integerValue]];
    
    
    items = _lottery_numbers.substrings;
        
}

- (NSArray<NSString *>*)numbers{
    
    return items;
}


@end
