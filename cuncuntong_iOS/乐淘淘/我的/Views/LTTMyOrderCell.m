//
//  LTTMyOrderCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMyOrderCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTMyOrderCell()
@property (weak, nonatomic) IBOutlet UIImageView *g_imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberlabel;
@property (weak, nonatomic) IBOutlet UILabel *lotteryNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *periodLabel;

@end

@implementation LTTMyOrderCell


- (void)setItem:(LTTMyOrder *)item{
    _item = item;
    
    [self.g_imageView sd_setImageWithURL:LTTImgaeURL(item.prize_cover_picture)];
    
    self.titleLabel.text = item.prize_title;
    
    self.orderNumberlabel.text = [NSString stringWithFormat:@"下单单号:%@",item.number];
    
    self.periodLabel.text = [NSString stringWithFormat:@"第%@期",item.now_qi];
    
    if (item.lottery_numbers) {
        
        NSMutableAttributedString *attribued = [[NSMutableAttributedString alloc]initWithString:@"开榜单号:" attributes:@{
               NSForegroundColorAttributeName:HEXCOLOR(0x313131),
               NSFontAttributeName:[UIFont systemFontOfSize:13]
           }];
        
        [attribued appendAttributedString:[[NSAttributedString alloc]initWithString:item.lottery_numbers attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0xee716a),
        NSFontAttributeName:[UIFont systemFontOfSize:13]}]];
        
          self.lotteryNumberLabel.attributedText = attribued;
    }else{
        self.lotteryNumberLabel.text = @"未开奖";
    }

    if([item.is_winning intValue] == 1){
        // 中奖
        
    }else{
        // 未中奖
        
    }
    
    
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
