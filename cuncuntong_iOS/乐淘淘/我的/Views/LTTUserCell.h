//
//  LTTUserCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTMyRelease.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTUserCell : UITableViewCell

@property(nonatomic,strong)LTTMyReleaseItem *item;


@end

NS_ASSUME_NONNULL_END
