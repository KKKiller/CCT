//
//  LTTAddressHeaderView.h
//  CunCunTong
//
//  Created by W&Z on 2020/1/6.
//  Copyright © 2020 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCAddressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTAddressHeaderView : UIView
@property (weak, nonatomic) IBOutlet UITextField *phoneField;

@property (weak, nonatomic) IBOutlet UITextField *addressField;
@property (weak, nonatomic) IBOutlet UITextField *addressNameField;
@property (weak, nonatomic) IBOutlet UITextField *nicknameField;
@property (weak, nonatomic) IBOutlet UIButton *locateBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
+ (instancetype)instanceView;

@property (nonatomic, strong) CCAddressModel *model;
@end

NS_ASSUME_NONNULL_END
