//
//  LTTPrizeCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPrizeCell.h"
#import "LTTPrizeInfoCell.h"
#import "LTTWinnerCell.h"
#import "LTTNumberCell.h"
#import "LTTCustomCell.h"
@interface LTTPrizeCell ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LTTPrizeCell

- (void)setItem:(LTTMyPrize *)item{
    _item = item;
    
    [self.tableView reloadData];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTPrizeInfoCell" bundle:nil] forCellReuseIdentifier:@"Cell1"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTWinnerCell" bundle:nil] forCellReuseIdentifier:@"Cell2"];
    
    //    [self.tableView registerNib:[UINib nibWithNibName:@"LTTNumberCell" bundle:nil] forCellReuseIdentifier:@"Cell4"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
    // Configure the view for the selected state
}


- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority verticalFittingPriority:(UILayoutPriority)verticalFittingPriority{
    
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
    
    targetSize.height = self.tableView.contentSize.height;
    
    return targetSize;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.item == nil ? 0 : 1 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            
            LTTPrizeInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
            
            cell.item = self.item;
            
            return cell;
        }
        case 1:{
            LTTWinnerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell2" forIndexPath:indexPath];
            
            cell.item = self.item;
            
            cell.changeAddress.hidden = [self.item.is_delivery intValue] == 1;

            [cell.changeAddress addTarget:self action:@selector(changeAddress) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
            //            LTTNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell4" forIndexPath:indexPath];
            //
            //            cell.items = self.item.numbers;
            //
            //            return cell;
            
        }
        case 2:{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell4"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell4"];
            }
            
            cell.textLabel.text = [NSString stringWithFormat:@"发布人:%@",self.item.fb_reader_name];
            
            cell.detailTextLabel.text = [NSString stringWithFormat:@"发布人电话:%@",self.item.fb_reader_mobile];
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
            
            
            
        case 3:{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell3"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell3"];
            }
            
            if ([self.item.is_delivery isEqualToString:@"1"]) {
                cell.textLabel.text =[NSString stringWithFormat:@"%@ %@",self.item.courier_comp_name,self.item.courier_sn];
                cell.detailTextLabel.text = [self.item.is_confirm intValue] == 0 ? @"未签收" : @"已签收";
                cell.textLabel.textColor = HEXCOLOR(0x313131);
                
                if ([self.item.is_confirm intValue] == 0) {
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.textLabel.userInteractionEnabled = YES;
                    [cell.textLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)]];
                    
                }else {
                  
                    for (UIGestureRecognizer *tap in cell.textLabel.gestureRecognizers) {
                        [cell.textLabel removeGestureRecognizer:tap];
                    }
                    
                    cell.accessoryType = UITableViewScrollPositionNone;
                }
                
            }else{
                cell.textLabel.text = @"未发货";
                
                cell.textLabel.textColor = HEXCOLOR(0xe6554d);
                cell.accessoryType = UITableViewScrollPositionNone;
                cell.detailTextLabel.text = nil;
                
            }
            
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }
            
            
        default:
            break;
    }
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 3 && [self.item.is_delivery isEqualToString:@"1"] && [self.item.is_confirm intValue] == 0) {
        
        if ([self.delegate respondsToSelector:@selector(confirm:)]) {
            [self.delegate confirm:self.item];
        }
    }
}

- (void)tap:(UITapGestureRecognizer *)tap{
    
    if ([self.delegate respondsToSelector:@selector(kuaididanhao:)]) {
        [self.delegate kuaididanhao:self.item.courier_sn];
    }
    
}


- (void)changeAddress{
    
    if ([self.delegate respondsToSelector:@selector(changeAddressWith:)]) {
        [self.delegate changeAddressWith:self.item];
    }
    
}



@end
