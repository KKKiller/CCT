//
//  LTTCustomCell.h
//  CunCunTong
//
//  Created by W&Z on 2020/1/8.
//  Copyright © 2020 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTMyPrize.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTCustomCell : UITableViewCell


@property (nonatomic,strong)LTTMyPrize *item;

@end

NS_ASSUME_NONNULL_END
