//
//  LTTPrizeInfoCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/25.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPrizeInfoCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LTTPhaseView.h"
@interface LTTPrizeInfoCell ()

@property (weak, nonatomic) IBOutlet UILabel *dateNumber;
@property (weak, nonatomic) IBOutlet UIImageView *g_imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet LTTPhaseView *phaseView;



@end

@implementation LTTPrizeInfoCell


- (void)setItem:(LTTMyPrize *)item{
    
    _item = item;
    
    self.dateNumber.text = [NSString stringWithFormat:@"第%@期",item.now_qi];
    
    self.titleLabel.text = item.prize_title;
    self.energyLabel.text = [[NSString alloc]initWithFormat:@"所需能量%@克",item.energy];
    [self.g_imageView sd_setImageWithURL:LTTImgaeURL(item.prize_cover_picture)];
    
    self.phaseView.current =[item.current_of_lottery intValue];
    
    self.phaseView.total = [item.number_of_lottery intValue];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority verticalFittingPriority:(UILayoutPriority)verticalFittingPriority{
    
    targetSize.height = 165;
    
    return targetSize;
}




@end
