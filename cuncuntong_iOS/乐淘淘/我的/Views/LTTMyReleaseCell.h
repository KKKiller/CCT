//
//  LTTMyReleaseCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTMyRelease.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTMyReleaseCell : UITableViewCell

@property (nonatomic,strong)LTTMyRelease *item;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmented;

@end

NS_ASSUME_NONNULL_END
