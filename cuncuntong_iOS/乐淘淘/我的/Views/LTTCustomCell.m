//
//  LTTCustomCell.m
//  CunCunTong
//
//  Created by W&Z on 2020/1/8.
//  Copyright © 2020 zhushuai. All rights reserved.
//

#import "LTTCustomCell.h"


@interface LTTCustomCell ()
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@end

@implementation LTTCustomCell

- (void)setItem:(LTTMyPrize *)item{
    
    _item = item;
    
    if ([item.is_delivery isEqualToString:@"1"]) {
        
        [self.leftButton setTitle:[NSString stringWithFormat:@"%@ %@",self.item.courier_comp_name,self.item.courier_sn] forState:UIControlStateNormal];
        [self.rightButton setTitle:[item.is_confirm intValue] == 0 ? @"未签收" : @"已签收" forState:UIControlStateNormal];
        [self.leftButton setTitleColor:HEXCOLOR(0x313131) forState:UIControlStateNormal];
        
        if ([item.is_confirm intValue] == 0) {
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
    }
    
//
//    if ([self.item.is_delivery isEqualToString:@"1"]) {
//
//
//
//                  if ([self.item.is_confirm intValue] == 0) {
//
//                      cell.userInteractionEnabled = YES;
//                      [cell.textLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)]];
//
//                  }else {
//
//                      for (UIGestureRecognizer *tap in cell.textLabel.gestureRecognizers) {
//                          [cell.textLabel removeGestureRecognizer:tap];
//                      }
//
//                      cell.accessoryType = UITableViewScrollPositionNone;
//                  }
//
//              }else{
//                  cell.textLabel.text = @"未发货";
//
//                  cell.textLabel.textColor = HEXCOLOR(0xe6554d);
//                  cell.accessoryType = UITableViewScrollPositionNone;
//                  cell.detailTextLabel.text = nil;
//
//              }
//
//              cell.textLabel.font = [UIFont systemFontOfSize:15];
//
//              cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)leftEventAction:(UIButton *)sender {
}
- (IBAction)rightEventAction:(UIButton *)sender {
}

@end
