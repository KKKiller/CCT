//
//  LTTUserCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTUserCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTUserCell()
@property (weak, nonatomic) IBOutlet UIImageView *h_imageView;
@property (weak, nonatomic) IBOutlet UILabel *nickname;
@property (weak, nonatomic) IBOutlet UILabel *mobile;
@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation LTTUserCell

- (void)setItem:(LTTMyReleaseItem *)item{
    _item = item;
    
    [self.h_imageView sd_setImageWithURL:LTTImgaeURL(item.portrait)];
    self.nickname.text = [NSString stringWithFormat:@"中奖人:%@",item.reader_name];
    self.mobile.text = [NSString stringWithFormat:@"电话:%@",item.mobile];
    self.address.text = [NSString stringWithFormat:@"地址:%@",item.address];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
