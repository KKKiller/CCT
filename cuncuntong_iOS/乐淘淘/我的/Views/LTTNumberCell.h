//
//  LTTNumberCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTNumberCell : UITableViewCell

@property (nonatomic,strong)NSArray<NSString *>*items;

@end

NS_ASSUME_NONNULL_END
