//
//  LTTPayView.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LTTPayView;
NS_ASSUME_NONNULL_BEGIN

@protocol PayDelegate <NSObject>

- (void)payWith:(LTTPayView *)view;

@end

IB_DESIGNABLE
@interface LTTPayView : UIView

@property (nonatomic,assign)IBInspectable NSInteger starvip;

@property (nonatomic,assign)NSInteger monthValue;

@property (nonatomic,assign)NSInteger level;

@property (nonatomic,weak)IBOutlet id<PayDelegate> delegate;

@property (nonatomic,assign)NSInteger days;

@end

NS_ASSUME_NONNULL_END
