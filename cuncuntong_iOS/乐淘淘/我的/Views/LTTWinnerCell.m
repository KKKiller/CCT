//
//  LTTWinnerCell.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTWinnerCell.h"
#import "LTTOrderNumberCell.h"
@interface LTTWinnerCell ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *winnerName;
@property (weak, nonatomic) IBOutlet UILabel *mobilePhone;
@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation LTTWinnerCell

- (void)setItem:(LTTMyPrize *)item{
    
    _item = item;
    
    self.winnerName.text = [NSString stringWithFormat:@"中奖人:%@",item.consignee_name];
    
    self.mobilePhone.text = [NSString stringWithFormat:@"联系电话:%@",item.consignee_mobile];
    
    self.address.text = [NSString stringWithFormat:@"联系地址:%@",item.consignee_address];
    
    [self.collectionView reloadData];
  
    
}

- (void)setReleaseItem:(LTTMyReleaseItem *)releaseItem{
    
    _releaseItem = releaseItem;
    
    self.winnerName.text = [NSString stringWithFormat:@"中奖人:%@",releaseItem.consignee_name];
    
    self.mobilePhone.text = [NSString stringWithFormat:@"联系电话:%@",releaseItem.consignee_mobile];
    
    self.address.text = [NSString stringWithFormat:@"联系地址:%@",releaseItem.consignee_address];
    [self.collectionView reloadData];
    
    
}




- (void)awakeFromNib {
    [super awakeFromNib];
    self.isRelease = NO;
    // Initialization code
    [self.collectionView registerClass:[DigitalTextCell class] forCellWithReuseIdentifier:@"Cell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (self.isRelease) { return self.releaseItem.numbers.count; }
    
    return self.item.numbers.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DigitalTextCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (self.isRelease) {
        cell.label.text = self.releaseItem.numbers[indexPath.row];
    }else{
        cell.label.text = self.item.numbers[indexPath.row];
    }
    
    cell.label.backgroundColor = HEXCOLOR(0xCCCCCC);
    
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(floor(44.0*0.9), 44.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 4.5;
}
@end

