//
//  LTTWinnerCell.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTMyPrize.h"
#import "LTTMyRelease.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTWinnerCell : UITableViewCell

@property (nonatomic,strong)LTTMyPrize *item;

@property (nonatomic,strong)LTTMyReleaseItem *releaseItem;

@property (nonatomic,assign)BOOL isRelease;

@property (weak, nonatomic) IBOutlet UIButton *changeAddress;


@end

NS_ASSUME_NONNULL_END
