//
//  LTTPrizeCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTMyPrize.h"
NS_ASSUME_NONNULL_BEGIN


@protocol LTTPrizeCellDelegate <NSObject>

- (void)confirm:(LTTMyPrize *)item;

- (void)changeAddressWith:(LTTMyPrize *)item;
- (void)kuaididanhao:(NSString *)danhao;

@end


@interface LTTPrizeCell : UITableViewCell

@property (nonatomic,strong)LTTMyPrize *item;


@property (nonatomic,weak)id<LTTPrizeCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
