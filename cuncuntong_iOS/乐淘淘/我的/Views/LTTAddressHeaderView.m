//
//  LTTAddressHeaderView.m
//  CunCunTong
//
//  Created by W&Z on 2020/1/6.
//  Copyright © 2020 zhushuai. All rights reserved.
//

#import "LTTAddressHeaderView.h"

@implementation LTTAddressHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (instancetype)instanceView {
    LTTAddressHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:[self className] owner:self options:nil] firstObject];
    view.saveBtn.layer.cornerRadius = 4;
    view.saveBtn.layer.masksToBounds = YES;
    return view;
}
- (void)setModel:(CCAddressModel *)model {
    _model = model;
    self.addressField.text = model.location;
    self.addressNameField.text = model.name;
}
@end
