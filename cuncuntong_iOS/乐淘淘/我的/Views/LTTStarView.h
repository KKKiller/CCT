//
//  LTTStarView.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE
@interface StarView : UIView

@end

IB_DESIGNABLE
@interface LTTStarView : UIView

@property (nonatomic,assign)IBInspectable NSInteger starCount;

@end

NS_ASSUME_NONNULL_END
