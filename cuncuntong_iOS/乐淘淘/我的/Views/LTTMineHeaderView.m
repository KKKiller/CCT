//
//  LTTMineHeaderView.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/23.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMineHeaderView.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTMineHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UIButton *level;
@property (weak, nonatomic) IBOutlet UIButton *orderCount;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLeftConstraint;


@end


@implementation LTTMineHeaderView


- (void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.buttonType = MineHeadrButtonTypeOrder;
    
    
    
    self.address.text = [Def valueForKey:@"locationText"];
    
    
}

- (void)setUserInfo:(NSDictionary *)userInfo{
    _userInfo = userInfo;
    
    [self.header sd_setImageWithURL:[NSURL URLWithString:userInfo[@"reader_head_portrait"]]];
    
    self.nickname.text = userInfo[@"realname"];
    self.address.text = userInfo[@"location"];
    
    [self.level setTitle:[NSString stringWithFormat:@"%@星",userInfo[@"reader_level"]] forState:UIControlStateNormal];
    [self.orderCount setTitle:[NSString stringWithFormat:@"下单%@手",userInfo[@"place_order_frequency"]] forState:UIControlStateNormal];
    
}

- (void)setButtonType:(MineHeadrButtonType)buttonType{
    _buttonType = buttonType;
    
    if ([self.delegate respondsToSelector:@selector(mineHeaderView:buttonType:)]) {
        [self.delegate mineHeaderView:self buttonType:buttonType];
    }
    
}




- (IBAction)upgradeEventAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(upgradeUser)]) {
        
        [self.delegate upgradeUser];
    }
    
}

- (IBAction)buttonsEvnetAction:(UIButton *)sender {
    
    self.lineLeftConstraint.constant = sender.frame.origin.x;

    [UIView animateWithDuration:0.35 animations:^{
        [self layoutIfNeeded];
    }];
    
    for (UIButton *button in self.buttons) {
        [button setSelected:NO];
    }
    
    [sender setSelected:YES];
    

    if ([sender.currentTitle isEqualToString:@"我的下单"]) {
        self.buttonType = MineHeadrButtonTypeOrder;
    }else if ([sender.currentTitle isEqualToString:@"我的发布"]){
        self.buttonType = MineHeadrButtonTypeRelease;
    }else if ([sender.currentTitle isEqualToString:@"我的奖品"]){
        self.buttonType = MineHeadrButtonTypePrize;
    }
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
