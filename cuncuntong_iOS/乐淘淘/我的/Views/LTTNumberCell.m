//
//  LTTNumberCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTNumberCell.h"

@interface LTTNumberCell ()
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *numbers;

@end

@implementation LTTNumberCell

- (void)setItems:(NSArray<NSString *> *)items{
    for (int i = 0; i < self.numbers.count; i++) {
        UILabel *label = self.numbers[i];
    
        label.text = items[i];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    for (int i = 0; i < self.numbers.count; i++) {
        UILabel *label = self.numbers[i];
    
        label.text = [NSString stringWithFormat:@"%d",i];
    }
        
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
