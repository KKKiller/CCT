//
//  LTTPrizeInfoCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/25.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTMyPrize.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTPrizeInfoCell : UITableViewCell

@property (nonatomic,strong)LTTMyPrize *item;

@end

NS_ASSUME_NONNULL_END
