//
//  LTTMineHeaderView.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/23.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LTTMineHeaderView;
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,MineHeadrButtonType) {
    
    MineHeadrButtonTypeOrder,
    MineHeadrButtonTypeRelease,
    MineHeadrButtonTypePrize
    
};


@protocol MineHeaderDelegate <NSObject>

- (void)mineHeaderView:(LTTMineHeaderView *)headerView buttonType:(MineHeadrButtonType)type;

- (void)upgradeUser;

@end



@interface LTTMineHeaderView : UIView

@property (nonatomic,assign)MineHeadrButtonType buttonType;

@property (nonatomic,weak)id<MineHeaderDelegate> delegate;

@property (nonatomic,strong)NSDictionary *userInfo;

@property (weak, nonatomic) IBOutlet UIImageView *header;
@property (weak, nonatomic) IBOutlet UILabel *nickname;

@end

NS_ASSUME_NONNULL_END
