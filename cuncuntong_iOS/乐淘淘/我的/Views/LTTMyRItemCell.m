//
//  LTTMyRItemCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMyRItemCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LTTPhaseView.h"
@interface LTTMyRItemCell ()
@property (weak, nonatomic) IBOutlet UIImageView *g_imageView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet LTTPhaseView *phaseView;

@end

@implementation LTTMyRItemCell

- (void)setItem:(LTTMyReleaseItem *)item{
    _item = item;
    
    [self.g_imageView sd_setImageWithURL:[NSURL URLWithString:item.prize_cover_picture]];
    
    self.segmentedControl.selectedSegmentIndex = item.isPrize;
    
   
    self.titleLabel.text = item.prize_title;
 
    self.energyLabel.text = [NSString stringWithFormat:@"%d克",[item.energy_for_lucky_draw intValue] * 25];
    
    self.phaseView.current = [item.current_of_lottery intValue];
    
    self.phaseView.total = [item.number_of_lottery intValue];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.segmentedControl setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:9],NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
    
    [self.segmentedControl setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:9],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateSelected];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
