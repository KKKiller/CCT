//
//  LTTStarView.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTStarView.h"



@interface StarView ()

@end

@implementation StarView

- (instancetype)init{
    
    if (self = [super init]) {
        
        [self configuration];
    }
    
    return self;
}


- (void)configuration {
    
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
          make.height.mas_equalTo(15);
      }];
    
    NSArray <UIImageView *>*imageViews = @[
        UIImageView.new,
        UIImageView.new,
        UIImageView.new,
        UIImageView.new,
        UIImageView.new,
        UIImageView.new,
        UIImageView.new,
    ];
    
    UIView *last = nil;
    
    for (UIImageView *imageView in imageViews) {
        
        imageView.image = [UIImage imageNamed:@"4-2-1"];
        
        [self addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(last == nil ? self:last.mas_right);
            make.top.bottom.equalTo(self);
            make.width.mas_equalTo(15);
        }];
        last = imageView;
    }
    
    self.clipsToBounds = YES;
    
}




@end

@interface LTTStarView ()

@property (nonatomic,strong)StarView *starView;

@property (nonatomic,strong)UILabel *levelLabel;

@end

@implementation LTTStarView


- (instancetype)initWithCoder:(NSCoder *)coder{
    
    if (self = [super initWithCoder:coder]) {
        
        self.starCount = 0;
        
        [self configuration];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.starCount = 0;
        [self configuration];
        
    }
    
    return self;
}



#pragma mark 配置

- (void)configuration{
    
    UILabel *label = UILabel.new;
    
    label.text = @"星级";
    
    label.textColor = HEXCOLOR(0x313131);
    
    label.font = [UIFont systemFontOfSize:13];
    
    [self addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self);
    }];
    
    self.starView = StarView.new;
    
    [self addSubview:self.starView];
    
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_right);
        make.bottom.equalTo(self);
        make.width.mas_equalTo(self.starCount * 15);
    }];
    
    self.levelLabel = UILabel.new;
    
    self.levelLabel.text = @"0级";
     
     self.levelLabel.textColor = HEXCOLOR(0x313131);
     
     self.levelLabel.font = [UIFont systemFontOfSize:13];
    
    [self addSubview:self.levelLabel];
    
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.starView.mas_right);
        make.right.bottom.equalTo(self);
    }];
    
    
}

- (void)setStarCount:(NSInteger)starCount{
    _starCount = starCount;
    
    [self.starView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(starCount * 15);
    }];
    
    self.levelLabel.text = [NSString stringWithFormat:@"%ld级",(long)starCount];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
