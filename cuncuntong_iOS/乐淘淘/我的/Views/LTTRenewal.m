//
//  LTTRenewal.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTRenewal.h"

@implementation LTTRenewal

- (instancetype)initWithCoder:(NSCoder *)coder{
    
    if (self = [super initWithCoder:coder]) {
        [self configuration];
    }
    

    return self;
    
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        [self configuration];
    }
    
    return self;
}

- (void)configuration{

    UILabel *label = UILabel.new;
    label.text = @"续费";
    label.textColor = UIColor.whiteColor;
    label.backgroundColor = HEXCOLOR(0x505050);
    label.font = [UIFont systemFontOfSize:14];
    label.layer.cornerRadius = 3;
    label.layer.masksToBounds = YES;
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    label.hidden = true;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.bottom.top.equalTo(self);
        make.width.mas_equalTo(45);
    }];
    

  
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
