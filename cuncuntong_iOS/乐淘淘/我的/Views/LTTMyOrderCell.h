//
//  LTTMyOrderCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTMyOrder.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTMyOrderCell : UITableViewCell

@property (nonatomic,strong)LTTMyOrder *item;

@end

NS_ASSUME_NONNULL_END
