//
//  LTTPayView.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPayView.h"
#import "LTTStarView.h"
@interface LTTPayView()

@property (nonatomic,strong)StarView *starView;

@property (nonatomic,strong)UILabel *starLabel;

@property (nonatomic,strong)UILabel *starDescription;

@property (nonatomic,strong)UILabel *money;

@property (nonatomic,strong)UILabel *month;

@property (nonatomic,strong)UILabel *leveLabel;

@end

@implementation LTTPayView

- (instancetype)initWithCoder:(NSCoder *)coder{
    
    if (self = [super initWithCoder:coder]) {
        [self configuration];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        [self configuration];
    }
    
    return self;
}

- (void)setLevel:(NSInteger)level{
    _level = level;
    self.leveLabel.text = [NSString stringWithFormat:@"%ld",level];
    [self calculatedAmount];
    
}


- (void)configuration{
    
    self.starvip = 0;
    _level = 1;
    self.days = 0;
    self.starLabel  = UILabel.new;
    
    self.starLabel.text = @"1星会员";
    self.starLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    self.starLabel.textColor = HEXCOLOR(0x313131);
    [self addSubview:self.starLabel];
    
    [self.starLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self).inset(20);
    }];
    
    self.starDescription = UILabel.new;
    
    self.starDescription.text = @"阅读、运动、购物正增速2倍";
    
    self.starDescription.font = [UIFont systemFontOfSize:9];
    
    self.starDescription.textColor = HEXCOLOR(0x666666);
    
    [self addSubview:self.starDescription];
    
    [self.starDescription mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.starLabel);
        make.top.equalTo(self.starLabel.mas_bottom);
    }];
    
    self.starView = [[StarView alloc]init];
    
    [self addSubview:self.starView];
    
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.starLabel.mas_right);
        make.bottom.equalTo(self.starLabel);
        make.width.mas_equalTo(self.starvip * 15.0);
    }];
    
    
    UILabel *level = UILabel.new;
    
    level.text = @"星级";
    level.textColor = HEXCOLOR(0xDCAE4A);
    level.font = [UIFont systemFontOfSize:15];
    [self addSubview:level];
    
    
    UIButton *levelplus = UIButton.new;
    
    [levelplus setTitle:@"+" forState:UIControlStateNormal];
    
    [levelplus setTitleColor:UIColor.lightGrayColor forState:UIControlStateNormal];
    
    levelplus.layer.cornerRadius = 10;
    
    levelplus.layer.masksToBounds = YES;
    
    levelplus.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    levelplus.layer.borderWidth = 0.5;
    
    [self addSubview:levelplus];
    
    [levelplus addTarget:self action:@selector(plusOneLevelEventAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [levelplus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(level);
        make.right.equalTo(level.mas_left).inset(5);
    }];
    
    
    self.leveLabel = UILabel.new;
    
    self.leveLabel.text = @"1";
    
    self.leveLabel.font = [UIFont systemFontOfSize:13];
    
    self.leveLabel.textAlignment = NSTextAlignmentCenter;
    
    self.leveLabel.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    self.leveLabel.layer.borderWidth = 0.5;
    
    self.leveLabel.textColor = HEXCOLOR(0xDCAE4A);
    
    [self addSubview:self.leveLabel];
    
    [self.leveLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(levelplus.mas_left).inset(5);
        make.bottom.top.equalTo(levelplus);
        make.width.mas_equalTo(40);
    }];
    
    UIButton *leveminusOne = UIButton.new;
    
    [leveminusOne setTitle:@"-" forState:UIControlStateNormal];
    
    [leveminusOne setTitleColor:UIColor.lightGrayColor forState:UIControlStateNormal];
    
    leveminusOne.layer.cornerRadius = 10;
    
    leveminusOne.layer.masksToBounds = YES;
    
    leveminusOne.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    leveminusOne.layer.borderWidth = 0.5;
    
    [leveminusOne addTarget:self action:@selector(minusOneLevelEventAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:leveminusOne];
    
    [leveminusOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(level);
        make.right.equalTo(self.leveLabel.mas_left).inset(5);
    }];
    
    
    self.money = UILabel.new;
    
    self.money.text = @"70元";
    self.money.textColor = HEXCOLOR(0xDCAE4A);
    self.money.font = [UIFont systemFontOfSize:15];
    
    [self addSubview:self.money];
    
    [self.money mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.top.equalTo(level.mas_bottom).offset(15);
    }];
    
    UILabel *month = UILabel.new;
    
    month.text = @"月";
    month.textColor = HEXCOLOR(0xDCAE4A);
    month.font = [UIFont systemFontOfSize:15];
    [self addSubview:month];
    
    [month mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).inset(60);
        make.centerY.equalTo(self.money);
    }];
    
    [level mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(month);
        make.centerY.equalTo(self.starView.mas_bottom);
    }];
    
    UIButton *plus = UIButton.new;
    
    [plus setTitle:@"+" forState:UIControlStateNormal];
    
    [plus setTitleColor:UIColor.lightGrayColor forState:UIControlStateNormal];
    
    plus.layer.cornerRadius = 10;
    
    plus.layer.masksToBounds = YES;
    
    plus.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    plus.layer.borderWidth = 0.5;
    
    [self addSubview:plus];
    
    [plus addTarget:self action:@selector(plusOneEventAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [plus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.money);
        make.right.equalTo(month.mas_left).inset(5);
    }];
    
    
    self.month = UILabel.new;
    
    self.month.text = @"1";
    
    self.month.font = [UIFont systemFontOfSize:13];
    
    self.month.textAlignment = NSTextAlignmentCenter;
    
    self.month.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    self.month.layer.borderWidth = 0.5;
    
    self.month.textColor = HEXCOLOR(0xDCAE4A);
    
    [self addSubview:self.month];
    
    [self.month mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(plus.mas_left).inset(5);
        make.bottom.top.equalTo(plus);
        make.width.mas_equalTo(40);
    }];
    
    UIButton *minusOne = UIButton.new;
    
    [minusOne setTitle:@"-" forState:UIControlStateNormal];
    
    [minusOne setTitleColor:UIColor.lightGrayColor forState:UIControlStateNormal];
    
    minusOne.layer.cornerRadius = 10;
    
    minusOne.layer.masksToBounds = YES;
    
    minusOne.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    minusOne.layer.borderWidth = 0.5;
    
    [minusOne addTarget:self action:@selector(minusOneEventAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:minusOne];
    
    [minusOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.money);
        make.right.equalTo(self.month.mas_left).inset(5);
    }];
    
    
    
    
    
    
    UIButton *pay = UIButton.new;
    
    [pay setTitle:@"支付" forState:UIControlStateNormal];
    
    pay.backgroundColor = HEXCOLOR(0x505050);
    
    pay.layer.cornerRadius = 3;
    pay.layer.masksToBounds = YES;
    
    pay.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [pay addTarget:self action:@selector(payWith:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:pay];
    
    [pay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.width.mas_equalTo(45);
        make.bottom.equalTo(self).inset(20);
        make.height.mas_equalTo(22);
    }];
    [self calculatedAmount];
    
}

- (NSInteger)monthValue{
    
    return [self.month.text intValue];
}

- (void)setStarvip:(NSInteger)starvip{
    _starvip = starvip;
    
    self.starLabel.text = [NSString stringWithFormat:@"%ld星会员",(long)starvip];
    
    self.starDescription.text = [NSString stringWithFormat:@"阅读、运动、购物正增速%ld倍",starvip *2];
    
    [self.starView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(starvip *15);
    }];
    
    [self calculatedAmount];
}


- (void)minusOneEventAction:(UIButton *)sender{
    
    NSString *month = self.month.text;
    
    if ([month isEqualToString:@"1"]) {
        SHOW(@"最低一个月");
        return;
    }
    
    self.month.text = [NSString stringWithFormat:@"%d",[month intValue] - 1];
    
    [self calculatedAmount];
    
}

- (void)plusOneEventAction:(UIButton *)sender{
    
    NSString *month = self.month.text;
    
    self.month.text = [NSString stringWithFormat:@"%d",[month intValue] + 1];
    [self calculatedAmount];
}
- (void)minusOneLevelEventAction:(UIButton *)sender{
    
    
    if (self.level > self.starvip) {
        self.level -= 1;
        [self calculatedAmount];
        
    }else{
        
        NSString *text = [NSString stringWithFormat:@"最低%ld星级",self.starvip + 1];
        
        SHOW(text);
    }
}

- (void)plusOneLevelEventAction:(UIButton *)sender{
    if (self.level >= 7) {
        SHOW(@"最高7星级");
    }else{
        self.level += 1;
        [self calculatedAmount];
    }
    
}

- (void)calculatedAmount{
    
   self.money.text = [NSString stringWithFormat:@"%ld元",self.level * 30 * [self.month.text intValue] + (self.level - self.starvip) * self.days];
}




- (void)payWith:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(payWith:)]) {
        [self.delegate payWith:self];
    }
}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
