//
//  LTTMyReleaseCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMyReleaseCell.h"
#import "LTTPhaseView.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTMyReleaseCell ()
@property (weak, nonatomic) IBOutlet UIImageView *g_imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet LTTPhaseView *phaseView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;


@end

@implementation LTTMyReleaseCell

- (void)setItem:(LTTMyRelease *)item{
    _item = item;
    
    self.dateLabel.text = item.qi != nil ? [NSString stringWithFormat:@"第%@期",item.qi] : nil;
    
    [self.g_imageView sd_setImageWithURL:[NSURL URLWithString:item.prize_cover_picture]];
    
    self.titleLabel.text = item.prize_title;
    
    switch ([item.energy_for_lucky_draw intValue]) {
        case 1:
            self.energyLabel.text = @"25克";
            break;
        case 2:
            self.energyLabel.text = @"50克";
            break;
        case 3:
            self.energyLabel.text = @"75克";
            break;
        case 4:
            self.energyLabel.text = @"100克";
            break;
        default:
            break;
    }
    
    self.phaseView.current = [item.current_of_lottery intValue];
    
    self.phaseView.total = [item.number_of_lottery intValue];
    
    self.reviewLabel.text = [item.is_audit intValue] == 1 ? @"已审核" : @"审核中";
    self.segmented.selectedSegmentIndex = [item.state intValue] == 1 ? 1 : 0;

    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.segmented setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}forState:UIControlStateSelected];

    [self.segmented setTitleTextAttributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x313131)}forState:UIControlStateNormal];
    self.segmented.transform = CGAffineTransformScale(self.segmented.transform, 0.8, 0.8);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)changeShelves:(UISegmentedControl *)sender {
    
}

@end
