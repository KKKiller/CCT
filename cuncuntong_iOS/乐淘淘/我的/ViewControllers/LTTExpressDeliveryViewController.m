//
//  LTTExpressDeliveryViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTExpressDeliveryViewController.h"

@interface LTTExpressDeliveryViewController ()
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UITextField *company;

@end

@implementation LTTExpressDeliveryViewController

- (NSString *)title{
    
    return @"物流快递";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.company.text = self.courier_comp_name;
    self.numberTextField.text = self.courier_sn;
    
}
- (IBAction)submitEventAction:(UIButton *)sender {
    
    if (self.numberTextField.text.length <= 0) { return SHOW(@"请输入订单号"); }
    
    if (self.company.text.length <= 0) return SHOW(@"请输入快递公司");
    
    
    [[MyNetWorking sharedInstance] LTTPostURL:LTTURL(@"delivery") params:@{@"id":self.ID,@"courier_sn":self.numberTextField.text,@"courier_comp_name":self.company.text} target:self success:^(NSDictionary *success) {
        
        SHOW(success[@"msg"]);
        
        if ([success[@"error"] intValue] == 0) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        
    } failure:^(NSError *failure) {
        SHOW(failure.localizedDescription);
    }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
