//
//  LTTVIPViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE
@interface LTTVIPViewController : LTTBaseViewController

@property (nonatomic,strong)UIImage *avatarImage;

@property (nonatomic,copy)NSString *nickNameText;

@end

NS_ASSUME_NONNULL_END
