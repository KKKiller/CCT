//
//  LTTAddressViewController.m
//  CunCunTong
//
//  Created by W&Z on 2020/1/6.
//  Copyright © 2020 zhushuai. All rights reserved.
//

#import "LTTAddressViewController.h"
#import "CCMyHeader.h"
#import "CCMyCell.h"
#import "CCAddressModel.h"
#import "CCHobbyModel.h"
#import "LTTAddressHeaderView.h"
@interface LTTAddressViewController ()<UITableViewDataSource,UITableViewDelegate,CCMyCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *hobbyArray;
@property (nonatomic, strong) LTTAddressHeaderView *header;


@end

@implementation LTTAddressViewController

- (NSString *)title{
    return @"地址";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
    //  [self loadData];
    
    [NOTICENTER addObserver:self selector:@selector(chooseAddressFinished:) name:@"ChooseLocationNotification" object:nil];
    
}
- (void)refreshData {
    [self loadData];
}
- (void)setUI {
    
    
    
    self.tableView.tableHeaderView = self.header;
    
    [self.header mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(300);
        make.width.equalTo(self.view);
    }];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.rowHeight = 120;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [ self.tableView registerNib:[UINib nibWithNibName:@"CCMyCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    
    [self.view addSubview:self.tableView];
    
}
#pragma mark - 获取数据
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"userid":USERID,@"offset":@"0",@"limit":@"30"}];
    // params[@"type"] = self.addressType;
    
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/getaddress") params:params target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            NSArray *array = success[@"data"];
            [self.dataArray removeAllObjects];
            for (NSDictionary *dic  in array) {
                CCAddressModel *model = [CCAddressModel modelWithJSON:dic];
                [self.dataArray addObject:model];
            }
        }else{
            
        }
        [self.tableView reloadData];
    } failure:^(NSError *failure) {
    }];
}

#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCMyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    cell.delegate = self;
    cell.model = self.dataArray[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CCAddressModel *model = self.dataArray[indexPath.row];
    
    NSDictionary *parameter = @{@"q_id":self.q_id,
                                @"consignee_address":model.location,
                                @"consignee_mobile":model.phone,
                                @"consignee_name":model.realname
    };
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"changeConsigneeInfo") params:parameter target:self success:^(NSDictionary *success) {
        
    } failure:^(NSError *failure) {
        
    }];
    
    
    
    
    
}
//保存
- (void)saveBtnClick {
    
    if (self.header.phoneField.text.length == 0 && ![MyUtil validateTelephoneNumber:self.header.phoneField.text]) {
        SHOW(@"请输入电话号码");
        return;
    }
    if (self.header.addressField.text.length == 0) {
        SHOW(@"请通过“定位当前位置”设置详细地址");
        return;
    }
    if (self.header.addressNameField.text.length == 0) {
        SHOW(@"请输入地址名称");
        return;
    }
    
    if(self.header.nicknameField.text.length == 0) {
        SHOW(@"用户名不能为空");
        return;
    }
    
    NSDictionary *parameter = @{@"q_id":self.q_id,
                                @"consignee_address":[NSString stringWithFormat:@"%@%@",self.header.addressField.text,self.header.addressNameField.text],
                                @"consignee_mobile":self.header.phoneField.text,
                                @"consignee_name":self.header.nicknameField.text
    };
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"changeConsigneeInfo") params:parameter target:self success:^(NSDictionary *success) {
        SHOW(success[@"msg"]);
        if ([success[@"error"] intValue] ==0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSError *failure) {
        SHOW(@"修改失败");
    }];
    
    //    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
    //        @"userid":USERID,
    //        @"phone":self.header.phoneField.text,
    //        @"location":self.header.addressField.text,
    //        @"location_info":self.header.addressField.text,
    //        @"name":self.header.addressNameField.text,
    //        @"realname":self.header.nicknameField.text,
    //        @"lat":self.header.model.lat,
    //        @"lng":self.header.model.lng}];
    //
    //    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/address") params:params target:nil success:^(NSDictionary *success) {
    //        if ([success[@"error"] integerValue] == 0) {
    //            SHOW(@"添加成功");
    //            [self refreshData];
    //            self.header.phoneField.text = @"";
    //            self.header.addressNameField.text = @"";
    //            self.header.addressField.text = @"";
    //        }else{
    //            NSString *msg = success[@"msg"];
    //            SHOW(msg);
    //        }
    //    } failure:^(NSError *failure) {
    //
    //    }];
}
//删除
- (void)deleteBtnClick:(CCAddressModel *)model {
    [[MyNetWorking sharedInstance]PostUrl:BASEURL_WITHOBJC(@"share/deleteaddress") params:@{@"userid":USERID,@"address_id":model.id} target:nil success:^(NSDictionary *success) {
        if ([success[@"error"] integerValue] == 0) {
            SHOW(@"删除成功");
            [self refreshData];
        }else{
            NSString *msg = success[@"msg"];
            SHOW(msg);
        }
    } failure:^(NSError *failure) {
        
    }];
}

- (void)chooseAddress {
    ShowMapViewController *vc = [[UIStoryboard storyboardWithName:@"ShowMapViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"ShowMapViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}
//选择地址
- (void)chooseAddressFinished:(NSNotification *)noti {
    NSDictionary *dict = noti.userInfo;
    NSLog(@"选择地址--%@",dict);
    CCAddressModel *model = [[CCAddressModel alloc]init];
    model.name = dict[@"name"];
    model.location = dict[@"address"];
    model.lat = dict[@"lat"];
    model.lng = dict[@"lng"];
    self.header.model = model;
    
}
#pragma mark - 懒加载

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (LTTAddressHeaderView *)header {
    if (_header) return _header;
    
    _header = [LTTAddressHeaderView instanceView];
    _header.frame = CGRectMake(0, 0, App_Width, 255);
    [_header.saveBtn addTarget:self action:@selector(saveBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_header.locateBtn addTarget:self action:@selector(chooseAddress) forControlEvents:UIControlEventTouchUpInside];
    
    
    return _header;
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
