//
//  LTTStarViewController.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/27.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"

@protocol StarControllerDelegate <NSObject>

- (void)selectedLevel:(NSInteger)level;

@end

NS_ASSUME_NONNULL_BEGIN

@interface LTTStarViewController : LTTBaseViewController

@property (nonatomic,weak) id <StarControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
