//
//  LTTMyReleaseViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"
#import "LTTMyRelease.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTMyReleaseViewController : LTTBaseViewController

@property (nonatomic,strong)LTTMyRelease *item;

@end

NS_ASSUME_NONNULL_END
