//
//  LTTMineViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMineViewController.h"
#import "LTTMineHeaderView.h"
#import "LTTMyOrder.h"
#import "LTTMyOrderCell.h"
#import "LTTVIPViewController.h"
#import "LTTPrizeViewController.h"
#import "LTTMyPrize.h"
#import "LTTPrizeCell.h"
#import "LTTMyRelease.h"
#import "LTTMyReleaseCell.h"
#import "LTTMyReleaseViewController.h"
#import "AccountViewController.h"
#import "LTTAddressViewController.h"
#import "CCShareMyController.h"
@interface LTTMineViewController ()<MineHeaderDelegate,UITableViewDataSource,UITableViewDelegate,LTTPrizeCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)LTTMineHeaderView *headerView;

@property (nonatomic,strong)NSArray *items;

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (nonatomic,assign)MineHeadrButtonType buttonType;

@end

@implementation LTTMineViewController

#pragma mark - 懒加载
- (LTTMineHeaderView *)headerView{
    
    if (_headerView) return _headerView;
    
    _headerView = [[UINib nibWithNibName:@"LTTMineHeaderView" bundle:nil] instantiateWithOwner:nil options:nil].firstObject;
    
    _headerView.delegate = self;
    
    [self.topView addSubview:_headerView];
    
    
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.topView);
        
    }];
    
    return _headerView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    [self.tableView.mj_header beginRefreshing];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.buttonType = MineHeadrButtonTypeOrder;
    [self configuration];
    [self loadData];
}

#pragma mark - 配置

- (void)configuration{
    
    [self.headerView layoutIfNeeded];
    __weak typeof(self)weak = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak loadData];
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTMyOrderCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTPrizeCell" bundle:nil] forCellReuseIdentifier:@"Cell1"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTMyReleaseCell" bundle:nil] forCellReuseIdentifier:@"Cell2"];
    
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"icon_cilun"] style:UIBarButtonItemStyleDone target:self action:@selector(settings)] ;
    //    self.navigationItem.rightBarButtonItem.tintColor = UIColor.whiteColor;
}


#pragma mark - 网络请求

- (void)loadData{
    self.items = nil;
    [self.tableView reloadData];
    switch (self.buttonType) {
        case MineHeadrButtonTypeOrder:{
            
            [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"personalCenter") params:nil target:self success:^(NSDictionary *success) {
                [self.tableView.mj_header endRefreshing];
                if ([success[@"error"] intValue] == 0) {
                    
                    self.headerView.userInfo =  success[@"data"][@"user_info"];
                    
                    self.items = [NSArray modelArrayWithClass:[LTTMyOrder class] json:success[@"data"][@"place_order_info"]];
                    
                    [self.tableView reloadData];
                    
                }else{ SHOW(success[@"msg"]); }
                
            } failure:^(NSError *failure) {
                [self.tableView.mj_header endRefreshing];
                [self.tableView reloadData];
                SHOW(failure.localizedDescription);
            }];
            
            break;
        }
        case MineHeadrButtonTypePrize:{
            [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"myPrize") params:nil target:self success:^(NSDictionary *success) {
                [self.tableView.mj_header endRefreshing];
                if ([success[@"error"] intValue] == 0) {
                    
                    self.items = [NSArray modelArrayWithClass:[LTTMyPrize class] json:success[@"data"]];
                    
                }else{ SHOW(success[@"msg"]); }
                
                [self.tableView reloadData];
                
            } failure:^(NSError *failure) {
                
                [self.tableView.mj_header endRefreshing];
                [self.tableView reloadData];
                SHOW(failure.localizedDescription);
                
            }];
            
            break;
        }
        case MineHeadrButtonTypeRelease:{
            [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"myRelease") params:nil target:self success:^(NSDictionary *success) {
                [self.tableView.mj_header endRefreshing];
                if ([success[@"error"] intValue] == 0) {
                    
                    self.items = [NSArray modelArrayWithClass:[LTTMyRelease class] json:success[@"data"]];
                    
                }else{ SHOW(success[@"msg"]); }
                
                [self.tableView reloadData];
                
            } failure:^(NSError *failure) {
                
                [self.tableView.mj_header endRefreshing];
                [self.tableView reloadData];
                SHOW(failure.localizedDescription);
                
            }];
            
            
            break;
        }
            
        default:
            break;
    }
}

- (void)settings{
    
    [self.navigationController pushViewController:AccountViewController.new animated:YES];
    
}


#pragma mark - MineHeaderDelegate
- (void)mineHeaderView:(LTTMineHeaderView *)headerView buttonType:(MineHeadrButtonType)type{
    
    self.buttonType = type;
    
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)upgradeUser{
    
    LTTVIPViewController *vip = [[LTTVIPViewController alloc]initWithNibName:@"LTTVIPViewController" bundle:nil];
    
    vip.nickNameText = self.headerView.nickname.text;
    
    vip.avatarImage = self.headerView.header.image;
    
    [self.navigationController pushViewController:vip animated:YES];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (self.buttonType) {
        case MineHeadrButtonTypeOrder:{
            LTTMyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
            if ([self.items[indexPath.section] isKindOfClass:[LTTMyOrder class]]) {
                cell.item = self.items[indexPath.section];
            }
            
            
            
            return cell;
        }
            
        case MineHeadrButtonTypeRelease:{
            
            LTTMyReleaseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell2" forIndexPath:indexPath];
            
            if ([self.items[indexPath.section] isKindOfClass:[LTTMyRelease class]]) {
                cell.item = self.items[indexPath.section];
                
                if ([cell.item.is_audit intValue] == 1){
                    cell.reviewLabel.hidden = true;
                    cell.segmented.hidden = false;
                    
                }else{
                    cell.reviewLabel.hidden = false;
                    cell.segmented.hidden = true;
                }
            }
            
            
            return cell;
        }
        case MineHeadrButtonTypePrize:{
            
            LTTPrizeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
            if ([self.items[indexPath.section] isKindOfClass:[LTTMyPrize class]]) {
                cell.item = self.items[indexPath.section];
                cell.delegate = self;
            }
            
            
            return cell;
        }
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *header = UIView.new;
    header.backgroundColor = UIColor.groupTableViewBackgroundColor;
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return UIView.new;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (self.buttonType) {
        case MineHeadrButtonTypeRelease:{
            
            if ([self.items[indexPath.section] isKindOfClass:[LTTMyRelease class]]) {
                LTTMyReleaseViewController *release = LTTMyReleaseViewController.new;
                
                release.item = self.items[indexPath.section];
                
                [self.navigationController pushViewController:release animated:YES];
            }
            
            break;
        }
            
        default:
            break;
    }
}

- (void)confirm:(LTTMyPrize *)item{
    
    UIAlertController  *alert = [UIAlertController alertControllerWithTitle:@"签收" message:[NSString stringWithFormat:@"签收单号为:%@ %@",item.courier_comp_name,item.courier_sn] preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"签收" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self confirmWith:item.ID];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}

- (void)changeAddressWith:(nonnull LTTMyPrize *)item {
    
    LTTAddressViewController *address = LTTAddressViewController.new;
    address.q_id = item.ID;
    [self.navigationController pushViewController:address animated:YES];
}


- (void)confirmWith:(NSString *)q_id{
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"confirmOfTheGoods") params:@{@"qid":q_id} target:self success:^(NSDictionary *success) {
        
        if ([success[@"error"] intValue] == 0) {
            [self.tableView.mj_header beginRefreshing];
        }
        
        SHOW(success[@"msg"]);
        
    } failure:^(NSError *failure) {
        SHOW(@"签收失败");
    }];
    
}

- (void)kuaididanhao:(NSString *)danhao {
    
    CCBaseWebviewController *vc = [CCBaseWebviewController new];
    [vc setHiddenShare:YES];
    vc.urlStr = [NSString stringWithFormat:@"https://m.kuaidi100.com/result.jsp?nu=%@",danhao];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
