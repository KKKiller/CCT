//
//  LTTPrizeViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPrizeViewController.h"
#import "LTTPrizeCell.h"
#import "LTTMyPrize.h"
@interface LTTPrizeViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSArray<LTTMyPrize *>*items;


@end

@implementation LTTPrizeViewController

- (NSString *)title{
    
    return @"我的奖品";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
}



#pragma mark - 配置

- (void)configuration{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTPrizeCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    
    [self.tableView.mj_header beginRefreshing];
    
}

#pragma mark - 网络

- (void)loadData{
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"myPrize") params:nil target:self success:^(NSDictionary *success) {
        [self.tableView.mj_header endRefreshing];
        if ([success[@"error"] intValue] == 0) {
            
            self.items = [NSArray modelArrayWithClass:[LTTMyPrize class] json:success[@"data"]];
            
        }else{ SHOW(success[@"msg"]); }
        
        [self.tableView reloadData];
        
    } failure:^(NSError *failure) {
        
        [self.tableView.mj_header endRefreshing];
        
        SHOW(failure.localizedDescription);
        
    }];
    
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTPrizeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.item = self.items[indexPath.row];
    
    return cell;
    
}
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return UIView.new;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footer = UIView.new;
    
    footer.backgroundColor = UIColor.groupTableViewBackgroundColor;
    
    return footer;
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
