//
//  LTTMyReleaseViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMyReleaseViewController.h"
#import "LTTMyReleaseCell.h"
#import "LTTMySubReleaseViewController.h"
@interface LTTMyReleaseViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSArray <LTTMyRelease *>*items;

@end

@implementation LTTMyReleaseViewController

- (NSString *)title{
    
    return @"我的发布";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
}

- (void)configuration{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTMyReleaseCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    __weak typeof(self)weak = self;
    
    [self.tableView setTableFooterView:UIView.new];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak loadData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)loadData{
    __weak typeof(self) weak = self;
    [[MyNetWorking sharedInstance] PostUrl:LTTURL(@"previousJackpot") params:@{@"id":self.item.ID} success:^(NSDictionary *success) {
        [weak.tableView.mj_header endRefreshing];
        if ([success[@"error"] intValue] == 0) {
            
            weak.items = [NSArray modelArrayWithClass:[LTTMyRelease class] json:success[@"data"]];
            
            [weak.tableView reloadData];
            
        }else{
            SHOW(success[@"msg"]);
        }
        
        
        
    } failure:^(NSError *failure) {

        [weak.tableView.mj_header endRefreshing];
        SHOW(failure.localizedDescription);
    }];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTMyReleaseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.item = self.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTMySubReleaseViewController *subRelease = LTTMySubReleaseViewController.new;
    
    LTTMyRelease *item = self.items[indexPath.row];
    
    if ([item.state intValue] == 1) { SHOW(@"参与进度未完成");}
    if ([item.state intValue] == 2) { SHOW(@"未开奖");}
    if ([item.state intValue] < 3) { return; }
    
    subRelease.ID = item.ID;
    subRelease.qi = item.qi;
    subRelease.title = [NSString stringWithFormat:@"第%@期",item.qi];

    [self.navigationController pushViewController:subRelease animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
