//
//  LTTStarViewController.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/27.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTStarViewController.h"

@interface LTTStarViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSArray <NSString *>*items;

@end

@implementation LTTStarViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    if ([super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
        self.modalPresentationStyle = UIModalPresentationCustom;
    
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
}


#pragma mark - 配置

- (void)configuration{
    
    self.items = @[
        @"1星会员",
        @"2星会员",
        @"3星会员",
        @"4星会员",
        @"5星会员",
        @"6星会员",
        @"7星会员"
    ];

    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView reloadData];
    
}
- (IBAction)cancelEventAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.items[indexPath.row];
    
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    cell.textLabel.textColor = HEXCOLOR(0x313131);
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.delegate respondsToSelector:@selector(selectedLevel:)]) {
        
        [self.delegate selectedLevel:indexPath.row + 1];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
