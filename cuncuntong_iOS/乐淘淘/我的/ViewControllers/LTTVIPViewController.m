//
//  LTTVIPViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTVIPViewController.h"
#import "LTTStarViewController.h"
#import "LTTPayView.h"
#import "LTTRenewal.h"
#import "LTTStarView.h"
#import "RechargeViewController.h"
@interface LTTVIPViewController ()<PayDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nickname;
@property (weak, nonatomic) IBOutlet UILabel *expire_time;
@property (weak, nonatomic) IBOutlet LTTStarView *currentStarLevel;
@property (weak, nonatomic) IBOutlet UILabel *vipLevel;
@property (weak, nonatomic) IBOutlet UILabel *remaining;
@property (weak, nonatomic) IBOutlet LTTRenewal *automaticRenewal;
@property (weak, nonatomic) IBOutlet LTTPayView *payView;
@property (weak, nonatomic) IBOutlet UIView *views;

@end

@implementation LTTVIPViewController

- (NSString *)title{
    
    return @"会员升级";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.avatar.image = self.avatarImage;
    
    self.nickname.text = self.nickNameText;
    
    
    [self.automaticRenewal addTarget:self action:@selector(isSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self level:@"0" expire_time:@"0"];
    self.automaticRenewal.hidden = YES;
    self.views.hidden = YES;
    [self loadData];
}


- (void)loadData{
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"readerLevelInfo") params:nil target:self success:^(NSDictionary *success) {
        
        if ([[success valueForKey:@"error"] intValue] == 0) {
            [self level:success[@"data"][@"level"] expire_time:success[@"data"][@"expire_time"]];
            
        }else{ SHOW(success[@"msg"]); }
        
        
    } failure:^(NSError *failure) {
        SHOW(failure.localizedDescription);
    }];
    
}

- (void)level:(NSString *)level expire_time:(NSString *)expire_time{
    
    self.automaticRenewal.hidden = NO;
    
    self.views.hidden = NO;
    
    self.currentStarLevel.starCount = [level intValue];
    
    if ([expire_time intValue] > 0) {
        self.expire_time.text = [NSString stringWithFormat:@"剩余天数%@天",expire_time];
        self.remaining.text = [NSString stringWithFormat:@"%@",expire_time];
        self.payView.days = [expire_time intValue];
    }else{
        self.expire_time.text = @"已过期";
        self.remaining.text = @"0";
        self.payView.days = 0;
    }
    
    if ([level intValue] != 0) {
        self.payView.starvip = [level intValue];
        
        self.payView.level = [level intValue];
    }
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (void)isSelectedAction:(UIButton *)selder{
    
    NSString *status = [NSString stringWithFormat:@"%d",selder.isSelected];
    
    [[MyNetWorking sharedInstance] LTTPostURL:LTTURL(@"readerLevelInfo") params:@{@"status":status} target:self success:^(NSDictionary *success) {
        
        if ([success[@"error"] intValue] == 0) {
            
            selder.selected = !selder.isSelected;
            
        }else{ SHOW(success[@"msg"]); }
        
    } failure:^(NSError *failure) {
        SHOW(@"msg");
    }];
    
}


- (void)payWith:(LTTPayView *)view{
    
    NSDictionary *paramters = @{
        @"months":[NSString stringWithFormat:@"%ld",view.monthValue],
        @"level":[NSString stringWithFormat:@"%ld",view.starvip]
        
    };
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"readerUpgradePay") params:paramters target:self success:^(NSDictionary *success) {
        
        NSInteger code = [success[@"error"] intValue];
        
        if ( code == 0) {
            SHOW(@"支付成功");
            [self loadData];
        }else if (code == 1){
            
            [self.navigationController pushViewController:[[RechargeViewController alloc]init] animated:YES];
        }
    } failure:^(NSError *failure) {
        SHOW(failure.localizedDescription);
    }];
}



@end
