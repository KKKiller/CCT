//
//  LTTExpressDeliveryViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LTTExpressDeliveryViewController : LTTBaseViewController

@property (nonatomic,copy)NSString *ID;

@property (nonatomic,copy)NSString *courier_comp_name;
@property (nonatomic,copy)NSString *courier_sn;
@end

NS_ASSUME_NONNULL_END
