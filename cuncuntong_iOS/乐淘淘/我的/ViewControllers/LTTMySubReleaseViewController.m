//
//  LTTMySubReleaseViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMySubReleaseViewController.h"
#import "LTTMyRItemCell.h"
#import "LTTNumberCell.h"
#import "LTTUserCell.h"
#import "NSString+SubString.h"
#import "LTTExpressDeliveryViewController.h"
#import "LTTWinnerCell.h"
@interface LTTMySubReleaseViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)LTTMyReleaseItem*items;

@end

@implementation LTTMySubReleaseViewController

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
}

- (void)configuration{
    
    [self.tableView setTableFooterView:UIView.new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTMyRItemCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTWinnerCell" bundle:nil] forCellReuseIdentifier:@"Cell1"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTUserCell" bundle:nil] forCellReuseIdentifier:@"Cell2"];
    
    __weak typeof(self)weak = self;
    
    [self.tableView setTableFooterView:UIView.new];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak loadData];
    }];
    
    
    
}

- (void)loadData{
    __weak typeof(self) weak = self;
    [[MyNetWorking sharedInstance] PostUrl:LTTURL(@"getJackpotUser") params:@{@"id":self.ID} success:^(NSDictionary *success) {
        [weak.tableView.mj_header endRefreshing];
        if ([success[@"error"] intValue] == 0) {
            
            weak.items = [LTTMyReleaseItem modelWithDictionary:success[@"data"]];
            [weak.tableView reloadData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weak.tableView reloadData];
            });
            
        }else{
            SHOW(success[@"msg"]);
        }
        
        
        
    } failure:^(NSError *failure) {
        
        [weak.tableView.mj_header endRefreshing];
        SHOW(failure.localizedDescription);
    }];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
    //    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.items == nil) { return 0; }
    
    switch (section) {
        case 0: return 1;
            
        case 1: return self.items.isPrize;
            
        case 2: return 1;
            //        case 2: return self.items.isUser ? 1 : 0;
            
            //        case 3: return 1;
            
        default: return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            LTTMyRItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.item = self.items;
            
            cell.dateLabel.text = [NSString stringWithFormat:@"第%@期",self.qi];
            
            return cell;
        }
        case 1:{
            LTTWinnerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
            cell.isRelease = YES;
            cell.releaseItem = self.items;
            
            return cell;
        }
            //        case 2:{
            //            LTTUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell2" forIndexPath:indexPath];
            //
            //            cell.item = self.items;
            //
            //            return cell;
            //        }
            
        default:{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell3"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell3"];
            }
            
            if ([self.items.is_delivery isEqualToString:@"1"]) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",self.items.courier_comp_name,self.items.courier_sn];
                cell.textLabel.textColor = HEXCOLOR(0x313131);
        
                cell.detailTextLabel.text = [self.items.is_confirm intValue] == 0 ?@"用户未签收" : @"用户已签收";
        
            }else{
                
                cell.detailTextLabel.text = nil;
                cell.textLabel.text = @"未发货";
                
                cell.textLabel.textColor = HEXCOLOR(0xe6554d);
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
            UIView *line = UIView.new;
            line.backgroundColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:line];
            
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.top.equalTo(cell.contentView);
                make.right.equalTo(cell);
                make.height.mas_equalTo(0.5);
                make.bottom.equalTo(cell.contentView).inset(44).priority(750);
            }];
            
            return cell;
        }
    }
    
}

#pragma mark - UITableViewDelegate


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 2) {
        
        LTTExpressDeliveryViewController *vc = LTTExpressDeliveryViewController.new;
        
        vc.ID = self.ID;
        
        vc.courier_comp_name = self.items.courier_comp_name;
        vc.courier_sn = self.items.courier_sn;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
