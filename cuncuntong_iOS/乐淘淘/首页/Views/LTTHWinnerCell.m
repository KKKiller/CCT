//
//  LTTWinnerCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/12/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTHWinnerCell.h"
#import "NSString+SubString.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTHWinnerCell ()
@property (weak, nonatomic) IBOutlet UIView *numberView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *l_imageView;
@property (weak, nonatomic) IBOutlet UILabel *dateNumber;
@property (weak, nonatomic) IBOutlet UILabel *openTime;

@end


@implementation LTTHWinnerCell

- (void)setItem:(LTTOpenPrizeItem *)item{
    _item = item;
    NSArray *substrings = item.lottery_numbers.substrings;
    
    for (int i = 0; i< substrings.count; i ++) {
        
        if (self.numberView.subviews.count > i) {
            UILabel *label = self.numberView.subviews[i];
            
            label.text = substrings[i];
        }
    
    }
    self.dateNumber.text = [NSString stringWithFormat:@"第%@期",item.now_qi];
    [self.l_imageView sd_setImageWithURL:[NSURL URLWithString:item.prize_cover_picture]];
    
    self.titleLabel.text = [NSString stringWithFormat:@"中奖人:%@",item.realname];

    self.openTime.text = [NSString stringWithFormat:@"开奖时间:%@",item.kaibang_time];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
