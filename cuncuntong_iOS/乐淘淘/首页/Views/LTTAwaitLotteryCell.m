//
//  LTTAwaitLotteryCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTAwaitLotteryCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTAwaitLotteryCell ()<timeDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *l_imageView;
@property (weak, nonatomic) IBOutlet UILabel *dateNumber;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hour;
@property (weak, nonatomic) IBOutlet UILabel *min;
@property (weak, nonatomic) IBOutlet UILabel *second;
@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UIView *views;

@end

@implementation LTTAwaitLotteryCell

- (void)setItem:(LTTOpenPrizeItem *)item{
    _item = item;
    
    [item open];
    
    [self.l_imageView sd_setImageWithURL:LTTImgaeURL(item.prize_cover_picture)];
    
    self.titleLabel.text = item.prize_title;

    self.hour.text = item.hour;
    
    self.min.text = item.minute;
    
    self.second.text = item.second;

    self.dateNumber.text = [NSString stringWithFormat:@"第%@期",item.now_qi];
    
    self.number.text = item.total_participation;
    
    self.views.hidden = ![item.state isEqualToString:@"2"];
    
    item.delegate = self;
    
}

- (void)timeUpdateWith:(LTTOpenPrizeItem *)item{
    
    self.hour.text = item.hour;
    
    self.min.text = item.minute;
    
    self.second.text = item.second;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
