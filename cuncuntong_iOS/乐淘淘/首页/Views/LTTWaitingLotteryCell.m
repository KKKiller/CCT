//
//  LTTWaitingLotteryCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/8.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTWaitingLotteryCell.h"

@interface LTTWaitingLotteryCell ()<timerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *hourLabel;
@property (weak, nonatomic) IBOutlet UILabel *minuteLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;

@end

@implementation LTTWaitingLotteryCell

- (void)setItem:(LTTLotteryHeaderItem *)item{
    _item = item;
    self.hourLabel.text = item.hour;
    self.minuteLabel.text = item.minute;
    self.secondLabel.text = item.second;
    item.delegate = self;
    [item open];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)timeUpdateWith:(LTTLotteryHeaderItem *)item{
    
    self.hourLabel.text = item.hour;
    self.minuteLabel.text = item.minute;
    self.secondLabel.text = item.second;
}

@end
