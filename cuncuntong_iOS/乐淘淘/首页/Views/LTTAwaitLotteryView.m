//
//  LTTAwaitLotteryView.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTAwaitLotteryView.h"
#import "NSString+SubString.h"
@interface LTTAwaitLotteryView ()
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *numbers;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end

@implementation LTTAwaitLotteryView

- (void)setItem:(LTTOpenPrizeItem *)item{
    _item = item;
    
    NSArray *substrings = item.lottery_numbers.substrings;
    
    for (int i = 0; i< substrings.count; i ++) {
        
        UILabel *label = self.numbers[i];
        
        label.text = substrings[i];
        
    }
    self.titleLabel.text = [NSString stringWithFormat:@"中奖人:%@",item.realname];
    
    
    NSArray <NSString *>*items = [item.kaibang_time componentsSeparatedByString:@" "];
    
    NSMutableAttributedString *attributeds = [[NSMutableAttributedString alloc]initWithString:@"开榜时间:" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName : [UIFont systemFontOfSize:14]}];
    
    if (items != nil) {
        [attributeds appendAttributedString:[[NSAttributedString alloc]initWithString:items.firstObject attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName : [UIFont systemFontOfSize:14]}]];
        
        [attributeds appendAttributedString:[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@",items.lastObject] attributes:@{NSForegroundColorAttributeName:[UIColor yellowColor],NSFontAttributeName : [UIFont systemFontOfSize:14]}]];
    }

    self.timeLabel.attributedText = attributeds;
}


- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [ self setRadian:30];
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)setRadian:(CGFloat) radian
{
    if(radian == 0) return;
    CGFloat t_width = CGRectGetWidth(self.frame); // 宽
    CGFloat t_height = CGRectGetHeight(self.frame); // 高
    CGFloat height = fabs(radian); // 圆弧高度
    CGFloat x = 0;
    CGFloat y = 0;
    
    // 计算圆弧的最大高度
    
    CGFloat _maxRadian =  MIN(t_height, t_width / 2);
    
    if(height > _maxRadian){
        NSLog(@"圆弧半径过大, 跳过设置。");
        return;
    }
    
    // 计算半径
    CGFloat radius = 0;
    
    CGFloat c = sqrt(pow(t_width / 2, 2) + pow(height, 2));
    CGFloat sin_bc = height / c;
    radius = c / ( sin_bc * 2);
    
    
    // 画圆
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setFillColor:[[UIColor whiteColor] CGColor]];
    CGMutablePathRef path = CGPathCreateMutable();
    
    if(radian > 0){
        CGPathMoveToPoint(path,NULL, t_width,t_height - height);
        CGPathAddArc(path,NULL, t_width / 2, t_height - radius, radius, asin((radius - height ) / radius), M_PI - asin((radius - height ) / radius), NO);
    }else{
        CGPathMoveToPoint(path,NULL, t_width,t_height);
        CGPathAddArc(path,NULL, t_width / 2, t_height + radius - height, radius, 2 * M_PI - asin((radius - height ) / radius), M_PI + asin((radius - height ) / radius), YES);
    }
    CGPathAddLineToPoint(path,NULL, x, y);
    CGPathAddLineToPoint(path,NULL, t_width, y);
    
    
    CGPathCloseSubpath(path);
    [shapeLayer setPath:path];
    CFRelease(path);
    self.layer.mask = shapeLayer;
}


@end
