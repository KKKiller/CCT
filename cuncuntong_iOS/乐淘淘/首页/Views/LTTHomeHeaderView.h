//
//  LTTHomeHeaderReusableView.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,Energy){
    
    Energy25,
    Energy50
};



@protocol EnergySelectionDelegate <NSObject>

- (void)energySelection:(Energy)energy;

@end


@interface LTTHomeHeaderView : UIView

@property (nonatomic,weak)id<EnergySelectionDelegate> delegate;

- (void)clearSelected;

@property (nonatomic,strong)NSArray <NSString *>*images;

@end

NS_ASSUME_NONNULL_END
