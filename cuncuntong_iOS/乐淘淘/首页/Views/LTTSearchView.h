//
//  LTTSearchView.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTSearchView : UIView

@property (nonatomic,readonly)UITextField *textField;

@end

NS_ASSUME_NONNULL_END
