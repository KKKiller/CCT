//
//  LTTLotteryCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/31.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLotteryCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+SubString.h"
@interface LTTLotteryCell ()
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *h_imageView;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *numbers;

@end

@implementation LTTLotteryCell


- (void)setItem:(LTTLotteryHeaderItem *)item{
    _item = item;
    
    NSTimeInterval time = [item.lottery_time integerValue];

    NSDateFormatter *formatter = NSDateFormatter.new;

    formatter.dateFormat = @"YYYY.MM.dd HH:mm:ss";
    
   NSString *timeStr = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
    
    self.timeLabel.text = [NSString stringWithFormat:@"开奖日期:%@",timeStr];
    
    self.nicknameLabel.text = item.reader_name;
    
    [self.h_imageView sd_setImageWithURL:[NSURL URLWithString:item.portrait]];
    
    NSArray *items = [NSString stringWithFormat:@"%06d",[item.lottery_numbers intValue]].substrings;
    
    for (int i = 0 ; i< self.numbers.count; i++) {
        
        UILabel *label = self.numbers[i];
        
        label.text = items[i];
    }
   
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
