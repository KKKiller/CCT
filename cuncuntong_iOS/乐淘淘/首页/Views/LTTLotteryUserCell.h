//
//  LTTLotteryUserCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/1.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTLotteryItem.h"
#import "LTTLineCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTLotteryUserCell : LTTLineCell

@property (nonatomic,strong)LTTLotteryItem *item;

@end

NS_ASSUME_NONNULL_END
