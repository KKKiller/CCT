//
//  LTTLatestPrizeCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLatestPrizeCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTLatestPrizeCell ()<timeDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *hLabel;

@property (weak, nonatomic) IBOutlet UILabel *mLabel;

@property (weak, nonatomic) IBOutlet UILabel *sLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *b_imageView;
@property (weak, nonatomic) IBOutlet UIView *lastView;
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *h_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *l_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *r_constraint;

@end

@implementation LTTLatestPrizeCell

- (void)setCollection:(PrizeCollection *)collection{
    
    _collection = collection;
    
    if(collection.countdown != nil){
        
        collection.countdown.delegate = self;
        
        [self.imageView sd_setImageWithURL:LTTImgaeURL(collection.countdown.prize_cover_picture)];
        
        self.hLabel.text = collection.countdown.hour;
        self.mLabel.text = collection.countdown.minute;
        self.sLabel.text = collection.countdown.second;
        
        [collection.countdown open];
        self.firstView.hidden = NO;
        self.l_constraint.active = YES;
        self.r_constraint.active = YES;
        
    }else{
        self.l_constraint.active = NO;
        self.firstView.hidden = YES;
        self.r_constraint.active = NO;
    }
    
    self.lastView.hidden = collection.began_draw == nil ? YES : NO;
    
    self.descriptionLabel.text =  collection.began_draw.prize_title;
    
    [self.b_imageView sd_setImageWithURL:LTTImgaeURL(collection.began_draw.prize_cover_picture)];
}



- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.imageView.superview addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(awaitLotteryEventAction:)]];
    [self.b_imageView.superview addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lootteryEventAction:)]];
    
    
    // Initialization code
}
- (void)awaitLotteryEventAction:(id)sender {
    
    if([self.delegate respondsToSelector:@selector(upcomingAwardsWith:)]){
        
        [self.delegate upcomingAwardsWith:self.collection.countdown];
    }
    
}
- (void)lootteryEventAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(lotteryWith:)]) {
        [self.delegate lotteryWith:self.collection.began_draw];
    }
    
}

- (void)timeUpdateWith:(LTTOpenPrizeItem *)item {
    self.hLabel.text = item.hour;
    self.mLabel.text = item.minute;
    self.sLabel.text = item.second;
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
    
    UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    CGFloat width = UIScreen.mainScreen.bounds.size.width - 24;
    CGRect frame = attributes.frame;
    
    frame.size.width = width;
    
    frame.size.height = 54.0/133.0 * width;
    
    attributes.frame = frame;
    
    return  attributes;

}


@end
