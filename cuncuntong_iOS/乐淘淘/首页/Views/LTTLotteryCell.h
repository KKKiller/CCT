//
//  LTTLotteryCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/31.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTLotteryItem.h"
#import "LTTLineCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTLotteryCell : LTTLineCell

@property (nonatomic,strong)LTTLotteryHeaderItem *item;

@end

NS_ASSUME_NONNULL_END
