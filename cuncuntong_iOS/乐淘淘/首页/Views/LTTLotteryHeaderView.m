//
//  LTTLotteryHeaderView.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/30.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLotteryHeaderView.h"
#import "LTTPhaseView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "QiCardView.h"

@interface CardImageCell : QiCardViewCell

@property (nonatomic,strong)UIImageView *imageView;

@end

@implementation CardImageCell

- (UIImageView *)imageView{
    
    if(_imageView) return _imageView;
    
    _imageView = UIImageView.new;
    
    _imageView.layer.cornerRadius = 5;
    
    _imageView.layer.masksToBounds = YES;
    
    _imageView.clipsToBounds = YES;
        
    [self addSubview:_imageView];
    
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    return _imageView;
}


@end

@interface LTTLotteryHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet LTTPhaseView *phaseView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end


@implementation LTTLotteryHeaderView

- (void)setItem:(LTTLotteryHeaderItem *)item{
    _item = item;
    
    
    self.titleLabel.text = item.prize_title;
    
    NSString *str = [NSString stringWithFormat:@"价值%@元 (%@能量",item.prize_price,item.energy];
    
    self.energyLabel.text = str;
    
    self.phaseView.total = [item.number_of_lottery intValue];
    
    self.phaseView.current = [item.current_of_lottery intValue];
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:item.prize_cover_picture]];
    

    
}

- (void)awakeFromNib{
    
    [super awakeFromNib];
//    self.carview.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
//    self.carview.dataSource = self;
//    self.carview.delegate = self;
//    self.carview.visibleCount = 4;
//    self.carview.lineSpacing = 15.0;
//    self.carview.interitemSpacing = 10.0;
//    self.carview.maxAngle = 10.0;
//    self.carview.isAlpha = YES;
//    self.carview.maxRemoveDistance = 100.0;
//    self.carview.layer.cornerRadius = 10.0;
//    [self.carview registerClass:[CardImageCell class] forCellReuseIdentifier:@"Cell"];
    
}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */



@end
