//
//  LTTHomeCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTHomeCell.h"

@implementation LTTHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
    CGFloat width = UIScreen.mainScreen.bounds.size.width -  36;
    UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    
    CGRect frame = attributes.frame;
    
    frame.size.width = width / 2.0;
    
    CGFloat height = (width / 2.0) + self.titleLabel.bounds.size.height + 60;
    
    frame.origin.x = 12;
    frame.size.height = height;
    
    attributes.frame = frame;
    
    return attributes;
}



@end
