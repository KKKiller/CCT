//
//  LTTLatestPrizeCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTPrizeItem.h"
NS_ASSUME_NONNULL_BEGIN

@protocol PrizeDelegate <NSObject>

- (void)upcomingAwardsWith:(LTTOpenPrizeItem *)item;
- (void)lotteryWith:(LTTOpenPrizeItem *)item;
@end

@interface LTTLatestPrizeCell : UICollectionViewCell

@property (nonatomic,strong)PrizeCollection *collection;

@property (nonatomic,weak)id<PrizeDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
