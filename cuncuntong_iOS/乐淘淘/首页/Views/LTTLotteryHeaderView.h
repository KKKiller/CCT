//
//  LTTLotteryHeaderView.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/30.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTLotteryItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTLotteryHeaderView : UIView

@property (nonatomic,strong)LTTLotteryHeaderItem *item;

@end

NS_ASSUME_NONNULL_END
