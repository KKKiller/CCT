//
//  LTTWaitingLotteryCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/8.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTLotteryItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTWaitingLotteryCell : UITableViewCell

@property (nonatomic,strong)LTTLotteryHeaderItem *item;
@end

NS_ASSUME_NONNULL_END
