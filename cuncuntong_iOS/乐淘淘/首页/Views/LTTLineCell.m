//
//  LTTLineCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/1.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLineCell.h"

@implementation LTTLineCell

- (instancetype)initWithCoder:(NSCoder *)coder{
    
    if (self = [super initWithCoder:coder]) {
        [self configuration];
    }
    
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self configuration];
    }
    
    return self;
}

- (void)configuration{
    
    UIView *line = UIView.new;
    
    line.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
    
    [self addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
