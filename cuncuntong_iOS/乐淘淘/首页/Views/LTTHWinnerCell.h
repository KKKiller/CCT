//
//  LTTWinnerCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/12/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTPrizeItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface LTTHWinnerCell : UITableViewCell

@property (nonatomic,strong)LTTOpenPrizeItem *item;

@end

NS_ASSUME_NONNULL_END
