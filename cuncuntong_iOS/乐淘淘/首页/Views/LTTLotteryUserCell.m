//
//  LTTLotteryUserCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/1.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLotteryUserCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTLotteryUserCell ()
@property (weak, nonatomic) IBOutlet UIImageView *h_imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ipLabel;
@property (weak, nonatomic) IBOutlet UIButton *countButton;

@end

@implementation LTTLotteryUserCell

- (void)setItem:(LTTLotteryItem *)item{
    _item = item;
    
    [self.h_imageView sd_setImageWithURL:LTTImgaeURL(item.portrait)];
    
    self.titleLabel.text = item.realname;
    
    self.orderTimeLabel.text = [NSString stringWithFormat:@"下单时间:%@",item.create_time];
    
    self.ipLabel.text = [NSString stringWithFormat:@"IP:%@",item.ip];
    [self.countButton setTitle:[NSString stringWithFormat:@"下单%@手",item.count] forState:UIControlStateNormal];

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
