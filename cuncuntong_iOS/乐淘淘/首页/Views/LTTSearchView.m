//
//  LTTSearchView.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/15.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTSearchView.h"

@interface LTTSearchView (){
    
    UIView *searchBackgrund;
    
    UITextField *textField;
}

@end


@implementation LTTSearchView

- (UITextField *)textField{
    
    return textField;
}

- (instancetype)initWithCoder:(NSCoder *)coder{
    if (self = [super initWithCoder:coder]) {
        [self configuration];
    }
    
    return self;
}

- (instancetype)init{
    
    if (self = [super init] ) {
        
        [self configuration];
        
    }
    
    return self;
}

- (void)updateConstraints{
    
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.height.mas_equalTo(52);
    }];
    
    [searchBackgrund mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self).insets(UIEdgeInsetsMake(0, 12, 12, 12));
        
    }];
    
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.right.equalTo(searchBackgrund).inset(106);
        make.centerY.equalTo(searchBackgrund);
        
    }];
    
    [super updateConstraints];
}

- (void)configuration{
    
    [self setBackgroundColor:HEXCOLOR(0xED4741)];
    
    searchBackgrund = [[UIView alloc]init];
    
    searchBackgrund.backgroundColor = [UIColor whiteColor];
    
    searchBackgrund.layer.cornerRadius = 5.0;
    
    searchBackgrund.layer.masksToBounds = YES;

    [self addSubview:searchBackgrund];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    
    imageView.image = [UIImage imageNamed:@"0-1"];
    
    imageView.frame = CGRectMake(0, 0, 15, 15);
    
    textField = [[UITextField alloc]init];
    
    textField.placeholder = @"搜索奖品";
    
    textField.leftView = imageView;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    [searchBackgrund addSubview:textField];
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
