//
//  LTTHomeHeaderReusableView.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/16.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTHomeHeaderView.h"
#import "WCCycleScrollView.h"
@interface LTTHomeHeaderView ()

@property (weak, nonatomic) IBOutlet UIView *leftEnergy;
@property (weak, nonatomic) IBOutlet UIView *rightEnergy;
@property (weak, nonatomic) IBOutlet WCCycleScrollView *cycleScrollView;

@end

@implementation LTTHomeHeaderView

- (instancetype)initWithCoder:(NSCoder *)coder{
    
    if (self = [super initWithCoder:coder]) {
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo([UIScreen mainScreen].bounds.size.width);
        }];
    }
    
    return self;
}

- (void)clearSelected {
    
    [self setTextColor:HEXCOLOR(0xF7CC35) view:self.rightEnergy];
    [self setTextColor:HEXCOLOR(0xF7CC35) view:self.leftEnergy];
}

- (void)leftTapEventAction:(id)sender {
    [self setTextColor:HEXCOLOR(0xF7CC35) view:self.rightEnergy];
    [self setTextColor:HEXCOLOR(0xFF4261) view:self.leftEnergy];
    
    if ([self.delegate respondsToSelector:@selector(energySelection:)]) {
        [self.delegate energySelection:Energy25];
    }
    
}

- (void)rightTapEventAction:(id)sender {
    [self setTextColor:HEXCOLOR(0xF7CC35) view:self.leftEnergy];
    [self setTextColor:HEXCOLOR(0xFF4261) view:self.rightEnergy];
    
    if ([self.delegate respondsToSelector:@selector(energySelection:)]) {
        [self.delegate energySelection:Energy50];
    }
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setTextColor:HEXCOLOR(0xF7CC35) view:self.leftEnergy];
    [self setTextColor:HEXCOLOR(0xF7CC35) view:self.rightEnergy];
    
    [self.leftEnergy addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftTapEventAction:)]];
    
    [self.rightEnergy addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightTapEventAction:)]];
    
    self.cycleScrollView.imageURLStringGroup = @[[UIImage imageNamed:@"0-2"],
                                                 [UIImage imageNamed:@"banner1"]];
    
}

- (void)setImages:(NSArray<NSString *> *)images{
    _images = images;
    self.cycleScrollView.imageURLStringGroup = images;
    
}

- (void)setTextColor:(UIColor *)color view:(UIView *)view{
    
    for (UIView *subview in view.subviews) {
        
        if ([subview isKindOfClass:[UILabel class]]) {
            
            UILabel *label = (UILabel *)subview;
            
            label.textColor = color;
            
        }
    }
}






@end
