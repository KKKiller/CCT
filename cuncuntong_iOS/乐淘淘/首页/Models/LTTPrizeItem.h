//
//  LTTPrizeItem.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/18.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>
@class LTTOpenPrizeItem;
NS_ASSUME_NONNULL_BEGIN
@interface LTTPrizeItem : NSObject

/// <#Description#>
@property (nonatomic,copy)NSString *current_of_lottery;
/// <#Description#>
@property (nonatomic,copy)NSString *energy_for_lucky_draw;
/// 期id
@property (nonatomic,copy)NSString *ID;
/// 开奖进度
@property (nonatomic,copy)NSString *lottery_schedule;
/// <#Description#>
@property (nonatomic,copy)NSString *number_of_lottery;
/// 奖品封面图片
@property (nonatomic,copy)NSString *prize_cover_picture;
/// 奖品标题名称
@property (nonatomic,copy)NSString *prize_title;
/// 开奖状态 （1进行中 2倒计时中 3开奖中 4已完成）
@property (nonatomic,copy)NSString *state;
/// 抽奖所需能量（1-25克，2-50克，3-75克，4-100克
@property (nonatomic,readonly)NSString *number;
@end


@protocol timeDelegate <NSObject>

- (void)timeUpdateWith:(LTTOpenPrizeItem *)item;

@end

@interface LTTOpenPrizeItem : NSObject

@property (nonatomic,copy)NSString *ID;
/// 开奖号码
@property (nonatomic,copy)NSString *lottery_numbers;
/// 开奖时间
@property (nonatomic,copy)NSString *lottery_time;
/// 奖品名称标题
@property (nonatomic,copy)NSString *prize_title;
/// 期ID （该抽奖产品是第几期
@property (nonatomic,copy)NSString *dp_id;
/// 总参与人次
@property (nonatomic,copy)NSString *total_participation;
/// 奖品封面图
@property (nonatomic,copy)NSString *prize_cover_picture;
/// （2倒计时中 3开奖中）
@property (nonatomic,copy)NSString *state;

@property (nonatomic,copy)NSString *qi;


@property (nonatomic,weak)id<timeDelegate> delegate;

@property (nonatomic,readonly)NSString *hour;
@property (nonatomic,readonly)NSString *minute;
@property (nonatomic,readonly)NSString *second;

@property (nonatomic,copy)NSString *now_qi;

@property (nonatomic,copy)NSString *realname;

@property (nonatomic,copy)NSString *kaibang_time;


- (void)open;

@end


@interface PrizeCollection : NSObject

+ (instancetype)collectionWithBegan_draw:(LTTOpenPrizeItem *)began_draw countdown:(LTTOpenPrizeItem *)countdown;


/// 可以抽奖
@property (nonatomic,strong)LTTOpenPrizeItem *began_draw;

/// 倒计时
@property (nonatomic,strong)LTTOpenPrizeItem *countdown;

@end


NS_ASSUME_NONNULL_END
