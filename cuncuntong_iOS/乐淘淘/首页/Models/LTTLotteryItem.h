//
//  LTTLotteryItem.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class LTTLotteryHeaderItem;
@protocol timerDelegate <NSObject>

- (void)timeUpdateWith:(LTTLotteryHeaderItem *)item;

@end

@interface LTTLotteryHeaderItem : NSObject

@property (nonatomic,copy)NSString *prize_cover_picture;

///  当前参与人次
@property (nonatomic,copy)NSString *current_of_lottery;
/// 抽奖所需能量（1-25克，2-50克，3-75克，4-100克）
@property (nonatomic,copy)NSString *energy_for_lucky_draw;
/// 期表ID
@property (nonatomic,copy)NSString *ID;
/// 开奖时间
@property (nonatomic,copy)NSString *lottery_time ;
/// 开奖总人次
@property (nonatomic,copy)NSString *number_of_lottery;
/// 开奖活动图集
@property (nonatomic,copy)NSString * prize_pictures ;

@property (nonatomic,strong)NSArray *images;
/// 开奖活动名称
@property (nonatomic,copy)NSString * prize_title;
/// 开奖号码
@property (nonatomic,copy)NSString *lottery_numbers;
/// 奖品价格
@property (nonatomic,copy)NSString *prize_price;

@property (nonatomic,readonly)NSString *energy;

/// 用户名称
@property (nonatomic,copy)NSString *reader_name;
/// 用户头像
@property (nonatomic,copy)NSString *portrait;


@property (weak,nonatomic)id<timerDelegate> delegate;

@property (nonatomic,readonly)NSString *hour;
@property (nonatomic,readonly)NSString *minute;
@property (nonatomic,readonly)NSString *second;
@property (nonatomic,copy)NSString *next_qid;
- (void)open;

@end

@interface LTTLotteryItem : NSObject
/// 用户下单次数
@property (nonatomic,copy)NSString *count;
/// 用户ip地址
@property (nonatomic,copy)NSString *ip;
/// 用户头像
@property (nonatomic,copy)NSString *portrait;
/// 用户名字
@property (nonatomic,copy)NSString *realname;

/// 创建时间
@property (nonatomic,copy)NSString *create_time;

@end

NS_ASSUME_NONNULL_END
