//
//  LTTPrizeItem.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/18.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPrizeItem.h"

@implementation LTTPrizeItem


+ (NSDictionary *)modelCustomPropertyMapper{
    
    return @{@"ID":@[@"id"]};
}

- (NSString *)number{
    
    switch ([self.energy_for_lucky_draw intValue]) {
        case 1:
            return @"25";
            break;
        case 2:
            return @"50";
        case 3:
            return @"75";
        case 4:
            return @"100";
        default:
            return @"未知";
            break;
    }
}

@end




@interface LTTOpenPrizeItem (){
    NSTimer *timer;
    NSTimeInterval future;
    NSString *hour;
    NSString *minute;
    NSString *second;
}

@end

@implementation LTTOpenPrizeItem

- (instancetype)init{
    
    if (self = [super init]) {
        
        __weak typeof(self)weak = self;
        
        timer = [NSTimer timerWithTimeInterval:1.0 block:^(NSTimer * _Nonnull timer) {
            [weak timerRun];
        } repeats:YES];
        [timer setFireDate:[NSDate distantFuture]];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
    
    return self;
}
+ (NSDictionary *)modelCustomPropertyMapper{
    
    return @{@"ID":@[@"id"]};
}

- (void)setLottery_time:(NSString *)lottery_time {
    
    NSDateFormatter *formatter = NSDateFormatter.new;
    
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
   NSDate *date =  [formatter dateFromString:lottery_time];
    
    
    future = date.timeIntervalSince1970;
    _lottery_time = [NSString stringWithFormat:@"%f",future] ;


    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    
    if (future > now) {
        
        NSTimeInterval interval =   future - now ;
        
        NSInteger h = interval / 3600;
        NSInteger m = (NSInteger)interval / 60 % 60;
        NSInteger sec = (NSInteger)interval % 60;
        
        hour = [NSString stringWithFormat:@"%02ld",h];
        minute = [NSString stringWithFormat:@"%02ld",m];
        second = [NSString stringWithFormat:@"%02ld",sec];
    }else{
        hour = @"00";
        minute = @"00";
        second = @"00";
    }
    
}

- (void)timerRun{
    
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    
    if (future > now) {
        
        NSTimeInterval interval =   future - now ;
        
        NSInteger h = interval / 3600;
        NSInteger m = (NSInteger)interval / 60 % 60;
        NSInteger sec = (NSInteger)interval % 60;
        
        hour = [NSString stringWithFormat:@"%02ld",h];
        minute = [NSString stringWithFormat:@"%02ld",m];
        second = [NSString stringWithFormat:@"%02ld",sec];
        
    }else {
        
        hour = @"00";
        minute = @"00";
        second = @"00";
        [timer invalidate];
        timer = nil;
    }
    
    if ([self.delegate respondsToSelector:@selector(timeUpdateWith:)]) {
        [self.delegate timeUpdateWith:self];
    }

}

- (NSString *)hour{
    return hour;
}
- (NSString *)minute{
    return minute;
}

- (NSString *)second{
    return second;
}

- (void)open{
    [timer setFireDate:[NSDate date]];
}

- (void)dealloc{
    [timer invalidate];
    timer = nil;
}

@end


@implementation PrizeCollection

+ (instancetype)collectionWithBegan_draw:(LTTOpenPrizeItem *)began_draw countdown:(LTTOpenPrizeItem *)countdown{
    
    if (began_draw == nil && countdown == nil) { return nil ; }
    
    PrizeCollection *collection = [[PrizeCollection alloc]init];
    
    collection.began_draw = began_draw;
    collection.countdown = countdown;
    
    return collection;
}



@end
