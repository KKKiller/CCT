//
//  LTTLotteryItem.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLotteryItem.h"

@interface LTTLotteryHeaderItem (){
    
    NSTimer *timer;
    NSString *hour;
    NSString *minute;
    NSString *second;
    NSTimeInterval future;
    
}

@end

@implementation LTTLotteryHeaderItem

- (instancetype)init{
    
    if (self = [super init]) {
        
        __weak typeof(self)weak = self;
        
        timer = [NSTimer timerWithTimeInterval:1.0 block:^(NSTimer * _Nonnull timer) {
            [weak timerRun];
        } repeats:YES];
        [timer setFireDate:[NSDate distantFuture]];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
    
    return self;
}

+ (NSDictionary *)modelCustomPropertyMapper{
    
    return @{@"ID":@[@"id"]};
}

- (NSString *)energy{
    
    switch ([self.energy_for_lucky_draw intValue]) {
        case 1:
            return @"25";
            break;
        case 2:
            return @"50";
        case 3:
            return @"75";
        case 4:
            return @"100";
        default:
            return @"0";
            break;
    }
}

- (void)timerRun{
    
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    
    if (future > now) {
        
        NSTimeInterval interval =   future - now ;
        
        NSInteger h = interval / 3600;
        NSInteger m = (NSInteger)interval / 60 % 60;
        NSInteger sec = (NSInteger)interval % 60;
        
        hour = [NSString stringWithFormat:@"%02ld",h];
        minute = [NSString stringWithFormat:@"%02ld",m];
        second = [NSString stringWithFormat:@"%02ld",sec];
        
    }else {
        
        hour = @"00";
        minute = @"00";
        second = @"00";
        [timer invalidate];
        timer = nil;
    }
    
    if ([self.delegate respondsToSelector:@selector(timeUpdateWith:)]) {
        [self.delegate timeUpdateWith:self];
    }
    
}

- (void)setPrize_pictures:(NSString *)prize_pictures{
    _prize_pictures = prize_pictures;
    
    self.images = [prize_pictures componentsSeparatedByString:@","];
}

- (void)setLottery_time:(NSString *)lottery_time {
    
    NSArray *arr = [lottery_time componentsSeparatedByString:@"-"];
    
    
    
    if (arr.count > 1) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        
        future =  [formatter dateFromString:lottery_time].timeIntervalSince1970;
        
        _lottery_time = [NSString stringWithFormat:@"%f",future];
        
    }else{
        future = [lottery_time integerValue];
        _lottery_time = lottery_time;
    }
    
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    
    if (future > now) {
        
        NSTimeInterval interval =   future - now ;
        
        NSInteger h = interval / 3600;
        NSInteger m = (NSInteger)interval / 60 % 60;
        NSInteger sec = (NSInteger)interval % 60;
        
        hour = [NSString stringWithFormat:@"%02ld",h];
        minute = [NSString stringWithFormat:@"%02ld",m];
        second = [NSString stringWithFormat:@"%02ld",sec];
    }else{
        hour = @"00";
        minute = @"00";
        second = @"00";
    }
    
}


- (NSString *)hour{
    return hour;
}
- (NSString *)minute{
    return minute;
}

- (NSString *)second{
    return second;
}

- (void)open{
    [timer setFireDate:[NSDate date]];
}

- (void)dealloc{
    [timer invalidate];
    timer = nil;
}

@end

@implementation LTTLotteryItem

@end






