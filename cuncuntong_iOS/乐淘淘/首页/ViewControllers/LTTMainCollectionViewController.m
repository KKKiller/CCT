//
//  LTTMainCollectionViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/25.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTMainCollectionViewController.h"
#import "LTTHomeCell.h"
#import "LTTLatestPrizeCell.h"
#import "LTTAwaitLotteryViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface LTTHomeCollectionViewFlowLayout : UICollectionViewFlowLayout

@end

@implementation LTTHomeCollectionViewFlowLayout

- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect{
    
    NSArray * layoutAttributes_t = [super layoutAttributesForElementsInRect:rect];
    
    
    for (NSUInteger index = 0; index < layoutAttributes_t.count ; index++) {
        
        UICollectionViewLayoutAttributes *currentAttr = layoutAttributes_t[index]; //
        
        if (currentAttr.indexPath.section == 1) {
            if (currentAttr.indexPath.row % 2 == 0) {
                currentAttr.frame = CGRectMake(12, currentAttr.frame.origin.y, currentAttr.frame.size.width, currentAttr.frame.size.height);
            }else{
                
                currentAttr.frame = CGRectMake(currentAttr.frame.size.width + 24, currentAttr.frame.origin.y, currentAttr.frame.size.width, currentAttr.frame.size.height);
            }
        }
    }
    return layoutAttributes_t;
}




@end


@interface LTTMainCollectionViewController ()<PrizeDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@end

@implementation LTTMainCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    LTTHomeCollectionViewFlowLayout *flowLayout = (LTTHomeCollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    
    
    flowLayout.sectionInset = UIEdgeInsetsMake(12, 12, 12, 12);
    
    flowLayout.minimumLineSpacing = 12;
    
    flowLayout.minimumInteritemSpacing = 12;
    
    flowLayout.estimatedItemSize = CGSizeMake((self.view.width - 36) / 2.0 , 200);
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTTHomeCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTTLatestPrizeCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"PC"];
}



#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (section == 0) return self.collection != nil  ? 1 : 0;
    
    return self.items.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        LTTLatestPrizeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PC" forIndexPath:indexPath];
        
        cell.collection = self.collection;
        
        cell.delegate = self;
        
        return cell;
        
    }
    
    LTTHomeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    LTTPrizeItem *item = self.items[indexPath.row];
    
    cell.titleLabel.text =  item.prize_title;
    
    cell.number.text = item.number;
    
    cell.progress.progress = [item.lottery_schedule floatValue] / 100.0;
    
    cell.percentage.text = [NSString stringWithFormat:@"%@%%",item.lottery_schedule];
    
    [cell.l_imageView sd_setImageWithURL:LTTImgaeURL(item.prize_cover_picture)];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) { return ;}
    
    LTTPrizeItem *item = self.items[indexPath.row];
    
    switch ([item.state intValue]) {
        case 2:{
            
            LTTWaitingLotteryViewController *lottery = [[LTTWaitingLotteryViewController alloc] initWithNibName:@"LTTWaitingLotteryViewController" bundle:nil];
            
            lottery.q_id  = item.ID;
            lottery.state = item.state;
            
            [self.navigationController pushViewController:lottery animated:YES];
            break;
        }
        case 3:{
            LTTLotteryViewController *lottery = [[LTTLotteryViewController alloc] initWithNibName:@"LTTLotteryViewController" bundle:nil];
            
            lottery.q_id  = item.ID;
            lottery.state = item.state;
            [self.navigationController pushViewController:lottery animated:YES];
            
            break;
        }
            
        default:{
            LTTReadyViewController *ready = LTTReadyViewController.new;
            
            ready.q_id = item.ID;
            ready.state = item.state;
            
            [self.navigationController pushViewController:ready animated:YES];
        }
    }
    
}
#pragma mark - 最新开奖 PrizeDelegate

- (void)upcomingAwardsWith:(LTTOpenPrizeItem *)item{
    
    LTTAwaitLotteryViewController *await = [[LTTAwaitLotteryViewController alloc] initWithNibName:@"LTTAwaitLotteryViewController" bundle:nil];
    
    await.open = WaitingForPrizes;
    
    [self.navigationController pushViewController:await animated:YES];
}

- (void)lotteryWith:(LTTOpenPrizeItem *)item{
    
    LTTAwaitLotteryViewController *await = [[LTTAwaitLotteryViewController alloc] initWithNibName:@"LTTAwaitLotteryViewController" bundle:nil];
    
    await.open = Lottery;
    
    [self.navigationController pushViewController:await animated:YES];
}

#pragma -mark NestedDelegate
-(UIScrollView *)getNestedScrollView{
    return self.collectionView;
}
- (void)reloadData{
    
    [self.collectionView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
