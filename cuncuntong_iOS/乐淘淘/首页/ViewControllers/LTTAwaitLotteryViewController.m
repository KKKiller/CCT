//
//  LTTAwaitLotteryViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTAwaitLotteryViewController.h"
#import "LTTAwaitLotteryView.h"
#import "LTTAwaitLotteryCell.h"
#import "LTTWaitingLotteryViewController.h"
#import "LTTLotteryViewController.h"
#import "LTTReadyViewController.h"
#import "LTTHWinnerCell.h"
@interface LTTAwaitLotteryViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)LTTAwaitLotteryView *headerLotteryView;

@property (nonatomic,strong)NSArray <LTTOpenPrizeItem*> *items;

@end

@implementation LTTAwaitLotteryViewController

#pragma mark - 懒加载

- (LTTAwaitLotteryView *)headerLotteryView{
    
    if (_headerLotteryView) return _headerLotteryView;
    
    _headerLotteryView = [[UINib nibWithNibName:@"LTTAwaitLotteryView" bundle:nil] instantiateWithOwner:self options:nil].firstObject;
    
    _headerLotteryView .frame = CGRectMake(0, 0, self.view.width, 200);
    
    return _headerLotteryView;
}

- (NSString *)title{
    
    return @"最新开榜";
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
    [self loadData];
}

#pragma mark - 配置
- (void)configuration{
    
    self.tableView.rowHeight = 150;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTAwaitLotteryCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTHWinnerCell" bundle:nil] forCellReuseIdentifier:@"Cell1"];
    
}

- (void)setHeaderWith:(LTTOpenPrizeItem *)item{
    
    
    if (item) {
        self.headerLotteryView.item = item;
    
        UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 240)];
        header.backgroundColor = [UIColor whiteColor];
        [header addSubview:self.headerLotteryView];
        
        [self.tableView setTableHeaderView:header];
    }
    
}

#pragma mark - 网络
- (void)loadData{
    
    __weak typeof(self)weak = self;
    
    [[MyNetWorking sharedInstance]LTTGetUrl:LTTURL(@"newBeganLuckyDraw") params:nil target:self success:^(NSDictionary *success) {
        
        if ([success[@"code"] intValue] == 0) {
            
            switch (self.open) {
                case WaitingForPrizes:{
                    
                    NSDictionary *data = success[@"data"];
                    
                    [weak setHeaderWith:[LTTOpenPrizeItem modelWithJSON:data[@"public_data"]]];
                    
                    NSArray *items = [NSArray modelArrayWithClass:[LTTOpenPrizeItem class] json:data[@"countdown"]];
                    
                    weak.items = items;
                    break;
                }
                case Lottery:{
                    
                    NSMutableArray *items = [NSMutableArray arrayWithArray:[NSArray modelArrayWithClass:[LTTOpenPrizeItem class] json:success[@"data"][@"began_draw"]]];
                    
                     [weak setHeaderWith:[LTTOpenPrizeItem modelWithJSON:success[@"data"][@"public_data"]]];
                    
                    weak.items =items;
                    break;
                }
            }
            
            [weak.tableView reloadData];
            
        }
        
    } failure:^(NSError *failure) {
        SHOW(failure.localizedDescription);
    }];
    
    
    
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (self.open) {
        case WaitingForPrizes:{
            LTTAwaitLotteryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
             
             cell.item = self.items[indexPath.section];
             
             return cell;
            break;
        }
        case Lottery:{
            LTTHWinnerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
             
             cell.item = self.items[indexPath.section];
             
             return cell;

            break;
        }
    }
    
 
}



#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footer = [UIView new];
    footer.backgroundColor = [UIColor groupTableViewBackgroundColor];
    return  footer;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTOpenPrizeItem *item = self.items[indexPath.section];
    
    switch ([item.state intValue]) {
        case 2:{
            
            LTTWaitingLotteryViewController *lottery = [[LTTWaitingLotteryViewController alloc] initWithNibName:@"LTTWaitingLotteryViewController" bundle:nil];
            
            lottery.q_id = item.ID;
            lottery.state = item.state;
            
            [self.navigationController pushViewController:lottery animated:YES];
            break;
        }
        default:
            break;
            //        case 3:{
            //            LTTLotteryViewController *lottery = [[LTTLotteryViewController alloc] initWithNibName:@"LTTLotteryViewController" bundle:nil];
            //
            //            lottery.q_id = item.ID;
            //            lottery.state = item.state;
            //
            //            [self.navigationController pushViewController:lottery animated:YES];
            //
            //            break;
            //        }
            //
            //        default:{
            //            LTTReadyViewController *ready = LTTReadyViewController.new;
            //
            //            ready.q_id = item.ID;
            //            ready.state = item.state;
            //
            //            [self.navigationController pushViewController:ready animated:YES];
            //        }
    }
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
