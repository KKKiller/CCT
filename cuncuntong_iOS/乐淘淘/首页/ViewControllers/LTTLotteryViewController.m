//
//  LTTLotteryViewController.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLotteryViewController.h"
#import "LTTLotteryHeaderView.h"
#import "LTTLotteryItem.h"
#import "LTTLotteryCell.h"
#import "LTTLotteryUserCell.h"
#import "LTTLineCell.h"
#import "LTTPublishOrderViewController.h"
#import "LTTOrderViewController.h"
@interface LTTLotteryViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSArray <LTTLotteryItem *>*items;
@property (nonatomic,strong)LTTLotteryHeaderItem *user;
@end

@implementation LTTLotteryViewController

- (NSString *)title{
    return @"开榜结果";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configruation];
}

#pragma mark - 配置
- (void)configruation{
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTLotteryCell" bundle:nil] forCellReuseIdentifier:@"User"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTLotteryUserCell" bundle:nil] forCellReuseIdentifier:@"UserCell"];
    
    [self.tableView registerClass:[LTTLineCell class] forCellReuseIdentifier:@"Cell"];
    
    __weak typeof(self)weak = self;
    self.tableView.mj_header = [MJRefreshHeader headerWithRefreshingBlock:^{
        [weak loadData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - 网络
- (void)loadData{
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"DetailsOnTheLottery") params:@{@"q_id":self.q_id,@"state":self.state} target:self success:^(NSDictionary *success) {
        
        [self.tableView.mj_header endRefreshing];
        
        if ([success[@"error"] intValue] == 0) {
            
            [self setDataWithHeader:[NSArray modelArrayWithClass:[LTTLotteryHeaderItem class] json:success[@"data"][@"prize_info"]].firstObject items:[NSArray modelArrayWithClass:[LTTLotteryItem class] json:success[@"data"][@"user_info"]]];
        }else{
            SHOW(success[@"msg"]);
        }
        
    } failure:^(NSError *failure) {
        [self.tableView.mj_header endRefreshing];
        
        SHOW(failure.localizedDescription);
        
    }];
    
}

- (void)setDataWithHeader:(LTTLotteryHeaderItem *)header items:(NSArray<LTTLotteryItem*>*)items{
    
    self.items = items;
    self.user = header;
    
    LTTLotteryHeaderView *headerView = [[UINib nibWithNibName:@"LTTLotteryHeaderView" bundle:nil] instantiateWithOwner:nil options:nil].firstObject;
    
    headerView.item = header;
    
    [self.tableView setTableHeaderView:headerView];
    
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(self.view.width);
    }];
    
    [headerView layoutIfNeeded];
        
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) { return self.user == nil ? 0 : 3; }
    
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) return [self tableView:tableView cellHeaderForRowAtIndexPath:indexPath];
    
    
    LTTLotteryUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
    
    cell.item = self.items[indexPath.row];
    
    return cell;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellHeaderForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == 0) {
        LTTLotteryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"User" forIndexPath:indexPath];
        cell.item = self.user;
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == 1) {
        cell.textLabel.text = @"最新一期正在进行中,请立即前往>";
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (indexPath.row == 2) {
        cell.textLabel.text = @"晒单分享";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    
    cell.textLabel.textColor = HEXCOLOR(0x666666);
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0 && self.user != nil) {return 10 ; }
    
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if (section == 0) { return UIView.new; }
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.whiteColor;
    
    UILabel *label = UILabel.new;
    
    label.text = @"所有参与记录";
    
    label.textColor = HEXCOLOR(0x313131);
    
    label.font = [UIFont systemFontOfSize:12];
    
    [view addSubview:label];

    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).inset(12);
        make.centerY.equalTo(view);
    }];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if (section == 0) { return 10 ;}
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return  UIView.new;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 1:{
             
                LTTOrderViewController *order = LTTOrderViewController.new;
                
                order.energy = self.user.energy_for_lucky_draw;
                
                order.ID = self.user.ID;
                
                [self.navigationController pushViewController:order animated:YES];
                
                break;
            }
            case 2:{
             
                [self.navigationController pushViewController:LTTPublishOrderViewController.new animated:YES];
                break;
            }
            default:
                break;
        }
    }
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
