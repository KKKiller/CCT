//
//  LTTMainCollectionViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/25.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"
#import "LTTLotteryViewController.h"
#import "LTTWaitingLotteryViewController.h"
#import "LTTReadyViewController.h"
NS_ASSUME_NONNULL_BEGIN
@protocol NestedDelegate <NSObject>

-(UIScrollView *)getNestedScrollView;

@end
@interface LTTMainCollectionViewController : LTTBaseViewController<NestedDelegate>
@property (nonatomic,strong)NSArray <LTTPrizeItem *> *items;

@property (nonatomic,strong)PrizeCollection *collection;

- (void)reloadData;
@end

NS_ASSUME_NONNULL_END
