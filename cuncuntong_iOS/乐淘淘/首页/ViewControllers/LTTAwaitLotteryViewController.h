//
//  LTTAwaitLotteryViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"
typedef NS_ENUM(NSInteger,Open) {
    WaitingForPrizes,
    Lottery,
};

NS_ASSUME_NONNULL_BEGIN

@interface LTTAwaitLotteryViewController : LTTBaseViewController


@property (nonatomic,assign)Open open;
@end

NS_ASSUME_NONNULL_END
