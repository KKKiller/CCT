//
//  LTTHomeViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTHomeViewController.h"
#import "LTTSearchView.h"
#import "LTTHomeHeaderView.h"

#import "LTTPrizeItem.h"

#import "LTTAwaitLotteryViewController.h"
#import "LTTLotteryViewController.h"
#import "LTTWaitingLotteryViewController.h"
#import "LTTReadyViewController.h"
#import "LTTMainCollectionViewController.h"


static NSString *const kObseverKeyContentOffset = @"contentOffset";
@interface LTTHomeViewController ()<EnergySelectionDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet LTTSearchView *searchView;
@property (weak, nonatomic) IBOutlet LTTHomeHeaderView *headerView;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIView *bottomContentView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutBottomViewHeight;
@property (nonatomic,weak)id<NestedDelegate> currentNestedDelegate;

@property (nonatomic,copy)NSString *energy;

@property (nonatomic,weak)LTTMainCollectionViewController *mainCollection;




@end

@implementation LTTHomeViewController

- (NSString *)title {
    
    return @"乐淘淘";
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadprize];
    [self ltt_loadData];
}


- (LTTMainCollectionViewController *)mainCollection{
    
    return self.childViewControllers.firstObject;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configruation];
    [self loadbanner];
}


#pragma mark - 配置

- (void)configruation {
    self.searchView.textField.delegate = self;
    [self.headerView setDelegate:self];
    
    LTTMainCollectionViewController *collection = LTTMainCollectionViewController.new;
    [self addChildViewController:collection];
    
    [self.bottomContentView addSubview:collection.view];
    
    [collection.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.bottomContentView);
    }];
    
    self.layoutBottomViewHeight.constant =   self.view.height - 165;
    
    [self chooseNested:collection];
    
    self.mainScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(lttLoadData)];
    
    [self.mainScrollView.mj_header beginRefreshing];
    
}

- (void)lttLoadData{
    [self.headerView clearSelected];
    [self ltt_loadData];
}

-(void)chooseNested:(id<NestedDelegate>)delegate{
    if (_currentNestedDelegate) {
        [_currentNestedDelegate.getNestedScrollView removeObserver:self forKeyPath:kObseverKeyContentOffset];
    }
    
    _currentNestedDelegate = delegate;
    
    [_currentNestedDelegate.getNestedScrollView  addObserver:self forKeyPath:kObseverKeyContentOffset options:NSKeyValueObservingOptionNew context:nil];
    
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if ([kObseverKeyContentOffset isEqualToString:keyPath]) {
        [self scrollViewDidScroll:object];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //最大上滑距离
    CGFloat maxOffset = self.headerView.frame.size.height;
    CGFloat mainContentOffsetY = _mainScrollView.contentOffset.y;
    
    if (scrollView == _mainScrollView) {
        if (mainContentOffsetY > maxOffset) {
            [_mainScrollView setContentOffset:CGPointMake(0, maxOffset)];
        }else if(mainContentOffsetY < 0){
            [_mainScrollView setContentOffset:CGPointMake(0, 0)];
        }
    }else if (scrollView == _currentNestedDelegate.getNestedScrollView){
        UIScrollView *currentScrollView = _currentNestedDelegate.getNestedScrollView;
        CGFloat currentOffsetY = currentScrollView.contentOffset.y;
        //避免死循环
        if (currentOffsetY == 0) {
            return;
        }
        
        if(currentOffsetY < 0){
            //下滑
            if(mainContentOffsetY > 0){
                [_mainScrollView setContentOffset:CGPointMake(0, mainContentOffsetY+currentOffsetY)];
                [currentScrollView setContentOffset:CGPointMake(0, 0)];
            }
        }else if(currentOffsetY >0){
            //上拉
            if(mainContentOffsetY < maxOffset){
                [_mainScrollView setContentOffset:CGPointMake(0, mainContentOffsetY+currentOffsetY)];
                [currentScrollView setContentOffset:CGPointMake(0, 0)];
            }
        }
    }
}


#pragma mark - 开榜网络
- (void)loadprize{
    
    [[MyNetWorking sharedInstance] GetUrl:LTTURL(@"newBeganLuckyDraw") params:nil success:^(NSDictionary *success) {
        
        if ([success[@"error"] intValue] == 0) {
    
           self.mainCollection.collection = [PrizeCollection collectionWithBegan_draw:[NSArray modelArrayWithClass:[LTTOpenPrizeItem class] json:success[@"data"][@"began_draw"]].firstObject countdown:[NSArray modelArrayWithClass:[LTTOpenPrizeItem class] json:success[@"data"][@"countdown"]].firstObject];
            
            [self.mainCollection reloadData];
            
        }
    } failure:^(NSError *failure) {
        NSLog(@"%@",failure);
    }];
   
    
    
}

- (void)loadbanner{
    [[MyNetWorking sharedInstance]GetUrl:LTTURL(@"pageBanner") params:nil success:^(NSDictionary *success) {
           
           if ([success[@"error"] intValue] == 0) {
               
               if ([success[@"data"] isKindOfClass:[NSArray class]]) {
                   
                   NSArray *items = success[@"data"];
                   
                   NSMutableArray *images = [NSMutableArray array];
                  
                   for (NSDictionary *dic in items) { [images addObject:dic[@"banner"]]; }
                   
                   if (images.count > 0) { self.headerView.images = images; }
               
               }
               
           }
                   
       } failure:^(NSError *failure) {
           
       }];
}

//
//#pragma mark - 网络
- (void)ltt_loadData{
    
    __weak typeof(self) weak = self;
    
    NSDictionary *parameter = nil;
    
    if (self.energy.length > 0) {
        parameter = @{@"energy":self.energy};
    }
    
    if (self.searchView.textField.text.length > 0) {
        
        parameter = @{@"keywords":self.searchView.textField.text};
    }
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"searchPrize") params:parameter target:self success:^(NSDictionary *success) {
        [weak.mainScrollView.mj_header endRefreshing];
        if ([[success valueForKey:@"code"] intValue] == 0) {
            weak.mainCollection.items = [NSArray modelArrayWithClass:[LTTPrizeItem class] json:[success valueForKey:@"data"]];
            
            [weak.mainCollection reloadData];
            
        }
    } failure:^(NSError *failure) {
        [weak.mainScrollView.mj_header endRefreshing];
        SHOW(failure.localizedDescription);
    }];
    
}

#pragma mark - EnergySelectionDelegate 25克 or 50克选中代理

- (void)energySelection:(Energy)energy{
    
    switch (energy) {
        case Energy25:
            self.energy = @"1";
            break;
        case Energy50:
            self.energy = @"2";
            break;
        default:
            break;
    }
    
    self.searchView.textField.text = nil;
    
    [self ltt_loadData];
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.text.length > 0) {
        self.energy = nil;
        [self.headerView clearSelected];
        [self ltt_loadData];
    }
    return YES;
}

-(void)dealloc{
    if (_currentNestedDelegate) {
        [_currentNestedDelegate.getNestedScrollView removeObserver:self forKeyPath:kObseverKeyContentOffset];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
