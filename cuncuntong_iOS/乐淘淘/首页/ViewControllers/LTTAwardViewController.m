//
//  LTTPrizeViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTAwardViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface Prize : NSObject
@property (nonatomic,copy)NSString *prize_cover_picture;
@property (nonatomic,copy)NSString *prize_detail;
@property (nonatomic,copy)NSString *prize_pictures;
@property (nonatomic,strong)NSArray *images;

@end

@implementation Prize

- (void)setPrize_pictures:(NSString *)prize_pictures{
    _prize_pictures = prize_pictures;
    
    if ([prize_pictures isKindOfClass:[NSNull class]]) {
        return;
    }
    if ( [prize_pictures containsString:@"png"] ||  [prize_pictures containsString:@"jpg"]) {
        self.images = [prize_pictures componentsSeparatedByString:@","];
    }
}


@end




@interface LTTAwardViewController ()
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end

@implementation LTTAwardViewController

- (NSString *)title{
    
    return @"奖品详情";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
}

- (void)configuration{
    
    
    [[MyNetWorking sharedInstance] LTTPostURL:LTTURL(@"prizeDetails") params:@{@"id":self.ID} target:self success:^(NSDictionary *success) {
        
        if ([success[@"error"] intValue] == 0) {
            
            Prize *prize = [NSArray modelArrayWithClass:[Prize class] json:success[@"data"]].firstObject;
            
            [self setContext:prize.prize_detail images:prize.images];
            
        }else{
            SHOW(success[@"msg"]);
        }
        
        
    } failure:^(NSError *failure) {
        
        SHOW(failure.localizedDescription);
    }];
}

- (void)setContext:(NSString *)context images:(NSArray *)images{
    
    UIView *firstView = nil;
    
    if (context.length > 0) {
        
        UILabel *label = UILabel.new;
        
        label.text = context;
        
        label.numberOfLines = 0;
        
        label.font = [UIFont systemFontOfSize:14];
        
        label.textColor = HEXCOLOR(0x313131);
        
        [self.mainView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.right.equalTo(self.mainView).inset(12);
            make.top.equalTo(self.mainView).inset(12);
            
        }];
        
        firstView = label;
        
    }
    
    CGFloat width = UIScreen.mainScreen.bounds.size.width - 12;
    
    for (NSString *item in images) {
        
        if (item.length < 5) { continue ; }
        
        
        UIImageView *imageView = UIImageView.new;
        [self.mainView addSubview:imageView];
        
        
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:item] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            CGFloat height = width / image.size.width   * image.size.height;
            
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(height);
            }];
        }];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.mainView).inset(12);
            
            if (firstView != nil) {
                make.top.equalTo(firstView.mas_bottom).offset(12);
            }else{
                make.top.equalTo(self.mainView.mas_top).inset(12);
            }
        }];
        
        firstView = imageView;
    }
    
    [firstView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mainView);
    }];
    
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
