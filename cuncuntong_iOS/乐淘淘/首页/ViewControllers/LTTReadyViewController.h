//
//  LTTReadyViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTBaseViewController.h"
#import "LTTPrizeItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface LTTReadyViewController : LTTBaseViewController
@property (nonatomic,copy)NSString *q_id;
@property (nonatomic,copy)NSString *state;

@end

NS_ASSUME_NONNULL_END
