//
//  LTTReadyViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/24.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTReadyViewController.h"
#import "LTTLotteryHeaderView.h"
#import "LTTLotteryItem.h"
#import "LTTLotteryCell.h"
#import "LTTLotteryUserCell.h"
#import "LTTLineCell.h"
#import "LTTPublishOrderViewController.h"
#import "LTTOrderViewController.h"
#import "LTTWaitingLotteryCell.h"
#import "LTTPublishOrderViewController.h"
#import "LTTOrderViewController.h"
#import "LTTAwardViewController.h"
@interface LTTReadyViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSArray <LTTLotteryItem *>*items;
@property (nonatomic,strong)LTTLotteryHeaderItem *user;

@end

@implementation LTTReadyViewController

- (NSString *)title{
    
    return @"最新开榜";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configruation];
}

#pragma mark - 配置
- (void)configruation{
    
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"User"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTLotteryUserCell" bundle:nil] forCellReuseIdentifier:@"UserCell"];
    
    [self.tableView registerClass:[LTTLineCell class] forCellReuseIdentifier:@"Cell"];
    
    __weak typeof(self)weak = self;
    self.tableView.mj_header = [MJRefreshHeader headerWithRefreshingBlock:^{
        [weak loadData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - 网络
- (void)loadData{
    
    NSMutableDictionary *parameter = NSMutableDictionary.new;
    
    if (self.q_id != nil) {
        [parameter setValue:self.q_id forKey:@"q_id"];
    }
    
    if (self.state != nil) {
        [parameter setValue:self.state forKey:@"state"];
    }
    

    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"DetailsOnTheLottery") params:parameter target:self success:^(NSDictionary *success) {
        
        [self.tableView.mj_header endRefreshing];
        
        if ([success[@"error"] intValue] == 0) {
            
            NSDictionary *data = success[@"data"];
            
            [self setDataWithHeader:[NSArray modelArrayWithClass:[LTTLotteryHeaderItem class] json:data[@"prize_info"]].firstObject items:[NSArray modelArrayWithClass:[LTTLotteryItem class] json:data[@"user_info"]]];
        }else{
            SHOW(success[@"msg"]);
        }
        
    } failure:^(NSError *failure) {
        [self.tableView.mj_header endRefreshing];
        
        SHOW(failure.localizedDescription);
        
    }];
    
}

- (void)setDataWithHeader:(LTTLotteryHeaderItem *)header items:(NSArray<LTTLotteryItem*>*)items{
    
    self.items = items;
    self.user = header;
    
    
    UIView *tableHeaderView = UIView.new;
    
    LTTLotteryHeaderView *headerView = [[UINib nibWithNibName:@"LTTLotteryHeaderView" bundle:nil] instantiateWithOwner:nil options:nil].firstObject;
    
    headerView.item = header;
    
    [tableHeaderView addSubview:headerView];
    
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(tableHeaderView);
    }];
    
    UIView *bottomView = UIView.new;

    UIButton * order = UIButton.new;
    
    order.layer.cornerRadius = 5;
    
    order.layer.masksToBounds = YES;

    [order setTitle:@"立即下单" forState:UIControlStateNormal];
    [order setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [order addTarget:self action:@selector(onOrder) forControlEvents:UIControlEventTouchUpInside];
    order.backgroundColor = HEXCOLOR(0xEE5D5A);
    
    order.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [bottomView addSubview:order];
    
    [order mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView).inset(12);
        make.right.equalTo(bottomView.mas_centerX).inset(-6);
        make.top.equalTo(bottomView);
        make.bottom.equalTo(bottomView.mas_bottom).inset(12);
    }];
    
    UIButton * collection = UIButton.new;
    
    collection.layer.cornerRadius = 5;
    
    collection.layer.masksToBounds = YES;

    collection.titleLabel.font = [UIFont systemFontOfSize:14];

    [collection addTarget:self action:@selector(onCollection) forControlEvents:UIControlEventTouchUpInside];
    
    [collection setTitle:@"加入收藏" forState:UIControlStateNormal];
    [collection setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    collection.backgroundColor = HEXCOLOR(0xF8B126);
    
    [bottomView addSubview:collection];
    
    [collection mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.equalTo(order.mas_right).inset(12);
         make.right.equalTo(bottomView).inset(12);
         make.top.equalTo(bottomView);
         make.bottom.equalTo(bottomView.mas_bottom).inset(12);
     }];
    
    [tableHeaderView addSubview:bottomView];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerView.mas_bottom);
        make.left.right.bottom.equalTo(tableHeaderView);
        make.height.mas_equalTo(44);
    }];
    
    [self.tableView setTableHeaderView:tableHeaderView];
    
    [tableHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(self.view.width);
    }];
    
    [tableHeaderView layoutIfNeeded];
    
    [self.tableView reloadData];
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) { return self.user == nil ? 0 : 2; }
    
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) return [self tableView:tableView cellHeaderForRowAtIndexPath:indexPath];
    
    
    LTTLotteryUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
    
    cell.item = self.items[indexPath.row];
    
    return cell;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellHeaderForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"User" forIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = indexPath.row == 0 ? @"图文详情" : @"晒单分享";
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = HEXCOLOR(0x313131);
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0 && self.user != nil) {return 10 ; }
    
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        
        UIView *header = UIView.new;
        
        header.backgroundColor = HEXCOLOR(0xF4F5F6);
        return header;
        
    }
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.whiteColor;
    
    UILabel *label = UILabel.new;
    
    label.text = @"所有参与记录";
    
    label.textColor = HEXCOLOR(0x313131);
    
    label.font = [UIFont systemFontOfSize:12];
    
    [view addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).inset(12);
        make.centerY.equalTo(view);
    }];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (section == 0) { return 10 ;}
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footer = UIView.new;
    footer.backgroundColor = HEXCOLOR(0xF4F5F6);
    return  footer;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 ) {
        
        switch (indexPath.row) {
            case 0:{
             
                LTTAwardViewController *award = LTTAwardViewController.new;
                
                award.ID = self.user.ID;
                
                [self.navigationController pushViewController:award animated:YES];
                
                break;
            }
            case 1:
                [self.navigationController pushViewController: LTTPublishOrderViewController.new animated:YES];
                break;
            default:
                break;
        }
    }
}

- (void)onOrder{
    
    LTTOrderViewController *order = LTTOrderViewController.new;
    
    order.energy = self.user.energy_for_lucky_draw;
    
    order.ID = self.user.ID;
    
    [self.navigationController pushViewController:order animated:YES];
    
}

- (void)onCollection{
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"addCollect") params:@{@"id":self.user.ID} target:self success:^(NSDictionary *success) {
        
        if ([success[@"error"] intValue] == 0) {
            SHOW(@"收藏成功");
        }else{
            SHOW(success[@"msg"]);
        }
        
    } failure:^(NSError *failure) {
        SHOW(failure.localizedDescription);
    }];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
