//
//  LTTAlertViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/12/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^ShareMenu)(UMSocialPlatformType platform);

@interface LTTAlertViewController : LTTBaseViewController

- (void)shareItemSelected:(ShareMenu)menuItem;

@end

NS_ASSUME_NONNULL_END
