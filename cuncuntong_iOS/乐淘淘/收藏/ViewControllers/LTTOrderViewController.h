//
//  LTTOrderViewController.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LTTOrderViewController : LTTBaseViewController

/// 能量
@property (nonatomic,copy)NSString *energy;

/// 第几期
@property (nonatomic,copy)NSString *ID;

@end

NS_ASSUME_NONNULL_END
