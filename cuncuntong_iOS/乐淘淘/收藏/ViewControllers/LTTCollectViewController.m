//
//  LTTCollectViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTCollectViewController.h"
#import "LTTLikeItem.h"
#import "LTTCollectCell.h"
#import "LTTOrderViewController.h"
#import "LTTWaitingLotteryViewController.h"
#import "LTTLotteryViewController.h"
#import "LTTReadyViewController.h"
@interface LTTCollectViewController ()<UITableViewDataSource,UITableViewDelegate,CollectDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSArray<LTTLikeItem *>*items;

@end

@implementation LTTCollectViewController

- (NSString *)title{
    
    return @"收藏";
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.tableView.mj_header beginRefreshing];
    
//    "collect_id" = 40;
//               "current_of_lottery" = 4;
//               "dp_id" = 171;
//               "energy_for_lucky_draw" = 1;
//               id = 63;
//               "number_of_lottery" = 20;
//               "prize_cover_picture" = "http://www.cct369.com/village/public/lttimages/20191216/281bf69d8aee2cf1910a08f1dd95f56e.jpg";
//               "prize_title" = "\U54c8\U54c8\U55bd";
//               state = 1;
//
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
}

#pragma mark - 配置
- (void)configuration{
    self.tableView.rowHeight = 165;
    [self.tableView setTableFooterView:UIView.new];
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTCollectCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
}

#pragma mark 网络
- (void)loadData{
    
    [[MyNetWorking sharedInstance] LTTGetUrl:LTTURL(@"collectList") params:nil target:self success:^(NSDictionary *success) {
        [self.tableView.mj_header endRefreshing];
        if ([success[@"code"] intValue] == 0) {
            
            self.items = [NSArray modelArrayWithClass:[LTTLikeItem class] json:success[@"data"]];
        }
        
        [self.tableView reloadData];
        
    } failure:^(NSError *failure) {
        
        [self.tableView.mj_header endRefreshing];
        
        SHOW(failure.localizedDescription);
        
    }];
    
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTCollectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.item = self.items[indexPath.section];
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footer =[UIView new];
    
    footer.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    return footer;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTLikeItem *item = self.items[indexPath.row];
    
    switch ([item.state intValue]) {
        case 2:{
            
            LTTWaitingLotteryViewController *lottery = [[LTTWaitingLotteryViewController alloc] initWithNibName:@"LTTWaitingLotteryViewController" bundle:nil];
            
            lottery.q_id  = item.ID;
            lottery.state = item.state;
            
            [self.navigationController pushViewController:lottery animated:YES];
            break;
        }
        case 3:{
            LTTLotteryViewController *lottery = [[LTTLotteryViewController alloc] initWithNibName:@"LTTLotteryViewController" bundle:nil];
            
            lottery.q_id  = item.ID;
            lottery.state = item.state;
            [self.navigationController pushViewController:lottery animated:YES];
            
            break;
        }
            
        default:{
            LTTReadyViewController *ready = LTTReadyViewController.new;
            
            ready.q_id = item.ID;
            ready.state = item.state;
            
            [self.navigationController pushViewController:ready animated:YES];
        }
    }
}

#pragma mark - CollectDelegate 删除收藏/下单

- (void)deleteToLike:(LTTLikeItem *)item{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"您确定删除'%@'这个收藏吗?",item.prize_title] preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"delCollect") params:@{@"collect_id":item.collect_id} target:self success:^(NSDictionary *success) {
            
            if ([success[@"error"] intValue] == 0) {
                SHOW(@"删除成功");
                [self.tableView.mj_header beginRefreshing];
            }
            
        } failure:^(NSError *failure) {
            SHOW(@"删除失败");
        }];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];

    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}

- (void)orderLotteryWithEnergy:(NSString *)energy ID:(nonnull NSString *)ID{
    
    LTTOrderViewController *order = [[LTTOrderViewController alloc]initWithNibName:@"LTTOrderViewController" bundle:nil];
    
    order.energy = energy;
    
    order.ID = ID;
    
    [self.navigationController pushViewController:order animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
