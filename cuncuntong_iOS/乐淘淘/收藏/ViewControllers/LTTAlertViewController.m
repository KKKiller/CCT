//
//  LTTAlertViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/12/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTAlertViewController.h"

@interface LTTAlertViewController ()

@property (nonatomic,copy)ShareMenu closure;

@end

@implementation LTTAlertViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    
    return self;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelEventAction:)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (IBAction)buttonEventAction:(UIButton *)sender {
    
    switch (sender.tag) {
        case 0:
            if (self.closure) { self.closure(UMSocialPlatformType_WechatSession);}
            
            break;
        case 1:
            if (self.closure) { self.closure(UMSocialPlatformType_WechatTimeLine);}
            
            break;
        case 2:
            if (self.closure) { self.closure(UMSocialPlatformType_QQ);}
            
            break;
        case 3:
            if (self.closure) { self.closure(UMSocialPlatformType_Qzone);}
            
            break;
            
        default:
            break;
    }
    
    [self cancelEventAction:sender];
    
}




- (IBAction)cancelEventAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)shareItemSelected:(ShareMenu)menuItem{
    self.closure = menuItem;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
