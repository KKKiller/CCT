//
//  LTTOrderViewController.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTOrderViewController.h"
#import "LTTChooseLotteryNumberView.h"
#import "LTTOrderNumber.h"
#import "LTTOrderNumberCell.h"
#import "LTTWinningHistoryViewController.h"
#import "MYHomeController.h"
#import "ListView.h"
#import "LTTAlertViewController.h"
@interface LTTOrderViewController ()<UITableViewDataSource,UITableViewDelegate,ChooseLotteryNumberDelegate,OrderNumberDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lotteryLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)LTTChooseLotteryNumberView *lotteryNumberView;

@property (nonatomic,assign)NSInteger residualEnergy;

@property (nonatomic,strong)NSMutableArray <LTTOrderNumber *>*items;

@end

@implementation LTTOrderViewController



#pragma mark 懒加载

- (LTTChooseLotteryNumberView *)lotteryNumberView{
    
    if (_lotteryNumberView) return _lotteryNumberView;
    
    _lotteryNumberView = [[UINib nibWithNibName:@"LTTChooseLotteryNumberView" bundle:nil] instantiateWithOwner:nil options:nil].firstObject;
    
    _lotteryNumberView.delegate = self;
    
    [_lotteryNumberView setFrame:CGRectMake(0, 0, self.view.width, 150)];
    
    return _lotteryNumberView;
}

- (NSMutableArray<LTTOrderNumber *>*)items{
    
    if (_items) return _items;
    
    _items = [NSMutableArray array];
    
    return _items;
}

- (NSString *)title{
    
    return @"选号下单";
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self configuration];
    
    [self loadData];
}

#pragma mark - 配置

- (void)configuration{
    
    [self.view.subviews.firstObject setHidden:YES];
    
    self.navigationController.navigationBar.translucent = NO;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"中奖记录" style:UIBarButtonItemStyleDone target:self action:@selector(winningHistory)];
    
    self.navigationItem.rightBarButtonItem.tintColor = UIColor.whiteColor;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTOrderNumberCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
}
- (IBAction)shownengliang:(id)sender {
    MYHomeController *vc = [[MYHomeController alloc]init];
               [self.navigationController pushViewController:vc animated:YES];
}


- (void)winningHistory{
    [self.navigationController pushViewController:LTTWinningHistoryViewController.new animated:YES];
}

- (void)setData:(NSInteger)place_order_number energy:(NSInteger)energy{
    
    
    [self.view.subviews.firstObject setHidden:NO];
    

    self.residualEnergy = energy;
    
    self.lotteryLabel.text = [NSString stringWithFormat:@"您当前有%ld克阅读能量,可下%ld单",(long)self.residualEnergy,place_order_number];
        
    [self isShowLotteryNumberView];
}

- (void)isShowLotteryNumberView{
    [self.tableView setTableFooterView:self.lotteryNumberView];
    
    [self.tableView reloadData];
    
}


#pragma mark - 网络

- (void)loadData{
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"chooseNumber") params:@{@"energy_for_lucky_draw":self.energy} target:self success:^(NSDictionary *success) {
        
        if ([success[@"error"] intValue] == 0) {
            
            NSDictionary *data = success[@"data"];
            [self setData: [data[@"place_order_number"] intValue] energy:[data[@"energy"] integerValue]];
            
        }else{
            
            SHOW(success[@"msg"]);
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self.navigationController popViewControllerAnimated:YES];
//            });0
  
            [self setData:0 energy:0];
        }
        
        
    } failure:^(NSError *failure) {
        
        SHOW(failure.localizedDescription);
    }];
    
}



- (IBAction)submitEventAction:(id)sender {
    
    if (self.items.count > 0) {
        
        NSMutableArray *numbers = [NSMutableArray array];
        
        for (LTTOrderNumber *item in self.items) {
            [numbers addObject:item.random_number];
        }
        
        NSString *str = [numbers componentsJoinedByString:@","];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        
        [parameter setValue:self.ID forKey:@"id"];
        [parameter setValue:str forKey:@"number"];
        
        
        [[MyNetWorking sharedInstance] LTTPostURL:LTTURL(@"addBottomPour") params:parameter target:self success:^(NSDictionary *success) {
            
            if ([success[@"error"] intValue] == 0) {
                
                SHOW(@"下单成功");
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                });
            }else{
                
                SHOW(success[@"msg"]);
            }
            
            
        } failure:^(NSError *failure) {
            
            SHOW(failure.localizedDescription);
        }];
        
        
        
    }else{
        SHOW(@"请选择号码");
    }
    
    
    
    
}
- (IBAction)sharedEventAction:(id)sender {
    
    LTTAlertViewController *alert = LTTAlertViewController.new;
    
    [alert shareItemSelected:^(UMSocialPlatformType platform) {
        [self share:platform];
    }];

    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}

- (void)share:(UMSocialPlatformType)platform {
   
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
   
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:@"免费抽奖了，金砖大奖等你抽"  descr:@"每天有大量商家和土豪发布奖品，奖品拿的你手软！" thumImage:IMAGENAMED(@"100-100.jpg")];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"http://wanshitong.com/village/public/center/qrcodereg?iid=%@",USERID];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platform messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            SHOW(@"分享失败");
        }else{
            SHOW(@"分享成功");
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTOrderNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.item = self.items[indexPath.row];
    
    cell.delegate = self;
    

    return cell;
}



#pragma mark - ChooseLotteryNumberDelegate
- (void)lotteryNumberView:(LTTChooseLotteryNumberView *)view number:(NSString *)number{
    
    LTTOrderNumber *order = [[LTTOrderNumber alloc]initWithRandom_number:number];
    
    if ([self.items containsObject:order]) {
        order.isRepeat = YES;
    }
    [view clearNumber];
    
    [self.items addObject:order];
    
    [self  isShowLotteryNumberView];
}

#pragma mark - OrderNumberDelegate

- (void)deleteOrderWith:(LTTOrderNumberCell *)cell{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    LTTOrderNumber *order = cell.item;
    
    [self.items removeObjectAtIndex:indexPath.row];
    
    if ([self.items containsObject:order]) {
        
        NSInteger index =  [self.items indexOfObject:order];
        
        if (index > 0) {
            
            self.items[index].isRepeat = NO;
        }
    }
    
    [self  isShowLotteryNumberView];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
