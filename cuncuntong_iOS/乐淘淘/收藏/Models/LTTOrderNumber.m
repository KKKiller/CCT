//
//  LTTOrderNumber.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/23.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTOrderNumber.h"
#import "NSString+SubString.h"
@interface LTTOrderNumber (){
    
    NSString *random_number;
    
    NSArray *numbers;
}

@end

@implementation LTTOrderNumber

- (instancetype)initWithRandom_number:(NSString *)number{
    
    if (self = [super init]) {
        
        random_number = [NSString stringWithFormat:@"%06ld",(long)[number integerValue]];
        
        numbers = number.substrings;
        
        self.isRepeat = NO;
    }
    
    return self;
}

- (NSString *)random_number{
    return random_number;
}

- (NSArray *)numbers{
    return numbers;
}

- (BOOL)isEqualToOrderNumber:(LTTOrderNumber *)orderNumber{

    return [self.random_number isEqualToString:orderNumber.random_number];
}

- (BOOL)isEqual:(id)object{
    
    if ([object isKindOfClass:[LTTOrderNumber class]]) {
        
        return [self isEqualToOrderNumber:object];
    }
    
    return NO;
}

@end
