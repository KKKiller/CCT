//
//  LTTLikeItem.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTLikeItem.h"

@implementation LTTLikeItem
+ (NSDictionary *)modelCustomPropertyMapper{
    
    return @{@"ID":@[@"id"]};
}

- (NSString *)energy{
    
    
    switch ([self.energy_for_lucky_draw integerValue]) {
        case 0:
            return @"25";
            break;
        case 2:
            return @"50";
            break;
         case 3:
            return @"75";
            break;
        default:
            return @"100";
            break;
    }
}

@end
