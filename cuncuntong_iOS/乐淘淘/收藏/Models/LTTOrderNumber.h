//
//  LTTOrderNumber.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/23.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTOrderNumber : NSObject


- (instancetype)initWithRandom_number:(NSString *)number;

/// 随机数
@property (nonatomic,readonly)NSString *random_number;

@property (nonatomic,readonly)NSArray *numbers;

@property (nonatomic,assign)BOOL isRepeat;

@end

NS_ASSUME_NONNULL_END
