//
//  LTTLikeItem.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTLikeItem : NSObject

/// 开奖当前人数
@property (nonatomic,copy)NSString *current_of_lottery;
/// 所需能量
@property (nonatomic,copy)NSString *energy_for_lucky_draw;
/// 开奖总人次
@property (nonatomic,copy)NSString *number_of_lottery;
/// 开奖封面图
@property (nonatomic,copy)NSString *prize_cover_picture;
/// 开奖标题
@property (nonatomic,copy)NSString *prize_title;
/// 期id
@property (nonatomic,copy)NSString *ID;

@property (nonatomic,readonly)NSString *energy;

@property (nonatomic,copy)NSString *collect_id;

@property (nonatomic,copy)NSString *dp_id;

@property (nonatomic,copy)NSString *state;


@end

NS_ASSUME_NONNULL_END
