//
//  LTTLTTChooseLotteryNumberView.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTChooseLotteryNumberView.h"
#import "MQVerCodeInputView.h"
@interface LTTChooseLotteryNumberView ()
@property (weak, nonatomic) IBOutlet MQVerCodeInputView *numberInputView;


@end

@implementation LTTChooseLotteryNumberView

- (void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.numberInputView.maxLenght = 6;
    self.numberInputView.keyBoardType = UIKeyboardTypeNumberPad;
    
    self.numberInputView.block = ^(NSString *text) {
        NSLog(@"%@",text);
    };
    
    [self.numberInputView mq_verCodeViewWithMaxLenght];
    
}


- (IBAction)randomNumber:(id)sender {
    
    NSMutableString *numbers = [[NSMutableString alloc]init];
    
    for ( int i = 0; i < 6; i++) {
        
        [numbers appendFormat:@"%ld",(long)[self random]];
        
    }
    
    self.numberInputView.text = numbers;
    
}
- (IBAction)doneEventAction:(id)sender {
    
    if (self.numberInputView.text.length != 6) {
        SHOW(@"请选择正确单号");
        
    }else{
        if ([self.delegate respondsToSelector:@selector(lotteryNumberView:number:)]) {
            
            [self.delegate lotteryNumberView:self number:self.numberInputView.text];
        }
    }
    
    
    
}

- (void)clearNumber{
    self.numberInputView.text = @"";
}

-(NSInteger)random{
    
    return (0 + (arc4random() % (9 - 0 + 1)));
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
