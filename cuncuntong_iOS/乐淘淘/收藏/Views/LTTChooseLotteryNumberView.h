//
//  LTTLTTChooseLotteryNumberView.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class LTTChooseLotteryNumberView;

@protocol ChooseLotteryNumberDelegate <NSObject>

- (void)lotteryNumberView:(LTTChooseLotteryNumberView *)view number:(NSString *)number;

@end

@interface LTTChooseLotteryNumberView : UIView


@property (nonatomic,weak)id<ChooseLotteryNumberDelegate> delegate;

- (void)clearNumber;

@end

NS_ASSUME_NONNULL_END
