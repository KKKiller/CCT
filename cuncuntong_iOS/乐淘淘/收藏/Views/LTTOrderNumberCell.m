//
//  LTTOrderNumberCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/23.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTOrderNumberCell.h"



@implementation DigitalTextCell

- (UILabel *)label{
    
    if (_label)return _label;
    
    _label = [[UILabel alloc]init];
    
    _label.textColor = [UIColor whiteColor];
    [_label setBackgroundColor:[UIColor blackColor]];
    _label.textAlignment = NSTextAlignmentCenter;
    _label.layer.cornerRadius = 5.0;
    _label.font = [UIFont systemFontOfSize:38];
    _label.layer.masksToBounds = YES;
    
    [self addSubview:_label];
    
    [_label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    return _label;
}
@end



@interface LTTOrderNumberCell ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *prompt;

@end


@implementation LTTOrderNumberCell

- (void)setItem:(LTTOrderNumber *)item{
    _item = item;
    
    self.prompt.hidden = !item.isRepeat;
    
    [self.collectionView reloadData];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.collectionView registerClass:[DigitalTextCell class] forCellWithReuseIdentifier:@"Cell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)deleteEventAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(deleteOrderWith:)]) {
        
        [self.delegate deleteOrderWith:self];
    }
    
}



#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return self.item.numbers.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DigitalTextCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.label.text = self.item.numbers[indexPath.row];
    
    cell.label.backgroundColor = self.item.isRepeat ? HEXCOLOR(0xCCCCCC) : [UIColor blackColor];
    
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(44.0*0.9, 44.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 4.5;
}
@end
