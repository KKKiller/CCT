//
//  LTTOrderNumberCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/23.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTOrderNumber.h"
NS_ASSUME_NONNULL_BEGIN
@class LTTOrderNumberCell;
@protocol OrderNumberDelegate <NSObject>

- (void)deleteOrderWith:(LTTOrderNumberCell *)cell;

@end

@interface DigitalTextCell : UICollectionViewCell

@property (nonatomic,strong)UILabel *label;

@end

@interface LTTOrderNumberCell : UITableViewCell

@property (nonatomic,strong)LTTOrderNumber *item;

@property (nonatomic,weak)id<OrderNumberDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
