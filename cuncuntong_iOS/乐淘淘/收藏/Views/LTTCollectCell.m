//
//  LTTCollectCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTCollectCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LTTPhaseView.h"
@interface LTTCollectCell ()

@property (weak, nonatomic) IBOutlet UIImageView *g_imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UIButton *order;

@property (weak, nonatomic) IBOutlet LTTPhaseView *phaseView;

@end

@implementation LTTCollectCell

- (void)setItem:(LTTLikeItem *)item{
    
    _item = item;
    
    [self.g_imageView sd_setImageWithURL:LTTImgaeURL(item.prize_cover_picture)];
    
    self.titleLabel.text = item.prize_title;
    
    self.energyLabel.text = [[NSString alloc]initWithFormat:@"所需能量%@克",item.energy];
    
    self.phaseView.current = [item.current_of_lottery intValue];
    
    self.phaseView.total = [item.number_of_lottery intValue];
    
    
}

- (IBAction)deleteEventActio:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(deleteToLike:)]) {
        
        [self.delegate deleteToLike:self.item];
    }
    
}
- (IBAction)orderEventAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(orderLotteryWithEnergy:ID:)]) {
        
        [self.delegate orderLotteryWithEnergy:self.item.energy_for_lucky_draw ID:self.item.ID];
        
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.order.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.order.layer.borderWidth = 0.5;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
