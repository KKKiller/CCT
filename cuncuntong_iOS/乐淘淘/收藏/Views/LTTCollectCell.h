//
//  LTTCollectCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTLikeItem.h"
NS_ASSUME_NONNULL_BEGIN

@protocol CollectDelegate <NSObject>

- (void)orderLotteryWithEnergy:(NSString *)energy ID:(NSString *)ID;

- (void)deleteToLike:(LTTLikeItem *)item;

@end

@interface LTTCollectCell : UITableViewCell

@property (nonatomic,strong)LTTLikeItem *item;

@property (nonatomic,weak)id<CollectDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
