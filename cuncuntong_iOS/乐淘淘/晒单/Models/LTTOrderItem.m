//
//  LTTOrderItem.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTOrderItem.h"

@interface LTTOrderItem (){
    
    NSMutableAttributedString *attributeds;
    NSArray *items;
}

@end


@implementation LTTOrderItem

- (instancetype)init{
    if (self = [super init]) {
        self.height = 20;
    }
    return self;
}

- (void)setShare_talk:(NSString *)share_talk{
    
    _share_talk = share_talk;
    
    attributeds = [NSMutableAttributedString new];
    
    [attributeds appendAttributedString:[[NSAttributedString alloc]initWithString:share_talk]];
    
    attributeds.font = [UIFont systemFontOfSize:15];
    
}

- (void)setShare_picture:(NSArray *)share_picture{
    _share_picture = share_picture;
    NSLog(@"_share_picture = %@",_share_picture);
    
    items = share_picture;
    
    
}
- (NSArray <NSString *>*)images{
    return items;
}
- (NSAttributedString *)text{
    
    return attributeds;
}

@end
