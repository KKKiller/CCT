//
//  LTTOrderItem.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTOrderItem : NSObject

/// 发布晒单时间
@property (nonatomic,copy)NSString * create_time;
/// <#Description#>
@property (nonatomic,copy)NSString * reader_id;
///  晒单图片
@property (nonatomic,copy)NSArray<NSString *>* share_picture;
/// 晒单说说
@property (nonatomic,copy)NSString * share_talk;
/// 用户名
@property (nonatomic,copy)NSString *realname;
/// 用户头像
@property (nonatomic,copy)NSString *portrait;

@property (nonatomic,readonly)NSAttributedString *text;

@property (nonatomic,assign)CGFloat height;

@property (nonatomic,readonly)NSArray<NSString *>*images;

@end

NS_ASSUME_NONNULL_END
