//
//  LTTShowOrderCell.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTShowOrderCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <YYKit/YYKit.h>
#import <YYKit/NSAttributedString+YYText.h>
#import "LTTOrderImageCell.h"
@interface LTTShowOrderCell ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong)YYLabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *h_imageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *context;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contextHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionHeight;

@end



@implementation LTTShowOrderCell

- (YYLabel *)label{
    
    if (_label) return _label;
    
    _label = [[YYLabel alloc]init];
    
    _label.userInteractionEnabled = YES;
    _label.numberOfLines = 0;
    _label.textVerticalAlignment = YYTextVerticalAlignmentTop;
    
    [self.context addSubview:_label];
    
    [_label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.context);
    }];
    
    return _label;
}


- (void)setItem:(LTTOrderItem *)item{
    
    _item = item;
    
    self.titleLabel.text = item.realname;
    
    self.dateLabel.text = item.create_time;
    
    self.contextHeight.constant = item.height;
    
    [self.h_imageView sd_setImageWithURL:[NSURL URLWithString:item.portrait]];
    
    self.label.attributedText = item.text;
    
    [self addSeeMoreButtonInLabel:self.label];
    
    
    
    [self.collectionView reloadData];
    
}

- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority verticalFittingPriority:(UILayoutPriority)verticalFittingPriority{
    
    CGFloat height = (self.width - 52) / 3.0;
    
    self.collectionHeight.constant =   self.item.images.count % 3 == 0 ? self.item.images.count / 3 * height : (self.item.images.count / 3 + 1) * height;
    
    return [super systemLayoutSizeFittingSize:targetSize withHorizontalFittingPriority:horizontalFittingPriority verticalFittingPriority:verticalFittingPriority];
}

- (void)addSeeMoreButtonInLabel:(YYLabel *)label {
    
    NSString *moreString = @"展开全文";
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"... %@", moreString]];
    NSRange expandRange = [text.string rangeOfString:moreString];
    
    [text addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xFE3734) range:expandRange];
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor darkTextColor] range:NSMakeRange(0, expandRange.location)];
    
    //添加点击事件
    YYTextHighlight *hi = [YYTextHighlight new];
    [text setTextHighlight:hi range:[text.string rangeOfString:moreString]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        
        weakSelf.item.height = [weakSelf.label.attributedText boundingRectWithSize:CGSizeMake(weakSelf.label.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
        if ([weakSelf.delegate respondsToSelector:@selector(textUpdate)]) {
            [weakSelf.delegate textUpdate];
        }
    };
    
    text.font = label.font;
    
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = text;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:text.font alignment:YYTextVerticalAlignmentTop];
    
    label.truncationToken = truncationToken;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTTOrderImageCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.item.images.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTOrderImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.imageView sd_setImageWithURL:LTTImgaeURL(self.item.images[indexPath.row])];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat width = (collectionView.width - 12)  /3.0;
    
    return CGSizeMake(width, width);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 6;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 6;
}
@end
