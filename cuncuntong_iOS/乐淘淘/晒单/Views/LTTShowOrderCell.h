//
//  LTTShowOrderCell.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/22.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTTOrderItem.h"
NS_ASSUME_NONNULL_BEGIN

@protocol TextUpdateDelegate <NSObject>

- (void)textUpdate;

@end

@interface LTTShowOrderCell : UITableViewCell

@property (nonatomic,strong)LTTOrderItem *item;

@property (nonatomic,weak)id<TextUpdateDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
