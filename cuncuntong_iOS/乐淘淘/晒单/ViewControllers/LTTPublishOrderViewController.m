//
//  LTTPublishOrderViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/29.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPublishOrderViewController.h"
#import "UITextView+Placeholder.h"
#import "LTTOrderImageCell.h"
#import "LTTImagePickerController.h"
@interface LTTPublishOrderViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic,strong)NSMutableArray<UIImage *>*items;

@end

@implementation LTTPublishOrderViewController

#pragma mark - 懒加载

- (NSString *)title {
    
    return @"我要晒单";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configuration];
    // Do any additional setup after loading the view.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - 配置
- (void)configuration{
    self.items = [NSMutableArray array];
    [self.textView setPlaceholder:@"宝贝满足你的期待吗？说说你的使用心得,分享给想买的他们吧！" placeholdColor:HEXCOLOR(0x666666)];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTTOrderImageCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    [self updateConstraints];
    
}

- (void)appImage{
    __weak typeof(self)weak = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"相机相册" message:@"选择奖品图片" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        LTTImagePickerController *imagePicker = LTTImagePickerController.new;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [imagePicker imageWithSelected:^(UIImage * _Nonnull image) {
            [weak.items addObject:image];
            [weak updateConstraints];
        }];
        
        [weak presentViewController:imagePicker animated:YES completion:nil];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        LTTImagePickerController *imagePicker = LTTImagePickerController.new;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [imagePicker imageWithSelected:^(UIImage * _Nonnull image) {
            [weak.items addObject:image];
            [weak updateConstraints];
        }];
        
        [weak presentViewController:imagePicker animated:YES completion:nil];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]];
    
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

- (void)deleteImageWith:(NSInteger)index{
    
    __weak typeof(self)weak = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"删除" message:@"删除图片" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [weak.items removeObjectAtIndex:index];
        [weak updateConstraints];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}

/// 发布
/// @param sender 发送者
- (IBAction)publishEventAction:(UIButton *)sender {
    
    if (self.textView.text.length <= 0) {
        
        return SHOW(@"请输入描述");
    }
    
    if (self.items.count <= 0) {
        
        return SHOW(@"请选择描述图片");
    }
    [TOOL showLoading:self.view];
    [[MyNetWorking sharedInstance]lttMoreImages:self.items URL:LTTURL(@"uploadSharePicture") name:@"share_picture[]" ID:nil success:^(NSDictionary *success) {
        
        if ([success[@"error"] intValue] == 0) {
            
            [self finishedWithID:success[@"data"][@"id"]];
        }else{
            [TOOL hideLoading:self.view];
            SHOW(success[@"msg"]);
        }
        
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
        SHOW(failure.localizedDescription);
    }];
    
    
}

- (void)finishedWithID:(NSString *)ID{
    
    NSDictionary *paramter = @{@"share_talk":self.textView.text,@"id":ID};
    
    [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"addShare") params:paramter target:self success:^(NSDictionary *success) {
        [TOOL hideLoading:self.view];
        
        if ([success[@"error"] intValue] == 0) {
            
            SHOW(@"发布完成");
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }else{
            SHOW(success[@"msg"]);
        }
    
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
        SHOW(failure.localizedDescription);
    }];
}

- (void)updateConstraints{
    
    [self.collectionView reloadData];
    
    CGFloat height = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    
    self.heightConstraint.constant = height;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (self.items.count >= 9) {
        return 9;
    }
    
    return self.items.count + 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTOrderImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.imageView.backgroundColor = [UIColor clearColor];
    
    if (self.items.count > indexPath.row) {
        cell.imageView.image = self.items[indexPath.row];
    }else{
        cell.imageView.image = [UIImage imageNamed:@"2-0"];
    }
    
    return cell;
}




#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < self.items.count) {
        [self deleteImageWith:indexPath.row];
    }else{
        [self appImage];
    }
    
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat width = collectionView.width - 24;
    
    return CGSizeMake(width / 3.0, width / 3.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 12.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 12.0f;
}


@end
