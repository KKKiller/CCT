//
//  LTTShowOrderViewController.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTShowOrderViewController.h"
#import "LTTOrderItem.h"
#import "LTTShowOrderCell.h"
#import "LTTPublishOrderViewController.h"
@interface LTTShowOrderViewController ()<UITableViewDataSource,UITableViewDelegate,TextUpdateDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSArray<LTTOrderItem *>*items;

@end

@implementation LTTShowOrderViewController

- (NSString *)title{
    return @"晒单";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

   [self configuration];
}


#pragma mark - 配置

- (void)configuration{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LTTShowOrderCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    __weak typeof(self) weak = self;
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak loadData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"我要晒单" style:UIBarButtonItemStyleDone target:self action:@selector(sunOrder)];
//    
//    self.navigationItem.rightBarButtonItem.tintColor = UIColor.whiteColor;
    
}

- (void)sunOrder{
    
    LTTPublishOrderViewController *publish = [[LTTPublishOrderViewController alloc]initWithNibName:@"LTTPublishOrderViewController" bundle:nil];
    
    [self.navigationController pushViewController:publish animated:YES];
}

#pragma mark - 网络

- (void)loadData{
    
    [[MyNetWorking sharedInstance]LTTGetUrl:LTTURL(@"shareList") params:nil target:self success:^(NSDictionary *success) {
        
        [self.tableView.mj_header endRefreshing];
        
        if ([success[@"error"] intValue] == 0) {
            
            self.items = [NSArray modelArrayWithClass:[LTTOrderItem class] json:success[@"data"]];
        }
        
        [self.tableView reloadData];
        
    } failure:^(NSError *failure) {
        [self.tableView.mj_header endRefreshing];
        SHOW(failure.localizedDescription);
        
    }];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTShowOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.item = self.items[indexPath.section];
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footer = [UIView new];
    
    footer.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    return footer;
}
#pragma mark - TextUpdateDelegate
- (void)textUpdate{
    [self.tableView reloadData];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
