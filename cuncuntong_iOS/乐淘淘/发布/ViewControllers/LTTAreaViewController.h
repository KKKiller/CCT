//
//  LTTAreaViewController.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"
typedef void (^AreaBlock)(NSString *title ,NSString *ID);
NS_ASSUME_NONNULL_BEGIN

@interface LTTAreaViewController : LTTBaseViewController


- (void)itemSelecetd:(AreaBlock)area;

@end

NS_ASSUME_NONNULL_END
