//
//  LTTAreaViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/11/12.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTAreaViewController.h"
#import "CCGirlHttpManager.h"

@interface AreaItem : NSObject

@property (nonatomic,copy)NSString *name;

@property (nonatomic,assign)NSInteger ID;

@end

@implementation AreaItem

+ (NSDictionary *)modelCustomPropertyMapper{
    
    return @{@"ID":@[@"id"]};
}



@end


@interface LTTAreaViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic,copy)NSString *ID;

@property (nonatomic,copy)NSString *str;

@property (nonatomic,copy)AreaBlock block;

@property (nonatomic,strong)NSArray <AreaItem *>*items;

@property (nonatomic,assign)NSInteger level;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation LTTAreaViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    
    return self;
}


- (void)setStr:(NSString *)str{
    _str = str;
    self.titleLabel.text = str;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self configuration];
    [self loadData];
}

- (void)configuration{
    
    self.level = 0;
    self.ID = @"0";
    [self.topView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewController)]];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView setTableFooterView:UIView.new];
}

- (void)loadData{
    [TOOL showLoading:self.bottomView];
    [[CCGirlHttpManager share]getSelectAreaList:self.ID succeedBd:^(CCNetBaseModel * _Nonnull baseModel, NSArray * _Nullable areaList) {
        [TOOL hideLoading:self.bottomView];
        
        self.items = [NSArray modelArrayWithClass:[AreaItem class] json:areaList];
        
        [self.tableView reloadData];
    } failBd:^(NSURLSessionDataTask *operation, NSError *error) {
         [TOOL hideLoading:self.bottomView];
        SHOW(error.localizedDescription);
        
    }];
    
}

- (IBAction)doneEventAction:(UIButton *)sender {
    
    if ([self.ID isEqualToString:@"0"]){ self.ID = @"-1"; }

    if (self.block) { self.block(self.titleLabel.text, self.ID); }
    
    [self dismissViewController];
}

- (void)dismissViewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)itemSelecetd:(AreaBlock)area{
    self.block = area;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.items[indexPath.row].name;
    
    return cell;
}


#pragma ,mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.level += 1;
    
    self.ID = [NSString stringWithFormat:@"%ld",self.items[indexPath.row].ID];
    
    if (self.str == nil) {
        self.str = self.items[indexPath.row].name;
    }else{
        self.str = [NSString stringWithFormat:@"%@%@",self.str,self.items[indexPath.row].name];
    }

    if (self.level >= 3) {
        if (self.block) {
            self.block(self.str, self.ID);
        }
        [self dismissViewController];

    }else{
        [self loadData];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
