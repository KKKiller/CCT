//
//  LTTPublishViewController.m
//  CunCunTong
//
//  Created by Whisper on 2019/10/28.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPublishViewController.h"
#import "UITextView+Placeholder.h"
#import "LTTImagePickerController.h"
#import "NSString+SubString.h"
#import "LTTOrderImageCell.h"
#import "LTTAreaViewController.h"
#import "RechargeViewController.h"
@interface LTTPublishViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionHeight;
/// 封面
@property (weak, nonatomic) IBOutlet UIButton *coverButtion;
/// 奖品名字
@property (weak, nonatomic) IBOutlet UITextField *titleLabel;
/// 奖品描述
@property (weak, nonatomic) IBOutlet UITextView *prizeDescription;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
/// 价格
@property (weak, nonatomic) IBOutlet UITextField *price;
/// 链接
@property (weak, nonatomic) IBOutlet UITextField *link;

/// 开奖人次
@property (weak, nonatomic) IBOutlet UITextField *frequency;
/// 抽奖次数
@property (weak, nonatomic) IBOutlet UITextField *lotteryFrequency;
///  能量
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *energys;

@property (nonatomic,copy)NSString *areaID;
/// 押金
@property (weak, nonatomic) IBOutlet UILabel *money;

@property (nonatomic,strong)UIImage *coverImage;

@property (nonatomic,strong)NSMutableArray<UIImage *> *priceImage;


@end

@implementation LTTPublishViewController


- (NSString *)title{
    
    return @"发布奖品";
}

- (void)setCoverImage:(UIImage *)coverImage{
    _coverImage = coverImage;
    
    [self.coverButtion setBackgroundImage:coverImage forState:UIControlStateNormal];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configuration];
    [self updateConstraints];
}

#pragma mark - 配置
- (void)configuration{
    
    self.priceImage = NSMutableArray.new;
    
    [self.prizeDescription setPlaceholder:@"请输入详情介绍,不超过150字" placeholdColor:UIColor.lightGrayColor];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTTOrderImageCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
}



#pragma mark - Action

/// 封面选择事件
/// @param sender 发送者
- (IBAction)coverEventAction:(UIButton *)sender {
    
    __weak typeof(self)weak = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"相机相册" message:@"选择封面" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        LTTImagePickerController *imagePicker = LTTImagePickerController.new;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [imagePicker imageWithSelected:^(UIImage * _Nonnull image) {
            weak.coverImage = image;
        }];
        
        [weak presentViewController:imagePicker animated:YES completion:nil];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        LTTImagePickerController *imagePicker = LTTImagePickerController.new;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [imagePicker imageWithSelected:^(UIImage * _Nonnull image) {
            weak.coverImage = image;
        }];
        
        [weak presentViewController:imagePicker animated:YES completion:nil];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]];
    
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}

/// 图片选择事件
/// @param sender 发送者
- (IBAction)imageEventAction{
    
    __weak typeof(self)weak = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"相机相册" message:@"选择奖品图片" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        LTTImagePickerController *imagePicker = LTTImagePickerController.new;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [imagePicker imageWithSelected:^(UIImage * _Nonnull image) {
            [weak.priceImage addObject:image];
            [weak updateConstraints];
        }];
        
        [weak presentViewController:imagePicker animated:YES completion:nil];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        LTTImagePickerController *imagePicker = LTTImagePickerController.new;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [imagePicker imageWithSelected:^(UIImage * _Nonnull image) {
            [weak.priceImage  addObject:image];
            [weak updateConstraints];
        }];
        
        [weak presentViewController:imagePicker animated:YES completion:nil];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]];
    
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)araeButtonEventAction:(UIButton *)sender {
    
    LTTAreaViewController *area = LTTAreaViewController.new;
    __weak typeof(self)weak = self;
    [area itemSelecetd:^(NSString *title, NSString *ID) {
        weak.areaID = ID;
        [sender setTitle:title forState:UIControlStateNormal];
        [sender setTitleColor:HEXCOLOR(0x313131) forState:UIControlStateNormal];
    }];
    
    [self.navigationController presentViewController:area animated:YES completion:nil];
}

/// 能量事件
/// @param sender 发送者
- (IBAction)energyEventAction:(UIButton *)sender {
    
    for (UIButton *button in  self.energys) {
        [button setSelected:NO];
    }
    
    [sender setSelected:YES];
    
}

/// 发布
/// @param sender 发送者
- (IBAction)submitEventAction:(id)sender {
    
    
    if (self.coverImage == nil) { return SHOW(@"请选择封面"); }
    
    if (self.titleLabel.text.length <= 0) { return SHOW(@"请输入奖品标题"); }
    
    if (self.prizeDescription.text.length <= 0) {  return SHOW(@"请输入奖品描述");}
    
    if (self.priceImage == nil) { return SHOW(@"请选择封面图片");}
    
    if (!self.price.text.isPureFloat) {  return SHOW(@"请输入奖品价格");}
    
    if (self.areaID.length <=0 ) {  return SHOW(@"请选择区域"); }
    
    if (!self.frequency.text.isPureInt) { return SHOW(@"请输入开奖人次");}
    
    if (!self.lotteryFrequency.text.isPureInt) { return SHOW(@"请输入抽奖次数");}
    
    if (![self isEnergy]) { return SHOW(@"请选择抽奖所需能量"); }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setValue:self.titleLabel.text forKey:@"prize_title"];
    [parameters setValue:self.prizeDescription.text forKey:@"prize_detail"];
    [parameters setValue:self.price.text forKey:@"prize_price"];
    
    if (self.link.text.length > 0) {
        [parameters setValue:self.link.text forKey:@"prize_url"];
        
    }
    
    [parameters setValue:self.frequency.text forKey:@"number_of_lottery"];
    [parameters setValue:self.lotteryFrequency.text forKey:@"number_of_lucky_draw"];
    [parameters setValue:self.areaID forKey:@"scope"];
    [parameters setValue:[self enenrgy] forKey:@"energy_for_lucky_draw"];
    
    
    [TOOL showLoading:self.view];
    
    __weak typeof(self)weak  = self;
    
    [[MyNetWorking sharedInstance]lttImage:self.coverImage success:^(NSDictionary *success) {
        
        if([success[@"error"] intValue] == 0){
            
            NSString *ID = success[@"data"][@"id"];
            
            [parameters setValue:ID forKey:@"id"];
            
            [weak uploadImageWith:parameters ID:ID];
        }else{
            
            [TOOL hideLoading:self.view];
            SHOW(success[@"msg"]);
        }
        
        
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
        SHOW(failure.localizedDescription);
    }];
    
}

- (void)uploadImageWith:(NSDictionary *)parameter ID:(NSString *)ID {
    __weak typeof(self)weak = self;
    [[MyNetWorking sharedInstance]lttMoreImages:self.priceImage URL:LTTURL(@"uploadPictures") name:@"prize_pictures[]" ID:ID success:^(NSDictionary *success) {
        if([success[@"error"] intValue] == 0){
            
            [weak finishedWithPrameter:parameter];
        }else{
            
            [TOOL hideLoading:self.view];
            SHOW(success[@"msg"]);
        }
        
    } failure:^(NSError *failure) {
        [TOOL hideLoading:self.view];
        SHOW(failure.localizedDescription);
    }];
}

- (void)finishedWithPrameter:(NSDictionary *)parameter{
    
     [[MyNetWorking sharedInstance]LTTPostURL:LTTURL(@"publishPrize") params:parameter target:self success:^(NSDictionary *success) {
          [TOOL hideLoading:self.view];
          
          if ([success[@"error"] intValue] == 0) {
              
              SHOW(@"发布完成");
              
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  [self.navigationController popViewControllerAnimated:YES];
              });
              
          }else if([success[@"error"] intValue] == 2) {
              
              [self.navigationController pushViewController:RechargeViewController.new animated:YES];
              
          }else{
              SHOW(success[@"msg"]);
          }
          
          
      } failure:^(NSError *failure) {
          [TOOL hideLoading:self.view];
          SHOW(failure.localizedDescription);
      }];
    
}

- (BOOL)isEnergy{
    
    for (UIButton *sender in self.energys) {
        NSLog(@"%d",sender.isSelected);
        if (sender.isSelected) { return  YES; }
        
        
    }
    
    return NO;
}

- (NSString *)enenrgy{
    
    for (UIButton *sender in self.energys) {
        
        if (sender.isSelected) {
            
            if ([sender.currentTitle containsString:@"25"]) {
                return @"1";
            }else if ([sender.currentTitle containsString:@"50"]) {
                return @"2";
            }else if ([sender.currentTitle containsString:@"75"]) {
                return @"3";
            }else if ([sender.currentTitle containsString:@"100"]) {
                return @"4";
            }
        }
    }
    return  @"";
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (self.priceImage.count >= 9) {
        return 9;
    }
    
    return self.priceImage.count + 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    LTTOrderImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.imageView.backgroundColor = [UIColor clearColor];
    
    if (self.priceImage.count > indexPath.row) {
        cell.imageView.image = self.priceImage[indexPath.row];
    }else{
        cell.imageView.image = [UIImage imageNamed:@"2-0"];
    }
    
    return cell;
}


- (void)updateConstraints{
    
    [self.collectionView reloadData];
    
    CGFloat height = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    
    self.collectionHeight.constant = height;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < self.priceImage.count) {
        [self deleteImageWith:indexPath.row];
    }else{
        [self imageEventAction];
    }
    
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat width = collectionView.width - 24;
    
    return CGSizeMake(width / 3.0, width / 3.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 12.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 12.0f;
}

- (void)deleteImageWith:(NSInteger)index{
    
    __weak typeof(self)weak = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"删除" message:@"删除图片" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [weak.priceImage removeObjectAtIndex:index];
        [weak updateConstraints];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *value = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if ([string isEqualToString:@""]) { value = [value substringToIndex:range.location]; }
    
    if ([value integerValue] * 0.1 >300) {
        self.money.text = [NSString stringWithFormat:@"%.2f元",[value integerValue] * 0.1];
    }else{
        self.money.text = @"300元";
    }

    return [self isPureInt:string] || [string isEqualToString:@""];
    
}


- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}





/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
