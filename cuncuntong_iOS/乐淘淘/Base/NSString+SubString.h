//
//  NSArray+SubString.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//



#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (SubString)

- (NSArray *)substrings;

- (BOOL)isPureFloat;
 
// 判断是否为整型
- (BOOL)isPureInt;

@end

NS_ASSUME_NONNULL_END
