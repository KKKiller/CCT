//
//  UIImage+LTTImage.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/26.
//  Copyright © 2019 zhushuai. All rights reserved.
//



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (LTTImage)
- (UIImage *)lj_fixOrientationImage;
@end

NS_ASSUME_NONNULL_END
