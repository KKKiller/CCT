//
//  LTTImagePickerController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/29.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTImagePickerController.h"

@interface LTTImagePickerController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,copy)ImagePickerCompletion completion;

@end

@implementation LTTImagePickerController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = self;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)imageWithSelected:(ImagePickerCompletion)completion{
    
    self.completion = completion;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    if (self.completion) {
        self.completion(image);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}



@end
