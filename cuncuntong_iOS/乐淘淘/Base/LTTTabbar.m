//
//  KBTabbar.m
//  KBTabbarController
//
//  Created by kangbing on 16/5/31.
//  Copyright © 2016年 kangbing. All rights reserved.
//

#import "LTTTabbar.h"

@implementation LTTTabbar


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [btn setImage:[UIImage imageNamed:@"2"] forState:UIControlStateNormal];
        
        btn.bounds = CGRectMake(0, 0, 84, 84);
        
        [btn setTitle:@"发布" forState:UIControlStateNormal];
        
        [btn setTitleColor:HEXCOLOR(0xb5b5b5) forState:UIControlStateNormal];
        
        btn.titleLabel.font = [UIFont systemFontOfSize:9.5 weight:UIFontWeightBold];
        
        CGSize imgViewSize = btn.imageView.bounds.size;
        CGSize titleSize = btn.titleLabel.bounds.size;
        CGSize btnSize = btn.bounds.size;
    
        [btn setImageEdgeInsets: UIEdgeInsetsMake(5,0.0, btnSize.height - imgViewSize.height - 5, - titleSize.width)];

        [btn setTitleEdgeInsets:UIEdgeInsetsMake(imgViewSize.height, -(imgViewSize.width - 2), 3,0)];
        
        self.centerBtn = btn;
        
        [self addSubview:btn];
        
        
        
    }
    return self;
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    
    CGFloat itemW  = self.frame.size.width / 5;
    CGFloat itemIndex = 0;
    
    self.centerBtn.center = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height * 0.2);

    // 计算每个item位置
    for (UIView *child in self.subviews) {
        Class class = NSClassFromString(@"UITabBarButton");
        if ([child isKindOfClass:class]) {
            child.frame = CGRectMake(itemIndex *itemW, child.frame.origin.y, itemW, child.frame.size.height);
            itemIndex ++;
            if (itemIndex == 2) {
                itemIndex ++;
            }
        }
    }
    
}


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    if (self.isHidden == NO) {
        
        CGPoint newPoint = [self convertPoint:point toView:self.centerBtn];
        
        if ( [self.centerBtn pointInside:newPoint withEvent:event]) {
            return self.centerBtn;
        }else{
            
            return [super hitTest:point withEvent:event];
        }
    }
    
    else {
        return [super hitTest:point withEvent:event];
    }
}



@end
