//
//  LTTImagePickerController.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/29.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ImagePickerCompletion)(UIImage *image);


@interface LTTImagePickerController : UIImagePickerController


- (void)imageWithSelected:(ImagePickerCompletion)completion;



@end

NS_ASSUME_NONNULL_END
