//
//  NSArray+SubString.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/21.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "NSString+SubString.h"

@implementation NSString (SubString)

- (NSArray *)substrings{
    NSMutableArray *textArray = [NSMutableArray array];
    for (NSInteger i = 0; i < self.length; i++) {
        NSRange   range =  NSMakeRange(i, 1);
        NSString *subStr = [self substringWithRange:range];
        [textArray addObject:subStr];
    }
    return textArray;
}


- (BOOL)isPureFloat {
    NSScanner *scan = [NSScanner scannerWithString:self];
 
    float val;
 
    return [scan scanFloat:&val] && [scan isAtEnd];
}
 
// 判断是否为整型
- (BOOL)isPureInt{
    NSScanner *scan = [NSScanner scannerWithString:self];
 
    int val;
 
    return [scan scanInt:&val] && [scan isAtEnd];

}

@end
