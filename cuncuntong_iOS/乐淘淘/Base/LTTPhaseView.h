//
//  LTTPhaseView.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/30.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE @interface LTTPhaseView : UIView

@property (nonatomic,assign)IBInspectable NSInteger total;

@property (nonatomic,assign)IBInspectable NSInteger current;


@end

NS_ASSUME_NONNULL_END
