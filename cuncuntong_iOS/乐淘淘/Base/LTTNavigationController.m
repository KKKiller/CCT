//
//  LTTNavigationController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTNavigationController.h"
#import "UIImage+Color.h"
@interface LTTNavigationController ()

@end

@implementation LTTNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    [self.navigationBar setBackgroundImage:[UIImage imageWithColor:HEXCOLOR(0xED4741)] forBarMetrics:UIBarMetricsDefault];
      // #ED4741
       self.navigationBar.shadowImage = [UIImage new];
      // 0xF9484A
       [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count > 0) {
    
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
