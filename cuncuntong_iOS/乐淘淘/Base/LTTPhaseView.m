//
//  LTTPhaseView.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/30.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTPhaseView.h"


@interface LTTPhaseView ()

@property (nonatomic,strong)UIView *totalView;

@property (nonatomic,strong)UIView *currentView;

@property (nonatomic,strong)UILabel *currentLabel;

@property (nonatomic,strong)UILabel *totalLabel;

@end

@implementation LTTPhaseView


- (instancetype)initWithCoder:(NSCoder *)coder{
    
    if (self = [super initWithCoder:coder]) {
        
        [self configuration];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self configuration];
    }
    
    return self;
}

- (void)setTotal:(NSInteger)total{
    _total = total;
    self.totalLabel.text = [NSString stringWithFormat:@"总需%ld单",(long)total];
}

- (void)setCurrent:(NSInteger)current{
    _current = current;
    self.currentLabel.text = [NSString stringWithFormat:@"参与%ld单",(long)current];
    
}

#pragma mark - 配置
- (void)configuration{
    
    _total = 0;
    _current = 0;
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:25]];
    
    self.totalView = UIView.new;
    
    self.totalView.layer.cornerRadius = 3.0;
    
    self.totalView.layer.masksToBounds = YES;
    
    self.totalView.backgroundColor = HEXCOLOR(0x14b5a0);
    
    [self addSubview:self.totalView];
    
    [self.totalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(6);
    }];
    
    self.currentView = UIView.new;
    
    self.currentView.backgroundColor = HEXCOLOR(0xef5955);
    
    self.currentView.layer.cornerRadius = 3.0;
    
    self.currentView.layer.masksToBounds = YES;
    
    [self.totalView addSubview:self.currentView];
    
    [self.currentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self.totalView);
        make.width.mas_equalTo(0);
    }];
    
    UILabel *label = UILabel.new;
    label.text = @"0";
    label.textColor = HEXCOLOR(0x14b5a0);
    label.font = [UIFont systemFontOfSize:11];
    [self addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.bottom.equalTo(self.totalView.mas_top);
    }];
    
    self.currentLabel = UILabel.new;
    self.currentLabel.text = @"参与0单";
    self.currentLabel.textColor = HEXCOLOR(0x14b5a0);
    self.currentLabel.font = [UIFont systemFontOfSize:11];
    [self addSubview:self.currentLabel];
    
    [self.currentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.totalView.mas_top);
        make.right.equalTo(self.currentView);
    }];
    
    self.totalLabel = UILabel.new;
    self.totalLabel.text = @"总需0单";
    self.totalLabel.textColor = HEXCOLOR(0x14b5a0);
    self.totalLabel.font = [UIFont systemFontOfSize:11];
    [self addSubview:self.totalLabel];
    
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.totalView.mas_top);
        make.right.equalTo(self.totalView);
    }];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat a = self.total;
    
    CGFloat b = self.current;
    
    CGFloat scale = self.total > self.current ?  b / a : 1.0;
    
     self.currentLabel.hidden = !(scale > 0.1 && scale < 0.9);

    [self.currentView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(scale * self.frame.size.width);
    }];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
