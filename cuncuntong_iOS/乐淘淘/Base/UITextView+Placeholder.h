//
//  UITextView+Placeholder.h
//  CunCunTong
//
//  Created by Whisper on 2019/10/19.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (Placeholder)

-(void)setPlaceholder:(NSString *)placeholdStr placeholdColor:(UIColor *)placeholdColor;

@end

NS_ASSUME_NONNULL_END
