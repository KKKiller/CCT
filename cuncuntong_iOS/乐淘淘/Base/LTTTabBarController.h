//
//  LTTTabBarController.h
//  CunCunTong
//
//  Created by W&Z on 2019/10/13.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTTTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
