//
//  UITextViewWorkaround.h
//  CunCunTong
//
//  Created by W&Z on 2019/11/8.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextViewWorkaround : NSObject

+ (void)executeWorkaround; 
@end

NS_ASSUME_NONNULL_END
