//
//  LTTBaseViewController.m
//  CunCunTong
//
//  Created by W&Z on 2019/10/14.
//  Copyright © 2019 zhushuai. All rights reserved.
//

#import "LTTBaseViewController.h"

@interface LTTBaseViewController ()

@end

@implementation LTTBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc]init];
    
    back.tintColor = [UIColor whiteColor];
    back.title = @"";
    
    self.navigationItem.backBarButtonItem = back;
    
    
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
