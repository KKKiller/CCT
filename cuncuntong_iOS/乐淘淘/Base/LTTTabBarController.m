//
//  LTTTabBarController.m
//  CunCunTong
//
//  Created by W&Z on 3019/10/13.
//  Copyright © 3019 zhushuai. All rights reserved.
//

#import "LTTTabBarController.h"
#import "LTTHomeViewController.h"
#import "LTTOrderViewController.h"
#import "LTTPublishViewController.h"
#import "LTTCollectViewController.h"
#import "LTTMineViewController.h"
#import "LTTNavigationController.h"
#import "LTTShowOrderViewController.h"
#import "LTTTabbar.h"
@interface LTTTabBarController ()

@end

@implementation LTTTabBarController


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setBarItemWithViewControoler:[LTTHomeViewController new]
                                 title:@"首页"
                                 image:@"0-n"
                         selectedImage:@"0-s"
     ];
    [self setBarItemWithViewControoler:[[LTTShowOrderViewController alloc]initWithNibName:@"LTTShowOrderViewController" bundle:nil]
                                 title:@"晒单"
                                 image:@"1-n"
                         selectedImage:@"1-s"
     ];
    
    [self setBarItemWithViewControoler:[[LTTCollectViewController alloc]initWithNibName:@"LTTCollectViewController" bundle:nil]
                                 title:@"收藏"
                                 image:@"3-n"
                         selectedImage:@"3-s"
     ];
    [self setBarItemWithViewControoler:[[LTTMineViewController alloc]initWithNibName:@"LTTMineViewController" bundle:nil]
                                 title:@"我的"
                                 image:@"4-n"
                         selectedImage:@"4-s"
     ];
    
    
    // 设置自定义的tabbar
    [self setCustomtabbar];
    
}

- (void)setCustomtabbar{
    
    LTTTabbar *tabbar = [[LTTTabbar alloc]init];
    tabbar.barTintColor = [UIColor whiteColor];
    [self setValue:tabbar forKeyPath:@"tabBar"];
    
    [tabbar.centerBtn addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)centerBtnClick:(UIButton *)btn{
    
    UINavigationController *nav = self.selectedViewController;
    
    [nav pushViewController: [[LTTPublishViewController alloc]initWithNibName:@"LTTPublishViewController" bundle:nil] animated:YES];
}

- (void)setBarItemWithViewControoler:(UIViewController *)viewController
                               title:(NSString *)title
                               image:(NSString *)image
                       selectedImage:(NSString *)selectedImage{
    
    LTTNavigationController *nav =  [[LTTNavigationController alloc]initWithRootViewController:viewController];
    
    [self setBackWithViewController:viewController];
    
    nav.tabBarItem.title = title;
    
    [nav.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:HEXCOLOR(0xb5b5b5)} forState:UIControlStateNormal];
    
    [nav.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:HEXCOLOR(0xee2be6)} forState:UIControlStateSelected];
    
    nav.tabBarItem.image = [[UIImage imageNamed:image ]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    nav.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self addChildViewController:nav];
}


- (void)setBackWithViewController:(UIViewController *)viewController {
    
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]init];
    
    viewController.navigationItem.leftBarButtonItem.image = [UIImage imageNamed:@"btn_back"];
    
    viewController.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    viewController.navigationItem.leftBarButtonItem.action = @selector(back);
}


- (void)back{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}








/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
